package im.telegraph.generator.type;

public class SchemeBaseResponse extends SchemeContainer implements SchemeRecord {
    private int header;

    public SchemeBaseResponse(int header) {
        this.header = header;
    }

    public int getHeader() {
        return header;
    }
}