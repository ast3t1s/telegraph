package im.telegraph.pubsub;

import im.telegraph.cluster.Cluster;
import im.telegraph.cluster.Member;
import im.telegraph.cluster.event.ClusterInitialized;
import im.telegraph.cluster.event.ClusterMembershipEvent;
import im.telegraph.internal.remote.Address;
import im.telegraph.internal.remote.RemoteMessage;
import im.telegraph.internal.remote.RemoteTransport;
import im.telegraph.common.ResettableInterval;
import im.telegraph.common.property.pubsub.PubSubProperties;
import im.telegraph.pubsub.internal.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import reactor.core.Disposable;
import reactor.core.Disposables;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Sinks;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuples;

import java.time.Duration;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Ast3t1s
 */
public class DistributedPubSubMediator implements PubSubMediator, DisposableBean {

    private static final Logger log = LoggerFactory.getLogger(DistributedPubSubMediator.class);

    private final Set<Address> nodes = new CopyOnWriteArraySet<>();
    private final Map<String, Sinks.Many<Object>> sinks = new ConcurrentHashMap<>();
    private final AtomicReference<Address> selfAddress = new AtomicReference<>();

    private final Sinks.One<Void> started = Sinks.one();
    private final Sinks.Many<String> subscriptions;

    private final Map<Address, Bucket> registry = new ConcurrentHashMap<>();
    private final Disposable.Composite forCleanUp = Disposables.composite();

    private final AtomicLong version = new AtomicLong(0L);
    private final AtomicInteger deltaCount = new AtomicInteger(0);

    private final PubSubProperties properties;
    private final Cluster cluster;
    private final RemoteTransport transport;

    private final Scheduler scheduler;
    private final Duration pruneInterval;
    private final Duration gossipInterval;
    private final ResettableInterval pruneTick;
    private final ResettableInterval gossipTick;

    public DistributedPubSubMediator(PubSubProperties properties, Cluster cluster, RemoteTransport transport) {
        this.properties = properties;
        this.cluster = cluster;

        this.gossipInterval = properties.getGossipInterval();
        this.pruneInterval = properties.getRemovedTtl().dividedBy(2);
        this.transport = transport;

        this.scheduler = Schedulers.newSingle(getClass().getSimpleName().toLowerCase(), true);
        this.gossipTick = new ResettableInterval(scheduler, gossipInterval, gossipInterval,
                tick -> Mono.fromRunnable(this::gossip));
        this.pruneTick = new ResettableInterval(scheduler, pruneInterval, pruneInterval,
                tick -> Mono.fromRunnable(this::pruneSubscriptions));

        this.subscriptions = Sinks.many().replay().limit(1024);
        startListeners();
    }

    private void startListeners() {
        forCleanUp.add(transport.receive()
                .publishOn(scheduler)
                .filter(message -> message.getPayload() instanceof PubSubMessage)
                .doOnNext(this::handlePubSubMessage)
                .subscribe(null, t -> log.warn("Distributed pubsub receiver terminated with an error", t)));

        forCleanUp.add(subscriptions.asFlux()
                .delayUntil(__ -> started.asMono())
                .doOnNext(this::handleRegisterTopic)
                .subscribe(null,
                        t -> log.warn("Distributed pubsub subscription listener terminated with an error", t)));

        forCleanUp.add(cluster.subscribe()
                .ofType(ClusterInitialized.class)
                .doOnNext(this::initialized)
                .subscribe(null,
                        t -> log.warn("Distributed pubSub cluster initialized listener terminated with an error", t)));

        forCleanUp.add(cluster.subscribe()
                .ofType(ClusterMembershipEvent.class)
                .flatMap(this::handleMembershipEvent)
                .subscribe(null,
                        t -> log.warn("Distributed pubSub cluster membership listener terminated with an error", t)));
    }

    private void handlePubSubMessage(RemoteMessage message) {
        log.info("Handle pub sub message {}", message);
        Address sender = message.getSender();
        if (message.getPayload() instanceof Status) {
            handleStatus(sender, (Status) message.getPayload());
        } else if (message.getPayload() instanceof Delta) {
            handleDelta(sender, (Delta) message.getPayload());
        } else if (message.getPayload() instanceof Publish) {
            Publish publish = (Publish) message.getPayload();
            publishToSink(publish.getData().getTopic(), publish.getData().getPayload());
        } else if (message.getPayload() instanceof Send) {
            Send send = (Send) message.getPayload();
            publishToSink(send.getData().getTopic(), send.getData().getPayload());
        } else if (message.getPayload() instanceof SendToAll) {
            SendToAll sendToAll = (SendToAll) message.getPayload();
            publishToSink(sendToAll.getData().getTopic(), sendToAll.getData().getPayload());
        }
    }

    private void publishToSink(String topic, Object payload) {
        Optional.ofNullable(sinks.get(topic)).ifPresent(sink -> sink.tryEmitNext(payload));
    }

    private Mono<Void> handleMembershipEvent(ClusterMembershipEvent event) {
        return Mono.fromRunnable(
                () -> {
                    final Member member = event.member();
                    if (event.isAdded()) {
                        nodes.add(member.getAddress());
                        log.info("Added {} to distributed pubSub node list (size={})", member, nodes.size());
                    }
                    if (event.isRemoved()) {
                        if (member.getAddress().equals(selfAddress.get())) {
                            log.warn("Current node {} was deleted", member);
                        } else {
                            nodes.remove(member.getAddress());
                            registry.remove(member.getAddress());
                            log.debug("Remove {} from distributed pubSub node list (size={})", member, nodes.size());
                        }
                    }
                });
    }

    @Override
    public Mono<Void> publish(String topic, Object payload) {
        return Mono.defer(() -> publishMessage(topic, payload, false,
                address -> transport.send(address, new Publish(topic, payload))));
    }

    @Override
    public Mono<Void> send(String topic, String destination, Object payload) {
        Mono<Address> selectForSend = destination == null ?
                Mono.justOrEmpty(selectForSend(topic)) : cluster.find(destination).map(Member::getAddress);
        return Mono.defer(() -> selectForSend)
                .flatMap(member -> transport.send(member, new Send(topic, payload)))
                .then();
    }

    @Override
    public Mono<Void> sendToAll(String topic, Object payload, boolean allButSelf) {
        return Mono.defer(() -> publishMessage(topic, payload, allButSelf, address ->
                transport.send(address, new SendToAll(topic, payload, allButSelf))));
    }

    private Mono<Void> publishMessage(String topic, Object payload, boolean allButSelf,
                                      Function<Address, Mono<Void>> processor) {
        return Flux.fromIterable(selectForPublish(topic))
                .doOnNext(address -> {
                    if (Objects.equals(address, selfAddress.get()) && !allButSelf) {
                        publishToSink(topic, payload);
                    } else {
                        processor.apply(address).subscribe();
                    }
                })
                .then();
    }

    private List<Address> selectForPublish(String topic) {
       return registry.entrySet().stream()
               .filter(entry -> entry.getValue().getContent() != null)
               .filter(entry -> entry.getValue().getContent().containsKey(topic))
               .map(Map.Entry::getKey)
               .collect(Collectors.toList());
    }
    
    private Address selectForSend(String topic) {
        List<Address> candidates = selectForPublish(topic);
        if (candidates.isEmpty()) {
            return null;
        }
        return candidates.get(ThreadLocalRandom.current().nextInt(candidates.size()));
    }

    @Override
    public <T> Flux<T> receive(String topic, Class<T> type) {
        return Flux.defer(() -> {
            subscriptions.tryEmitNext(Objects.requireNonNull(topic));
            return getSink(topic).asFlux()
                    .onBackpressureBuffer()
                    .ofType(type)
                    .doOnNext(payload -> log.info("Received from topic: {} sub: {}", topic, payload))
                    .onErrorContinue((throwable, o) -> log.warn("Unexpected error", throwable));
        });
    }

    private Sinks.Many<Object> getSink(String topic) {
        return sinks.computeIfAbsent(topic, s -> Sinks.many().replay().limit(1024));
    }

    private void handleDelta(Address sender, Delta delta) {
        deltaCount.incrementAndGet();

        // reply from Status message in the gossip chat
        // the Delta contains potential updates (newer versions) from the other node
        // only accept deltas/buckets from known nodes, otherwise there is a risk of
        // adding back entries when nodes are removed
        if (nodes.contains(sender)) {
            for (final var bucket : delta.getData().getBuckets()) {
                if (nodes.contains(bucket.getAddress())) {
                    var myBucket = registry.get(bucket.getAddress());
                    if (myBucket == null) {
                        myBucket = new Bucket(bucket.getAddress(), 0, Collections.emptyMap());
                    }
                    if (bucket.getVersion() > myBucket.getVersion()) {
                        final var content = new HashMap<>(myBucket.getContent());
                        content.putAll(bucket.getContent());
                        registry.put(bucket.getAddress(), new Bucket(myBucket.getAddress(), bucket.getVersion(), content));
                    }
                }
            }
        }
    }

    private void handleStatus(Address sender, Status status) {
        if (nodes.contains(sender)) {
            // gossip chat starts with a Status message, containing the bucket versions of the other node
            final var delta = collectData(status.getData().getVersions()
                    .stream()
                    .map(inner -> Tuples.of(inner.getAddress(), inner.getTimestamp()))
                    .collect(Collectors.toMap(Tuple2::getT1, Tuple2::getT2)));
            if (delta.size() > 0) {
                transport.send(sender, new Delta(delta))
                        .subscribe(null, t -> log.warn("Unexpected error due sending delta", t));
            }
            if (!status.getData().isReplyToStatus() && otherHasNewerVersions(Collections.emptyMap())) {
                transport.send(sender, new Status(getOwnVersion(), true))
                        .subscribe(null, t -> log.warn("Unexpected error due sending status", t));
            }
        }
        log.info("Registry size {}", registry.size());
    }

    private void handleRegisterTopic(String topic) {
        final var version = nextVersion();
        final var selfAddress = this.selfAddress.get();
        final var bucket = registry.get(selfAddress);
        if (bucket == null) {
            registry.put(selfAddress, new Bucket(selfAddress, version,
                    Collections.singletonMap(topic, new ValueHolder(version, topic))));
        } else {
            final var content = new LinkedHashMap<>(bucket.getContent());
            content.put(topic, new ValueHolder(version, topic));
            registry.put(selfAddress, new Bucket(bucket.getAddress(), version, content));
        }
    }

    private boolean otherHasNewerVersions(Map<Address, Long> versions) {
        return versions.entrySet().stream().anyMatch(entry -> {
            Bucket bucket = registry.get(entry.getKey());
            if (bucket != null) {
                return entry.getValue() > bucket.getVersion();
            } else {
                return entry.getValue() > 0;
            }
        });
    }

    private List<Bucket> collectData(Map<Address, Long> versions) {
        // Missing entries are represented by version 0
        final var filledOtherVersions = getOwnVersion().entrySet()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getKey, entry -> 0L));
        filledOtherVersions.putAll(versions);

        int count = 0;
        final List<Bucket> result = new ArrayList<>();

        for (final var entry : filledOtherVersions.entrySet()) {
            final var owner = entry.getKey();
            final var version = entry.getValue();

            Bucket bucket = registry.get(owner);
            if (bucket == null) {
                bucket = new Bucket(owner, 0L, Collections.emptyMap());
            }

            if (bucket.getVersion() > version && count < properties.getMaxDeltaElements()) {
                final var deltaContent = bucket.getContent().entrySet()
                        .stream()
                        .filter(bucketEntry -> bucketEntry.getValue().getVersion() > version)
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

                count += deltaContent.size();

                if (count <= properties.getMaxDeltaElements()) {
                    result.add(new Bucket(bucket.getAddress(), bucket.getVersion(), bucket.getContent()));
                } else {
                    // Exceeded the maxDeltaElements, pick the elements with the lowest versions
                    final var sortedContent = deltaContent.entrySet()
                            .stream()
                            .sorted(Comparator.comparingLong(o -> o.getValue().getVersion()))
                            .collect(Collectors.toList());
                    final var chunk = sortedContent
                            .stream()
                            .limit(properties.getMaxDeltaElements() - (count - sortedContent.size()))
                            .collect(Collectors.toList());
                    final var content = chunk
                            .stream()
                            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

                    result.add(new Bucket(bucket.getAddress(),
                            chunk.get(chunk.size() - 1).getValue().getVersion(), content));
                }
            }
        }
        return result;
    }

    private Map<Address, Long> getOwnVersion() {
        return registry.entrySet().stream()
                .map(entry -> Tuples.of(entry.getKey(), entry.getValue().getVersion()))
                .collect(Collectors.toMap(Tuple2::getT1, Tuple2::getT2));
    }

    private void gossip() {
        selectRandomNode().ifPresent(this::gossipTo);
    }

    private void gossipTo(final Address address) {
        log.info("Send pubSub gossip to {}", address);
        transport.send(address, new Status(getOwnVersion(), false))
                .subscribe(null, t -> log.warn("Unexpected error due pubSub gossiping", t));
    }

    private Optional<Address> selectRandomNode() {
        List<Address> candidates = nodes.stream()
                .filter(address -> !Objects.equals(selfAddress.get(), address))
                .collect(Collectors.toList());
        if (candidates.isEmpty()) {
            return Optional.empty();
        }
        Collections.shuffle(candidates);
        return Optional.of(candidates.get(ThreadLocalRandom.current().nextInt(candidates.size())));
    }

    private void pruneSubscriptions() {
        final var modifications = new LinkedHashMap<Address, Bucket>();
        for (final var entry : registry.entrySet()) {
            final var owner = entry.getKey();
            final var bucket = entry.getValue();

            final var oldRemoved = bucket.getContent().entrySet()
                    .stream()
                    .filter(old -> (bucket.getVersion() - old.getValue().getVersion()) > properties.getRemovedTtl().toMillis())
                    .map(Map.Entry::getKey)
                    .collect(Collectors.toList());

            if (oldRemoved.size() > 0) {
                final var content = new HashMap<>(bucket.getContent());
                oldRemoved.forEach(content::remove);
                modifications.put(owner, new Bucket(bucket.getAddress(), bucket.getVersion(), content));
            }
        }
        modifications.forEach(registry::put);
    }

    private long nextVersion() {
        var current = System.currentTimeMillis() / 10000;
        if (current > version.get()) {
            version.set(current);
            return version.get();
        } else {
            return version.updateAndGet(operand -> operand + 1);
        }
    }

    private void initialized(ClusterInitialized event) {
        if (selfAddress.compareAndSet(null, event.getSelfAddress())) {
            pruneTick.start();
            gossipTick.start();
            started.tryEmitEmpty();
        }
    }

    @Override
    public void destroy() {
        gossipTick.stop();
        pruneTick.stop();
        forCleanUp.dispose();
        scheduler.dispose();
        nodes.clear();
        registry.clear();
        selfAddress.set(null);
    }
}
