package im.telegraph.domain.repository;

import im.telegraph.domain.entity.Dialog;
import im.telegraph.domain.entity.Id;
import im.telegraph.domain.entity.Peer;
import org.reactivestreams.Publisher;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Instant;

public interface DialogRepository {

    /**
     * Saves a dialog in the repository.
     *
     * @param dialog the dialog entity to save
     * @return A {@link Mono} that completes when the dialog is successfully saved. If an error occurs during the save,
     * the {@code Mono} emits an error.
     */
    Mono<Void> save(Dialog dialog);

    Mono<Void> save(Publisher<Dialog> save);

    Mono<Void> saveAndIncrementCounter(Dialog dialog);

    Mono<Void> saveAndIncrementCounter(Publisher<Dialog> entryStream);

    Mono<Void> resetUnread(Id userId, Peer peer, Id messageId, Instant date);

    /**
     * Finds a dialog by the user ID and peer.
     *
     * @param userId the ID of the user
     * @param peer the peer associated with the dialog
     * @return A {@link Mono} that emits the {@link Dialog} instance if found, or an empty {@code Mono} if no dialog
     * is found. If an error occurs during the search, it is emitted through the {@code Mono}.
     */
    Mono<Dialog> findByUserIdAndPeer(Id userId, Peer peer);

    /**
     * Finds all dialogs by the user ID with pagination support.
     *
     * @param userId the ID of the user
     * @param limit the maximum number of dialogs to return
     * @param offset the offset for pagination
     * @return A {@link Flux} that emits the {@link Dialog} instances for the given {@link UserId}.
     * If no dialogs are found, the {@code Flux} completes empty. If an error occurs during the search, it is emitted
     * through the {@code Flux}.
     */
    Flux<Dialog> findAllByUserId(Id userId, int limit, int offset);

    /**
     * Finds all dialogs by the user ID where the last message date is less than or equal to the specified date,
     * with a limit on the number of results.
     *
     * @param userId the ID of the user
     * @param lastMessageDate the maximum date of the last message to include
     * @param limit the maximum number of dialogs to return
     * @return A {@link Flux} that emits the {@link Dialog} instances matching the criteria.
     * If no dialogs are found, the {@code Flux} completes empty. If an error occurs during the search, it is emitted
     * through the {@code Flux}.
     */
    Flux<Dialog> finAllByUserIdAndDateLessThanEqual(Id userId, Instant lastMessageDate, int limit);

    /**
     * Counts the total number of dialogs for a given user ID.
     *
     * @param userId the ID of the user
     * @return A {@link Mono} that emits the total number of dialogs as a {@link Long}. If an error occurs during the count,
     * it is emitted through the {@code Mono}.
     */
    Mono<Long> countByUserId(Id userId);

    /**
     * Deletes a dialog by the user ID and peer.
     *
     * @param userId the ID of the user
     * @param peer the peer associated with the dialog
     * @return A {@link Mono} that completes when the dialog is successfully deleted. If the dialog does not exist or an error
     * occurs during deletion, the {@code Mono} emits an error.
     */
    Mono<Void> deleteByUserIdAndPeer(Id userId, Peer peer);
}