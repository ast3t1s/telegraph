package im.telegraph.common.rdbms;

import io.r2dbc.spi.Result;
import org.reactivestreams.Publisher;
import reactor.core.publisher.Flux;

import java.util.Objects;
import java.util.function.Function;

/**
 * @author Ast3t1s
 */
public final class Query extends StatementSpec<Query> implements ResultBearing {

    Query(Handle handle, String sql) {
        super(handle, sql);
    }

    @Override
    public <T> Flux<T> mapResult(Function<Result, ? extends Publisher<? extends T>> mappingFunction) {
        Objects.requireNonNull(mappingFunction);
        return Flux
                .from(prepareStatement(this::getSql).execute())
                .flatMap(mappingFunction);
    }

    @Override
    public String toString() {
        return "Query{" +
                "statement=" + getSql() +
                '}';
    }
}
