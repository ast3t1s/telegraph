package im.telegraph;

import im.telegraph.domain.entity.Id;
import im.telegraph.domain.entity.chat.Chat;
import im.telegraph.domain.entity.chat.ChatParticipant;
import im.telegraph.domain.entity.chat.PermissionSet;

import java.time.Instant;

public final class EntityUtils {

    private static final String DEFAULT_TITLE = "Chat title";
    private static final String DEFAULT_ABOUT = "Chat about";

    public static Chat createChat(long ownerId, PermissionSet permissions, int membersCount, long updatedBy) {
        return Chat.builder().ownerUserId(Id.of(ownerId))
                .permissions(permissions.getRawValue())
                .membersCount(membersCount)
                .updatedById(Id.of(updatedBy))
                .createdAt(Instant.now())
                .updatedAt(Instant.now())
                .build();
    }

    public static ChatParticipant createChatOwner(long userId, long chatId) {
        return ChatParticipant.builder()
                .chatId(Id.of(chatId))
                .userId(Id.of(userId))
                .createdAt(Instant.now())
                .inviterId(Id.of(userId))
                .status(ChatParticipant.Status.OWNER)
                .build();
    }
    public static ChatParticipant createChatParticipant(long userId, long inviterId, long chatId) {
        return ChatParticipant.builder()
                .chatId(Id.of(chatId))
                .userId(Id.of(userId))
                .createdAt(Instant.now())
                .inviterId(Id.of(inviterId))
                .status(ChatParticipant.Status.PARTICIPANT)
                .build();
    }
} 
