package im.telegraph.common.property.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.time.Duration;

/**
 * @author Ast3t1s
 */
@Getter
@Setter
@Accessors(chain = true)
public class SessionProperties {

    /**
     * Time to live of the session.
     */
    private Duration ttl = Duration.ofSeconds(120);

    /**
     * Storage backend possible types: memory, redis, rdbms.
     */
    private String storage = "memory";
} 
