package im.telegraph.handlers;

import im.telegraph.api.ApiAbsUser;
import im.telegraph.api.ApiAvatar;
import im.telegraph.api.ApiUserEmpty;
import im.telegraph.api.ApiUserOutPeer;
import im.telegraph.api.rpc.*;
import im.telegraph.common.Page;
import im.telegraph.domain.entity.Id;
import im.telegraph.handlers.dispatch.DispatchContext;
import im.telegraph.handlers.internal.ApiEntityMapper;
import im.telegraph.handlers.mapper.UserConverter;
import im.telegraph.service.ContactsService;
import im.telegraph.service.PhotoService;
import im.telegraph.service.UpdatesService;
import im.telegraph.service.UserService;
import org.reactivestreams.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.function.TupleUtils;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class UserDispatchHandlers {

    private static final Logger log = LoggerFactory.getLogger(UserDispatchHandlers.class);

    private final UserService userService;
    private final ContactsService contactsService;
    private final PhotoService photoService;
    private final UpdatesService updateService;

    private final UserConverter userConverter;

    public UserDispatchHandlers(UserService userService,
                                ContactsService contactsService,
                                PhotoService photoService,
                                UpdatesService updateService,
                                UserConverter userConverter) {
        this.userService = userService;
        this.contactsService = contactsService;
        this.photoService = photoService;
        this.updateService = updateService;
        this.userConverter = userConverter;
    }

    /**
     * Invoked when the client attempt to check if username is already busy.
     *
     * @param context the client data.
     * @return a {@link Publisher} that completes when this listener has done processing the incoming request, for
     * example returning any {@link Mono}, {@link Flux} or  synchronous code using {@link Mono#fromRunnable(Runnable)}.
     */
    public Mono<ResponseBool> handleCheckUsername(DispatchContext<RequestProfileCheckNickName> context) {
        return userService.checkUsernameExists(context.getRequest().getNickname()).map(ResponseBool::new);
    }

    /**
     * Invoked when the client attempt to check if email is already busy.
     *
     * @param context the client data.
     * @return a {@link Publisher} that completes when this listener has done processing the incoming request, for
     * example returning any {@link Mono}, {@link Flux} or  synchronous code using {@link Mono#fromRunnable(Runnable)}.
     */
    public Mono<ResponseBool> handleCheckEmail(DispatchContext<RequestProfileCheckEmail> context) {
        return userService.checkEmailExists(context.getRequest().getEmail()).map(ResponseBool::new);
    }

    /**
     * Invoked when the client attempt to get current user info.
     *
     * @param context the client data.
     * @return a {@link Publisher} that completes when this listener has done processing the incoming request, for
     * example returning any {@link Mono}, {@link Flux} or  synchronous code using {@link Mono#fromRunnable(Runnable)}.
     */
    public Mono<ResponseProfileGetUserSelf> handleGetUserSelf(DispatchContext<RequestProfileGetUserSelf> context) {
        return Mono.justOrEmpty(context.getUserId().map(Id::of))
                .flatMap(selfId -> userService.findById(selfId)
                        .map(user -> Optional.ofNullable(userConverter.toApiUserFull(context.getAuthId(), user)))
                        .filter(Optional::isPresent)
                        .map(Optional::get)
                        .defaultIfEmpty(new ApiUserEmpty(selfId.asLong())))
                .map(ResponseProfileGetUserSelf::new);
    }

    /**
     * Invoked when the client attempt to get extended user info by ID.
     *
     * @param context the client data.
     * @return a {@link Publisher} that completes when this listener has done processing the incoming request, for
     * example returning any {@link Mono}, {@link Flux} or  synchronous code using {@link Mono#fromRunnable(Runnable)}.
     */
    public Mono<ResponseProfileGetFullUser> handleGetUserFull(DispatchContext<RequestProfileGetFullUser> context) {
        return Mono.fromCallable(context.getRequest()::getUser)
                .filterWhen(peer -> userService.checkAccessHash(Id.of(peer.getUid()), context.getAuthId(), peer.getAccessHash()))
                .flatMap(peer -> userService.findById(Id.of(peer.getUid()))
                        .map(user -> Optional.ofNullable(userConverter.convert(context.getAuthId(), user)))
                        .filter(Optional::isPresent)
                        .map(Optional::get))
                .defaultIfEmpty(new ApiUserEmpty(context.getRequest().getUser().getUid()))
                .map(ResponseProfileGetFullUser::new);
    }

    /**
     * Invoked when the client attempt to get basic user info according to their identifiers.
     *
     * @param context the client data.
     * @return a {@link Publisher} that completes when this listener has done processing the incoming request, for
     * example returning any {@link Mono}, {@link Flux} or  synchronous code using {@link Mono#fromRunnable(Runnable)}.
     */
    public Mono<ResponseProfileGetUsers> handleGetUsers(DispatchContext<RequestProfileGetUsers> context) {
        return Flux.fromIterable(context.getRequest().getUsers())
                .filterWhen(peer -> userService.checkAccessHash(Id.of(peer.getUid()), context.getAuthId(), peer.getAccessHash()))
                .map(ApiUserOutPeer::getUid)
                .map(Id::of)
                .collectList()
                .flatMapMany(userService::getUsers)
                .map(user -> Optional.ofNullable(userConverter.convert(context.getAuthId(), user)))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collectList()
                .defaultIfEmpty(context.getRequest().getUsers()
                        .stream()
                        .map(peer -> new ApiUserEmpty(peer.getUid()))
                        .collect(Collectors.toList()))
                .map(ResponseProfileGetUsers::new);
    }

    /**
     * Invoked when the client attempt to update user username.
     *
     * @param context the client data.
     * @return a {@link Publisher} that completes when this listener has done processing the incoming request, for
     * example returning any {@link Mono}, {@link Flux} or  synchronous code using {@link Mono#fromRunnable(Runnable)}.
     */
    public Mono<ResponseUser> handleEditUsername(DispatchContext<RequestProfileEditNickName> context) {
        return Mono.justOrEmpty(context.getUserId().map(Id::of))
                .flatMap(selfId -> userService.editNick(selfId, context.getRequest().getNickname()))
                .map(user -> Optional.ofNullable(userConverter.convert(context.getAuthId(), user)))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .defaultIfEmpty(new ApiUserEmpty())
                .map(ResponseUser::new);
    }

    /**
     * Invoked when an request relevant to the current user is attempt to update user's email.
     *
     * @param context the client data.
     * @return a {@link Publisher} that completes when this listener has done processing the incoming request, for
     * example returning any {@link Mono}, {@link Flux} or  synchronous code using {@link Mono#fromRunnable(Runnable)}.
     */
    public Mono<ResponseUser> handleEditEmail(DispatchContext<RequestProfileEditEmail> context) {
        return Mono.justOrEmpty(context.getUserId().map(Id::of))
                .flatMap(selfId -> userService.editEmail(selfId, context.getRequest().getEmail())
                        .then(userService.findById(selfId)))
                .map(user -> Optional.ofNullable(userConverter.convert(context.getAuthId(), user)))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .defaultIfEmpty(new ApiUserEmpty())
                .map(ResponseUser::new);
    }

    /**
     * Invoked when an user try to change password.
     *
     * @param context the client data.
     * @return a {@link Publisher} that completes when this listener has done processing the incoming request, for
     * example returning any {@link Mono}, {@link Flux} or  synchronous code using {@link Mono#fromRunnable(Runnable)}.
     */
    public Mono<ResponseVoid> handleChangePassword(DispatchContext<RequestProfileChangePassword> context) {
        return Mono.justOrEmpty(context.getUserId().map(Id::of))
                .flatMap(selfId -> userService.changePassword(selfId, context.getRequest().getOldPassword(), context.getRequest().getNewPassword()))
                .thenReturn(new ResponseVoid());
    }

    // photos namespace
    // ======================

    public Mono<ResponsePhotosUpdateAvatarPhoto> handleUpdateProfilePhoto(DispatchContext<RequestPhotosUpdateAvatarPhoto> context) {
        final long photoId = context.getRequest().getId();
        final long accessHash = context.getRequest().getAccessHash();

        return Mono.justOrEmpty(context.getUserId().map(Id::of))
                .flatMap(selfId -> photoService.getPhoto(Id.of(photoId), accessHash)
                        .flatMap(photo -> userService.editPhoto(selfId, photo)))
                .map(user -> user.getProfilePhoto().map(userConverter::convertToPhoto))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(ResponsePhotosUpdateAvatarPhoto::new);
    }

    public Mono<ResponsePhotosUploadAvatarPhoto> handleUploadProfilePhoto(DispatchContext<RequestPhotosUploadAvatarPhoto> context) {
        final long fileId = context.getRequest().getFileLocation().getFileId();
        final long accessHash = context.getRequest().getFileLocation().getAccessHash();

        return Mono.justOrEmpty(context.getUserId().map(Id::of))
                .flatMap(selfId -> photoService.uploadProfilePhoto(selfId, Id.of(fileId), accessHash))
                .flatMap(photo -> {
                    Mono<List<ApiAbsUser>> apiUsers = userService.getUsers(Collections.singletonList(photo.getUserId()))
                            .map(user -> Optional.ofNullable(userConverter.convert(context.getAuthId(), user)))
                            .filter(Optional::isPresent)
                            .map(Optional::get)
                            .collectList()
                            .defaultIfEmpty(Collections.emptyList());

                    return Mono.justOrEmpty(ApiEntityMapper.convertToApiAvatar(photo)).zipWith(apiUsers);
                })
                .map(TupleUtils.function(ResponsePhotosUploadAvatarPhoto::new));
    }

    public Mono<ResponsePhotosDeleteAvatarPhotos> handleDeletePhotos(DispatchContext<RequestPhotosDeleteAvatarPhotos> context) {
        return Mono.justOrEmpty(context.getUserId().map(Id::of))
                .flatMapMany(selfId -> Flux.fromIterable(context.getRequest().getId())
                        .distinct()
                        .mapNotNull(Id::of)
                        .collectList()
                        .flatMapMany(photoIds -> photoService.deletePhotos(selfId, photoIds))
                        .map(Id::asLong))
                .collectList()
                .defaultIfEmpty(Collections.emptyList())
                .map(ResponsePhotosDeleteAvatarPhotos::new);
    }

    public Mono<ResponsePhotosGetAvatarPhotos> handleGetUserPhotos(DispatchContext<RequestPhotosGetAvatarPhotos> context) {
        final Id userId = Id.of(context.getRequest().getUserId());
        final int limit = context.getRequest().getLimit();
        final int offset = context.getRequest().getOffset();

        return photoService.getUserPhotos(userId, limit, offset)
                .defaultIfEmpty(new Page<>(Collections.emptyList()))
                .flatMap(photos -> {
                    Mono<Integer> count = Mono.justOrEmpty((int) photos.getTotalCount())
                            .defaultIfEmpty(0);

                    Mono<List<ApiAvatar>> apiPhotos = Flux.fromIterable(photos)
                            .map(avatar -> Optional.ofNullable(ApiEntityMapper.convertToApiAvatar(avatar)))
                            .filter(Optional::isPresent)
                            .map(Optional::get)
                            .collectList()
                            .defaultIfEmpty(Collections.emptyList());

                    return Mono.zip(count, apiPhotos);
                })
                .map(TupleUtils.function(ResponsePhotosGetAvatarPhotos::new));
    }
}
