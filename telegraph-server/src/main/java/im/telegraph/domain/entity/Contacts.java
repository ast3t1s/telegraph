package im.telegraph.domain.entity;

import org.springframework.lang.Nullable;

import java.io.Serializable;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toMap;

public final class Contacts implements Iterable<Contact>, Serializable {

    private final Map<Id, Contact> relations;

    private static final Contacts EMPTY = new Contacts(Collections.emptyList());

    protected Contacts(Collection<Contact> relations) {
        if (relations.isEmpty()) {
            this.relations = Collections.emptyMap();
        } else {
            this.relations = relations.stream().collect(toMap(Contact::getContactId, Function.identity()));
        }
    }

    public static Contacts of(@Nullable Collection<Contact> relations) {
        if (relations == null || relations.isEmpty()) {
            return EMPTY;
        }
        return new Contacts(relations);
    }

    public static Contacts empty() {
        return EMPTY;
    }

    @Override
    public Iterator<Contact> iterator() {
        return new ReadOnlyIterator<>(this.relations.values().iterator());
    }

    public Contacts addRelation(Contact relation) {
        HashMap<Id, Contact> newRelations = new HashMap<>(this.relations);
        newRelations.put(relation.getContactId(), relation);
        return new Contacts(newRelations.values());
    }

    public Contacts removeRelation(final Id relationId) {
        HashMap<Id, Contact> updated = new HashMap<>(this.relations);
        updated.remove(relationId);
        return new Contacts(updated.values());
    }

    public Stream<Contact> stream() {
        return this.relations.values().stream();
    }

    protected static final class ReadOnlyIterator<T> implements Iterator<T> {

        private final Iterator<T> delegate;

        private ReadOnlyIterator(Iterator<T> delegate) {
            this.delegate = delegate;
        }

        @Override
        public boolean hasNext() {
            return this.delegate.hasNext();
        }

        @Override
        public T next() {
            return this.delegate.next();
        }
    }
}
