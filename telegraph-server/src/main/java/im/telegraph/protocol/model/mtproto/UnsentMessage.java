package im.telegraph.protocol.model.mtproto;

import im.telegraph.engine.serialization.DataInputStream;
import im.telegraph.engine.serialization.DataOutputStream;

import java.io.IOException;

public class UnsentMessage extends ProtoStruct {

    public static final byte HEADER = (byte) 0x07;

    private long messageId;
    private int len;

    public UnsentMessage(long messageId, int len) {
        this.messageId = messageId;
        this.len = len;
    }

    public UnsentMessage(DataInputStream stream) throws IOException {
        super(stream);
    }

    public long getMessageId() {
        return messageId;
    }

    public int getLen() {
        return len;
    }

    @Override
    protected byte getHeader() {
        return HEADER;
    }

    @Override
    protected void writeBody(DataOutputStream outputStream) throws IOException {
        outputStream.writeInt64(messageId);
        outputStream.writeInt32(len);
    }

    @Override
    protected void readBody(DataInputStream inputStream) throws IOException {
        messageId = inputStream.readInt64();
        len = inputStream.readInt();
    }

    @Override
    public String toString() {
        return "UnsentMessage[" + messageId + "]";
    }
} 
