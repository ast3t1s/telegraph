package im.telegraph.api.rpc;

import static im.telegraph.engine.serialization.StreamUtils.*;

import im.telegraph.engine.serialization.StreamReader;
import im.telegraph.engine.serialization.StreamWriter;
import im.telegraph.engine.serialization.io.IOContext;
import im.telegraph.rpc.base.Request;
import java.io.IOException;
import java.lang.Long;
import java.lang.Override;
import java.lang.String;

/**
 * Generated by the Telegraph API Scheme generator. DO NOT EDIT!
 * @author Telegraph Scheme Generator
 */
public class RequestChatsCreateChat extends Request<ResponseStatedMessage> {
    public static final int CLASS_ID = 0xe6;

    protected String title;

    protected String description;

    protected Long settings;

    public RequestChatsCreateChat() {
    }

    public RequestChatsCreateChat(String title, String description, Long settings) {
        this.title = title;
        this.description = description;
        this.settings = settings;
    }

    public static RequestChatsCreateChat fromBytes(byte[] data) throws IOException {
        return IOContext.fromByteArray(new RequestChatsCreateChat(), data);
    }

    @Override
    public int getHeader() {
        return CLASS_ID;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getSettings() {
        return this.settings;
    }

    public void setSettings(Long settings) {
        this.settings = settings;
    }

    @Override
    public void serialize(StreamWriter writer) throws IOException {
        if (this.title == null) {
            throw new IOException();
        }
        writer.writeString(1, this.title);
        if (this.description != null) {
            writer.writeString(2, this.description);
        }
        if (this.settings != null) {
            writer.writeLong(3, this.settings);
        }
    }

    @Override
    public void deserialize(StreamReader reader) throws IOException {
        this.title = reader.readString(1);
        this.description = reader.readOptionalString(2);
        this.settings = reader.readOptionalLong(3);
    }

    @Override
    public String toString() {
        String res = "rpc CreateChat{";
        res += "title=" + this.title;
        res += ", description=" + this.description;
        res += ", settings=" + this.settings;
        res += "}";
        return res;
    }
}
