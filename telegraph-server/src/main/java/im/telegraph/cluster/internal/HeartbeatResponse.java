package im.telegraph.cluster.internal;

import im.telegraph.cluster.ClusterMessage;
import im.telegraph.cluster.UniqueAddress;

/**
 * @author Ast3t1s
 */
public class HeartbeatResponse implements ClusterMessage {

    private static final long serialVersionUID = 1L;

    private final UniqueAddress from;
    private final long seq;
    private final long timestamp;

    public HeartbeatResponse(UniqueAddress from, long seq, long timestamp) {
        this.from = from;
        this.seq = seq;
        this.timestamp = timestamp;
    }

    public UniqueAddress getFrom() {
        return from;
    }

    public long getSeq() {
        return seq;
    }

    public long getTimestamp() {
        return timestamp;
    }

    @Override
    public String toString() {
        return "HeartbeatAck{" +
                "from=" + from +
                ", seq=" + seq +
                ", timestamp=" + timestamp +
                '}';
    }
} 
