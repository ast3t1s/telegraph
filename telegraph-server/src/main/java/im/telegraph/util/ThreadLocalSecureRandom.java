package im.telegraph.util;

import io.netty.util.concurrent.FastThreadLocal;

import java.security.SecureRandom;

public class ThreadLocalSecureRandom extends SecureRandom {

    private static final FastThreadLocal<ThreadLocalSecureRandom> INSTANCE = new FastThreadLocal<>() {
        @Override
        protected ThreadLocalSecureRandom initialValue() {
            return new ThreadLocalSecureRandom();
        }
    };

    public static ThreadLocalSecureRandom current() {
        return INSTANCE.get();
    }
} 
