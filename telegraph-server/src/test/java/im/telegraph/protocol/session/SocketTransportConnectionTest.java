package im.telegraph.protocol.session;

import im.telegraph.server.TransportConnection;
import im.telegraph.protocol.model.transport.Ack;
import im.telegraph.server.TransportFrame;
import im.telegraph.server.reactor.TcpTransportConnection;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import reactor.core.publisher.Mono;
import reactor.netty.Connection;
import reactor.test.StepVerifier;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class SocketTransportConnectionTest {

    @Test
    public void testInitSocketTransportSession() {
        Connection session = mock(Connection.class);

        //when(session.getId()).thenReturn("test-id");

        TransportConnection transportSession = new TcpTransportConnection(session);

       // assertThat(transportSession.getId()).isEqualTo(session.getId());
        assertThat(transportSession.getSentPackets()).isEqualTo(0);
        assertThat(transportSession.getReceivedPackets()).isEqualTo(0);
    }

    @Test
    public void testReceivePacket() {
        Connection session = mock(Connection.class);

        TransportFrame transportPackage = new TransportFrame(1, 2, 3, new byte[] {1, 2, 3}, 31);

        DefaultDataBufferFactory factory = DefaultDataBufferFactory.sharedInstance;

        //when(session.getId()).thenReturn("test-id");
        //when(session.receive()).thenReturn(Flux.just(new SocketPayload(factory.wrap(transportPackage.toByteArray()))));

        TransportConnection transportSession = new TcpTransportConnection(session);
        TransportFrame received = transportSession.receive().blockLast();

        verify(session, times(1)).inbound().receive();

        assertThat(transportSession.getReceivedPackets()).isEqualTo(1);
        assertThat(received.getPackageIndex()).isEqualTo(1);
        assertThat(received.getHeader()).isEqualTo(2);
        assertThat(received.getSize()).isEqualTo(3);
        assertThat(received.getPayload()).isEqualTo(new byte[] {1, 2, 3});
        assertThat(received.getChecksum()).isEqualTo(31);
    }

    @Test
    public void testSendPacket() {
        Connection session = mock(Connection.class);

     //   when(session.getId()).thenReturn("test-id");
       // when(session.send(any())).thenReturn(Mono.empty());

        TransportConnection transportSession = new TcpTransportConnection(session);

        StepVerifier.create(transportSession.send(Mono.just(new Ack(1))))
                .expectSubscription()
                .verifyComplete();

        //verify(session, times(0)).send(any());

        assertThat(transportSession.getSentPackets()).isEqualTo(1);
    }
}