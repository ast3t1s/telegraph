package im.telegraph.admin.object;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Ast3t1s
 */
public class UpdatesBean {

    @JsonProperty("user_id")
    private final Long userId;
    private final int sequence;
    private final Long timestamp;
    private final String update;

    public UpdatesBean(Long userId, int sequence, Long timestamp, String update) {
        this.userId = userId;
        this.sequence = sequence;
        this.timestamp = timestamp;
        this.update = update;
    }

    @Override
    public String toString() {
        return "UpdatesBean{" +
                "  userId=" + userId +
                ", sequence=" + sequence +
                ", timestamp=" + timestamp +
                ", update='" + update + '\'' +
                '}';
    }
}
