package im.telegraph.cluster.internal;

import im.telegraph.cluster.ClusterMessage;
import im.telegraph.cluster.UniqueAddress;

/**
 * @author Ast3t1s
 */
public class GossipSync implements ClusterMessage {

    private static final long serialVersionUID = 1;

    private final UniqueAddress from;
    private final UniqueAddress to;
    private final GossipOverview gossipOverview;

    public GossipSync(UniqueAddress from, UniqueAddress to, GossipOverview gossipOverview) {
        this.from = from;
        this.to = to;
        this.gossipOverview = gossipOverview;
    }

    public UniqueAddress getFrom() {
        return from;
    }

    public UniqueAddress getTo() {
        return to;
    }

    public GossipOverview getGossipOverview() {
        return gossipOverview;
    }

    @Override
    public String toString() {
        return "Sync{" +
                "from=" + from +
                ", gossipOverview=" + gossipOverview +
                '}';
    }
}
