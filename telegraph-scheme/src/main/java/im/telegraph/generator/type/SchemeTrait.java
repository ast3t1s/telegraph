package im.telegraph.generator.type;

public class SchemeTrait implements SchemeRecord {
    private String name;
    private boolean isContainer;

    public SchemeTrait(String name, boolean isContainer) {
        this.name = name;
        this.isContainer = isContainer;
    }

    public String getName() {
        return name;
    }

    public boolean isContainer() {
        return isContainer;
    }
}