package im.telegraph.domain.repository.rdbms;

import im.telegraph.common.rdbms.RdbmsOperations;
import im.telegraph.domain.entity.Id;
import im.telegraph.domain.entity.SequenceUpdate;
import im.telegraph.domain.repository.UpdatesRepository;
import io.r2dbc.spi.Parameters;
import io.r2dbc.spi.R2dbcType;
import io.r2dbc.spi.Row;
import io.r2dbc.spi.RowMetadata;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.function.BiFunction;

/**
 * @author Ast3t1s
 */
@Repository
public class RdbmsUpdatesRepository implements UpdatesRepository {

    private static final String INSERT_UPDATE = "INSERT INTO updates (user_id, seq, timestamp, mapping) " +
            "VALUES (:userId, COALESCE((SELECT seq FROM updates WHERE user_id = :userId ORDER BY seq DESC LIMIT 1), 0) + 1, " +
            ":timestamp, :mapping)";
    private static final String LOAD_LAST_UPDATE = "SELECT user_id, seq, timestamp, mapping FROM updates " +
            "WHERE user_id = :userId ORDER BY seq DESC LIMIT 1";
    private static final String LOAD_UPDATES = "SELECT user_id, seq, timestamp, mapping FROM updates " +
            "WHERE user_id = :userId AND seq > :sequence ORDER BY seq ASC LIMIT :limit";

    private final RdbmsOperations rdbmsOperations;
    private final SequenceUpdateRowMapper updateRowMapper;

    public RdbmsUpdatesRepository(RdbmsOperations rdbmsOperations) {
        this.rdbmsOperations = rdbmsOperations;
        this.updateRowMapper = new SequenceUpdateRowMapper();
    }

    @Override
    public Mono<SequenceUpdate> insert(SequenceUpdate update) {
        return rdbmsOperations.withHandle(handle -> handle.createUpdate(INSERT_UPDATE)
                .bind("userId", update.getUserId().asLong())
                .bind("timestamp", update.getTimestamp().toEpochMilli())
                .bind("mapping", update.getMapping())
                .executeAndReturnGeneratedKeys("*")
                .mapRow(updateRowMapper))
                .next();
    }

    @Override
    public Mono<SequenceUpdate> insert(Id userId, Long timestamp, byte[] update) {
        return rdbmsOperations.withHandle(handle -> handle.createUpdate(INSERT_UPDATE)
                .bind("userId", Parameters.in(R2dbcType.INTEGER, userId.asLong()))
                .bind("timestamp", Parameters.in(R2dbcType.BIGINT, timestamp))
                .bind("mapping", Parameters.in(R2dbcType.BINARY, update))
                .executeAndReturnGeneratedKeys("*")
                .mapRow(updateRowMapper))
                .next();
    }

    @Override
    public Mono<SequenceUpdate> findMaxSeq(Id userId) {
        return rdbmsOperations.withHandle(handle -> handle.createQuery(LOAD_LAST_UPDATE)
                .bind("userId", userId.asLong())
                .mapRow(updateRowMapper))
                .next();
    }

    @Override
    public Flux<SequenceUpdate> findSequenceAfter(Id userId, int sequence, int limit) {
        return rdbmsOperations.withHandle(handle -> handle.createQuery(LOAD_UPDATES)
                .bind("userId", userId.asLong())
                .bind("sequence", sequence)
                .bind("limit", limit)
                .mapRow(updateRowMapper));
    }

    private static class SequenceUpdateRowMapper implements BiFunction<Row, RowMetadata, SequenceUpdate> {

        @Override
        public SequenceUpdate apply(Row row, RowMetadata metadata) {
            final var userId = row.get("user_id", Long.class);
            final var seq = row.get("seq", Integer.class);
            final var timestamp = row.get("timestamp", Long.class);
            final var mapping = row.get("mapping", byte[].class);
            return new SequenceUpdate(Id.of(userId), seq, timestamp, mapping);
        }
    }
}
