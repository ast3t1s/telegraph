package im.telegraph.admin.route;

import com.fasterxml.jackson.databind.ObjectMapper;
import im.telegraph.common.JacksonResources;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufOutputStream;
import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpResponseStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Mono;
import reactor.netty.http.server.HttpServerRequest;
import reactor.netty.http.server.HttpServerResponse;

import java.io.IOException;
import java.io.OutputStream;

/**
 * @author Ast3t1s
 */
public abstract class AbstractRoute implements Routes {

    protected final Logger log = LoggerFactory.getLogger(getClass());

    private static final String JSON_CONTENT_TYPE = "application/json";

    private final ObjectMapper objectMapper = new JacksonResources().getObjectMapper();

    protected <T> Mono<T> readJson(HttpServerRequest request, Class<T> clazz) {
        return request.receive().aggregate().asString()
                .flatMap(content -> Mono.fromCallable(() -> objectMapper.readValue(content, clazz)));
    }

    protected Mono<Void> writeJson(HttpServerResponse response, Object value) {
        ByteBuf buffer = response.alloc().directBuffer();
        OutputStream out = new ByteBufOutputStream(buffer);
        try {
            getObjectMapper().writeValue(out, value);
            return response
                    .header(HttpHeaderNames.CONTENT_TYPE, JSON_CONTENT_TYPE)
                    .send(Mono.just(buffer))
                    .then();
        } catch (IOException ex) {
            log.warn("Error write http response", ex);
            return response.status(HttpResponseStatus.INTERNAL_SERVER_ERROR).send();
        }
    }

    protected ObjectMapper getObjectMapper() {
        return objectMapper;
    }
} 
