package im.telegraph.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

/**
 * @author Ast3t1s
 */
public class JSONMapper {

    private static ObjectMapper mapper;

    static {
        mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        mapper.registerModule(new JavaTimeModule());
    }

    public static <T> T toBean(String json, Class<T> beanClass) {
        T result = null;
        if (json != null) {
            try {
                result = mapper.readValue(json, beanClass);
            } catch (JsonProcessingException e) {
                throw new JsonMapperException("Unable to instantiate Bean " + beanClass.getName(), e);
            }
        }
        return result;
    }

    public static <T> T toCollectionOfBean(String json, TypeReference typeRef)  {
        T result = null;
        if (json != null) {
            try {
                result = (T) mapper.readValue(json, typeRef);
            } catch (JsonProcessingException e) {
                throw new JsonMapperException("Unable to instantiate Bean " + typeRef.getType(), e);
            }
        }
        return result;
    }

    public static String toJson(Object object) {
        String result = null;
        if (object != null) {
            try {
                result = mapper.writeValueAsString(object);
            } catch (JsonProcessingException e) {
                throw new JsonMapperException("Unable to serialize Bean " + object.getClass().getName(), e);
            }
        }
        return result;
    }

    public static class JsonMapperException extends RuntimeException {

        public JsonMapperException(String message, Throwable cause) {
            super(message, cause);
        }
    }
} 
