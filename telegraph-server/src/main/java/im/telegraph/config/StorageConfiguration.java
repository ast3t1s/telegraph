package im.telegraph.config;

import im.telegraph.common.property.file.S3Properties;
import im.telegraph.file.FileStorage;
import im.telegraph.file.s3.S3FileStorage;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;
import org.springframework.util.unit.DataSize;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;
import software.amazon.awssdk.auth.credentials.DefaultCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3AsyncClient;
import software.amazon.awssdk.services.s3.presigner.S3Presigner;
import software.amazon.awssdk.transfer.s3.S3TransferManager;

@Configuration
public class StorageConfiguration {

    private final TelegraphProperties properties;

    public StorageConfiguration(TelegraphProperties properties) {
        this.properties = properties;
    }

    @Configuration(proxyBeanMethods = false)
    @ConditionalOnProperty(prefix = "telegraph.storage.s3", name = "enabled", matchIfMissing = true)
    public static class S3StorageConfiguration {

        private final S3Properties properties;

        public S3StorageConfiguration(TelegraphProperties properties) {
            this.properties = properties.getStorage().getS3();
        }

        @Bean
        @ConditionalOnMissingBean(FileStorage.class)
        public S3FileStorage s3FileStorage(S3AsyncClient client, S3Presigner presigner, S3TransferManager transferManager) {
            return new S3FileStorage(properties.getBucket(), client, presigner, transferManager);
        }

        @Bean
        public S3AsyncClient s3AsyncClient(AwsCredentialsProvider provider) {
            return S3AsyncClient.crtBuilder()
                    .credentialsProvider(provider)
                    .region(Region.of(this.properties.getRegion()))
                    .targetThroughputInGbps((double) DataSize.ofMegabytes(100).toBytes())
                    .minimumPartSizeInBytes(DataSize.ofMegabytes(5).toBytes())
                    .build();
        }

        @Bean
        public S3TransferManager transferManager(S3AsyncClient client) {
            return S3TransferManager.builder().s3Client(client).build();
        }

        @Bean
        public AwsCredentialsProvider awsCredentialsProvider() {
            if (!StringUtils.hasText(this.properties.getAccessKey())) {
                return DefaultCredentialsProvider.create();
            } else {
                return () -> AwsBasicCredentials.create(this.properties.getAccessKey(),
                        this.properties.getSecretKey());
            }
        }

        @Bean
        public S3Presigner s3Presigner(AwsCredentialsProvider provider) {
            return S3Presigner.builder()
                    .credentialsProvider(provider)
                    .region(Region.of(this.properties.getRegion()))
                    .build();
        }
    }
} 
