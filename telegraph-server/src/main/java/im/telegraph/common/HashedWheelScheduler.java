package im.telegraph.common;

import io.netty.util.HashedWheelTimer;
import io.netty.util.Timeout;
import io.netty.util.TimerTask;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.Disposable;
import reactor.core.Scannable;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Scheduler that using {@link io.netty.util.HashedWheelTimer}
 *
 * @author Ast3t1s
 */
public final class HashedWheelScheduler extends AtomicBoolean implements Scheduler {

    private static final Logger log = LoggerFactory.getLogger(HashedWheelScheduler.class);

    private final HashedWheelTimer hashedWheelTimer;
    private final Worker worker;
    private final Scheduler scheduler;

    public HashedWheelScheduler(String name,
                                long tickDuration,
                                int tickPerWheel,
                                int maxPendingTimeouts,
                                boolean daemon) {
        HashedWheelThreadNameFactory threadNameFactory =
                new HashedWheelThreadNameFactory(name, daemon,
                        (t, e) -> log.error("Uncaught exception on thread {}", t.getName(), e));

        this.hashedWheelTimer = new HashedWheelTimer(
                threadNameFactory,
                tickDuration,
                TimeUnit.MILLISECONDS,
                tickPerWheel,
                true,
                maxPendingTimeouts);

        this.worker = new HashedWheelWorker(this.hashedWheelTimer);
        this.scheduler = Schedulers.newSingle("hashed-wheel-offloop-thread", true);
    }

    public static HashedWheelScheduler create(String name, int tickPerWheel, int maxPendingTimeouts, boolean daemon) {
        return new HashedWheelScheduler(name, 100L, tickPerWheel, maxPendingTimeouts, daemon);
    }

    public static HashedWheelScheduler create(String name, int tickPerWheel, boolean daemon) {
        return new HashedWheelScheduler(name, 100L, tickPerWheel, -1, daemon);
    }

    @Override
    public Disposable schedule(Runnable task) {
        return worker.schedule(task);
    }

    @Override
    public Disposable schedule(Runnable task, long delay, TimeUnit unit) {
        if (delay < 0) {
            return schedule(task);
        }
        return worker.schedule(task, delay, unit);
    }

    @Override
    public Disposable schedulePeriodically(Runnable task, long initialDelay, long period, TimeUnit unit) {
        return scheduler.schedulePeriodically(task, initialDelay, period, unit);
    }

    @Override
    public Worker createWorker() {
        return worker;
    }

    @Override
    public void dispose() {
        try {
            worker.dispose();
        } finally {
            scheduler.dispose();
        }
    }

    @Override
    public boolean isDisposed() {
        return worker.isDisposed();
    }

    private static class HashedWheelWorker implements Scheduler.Worker, Scannable {

        private final HashedWheelTimer timer;
        private volatile boolean shutdown;

        HashedWheelWorker(HashedWheelTimer timer) {
            this.timer = timer;
        }

        @Override
        public Object scanUnsafe(Attr key) {
            if (key == Attr.BUFFERED)
                return timer.pendingTimeouts();
            if (key == Attr.TERMINATED || key == Attr.CANCELLED)
                return isDisposed();
            if (key == Attr.NAME)
                return "HashedWheelWorker";
            return null;
        }

        @Override
        public Disposable schedule(Runnable task) {
            ExtendedTimerTask timerTask = new ExtendedTimerTask(task);
            timer.newTimeout(timerTask, 0, TimeUnit.MILLISECONDS);
            return timerTask::cancel;
        }

        @Override
        public Disposable schedule(Runnable task, long delay, TimeUnit unit) {
            ExtendedTimerTask timerTask = new ExtendedTimerTask(task);
            timer.newTimeout(timerTask, delay, unit);
            return timerTask::cancel;
        }

        @Override
        public boolean isDisposed() {
            return shutdown;
        }

        @Override
        public void dispose() {
            if (shutdown) {
                return;
            }
            shutdown = true;
            timer.stop();
        }
    }

    private static class HashedWheelThreadNameFactory extends AtomicInteger implements ThreadFactory {

        private final String prefix;
        private final boolean daemon;
        private final Thread.UncaughtExceptionHandler uncaughtExceptionHandler;

        public HashedWheelThreadNameFactory(String prefix,
                                            boolean daemon,
                                            Thread.UncaughtExceptionHandler uncaughtExceptionHandle) {
            this.prefix = prefix;
            this.daemon = daemon;
            this.uncaughtExceptionHandler = uncaughtExceptionHandle;
        }

        @Override
        public Thread newThread(@NotNull Runnable r) {
            Thread thread = new Thread(r);
            thread.setUncaughtExceptionHandler(uncaughtExceptionHandler);
            thread.setName(prefix + getAndIncrement());
            thread.setDaemon(daemon);
            return thread;
        }
    }

    /**
     * Use our efficient cancel implementation instead of
     * {@link HashedWheelTimer.HashedWheelTimeout#cancel}
     *
     */
    private static class ExtendedTimerTask implements TimerTask {

        private final Runnable task;
        private volatile boolean cancelled;

        public ExtendedTimerTask(Runnable task) {
            this.task = task;
        }

        @Override
        public void run(Timeout t) throws Exception {
            if (!cancelled) {
                task.run();
            }
        }

        public void cancel() {
            cancelled = true;
        }
    }

}
