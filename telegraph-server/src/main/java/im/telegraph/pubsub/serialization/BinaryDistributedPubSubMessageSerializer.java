package im.telegraph.pubsub.serialization;

import im.telegraph.engine.serialization.DataInputStream;
import im.telegraph.engine.serialization.DataOutputStream;
import im.telegraph.engine.serialization.stream.BinaryInputStream;
import im.telegraph.engine.serialization.stream.BinaryOutputStream;
import im.telegraph.internal.remote.Address;
import im.telegraph.internal.remote.serializer.Serialization;
import im.telegraph.internal.remote.serializer.SerializationException;
import im.telegraph.internal.remote.serializer.Serializer;
import im.telegraph.pubsub.internal.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * @author Ast3t1s
 */
public class BinaryDistributedPubSubMessageSerializer implements Serializer {

    private final int BUFFER_SIZE = 1024 * 4;

    private final Serialization payloadSerialization;

    public BinaryDistributedPubSubMessageSerializer(Serialization payloadSerialization) {
        this.payloadSerialization = payloadSerialization;
    }

    @Override
    public int serializerId() {
        return 20;
    }

    @Override
    public byte[] toBinary(Object obj) {
        if (obj instanceof Status) {
            return statusToBinary((Status) obj);
        } else if (obj instanceof Delta) {
            return deltaToBinary((Delta) obj);
        } else if (obj instanceof Send) {
            return sendToBinary((Send) obj);
        } else if (obj instanceof SendToAll) {
            return sendToAllToBinary((SendToAll) obj);
        } else if (obj instanceof Publish) {
            return publishToBinary((Publish) obj);
        }
        throw new IllegalArgumentException("Can't serialize object of type " + obj.getClass());
    }

    @Override
    public Object fromBinary(byte[] array) throws SerializationException {
        try {
            return fromBinary(new BinaryInputStream(array, 0, array.length));
        } catch (IOException e) {
            throw new SerializationException("Unknown error while deserializing binary pubSub message", e);
        }
    }

    private Object fromBinary(BinaryInputStream inputStream) throws IOException {
        final int header = inputStream.readInt();
        return switch (header) {
            case Status.OP -> statusFrom(inputStream);
            case Delta.OP -> deltaFrom(inputStream);
            case Send.OP -> sendFrom(inputStream);
            case SendToAll.OP -> sendToAllFrom(inputStream);
            case Publish.OP -> publishFromBinary(inputStream);
            default -> throw new IOException("Unable to read pubSub message with header #" + header);
        };
    }

    private byte[] compress(byte[] msg) {
        try (final ByteArrayOutputStream outputStream = new ByteArrayOutputStream(BUFFER_SIZE);
             final GZIPOutputStream gzipOutputStream = new GZIPOutputStream(outputStream)) {
            gzipOutputStream.write(msg);
            gzipOutputStream.finish();
            return outputStream.toByteArray();
        } catch (IOException e) {
            throw new IllegalStateException("Error compressing cluster message");
        }
    }

    private byte[] decompress(byte[] msg) {
        try (final var inputStream = new GZIPInputStream(new ByteArrayInputStream(msg))) {
            final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            byte[] buffer = new byte[BUFFER_SIZE];
            int count;
            while ((count = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, count);
            }
            return outputStream.toByteArray();
        } catch (IOException e) {
            throw new IllegalStateException("Error decompressing cluster message", e);
        }
    }

    private byte[] statusToBinary(Status status) {
        DataOutputStream outputStream = new BinaryOutputStream();
        outputStream.writeInt32(status.getOp());
        DataOutputStream payloadStream = new BinaryOutputStream();
        payloadStream.writeInt32(status.getData().getVersions().size());
        for (Status.Data.Inner inner : status.getData().getVersions()) {
            payloadStream.writeProtoBytes(addressToBinary(inner.getAddress()));
            payloadStream.writeInt64(inner.getTimestamp());
        }
        payloadStream.writeByte(status.getData().isReplyToStatus() ? 1 : 0);
        outputStream.writeProtoBytes(compress(payloadStream.toByteArray()));
        return outputStream.toByteArray();
    }

    private Status statusFrom(DataInputStream inputStream) throws IOException {
        BinaryInputStream payloadStream = new BinaryInputStream(decompress(inputStream.readProtoBytes()));
        final int versionsSize = payloadStream.readInt();
        Map<Address, Long> versions = new HashMap<>(versionsSize);
        for (int i = 0; i < versionsSize; i++) {
            final Address address = addressFrom(payloadStream.readProtoBytes());
            final long version = payloadStream.readInt64();
            versions.put(address, version);
        }
        return new Status(versions, payloadStream.readByte() == 1);
    }

    private byte[] deltaToBinary(Delta delta) {
        DataOutputStream outputStream = new BinaryOutputStream();
        outputStream.writeInt32(delta.getOp());
        DataOutputStream payloadStream = new BinaryOutputStream();
        payloadStream.writeInt32(delta.getData().getBuckets().size());
        for (Bucket bucket : delta.getData().getBuckets()) {
            payloadStream.writeProtoBytes(bucketToBinary(bucket));
        }
        outputStream.writeProtoBytes(compress(payloadStream.toByteArray()));
        return outputStream.toByteArray();
    }

    private Delta deltaFrom(DataInputStream inputStream) throws IOException {
        DataInputStream payloadStream = new BinaryInputStream(decompress(inputStream.readProtoBytes()));
        final int bucketSize = payloadStream.readInt();
        final List<Bucket> buckets = new ArrayList<>(bucketSize);
        for (int i = 0; i < bucketSize; i++) {
            buckets.add(deserializeBucket(payloadStream.readProtoBytes()));
        }
        return new Delta(buckets);
    }

    private byte[] sendToBinary(Send send) {
        DataOutputStream outputStream = new BinaryOutputStream();
        outputStream.writeInt32(send.getOp());
        outputStream.writeProtoString(send.getData().getTopic());
        Serializer serializer = payloadSerialization.findSerializerFor(send.getData().getPayload().getClass());
        outputStream.writeInt32(serializer.serializerId());
        outputStream.writeProtoBytes(serializer.toBinary(send.getData().getPayload()));
        return outputStream.toByteArray();
    }

    private Send sendFrom(DataInputStream inputStream) throws IOException {
        final String topic = inputStream.readProtoString();
        final int serializerId = inputStream.readInt();
        final Object payload = payloadSerialization.deserialize(inputStream.readProtoBytes(), serializerId);
        return new Send(topic, payload);
    }

    private byte[] sendToAllToBinary(SendToAll sendToAll) {
        DataOutputStream outputStream = new BinaryOutputStream();
        outputStream.writeInt32(sendToAll.getOp());
        outputStream.writeProtoString(sendToAll.getData().getTopic());
        Serializer serializer = payloadSerialization.findSerializerFor(sendToAll.getData().getPayload().getClass());
        outputStream.writeInt32(serializer.serializerId());
        outputStream.writeProtoBytes(serializer.toBinary(sendToAll.getData().getPayload()));
        outputStream.writeByte(sendToAll.getData().isExcludeSelf() ? 1 : 0);
        return outputStream.toByteArray();
    }

    private SendToAll sendToAllFrom(DataInputStream inputStream) throws IOException {
        final String topic = inputStream.readProtoString();
        final int serializerId = inputStream.readInt();
        final byte[] payloadData = inputStream.readProtoBytes();
        final Object payload = payloadSerialization.deserialize(payloadData, serializerId);
        return new SendToAll(topic, payload, inputStream.readByte() == 1);
    }

    private byte[] publishToBinary(Publish publish) {
        DataOutputStream outputStream = new BinaryOutputStream();
        outputStream.writeInt32(publish.getOp());
        outputStream.writeProtoString(publish.getData().getTopic());
        Serializer serializer = payloadSerialization.findSerializerFor(publish.getData().getPayload().getClass());
        outputStream.writeInt32(serializer.serializerId());
        outputStream.writeProtoBytes(serializer.toBinary(publish.getData().getPayload()));
        return outputStream.toByteArray();
    }

    private Publish publishFromBinary(DataInputStream inputStream) throws IOException {
        final String topic = inputStream.readProtoString();
        final int serializerId = inputStream.readInt();
        final Object payload = payloadSerialization.deserialize(inputStream.readProtoBytes(), serializerId);
        return new Publish(topic, payload);
    }

    private byte[] addressToBinary(Address address) {
        DataOutputStream outputStream = new BinaryOutputStream();
        outputStream.writeProtoString(address.toString());
        return outputStream.toByteArray();
    }

    private Address addressFrom(byte[] data) throws IOException {
        DataInputStream inputStream = new BinaryInputStream(data);
        return Address.create(inputStream.readProtoString());
    }

    private byte[] bucketToBinary(Bucket bucket) {
        DataOutputStream outputStream = new BinaryOutputStream();
        outputStream.writeProtoBytes(addressToBinary(bucket.getAddress()));
        outputStream.writeInt64(bucket.getVersion());
        outputStream.writeInt32(bucket.getContent().size());
        bucket.getContent().forEach((topic, holder) -> {
            outputStream.writeProtoString(topic);
            outputStream.writeProtoBytes(valueHolderToBinary(holder));
        });
        return outputStream.toByteArray();
    }

    private Bucket deserializeBucket(byte[] array) throws IOException {
        final BinaryInputStream inputStream = new BinaryInputStream(array);
        final byte[] addressData = inputStream.readProtoBytes();
        final long version = inputStream.readInt64();
        final int contentSize = inputStream.readInt();
        final Map<String, ValueHolder> content = new HashMap<>(contentSize);
        for (int i = 0; i < contentSize; i++) {
            content.put(inputStream.readProtoString(), deserializeValueHolder(inputStream.readProtoBytes()));
        }
        return new Bucket(addressFrom(addressData), version, content);
    }

    private byte[] valueHolderToBinary(ValueHolder valueHolder) {
        DataOutputStream outputStream = new BinaryOutputStream();
        outputStream.writeInt64(valueHolder.getVersion());
        outputStream.writeProtoString(valueHolder.getDestination());
        return outputStream.toByteArray();
    }

    private ValueHolder deserializeValueHolder(byte[] array) throws IOException {
        BinaryInputStream inputStream = new BinaryInputStream(array);
        return new ValueHolder(inputStream.readInt64(), inputStream.readProtoString());
    }
} 
