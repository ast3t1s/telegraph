package im.telegraph.domain.entity;

import lombok.Data;
import org.springframework.lang.Nullable;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import java.util.Optional;

/**
 * Entity which stores authorization session info.
 *
 * @author Vsevolod Bondarenko
 */
@Data
public class AuthSession implements Serializable {

    public static final String ENTITY_NAME = AuthSession.class.getSimpleName();

    private static final long serialVersionUID = 1L;

    private final Id id;
    private final Id userId;
    private final Long authId;
    private final String deviceModel;
    private final String platform;
    private final String systemVersion;
    private final String appName;
    private final String appVersion;
    @Nullable
    private final String country;
    @Nullable
    private final String region;
    @Nullable
    private final String ip;
    private final Instant createdAt;
    @Nullable
    private final Instant deletedAt;

    /**
     * Contains information about session used by the current user.
     *
     * @param id            Session identifier.
     * @param userId        User identifier associated with the session.
     * @param deviceModel   Model of the device the application has been run or is running on, as provided by application.
     * @param createdAt     Date (Unix Timestamp) when the user has logged in.
     * @param country       A two - letter country code for the country from which the session was created, based on the IP address.
     * @param ip            IP address from which the session was created, in human - readable format.
     * @param authId        Authorization key identifier bound to the session.
     * @param platform      Operating system the application has been run or is running on, as provided by the application.
     * @param systemVersion Version of the operation system the application has been run or is running on, as provided by the application.
     * @param appName       Name of the application, as provided by the application.
     * @param appVersion    The version of the application, as provided by the application.
     * @param region        Region code from which the session was created, based on the IP address.
     * @param deletedAt     Date (Unix Timestamp) when user has been deleted.
     */
    @lombok.Builder(builderClassName = "Builder", toBuilder = true)
    AuthSession(Id id, Id userId, String deviceModel, Instant createdAt, @Nullable String country,
                @Nullable String ip, Long authId, String platform, String systemVersion, String appName,
                String appVersion, @Nullable String region, @Nullable Instant deletedAt) {
        this.id = id;
        this.userId = userId;
        this.deviceModel = deviceModel;
        this.createdAt = createdAt;
        this.country = country;
        this.ip = ip;
        this.authId = authId;
        this.platform = platform;
        this.systemVersion = systemVersion;
        this.appName = appName;
        this.appVersion = appVersion;
        this.region = region;
        this.deletedAt = deletedAt;
    }

    public Optional<Instant> getDeletedAt() {
        return Optional.ofNullable(deletedAt);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AuthSession that = (AuthSession) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    @Override
    public String toString() {
        return "AuthSession{" +
                "id=" + id +
                ", authId=" + authId +
                ", userId=" + userId +
                ", deviceModel='" + deviceModel + '\'' +
                ", platform='" + platform + '\'' +
                ", systemVersion='" + systemVersion + '\'' +
                ", appName='" + appName + '\'' +
                ", appVersion='" + appVersion + '\'' +
                ", country='" + country + '\'' +
                ", region='" + region + '\'' +
                ", ip='" + ip + '\'' +
                ", createdAt=" + createdAt +
                ", deletedAt=" + deletedAt +
                '}';
    }
}
