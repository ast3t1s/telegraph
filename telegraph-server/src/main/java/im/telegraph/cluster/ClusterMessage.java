package im.telegraph.cluster;

import java.io.Serializable;

/**
 * Marker interface for cluster messages.
 *
 * @author Ast3t1s
 */
public interface ClusterMessage extends Serializable {

}
