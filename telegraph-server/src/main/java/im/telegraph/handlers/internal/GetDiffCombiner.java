package im.telegraph.handlers.internal;

import im.telegraph.api.ApiDifferenceUpdate;
import im.telegraph.api.update.*;
import im.telegraph.domain.entity.SequenceUpdate;
import im.telegraph.rpc.base.Update;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public final class GetDiffCombiner {

    /**
     * Extract users, chats from the given {@code Update}
     *
     * @param update the update.
     * @return {@link CombinedDifference instance}.
     */
    public static CombinedDifference buildDiff(final Update update) {
        return buildDiff(Collections.singletonList(update));
    }

    /**
     * Extract users and chats from the given {@link List<Update> updates}.
     *
     * @param updates the list of updates.
     * @return {@link CombinedDifference instance}.
     */
    public static CombinedDifference buildDiff(final List<Update> updates) {
        final CombinedDifference.Builder builder = new CombinedDifference.Builder();
        for (final Update update : updates) {
            if (update instanceof UpdateUserName) {
                builder.user(((UpdateUserName) update).getUserId());
            } else if (update instanceof UpdateUser) {
                builder.user(((UpdateUser) update).getUserId());
            } else if (update instanceof UpdateChat) {
                builder.chat(((UpdateChat) update).getChatId());
            } else if (update instanceof UpdateChatParticipantAdd) {
                builder.chat(((UpdateChatParticipantAdd) update).getChatId());
                builder.user(((UpdateChatParticipantAdd) update).getUid());
            }
        }
        return builder.build();
    }

    /**
     * Extract users, chats and updates from the given {@link List<SequenceUpdate> updates}.
     *
     * @param authId the authorization identifier.
     * @param updates the list of sequence updates.
     * @return {@link CombinedDifference instance}.
     */
    public static CombinedDifference buildSequenceDiff(final Long authId, final List<SequenceUpdate> updates) {
        final CombinedDifference.Builder builder = new CombinedDifference.Builder();
        for (final SequenceUpdate seqUpdate : updates) {
            final SerialUpdate update = getSerialUpdate(seqUpdate.getMapping()).getCustomOrDefault(authId);

            Optional.ofNullable(update.getUsers())
                    .ifPresent(users -> users.forEach(builder::user));

            Optional.ofNullable(update.getGroups())
                    .ifPresent(chats -> chats.forEach(builder::chat));

            builder.update(new ApiDifferenceUpdate(update.getHeader(), update.getBody()));
        }
        return builder.build();
    }

    public static SerialMapping getSerialUpdate(byte[] source) {
        try {
            return SerialMapping.fromBytes(source);
        } catch (IOException e) {
            return new SerialMapping();
        }
    }
} 
