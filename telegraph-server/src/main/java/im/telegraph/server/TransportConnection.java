package im.telegraph.server;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.net.SocketAddress;
import java.time.Instant;

/**
 * This class represent Transport level of MTProto protocol {@link TransportFrame}
 *
 * @author Vsevolod Bondarenko
 */
public interface TransportConnection {

    /**
     * Obtain the id of the context.
     *
     * @return The id of the context.
     */
    String getId();

    /**
     * Return the current connection host address.
     *
     * @return the host address
     */
    SocketAddress remoteAddress();

    /**
     * Obtain the number of packets sent from server to the client.
     *
     * @return The number of packets sent from server to the client.
     */
    int getSentPackets();

    /**
     * Obtain the number of packets sent from client to the server.
     *
     * @return The number of packets sent from client to the server.
     */
    int getReceivedPackets();

    /**
     * Obtain the date the session was created.
     *
     * @return the session's creation time.
     */
    Instant createdDate();

    /**
     * Return the last time of session access as a result of user activity such
     * as request.
     */
    Instant getLastAccessTime();

    /**
     * Provides access to the stream of inbound messages.
     */
    Flux<TransportFrame> receive();

    /**
     * Give a source of outgoing messages, write the messages and return a
     * {@code Mono<Void>} that completes when the source completes and writing is done.
     */
    Mono<Void> send(Publisher<MTProtoObject> messages);

    /**
     * Whether the underlying connection is open.
     */
    boolean isOpen();

    /**
     * Returns a {@link Mono} that terminates when the instance is terminated by any reason. Note, in
     * case of error termination, the cause of error will be propagated as an error signal through
     * {@link org.reactivestreams.Subscriber#onError(Throwable)}. Otherwise, {@link
     * Subscriber#onComplete()} will be called.
     *
     * @return a {@link Mono} to track completion with success or error of the underlying resource.
     */
    Mono<Void> onClose();

    /**
     * Close the Socket session.
     */
    Mono<Void> close();

}
