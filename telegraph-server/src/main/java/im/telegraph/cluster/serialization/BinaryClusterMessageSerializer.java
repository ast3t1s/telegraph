package im.telegraph.cluster.serialization;

import im.telegraph.cluster.*;
import im.telegraph.cluster.internal.*;
import im.telegraph.engine.serialization.stream.BinaryInputStream;
import im.telegraph.engine.serialization.stream.BinaryOutputStream;
import im.telegraph.internal.remote.Address;
import im.telegraph.internal.remote.serializer.SerializationException;
import im.telegraph.internal.remote.serializer.Serializer;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * @author Ast3t1s
 */
public class BinaryClusterMessageSerializer implements Serializer {

    private static final int BUFFER_SIZE = 4 * 1024;

    private static final int WELCOME_HEADER = 0x01;
    private static final int JOIN_HEADER = 0x02;
    private static final int HEARTBEAT_HEADER = 0x03;
    private static final int HEARTBEAT_RSP_HEADER = 0x04;
    private static final int GOSSIP_SYNC_HEADER = 0x05;
    private static final int GOSSIP_SYNC_ACK_HEADER = 0x06;
    private static final int GOSSIP_ENVELOPE_HEADER = 0x07;

    @Override
    public byte[] toBinary(Object object) throws SerializationException {
        if (object instanceof Welcome) {
            return welcomeToBinary((Welcome) object);
        } else if (object instanceof Join) {
            return joinToBinary((Join) object);
        } else if (object instanceof Heartbeat) {
            return heartbeatToBinary((Heartbeat) object);
        } else if (object instanceof HeartbeatResponse) {
            return heartbeatResponseToBinary((HeartbeatResponse) object);
        } else if (object instanceof GossipSync) {
            return gossipSyncToBinary((GossipSync) object);
        } else if (object instanceof GossipSyncAck) {
            return gossipSyncAckToBinary((GossipSyncAck) object);
        } else if (object instanceof GossipEnvelope) {
            return gossipEnvelopeToBinary((GossipEnvelope) object);
        } else {
            throw new IllegalArgumentException("Can't serialize object of type " + object.getClass());
        }
    }

    @Override
    public Object fromBinary(byte[] array) throws SerializationException {
        try {
            return deserializeClusterMessage(new BinaryInputStream(array, 0, array.length));
        } catch (IOException e) {
            throw new SerializationException("Unknown error while deserializing binary cluster message", e);
        }
    }

    @Override
    public int serializerId() {
        return 10;
    }

    private ClusterMessage deserializeClusterMessage(BinaryInputStream inputStream) throws IOException {
        final int header = inputStream.readByte();
        return switch (header) {
            case WELCOME_HEADER -> deserializeWelcome(inputStream);
            case JOIN_HEADER -> deserializeJoin(inputStream);
            case HEARTBEAT_HEADER -> deserializeHeartbeat(inputStream);
            case HEARTBEAT_RSP_HEADER -> deserializeHeartbeatResponse(inputStream);
            case GOSSIP_SYNC_HEADER -> deserializeGossipSync(inputStream);
            case GOSSIP_SYNC_ACK_HEADER -> deserializeGossipSyncAck(inputStream);
            case GOSSIP_ENVELOPE_HEADER -> deserializeGossipEnvelope(inputStream);
            default -> throw new IOException("Unable to read cluster message with header #" + header);
        };
    }

    private byte[] compress(byte[] msg) {
        try (final ByteArrayOutputStream outputStream = new ByteArrayOutputStream(BUFFER_SIZE);
             final GZIPOutputStream gzipOutputStream = new GZIPOutputStream(outputStream)) {
            gzipOutputStream.write(msg);
            gzipOutputStream.finish();
            return outputStream.toByteArray();
        } catch (IOException e) {
            throw new IllegalStateException("Error compressing cluster message");
        }
    }

    private byte[] decompress(byte[] msg) {
        try (final var inputStream = new GZIPInputStream(new ByteArrayInputStream(msg))) {
            final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            byte[] buffer = new byte[BUFFER_SIZE];
            int count;
            while ((count = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, count);
            }
            return outputStream.toByteArray();
        } catch (IOException e) {
            throw new IllegalStateException("Error decompressing cluster message");
        }
    }

    private GossipEnvelope deserializeGossipEnvelope(BinaryInputStream inputStream) throws IOException {
        final byte[] address = inputStream.readProtoBytes();
        final int gossipSize = inputStream.readInt();
        List<Gossip> gossips = new ArrayList<>(gossipSize);
        for (int i = 0; i < gossipSize; i++) {
            gossips.add(deserializeGossip(inputStream.readProtoBytes()));
        }
        return new GossipEnvelope(deserializeAddress(address), gossips);
    }

    private Welcome deserializeWelcome(BinaryInputStream inputStream) throws IOException {
        final byte[] addressData = inputStream.readProtoBytes();
        final byte[] overviewData = inputStream.readProtoBytes();
        return new Welcome(deserializeAddress(addressData), deserializeGossipOverview(overviewData));
    }

    private Join deserializeJoin(BinaryInputStream inputStream) throws IOException {
        final byte[] addressData = inputStream.readProtoBytes();
        final byte[] overviewData = inputStream.readProtoBytes();
        UniqueAddress address =  deserializeAddress(addressData);
        GossipOverview overview = deserializeGossipOverview(overviewData);
        return new Join(address, overview);
    }

    private Heartbeat deserializeHeartbeat(BinaryInputStream inputStream) throws IOException {
        final String addressData = inputStream.readProtoString();
        final long seq = inputStream.readInt64();
        final long timestamp = inputStream.readInt64();
        return new Heartbeat(Address.create(addressData), seq, timestamp);
    }

    private HeartbeatResponse deserializeHeartbeatResponse(BinaryInputStream inputStream) throws IOException {
        final byte[] addressData = inputStream.readProtoBytes();
        final long seq = inputStream.readInt64();
        final long timestamp = inputStream.readInt64();
        return new HeartbeatResponse(deserializeAddress(addressData), seq, timestamp);
    }

    private GossipSync deserializeGossipSync(BinaryInputStream inputStream) throws IOException {
        final byte[] fromData = inputStream.readProtoBytes();
        final byte[] toData = inputStream.readProtoBytes();
        final byte[] overviewData = inputStream.readProtoBytes();
        return new GossipSync(deserializeAddress(fromData), deserializeAddress(toData), deserializeGossipOverview(overviewData));
    }

    private GossipSyncAck deserializeGossipSyncAck(BinaryInputStream inputStream) throws IOException {
        final byte[] fromData = inputStream.readProtoBytes();
        final byte[] overviewData = inputStream.readProtoBytes();
        return new GossipSyncAck(deserializeAddress(fromData), deserializeGossipOverview(overviewData));
    }

    private byte[] gossipEnvelopeToBinary(GossipEnvelope envelope) {
        BinaryOutputStream outputStream = new BinaryOutputStream();
        outputStream.writeByte(GOSSIP_ENVELOPE_HEADER);
        outputStream.writeProtoBytes(uniqueAddressToBinary(envelope.getFrom()));
        final int gossipSize = envelope.getGossips().size();
        outputStream.writeInt32(gossipSize);
        for (Gossip gossip : envelope.getGossips()) {
            outputStream.writeProtoBytes(gossipToBinary(gossip));
        }
        return outputStream.toByteArray();
    }

    private byte[] gossipSyncToBinary(GossipSync gossipSync) {
        BinaryOutputStream outputStream = new BinaryOutputStream();
        outputStream.writeByte(GOSSIP_SYNC_HEADER);
        outputStream.writeProtoBytes(uniqueAddressToBinary(gossipSync.getFrom()));
        outputStream.writeProtoBytes(uniqueAddressToBinary(gossipSync.getTo()));
        outputStream.writeProtoBytes(gossipOverviewToBinary(gossipSync.getGossipOverview()));
        return outputStream.toByteArray();
    }

    private byte[] gossipSyncAckToBinary(GossipSyncAck gossipSyncAck) {
        BinaryOutputStream outputStream = new BinaryOutputStream();
        outputStream.writeByte(GOSSIP_SYNC_ACK_HEADER);
        outputStream.writeProtoBytes(uniqueAddressToBinary(gossipSyncAck.getFrom()));
        outputStream.writeProtoBytes(gossipOverviewToBinary(gossipSyncAck.getGossipOverview()));
        return outputStream.toByteArray();
    }

    private byte[] heartbeatToBinary(Heartbeat heartbeat) {
        BinaryOutputStream outputStream = new BinaryOutputStream();
        outputStream.writeByte(HEARTBEAT_HEADER);
        outputStream.writeProtoString(heartbeat.getFrom().toString());
        outputStream.writeInt64(heartbeat.getSeq());
        outputStream.writeInt64(heartbeat.getTimestamp());
        return outputStream.toByteArray();
    }

    private byte[] heartbeatResponseToBinary(HeartbeatResponse heartbeatResponse) {
        BinaryOutputStream outputStream = new BinaryOutputStream();
        outputStream.writeByte(HEARTBEAT_RSP_HEADER);
        outputStream.writeProtoBytes(uniqueAddressToBinary(heartbeatResponse.getFrom()));
        outputStream.writeInt64(heartbeatResponse.getSeq());
        outputStream.writeInt64(heartbeatResponse.getTimestamp());
        return outputStream.toByteArray();
    }

    private byte[] joinToBinary(Join join) {
        BinaryOutputStream outputStream = new BinaryOutputStream();
        outputStream.writeByte(JOIN_HEADER);
        outputStream.writeProtoBytes(uniqueAddressToBinary(join.getFrom()));
        outputStream.writeProtoBytes(gossipOverviewToBinary(join.getGossipOverview()));
        return outputStream.toByteArray();
    }

    private byte[] welcomeToBinary(Welcome welcome) {
        BinaryOutputStream outputStream = new BinaryOutputStream();
        outputStream.writeByte(WELCOME_HEADER);
        outputStream.writeProtoBytes(uniqueAddressToBinary(welcome.getFrom()));
        outputStream.writeProtoBytes(gossipOverviewToBinary(welcome.getGossipOverview()));
        return outputStream.toByteArray();
    }

    private byte[] gossipOverviewToBinary(GossipOverview overview) {
        BinaryOutputStream outputStream = new BinaryOutputStream();
        final int membershipSize = overview.getMembership().size();
        outputStream.writeInt32(membershipSize);
        for (MembershipState state : overview.getMembership()) {
            outputStream.writeProtoBytes(membershipStateToBinary(state));
        }
        return compress(outputStream.toByteArray());
    }

    private byte[] gossipToBinary(Gossip gossip) {
        BinaryOutputStream outputStream = new BinaryOutputStream();
        outputStream.writeProtoString(gossip.gossipId());
        outputStream.writeProtoBytes(gossipOverviewToBinary(gossip.overview()));
        return outputStream.toByteArray();
    }

    private byte[] membershipStateToBinary(MembershipState state) {
        BinaryOutputStream outputStream = new BinaryOutputStream();
        outputStream.writeProtoBytes(memberToBinary(state.member()));
        outputStream.writeByte(memberStatusToInt(state.status()));
        outputStream.writeInt32(state.incarnation());
        return outputStream.toByteArray();
    }

    private byte[] uniqueAddressToBinary(UniqueAddress address) {
        BinaryOutputStream outputStream = new BinaryOutputStream();
        outputStream.writeProtoString(address.getAddress().toString());
        outputStream.writeProtoString(address.getId());
        return outputStream.toByteArray();
    }

    private byte memberStatusToInt(MembershipState.MemberStatus status) {
        return switch (status) {
            case SUSPECT -> (byte) 1;
            case ALIVE -> (byte) 2;
            case DEAD -> (byte) 3;
            case LEAVING -> (byte) 4;
        };
    }

    private byte[] memberToBinary(Member member) {
        BinaryOutputStream outputStream = new BinaryOutputStream();
        outputStream.writeProtoBytes(uniqueAddressToBinary(member.getUniqueAddress()));
        outputStream.writeProtoString(member.getVersion().toString());
        final int rolesSize = member.getRoles().size();
        outputStream.writeInt64(rolesSize);
        for (String role : member.getRoles()) {
            outputStream.writeProtoString(role);
        }
        return outputStream.toByteArray();
    }

    private Member deserializeMember(byte[] source) throws IOException {
        BinaryInputStream inputStream = new BinaryInputStream(source);
        final byte[] address = inputStream.readProtoBytes();
        final String version = inputStream.readProtoString();
        final int rolesSize = inputStream.readInt();
        final Set<String> roles = new HashSet<>();
        for (int i = 0; i < rolesSize; i++) {
            roles.add(inputStream.readProtoString());
        }
        final UniqueAddress uniqueAddress = deserializeAddress(address);
        return Member.builder()
                .id(uniqueAddress.getId())
                .address(uniqueAddress.getAddress())
                .version(Version.from(version))
                .roles(roles)
                .build();
    }

    private MembershipState deserializeMembershipState(byte[] source) throws IOException {
        BinaryInputStream inputStream = new BinaryInputStream(source);
        final byte[] memberData = inputStream.readProtoBytes();
        final int memberStatus = inputStream.readByte();
        final int incarnation = inputStream.readInt();

        return new MembershipState(deserializeMember(memberData), deserializeStatus(memberStatus), incarnation);
    }

    private GossipOverview deserializeGossipOverview(byte[] source) throws IOException {
        BinaryInputStream inputStream = new BinaryInputStream(decompress(source));
        final int membershipSize = inputStream.readInt();

        final List<MembershipState> membershipStates = new ArrayList<>();
        for (int i = 0; i < membershipSize; i++) {
            membershipStates.add(deserializeMembershipState(inputStream.readProtoBytes()));
        }

        return new GossipOverview(membershipStates);
    }

    private Gossip deserializeGossip(byte[] source) throws IOException {
        BinaryInputStream inputStream = new BinaryInputStream(source);
        return new Gossip(inputStream.readProtoString(), deserializeGossipOverview(inputStream.readProtoBytes()));
    }

    private MembershipState.MemberStatus deserializeStatus(int value) {
        return switch (value) {
            case (byte) 1 -> MembershipState.MemberStatus.SUSPECT;
            case (byte) 2 -> MembershipState.MemberStatus.ALIVE;
            case (byte) 3 -> MembershipState.MemberStatus.DEAD;
            case (byte) 4 -> MembershipState.MemberStatus.LEAVING;
            default -> throw new IllegalArgumentException("Unknown member status");
        };
    }

    private UniqueAddress deserializeAddress(byte[] source) throws IOException {
        final BinaryInputStream inputStream = new BinaryInputStream(source);
        return new UniqueAddress(Address.create(inputStream.readProtoString()), inputStream.readProtoString());
    }
}
