package im.telegraph.handlers.dispatch;

import im.telegraph.rpc.base.Request;
import im.telegraph.rpc.base.Response;
import reactor.core.publisher.Mono;

/**
 * Functional interface for dispatching requests and handling the logic of invoking a request based on incoming data.
 *
 * @author Ast3t1s
 */
@FunctionalInterface
public interface DispatchServiceMapper {

    /**
     * Dispatches a request and handles the logic of invoking it based on the provided dispatch context.
     *
     * @param context the DispatchContext used with this Dispatch object.
     * @param <T> the Request type.
     * @param <R> the Response type.
     * @return a {@link Mono} of {@link Response} mapped from the given {@link DispatchContext} object, or empty if no
     * Response is produced. If an error occurs during processing, it is emitted through the {@code Mono}.
     */
    <T extends Request<? extends Response>, R extends Response> Mono<R> dispatch(DispatchContext<T> context);

    /**
     * Create a {@link DispatchServiceMapper} that doesn't process any dispatches
     *
     * @return a {@link DispatchServiceMapper} that does nothing
     */
    static DispatchServiceMapper noOp() {
        return new DispatchServiceMapper() {
            @Override
            public <T extends Request<? extends Response>, R extends Response> Mono<R> dispatch(DispatchContext<T> context) {
                return Mono.empty();
            }
        };
    }
}
