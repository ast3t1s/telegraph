package im.telegraph.common.property.cluster;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.boot.convert.DurationUnit;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

/**
 * @author Ast3t1s
 */
@Getter
@Setter
@Accessors(chain = true)
public class FailureDetectorProperties {

    /**
     * How often keep-alive heartbeat messages should be sent to each connection.
     */
    @DurationUnit(value = ChronoUnit.SECONDS)
    private Duration heartbeatInterval = Duration.ofSeconds(5);

    /**
     * How long wait for heartbeat ack message.
     */
    @DurationUnit(value = ChronoUnit.MILLIS)
    private Duration heartbeatTimeout = Duration.ofMillis(3_000);

    /**
     * Defines the failure detector threshold. A low threshold is prone to generate many wrong suspicions but ensures
     * a quick detection in the event of a real crash. Conversely, a high threshold generates fewer mistakes
     * but needs more time to detect actual crashes.
     */
    private double threshold = 10.0;

    /**
     * Number of the samples of inter-heartbeat arrival times to adaptively
     * calculate the failure timeout for connections.
     */
    private int maxSampleSize = 200;

    /**
     * Minimum standard deviation to use for the normal distribution in
     * FailureDetector. Too low standard deviation might result in
     * too much sensitivity for sudden, but normal, deviations in heartbeat
     * inter arrival times.
     */
    @DurationUnit(value = ChronoUnit.MILLIS)
    private Duration minStdDeviation = Duration.ofMillis(200);

    /**
     * Number of potentially lost/delayed heartbeats that will be
     * accepted before considering it to be an anomaly.
     * This margin is important to be able to survive sudden, occasional,
     * pauses in heartbeat arrivals, due to for example garbage collect or
     * network drop.
     */
    @DurationUnit(value = ChronoUnit.SECONDS)
    private Duration acceptableHeartbeatPause = Duration.ofSeconds(10);

    /**
     * How often to check for nodes marked as unreachable by the failure detector.
     */
    @DurationUnit(value = ChronoUnit.SECONDS)
    private Duration unreachableNodesReaperInterval = Duration.ofSeconds(10);

    /**
     * Number of member nodes that each member will send heartbeat messages to,
     * i.e. each node will be monitored by this number of other nodes.
     */
    private Integer monitoredMembers = 5;

}
