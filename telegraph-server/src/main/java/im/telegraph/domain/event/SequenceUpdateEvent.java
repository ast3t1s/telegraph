package im.telegraph.domain.event;

import im.telegraph.domain.entity.Id;
import reactor.util.annotation.Nullable;

import java.time.Instant;
import java.util.Set;

/**
 * @author Ast3t1s
 */
public class SequenceUpdateEvent implements Event {

    private final Id userId;
    private final int seq;
    private final Instant timestamp;
    @Nullable
    private final Set<Long> excluded;
    private final String payload;

    public SequenceUpdateEvent(Id userId, int seq, Instant timestamp, @Nullable Set<Long> excluded, String payload) {
        this.userId = userId;
        this.seq = seq;
        this.timestamp = timestamp;
        this.excluded = excluded;
        this.payload = payload;
    }

    public Id getUserId() {
        return userId;
    }

    public int getSeq() {
        return seq;
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    @Nullable
    public Set<Long> getExcluded() {
        return excluded;
    }

    public String getPayload() {
        return payload;
    }

    @Override
    public String toString() {
        return "SequenceUpdateEvent{" +
                "userId=" + userId +
                ", seq=" + seq +
                ", timestamp=" + timestamp +
                ", excluded=" + excluded +
                ", payload='" + payload + '\'' +
                '}';
    }
}
