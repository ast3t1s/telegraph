package im.telegraph.handlers;

import im.telegraph.exceptions.RpcException;
import im.telegraph.handlers.dispatch.DispatchContext;
import im.telegraph.handlers.dispatch.DispatchServiceMapper;
import im.telegraph.protocol.MTProtoException;
import im.telegraph.protocol.MTProtoSession;
import im.telegraph.protocol.model.mtproto.MTRpcResponse;
import im.telegraph.protocol.model.mtproto.ProtoStruct;
import im.telegraph.protocol.model.mtproto.rpc.RpcError;
import im.telegraph.protocol.model.mtproto.rpc.RpcInternalError;
import im.telegraph.protocol.model.mtproto.rpc.RpcOk;
import im.telegraph.rpc.base.Request;
import im.telegraph.rpc.base.Response;
import org.reactivestreams.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.net.InetSocketAddress;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @author Ast3t1s
 */
@Component
public class RpcRequestDispatcher {

    private static final Logger log = LoggerFactory.getLogger(RpcRequestDispatcher.class);

    private static final boolean INTERNAL_CAN_TRY_AGAIN = true;
    private static final int DEFAULT_TRY_AGAIN_DELAY = 5;

    private final List<DispatchServiceMapper> delegates;

    public RpcRequestDispatcher(List<DispatchServiceMapper> delegates) {
        this.delegates = Objects.requireNonNull(delegates, "'delegates'");
    }

    public Publisher<?> dispatch(MTProtoSession session, Long requestId, Request<? extends Response> request) {
        Objects.requireNonNull(session);
        Objects.requireNonNull(request);

        DispatchContext<?> dispatchContext = DispatchContext.of(
                session.getId().getAuthId(),
                session.getId().getSessionId(),
                (Long) session.getPrincipal(),
                Optional.ofNullable(session.getRemoteAddress())
                        .map(InetSocketAddress::toString).orElse(""),
                request);

        Mono<? extends Response> result = Mono.empty();
        for (DispatchServiceMapper delegate : delegates) {
            result = result.switchIfEmpty(delegate.dispatch(dispatchContext));
        }

        return result.switchIfEmpty(Mono.error(new UnsupportedRequestException(request)))
                .checkpoint("Dispatch handled for " + session.getId() + " request " + request.getClass())
                .flatMap(response -> writeOkResponse(session, getDefaultId(requestId), response))
                .onErrorResume(error -> {
                    log.warn("Unexpected error due processing RPC request: {}", request, error);
                    return writeErrorResponse(session, requestId, error);
                })
                .then();
    }

    protected Mono<Void> writeOkResponse(MTProtoSession session, long requestId, Response response) {
        return Mono.defer(
                () -> {
                    RpcOk ok = new RpcOk(response.getHeader(), response.toByteArray());
                    MTRpcResponse rpcResponse = new MTRpcResponse(requestId, ok.toByteArray());
                    return session.sendRpcMessage(Mono.just(rpcResponse));
                });
    }

    protected Mono<Void> writeErrorResponse(MTProtoSession session, Long requestId, Throwable ex) {
        return Mono.<ProtoStruct>create((sink) -> {
            if (ex instanceof RpcException) {
                RpcException rpcException = (RpcException) ex;
                if (log.isTraceEnabled()) {
                    log.trace("RPC exception while processing request " + requestId + rpcException.getMessage());
                }
                sink.success(new RpcError(rpcException.getErrorCode(), rpcException.getErrorTag(),
                        rpcException.getErrorMessage()));
            }
            log.warn("Unexpected error due processing rpc request {}", requestId, ex);
            sink.success(new RpcInternalError(INTERNAL_CAN_TRY_AGAIN, DEFAULT_TRY_AGAIN_DELAY));
        }).flatMap(errorResponse -> session.sendRpcMessage(Mono.just(new MTRpcResponse(getDefaultId(requestId),
                errorResponse.toByteArray()))));
    }

    private static long getDefaultId(final Long id) {
        return Optional.ofNullable(id).orElse(Long.MAX_VALUE);
    }

    /**
     * Exception occurs when the given request is not supported.
     *
     */
    @SuppressWarnings("serial")
    static class UnsupportedRequestException extends MTProtoException {

        /**
         * Constructs a {@link UnsupportedRequestException instance}.
         *
         * @param request the associated reason (optional)
         */
        public UnsupportedRequestException(Request<? extends Response> request) {
            /*super(RpcErrors.REQUEST_NOT_SUPPORTED.getErrorCode(), RpcErrors.REQUEST_NOT_SUPPORTED.getErrorTag(),
                    RpcErrors.REQUEST_NOT_SUPPORTED.getErrorMsg(), "Request " + request.getClass() + " is not
                    supported");*/
        }
    }

}
