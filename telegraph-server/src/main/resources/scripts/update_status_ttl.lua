--local ttl = struct.unpack('>h', KEYS[1])
--for i = 2, #KEYS do
  --  redis.call('expire', KEYS[i], ttl)
--end
local presenceKey = KEYS[1]
local expire = ARGV[1]

local result = redis.call("EXPIRE", presenceKey, expire)
return result > 0