package im.telegraph.server;

import im.telegraph.engine.serialization.stream.BinaryInputStream;
import im.telegraph.util.BufferUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.lang.Nullable;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

/**
 * A transformation function to transform {@link ByteBuffer} to a sequence of {@link TransportFrame} payloads.
 */
public class TransportFrameTransformer implements Function<ByteBuffer, List<TransportFrame>> {

    private static final Logger log = LoggerFactory.getLogger(TransportFrameTransformer.class);

    private int byteCount;

    private final int minInMemorySize;
    private final int maxInMemorySize;
    private final String logPrefix;

    public TransportFrameTransformer(int maxInMemorySize, int minInMemorySize, @Nullable String logPrefix) {
        if (maxInMemorySize < 0) {
            throw new IllegalArgumentException("Negative maxInMemorySize value");
        }
        if (minInMemorySize < 0) {
            throw new IllegalArgumentException("Negative minInMemorySize value");
        }
        this.maxInMemorySize = maxInMemorySize;
        this.minInMemorySize = minInMemorySize;
        this.logPrefix = logPrefix;
    }

    @Nullable
    protected String getLogPrefix() {
        return this.logPrefix;
    }

    @Override
    public List<TransportFrame> apply(ByteBuffer buffer) {
        try {
            final var packetSize = buffer.remaining();

            if (packetSize < minInMemorySize) {
                log.warn(getLogPrefix() + " Received transport packet is too small: {}", packetSize);
                return Collections.emptyList();
            }

            if (packetSize > maxInMemorySize) {
                log.warn(getLogPrefix() + " Received transport packet is too big: {}", packetSize);
                raiseLimitException();
            }
            increaseByteCount(buffer);

            final byte[] bytes = BufferUtils.asByteArrayV2(buffer);
           // buffer.get(bytes);

            final List<TransportFrame> frames = new ArrayList<>();

            final BinaryInputStream inputStream = new BinaryInputStream(bytes);
            do {
                final var packageId = inputStream.readInt();
                final var header = inputStream.readByte();
                final var size = inputStream.readInt();
                final var content = inputStream.readBytes(size);
                final var checksum = inputStream.readInt();

                frames.add(new TransportFrame(packageId, header, size, content, checksum));
            } while (!inputStream.isEOF());

            return frames;
        } catch (IOException ex) {
            log.warn(getLogPrefix() + " Unexpected I/O error while parsing transport packet.", ex);
            throw new RuntimeException("I/O error while parsing input stream", ex);
        }
    }

    private void increaseByteCount(ByteBuffer dataBuffer) {
        if (this.maxInMemorySize > 0) {
            if (dataBuffer.remaining() > Integer.MAX_VALUE - this.byteCount) {
                raiseLimitException();
            } else {
                this.byteCount += dataBuffer.remaining();
            }
        }
    }

    private void raiseLimitException() {
        throw new IllegalStateException("Exceeded limit on max bytes per packet: " + this.maxInMemorySize);
    }
}
