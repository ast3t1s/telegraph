package im.telegraph.server.test;

import im.telegraph.crypto.ActorProtoKey;
import im.telegraph.crypto.box.CBCHmacBox;
import im.telegraph.crypto.primitives.kuznechik.KuznechikFastEngine;
import im.telegraph.crypto.primitives.streebog.Streebog256;
import im.telegraph.crypto.primitives.util.ByteStrings;
import im.telegraph.engine.serialization.stream.BinaryInputStream;
import im.telegraph.engine.serialization.stream.BinaryOutputStream;
import im.telegraph.protocol.model.ProtoSerializer;
import im.telegraph.protocol.model.mtproto.*;
import im.telegraph.protocol.model.transport.*;
import im.telegraph.server.TransportFrame;
import im.telegraph.server.test.schedule.PreparedPackage;
import im.telegraph.server.test.schedule.SchedullerV2;
import im.telegraph.server.test.state.ConnectionInfo;
import im.telegraph.server.test.transport.TcpCallback;
import im.telegraph.server.test.transport.TcpContext;
import im.telegraph.util.Crypto;
import im.telegraph.util.ThreadLocalCRC32;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashSet;
import java.util.Queue;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;

public class MTProto {

    private static final Logger log = LoggerFactory.getLogger(MTProto.class);

    private final AtomicInteger idHelper = new AtomicInteger();

    private static final int HEADER_PROTO = 0;
    private static final int HEADER_PING = 1;
    private static final int HEADER_PONG = 2;
    private static final int HEADER_DROP = 3;
    private static final int HEADER_REDIRECT = 4;
    private static final int HEADER_ACK = 6;
    private static final int HEADER_HANDSHAKE_REQUEST = 0xFF;
    private static final int HEADER_HANDSHAKE_RESPONSE = 0xFE;

    private final AtomicInteger packetIndexer = new AtomicInteger(0);

    private final Queue<ProtoPackage> inQueue = new ConcurrentLinkedQueue<>();

    private final Set<TcpContext> contexts = new HashSet<>();

    private SchedullerThread schedullerThread;
    private ResponseProcessor responseProcessor;
    private ConnectionThread connectionThread;

    private final TcpListener tcpCallback;

    private volatile boolean isClosed;

    private final AtomicInteger outSeq = new AtomicInteger(0);
    private final AtomicInteger inSeq = new AtomicInteger(0);

    private static final Random RANDOM = new Random();

    private final Long authKey;

    private volatile byte[] masterKey;

    private final long sessionId;

    private final ActorProtoKey authProtoKey;
    private final CBCHmacBox serverUSDecryptor;
    private final CBCHmacBox serverRUDecryptor;
    private final CBCHmacBox clientUSEncryptor;
    private final CBCHmacBox clientRUEncryptor;

    private final im.telegraph.server.test.MTProtoCallback callback;

    private int receivedPackages = 0;

    private SchedullerV2 scheduller;

    public MTProto(Long authKey, byte[] masterKey, MTProtoCallback callback) {
        this.authKey = authKey;
        this.masterKey = masterKey;
        this.sessionId = ThreadLocalRandom.current().nextLong();
        this.callback = callback;
        this.scheduller = new SchedullerV2();
        if (this.masterKey != null) {
            this.authProtoKey = new ActorProtoKey(this.masterKey);
            this.serverUSDecryptor = new CBCHmacBox(Crypto.createAES128(this.authProtoKey.getServerKey()),
                    Crypto.createSHA256(), this.authProtoKey.getServerMacKey());
            this.serverRUDecryptor = new CBCHmacBox(new KuznechikFastEngine(this.authProtoKey.getServerRussianKey()),
                    new Streebog256(), this.authProtoKey.getServerMacRussianKey());
            this.clientUSEncryptor = new CBCHmacBox(Crypto.createAES128(this.authProtoKey.getClientKey()),
                    Crypto.createSHA256(), this.authProtoKey.getClientMacKey());
            this.clientRUEncryptor = new CBCHmacBox(new KuznechikFastEngine(this.authProtoKey.getClientRussianKey()),
                    new Streebog256(), this.authProtoKey.getClientMacRussianKey());
        } else {
            this.authProtoKey = null;
            this.serverUSDecryptor = null;
            this.serverRUDecryptor = null;
            this.clientUSEncryptor = null;
            this.clientRUEncryptor = null;
        }
        this.tcpCallback = new TcpListener();
        this.connectionThread = new ConnectionThread();
        this.connectionThread.start();
        this.schedullerThread = new SchedullerThread();
        this.schedullerThread.start();
        this.responseProcessor = new ResponseProcessor();
        this.responseProcessor.start();
    }

    public void eraseKey() {
        this.masterKey = null;
    }

    public void close() {
        if (!isClosed) {
            this.isClosed = true;
            if (this.connectionThread != null) {
                this.connectionThread.interrupt();
            }
            if (this.schedullerThread != null) {
                this.schedullerThread.interrupt();
            }
            if (this.responseProcessor != null) {
                this.responseProcessor.interrupt();
            }
            closeConnections();
        }
    }

    public void requestSchedule() {
        synchronized (scheduller) {
            scheduller.notifyAll();
        }
    }

    public long sendMessage(byte[] payload) {
        long id = scheduller.postPackage(new SchedullerV2.SendMessage(ThreadLocalRandom.current().nextLong(), payload));
        synchronized (scheduller) {
            scheduller.notifyAll();
        }
        return id;
    }

    public boolean isClosed() {
        return this.isClosed;
    }

    public void closeConnections() {
        synchronized (contexts) {
            for (TcpContext context : contexts) {
                context.close();
            }
            contexts.clear();
            contexts.notifyAll();
        }
    }

    private void onApiMessage(long messageId, byte[] payload) {
        callback.onResponseMessage(messageId, payload, this);
    }

    private void onUpdate(byte[] payload) {
        callback.onUpdate(payload);
    }

    private void onProtoMessage(long messageId, byte[] payload) {
        try {
            ProtoStruct obj;
            try {
                obj = ProtoSerializer.readMessagePayload(payload);
                if (obj instanceof NewSessionCreated) {
                    log.info("New session created");
                } else if (obj instanceof Container) {
                    Container container = (Container) obj;
                    for (ProtoMessage message : container.getMessages()) {
                        onProtoMessage(message.getMessageId(), message.getPayload());
                    }
                } else if (obj instanceof SessionLost) {

                } else if (obj instanceof MTRpcResponse) {
                    MTRpcResponse responseBox = (MTRpcResponse) obj;
                    onApiMessage(responseBox.getMessageId(), responseBox.getPayload());
                } else if (obj instanceof MTUpdateBox) {
                    MTUpdateBox event = (MTUpdateBox) obj;
                    onUpdate(event.getPayload());
                } else if (obj instanceof AuthIdInvalid) {
                    log.info("Auth ID invalid");
                    close();
                } else {
                    log.warn("Unsupported package " + obj);
                }
            } catch (IOException e) {
                log.warn("Unable to parse message: ignoring", e);
                e.printStackTrace();
                return;
            }
        } catch (Exception e) {
            log.warn("Error parsing", e);
            e.printStackTrace();
            close();
        }
    }

    private void onMTMessage(ProtoPackage mtPackage) {
        final var authId = mtPackage.getAuthId();
        final var sessionId = mtPackage.getSessionId();

        if (authKey != authId || this.sessionId != sessionId) {
            log.warn("Incorrect header");
            return;
        }
        try {
            if (masterKey != null) {
                EncryptedPackage encryptedPackage = new EncryptedPackage(
                        new BinaryInputStream(mtPackage.getPayload().toByteArray()));
                int seq = (int) encryptedPackage.getSeqNumber();
                if (seq != inSeq.get()) {
                    throw new IOException("Expected " + inSeq + ", got: " + seq);
                }
                inSeq.incrementAndGet();
                EncryptedCBCPackage usEncryptedPackage = new EncryptedCBCPackage(
                        new BinaryInputStream(encryptedPackage.getEncryptedPackage()));
                byte[] ruPackage = serverUSDecryptor.decryptPackage(ByteStrings.longToBytes(seq),
                        usEncryptedPackage.getIv(), usEncryptedPackage.getEncryptedContent());
                EncryptedCBCPackage ruEncryptedPackage = new EncryptedCBCPackage(new BinaryInputStream(ruPackage));
                byte[] plainText = serverRUDecryptor.decryptPackage(ByteStrings.longToBytes(seq),
                        ruEncryptedPackage.getIv(), ruEncryptedPackage.getEncryptedContent());

                BinaryInputStream ptInput = new BinaryInputStream(plainText);
                long messageId = ptInput.readInt64();
                byte[] ptPayload = ptInput.readProtoBytes();
                onProtoMessage(messageId, ptPayload);
            } else {
                long messageId = mtPackage.getPayload().getMessageId();
                byte[] payload = mtPackage.getPayload().getPayload();
                onProtoMessage(messageId, payload);
            }
        } catch (Exception e) {
            log.warn("Closing connection: exception during receive", e);
            close();
            outSeq.set(0);
            inSeq.set(0);
        }
    }

    private void onOutMessage(byte[] data, int offset, int len, TcpContext context) {
        try {
            if (masterKey != null) {
                int seq = outSeq.getAndIncrement();

                byte[] ruIv = new byte[16];
                Crypto.nextBytes(ruIv);
                byte[] usIv = new byte[16];
                Crypto.nextBytes(usIv);

                byte[] ruCipherText = clientRUEncryptor.encryptPackage(ByteStrings.longToBytes(seq), ruIv,
                        ByteStrings.substring(data, offset, len));
                byte[] ruPackage = new EncryptedCBCPackage(ruIv, ruCipherText).toByteArray();
                byte[] usCipherText = clientUSEncryptor.encryptPackage(ByteStrings.longToBytes(seq), usIv, ruPackage);
                byte[] usPackage = new EncryptedCBCPackage(usIv, usCipherText).toByteArray();

                EncryptedPackage encryptedPackage = new EncryptedPackage(seq, usPackage);
                byte[] cipherData = encryptedPackage.toByteArray();
                BinaryOutputStream bos = new BinaryOutputStream();
                bos.writeInt64(authKey);
                bos.writeInt64(sessionId);
                bos.writeBytes(cipherData, 0, cipherData.length);
                byte[] pkg = bos.toByteArray();

                rawPost(HEADER_PROTO, pkg, context);
            } else {
                BinaryOutputStream outputStream = new BinaryOutputStream();
                outputStream.writeInt64(authKey);
                outputStream.writeInt64(sessionId);
                outputStream.writeBytes(data, offset, len);
                byte[] payload = outputStream.toByteArray();
                rawPost(HEADER_PROTO, payload, context);
            }
        } catch (Exception e) {
            log.warn("Closing connection: exception during send", e);
            close();
            outSeq.set(0);
            inSeq.set(0);
        }
    }

    private class ConnectionThread extends Thread {

        public ConnectionThread() {
            setName("ConnectionThread#" + hashCode());
        }

        @Override
        public void run() {
            while (!isClosed) {
                synchronized (contexts) {
                    if (contexts.size() >= 1) {
                        try {
                            contexts.wait();
                        } catch (InterruptedException e) {
                            return;
                        }
                    }
                }

                try {
                    ConnectionInfo connectionInfo = new ConnectionInfo("127.0.0.1", 443);
                    TcpContext context = new TcpContext(connectionInfo.getHost(), connectionInfo.getPort(),
                            tcpCallback);
                    if (isClosed) {
                        return;
                    }
                    synchronized (contexts) {
                        contexts.add(context);
                    }
                    synchronized (scheduller) {
                        scheduller.notifyAll();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        }

    }

    private class ResponseProcessor extends Thread {

        public ResponseProcessor() {
            setName("ResponseProcessor#" + hashCode());
        }

        @Override
        public void run() {
            while (!isClosed) {
                synchronized (inQueue) {
                    if (inQueue.isEmpty()) {
                        try {
                            inQueue.wait();
                        } catch (InterruptedException e) {
                            return;
                        }
                    }
                    if (inQueue.isEmpty()) {
                        continue;
                    }
                }
                ProtoPackage message = inQueue.poll();
                if (message != null) {
                    onMTMessage(message);
                }
            }
        }

    }

    private void onPingPackage(byte[] content, TcpContext context) throws IOException {
        if (isClosed) {
            return;
        }
        Ping ping = (Ping) ProtoSerializer.readTransportPayload(Ping.HEADER, new BinaryInputStream(content));
        rawPost(HEADER_PONG, new Pong(ping.getRandomId()).toByteArray(), context);
    }

    private void sendAck(int index, TcpContext context) {
        if (isClosed) {
            return;
        }
        Ack ack = new Ack(index);
        rawPost(HEADER_ACK, ack.toByteArray(), context);
    }

    private void rawPost(int header, byte[] data, TcpContext context) {
        rawPost(header, data, 0, data.length, context);
    }

    private void rawPost(int header, byte[] data, int offset, int len, TcpContext context) {
        int packageId = packetIndexer.incrementAndGet();
        int checksum = ThreadLocalCRC32.calculateChecksum(data);
        TransportFrame transportPackage = new TransportFrame(packageId, header, len, data, checksum);
        byte[] payload = transportPackage.toByteArray();
        context.postMessage(payload);
    }

    private class SchedullerThread extends Thread {

        private SchedullerThread() {
            setName("Scheduller#" + hashCode());
        }

        @Override
        public void run() {
            while (!isClosed) {
                TcpContext context = null;
                synchronized (contexts) {
                    TcpContext[] currentContexts = contexts.toArray(new TcpContext[0]);
                    if (currentContexts.length != 0) {
                        context = currentContexts[0];
                    } else {
                        continue;
                    }
                }
                synchronized (scheduller) {
                    PreparedPackage preparedPackage = scheduller.doSchedule();
                    if (preparedPackage == null) {
                        continue;
                    }
                    if (!context.isClosed()) {
                        onOutMessage(preparedPackage.getPayload(), preparedPackage.getOffset(),
                                preparedPackage.getLength(), context);
                    }
                }
            }
        }

    }

    public long postRpcMessage(byte[] msg) {
        ProtoMessage protoMessage = new ProtoMessage(idHelper.incrementAndGet(), msg);
        TcpContext[] current = contexts.toArray(new TcpContext[0]);
        TcpContext curr = current[0];
        if (curr != null && !curr.isClosed()) {
            onOutMessage(msg, 0, msg.length, curr);
        }
        return protoMessage.getMessageId();
    }

    private class TcpListener implements TcpCallback {

        @Override
        public void onConnected(TcpContext context) {
            byte[] handshakeData = new byte[32];
            synchronized (RANDOM) {
                RANDOM.nextBytes(handshakeData);
            }
            Handshake handshakePackage = new HandshakeRequest(
                    1, 1, 0, handshakeData
            );
            BinaryOutputStream outputStream = new BinaryOutputStream();
            outputStream.writeByte(1);
            outputStream.writeByte(2);
            outputStream.writeByte(1);
            outputStream.writeInt32(handshakeData.length);
            outputStream.writeBytes(handshakeData, 0, handshakeData.length);
            
            rawPost(HEADER_HANDSHAKE_REQUEST, outputStream.toByteArray(), context);
        }

        @Override
        public void onRawMessage(byte[] data, TcpContext context) {
            if (isClosed) {
                return;
            }
            try {
                BinaryInputStream inputStream = new BinaryInputStream(data);
                int packageIndex = inputStream.readInt();

                if (packageIndex != receivedPackages) {
                    log.warn("Invalid package index. Expected: " + receivedPackages + ", got: " + packageIndex);
                    throw new IOException("Invalid package index. Expected: " + receivedPackages + ", got: " + packageIndex);
                }
                receivedPackages++;

                int header = inputStream.readByte();
                int dataSize = inputStream.readInt();
                byte[] content = inputStream.readBytes(dataSize);
                int crc32 = inputStream.readInt();

                if (dataSize != content.length) {
                    throw new IOException("Incorrect payload size");
                }

                int checksum = ThreadLocalCRC32.calculateChecksum(content);
                if (crc32 != checksum) {
                    throw new IOException("Incorrect crc32");
                }

                if (header == HEADER_PROTO) {
                    var mtStream = new BinaryInputStream(content, 0, content.length);
                    inQueue.add(new ProtoPackage(mtStream));
                    synchronized (inQueue) {
                        inQueue.notifyAll();
                    }
                    sendAck(packageIndex, context);
                } else if (header == HEADER_ACK) {
                    log.info("Received ack packet");
                } else if (header == HEADER_PING) {
                    onPingPackage(content, context);
                } else if (header == HEADER_PONG) {

                } else if (header == Drop.HEADER) {
                    log.info("Received drop message");
                } else if (header == HEADER_HANDSHAKE_RESPONSE) {
                    log.info("Received handshake response");
                } else {
                    log.warn("Received packet with unknown header#:{}", header);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        @Override
        public void onBroken(TcpContext context) {
            if (isClosed) {
                return;
            }
            log.debug("Channel broken (#" + context.getContextId() + ")");
            synchronized (contexts) {
                contexts.remove(context);
                contexts.notifyAll();
            }
            receivedPackages = 0;
        }

    }

}
