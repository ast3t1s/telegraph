package im.telegraph.domain.entity.chat;

import im.telegraph.domain.entity.FileReferenceId;

import java.io.Serializable;

/**
 * @author Ast3t1s
 */
public final class ChatPhoto implements Serializable {

    private static final long serialVersionUID = 1L;

    private final FileReferenceId smallPhoto;
    private final FileReferenceId largePhoto;

    public ChatPhoto(FileReferenceId smallPhoto, FileReferenceId largePhoto) {
        this.smallPhoto = smallPhoto;
        this.largePhoto = largePhoto;
    }

    public FileReferenceId getSmallPhoto() {
        return smallPhoto;
    }

    public FileReferenceId getLargePhoto() {
        return largePhoto;
    }

    @Override
    public String toString() {
        return "ChatPhoto{" +
                "smallPhoto=" + smallPhoto +
                ", largePhoto=" + largePhoto +
                '}';
    }
}
