package im.telegraph.common;

import reactor.core.Disposable;
import reactor.core.Disposables;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

import java.time.Duration;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Emit ticks at a constant rate specified at {@link #start()}} and will continue until
 * {@link #stop()} is called or {@link #start} is re-invoked, resetting the previous emitter.
 *
 * @author Ast3t1s
 */
public class ResettableInterval {

    private static final Supplier<Scheduler> DEFAULT_INTERVAL_SCHEDULER = () ->
            Schedulers.newSingle("tl-interval", true);

    private final Duration delay;
    private final Duration interval;
    private final Disposable.Swap task;
    private final Scheduler scheduler;
    private final Function<Long, Mono<Void>> tick;

    public ResettableInterval(Duration delay, Duration interval, Function<Long, Mono<Void>> tick) {
        this(DEFAULT_INTERVAL_SCHEDULER.get(), delay, interval, tick);
    }

    /**
     * Create a {@link ResettableInterval} that emits ticks on the given {@link Scheduler} upon calling
     * {@link #start()}
     *
     * @param scheduler the Reactor {@link Scheduler} to use to emit ticks
     * @param delay     delay the {@link Duration} to wait before emitting the first tick
     * @param interval  period the period {@link Duration} used to emit ticks.
     * @param tick      the {@link Function} that would invoke every tick
     */
    public ResettableInterval(Scheduler scheduler, Duration delay, Duration interval, Function<Long, Mono<Void>> tick) {
        this.scheduler = scheduler;
        this.delay = delay;
        this.interval = interval;
        this.tick = tick;
        this.task = Disposables.swap();
    }

    /**
     * Begin producing ticks at the given rate.
     *
     * @see Flux#interval(Duration, Duration, Scheduler)
     */
    public void start() {
        this.task.replace(Flux.interval(delay, interval, scheduler)
                .subscribeOn(scheduler)
                .concatMap(tick)
                .onErrorResume(ex -> Mono.empty())
                /*.retryWhen(Retry.indefinitely().doBeforeRetry(s -> log.warn("Unexpected error#-{} tick",
                        hashCode(), s.failure())))*/
                .subscribe());
    }

    /**
     * Dispose the current emitter task without completing or cancelling existing subscriptions to {@link #ticks()}.
     */
    public void stop() {
        if (task.get() != null) {
            task.get().dispose();
        }
    }
}
