package im.telegraph.domain.repository.rdbms.dialect;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Ast3t1s
 */
public class MySqlDialect implements Dialect {

    @Override
    public String getLimit(int limit) {
        return "LIMIT " + limit;
    }

    @Override
    public String getOffset(int offset) {
        // Ugly but the official workaround for offset without limit
        // see: https://stackoverflow.com/a/271650
        return String.format("LIMIT %d, 18446744073709551615", offset);
    }

    @Override
    public String getLimitOffset(int limit, int offset) {
        // LIMIT {[offset,] row_count}
        return String.format("LIMIT %s, %s", offset, limit);
    }

    @Override
    public String buildUpsertQuery(String table, Map<String, Object> insertSpec, List<String> uniqueColumns,
                                   List<String> updateColumns, String... incrementFields) {
        String updatePart = updateColumns.stream()
                .map(column -> column + " = VALUES(" + column + ")")
                .collect(Collectors.joining(", "));

        String insertExpression = insertSpec.values().stream()
                .map(value -> (String) value)
                .map(value -> ":" + value)
                .collect(Collectors.joining(", "));

        StringBuilder query = new StringBuilder("INSERT INTO ")
                .append(table).append("(").append(String.join(", ", insertSpec.keySet()))
                .append(") VALUES (").append(insertExpression).append(") ON CONFLICT (")
                .append(updatePart);

        if (incrementFields.length > 0) {
            String incrementExpression = Arrays.stream(incrementFields)
                    .map(value -> value + " = " + value + " VALUES(" + value + ")")
                    .collect(Collectors.joining(", "));

            query.append(incrementExpression);
        }

        return query.toString();
    }

    @Override
    public String buildUpsertQueryMany(String table, List<String> insertSpec, String batchName, List<String> uniqueColumns,
                                       List<String> updateColumns, String... incrementFields) {
        String insertFields = insertSpec.stream()
                .collect(Collectors.joining(", "));

        String updateExpression = uniqueColumns.stream()
                .map(value -> value + " = VALUES(" + value + ")")
                .collect(Collectors.joining(", "));

        StringBuilder query = new StringBuilder("INSERT INTO ")
                .append(table).append(insertFields)
                .append(" VALUES :").append(batchName)
                .append(" ON DUPLICATE KEY UPDATE ").append(updateExpression);

        if (incrementFields.length > 0) {
            String incrementExpression = Arrays.stream(incrementFields)
                    .map(value -> value + " = " + value + "VALUES(" + value + ")")
                    .collect(Collectors.joining(", "));

            query.append(", ").append(incrementExpression);
        }

        return query.toString();
    }
}
