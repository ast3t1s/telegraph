package im.telegraph.common.property.protocol.crypto;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Ast3t1s
 */
public class CryptoProperties {

    /**
     * Whether or not use encrypted version of protocol.
     */
    private boolean encrypting = true;

    /**
     * Shared secret key for access hashing.
     */
    private String sharedSecret;

    /**
     * Map of trusted keys.
     */
    private Map<String, TrustedKeyProperties> trustedKeys = new LinkedHashMap<>();

    public boolean isEncrypting() {
        return encrypting;
    }

    public void setEncrypting(boolean encrypting) {
        this.encrypting = encrypting;
    }

    public String getSharedSecret() {
        return sharedSecret;
    }

    public void setSharedSecret(String sharedSecret) {
        this.sharedSecret = sharedSecret;
    }

    public Map<String, TrustedKeyProperties> getTrustedKeys() {
        return trustedKeys;
    }

    public void setTrustedKeys(Map<String, TrustedKeyProperties> trustedKeys) {
        this.trustedKeys = trustedKeys;
    }
}
