package im.telegraph.protocol.auth;

import reactor.core.publisher.Mono;

/**
 * @author Ast3t1s
 */
public interface AuthKeyStore {

    Mono<AuthKey> find(Long id);

    Mono<Void> delete(Long id);
}
