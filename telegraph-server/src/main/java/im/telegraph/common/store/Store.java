package im.telegraph.common.store;

import org.reactivestreams.Publisher;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

/**
 * This provides an active data connection to a store's data source, supporting read and write operations.
 *
 * @param <K> The key type which provides a 1:1 mapping to the value type. This type is also expected to be
 * {@link Comparable} in order to allow for range operations.
 * @param <V> The value type, these follow
 * <a href="https://en.wikipedia.org/wiki/JavaBeans#JavaBean_conventions">JavaBean</a> conventions.
 */
public interface Store<K extends Comparable<K>, V> extends ReadOnlyStore<K, V> {

    /**
     * Stores a key value pair.
     *
     * @param key The key representing the value.
     * @param value The value.
     * @return A mono which signals the completion of the storage of the pair.
     */
    Mono<V> save(K key, V value);

    /**
     * Stores key value pairs.
     *
     * @param entryStream A flux providing the key value pairs.
     * @return A mono which signals the completion of the storage of the pairs.
     */
    Mono<Void> save(Publisher<Tuple2<K, V>> entryStream);

    /**
     * Remove a value associated with the provided id.
     *
     * @param key The key of the value to delete.
     * @return A mono which signals the completion of the deletion of the value.
     */
    Mono<Void> remove(K key);

    /**
     * Remove the values associated with the provided ids.
     *
     * @param keys A stream of keys to delete values for.
     * @return A mono which signals the completion of the deletion of the values.
     */
    Mono<Void> remove(Publisher<K> keys);

    /**
     * Remove all entries in the data source.
     *
     * @return A mono which signals the completion of the deletion operation.
     */
    Mono<Void> removeAll();

    /**
     * Invalidates the contents of the store. Once this is invoked, there is no longer a guarantee that the
     * data in the store is reliable.
     *
     * @return A mono which signals the completion of the invalidation of all values.
     */
    Mono<Void> invalidate();
}
