package im.telegraph.protocol.model.mtproto;

import im.telegraph.engine.serialization.DataInputStream;
import im.telegraph.engine.serialization.DataOutputStream;

import java.io.IOException;

public class EncryptedCBCPackage extends ProtoObject {

	private byte[] iv;
	private byte[] encryptedContent;

	public EncryptedCBCPackage(DataInputStream inputStream) throws IOException {
		super(inputStream);
	}

	public EncryptedCBCPackage(byte[] iv, byte[] encryptedContent) {
		this.iv = iv;
		this.encryptedContent = encryptedContent;
	}

	public byte[] getIv() {
		return iv;
	}

	public byte[] getEncryptedContent() {
		return encryptedContent;
	}

	@Override
	public void writeObject(DataOutputStream outputStream) throws IOException {
		outputStream.writeProtoBytes(iv);
		outputStream.writeProtoBytes(encryptedContent);
	}

	@Override
	public ProtoObject readObject(DataInputStream inputStream) throws IOException {
		this.iv = inputStream.readProtoBytes();
		this.encryptedContent = inputStream.readProtoBytes();
		return this;
	}

}
