package im.telegraph.common.property.protocol.crypto;

/**
 * @author Ast3t1s
 */
public class TrustedKeyProperties {

    /**
     * Path to public key.
     */
    private String publicKey;

    /**
     * Path to private key.
     */
    private String privateKey;

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }
}
