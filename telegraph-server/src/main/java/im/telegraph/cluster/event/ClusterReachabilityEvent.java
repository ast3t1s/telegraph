package im.telegraph.cluster.event;

import im.telegraph.cluster.ClusterEvent;
import im.telegraph.cluster.Member;

/**
 * @author Ast3t1s
 */
public class ClusterReachabilityEvent implements ClusterEvent {

    public enum Type {
        REACHABLE,
        UNREACHABLE
    }

    private final Type type;
    private final Member member;

    public ClusterReachabilityEvent(Type type, Member member) {
        this.type = type;
        this.member = member;
    }

    public static ClusterReachabilityEvent reachable(Member member) {
        return new ClusterReachabilityEvent(Type.REACHABLE, member);
    }

    public static ClusterReachabilityEvent unreachable(Member member) {
        return new ClusterReachabilityEvent(Type.UNREACHABLE, member);
    }

    public Type getType() {
        return type;
    }

    public Member getMember() {
        return member;
    }

    public boolean isReachable() {
        return type == Type.REACHABLE;
    }

    public boolean isUnreachable() {
        return type == Type.UNREACHABLE;
    }

    @Override
    public String toString() {
        return "ClusterReachabilityEvent{" +
                "type=" + type +
                ", member=" + member +
                '}';
    }
}
