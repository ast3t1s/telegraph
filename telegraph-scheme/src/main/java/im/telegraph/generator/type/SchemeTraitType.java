package im.telegraph.generator.type;

public class SchemeTraitType extends SchemeType {
    private String traitName;

    public SchemeTraitType(String traitName) {
        this.traitName = traitName;
    }

    public String getTraitName() {
        return traitName;
    }
} 
