package im.telegraph.domain.entity;

/**
 * @author Ast3t1s
 */
public final class ProfilePhoto {

    private final long id;
    private final FileReferenceId photoSmall;
    private final FileReferenceId photoLarge;
    private final int addedDate;

    public ProfilePhoto(long id, FileReferenceId photoSmall, FileReferenceId photoLarge, int addedDate) {
        this.id = id;
        this.photoSmall = photoSmall;
        this.photoLarge = photoLarge;
        this.addedDate = addedDate;
    }

    /**
     * Gets id of chat photo.
     *
     * @return The id of chat photo.
     */
    public long getId() {
        return id;
    }

    /**
     * Gets {@link FileReferenceId} of <b>small</b> chat photo.
     *
     * @return The {@link FileReferenceId} of <b>small</b> chat photo.
     */
    public FileReferenceId getPhotoSmall() {
        return photoSmall;
    }

    /**
     * Gets serialized {@link FileReferenceId} of <b>big</b> chat photo.
     *
     * @return The serialized {@link FileReferenceId} of <b>big</b> chat photo.
     */
    public FileReferenceId getPhotoLarge() {
        return photoLarge;
    }

    /**
     * Gets timestamp when photo was added.
     *
     * @return The timestamp when photo was added
     */
    public int getAddedDate() {
        return addedDate;
    }

    @Override
    public String toString() {
        return "ProfilePhoto{" +
                "id=" + id +
                ", smallImage=" + photoSmall +
                ", largeImage=" + photoLarge +
                ", addedDate=" + addedDate +
                '}';
    }
}
