package im.telegraph.common.property.admin;

import im.telegraph.common.property.TlsProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.util.unit.DataSize;

import java.net.InetAddress;

/**
 * @author Ast3t1s
 */
@Getter
@Setter
@Accessors(chain = true)
public class AdminApiProperties {

    /**
     * Hostname clients should connect to. Can be set to an ip, hostname or one of the following special values:
     */
    private InetAddress host;

    /**
     * The default remote server port clients should connect to.
     * Default is 7515, use 0 if you want a random available port
     */
    private int port = 9099;

    /**
     * Max request size;
     */
    private DataSize maxRequestSize = DataSize.ofMegabytes(10);

    @NestedConfigurationProperty
    private TlsProperties ssl = new TlsProperties();
} 
