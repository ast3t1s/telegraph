package im.telegraph.cluster;

import java.io.Serializable;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Ast3t1s
 */
public final class Version implements Comparable<Version>, Serializable {

    private static final long serialVersionUID = 1L;

    private static final String UNKNOWN_VERSION_STRING = "0.0.0";

    public static final Version ZERO = new Version(0, 0, 0);

    private static final Pattern VERSION_PATTERN = Pattern.compile("^(\\d+)\\.(\\d+)\\.(\\d+)(-SNAPSHOT)?$");

    private final byte major;
    private final byte minor;
    private final byte patch;

    private final String version;

    private final transient int versionNumber;

    private Version(int major, int minor, int patch) {
        if (major < 0 || minor < 0 || patch < 0) {
            throw new IllegalArgumentException("Wrong api version");
        }
        this.major = (byte) major;
        this.minor = (byte) minor;
        this.patch = (byte) patch;

        this.version = formatVersion(this.major, this.minor, this.patch);
        this.versionNumber = fromBytes(this.major, this.minor, this.patch, (byte) 0);
    }

    /**
     * Create a default {@link Version} using the given version in format "major.minor.patch".
     *
     * @param major major version
     * @param minor minor version
     * @param patch patch version
     * @return a default build version
     */
    public static Version of(int major, int minor, int patch) {
        return new Version(major, minor, patch);
    }

    /**
     * Create a default {@link Version} using the given version in format "major.minor.patch".
     *
     * @param version the version to parse
     * @return a default build version
     */
    public static Version from(final String version) {
        if (version == null || version.isEmpty()) {
            throw new IllegalArgumentException("Version is empty.");
        }
        if (version.startsWith(UNKNOWN_VERSION_STRING)) {
            return ZERO;
        }

        final Matcher matcher = VERSION_PATTERN.matcher(version);
        if (!matcher.matches()) {
            throw new IllegalStateException("Wrong instance version format: " + version);
        }

        final byte major = Byte.parseByte(matcher.group(1));
        final byte minor = Byte.parseByte(matcher.group(2));
        final byte patch = Byte.parseByte(matcher.group(3));

        return new Version(major, minor, patch);
    }

    /**
     * Returns the proto version.
     *
     * @return the proto version
     */
    public byte getMajor() {
        return major;
    }

    /**
     * Returns the major version.
     *
     * @return the major version
     */
    public byte getMinor() {
        return minor;
    }

    /**
     * Returns the minor version.
     *
     * @return the minor version
     */
    public byte getPatch() {
        return patch;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()){
            return false;
        }
        Version that = (Version) o;
        return major == that.major &&
                minor == that.minor &&
                patch == that.patch;
    }

    @Override
    public int hashCode() {
        return Objects.hash(major, minor, patch);
    }

    @Override
    public int compareTo(Version o) {
        return Integer.compare(this.versionNumber, o.versionNumber);
    }

    @Override
    public String toString() {
        return this.version;
    }

    /**
     * Format the given params to api version: protoVersion.major.minor for example 1.1.0
     */
    private static String formatVersion(byte major, byte minor, byte patch) {
        return String.format("%d.%d.%d", major, minor, patch);
    }

    /**
     * Returns the {@code int} value whose byte representation is the given 4 bytes, in big-endian
     * order.
     */
    private static int fromBytes(byte b1, byte b2, byte b3, byte b4) {
        return b1 << 24 | (b2 & 0xFF) << 16 | (b3 & 0xFF) << 8 | (b4 & 0xFF);
    }
}
