package im.telegraph.protocol.model.mtproto;

import im.telegraph.engine.serialization.DataInputStream;
import im.telegraph.engine.serialization.DataOutputStream;

import java.io.IOException;

public class UnsentResponse extends ProtoStruct {

    public static final byte HEADER = (byte) 0x08;

    private long messageId;
    private long responseMessageId;
    private int len;

    public UnsentResponse(long messageId, long responseMessageId, int len) {
        this.messageId = messageId;
        this.responseMessageId = responseMessageId;
        this.len = len;
    }

    public UnsentResponse(DataInputStream stream) throws IOException {
        super(stream);
    }

    public long getMessageId() {
        return messageId;
    }

    public long getResponseMessageId() {
        return responseMessageId;
    }

    public int getLen() {
        return len;
    }

    @Override
    protected byte getHeader() {
        return HEADER;
    }

    @Override
    protected void writeBody(DataOutputStream outputStream) throws IOException {
        outputStream.writeInt64(messageId);
        outputStream.writeInt64(responseMessageId);
        outputStream.writeInt32(len);
    }

    @Override
    protected void readBody(DataInputStream inputStream) throws IOException {
        messageId = inputStream.readInt64();
        responseMessageId = inputStream.readInt64();
        len = inputStream.readInt();
    }

    @Override
    public String toString() {
        return "UnsentResponse[" + messageId + "->" + responseMessageId + "]";
    }
}
