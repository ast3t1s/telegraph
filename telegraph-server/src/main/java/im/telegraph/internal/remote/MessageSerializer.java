package im.telegraph.internal.remote;

import im.telegraph.internal.remote.serializer.Serialization;
import im.telegraph.internal.remote.serializer.SerializationException;
import im.telegraph.internal.remote.serializer.Serializer;
import im.telegraph.util.ThreadLocalCRC32;
import org.reactivestreams.Publisher;
import org.springframework.util.FastByteArrayOutputStream;
import reactor.core.publisher.Mono;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;

/**
 * Helper class capable of converting a type used by {@link RemoteMessage} implementation into a sequence of
 * {@link RemoteMessage} message and vice-versa.
 *
 * @author Ast3t1s
 */
public final class MessageSerializer {

    private MessageSerializer() {
    }

    /**
     * Transform a single {@link RemoteMessage} into a {@link Publisher} with a target type.
     *
     * @param message the message to process
     * @return a reactive response with the target type
     */
    public static Publisher<byte[]> serialize(Serialization serialization, RemoteMessage message) {
        return Mono.fromCallable(() -> {
            try (final FastByteArrayOutputStream byteArrayOutputStream = new FastByteArrayOutputStream()) {
                try (final DataOutputStream outputStream = new DataOutputStream(byteArrayOutputStream)) {
                    outputStream.writeUTF(message.getSender().toString());
                    final Serializer serializer = serialization.findSerializerFor(message.payloadClass());
                    outputStream.writeInt(serializer.serializerId());
                    final byte[] payloadData = serializer.toBinary(message.getPayload());
                    outputStream.writeInt(payloadData.length);
                    outputStream.write(payloadData);
                    outputStream.writeInt(ThreadLocalCRC32.calculateChecksum(payloadData));
                    return byteArrayOutputStream.toByteArray();
                }
            }
        });
    }

    /**
     * Transform a single source into a {@link Publisher} of {@link RemoteMessage} instances.
     *
     * @param source the source element provided by a {@link RemoteMessage}
     * @return a reactive sequence of {@link RemoteMessage} messages
     */
    public static Publisher<RemoteMessage> deserialize(Serialization serialization, byte[] source) {
        return Mono.fromCallable(() -> {
            try (final DataInputStream dataInputStream = new DataInputStream(new ByteArrayInputStream(source))) {
                final var address = dataInputStream.readUTF();
                final var serializerId = dataInputStream.readInt();
                final var payloadSize = dataInputStream.readInt();
                final var payloadData = new byte[payloadSize];
                dataInputStream.read(payloadData);
                final var checksum = dataInputStream.readInt();
                final var calculatedChecksum = ThreadLocalCRC32.calculateChecksum(payloadData);
                if (checksum != calculatedChecksum) {
                    throw new SerializationException("Checksum mismatch expected: " + checksum + " but got: " + checksum);
                }

                return new RemoteMessage(Address.create(address), serialization.deserialize(payloadData, serializerId));
            }
        });
    }
}
