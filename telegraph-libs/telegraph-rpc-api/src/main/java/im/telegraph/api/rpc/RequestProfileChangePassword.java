package im.telegraph.api.rpc;

import static im.telegraph.engine.serialization.StreamUtils.*;

import im.telegraph.engine.serialization.StreamReader;
import im.telegraph.engine.serialization.StreamWriter;
import im.telegraph.engine.serialization.io.IOContext;
import im.telegraph.rpc.base.Request;
import java.io.IOException;
import java.lang.Override;
import java.lang.String;

/**
 * Generated by the Telegraph API Scheme generator. DO NOT EDIT!
 * @author Telegraph Scheme Generator
 */
public class RequestProfileChangePassword extends Request<ResponseVoid> {
    public static final int CLASS_ID = 0xdd;

    protected String oldPassword;

    protected String newPassword;

    public RequestProfileChangePassword() {
    }

    public RequestProfileChangePassword(String oldPassword, String newPassword) {
        this.oldPassword = oldPassword;
        this.newPassword = newPassword;
    }

    public static RequestProfileChangePassword fromBytes(byte[] data) throws IOException {
        return IOContext.fromByteArray(new RequestProfileChangePassword(), data);
    }

    @Override
    public int getHeader() {
        return CLASS_ID;
    }

    public String getOldPassword() {
        return this.oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return this.newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    @Override
    public void serialize(StreamWriter writer) throws IOException {
        if (this.oldPassword == null) {
            throw new IOException();
        }
        writer.writeString(1, this.oldPassword);
        if (this.newPassword == null) {
            throw new IOException();
        }
        writer.writeString(2, this.newPassword);
    }

    @Override
    public void deserialize(StreamReader reader) throws IOException {
        this.oldPassword = reader.readString(1);
        this.newPassword = reader.readString(2);
    }

    @Override
    public String toString() {
        String res = "rpc ChangePassword{";
        res += "}";
        return res;
    }
}
