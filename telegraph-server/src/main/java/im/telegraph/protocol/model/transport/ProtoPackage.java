package im.telegraph.protocol.model.transport;

import im.telegraph.engine.serialization.DataInputStream;
import im.telegraph.engine.serialization.DataOutputStream;
import im.telegraph.protocol.model.mtproto.ProtoMessage;
import im.telegraph.protocol.model.mtproto.ProtoObject;
import im.telegraph.server.MTProtoObject;

import java.io.IOException;

public class ProtoPackage extends MTProtoObject {

	public static final int HEADER = 0x00;

	private long authId;
	private long sessionId;
	private ProtoMessage payload;

	public ProtoPackage(DataInputStream inputStream) throws IOException {
		super(inputStream);
	}

	public ProtoPackage(long authId, long sessionId, ProtoMessage payload) {
		this.authId = authId;
		this.sessionId = sessionId;
		this.payload = payload;
	}

	@Override
	public int getHeader() {
		return HEADER;
	}

	public long getAuthId() {
		return authId;
	}

	public long getSessionId() {
		return sessionId;
	}

	public ProtoMessage getPayload() {
		return payload;
	}

	@Override
	public void writeObject(DataOutputStream outputStream) throws IOException {
		outputStream.writeInt64(authId);
		outputStream.writeInt64(sessionId);
		payload.writeObject(outputStream);
	}

	@Override
	public ProtoObject readObject(DataInputStream inputStream) throws IOException {
		authId = inputStream.readInt64();
		sessionId = inputStream.readInt64();
		payload = new ProtoMessage(inputStream);
		return this;
	}

	@Override
	public String toString() {
		return "ProtoPackage[" + authId + "|" + sessionId + "]";
	}

}
