package im.telegraph.domain.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

public class PushDevice implements Serializable {

    private static final long serialVersionUID = 1L;

    private final Platform platform;
    private final Long authKey;
    private final Boolean enabled;
    private final String token;
    private final LocalDateTime createdAt;
    private final LocalDateTime updatedAt;

    PushDevice(final Platform platform, final Long authKey, final Boolean enabled, final String token,
               final LocalDateTime createdAt, final LocalDateTime updatedAt) {
        this.platform = Objects.requireNonNull(platform);
        this.authKey = Objects.requireNonNull(authKey);
        this.enabled = Objects.requireNonNull(enabled);
        this.token = Objects.requireNonNull(token);
        this.createdAt = Objects.requireNonNull(createdAt);
        this.updatedAt = Objects.requireNonNull(updatedAt);
    }

    public static Builder builder() {
        return new Builder();
    }

    public Builder toBuilder() {
        return new Builder()
                .platform(getPlatform())
                .authId(getAuthKey())
                .enabled(getEnabled())
                .token(getToken())
                .createdAt(getCreatedAt())
                .updatedAt(getUpdatedAt());
    }

    public Platform getPlatform() {
        return platform;
    }

    public Long getAuthKey() {
        return authKey;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public String getToken() {
        return token;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    @Override
    public String toString() {
        return "PushDevice{" +
                ", platform=" + platform +
                ", authKey=" + authKey +
                ", enabled=" + enabled +
                ", token='" + token + '\'' +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                '}';
    }

    public enum Platform {

        APNS("apns"),
        FCM("firebase"),
        UNKNOWN("unknown");

        private final String value;

        Platform(final String value) {
            this.value = value;
        }

        public String getPlatform() {
            return this.value;
        }

        public static Platform of(final String value) {
            return switch (value) {
                case "apns" -> APNS;
                case "firebase" -> FCM;
                default -> UNKNOWN;
            };
        }
    }

    public static class Builder {
        private Platform platform;
        private Long authId;
        private Boolean enabled;
        private String token;
        private LocalDateTime createdAt;
        private LocalDateTime updatedAt;

        protected Builder() {
        }

        public Builder platform(Platform platform) {
            this.platform = platform;
            return this;
        }

        public Builder authId(Long authId) {
            this.authId = authId;
            return this;
        }

        public Builder enabled(Boolean enabled) {
            this.enabled = enabled;
            return this;
        }

        public Builder token(String token) {
            this.token = token;
            return this;
        }

        public Builder createdAt(LocalDateTime createdAt) {
            this.createdAt = createdAt;
            return this;
        }

        public Builder updatedAt(LocalDateTime updatedAt) {
            this.updatedAt = updatedAt;
            return this;
        }

        public PushDevice build() {
            return new PushDevice(platform, authId, enabled, token, createdAt, updatedAt);
        }
    }
}
