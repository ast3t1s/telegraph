package im.telegraph.common.rdbms;

import im.telegraph.common.rdbms.bind.BindOperation;
import io.r2dbc.spi.Parameter;
import io.r2dbc.spi.Parameters;
import io.r2dbc.spi.Statement;
import org.springframework.util.CollectionUtils;
import reactor.util.annotation.Nullable;

import java.util.*;
import java.util.function.Supplier;

/**
 * @author Ast3t1s
 */
public abstract class StatementSpec<This extends StatementSpec<This>> {

    private final Handle handle;
    private final String sql;

    private final Map<Integer, Parameter> byIndex = new LinkedHashMap<>();
    private final Map<String, Parameter> byName = new LinkedHashMap<>();

    StatementSpec(Handle handle, String sql) {
        this.handle = handle;
        this.sql = sql;
    }

    /**
     * Returns the un-translated SQL used to create this statement.
     *
     * @return the un-translated SQL used to create this statement.
     */
    protected String getSql() {
        return sql;
    }

    /**
     * Binds named parameters from a map of String to Object instances
     *
     * @param map map where keys are matched to named parameters in order to bind arguments.
     * Can be null, in which case the binding has no effect.
     *
     * @return modified statement
     */
    @SuppressWarnings("unchecked")
    public This bindMap(Map<String, ?> map) {
        if (map != null) {
            map.forEach(this::bind);
        }
        return (This) this;
    }

    /**
     * Bind a value.
     *
     * @param identifier the identifier to bind to
     * @param value      the value to bind
     * @return this {@link Statement}
     */
    @SuppressWarnings("unchecked")
    public This bind(String identifier, Object value) {
        Objects.requireNonNull(identifier);
        Objects.requireNonNull(value);
        byName.put(identifier, resolveParameter(value));
        return (This) this;
    }

    /**
     * Bind a value.
     *
     * @param index the index to bind to
     * @param value the value to bind
     * @return this {@link Statement}
     */
    @SuppressWarnings("unchecked")
    public This bind(int index, Object value) {
        Objects.requireNonNull(value);
        byIndex.put(index, resolveParameter(value));
        return (This) this;
    }

    /**
     * Bind a {@code null} value.
     *
     * @param identifier the identifier to bind to
     * @param type       the type of null value
     * @return this {@link Statement}
     */
    @SuppressWarnings("unchecked")
    public This bindNull(String identifier, Class<?> type) {
        Objects.requireNonNull(identifier);
        Objects.requireNonNull(type);
        byName.put(identifier, Parameters.in(type));
        return (This) this;
    }

    /**
     * Bind a {@code null} value.
     *
     * @param index the index to bind to
     * @param type  the type of null value
     * @return this {@link Statement}
     */
    @SuppressWarnings("unchecked")
    public This bindNull(int index, Class<?> type) {
        Objects.requireNonNull(type);
        byIndex.put(index, Parameters.in(type));
        return (This) this;
    }

    private Parameter resolveParameter(Object value) {
        if (value instanceof Parameter) {
            return (Parameter) value;
        } else {
            return Parameters.in(value);
        }
    }

    protected Statement prepareStatement(Supplier<String> query) {
        final Map<String, Parameter> remainderByName = new LinkedHashMap<>(byName);
        final Map<Integer, Parameter> remainderByIndex = new LinkedHashMap<>(byIndex);

        List<String> parameterNames = handle.getExpander().getParameterNames(query.get());
        Map<String, Parameter> namedBindings = retrieveParameters(query.get(), parameterNames, remainderByName,
                remainderByIndex);

        NamedParameterUtils.ExpandedQuery operation = handle.getExpander().expand(query.get(), handle.getBindMarkers(),
                namedBindings);

        Statement statement = handle.getConnection().createStatement(operation.toQuery());
        BindOperation bindOperation = new StatementWrapper(statement);

        operation.bindTo(bindOperation);

        bindByName(statement, remainderByName);
        bindByIndex(statement, remainderByIndex);

        return statement;
    }

    private void bindByName(Statement statement, Map<String, Parameter> byName) {
        byName.forEach(statement::bind);
    }

    private void bindByIndex(Statement statement, Map<Integer, Parameter> byIndex) {
        byIndex.forEach(statement::bind);
    }

    private Map<String, Parameter> retrieveParameters(String sql, List<String> parameterNames,
                                                      Map<String, Parameter> remainderByName,
                                                      Map<Integer, Parameter> remainderByIndex) {
        Map<String, Parameter> namedBindings = CollectionUtils.newLinkedHashMap(parameterNames.size());
        for (String parameterName : parameterNames) {
            Parameter parameter = getParameter(remainderByName, remainderByIndex, parameterNames, parameterName);
            if (parameter == null) {
                throw new IllegalStateException(
                        String.format("No parameter specified for [%s] in query [%s]", parameterName, sql));
            }
            namedBindings.put(parameterName, parameter);
        }
        return Collections.unmodifiableMap(namedBindings);
    }

    @Nullable
    private Parameter getParameter(Map<String, Parameter> remainderByName, Map<Integer, Parameter> remainderByIndex,
                                   List<String> parameterNames, String parameterName) {
        if (byName.containsKey(parameterName)) {
            remainderByName.remove(parameterName);
            return byName.get(parameterName);
        }

        int index = parameterNames.indexOf(parameterName);
        if (byIndex.containsKey(index)) {
            remainderByIndex.remove(index);
            return byIndex.get(index);
        }

        return null;
    }

    static class StatementWrapper implements BindOperation {

        final Statement statement;

        StatementWrapper(Statement statement) {
            this.statement = statement;
        }

        @Override
        public void bind(String identifier, Object value) {
            statement.bind(identifier, value);
        }

        @Override
        public void bind(int index, Object value) {
            statement.bind(index, value);
        }

        @Override
        public void bindNull(String identifier, Class<?> type) {
            statement.bindNull(identifier, type);
        }

        @Override
        public void bindNull(int index, Class<?> type) {
            statement.bindNull(index, type);
        }
    }
}
