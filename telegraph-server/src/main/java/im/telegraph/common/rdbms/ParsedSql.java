package im.telegraph.common.rdbms;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author Ast3t1s
 */
public final class ParsedSql {

    private final String originalSql;
    private final List<String> parameterNames;
    private final List<int[]> parameterIndexes;

    /**
     * Create a Parsed sql query
     *
     * @param builder a builder used to configure this object
     */
    protected ParsedSql(final Builder builder) {
        this.originalSql = Objects.requireNonNull(builder.originalSql);
        this.parameterNames = builder.parameterNames;
        this.parameterIndexes = builder.parameterIndexes;
    }

    /**
     * Create a builder to create an {@link ParsedSql} using the given sql query.
     *
     * @param originalSql the original sql query to parse
     * @return a {@link Builder}
     */
    public static Builder builder(final String originalSql) {
        return new Builder(originalSql);
    }

    /**
     * Return the SQL statement that is being parsed.
     */
    public String getOriginalSql() {
        return originalSql;
    }

    /**
     * Return all the parameters (bind variables) in the parsed SQL statement.
     * Repeated occurrences of the same parameter name are included here.
     */
    public List<String> getParameterNames() {
        return parameterNames;
    }

    /**
     * Return the parameter indexes for the specified parameter.
     *
     * @param parameterPosition the position of the parameter (as index in the parameter names List)
     * @return the start index and end index, combined into an int array of length 2
     */
    public int[] getParameterIndexes(int parameterPosition) {
        return parameterIndexes.get(parameterPosition);
    }

    @Override
    public String toString() {
        return originalSql;
    }

    /**
     * Builder class for {@link ParsedSql}
     */
    public static class Builder {

        private final String originalSql;
        private final List<String> parameterNames = new ArrayList<>();
        private final List<int[]> parameterIndexes = new ArrayList<>();

        protected Builder(String originalSql) {
            this.originalSql = originalSql;
        }

        /**
         * Adds a named parameter to the SQL query.
         *
         * @param parameterName the name of the parameter to add
         * @param startIndex the start index of the parameter in the SQL string
         * @param endIndex the end index of the parameter in the SQL string
         * @return the Builder instance for method chaining
         */
        public Builder addNamedParameter(String parameterName, int startIndex, int endIndex) {
            this.parameterNames.add(parameterName); // Add the parameter name to the list
            this.parameterIndexes.add(new int[] {startIndex, endIndex}); // Add the parameter indexes to the list
            return this;
        }

        /**
         * Builds the {@link ParsedSql} object from the Builder.
         *
         * @return a new instance of ParsedSql initialized with the Builder's data
         */
        public ParsedSql build() {
            return new ParsedSql(this);
        }
    }
}