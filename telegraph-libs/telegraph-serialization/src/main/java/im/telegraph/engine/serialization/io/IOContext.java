/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package im.telegraph.engine.serialization.io;

import im.telegraph.engine.serialization.DataInputStream;
import im.telegraph.engine.serialization.DataOutputStream;
import im.telegraph.engine.serialization.StreamSerializable;
import im.telegraph.engine.serialization.StreamWriter;
import im.telegraph.engine.serialization.stream.BinaryInputStream;
import im.telegraph.engine.serialization.stream.BinaryOutputStream;

import java.io.IOException;

public final class IOContext {

	public static <T extends StreamSerializable> T fromByteArray(T res, byte[] data) throws IOException {
		return fromByteArray(res, new BinaryInputStream(data, 0, data.length));
	}

	public static <T extends StreamSerializable> T fromByteArray(T res, DataInputStream dataInput) throws IOException {
		BinaryStreamReader reader = new BinaryStreamReader(dataInput);
		res.deserialize(reader);
		return res;
	}

	public static byte[] toByteArray(StreamSerializable serializable) {
		DataOutputStream outputStream = new BinaryOutputStream();
		StreamWriter streamWriter = new BinaryStreamWriter(outputStream);
		try {
			serializable.serialize(streamWriter);
		}
		catch (IOException e) {
			throw new RuntimeException("Unexpected IO exception");
		}
		return outputStream.toByteArray();
	}

}
