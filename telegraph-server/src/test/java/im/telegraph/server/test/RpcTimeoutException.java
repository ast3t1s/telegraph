package im.telegraph.server.test;

public class RpcTimeoutException extends RpcException {

    public RpcTimeoutException() {
        super("TIMEOUT", 500, "Request timeout");
    }

}
