package im.telegraph.common.property.cluster;

import im.telegraph.common.property.cluster.transport.RemoteTransportProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.boot.convert.DurationUnit;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.*;

/**
 * @author Ast3t1s
 */
@Getter
@Setter
@Accessors(chain = true)
public class ClusterProperties {

    /**
     * Flag whether or not clustering is enabled.
     */
    private boolean enabled;

    /**
     * Comparable version information. The typical convention is to use 3 digit version numbers major.minor.patch,
     * but 1 or two digits are also supported.
     */
    private String version;

    /**
     * The list of seed nodes
     */
    private List<String> seedNodes = new ArrayList<>();

    /**
     * The set of the cluster roles.
     */
    private Set<String> roles = new LinkedHashSet<>();

    /**
     * Fanout of gossip
     */
    private int gossipFanout = 2;

    /**
     * how often should the node send out gossip information?
     */
    @DurationUnit(value = ChronoUnit.SECONDS)
    private Duration gossipInterval = Duration.ofSeconds(1);

    /**
     * The number of nodes info gossip info contains
     */
    private int gossipRepeatMult = 3;

    /**
     * How often should the node move nodes, marked as unreachable by the failure detector, out of membership ring.
     *
     */
    private Duration unreachableNodesReaperInterval = Duration.ofMillis(3_000);

    /**3_000
     * Time tick for sync members.
     */
    private Duration syncInterval = Duration.ofMillis(30_000);

    /**
     * Sync timeout.
     */
    private Duration syncTimeout = Duration.ofMillis(3_000);

    /**
     * Remote transport settings.
     */
    @NestedConfigurationProperty
    private final RemoteTransportProperties transport = new RemoteTransportProperties();

    /**
     * Failure detector settings.
     */
    @NestedConfigurationProperty
    private final FailureDetectorProperties failureDetector = new FailureDetectorProperties();

} 
