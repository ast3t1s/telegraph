package im.telegraph.protocol.model.mtproto.auth;

import im.telegraph.engine.serialization.DataInputStream;
import im.telegraph.engine.serialization.DataOutputStream;
import im.telegraph.protocol.model.mtproto.ProtoStruct;

import java.io.IOException;

public class ResponseStartAuth extends ProtoStruct {

	public static final int HEADER = 0xE1;

	private long randomId;
	private long[] availableKeys;
	private byte[] serverNonce;

	public ResponseStartAuth(DataInputStream inputStream) throws IOException {
		super(inputStream);
	}

	public ResponseStartAuth(long randomId, long[] availableKeys, byte[] serverNonce) {
		this.randomId = randomId;
		this.availableKeys = availableKeys;
		this.serverNonce = serverNonce;
	}

	public long getRandomId() {
		return randomId;
	}

	public long[] getAvailableKeys() {
		return availableKeys;
	}

	public byte[] getServerNonce() {
		return serverNonce;
	}

	@Override
	protected byte getHeader() {
		return (byte) HEADER;
	}

	@Override
	protected void writeBody(DataOutputStream outputStream) throws IOException {
		outputStream.writeInt64(randomId);
		outputStream.writeHeader(availableKeys.length);
		for (long key : availableKeys) {
			outputStream.writeInt64(key);
		}
		outputStream.writeProtoBytes(serverNonce);
	}

	@Override
	protected void readBody(DataInputStream inputStream) throws IOException {
		this.randomId = inputStream.readInt64();
		long len = inputStream.readHeader();
		this.availableKeys = new long[(int) len];
		for (int i = 0; i < len; i++) {
			availableKeys[i] = inputStream.readInt64();
		}
		this.serverNonce = inputStream.readProtoBytes();
	}

}
