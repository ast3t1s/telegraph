package im.telegraph.server;

import im.telegraph.TelegraphApplication;
import im.telegraph.api.ApiUser;
import im.telegraph.api.ApiUserFull;
import im.telegraph.api.rpc.*;
import im.telegraph.domain.repository.AuthKeyRepository;
import im.telegraph.domain.repository.UserRepository;
import im.telegraph.server.test.api.ProtoApi;
import im.telegraph.server.test.auth.Authorizer;
import im.telegraph.server.test.auth.PqAuth;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.ZoneId;
import java.util.Locale;
import java.util.concurrent.ThreadLocalRandom;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {TelegraphApplication.class})
class UserRpcHandlerTest {

    private ProtoApi testApi;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthKeyRepository authKeyRepository;

    @BeforeEach
    public void setUp() throws Exception{
       /* Authorizer authorizer = new Authorizer("127.0.0.1", 443);
        PqAuth pqAuth = authorizer.authAttempt();
        testApi = new ProtoApi(pqAuth.getAuthKey(), pqAuth.getMasterKey());*/
    }

    @AfterEach
    public void tearDown() {
     /*   if (testApi != null) {
            testApi.close();
        }*/
    }

    @RepeatedTest(1)
    public void test_check_nick_is_busy() throws Exception {
        Authorizer authorizer = new Authorizer();
        PqAuth pqAuth = authorizer.doAuth("127.0.0.1", 443);
        ProtoApi testApi = new ProtoApi(pqAuth.getAuthKey(), pqAuth.getMasterKey());
       // testApi.clearKey();
      //  authKeyRepository.deleteById(pqAuth.getAuthKey()).block();
        ResponseBool response = testApi.doRpcCall(new RequestProfileCheckNickName("none"));
        assertThat(response.value()).isFalse();
    }

    @RepeatedTest(1)
    public void test_check_email_is_busy() throws Exception {
        Authorizer authorizer = new Authorizer();
        PqAuth pqAuth = authorizer.doAuth("127.0.0.1", 443);
        ProtoApi testApi = new ProtoApi(pqAuth.getAuthKey(), pqAuth.getMasterKey());
        ResponseBool response = testApi.doRpcCall(new RequestProfileCheckEmail("test"));
        assertThat(response.value()).isFalse();
        ResponseBool response2 = testApi.doRpcCall(new RequestProfileCheckEmail("test1"));
        assertThat(response2.value()).isFalse();
       // testApi.close();
    }

    @Test
    public void test_register_user() throws Exception {
        Authorizer authorizer = new Authorizer();
        PqAuth pqAuth = authorizer.doAuth("127.0.0.1", 443);
        ProtoApi testApi = new ProtoApi(pqAuth.getAuthKey(), pqAuth.getMasterKey());
        byte[] deviceHash = new byte[32];
        ThreadLocalRandom.current().nextBytes(deviceHash);
        ResponseAuth responseAuth = testApi.doRpcCall(new RequestAuthSignUp("testuser7",  "some7@gmail.com",
                "password", 1, "My device", deviceHash, ZoneId.systemDefault().toString(), Locale.US.getCountry(),
                "Test name"));
        assertThat(((ApiUser) responseAuth.getUser()).getNick()).isEqualTo("testuser7");
        assertThat(((ApiUser) responseAuth.getUser()).getName()).isEqualTo("Test name");
    }

    @Test
    public void test_sign_in() throws Exception {
        Authorizer authorizer = new Authorizer();
        PqAuth pqAuth = authorizer.doAuth("127.0.0.1", 443);
        ProtoApi testApi = new ProtoApi(pqAuth.getAuthKey(), pqAuth.getMasterKey());
        byte[] deviceHash = new byte[32];
        ThreadLocalRandom.current().nextBytes(deviceHash);
        ResponseAuth responseAuth = testApi.doRpcCall(new RequestAuthSignIn("testuser7", "password",
                "Device title", deviceHash, 1));
        assertThat(((ApiUser) responseAuth.getUser()).getNick()).isEqualTo("testuser7");
    }

    @Test
    public void test_get_sessions() throws Exception {
        Authorizer authorizer = new Authorizer();
        PqAuth pqAuth = authorizer.doAuth("127.0.0.1", 443);
        ProtoApi testApi = new ProtoApi(pqAuth.getAuthKey(), pqAuth.getMasterKey());
        byte[] deviceHash = new byte[32];
        ThreadLocalRandom.current().nextBytes(deviceHash);
        ResponseAuth responseAuth = testApi.doRpcCall(new RequestAuthSignIn("testuser4", "password",
                "Device title", deviceHash,1));
        assertThat(((ApiUser) responseAuth.getUser()).getNick()).isEqualTo("testuser4");

        ResponseAuthGetAuthSessions responseGetAuthSessions = testApi.doRpcCall(new RequestAuthGetAuthSessions());
        assertThat(responseGetAuthSessions.getUserAuths()).isNotEmpty();
    }

    @Test
    public void test_get_privacyList() throws Exception {
        Authorizer authorizer = new Authorizer();
        PqAuth pqAuth = authorizer.doAuth("127.0.0.1", 443);
        ProtoApi testApi = new ProtoApi(pqAuth.getAuthKey(), pqAuth.getMasterKey());
        byte[] deviceHash = new byte[32];
        ThreadLocalRandom.current().nextBytes(deviceHash);
        ResponseAuth responseAuth = testApi.doRpcCall(new RequestAuthSignIn("testuser4", "password",
                "Device title", deviceHash,1));
        assertThat(((ApiUser) responseAuth.getUser()).getNick()).isEqualTo("testuser4");

        ResponsePrivacyLoadBlockedUsers blockedUsers = testApi.doRpcCall(new RequestPrivacyLoadBlockedUsers(0, 20));
        ResponsePrivacyLoadBlockedUsers blockedUsers1 = testApi.doRpcCall(new RequestPrivacyLoadBlockedUsers(0, 20));
        ResponsePrivacyLoadBlockedUsers blockedUsers2 = testApi.doRpcCall(new RequestPrivacyLoadBlockedUsers(0, 20));


        System.out.println("Blocked size " + blockedUsers.getCount());
        System.out.println("Blocked users" + blockedUsers.getUsers().size());

    }

    @RepeatedTest(5)
    public void test_get_diference() throws Exception {
        Authorizer authorizer = new Authorizer();
        PqAuth pqAuth = authorizer.doAuth("127.0.0.1", 443);
        ProtoApi testApi = new ProtoApi(pqAuth.getAuthKey(), pqAuth.getMasterKey());
        byte[] deviceHash = new byte[32];
        ThreadLocalRandom.current().nextBytes(deviceHash);
        ResponseAuth responseAuth = testApi.doRpcCall(new RequestAuthSignIn("testuser4", "password",
                "Device title", deviceHash,1));
        assertThat(((ApiUser) responseAuth.getUser()).getNick()).isEqualTo("testuser4");

        ResponseSequenceGetDifference difference = testApi.doRpcCall(new RequestSequenceGetDifference(1));

        System.out.println("Difference seq " + difference.getSeq());
        System.out.println("Difference users" + difference.getUsers().size());

        difference.getUsers().forEach(System.out::println);
        difference.getUpdates().forEach(System.out::println);

    }

    @Test
    public void test_sign_out() throws Exception {
        Authorizer authorizer = new Authorizer();
        PqAuth pqAuth = authorizer.doAuth("127.0.0.1", 443);
        ProtoApi testApi = new ProtoApi(pqAuth.getAuthKey(), pqAuth.getMasterKey());

        byte[] deviceHash = new byte[32];
        ThreadLocalRandom.current().nextBytes(deviceHash);
        ResponseAuth responseAuth = testApi
                .doRpcCall(new RequestAuthSignIn("testuser", "password", "Device title", deviceHash, 1));
        assertThat(((ApiUser) responseAuth.getUser()).getNick()).isEqualTo("testuser");

        ResponseVoid response = testApi.doRpcCall(new RequestAuthSignOut());
        assertThat(response).isNotNull();
    }

    @RepeatedTest(value = 15)
   // @Test
    public void test_get_user_profile() throws Exception {
        Authorizer authorizer = new Authorizer();
        PqAuth pqAuth = authorizer.doAuth("127.0.0.1", 443);
        ProtoApi testApi = new ProtoApi(pqAuth.getAuthKey(), pqAuth.getMasterKey());

        byte[] deviceHash = new byte[32];
        ThreadLocalRandom.current().nextBytes(deviceHash);
        ResponseAuth responseAuth = testApi.doRpcCall(new RequestAuthSignIn("testuser4", "password",
                "Device title",  deviceHash, 1));
        assertThat(((ApiUser) responseAuth.getUser()).getNick()).isEqualTo("testuser4");
        ResponseProfileGetUserSelf responseUserProfile = testApi
                .doRpcCall(new RequestProfileGetUserSelf());
        assertThat(((ApiUserFull) responseUserProfile.getUser()).getNick()).isEqualTo("testuser4");

        ResponseProfileGetUserSelf responseUserProfile1 = testApi
                .doRpcCall(new RequestProfileGetUserSelf());
        assertThat(((ApiUserFull) responseUserProfile1.getUser()).getNick()).isEqualTo("testuser4");

      /*  ResponseGetFullUser responseUserProfile2 = testApi
                .doRpcCall(new RequestGetFullUser(new ApiUserOutPeer(((ApiUser) responseAuth.getUser()).getId(), 0L)));
        assertThat(((ApiUser) responseAuth.getUser()).getNick()).isEqualTo("testuser");*/
    }

    @Test
    public void test_update_user_display_name() throws Exception {
        Authorizer authorizer = new Authorizer();
        PqAuth pqAuth = authorizer.doAuth("127.0.0.1", 443);
        ProtoApi testApi = new ProtoApi(pqAuth.getAuthKey(), pqAuth.getMasterKey());

        byte[] deviceHash = new byte[32];
        ThreadLocalRandom.current().nextBytes(deviceHash);
        ResponseAuth responseAuth = testApi.doRpcCall(new RequestAuthSignIn("testuser", "password",
                "Device title", deviceHash,1));
        assertThat(((ApiUser) responseAuth.getUser()).getNick()).isEqualTo("testuser");
       // ResponseGetFullUser responseUserProfile = testApi.doRpcCall(new RequestEditName("New Name"));
        assertThat(((ApiUser) responseAuth.getUser()).getNick()).isEqualTo("testuser");
        assertThat(((ApiUser) responseAuth.getUser()).getName()).isEqualTo("New Name");
    }

    @Test
    public void test_update_user_about() throws Exception {
        Authorizer authorizer = new Authorizer();
        PqAuth pqAuth = authorizer.doAuth("127.0.0.1", 443);
        ProtoApi testApi = new ProtoApi(pqAuth.getAuthKey(), pqAuth.getMasterKey());

        byte[] deviceHash = new byte[32];
        ThreadLocalRandom.current().nextBytes(deviceHash);
        ResponseAuth responseAuth = testApi.doRpcCall(new RequestAuthSignIn("testuser", "password", "Device title",
                deviceHash, 1));
        assertThat(((ApiUser) responseAuth.getUser()).getNick()).isEqualTo("testuser");
  //      ResponseGetFullUser responseUserProfile = testApi.doRpcCall(new RequestEditAbout("Some bio"));
        assertThat(((ApiUser) responseAuth.getUser()).getNick()).isEqualTo("testuser");
        assertThat(((ApiUser) responseAuth.getUser()).getAbout()).isEqualTo("Some bio");
    }

    @Test
    public void test_update_user_nick() throws Exception {
        Authorizer authorizer = new Authorizer();
        PqAuth pqAuth = authorizer.doAuth("127.0.0.1", 443);
        ProtoApi testApi = new ProtoApi(pqAuth.getAuthKey(), pqAuth.getMasterKey());

        byte[] deviceHash = new byte[32];
        ThreadLocalRandom.current().nextBytes(deviceHash);
        ResponseAuth responseAuth = testApi.doRpcCall(new RequestAuthSignIn("testuser", "password", "Device title",
                deviceHash, 1));
        assertThat(((ApiUser) responseAuth.getUser()).getNick()).isEqualTo("testuser");
//        ResponseGetFullUser response = testApi.doRpcCall(new RequestGet("NewNick"));
        assertThat(((ApiUser) responseAuth.getUser()).getNick()).isEqualTo("NewNick");
    }
}