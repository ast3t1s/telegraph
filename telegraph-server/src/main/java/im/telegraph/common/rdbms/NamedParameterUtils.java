package im.telegraph.common.rdbms;

import im.telegraph.common.rdbms.bind.BindMarker;
import im.telegraph.common.rdbms.bind.BindMarkers;
import im.telegraph.common.rdbms.bind.BindMarkersFactory;
import im.telegraph.common.rdbms.bind.BindOperation;
import io.r2dbc.spi.Parameter;
import org.springframework.util.Assert;
import reactor.util.annotation.Nullable;

import java.util.*;

/**
 * @author Ast3t1s
 */
public abstract class NamedParameterUtils {

    /**
     * Set of characters that qualify as comment or quote starting characters.
     */
    private static final String[] START_SKIP = {"'", "\"", "--", "/*", "`"};

    /**
     * Set of characters that are the corresponding comment or quote ending characters.
     */
    private static final String[] STOP_SKIP = {"'", "\"", "\n", "*/", "`"};

    /**
     * Set of characters that qualify as parameter separators,
     * indicating that a parameter name in an SQL String has ended.
     */
    private static final String PARAMETER_SEPARATORS = "\"':&,;()|=+-*%/\\<>^";

    /**
     * An index with separator flags per character code.
     * Technically only needed between 34 and 124 at this point.
     */
    private static final boolean[] separatorIndex = new boolean[128];

    static {
        for (char c : PARAMETER_SEPARATORS.toCharArray()) {
            separatorIndex[c] = true;
        }
    }

    /**
     * Parse the SQL statement and locate any placeholders or named parameters.
     * Named parameters are substituted for an R2DBC placeholder.
     * @param sql the SQL statement
     * @return the parsed statement, represented as {@link ParsedSql} instance
     */
    public static ParsedSql parseSqlStatement(String sql) {
        Set<String> namedParameters = new HashSet<>();
        StringBuilder sqlToUse = new StringBuilder(sql);
        List<ParameterHolder> parameterList = new ArrayList<>();

        char[] statement = sql.toCharArray();
        int namedParameterCount = 0;
        int totalParameterCount = 0;

        int escapes = 0;
        int i = 0;
        while (i < statement.length) {
            int skipToPosition = i;
            while (i < statement.length) {
                skipToPosition = skipCommentsAndQuotes(statement, i);
                if (i == skipToPosition) {
                    break;
                } else {
                    i = skipToPosition;
                }
            }
            if (i >= statement.length) {
                break;
            }
            char c = statement[i];
            if (c == ':' || c == '&') {
                int j = i + 1;
                if (c == ':' && j < statement.length && statement[j] == ':') {
                    // Postgres-style "::" casting operator should be skipped
                    i = i + 2;
                    continue;
                }
                String parameter = null;
                if (c == ':' && j < statement.length && statement[j] == '{') {
                    // :{x} style parameter
                    while (statement[j] != '}') {
                        j++;
                        if (j >= statement.length) {
                            throw new IllegalStateException(
                                    "Non-terminated named parameter declaration at position " + i +
                                            " in statement: " + sql);
                        }
                        if (statement[j] == ':' || statement[j] == '{') {
                            throw new IllegalStateException(
                                    "Parameter name contains invalid character '" + statement[j] +
                                            "' at position " + i + " in statement: " + sql);
                        }
                    }
                    if (j - i > 2) {
                        parameter = sql.substring(i + 2, j);
                        namedParameterCount = addNewNamedParameter(
                                namedParameters, namedParameterCount, parameter);
                        totalParameterCount = addNamedParameter(
                                parameterList, totalParameterCount, escapes, i, j + 1, parameter);
                    }
                    j++;
                }
                else {
                    boolean paramWithSquareBrackets = false;
                    while (j < statement.length) {
                        c = statement[j];
                        if (isParameterSeparator(c)) {
                            break;
                        }
                        if (c == '[') {
                            paramWithSquareBrackets = true;
                        } else if (c == ']') {
                            if (!paramWithSquareBrackets) {
                                break;
                            }
                            paramWithSquareBrackets = false;
                        }
                        j++;
                    }
                    if (j - i > 1) {
                        parameter = sql.substring(i + 1, j);
                        namedParameterCount = addNewNamedParameter(
                                namedParameters, namedParameterCount, parameter);
                        totalParameterCount = addNamedParameter(
                                parameterList, totalParameterCount, escapes, i, j, parameter);
                    }
                }
                i = j - 1;
            } else {
                if (c == '\\') {
                    int j = i + 1;
                    if (j < statement.length && statement[j] == ':') {
                        // escaped ":" should be skipped
                        sqlToUse.deleteCharAt(i - escapes);
                        escapes++;
                        i = i + 2;
                        continue;
                    }
                }
            }
            i++;
        }

        ParsedSql.Builder builder = ParsedSql.builder(sqlToUse.toString());
        for (ParameterHolder ph : parameterList) {
            builder.addNamedParameter(ph.getParameterName(), ph.getStartIndex(), ph.getEndIndex());
        }
        return builder.build();
    }

    private static int addNamedParameter(List<ParameterHolder> parameterList,
                                         int totalParameterCount, int escapes, int i, int j, String parameter) {
        parameterList.add(new ParameterHolder(parameter, i - escapes, j - escapes));
        totalParameterCount++;
        return totalParameterCount;
    }

    private static int addNewNamedParameter(Set<String> namedParameters, int namedParameterCount, String parameter) {
        if (!namedParameters.contains(parameter)) {
            namedParameters.add(parameter);
            namedParameterCount++;
        }
        return namedParameterCount;
    }

    public static ExpandedQuery substituteNamedParameters(ParsedSql parsedSql,
                                                          BindMarkersFactory bindMarkersFactory,
                                                          Map<String, Parameter> paramSource) {
        NamedParameters markerHolder = new NamedParameters(bindMarkersFactory);
        String originalSql = parsedSql.getOriginalSql();
        List<String> paramNames = parsedSql.getParameterNames();
        if (paramNames.isEmpty()) {
            return new ExpandedQuery(originalSql, markerHolder, paramSource);
        }

        StringBuilder actualSql = new StringBuilder(originalSql.length());
        int lastIndex = 0;
        for (int i = 0; i < paramNames.size(); i++) {
            String paramName = paramNames.get(i);
            int[] indexes = parsedSql.getParameterIndexes(i);
            int startIndex = indexes[0];
            int endIndex = indexes[1];
            actualSql.append(originalSql, lastIndex, startIndex);
            NamedParameters.NamedParameter marker = markerHolder.getOrCreate(paramName);
            if (paramSource.containsKey(paramName)) {
                Parameter parameter = Optional.ofNullable(paramSource.get(paramName))
                        .orElseThrow(
                                () -> new IllegalArgumentException(
                                        "No value registered for key '" + paramName + "'"));
                if (parameter.getValue() instanceof Collection<?>) {
                    Collection<?> collection = (Collection<?> ) parameter.getValue();
                    int k = 0;
                    int counter = 0;
                    for (Object entryItem : collection) {
                        if (k > 0) {
                            actualSql.append(", ");
                        }
                        k++;
                        if (entryItem instanceof Object[]) {
                            Object[] expressionList = (Object[]) entryItem;
                            actualSql.append('(');
                            for (int m = 0; m < expressionList.length; m++) {
                                if (m > 0) {
                                    actualSql.append(", ");
                                }
                                actualSql.append(marker.getPlaceholder(counter));
                                counter++;
                            }
                            actualSql.append(')');
                        } else {
                            actualSql.append(marker.getPlaceholder(counter));
                            counter++;
                        }
                    }
                } else {
                    actualSql.append(marker.getPlaceholder());
                }
            } else {
                actualSql.append(marker.getPlaceholder());
            }
            lastIndex = endIndex;
        }
        actualSql.append(originalSql, lastIndex, originalSql.length());

        return new ExpandedQuery(actualSql.toString(), markerHolder, paramSource);
    }

    /**
     * Skip over comments and quoted names present in an SQL statement.
     * @param statement character array containing SQL statement
     * @param position current position of statement
     * @return next position to process after any comments or quotes are skipped
     */
    private static int skipCommentsAndQuotes(char[] statement, int position) {
        for (int i = 0; i < START_SKIP.length; i++) {
            if (statement[position] == START_SKIP[i].charAt(0)) {
                boolean match = true;
                for (int j = 1; j < START_SKIP[i].length(); j++) {
                    if (statement[position + j] != START_SKIP[i].charAt(j)) {
                        match = false;
                        break;
                    }
                }
                if (match) {
                    int offset = START_SKIP[i].length();
                    for (int m = position + offset; m < statement.length; m++) {
                        if (statement[m] == STOP_SKIP[i].charAt(0)) {
                            boolean endMatch = true;
                            int endPos = m;
                            for (int n = 1; n < STOP_SKIP[i].length(); n++) {
                                if (m + n >= statement.length) {
                                    // last comment not closed properly
                                    return statement.length;
                                }
                                if (statement[m + n] != STOP_SKIP[i].charAt(n)) {
                                    endMatch = false;
                                    break;
                                }
                                endPos = m + n;
                            }
                            if (endMatch) {
                                // found character sequence ending comment or quote
                                return endPos + 1;
                            }
                        }
                    }
                    // character sequence ending comment or quote not found
                    return statement.length;
                }
            }
        }
        return position;
    }

    /**
     * Determine whether a parameter name ends at the current position,
     * that is, whether the given character qualifies as a separator.
     */
    private static boolean isParameterSeparator(char c) {
        return (c < 128 && separatorIndex[c]) || Character.isWhitespace(c);
    }

    private static class ParameterHolder {

        private final String parameterName;
        private final int startIndex;
        private final int endIndex;

        ParameterHolder(String parameterName, int startIndex, int endIndex) {
            this.parameterName = Objects.requireNonNull(parameterName);
            this.startIndex = startIndex;
            this.endIndex = endIndex;
        }

        String getParameterName() {
            return parameterName;
        }

        int getStartIndex() {
            return startIndex;
        }

        int getEndIndex() {
            return endIndex;
        }

        @Override
        public boolean equals(Object object) {
            if (this == object) {
                return true;
            }
            if (object == null || getClass() != object.getClass()) {
                return false;
            }
            ParameterHolder that = (ParameterHolder) object;
            return startIndex == that.startIndex &&
                    endIndex == that.endIndex &&
                    Objects.equals(parameterName, that.parameterName);
        }

        @Override
        public int hashCode() {
            return Objects.hash(parameterName);
        }
    }

    /**
     * Holder for bind markers progress.
     */
    static class NamedParameters {

        private final BindMarkers bindMarkers;
        private final boolean identifiable;
        private final Map<String, List<NamedParameter>> references = new TreeMap<>();

        NamedParameters(BindMarkersFactory factory) {
            this.bindMarkers = factory.create();
            this.identifiable = factory.identifiablePlaceholders();
        }

        /**
         * Get the {@link NamedParameter} identified by {@code namedParameter}.
         * Parameter objects get created if they do not yet exist.
         * @param namedParameter the parameter name
         * @return the named parameter
         */
        NamedParameter getOrCreate(String namedParameter) {
            List<NamedParameter> reference = references.computeIfAbsent(namedParameter, key -> new ArrayList<>());
            if (reference.isEmpty()) {
                NamedParameter param = new NamedParameter(namedParameter);
                reference.add(param);
                return param;
            }
            if (identifiable) {
                return reference.get(0);
            }
            NamedParameter param = new NamedParameter(namedParameter);
            reference.add(param);
            return param;
        }

        @Nullable
        List<NamedParameter> getMarker(String name) {
            return references.get(name);
        }

        class NamedParameter {

            private final String namedParameter;
            private final List<BindMarker> placeholders = new ArrayList<>();

            NamedParameter(String namedParameter) {
                this.namedParameter = namedParameter;
            }

            /**
             * Create a placeholder to translate a single value into a bindable parameter.
             * <p>Can be called multiple times to create placeholders for array/collections.
             * @return the placeholder to be used in the SQL statement
             */
            String addPlaceholder() {
                BindMarker bindMarker = NamedParameters.this.bindMarkers.next(this.namedParameter);
                placeholders.add(bindMarker);
                return bindMarker.getPlaceholder();
            }

            String getPlaceholder() {
                return getPlaceholder(0);
            }

            String getPlaceholder(int counter) {
                while (counter + 1 > placeholders.size()) {
                    addPlaceholder();
                }
                return placeholders.get(counter).getPlaceholder();
            }
        }
    }

    public static class ExpandedQuery {

        private final String expandedSql;
        private final NamedParameters parameters;
        private final Map<String, Parameter> parameterSource;

        ExpandedQuery(String expandedSql, NamedParameters parameters, Map<String, Parameter> parameterSource) {
            this.expandedSql = expandedSql;
            this.parameters = parameters;
            this.parameterSource = parameterSource;
        }

        @SuppressWarnings({"rawtypes", "unchecked"})
        public void bind(BindOperation target, String identifier, Parameter parameter) {
            List<BindMarker> bindMarkers = getBindMarkers(identifier);
            if (bindMarkers == null) {
                target.bind(identifier, parameter);
                return;
            }
            if (parameter.getValue() instanceof Collection) {
                Collection collection = (Collection) parameter.getValue();
                Iterator<Object> iterator = collection.iterator();
                Iterator<BindMarker> markers = bindMarkers.iterator();
                while (iterator.hasNext()) {
                    Object valueToBind = iterator.next();
                    if (valueToBind instanceof Object[]) {
                        Object[] objects = (Object[]) valueToBind;
                        for (Object object : objects) {
                            bind(target, markers, object);
                        }
                    } else {
                        bind(target, markers, valueToBind);
                    }
                }
            } else {
                for (BindMarker bindMarker : bindMarkers) {
                    bindMarker.bind(target, parameter);
                }
            }
        }

        private void bind(BindOperation target, Iterator<BindMarker> markers, Object valueToBind) {
            Assert.isTrue(markers.hasNext(), () -> String.format(
                    "No bind marker for value [%s] in SQL [%s]. Check that the query was expanded using the same arguments.",
                    valueToBind, toQuery()));
            markers.next().bind(target, valueToBind);
        }

        public void bindNull(BindOperation target, String identifier, Parameter parameter) {
            List<BindMarker> bindMarkers = getBindMarkers(identifier);
            if (bindMarkers == null) {
                target.bind(identifier, parameter);
                return;
            }
            for (BindMarker bindMarker : bindMarkers) {
                bindMarker.bind(target, parameter);
            }
        }

        @Nullable
        List<BindMarker> getBindMarkers(String identifier) {
            List<NamedParameters.NamedParameter> parameters = this.parameters.getMarker(identifier);
            if (parameters == null) {
                return null;
            }
            List<BindMarker> markers = new ArrayList<>();
            for (NamedParameters.NamedParameter parameter : parameters) {
                markers.addAll(parameter.placeholders);
            }
            return markers;
        }

        public String getSource() {
            return this.expandedSql;
        }

        public void bindTo(BindOperation target) {
            for (String namedParameter : parameterSource.keySet()) {
                Parameter parameter = getValue(namedParameter);
                if (parameter.getValue() == null) {
                    bindNull(target, namedParameter, parameter);
                } else {
                    bind(target, namedParameter, parameter);
                }
            }
        }

        private Parameter getValue(String paramName) throws IllegalArgumentException {
            if (!parameterSource.containsKey(paramName)) {
                throw new IllegalArgumentException("No value registered for key '" + paramName + "'");
            }
            return parameterSource.get(paramName);
        }

        public String toQuery() {
            return expandedSql;
        }
    }
} 
