package im.telegraph.domain.repository.rdbms;

import im.telegraph.domain.repository.rdbms.dialect.Dialect;
import im.telegraph.domain.repository.rdbms.dialect.Dialects;
import io.r2dbc.spi.ConnectionFactory;

import java.util.List;
import java.util.Map;

/**
 * @author Ast3t1s
 */
public abstract class AbstractRdbmsRepository {

    private final Dialect dialect;

    protected AbstractRdbmsRepository(ConnectionFactory connectionFactory) {
        this.dialect = Dialects.getDialect(connectionFactory)
                .orElseThrow(() -> new IllegalStateException(String.format("Cannot determine a dialect for %s using %s; Please provide a Dialect",
                        connectionFactory.getMetadata().getName(), connectionFactory)));
        prepareQuery();
    }

    protected void prepareQuery() {
    }

    protected String limitQuery(String query, int limit) {
        return query.trim() + " " + dialect.getLimit(limit);
    }

    protected String offsetQuery(String query, int offset) {
        return query.trim() + " " + dialect.getOffset(offset);
    }

    protected String limitQuery(String query, int limit, int offset) {
        return query.trim() + " " + dialect.getLimitOffset(limit, offset);
    }

    protected String countQuery(String table, String whereClause) {
        return String.format("SELECT COUNT(*) FROM %TABLE%" + whereClause, "%TABLE%", table);
    }

    protected String prepareUpsert(String table, Map<String, Object> columns, List<String> conflictFields,
                                   List<String> updates, String... incrementsColumns) {
        return dialect.buildUpsertQuery(table, columns, conflictFields, updates, incrementsColumns);
    }

    protected String prepareUpsertMany(String table, String batchName, List<String> insertFields,
                                       List<String> uniqueKeyFields, List<String> updateFields, String... incrementColumns) {
        return dialect.buildUpsertQueryMany(table, insertFields, batchName, uniqueKeyFields, updateFields, incrementColumns);
    }
}
