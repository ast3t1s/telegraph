package im.telegraph.protocol.model.transport;

import im.telegraph.engine.serialization.DataInputStream;

import java.io.IOException;
import java.util.Arrays;

/**
 * @author Ast3t1s
 */
public class HandshakeResponse extends Handshake {

    public static final int HEADER = 0xFE;

    public HandshakeResponse(DataInputStream inputStream) throws IOException {
        super(inputStream);
    }

    public HandshakeResponse(int mtprotoVersion, int apiMajorVersion, int apiMinorVersion, byte[] handshakeData) {
        super(mtprotoVersion, apiMajorVersion, apiMinorVersion, handshakeData);
    }

    @Override
    public int getHeader() {
        return HEADER;
    }

    @Override
    public String toString() {
        return "HandshakeResponse{" +
                "mtprotoVersion=" + getMtprotoVersion() +
                ", apiMajorVersion=" + getApiMajorVersion() +
                ", apiMinorVersion=" + getApiMinorVersion() +
                ", handshakeData=" + Arrays.toString(getHandshakeData()) +
                '}';
    }
}
