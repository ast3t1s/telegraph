package im.telegraph.modules.privacy;

import im.telegraph.common.DateTimeFactory;
import im.telegraph.common.StateHolder;
import im.telegraph.domain.entity.Id;
import im.telegraph.domain.entity.Privacy;
import im.telegraph.domain.repository.PrivacyRepository;
import im.telegraph.exceptions.RpcException;
import im.telegraph.service.PrivacyService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PrivacyServiceTest {

    @Mock
    private PrivacyRepository privacyRepository;

    private PrivacyService privacyService;

    private DateTimeFactory dateTimeFactory = new DateTimeFactory();

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        this.privacyService = new PrivacyService(privacyRepository, dateTimeFactory);
    }

    @Test
    void createPrivacy_shouldSave() {
        final Id firstUser = Id.of(1);
        final Id secondUser = Id.of(2);

        final Privacy privacy = Privacy.of(firstUser, secondUser, dateTimeFactory.getClock().instant());

        when(this.privacyRepository.findByUserIdAndBlockedId(firstUser, secondUser)).thenReturn(Mono.empty());
        //when(this.privacyRepository.insert(privacy)).thenReturn(Mono.just(privacy));

        StepVerifier.create(this.privacyService.blockUser(firstUser, secondUser))
                .expectSubscription()
                //.expectNext(true)
                .verifyComplete();

        verify(this.privacyRepository, times(1)).findByUserIdAndBlockedId(firstUser, secondUser);
        verify(this.privacyRepository, times(1)).insert(privacy);
        verifyNoMoreInteractions(this.privacyRepository);
    }

    @Test
    void blockUser_alreadyExists() {
        final Id firstUser = Id.of(1);
        final Id secondUser = Id.of(2);

        final Privacy privacy = Privacy.of(firstUser, secondUser, dateTimeFactory.getClock().instant());

        when(this.privacyRepository.findByUserIdAndBlockedId(firstUser, secondUser)).thenReturn(Mono.just(privacy));

        StepVerifier.create(this.privacyService.blockUser(firstUser, secondUser))
                .expectSubscription()
                .expectError(RpcException.class)
                .verify();

        verify(this.privacyRepository, times(1)).findByUserIdAndBlockedId(firstUser, secondUser);
        verify(this.privacyRepository, times(0)).insert(privacy);
        verifyNoMoreInteractions(this.privacyRepository);
    }

    @Test
    void unblockUser_shouldUnblock() {
        final Id firstUser = Id.of(1);
        final Id secondUser = Id.of(2);

        final Privacy privacy = Privacy.of(firstUser, secondUser, dateTimeFactory.getClock().instant());

        when(this.privacyRepository.findByUserIdAndBlockedId(firstUser, secondUser)).thenReturn(Mono.just(privacy));
       // when(this.privacyRepository.d(privacy)).thenReturn(Mono.empty());

        StepVerifier.create(this.privacyService.unblockUser(firstUser, secondUser))
                .expectSubscription()
                //.expectNext(true)
                .verifyComplete();

        verify(this.privacyRepository, times(1)).findByUserIdAndBlockedId(firstUser, secondUser);
       // verify(this.privacyRepository, times(1)).delete(privacy);
        verifyNoMoreInteractions(this.privacyRepository);
    }

    @Test
    void unblockUser_nonExists() {
        final Id firstUser = Id.of(1);
        final Id secondUser = Id.of(2);

        final Privacy privacy = Privacy.of(firstUser, secondUser, dateTimeFactory.getClock().instant());

        when(this.privacyRepository.findByUserIdAndBlockedId(firstUser, secondUser)).thenReturn(Mono.empty());

        StepVerifier.create(this.privacyService.unblockUser(firstUser, secondUser))
                .expectSubscription()
                .expectError(RpcException.class)
                .verify();

        verify(this.privacyRepository, times(1)).findByUserIdAndBlockedId(firstUser, secondUser);
       // verify(this.privacyRepository, times(0)).delete(privacy);
        verifyNoMoreInteractions(this.privacyRepository);
    }

    @Test
    void checkIsBlocked_shouldOk() {
        final Id firstUser = Id.of(1);
        final Id secondUser = Id.of(2);

        final Privacy privacy = Privacy.of(firstUser, secondUser, dateTimeFactory.getClock().instant());

        when(this.privacyRepository.findByUserIdAndBlockedId(firstUser, secondUser)).thenReturn(Mono.just(privacy));

        StepVerifier.create(this.privacyService.isUserBlocked(firstUser, secondUser))
                .expectSubscription()
                .expectNext(true)
                .verifyComplete();

        verify(this.privacyRepository, times(1)).findByUserIdAndBlockedId(firstUser, secondUser);
        verifyNoMoreInteractions(this.privacyRepository);
    }

    @Test
    void test_LoadBlockList_resolve() {
        final Id userId = Id.of(1);
        Flux<Privacy> blocked = Flux.range(1, 50)
                .map(id -> Privacy.of(userId, Id.of(id), dateTimeFactory.getClock().instant()));

      //  when(this.privacyRepository.findAllByUserId(userId, page)).thenReturn(blocked);
      //  when(this.privacyRepository.countByUserId(userId)).thenReturn(Mono.just(50));

        StepVerifier.create(this.privacyService.getBlockList(userId, 20, 0))
                .expectSubscription()
                .assertNext(privacies -> assertThat(privacies.getTotalCount()).isEqualTo(50))
                .verifyComplete();

     //   verify(this.privacyRepository, times(1)).findAllByUserId(userId, page);
        verify(this.privacyRepository, times(1)).countByUserId(userId);
        verifyNoMoreInteractions(this.privacyRepository);
    }
}