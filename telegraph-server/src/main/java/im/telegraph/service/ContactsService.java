package im.telegraph.service;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import im.telegraph.common.DateTimeFactory;
import im.telegraph.domain.entity.Contact;
import im.telegraph.domain.entity.Id;
import im.telegraph.domain.repository.ContactsRepository;
import im.telegraph.exceptions.RpcErrors;
import im.telegraph.exceptions.RpcException;
import im.telegraph.util.PaginationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.function.BiFunction;
import java.util.function.ToLongFunction;

@Service
public class ContactsService {

    private static final Logger log = LoggerFactory.getLogger(ContactsService.class);

    private final ContactsRepository contactsRepository;
    private final UserService userService;
    private final DateTimeFactory dateFactory;

    private final Cache<Id, List<Contact>> cache;

    public ContactsService(ContactsRepository contactsRepository,
                           UserService userService,
                           DateTimeFactory dateFactory) {
        this.contactsRepository = contactsRepository;
        this.userService = userService;
        this.dateFactory = dateFactory;

        this.cache = Caffeine.newBuilder()
                .expireAfterWrite(60, TimeUnit.SECONDS)
                .build();
    }

    /**
     * Adds a user to the contact list or edits existing contact by the their user identifier.
     * It means that you attempt to add user to contact list in case where user approve request
     * you will receive user presence status and allow to send messages.
     *
     * @param userId  User identifier.
     * @param contactId User identifier to add.
     * @return A {@link Mono} where, upon successful completion, emits the {@code boolean} true .If an error is received,
     * it is emitted the {@code boolean} false through the {@code Mono}.
     */
    public Mono<Void> addContact(final Id userId, final Id contactId) {
        Mono<Void> isUserActive = userService.findById(userId)
                .filter(user -> !user.getEnabled() || user.getDeletedAt().isPresent())
                .flatMap(__ -> Mono.error(RpcException.create(RpcErrors.USER_DEACTIVATED)))
                .then();

        Mono<Void> outRequest = contactsRepository.findByUserIdAndContactId(userId, contactId)
                .flatMap(__ -> Mono.error(() -> RpcException.create(RpcErrors.CONTACT_EXISTS)))
                .then(Mono.fromCallable(
                        () -> new Contact(userId, contactId, Contact.Status.RECV_TO, dateFactory.getClock().instant())))
                .flatMap(contactsRepository::insert)
                .then();

        Mono<Void> inRequest = contactsRepository.findByUserIdAndContactId(contactId, userId)
                .flatMap(__ -> Mono.error(() -> RpcException.create(RpcErrors.CONTACT_EXISTS)))
                .then(Mono.fromCallable(
                        () -> new Contact(contactId, userId, Contact.Status.RECV_FROM, dateFactory.getClock().instant())))
                .flatMap(contactsRepository::insert)
                .then();

        return Mono.when(isUserActive, outRequest, inRequest, invalidateCache(userId), invalidateCache(contactId));
    }

    /**
     * Approve incoming subscription request if exists. If user approve subscription request he/she will receive
     * presences and messages.
     *
     * @param userId  the user which send request.
     * @param contactId the user which receive request.
     * @return A {@link Mono} where, upon successful completion, emits the {@code Mono.empty()}.If an error is received,
     * it is emitted through the {@code Mono}.
     */
    public Mono<Void> acceptContact(final Id userId, final Id contactId) {
        Mono<Void> inRequest = contactsRepository.findByUserIdAndContactId(userId, contactId)
                .filter(Contact::isRequestedOut)
                .switchIfEmpty(Mono.error(() -> RpcException.create(RpcErrors.CONTACT_REQUEST_NOT_FOUND)))
                .map(oldState -> oldState.withStatus(Contact.Status.BOTH))
                .flatMap(contactsRepository::update)
                .then();

        Mono<Void> outRequest = contactsRepository.findByUserIdAndContactId(contactId, userId)
                .filter(Contact::isRequestedIn)
                .switchIfEmpty(Mono.error(() -> RpcException.create(RpcErrors.CONTACT_REQUEST_NOT_FOUND)))
                .map(oldState -> oldState.withStatus(Contact.Status.BOTH))
                .flatMap(contactsRepository::update)
                .then();

        return Mono.when(inRequest, outRequest, invalidateCache(userId), invalidateCache(contactId));
    }

    /**
     * Remove subscription request, means you remove user from your relations list and you will not receive
     * any presences or messages from him/her.
     *
     * @param userId  the ID of the user who remove.
     * @param contactId the ID of the user whom remove
     * @return A {@link Mono} where, upon successful completion, emits the {@code Mono.empty()}.If an error is received,
     * it is emitted through the {@code Mono}.
     */
    public Mono<Void> deleteContact(final Id userId, final Id contactId) {
        Mono<Void> userRelation = contactsRepository.findByUserIdAndContactId(userId, contactId)
                .switchIfEmpty(Mono.error(() -> RpcException.create(RpcErrors.CONTACT_NOT_FOUND)))
                .flatMap(contact -> contactsRepository.deleteByUserIdAndContactId(contact.getUserId(), contact.getContactId()))
                .then();

        Mono<Void> related = contactsRepository.findByUserIdAndContactId(contactId, userId)
                .map(oldState -> oldState.withStatus(Contact.Status.REMOVE))
                .flatMap(contactsRepository::update)
                .then();

        return Mono.when(userRelation, related, invalidateCache(userId), invalidateCache(contactId));
    }

    private Mono<Void> invalidateCache(final Id userId) {
        return Mono.justOrEmpty(userId).doOnNext(id -> cache.invalidate(userId)).then();
    }

    /**
     * Return all user contacts including incoming and outgoing subscription requests.
     *
     * @param userId the ID of the user.
     * @return A {@link Mono} where, upon successful completion, emits the {@code Mono.empty()}.If an error is received,
     * it is emitted through the {@code Mono}.
     */
    public Mono<List<Contact>> getContacts(final Id userId) {
        final BiFunction<Integer, Integer, Flux<Contact>> chunk =
                (offset, pageSize) -> contactsRepository.findAllByUserId(userId, pageSize, offset)
                        .doFinally(s -> log.debug("Relations chunk of user: {}", userId));

        final ToLongFunction<Contact> sortedKey = (contact) -> contact.getCreatedAt().toEpochMilli();

        final Mono<List<Contact>> contacts = contactsRepository.countByUserId(userId)
                .flatMapMany(count -> PaginationUtils.loadPageAsc(chunk, sortedKey, 0, count))
                .collectList()
                .doOnNext(toCache -> cache.put(userId, toCache));

        return Mono.justOrEmpty(Optional.ofNullable(cache.getIfPresent(userId))).switchIfEmpty(contacts);
    }
}
