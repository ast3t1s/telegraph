package im.telegraph.admin.object;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Ast3t1s
 */
public class UpdatesStateBean {

    @JsonProperty("seq")
    private final int sequence;
    private final Long timestamp;

    public UpdatesStateBean(int sequence, Long timestamp) {
        this.sequence = sequence;
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "UpdatesState{" +
                "sequence=" + sequence +
                ", timestamp=" + timestamp +
                '}';
    }
}
