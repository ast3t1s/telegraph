package im.telegraph.api.rpc;

import static im.telegraph.engine.serialization.StreamUtils.*;

import im.telegraph.engine.serialization.StreamReader;
import im.telegraph.engine.serialization.StreamWriter;
import im.telegraph.engine.serialization.io.IOContext;
import im.telegraph.rpc.base.Request;
import java.io.IOException;
import java.lang.Override;
import java.lang.String;

/**
 * Generated by the Telegraph API Scheme generator. DO NOT EDIT!
 * @author Telegraph Scheme Generator
 */
public class RequestMessagesLoadDialogs extends Request<ResponseMessagesLoadDialogs> {
    public static final int CLASS_ID = 0x68;

    protected long minDate;

    protected int limit;

    public RequestMessagesLoadDialogs() {
    }

    public RequestMessagesLoadDialogs(long minDate, int limit) {
        this.minDate = minDate;
        this.limit = limit;
    }

    public static RequestMessagesLoadDialogs fromBytes(byte[] data) throws IOException {
        return IOContext.fromByteArray(new RequestMessagesLoadDialogs(), data);
    }

    @Override
    public int getHeader() {
        return CLASS_ID;
    }

    public long getMinDate() {
        return this.minDate;
    }

    public void setMinDate(long minDate) {
        this.minDate = minDate;
    }

    public int getLimit() {
        return this.limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    @Override
    public void serialize(StreamWriter writer) throws IOException {
        writer.writeLong(1, this.minDate);
        writer.writeInt(2, this.limit);
    }

    @Override
    public void deserialize(StreamReader reader) throws IOException {
        this.minDate = reader.readLong(1);
        this.limit = reader.readInt(2);
    }

    @Override
    public String toString() {
        String res = "rpc LoadDialogs{";
        res += "minDate=" + this.minDate;
        res += ", limit=" + this.limit;
        res += "}";
        return res;
    }
}
