package im.telegraph.internal.remote;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Transport is responsible for maintaining p2p connections to/from other transports. It
 * allows to send messages to other transports and listen for incoming messages. Using for s2s, clustering connection.
 *
 * @author Ast3t1s
 */
public interface RemoteTransport {

    /**
     * Return local {@link Address} on which current instance of transport is running on.
     *
     * @return the address of the transport.
     */
    Address address();

    /**
     * Returns flag whether or not transport is running.
     *
     * @return flag whether or not transport is running.
     */
    boolean isRunning();

    /**
     * Start transport. After this call method {@link #address()} shall be eligible for calling.
     *
     * @return started {@code RemoteTransport}
     */
    Mono<RemoteTransport> start();

    /**
     * Sends message to the given address. It will issue connect in case if no transport channel by
     * given transport {@code address} exists already. Send is an async operation.
     *
     * @param address address where message will be sent
     * @param message message to send
     * @return promise which will be completed with result of sending (void or exception)
     * @throws IllegalArgumentException if {@code message} or {@code address} is null
     */
    Mono<Void> send(Address address, Object message);

    /**
     * Returns stream of received messages. For each observers subscribed to the returned observable:
     *
     * @return Observable which emit received messages or complete event when transport is closed
     */
    Flux<RemoteMessage> receive();

    /**
     * Stop transport, disconnect all connections and release all resources which belong to this
     * transport. After transport is stopped it can't be used again. Observable returned from method
     * {@link #receive()} will immediately emit onComplete event for all subscribers.
     */
    Mono<Void> shutdown();
}
