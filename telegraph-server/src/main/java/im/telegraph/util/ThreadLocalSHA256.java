package im.telegraph.util;

import io.netty.util.concurrent.FastThreadLocal;

import java.security.MessageDigest;

public class ThreadLocalSHA256 {

    private static final FastThreadLocal<MessageDigest> LOCAL = new FastThreadLocal<>() {

        @Override
        protected MessageDigest initialValue() throws Exception {
            return MessageDigest.getInstance("SHA-256");
        }
    };

    public static MessageDigest current() {
        return LOCAL.get();
    }

}
