package im.telegraph.api.rpc;

import static im.telegraph.engine.serialization.StreamUtils.*;

import im.telegraph.engine.serialization.StreamReader;
import im.telegraph.engine.serialization.StreamWriter;
import im.telegraph.engine.serialization.io.IOContext;
import im.telegraph.rpc.base.Request;
import java.io.IOException;
import java.lang.Override;
import java.lang.String;

/**
 * Generated by the Telegraph API Scheme generator. DO NOT EDIT!
 * @author Telegraph Scheme Generator
 */
public class RequestSequenceGetDifference extends Request<ResponseSequenceGetDifference> {
    public static final int CLASS_ID = 0xb;

    protected int seq;

    public RequestSequenceGetDifference() {
    }

    public RequestSequenceGetDifference(int seq) {
        this.seq = seq;
    }

    public static RequestSequenceGetDifference fromBytes(byte[] data) throws IOException {
        return IOContext.fromByteArray(new RequestSequenceGetDifference(), data);
    }

    @Override
    public int getHeader() {
        return CLASS_ID;
    }

    public int getSeq() {
        return this.seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    @Override
    public void serialize(StreamWriter writer) throws IOException {
        writer.writeInt(1, this.seq);
    }

    @Override
    public void deserialize(StreamReader reader) throws IOException {
        this.seq = reader.readInt(1);
    }

    @Override
    public String toString() {
        String res = "rpc GetDifference{";
        res += "seq=" + this.seq;
        res += "}";
        return res;
    }
}
