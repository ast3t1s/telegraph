package im.telegraph.cluster;

import im.telegraph.cluster.event.ClusterMembershipEvent;
import im.telegraph.cluster.event.ClusterReachabilityEvent;
import im.telegraph.cluster.internal.*;
import im.telegraph.internal.remote.Address;
import im.telegraph.internal.remote.RemoteMessage;
import im.telegraph.internal.remote.RemoteTransport;
import im.telegraph.common.ResettableInterval;
import im.telegraph.common.property.cluster.ClusterProperties;
import im.telegraph.common.sinks.EmissionStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.Disposable;
import reactor.core.Disposables;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.MonoSink;
import reactor.core.publisher.Sinks;
import reactor.core.scheduler.Scheduler;

import java.net.InetAddress;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import static im.telegraph.cluster.MembershipState.MemberStatus.*;

/**
 * @author Ast3t1s
 */
class ClusterDaemon {

    private static final Logger log = LoggerFactory.getLogger(ClusterDaemon.class);

    private enum MembershipUpdateReason {
        FAILURE_DETECTOR_EVENT,
        MEMBERSHIP_GOSSIP,
        SYNC,
        INITIAL_SYNC,
        SUSPICION_TIMEOUT
    }

    private final Member selfMember;
    private final Map<UniqueAddress, Member> members = new ConcurrentHashMap<>();
    private final Map<UniqueAddress, MembershipState> memberships = new ConcurrentHashMap<>();
    private final Set<UniqueAddress> aliveMembers = ConcurrentHashMap.newKeySet();

    private final AtomicBoolean started = new AtomicBoolean(false);
    private final AtomicBoolean isSyncing = new AtomicBoolean(false);

    private final Disposable.Composite actionsDisposables = Disposables.composite();
    private final Disposable.Swap disposable = Disposables.swap();

    private final Map<UniqueAddress, Disposable> suspicionTimeoutTasks = new ConcurrentHashMap<>();

    private final Duration heartbeatInterval;
    private final Duration syncInterval;
    private final Duration syncTimeout;
    private final ResettableInterval syncMembersTick;
    private final ResettableInterval reapUnreachableTick;

    private final ClusterProperties properties;
    private final FailureDetector failureDetector;
    private final Scheduler scheduler;
    private final RemoteTransport transport;
    private final Sinks.Many<ClusterEvent> eventDispatcher;

    private final EmissionStrategy emissionStrategy;

    // Internal components
    private final Gossiper gossiper;
    private final ClusterHeartbeat heartbeat;

    private final List<Address> seedNodes;

    ClusterDaemon(Member self, ClusterProperties properties, FailureDetector failureDetector, Scheduler scheduler,
                  RemoteTransport transport, Sinks.Many<ClusterEvent> eventDispatcher) {
        this.selfMember = self;
        this.properties = properties;
        this.failureDetector = failureDetector;
        this.transport = transport;
        this.scheduler = scheduler;
        this.eventDispatcher = eventDispatcher;

        this.emissionStrategy = EmissionStrategy.park(Duration.ofNanos(10));
        this.heartbeatInterval = properties.getFailureDetector().getHeartbeatInterval();
        this.syncInterval = properties.getSyncInterval();
        this.syncTimeout = properties.getSyncTimeout();

        this.syncMembersTick = new ResettableInterval(scheduler, syncInterval, syncInterval, tick -> syncMembers());
        this.reapUnreachableTick = new ResettableInterval(scheduler, properties.getUnreachableNodesReaperInterval(),
                properties.getUnreachableNodesReaperInterval(), tick -> Mono.fromRunnable(this::reapUnreachableMembers));

        this.heartbeat = new ClusterHeartbeat(self.getUniqueAddress(), scheduler, failureDetector, properties.getFailureDetector(),
                transport, eventDispatcher);
        this.gossiper = new Gossiper(self, scheduler, properties.getGossipInterval(), properties.getGossipFanout(),
                properties.getGossipRepeatMult(), transport, eventDispatcher);

        this.members.put(selfMember.getUniqueAddress(), selfMember);
        this.memberships.put(selfMember.getUniqueAddress(), new MembershipState(selfMember, ALIVE, 0));

        startListeners();

        this.seedNodes = cleanUpSeedNodes(properties.getSeedNodes().stream()
                .distinct()
                .map(Address::create)
                .collect(Collectors.toList()));
    }
    
    Optional<Member> findById(String id) {
        return memberships.entrySet().stream()
                .filter(entry -> Objects.equals(entry.getKey().getId(), id))
                .map(entry -> entry.getValue().member())
                .findFirst();
    }

    Collection<Member> allMembers() {
        return Collections.unmodifiableCollection(members.values());
    }

    Mono<Void> join() {
        if (started.compareAndSet(false, true)) {
            log.info("[{}] - Member activated: {}", selfMember.getUniqueAddress(), selfMember);
            return join0(seedNodes);
        }
        return Mono.empty();
    }

    Mono<Void> joinSeedNodes(Collection<Address> newSeedNodes) {
        if (started.compareAndSet(false, true)) {
            log.info("[{}] - Member activated: {}", selfMember.getUniqueAddress(), selfMember);
            return join0(cleanUpSeedNodes(newSeedNodes));
        }

        List<Address> seedNodes = newSeedNodes.stream()
                .filter(addr -> !selfMember.getUniqueAddress().getAddress().equals(addr))
                .filter(addr -> !this.seedNodes.contains(addr))
                .distinct()
                .collect(Collectors.toList());

        seedNodes.forEach(failureDetector::remove);

        return joining(cleanUpSeedNodes(seedNodes));
    }

    Mono<Void> leave(Address address) {
        return Flux.fromIterable(memberships.values())
                .filter(state -> state.member().getAddress().equals(address))
                .flatMap(state -> {
                    MembershipState s1 = new MembershipState(state.member(), LEAVING,state.incarnation() + 1);
                    memberships.put(s1.member().getUniqueAddress(), s1);
                    return spreadMembershipGossip(s1);
                }).then();
    }

    Mono<Void> leave() {
        if (started.compareAndSet(true, false)) {
            MembershipState s0 = memberships.get(selfMember.getUniqueAddress());
            MembershipState s1 = new MembershipState(selfMember, LEAVING,s0.incarnation() + 1);
            memberships.put(selfMember.getUniqueAddress(), s1);
            return spreadMembershipGossip(s1);
        }
        return Mono.empty();
    }

    void shutdown() {
        reapUnreachableTick.stop();
        heartbeat.stop();
        gossiper.stop();
        syncMembersTick.stop();
        actionsDisposables.dispose();
        disposable.dispose();
        // Cancel remove members tasks
        for (UniqueAddress memberId : suspicionTimeoutTasks.keySet()) {
            Disposable task = suspicionTimeoutTasks.get(memberId);
            if (task != null && !task.isDisposed()) {
                task.dispose();
            }
        }
        suspicionTimeoutTasks.clear();
    }

    // Remove duplicates and local address(es)
    private List<Address> cleanUpSeedNodes(Collection<Address> seedNodes) {
        InetAddress localIpAddress = Address.getLocalIpAddress();

        String hostAddress = localIpAddress.getHostAddress();
        String hostName = localIpAddress.getHostName();

        Address memberAddr = selfMember.getAddress();
        Address transportAddr = transport.address();
        Address memberAddrByHostAddress = Address.create(hostAddress, memberAddr.getPort());
        Address transportAddrByHostAddress = Address.create(hostAddress, transportAddr.getPort());
        Address memberAddByHostName = Address.create(hostName, memberAddr.getPort());
        Address transportAddrByHostName = Address.create(hostName, transportAddr.getPort());

        return new LinkedHashSet<>(seedNodes)
                .stream()
                .filter(addr -> checkAddressesNotEqual(addr, memberAddr))
                .filter(addr -> checkAddressesNotEqual(addr, transportAddr))
                .filter(addr -> checkAddressesNotEqual(addr, memberAddrByHostAddress))
                .filter(addr -> checkAddressesNotEqual(addr, transportAddrByHostAddress))
                .filter(addr -> checkAddressesNotEqual(addr, memberAddByHostName))
                .filter(addr -> checkAddressesNotEqual(addr, transportAddrByHostName))
                .collect(Collectors.toList());
    }

    private boolean checkAddressesNotEqual(Address address0, Address address1) {
        if (!address0.equals(address1)) {
            return true;
        } else {
            log.warn("[{}] Filtering out seed address: {}", selfMember.getUniqueAddress(), address0);
            return false;
        }
    }

    private void startListeners() {
        actionsDisposables.add(transport.receive()
                .publishOn(scheduler)
                .flatMap(this::onMessage)
                .subscribe(null, t -> log.warn("Sync and sync ack message listener terminated with an error", t)));

        actionsDisposables.add(gossiper.listen()
                .publishOn(scheduler)
                .doOnNext(this::handleMembershipGossip)
                .subscribe(null, t -> log.warn("Gossip listener terminated with an error")));
    }

    private Mono<Void> join0(Collection<Address> seedNodes) {
        return Mono.create(sink -> start0(seedNodes, sink)).subscribeOn(scheduler).then();
    }

    private void start0(Collection<Address> seedNodes, MonoSink<Object> sink) {
        log.info("[{}] Making initial sync to all seed members: {}", selfMember.getUniqueAddress(), seedNodes);
        if (seedNodes.isEmpty()) {
            schedulePeriodSync();
            sink.success();
            return;
        }
        log.info("[{}] Making initial sync to all seed nodes: {}", selfMember.getUniqueAddress(), seedNodes);

        transport.receive()
                .publishOn(scheduler)
                .timeout(syncTimeout, scheduler)
                .filter(message -> message.getPayload() instanceof Welcome)
                .take(1)
                .flatMap(this::onMessage)
                .doFinally(s -> {
                    schedulePeriodSync();
                    sink.success();
                })
                .subscribe();

        joining(seedNodes).subscribe();
    }

    private Mono<Void> joining(Collection<Address> seedNodes) {
        return Flux.fromIterable(seedNodes)
                .publishOn(scheduler)
                .flatMap(address -> transport.send(address, new Join(selfMember.getUniqueAddress(), new GossipOverview(memberships.values()))))
                .doOnError(ex -> log.warn("[{}] Unexpected exception due initial sync", selfMember.getUniqueAddress(), ex))
                .onErrorResume(ex -> Mono.empty())
                .then();
    }

    private void schedulePeriodSync() {
        reapUnreachableTick.start();
        heartbeat.start();
        syncMembersTick.start();
        gossiper.start();
    }

    private boolean isSingletonCluster() {
        return memberships.size() == 1;
    }

    private void handleMembershipGossip(Gossip gossip) {
        gossip.overview().getMembership().forEach(state -> {
            log.debug("[{}] Received membership gossip: {}", selfMember.getUniqueAddress(), state);
            updateMembership(state, MembershipUpdateReason.MEMBERSHIP_GOSSIP)
                    .subscribe(null,
                            ex -> log.warn("[{}] Unexpected error due handling gossip message", selfMember.getUniqueAddress(), ex));
        });
    }

    private void reapUnreachableMembers() {
        if (!isSingletonCluster()) {
            final var oldUnreachableMembers = memberships.values().stream()
                    .filter(MembershipState::isSuspect)
                    .collect(Collectors.toList());

            final var newlyDetectedUnreachableMembers = memberships.values().stream()
                    .filter(state -> failureDetector.isMonitoring(state.member().getAddress()))
                    .filter(state -> !failureDetector.isAvailable(state.member().getAddress()))
                    .collect(Collectors.toList());

            final var newlyDetectedReachableMembers = oldUnreachableMembers.stream()
                    .filter(state -> !state.member().equals(selfMember))
                    .filter(state -> failureDetector.isMonitoring(state.member().getAddress()))
                    .filter(state -> failureDetector.isAvailable(state.member().getAddress()))
                    .collect(Collectors.toSet());

            if (!newlyDetectedUnreachableMembers.isEmpty() || !newlyDetectedReachableMembers.isEmpty()) {
                final var newUnreachable = newlyDetectedUnreachableMembers.stream()
                        .filter(state -> !oldUnreachableMembers.contains(state))
                        .collect(Collectors.toList());

                if (!newUnreachable.isEmpty()) {
                    log.warn("Cluster Node [{}] - Marking node(s) as UNREACHABLE [{}]. Node roles [{}]",
                            selfMember.getUniqueAddress(), newUnreachable, String.join(", ", selfMember.getRoles()));
                }

                if (!newlyDetectedReachableMembers.isEmpty()) {
                    log.info("Marking node(s) as REACHABLE [{}]. Node roles [{}]", newlyDetectedReachableMembers,
                            String.join(", ", selfMember.getRoles()));
                }

                newUnreachable.forEach(unreachable -> {
                    MembershipState record = new MembershipState(unreachable.member(), SUSPECT, unreachable.incarnation());
                    updateMembership(record, MembershipUpdateReason.FAILURE_DETECTOR_EVENT)
                            .subscribe(null,
                                    ex -> log.error("[{}] Unexpected error due processing failure event",
                                            selfMember.getUniqueAddress(), ex));
                });

                newlyDetectedReachableMembers.forEach(reachable -> {
                    MembershipState record = new MembershipState(reachable.member(), ALIVE, reachable.incarnation() + 1);
                    updateMembership(record, MembershipUpdateReason.FAILURE_DETECTOR_EVENT)
                            .subscribe(null,
                                    ex -> log.error("[{}] Unexpected error due processing failure event",
                                            selfMember.getUniqueAddress(), ex));
                });
            }
        }
    }

    private Mono<Void> onJoin(Join join) {
        log.info("[{}] Received join message from {}", selfMember, join.getFrom());
        return syncMembership(join.getGossipOverview().getMembership(), false)
                .doOnSuccess(__ -> transport.send(join.getFrom().getAddress(), new Welcome(selfMember.getUniqueAddress(), new GossipOverview(memberships.values())))
                        .subscribe(null, ex -> log.debug("[{}] Failed to send Welcome to {}",
                                selfMember.getUniqueAddress(), join.getFrom(), ex)));
    }

    private Mono<Void> onWelcome(Welcome welcome) {
        log.info("[{}] Received welcome message from {}", selfMember, welcome.getFrom());
        return syncMembership(welcome.getGossipOverview().getMembership(), true);
    }

    private Mono<Void> onSync(GossipSync sync) {
        log.info("[{}] Received Sync from {}", selfMember, sync.getFrom());

        final UniqueAddress from = sync.getFrom();
        final UniqueAddress to = sync.getTo();
        final Collection<MembershipState> syncMemberships = sync.getGossipOverview().getMembership();

        if (!to.equals(selfMember.getUniqueAddress())) {
            log.info("Ignoring received gossip sync intended from someone else, from [{}] to [{}]",
                    from.getAddress(), to);
            return Mono.empty();
        }
        if (!members.containsKey(from)) {
            log.info("Ignoring received gossip sync from unknown [{}]", from);
            return Mono.empty();
        }
        if (memberships.get(from).isSuspect()) {
            log.info("Ignoring received gossip sync from unreachable [{}]", from);
            return Mono.empty();
        }
        if (syncMemberships.stream().noneMatch(s -> s.member().getUniqueAddress().equals(selfMember.getUniqueAddress()))) {
            log.info("Ignoring received gossip syc that does not contain myself, from [{}]", from);
            return Mono.empty();
        }

        return syncMembership(syncMemberships, false).then(Mono.defer(() -> sendDiff(syncMemberships, from)));
    }

    private Mono<Void> sendDiff(Collection<MembershipState> syncMemberships, UniqueAddress from) {
        Collection<MembershipState> diff = syncDiff(syncMemberships);
        if (!diff.isEmpty() && started.get()) {
            GossipSyncAck syncAck = new GossipSyncAck(selfMember.getUniqueAddress(), new GossipOverview(diff));
            return transport.send(from.getAddress(), syncAck);
        }
        return Mono.empty();
    }

    private Mono<Void> onSyncAck(GossipSyncAck syncAck) {
        log.info("[{}] Received SyncAck from {}", selfMember.getUniqueAddress(), syncAck.getFrom());
        final UniqueAddress from = syncAck.getFrom();
        if (!memberships.containsKey(from)) {
            log.info("Received sync ack from unknown [{}]", from);
            return Mono.empty();
        } else if (memberships.get(from).isSuspect()) {
            log.info("Ignoring received sync ack from unreachable [{}]", from);
            return Mono.empty();
        }
        return syncMembership(syncAck.getGossipOverview().getMembership(), false);
    }

    private Mono<Void> onMessage(RemoteMessage message) {
        if (message.getPayload() instanceof GossipSync) {
            return onSync((GossipSync) message.getPayload())
                    .onErrorResume(ex -> Mono.fromRunnable(
                            () -> log.warn("[{}] Unexpected error due sync", selfMember.getUniqueAddress(), ex)));
        } else if (message.getPayload() instanceof GossipSyncAck) {
            return onSyncAck((GossipSyncAck) message.getPayload())
                    .onErrorResume(ex -> Mono.fromRunnable(
                            () -> log.warn("[{}] Unexpected error due syncAck", selfMember.getUniqueAddress(), ex)));
        } if (message.getPayload() instanceof Join) {
            return onJoin((Join) message.getPayload())
                    .onErrorResume(ex -> Mono.fromRunnable(
                            () -> log.warn("[{}] Unexpected error due join", selfMember.getUniqueAddress(), ex)));
        } else if (message.getPayload() instanceof Welcome) {
            return onWelcome((Welcome) message.getPayload())
                    .onErrorResume(ex -> Mono.fromRunnable(
                            () -> log.warn("[{}] Unexpected error due welcome", selfMember.getUniqueAddress(), ex)));
        } else {
            return Mono.empty();
        }
    }

    private Mono<Void> syncMembers() {
        UniqueAddress peer = selectSyncAddress().orElse(null);
        if (peer == null || !isSyncing.compareAndSet(false, true)) {
            return Mono.empty();
        }

        GossipSync gossipSync = new GossipSync(selfMember.getUniqueAddress(), peer,
                new GossipOverview(memberships.values()));

        log.info("[{}] Send sync {} to {}", selfMember.getUniqueAddress(), gossipSync, peer);
        return transport.send(peer.getAddress(), gossipSync)
                .doOnError(e -> log.warn("[{}] Failed to send Sync to {}", selfMember.getUniqueAddress(), peer, e))
                .onErrorResume(e -> Mono.empty())
                .doFinally(s -> {
                    isSyncing.set(false);
                    log.info("Member size {}", members.size());
                });
    }

    private Collection<MembershipState> syncDiff(Collection<MembershipState> syncMembership) {
        List<MembershipState> result = new ArrayList<>();
        for (MembershipState syncState : syncMembership) {
            MembershipState exists = memberships.get(syncState.member().getUniqueAddress());
            if ((exists != null) && exists.incarnation() > syncState.incarnation()) {
                result.add(syncState);
            }
        }
        for (MembershipState currentState : memberships.values()) {
            if (!syncMembership.contains(currentState)) {
                result.add(currentState);
            }
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    private Mono<Void> syncMembership(Collection<MembershipState> memberships, boolean onStart) {
        MembershipUpdateReason reason =
                onStart ? MembershipUpdateReason.INITIAL_SYNC : MembershipUpdateReason.SYNC;
        Mono<Void>[] syncMono = memberships.stream()
                .map(s1 -> updateMembership(s1, reason)
                        .doOnError(ex -> log.warn("[{}] Unexpected error due syncing membership {}",
                                selfMember.getUniqueAddress(), reason, ex))
                        .onErrorResume(ex -> Mono.empty()))
                .toArray(Mono[]::new);

        return Flux.mergeDelayError(syncMono.length, syncMono).then();
    }

    private Mono<Void> updateMembership(MembershipState s1, MembershipUpdateReason reason) {
        return Mono.defer(
                () -> {
                    Objects.requireNonNull(s1, "Membership state can't be null");

                    // Get current record
                    MembershipState s0 = memberships.get(s1.member().getUniqueAddress());

                    // if current state is LEAVING then we want to process other event too
                    // Check if new record s1 overrides existing membership state s0
                    if ((s0 == null || !s0.isLeaving()) && !s1.isOverrides(s0)) {
                        log.info("[{}][{}] Skipping update, can't override r0: {} with received r1: {}",
                                selfMember.getUniqueAddress(), reason, s0, s1);
                        return Mono.empty();
                    }

                    // If received updated for local member then increase incarnation and spread Alive gossip
                    if (s1.member().getUniqueAddress().equals(selfMember.getUniqueAddress())) {
                        return onSelfMemberDetected(s0, s1, reason);
                    }

                    if (s1.isLeaving()) {
                        return onLeavingDetected(s0, s1);
                    }

                    if (s1.isDead()) {
                        return onDeadMemberDetected(s1);
                    }

                    if (s1.isSuspect()) {
                        onUnreachableMemberDetected(s0, s1, reason);
                    }

                    if (s1.isAlive()) {
                        if (s0 != null && s0.isLeaving()) {
                            return onAliveAfterLeaving(s1);
                        }

                        if (s0 == null || s0.incarnation() < s1.incarnation()) {
                            onAliveMemberDetected(s0, s1);
                            cancelSuspicionTimeoutTask(s1.member().getUniqueAddress());
                            spreadMembershipGossipUnlessGossiped(s1, reason);
                        }
                    }

                    return Mono.empty();
                });
    }

    private Mono<Void> onSelfMemberDetected(MembershipState s0, MembershipState s1, MembershipUpdateReason reason) {
        final int currentIncarnation = Math.max(s0.incarnation(), s1.incarnation());
        final MembershipState s2 = new MembershipState(selfMember, s0.status(), currentIncarnation + 1);

        memberships.put(selfMember.getUniqueAddress(), s2);

        log.debug("[{}][{}] Updating incarnation, "
                + "local record r0: {} to received r1: {}, "
                + "spreading with increased incarnation r2: {}", selfMember.getUniqueAddress(), reason, s0, s1, s2);

        return spreadMembershipGossip(s2);
    }

    private Mono<Void> onLeavingDetected(MembershipState s0, MembershipState s1) {
        final Member member = s1.member();
        final UniqueAddress memberId = member.getUniqueAddress();

        memberships.put(memberId, s1);

        if (s0 != null && (s0.isAlive() || (s0.isSuspect() && aliveMembers.contains(memberId)))) {
            publishEvent(ClusterMembershipEvent.createUpdated(member));
        }

        if (s0 == null || !s0.isLeaving()) {
            scheduleSuspicionTimeoutTask(s1);
            return spreadMembershipGossip(s1);
        } else {
            return Mono.empty();
        }
    }

    private Mono<Void> onAliveAfterLeaving(MembershipState s1) {
        return Mono.fromRunnable(
                () -> {
                    // r1 is outdated "ALIVE" event because we already have "LEAVING"
                    final Member member = s1.member();
                    final UniqueAddress memberId = member.getUniqueAddress();

                    members.put(memberId, member);

                    // Emit events if needed and ignore alive
                    if (aliveMembers.add(memberId)) {
                        // There is no metadata in this case
                        // We could'n fetch metadata because node already wanted to leave
                        publishEvent(ClusterMembershipEvent.createAdded(member));
                        publishEvent(ClusterMembershipEvent.createLeaving(member));
                    }
                });
    }

    private Mono<Void> onDeadMemberDetected(MembershipState s1) {
        return Mono.fromRunnable(
                () -> {
                    final Member member = s1.member();

                    cancelSuspicionTimeoutTask(member.getUniqueAddress());

                    if (!members.containsKey(member.getUniqueAddress())) {
                        return;
                    }

                    // Removed membership
                    members.remove(member.getUniqueAddress());
                    aliveMembers.remove(member.getUniqueAddress());

                    final MembershipState s0 = memberships.remove(member.getUniqueAddress());

                    if (s0.isLeaving()) {
                        log.info("Member left gracefully: {}", member);
                    } else {
                        log.info("Member left without notification: {}", member);
                    }

                    publishEvent(ClusterMembershipEvent.createRemoved(member));
                });
    }

    private void onUnreachableMemberDetected(MembershipState s0, MembershipState s1,  MembershipUpdateReason reason) {
        log.debug("Unreachable member detected s0: {}, s1: {}", s0, s1);

        // Update membership and schedule/cancel suspicion timeout task;
        if (s0 == null || !s0.isLeaving()) {
            memberships.put(s1.member().getUniqueAddress(), s1);
            publishEvent(ClusterReachabilityEvent.unreachable(s1.member()));
        }
        //Start auto down task
        scheduleSuspicionTimeoutTask(s1);
        spreadMembershipGossipUnlessGossiped(s1, reason);
    }

    private void onAliveMemberDetected(MembershipState r0, MembershipState r1) {
        log.info("Alive member detected r0: {}, r1: {}", r0, r1);

        final Member member = r1.member();
        final boolean memberExists = members.containsKey(member.getUniqueAddress());

        ClusterMembershipEvent event = null;
        if (!memberExists) {
            event = ClusterMembershipEvent.createAdded(member);
        } else if (!r0.member().equals(r1.member())) {
            event = ClusterMembershipEvent.createUpdated(member);
        }

        members.put(member.getUniqueAddress(), member);
        memberships.put(member.getUniqueAddress(), r1);

        //Remove the node from the failure detector
        if (!memberExists) {
            failureDetector.remove(r1.member().getAddress());
        }

        //If member was unreachable is now reachable
        if (r0 != null && r0.isSuspect()) {
            publishEvent(ClusterReachabilityEvent.reachable(r1.member()));
        }

        if (event != null) {
            publishEvent(event);
            
            if (event.isAdded()) {
                aliveMembers.add(member.getUniqueAddress());
            }
        }
    }

    private void publishEvent(ClusterEvent event) {
        log.trace("[{}] Publish membership event: {}", selfMember.getUniqueAddress(), event);
        emissionStrategy.emitNext(eventDispatcher, event);
    }

    private void scheduleSuspicionTimeoutTask(MembershipState s) {
        final long suspicionTimeout = suspicionTimeout(5, members.size(), heartbeatInterval.toMillis());

        suspicionTimeoutTasks.computeIfAbsent(s.member().getUniqueAddress(), id -> {
            log.info("[{}] Scheduled SuspicionTimeoutTask for {}, suspicionTimeout: {}",
                    selfMember.getUniqueAddress(), id, suspicionTimeout);
            return scheduler.schedule(() -> onSuspicionTimeout(id), suspicionTimeout, TimeUnit.MILLISECONDS);
        });
    }

    private void onSuspicionTimeout(UniqueAddress id) {
        suspicionTimeoutTasks.remove(id);
        MembershipState state = memberships.get(id);
        if (state != null) {
            log.debug("[{}] Declare SUSPECTED member {} as DEAD by timeout", selfMember.getUniqueAddress(), state);
            MembershipState deadRecord = new MembershipState(state.member(), DEAD, state.incarnation());
            updateMembership(deadRecord, MembershipUpdateReason.SUSPICION_TIMEOUT)
                    .subscribe(null,
                            ex -> log.error("[{}] Unexpected error due suspicion timeout cause:",
                                    selfMember.getUniqueAddress(), ex));
        }
    }

    private void cancelSuspicionTimeoutTask(UniqueAddress id) {
        Disposable timeoutTask = suspicionTimeoutTasks.remove(id);
        if (timeoutTask != null && !timeoutTask.isDisposed()) {
            log.debug("[{}] Cancelled suspicion timeout task for {}", selfMember, id);
            timeoutTask.dispose();
        }
    }

    private Optional<UniqueAddress> selectSyncAddress() {
        List<UniqueAddress> otherMembers = members.values().stream()
                .map(Member::getUniqueAddress)
                .filter(uniqueAddress -> !Objects.equals(selfMember.getUniqueAddress(), uniqueAddress))
                .collect(Collectors.toList());

        Collections.shuffle(otherMembers);
        if (otherMembers.isEmpty()) {
            return Optional.empty();
        } else {
            int index = ThreadLocalRandom.current().nextInt(otherMembers.size());
            return Optional.of(otherMembers.get(index));
        }
    }

    private void spreadMembershipGossipUnlessGossiped(MembershipState s, MembershipUpdateReason reason) {
        // Spread gossip (unless already gossiped)
        if (reason != MembershipUpdateReason.MEMBERSHIP_GOSSIP
                && reason != MembershipUpdateReason.INITIAL_SYNC) {
            spreadMembershipGossip(s).subscribe();
        }
    }

    private Mono<Void> spreadMembershipGossip(MembershipState r) {
        return Mono.defer(
                () -> {
                    log.debug("[{}] Send membership with gossip", selfMember.getUniqueAddress());
                    return gossiper
                            .spread(new GossipOverview(Collections.singletonList(r)))
                            .doOnError(ex -> log.debug("[{}] Failed to send membership with gossip, cause: {}",
                                    selfMember.getUniqueAddress(), ex.toString()))
                            .then();
                });
    }

    private static long suspicionTimeout(int suspicionFactor, int clusterSize, long interval) {
        return suspicionFactor * (32 - Integer.numberOfLeadingZeros(clusterSize)) * interval;
    }
}
