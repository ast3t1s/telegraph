package im.telegraph.generator.type;

public class SchemeStructType extends SchemeType {
    private String type;

    public SchemeStructType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return type;
    }
}
