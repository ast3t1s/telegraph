package im.telegraph.protocol.auth;

import im.telegraph.crypto.Cryptos;
import im.telegraph.crypto.Curve25519;
import im.telegraph.crypto.primitives.util.ByteStrings;
import im.telegraph.protocol.cryptography.ServerTrustedKey;
import im.telegraph.protocol.cryptography.TrustedKeyStore;
import im.telegraph.protocol.model.mtproto.ProtoMessage;
import im.telegraph.protocol.model.mtproto.ProtoStruct;
import im.telegraph.protocol.model.mtproto.auth.*;
import im.telegraph.protocol.model.transport.ProtoPackage;
import im.telegraph.server.TransportConnection;
import im.telegraph.util.ThreadLocalSecureRandom;
import org.reactivestreams.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Function;

public abstract class AuthKeyHandler {

    private static final Logger log = LoggerFactory.getLogger(AuthKeyHandler.class);

    public static final int NONCE_BYTES = 32;
    public static final int PRF_BYTES = 256;
    public static final int SIGN_RANDOM_BYTES = 64;

    private static final Duration SESSION_TTL = Duration.ofMinutes(1);

    private final TrustedKeyStore trustedKeyStore;
    private volatile Clock clock = Clock.systemUTC();

    private final ExpireTimeChecker expireTimeChecker = new ExpireTimeChecker();

    private final ConcurrentMap<Long, TempAuthKey> temporaryKeys = new ConcurrentHashMap<>();

    private final Map<Class<?>, Function<?, Publisher<?>>> handlerMap = new HashMap<>();

    public AuthKeyHandler(TrustedKeyStore trustedKeyStore) {
        this.trustedKeyStore = trustedKeyStore;

        addHandler(RequestStartAuth.class, this::handleStartAuth);
        addHandler(RequestGetServerKey.class, this::handleGetServerKeyRequest);
        addHandler(RequestDH.class, this::handleDoDh);
    }

    protected abstract Mono<Void> storeAuthKey(byte[] masterKey);

    private <T extends ProtoStruct> void addHandler(Class<T> register, Function<T, Publisher<?>> handler) {
        this.handlerMap.put(register, handler);
    }

    @SuppressWarnings("unchecked")
    public Mono<Void> handle(TransportConnection connection, ProtoStruct request) {
        Objects.requireNonNull(connection, "'connection' can't be null");
        Objects.requireNonNull(request, "'request', can't be null");

        final Function<ProtoStruct, Publisher<?>> handler = (Function<ProtoStruct, Publisher<?>>) handlerMap.get(request.getClass());
        if (handler == null) {
            log.warn("Handler not found for request: {}", request);
            return Mono.error(() -> new AuthorizationException("Unknown authorization request"));
        }

        return Mono.defer(() -> Flux.from(handler.apply(request))
                .doOnSubscribe(s -> clearExpiredSessions())
                .checkpoint("Dispatch authorization request for " + connection.getId() + " request " + request.getClass())
                .ofType(ProtoStruct.class)
                .doOnError(ex -> log.warn("Unexpected error due processing authorization request {}", request, ex))
                .flatMap(response -> writeResponse(connection, response))
                .then());
    }

    protected Mono<ResponseStartAuth> handleStartAuth(RequestStartAuth request) {
        log.debug("Handle start auth request: {}", request);
        return trustedKeyStore.findAll().collectList()
                .map(trustedKeys -> {
                    final var requestId = request.getRandomId();
                    if (temporaryKeys.containsKey(requestId)) {
                        log.warn("Request ID: {} is already exists", requestId);
                        throw new AuthorizationException("Wrong request ID");
                    }
                    final var serverNonce = new byte[NONCE_BYTES];
                    final var random = ThreadLocalSecureRandom.current();
                    random.nextBytes(serverNonce);
                    final var availableKeys = trustedKeys.stream()
                            .mapToLong(ServerTrustedKey::getId)
                            .toArray();
                    final var tempAuthKey = new TempAuthKey(requestId, serverNonce, clock.instant().plus(SESSION_TTL));
                    temporaryKeys.put(requestId, tempAuthKey);
                    return new ResponseStartAuth(request.getRandomId(), availableKeys, serverNonce);
                });
    }

    protected Mono<ResponseGetServerKey> handleGetServerKeyRequest(RequestGetServerKey request) {
        log.debug("Handle get server key request: {}", request);
        return trustedKeyStore.find(request.getKeyId())
                .map(key -> new ResponseGetServerKey(key.getId(), key.getPublicKey()))
                .switchIfEmpty(Mono.defer(() -> Mono.error(() -> new AuthorizationException("Wrong key id"))));
    }

    protected Mono<ResponseDH> handleDoDh(RequestDH request) {
        log.debug("Handle do DH request: {}", request);
        return trustedKeyStore.find(request.getKeyId())
                .switchIfEmpty(Mono.defer(() -> Mono.error(() -> new AuthorizationException("Wrong server key"))))
                .flatMap(trustedKey -> {
                    final var holder = temporaryKeys.get(request.getRandomId());
                    if (holder == null) {
                        log.warn("Auth holder not found: {}", request.getRandomId());
                        return Mono.error(() -> new AuthorizationException("Wrong random id"));
                    }

                    if (holder.getServerNonce().length != request.getClientNonce().length) {
                        log.warn("Auth server nonce size does not match with client nonce");
                        return Mono.error(() -> new AuthorizationException("Wrong nonce"));
                    }

                    final var preMaster = Curve25519.calculateAgreement(trustedKey.getPrivateKey(), request.getClientKey());
                    final var fullNonce = ByteStrings.merge(request.getClientNonce(), holder.getServerNonce());
                    final var prfCombined = Cryptos.PRF_SHA_STREEBOG_256();
                    final var master = prfCombined.calculate(preMaster, "master secret", fullNonce, PRF_BYTES);
                    final var verify = prfCombined.calculate(master, "client finished", fullNonce, PRF_BYTES);
                    final var randomBytes = new byte[SIGN_RANDOM_BYTES];
                    final var secureRandom = ThreadLocalSecureRandom.current();
                    secureRandom.nextBytes(randomBytes);
                    final var verifySign = Curve25519.calculateSignature(randomBytes, trustedKey.getPrivateKey(), verify);

                    temporaryKeys.remove(request.getRandomId());
                    return storeAuthKey(master).thenReturn(new ResponseDH(holder.getRandomId(), verify, verifySign));
                });
    }

    protected Mono<Void> writeResponse(TransportConnection connection, ProtoStruct response) {
        return Mono.defer(
                () -> {
                    ProtoMessage message = new ProtoMessage(0, response.toByteArray());
                    return connection.send(Mono.just(new ProtoPackage(0, 0, message)));
                });
    }

    private void clearExpiredSessions() {
        expireTimeChecker.checkIfNecessary(clock.instant());
    }

    private class ExpireTimeChecker {

        private final ReentrantLock lock = new ReentrantLock();

        private volatile Instant checkTime = clock.instant().plus(SESSION_TTL);

        public void checkIfNecessary(Instant now) {
            if (this.checkTime.isBefore(now)) {
                removeExpiredSessions(now);
            }
        }

        public void removeExpiredSessions(Instant now) {
            if (temporaryKeys.isEmpty()) {
                return;
            }
            if (this.lock.tryLock()) {
                try {
                    temporaryKeys.values().removeIf(session -> session.isExpire(now));
                } finally {
                    this.checkTime = now.plus(SESSION_TTL);
                    this.lock.unlock();
                }
            }
        }
    }

    /**
     * Temporary auth key stores only in server memory using for authorization should be deleted after creating
     * permanent key
     */
    private static class TempAuthKey {

        private final long randomId;
        private final byte[] serverNonce;
        private final Instant expireAt;

        TempAuthKey(long randomId, byte[] serverNonce, Instant expireAt) {
            this.randomId = randomId;
            this.serverNonce = serverNonce;
            this.expireAt = expireAt;
        }

        public boolean isExpire(Instant now) {
            return expireAt.isBefore(now);
        }

        public long getRandomId() {
            return randomId;
        }

        public byte[] getServerNonce() {
            return serverNonce;
        }
    }
}
