package im.telegraph.handlers.internal;

import im.telegraph.api.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GetExtractedUsers {

    /**
     * Extract user ID's from conversation history messages.
     *
     * @param historyMessages the {@link Iterable} of history messages.
     */
    public static List<Long> extract(final Iterable<ApiHistoryMessage> historyMessages) {
        final List<Long> res = new ArrayList<>();
        for (final ApiHistoryMessage apiHistoryMessage : historyMessages) {
            final ApiMessage apiMessage = apiHistoryMessage.getMessage();
            if (apiMessage instanceof ApiServiceMessage) {
                final ApiServiceEx serviceEx = ((ApiServiceMessage) apiMessage).getExt();
                if (serviceEx instanceof ApiServiceExAddUser) {
                    res.add(((ApiServiceExAddUser) serviceEx).getUserId());
                } else if (serviceEx instanceof ApiServiceExDeleteUser) {
                    res.add(((ApiServiceExDeleteUser) serviceEx).getUserId());
                }
            } else if (apiMessage instanceof ApiTextMessage) {
                res.addAll(((ApiTextMessage) apiMessage).getMentions());
            }
        }
        return Collections.unmodifiableList(res);
    }
} 
