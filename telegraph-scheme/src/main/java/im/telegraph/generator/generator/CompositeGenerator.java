package im.telegraph.generator.generator;

import im.telegraph.generator.type.SchemeDefinition;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Composite scheme generator.
 *
 * @author Vsevolod Bondarenko
 */
public class CompositeGenerator implements SchemeGenerator {

    private final List<SchemeGenerator> delegates = new ArrayList<>();

    protected CompositeGenerator(final Builder builder) {
        this.delegates.addAll(builder.delegates);
    }

    public static Builder builder() {
        return Builder.builder();
    }

    @Override
    public void generate(SchemeDefinition definition, String out) throws IOException {
        for (SchemeGenerator schemeGenerator : delegates) {
            schemeGenerator.generate(definition, out);
        }
    }

    public static final class Builder {

        private final List<SchemeGenerator> delegates = new ArrayList<>();

        protected Builder() {
        }

        public static Builder builder() {
            return new Builder();
        }

        public Builder add(SchemeGenerator generator) {
            this.delegates.add(Objects.requireNonNull(generator));
            return this;
        }

        public Builder generators(List<SchemeGenerator> delegates) {
            Objects.requireNonNull(delegates);
            this.delegates.addAll(delegates);
            return this;
        }

        public CompositeGenerator build() {
            return new CompositeGenerator(this);
        }
    }
}
