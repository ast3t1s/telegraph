package im.telegraph.protocol.model.mtproto.rpc;

import im.telegraph.engine.serialization.DataInputStream;
import im.telegraph.engine.serialization.DataOutputStream;
import im.telegraph.protocol.model.mtproto.ProtoStruct;

import java.io.IOException;

public class RpcRequest extends ProtoStruct {

	public static final byte HEADER = (byte) 0x01;

	public int requestType;

	public byte[] payload;

	public RpcRequest(DataInputStream inputStream) throws IOException {
		super(inputStream);
	}

	public RpcRequest(int requestType, byte[] payload) {
		this.requestType = requestType;
		this.payload = payload;
	}

	public int getRequestType() {
		return requestType;
	}

	public byte[] getPayload() {
		return payload;
	}

	@Override
	protected byte getHeader() {
		return HEADER;
	}

	@Override
	protected void writeBody(DataOutputStream outputStream) throws IOException {
		outputStream.writeInt32(requestType);
		outputStream.writeProtoBytes(payload);
	}

	@Override
	protected void readBody(DataInputStream inputStream) throws IOException {
		this.requestType = inputStream.readInt();
		this.payload = inputStream.readProtoBytes();
	}

}
