package im.telegraph.server.reactor;

import im.telegraph.server.TransportFrame;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.nio.ByteBuffer;
import java.util.List;
import java.util.function.Function;

/**
 * @author Ast3t1s
 */
public class ReactorTransportFrameDecoder extends ByteToMessageDecoder {

    private final Function<ByteBuffer, List<TransportFrame>> transformer;

    public ReactorTransportFrameDecoder(Function<ByteBuffer,  List<TransportFrame>> transformer) {
        this.transformer = transformer;
    }

    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf, List<Object> list) throws Exception {
        ByteBuffer nioBuffer = byteBuf.nioBuffer();
        int start = nioBuffer.position();
        List<TransportFrame> frames = transformer.apply(nioBuffer);
        int bytesRead = nioBuffer.position() - start;
        //if (bytesRead > 0) {
            byteBuf.skipBytes(bytesRead);
       // }
        list.addAll(frames);
    }
}
