package im.telegraph;

import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
public class TransactionalRollbackHelper {

    public <T> Mono<T> withRollback(final Mono<T> mono){
        //return Mono.from(setupRollback(mono));
        return Mono.empty();
    }

    public <T> Mono<T> withRollback(final Flux<T> flux) {
        //return Flux.from(setupRollback(flux)).next();
        return Mono.empty();
    }

} 
