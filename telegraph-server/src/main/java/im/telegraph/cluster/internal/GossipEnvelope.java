package im.telegraph.cluster.internal;

import im.telegraph.cluster.ClusterMessage;
import im.telegraph.cluster.UniqueAddress;

import java.util.List;

/**
 * @author Ast3t1s
 */
public class GossipEnvelope implements ClusterMessage {

    private static final long serialVersionUID = 1L;

    private final UniqueAddress from;
    private final List<Gossip> gossips;

    public GossipEnvelope(UniqueAddress from, List<Gossip> gossips) {
        this.from = from;
        this.gossips = gossips;
    }

    public UniqueAddress getFrom() {
        return from;
    }

    public List<Gossip> getGossips() {
        return gossips;
    }

    @Override
    public String toString() {
        return "GossipEnvelope{" +
                "from='" + from + '\'' +
                ", gossips=" + gossips +
                '}';
    }
}
