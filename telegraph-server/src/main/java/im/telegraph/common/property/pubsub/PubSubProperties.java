package im.telegraph.common.property.pubsub;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.time.Duration;

/**
 * @author Ast3t1s
 */
@Getter
@Setter
@Accessors(chain = true)
public class PubSubProperties {

    /***
     * Start the mediator on members tagged with this role.
     */
    private String role;

    /**
     * How often the DistributedPubSubMediator should send out gossip information
     */
    private Duration gossipInterval = Duration.ofSeconds(1);

    /**
     * Removed entries are pruned after this duration
     */
    private Duration removedTtl = Duration.ofSeconds(120);

    /**
     * Maximum number of elements to transfer in one message when synchronizing the registries.
     * Next chunk will be transferred in next round of gossip.
     */
    private int maxDeltaElements = 3000;
} 
