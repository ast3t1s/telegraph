package im.telegraph.common;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * @author Ast3t1s
 */
public final class Page<T> implements Iterable<T>, Serializable {

    private static final long serialVersionUID = 8637922543023658793L;

    private final List<T> content;
    private final int currentPage;
    private final long totalCount;

    public Page(List<T> content) {
        this(content, 0, content.size());
    }

    public Page(List<T> content, long totalCount) {
        this(content, 0, totalCount);
    }

    public Page(List<T> content, int currentPage, long totalCount) {
        this.content = Objects.requireNonNull(content);
        this.currentPage = currentPage;
        this.totalCount = totalCount;
    }

    public List<T> getContent() {
        return Collections.unmodifiableList(content);
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public long getTotalCount() {
        return totalCount;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        Page<?> page = (Page<?>) object;
        return currentPage == page.currentPage &&
                totalCount == page.totalCount &&
                Objects.equals(content, page.content);
    }

    @Override
    public int hashCode() {
        return Objects.hash(content, currentPage, totalCount);
    }

    @Override
    public String toString() {
        String contentType = "UNKNOWN";
        List<T> content = getContent();
        if (!content.isEmpty() && content.get(0) != null) {
            contentType = content.get(0).getClass().getName();
        }
        return String.format("Page %s of %d containing %s instances", getCurrentPage() + 1, getTotalCount(),
                contentType);
    }

    public Stream<T> stream() {
        return getContent().stream();
    }

    @NotNull
    @Override
    public Iterator<T> iterator() {
        return getContent().iterator();
    }
}
