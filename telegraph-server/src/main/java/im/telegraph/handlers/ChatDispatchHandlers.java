package im.telegraph.handlers;

import im.telegraph.api.*;
import im.telegraph.api.rpc.*;
import im.telegraph.api.update.UpdateChatParticipantAdd;
import im.telegraph.api.update.UpdateMessage;
import im.telegraph.common.Page;
import im.telegraph.domain.entity.Id;
import im.telegraph.domain.entity.Peer;
import im.telegraph.domain.entity.chat.ChatParticipant;
import im.telegraph.domain.entity.chat.ChatPhoto;
import im.telegraph.domain.entity.chat.PermissionSet;
import im.telegraph.domain.entity.photo.Photo;
import im.telegraph.handlers.dispatch.DispatchContext;
import im.telegraph.handlers.internal.ApiEntityMapper;
import im.telegraph.handlers.mapper.DialogConverter;
import im.telegraph.handlers.mapper.UserConverter;
import im.telegraph.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.function.TupleUtils;

import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicReference;

@Component
public class ChatDispatchHandlers {

    private static final Logger log = LoggerFactory.getLogger(ChatDispatchHandlers.class);

    private final ChatService chatService;
    private final UserService userService;
    private final PhotoService photoService;
    private final UpdatesService updatesService;
    private final MessagingService messageService;

    private final UserConverter userConverter;
    private final DialogConverter dialogConverter;

    public ChatDispatchHandlers(ChatService chatService,
                                UserService userService,
                                PhotoService photoService,
                                UpdatesService updatesService,
                                MessagingService messageService,
                                UserConverter userConverter,
                                DialogConverter dialogConverter) {
        this.chatService = chatService;
        this.userService = userService;
        this.photoService = photoService;
        this.updatesService = updatesService;
        this.messageService = messageService;
        this.userConverter = userConverter;
        this.dialogConverter = dialogConverter;
    }

    public Mono<ResponseStatedMessage> handleCreateChat(DispatchContext<RequestChatsCreateChat> context) {
        final String title = context.getRequest().getTitle();
        final String description = context.getRequest().getDescription();
        final PermissionSet permissions = Optional.ofNullable(context.getRequest().getSettings())
                .map(PermissionSet::of).orElse(null);

        return Mono.justOrEmpty(context.getUserId().map(Id::of))
                .flatMap(selfId -> chatService.create(selfId, title, description, permissions))
                .flatMap(chat -> {
                    final Peer peer = Peer.ofChat(chat.getId());
                    final long randomId = ThreadLocalRandom.current().nextLong();
                    final Instant date = chat.getCreatedAt();

                    final ApiServiceMessage serviceMessage = new ApiServiceMessage(new ApiServiceExChatCreated());

                    Mono<ApiHistoryMessage> apiMessage = Mono.fromCallable(serviceMessage::buildContainer)
                            .flatMap(message -> messageService.createMessage(chat.getOwnerUserId(), peer, randomId, date, serviceMessage.getHeader(), message))
                            .map(message -> Optional.ofNullable(dialogConverter.convert(message)))
                            .filter(Optional::isPresent)
                            .map(Optional::get);

                    Mono<List<ApiAbsUser>> apiUsers = userService.getUsers(Collections.singletonList(chat.getOwnerUserId()))
                            .map(user -> Optional.ofNullable(userConverter.convert(context.getAuthId(), user)))
                            .filter(Optional::isPresent)
                            .map(Optional::get)
                            .collectList()
                            .defaultIfEmpty(Collections.emptyList());

                    Mono<List<ApiChat>> apiChats = Flux.just(chat)
                            .map(chat1 -> Optional.ofNullable(ApiEntityMapper.toApiChat(chat1)))
                            .filter(Optional::isPresent)
                            .map(Optional::get)
                            .collectList()
                            .defaultIfEmpty(Collections.emptyList());

                    return Mono.zip(apiMessage, apiUsers, apiChats)
                            .map(TupleUtils.function(ResponseStatedMessage::new));
                });
    }

    public Mono<ResponseStatedMessage> handleEditChatTitle(DispatchContext<RequestChatsEditTitle> context) {
        return Mono.justOrEmpty(context.getUserId().map(Id::of))
                .flatMap(selfId -> {
                    final Id chatId = Id.of(context.getRequest().getChatId());
                    final String title = context.getRequest().getTitle();
                    final Long randomId = ThreadLocalRandom.current().nextLong();

                    final ApiServiceMessage serviceMessage = new ApiServiceMessage(new ApiServiceExChangedTitle(title));

                    Mono<ApiHistoryMessage> apiMessage = Mono.fromCallable(serviceMessage::buildContainer)
                            .flatMap(message -> messageService.createMessage(selfId, Peer.ofChat(chatId), randomId, Instant.now(), serviceMessage.getHeader(), message))
                            .map(message -> Optional.ofNullable(dialogConverter.convert(message)))
                            .filter(Optional::isPresent)
                            .map(Optional::get);

                    Mono<List<ApiAbsUser>> apiUsers = userService.getUsers(Collections.singletonList(selfId))
                            .map(user -> Optional.ofNullable(userConverter.convert(context.getAuthId(), user)))
                            .filter(Optional::isPresent)
                            .map(Optional::get)
                            .collectList()
                            .defaultIfEmpty(Collections.emptyList());

                    Mono<List<ApiChat>> apiChats = chatService.getChats(Collections.singletonList(chatId))
                            .map(chat -> Optional.ofNullable(ApiEntityMapper.toApiChat((chat))))
                            .filter(Optional::isPresent)
                            .map(Optional::get)
                            .collectList()
                            .defaultIfEmpty(Collections.emptyList());

                    return chatService.editChatTitle(chatId, selfId, title)
                            .then(Mono.zip(apiMessage, apiUsers, apiChats))
                            .map(TupleUtils.function(ResponseStatedMessage::new));
                });
    }

    public Mono<ResponseBool> handleEditAbout(DispatchContext<RequestChatsEditAbout> context) {
        final Id chatId = Id.of(context.getRequest().getChatId());
        final String description = context.getRequest().getDescription();

        return Mono.justOrEmpty(context.getUserId().map(Id::of))
                .flatMap(selfId -> chatService.editChatAbout(chatId, selfId, description))
                .map(chat -> Objects.equals(chat.getDescription().orElse(null), description))
                .map(ResponseBool::new);
    }

    public Mono<ResponseStatedMessage> handleEditChatPhoto(DispatchContext<RequestChatsEditPhoto> context) {
        return Mono.justOrEmpty(context.getUserId().map(Id::of))
                .flatMap(selfId -> {
                    final Id chatId = Id.of(context.getRequest().getChatId());
                    final Peer peer = Peer.ofChat(chatId);
                    final Long randomId = ThreadLocalRandom.current().nextLong();

                    final AtomicReference<Photo> photoRef = new AtomicReference<>();

                    Mono<Void> updateChatPhoto;
                    if (context.getRequest().getFileLocation() != null) {
                        final long fileId = context.getRequest().getFileLocation().getFileId();
                        final long accessHash = context.getRequest().getFileLocation().getAccessHash();

                        updateChatPhoto = photoService.uploadProfilePhoto(selfId, Id.of(fileId), accessHash)
                                .doOnNext(photoRef::set)
                                .map(photo -> new ChatPhoto(
                                        photo.getPhotoSmall().getFileReferenceId(),
                                        photo.getPhotoLarge().getFileReferenceId())
                                )
                                .flatMap(chatPhoto -> chatService.editChatPhoto(chatId, selfId, chatPhoto));
                    } else {
                        updateChatPhoto = chatService.editChatPhoto(chatId, selfId, null);
                    }

                    Mono<ApiHistoryMessage> apiMessage = Mono.justOrEmpty(Optional.ofNullable(photoRef.get()))
                            .map(photo -> Optional.ofNullable(ApiEntityMapper.convertToApiAvatar(photo)))
                            .filter(Optional::isPresent)
                            .map(Optional::get)
                            .map(apiPhoto ->  new ApiServiceMessage(new ApiServiceExEditPhoto(apiPhoto)))
                            .defaultIfEmpty(new ApiServiceMessage(new ApiServiceExDeletePhoto()))
                            .flatMap(message -> Mono.fromCallable(message::buildContainer)
                                    .flatMap(serviceMsg -> messageService.createMessage(selfId, peer, randomId, Instant.now(), message.getHeader(), serviceMsg)))
                            .mapNotNull(dialogConverter::convert);

                    Mono<List<ApiAbsUser>> apiUsers = userService.getUsers(Collections.singletonList(selfId))
                            .map(user -> Optional.ofNullable(userConverter.convert(context.getAuthId(), user)))
                            .filter(Optional::isPresent)
                            .map(Optional::get)
                            .collectList()
                            .defaultIfEmpty(Collections.emptyList());

                    Mono<List<ApiChat>> apiChats = chatService.getChats(Collections.singletonList(chatId))
                            .map(chat -> Optional.ofNullable(ApiEntityMapper.toApiChat(chat)))
                            .filter(Optional::isPresent)
                            .map(Optional::get)
                            .collectList()
                            .defaultIfEmpty(Collections.emptyList());

                    return updateChatPhoto
                            .then(Mono.zip(apiMessage, apiUsers, apiChats))
                            .map(TupleUtils.function(ResponseStatedMessage::new));
                });
    }

    public Mono<ResponseStatedMessage> handleAddChatUser(DispatchContext<RequestChatsAddChatMember> context) {
        return Mono.justOrEmpty(context.getUserId().map(Id::of))
                .flatMap(selfId -> {
                    final Id chatId = Id.of(context.getRequest().getChatId());
                    final Id userId = Id.of(context.getRequest().getUserId());
                    final long randomId = ThreadLocalRandom.current().nextLong();

                    final ApiServiceMessage serviceMessage =
                            new ApiServiceMessage(new ApiServiceExAddUser(userId.asLong()));

                    final UpdateMessage updateMessage = new UpdateMessage();
                    updateMessage.setPeer(new ApiPeerChat(chatId.asLong()));
                    updateMessage.setSenderUid(selfId.asLong());
                    updateMessage.setRid(randomId);
                    updateMessage.setMessage(serviceMessage);

                    final UpdateChatParticipantAdd updateChatParticipantAdd = new UpdateChatParticipantAdd();
                    updateChatParticipantAdd.setChatId(chatId.asLong());
                    updateChatParticipantAdd.setUid(userId.asLong());
                    updateChatParticipantAdd.setInviterUid(selfId.asLong());

                    Mono<ApiHistoryMessage> apiMessage = Mono.fromCallable(serviceMessage::buildContainer)
                            .flatMap(message -> messageService.createMessage(selfId, Peer.ofChat(chatId), randomId,
                                    Instant.now(), serviceMessage.getHeader(), message))
                            .map(message -> Optional.ofNullable(dialogConverter.convert(message)))
                            .filter(Optional::isPresent)
                            .map(Optional::get);

                    Mono<List<ApiAbsUser>> apiUsers = userService.getUsers(Collections.singletonList(userId))
                            .map(user -> Optional.ofNullable(userConverter.convert(context.getAuthId(), user)))
                            .filter(Optional::isPresent)
                            .map(Optional::get)
                            .collectList()
                            .defaultIfEmpty(Collections.emptyList());

                    Mono<List<ApiChat>> apiChats = chatService.getChats(Collections.singletonList(chatId))
                            .map(chat -> Optional.ofNullable(ApiEntityMapper.toApiChat(chat)))
                            .filter(Optional::isPresent)
                            .map(Optional::get)
                            .collectList()
                            .defaultIfEmpty(Collections.emptyList());

                    return chatService.addChatUser(chatId, userId, selfId)
                            .then(Mono.zip(apiMessage, apiUsers, apiChats))
                            .map(TupleUtils.function(ResponseStatedMessage::new));
                });
    }

    public Mono<ResponseStatedMessage> handleDeleteChatUser(DispatchContext<RequestChatsDeleteChatUser> context) {
        return Mono.justOrEmpty(context.getUserId().map(Id::of))
                .flatMap(selfId -> {
                    final Id chatId = Id.of(context.getRequest().getChatId());
                    final Id userId = Id.of(context.getRequest().getUserId());
                    final Long randomId = ThreadLocalRandom.current().nextLong();

                    final ApiServiceMessage serviceMessage;

                    if (Objects.equals(selfId, userId)) {
                        serviceMessage = new ApiServiceMessage(new ApiServiceExDeleteUser(selfId.asLong()));
                    } else {
                        serviceMessage = new ApiServiceMessage(new ApiServiceExDeleteUser(userId.asLong()));
                    }

                    Mono<ApiHistoryMessage> apiMessage = Mono.fromCallable(serviceMessage::buildContainer)
                            .flatMap(message -> messageService.createMessage(selfId, Peer.ofChat(chatId), randomId,
                                    Instant.now(), serviceMessage.getHeader(), message))
                            .map(message -> Optional.ofNullable(dialogConverter.convert(message)))
                            .filter(Optional::isPresent)
                            .map(Optional::get);

                    Mono<List<ApiAbsUser>> apiUsers = userService.getUsers(Collections.singletonList(userId))
                            .map(user -> Optional.ofNullable(userConverter.convert(context.getAuthId(), user)))
                            .filter(Optional::isPresent)
                            .map(Optional::get)
                            .collectList()
                            .defaultIfEmpty(Collections.emptyList());

                    Mono<List<ApiChat>> apiChats = chatService.getChats(Collections.singletonList(chatId))
                            .map(chat -> Optional.ofNullable(ApiEntityMapper.toApiChat(chat)))
                            .filter(Optional::isPresent)
                            .map(Optional::get)
                            .collectList()
                            .defaultIfEmpty(Collections.emptyList());

                    return chatService.deleteChatUser(chatId, userId, selfId)
                            .then(Mono.zip(apiMessage, apiUsers, apiChats))
                            .map(TupleUtils.function(ResponseStatedMessage::new));
                });
    }

    public Mono<ResponseBool> handleEditChatAdmin(DispatchContext<RequestChatsEditChatAdmin> context) {
        return Mono.justOrEmpty(context.getUserId().map(Id::of))
                .flatMap(selfId -> {
                    final Id chatId = Id.of(context.getRequest().getChatId());
                    final Id userId = Id.of(context.getRequest().getUserId());
                    final boolean isAdmin = context.getRequest().isAdmin();

                    return chatService.editChatAdmin(chatId, userId, selfId, isAdmin)
                            .then(chatService.getChatParticipant(chatId, userId))
                            .map(participant -> participant.isAdmin() == isAdmin)
                            .map(ResponseBool::new);
                });
    }

    public Mono<ResponseStatedMessage> handleEditAdminSettings(DispatchContext<RequestChatsChatSaveAdminSettings> context) {
        final Id chatId = Id.of(context.getRequest().getChatId());
        final PermissionSet permissions = PermissionSet.of(context.getRequest().getPermission());

        return Mono.justOrEmpty(context.getUserId().map(Id::of))
                .flatMap(selfId -> chatService.editChatRights(chatId, selfId, permissions)
                        .thenMany(chatService.getChats(Collections.singletonList(chatId)))
                        .map(chat -> Optional.ofNullable(ApiEntityMapper.toApiChat(chat)))
                        .filter(Optional::isPresent)
                        .map(Optional::get)
                        .collectList()
                        .defaultIfEmpty(Collections.emptyList())
                        .map(chats -> new ResponseStatedMessage(null, Collections.emptyList(), chats)));
    }

    public Mono<ResponseChatsLoadParticipants> handleLoadChatParticipants(DispatchContext<RequestChatsLoadParticipants> context) {
        final Id chatId = Id.of(context.getRequest().getChatId());
        final int limit = context.getRequest().getLimit();
        final int offset = context.getRequest().getOffset();

        return Mono.justOrEmpty(context.getUserId().map(Id::of))
                .filterWhen(selfId -> chatService.isChatParticipant(chatId, selfId))
                .flatMap(selfId -> chatService.getChatParticipants(chatId, limit, offset))
                .defaultIfEmpty(new Page<>(Collections.emptyList()))
                .flatMap(participants -> {
                    Mono<Integer> count = Mono.justOrEmpty((int) participants.getTotalCount())
                            .defaultIfEmpty(0);

                    Mono<List<ApiParticipant>> apiMembers = Flux.fromIterable(participants)
                            .map(participant -> Optional.ofNullable(ApiEntityMapper.convertToApiMember(participant)))
                            .filter(Optional::isPresent)
                            .map(Optional::get)
                            .collectList()
                            .defaultIfEmpty(Collections.emptyList());

                    Mono<List<ApiAbsUser>> apiUsers = Flux.fromIterable(participants)
                            .map(ChatParticipant::getUserId)
                            .collectList()
                            .flatMapMany(userService::getUsers)
                            .map(user -> Optional.ofNullable(userConverter.convert(context.getAuthId(), user)))
                            .filter(Optional::isPresent)
                            .map(Optional::get)
                            .collectList()
                            .defaultIfEmpty(Collections.emptyList());

                    return Mono.zip(count, apiMembers, apiUsers);
                })
                .map(TupleUtils.function(ResponseChatsLoadParticipants::new));
    }
}
