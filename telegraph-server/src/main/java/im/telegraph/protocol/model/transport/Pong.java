package im.telegraph.protocol.model.transport;

import im.telegraph.engine.serialization.DataInputStream;
import im.telegraph.engine.serialization.DataOutputStream;
import im.telegraph.server.MTProtoObject;

import java.io.IOException;

public class Pong extends MTProtoObject {

	public static final int HEADER = 0x02;

	private long randomId;

	public Pong(DataInputStream stream) throws IOException {
		super(stream);
	}

	public Pong(long randomId) {
		this.randomId = randomId;
	}

	public long getRandomId() {
		return randomId;
	}

	@Override
	public int getHeader() {
		return HEADER;
	}

	@Override
	public void writeObject(DataOutputStream outputStream) throws IOException {
		outputStream.writeInt64(randomId);
	}

	@Override
	public MTProtoObject readObject(DataInputStream inputStream) throws IOException {
		randomId = inputStream.readInt64();
		return this;
	}

	@Override
	public String toString() {
		return "Pong{" + randomId + "}";
	}

}
