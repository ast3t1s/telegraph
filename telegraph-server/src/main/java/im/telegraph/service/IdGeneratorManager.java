package im.telegraph.service;

import im.telegraph.domain.entity.Id;
import im.telegraph.service.generator.IdGenerator;

import java.util.Collections;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Supplier;

/**
 * @author Ast3t1s
 */
public class IdGeneratorManager {

    private final ConcurrentMap<String, IdGenerator> generatorMap = new ConcurrentHashMap<>();
    private final Supplier<IdGenerator> supplier;

    public IdGeneratorManager(Supplier<IdGenerator> supplier) {
        this.supplier = Objects.requireNonNull(supplier);
    }

    /**
     * Register resource that need to use the ID generator.
     *
     * @param resource resource name
     */
    public void register(String resource) {
        generatorMap.computeIfAbsent(resource, rs -> supplier.get());
    }

    /**
     * Register resources that need to use the ID generator.
     *
     * @param resources resource name list
     */
    public void register(String... resources) {
        for (String resource : resources) {
            generatorMap.computeIfAbsent(resource, rs -> supplier.get());
        }
    }

    /**
     * request next id by resource name.
     *
     * @param resource resource name
     * @return id
     */
    public Id generateEntityId(final String resource) {
        if (generatorMap.containsKey(resource)) {
            return generatorMap.get(resource).generateId();
        }
        throw new NoSuchElementException(
                "The resource is not registered with the distributed " + "ID resource for the time being.");
    }

    /**
     * Get map of generators
     *
     * @return the map of generators
     */
    public Map<String, IdGenerator> getGeneratorMap() {
        return Collections.unmodifiableMap(generatorMap);
    }
} 
