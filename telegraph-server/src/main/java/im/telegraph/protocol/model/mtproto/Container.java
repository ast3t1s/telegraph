package im.telegraph.protocol.model.mtproto;

import im.telegraph.engine.serialization.DataInputStream;
import im.telegraph.engine.serialization.DataOutputStream;

import java.io.IOException;

public class Container extends ProtoStruct {

	public static final byte HEADER = (byte) 0x0A;

	private ProtoMessage[] messages;

	public Container(DataInputStream stream) throws IOException {
		super(stream);
	}

	public Container(ProtoMessage[] messages) {
		this.messages = messages;
	}

	public ProtoMessage[] getMessages() {
		return messages;
	}

	@Override
	protected byte getHeader() {
		return HEADER;
	}

	@Override
	protected void writeBody(DataOutputStream bs) throws IOException {
		if (messages != null && messages.length > 0) {
			bs.writeHeader(messages.length);
			for (ProtoMessage m : messages) {
				m.writeObject(bs);
			}
		}
		else {
			bs.writeHeader(0);
		}
	}

	@Override
	protected void readBody(DataInputStream bs) throws IOException {
		int size = (int) bs.readHeader();
		messages = new ProtoMessage[size];
		for (int i = 0; i < size; ++i) {
			messages[i] = new ProtoMessage(bs);
		}
	}

	@Override
	public String toString() {
		return "Container[" + messages.length + " items]";
	}

}
