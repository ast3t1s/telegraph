package im.telegraph.api.rpc;

import static im.telegraph.engine.serialization.StreamUtils.*;

import im.telegraph.api.ApiInputPeer;
import im.telegraph.api.ApiMessage;
import im.telegraph.engine.serialization.StreamReader;
import im.telegraph.engine.serialization.StreamWriter;
import im.telegraph.engine.serialization.io.IOContext;
import im.telegraph.rpc.base.Request;
import java.io.IOException;
import java.lang.Override;
import java.lang.String;

/**
 * Generated by the Telegraph API Scheme generator. DO NOT EDIT!
 * @author Telegraph Scheme Generator
 */
public class RequestMessagesSendMessage extends Request<ResponseMessagesSendMessage> {
    public static final int CLASS_ID = 0x5c;

    protected ApiInputPeer peer;

    protected long rid;

    protected long date;

    protected ApiMessage message;

    public RequestMessagesSendMessage() {
    }

    public RequestMessagesSendMessage(ApiInputPeer peer, long rid, long date, ApiMessage message) {
        this.peer = peer;
        this.rid = rid;
        this.date = date;
        this.message = message;
    }

    public static RequestMessagesSendMessage fromBytes(byte[] data) throws IOException {
        return IOContext.fromByteArray(new RequestMessagesSendMessage(), data);
    }

    @Override
    public int getHeader() {
        return CLASS_ID;
    }

    public ApiInputPeer getPeer() {
        return this.peer;
    }

    public void setPeer(ApiInputPeer peer) {
        this.peer = peer;
    }

    public long getRid() {
        return this.rid;
    }

    public void setRid(long rid) {
        this.rid = rid;
    }

    public long getDate() {
        return this.date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public ApiMessage getMessage() {
        return this.message;
    }

    public void setMessage(ApiMessage message) {
        this.message = message;
    }

    @Override
    public void serialize(StreamWriter writer) throws IOException {
        if (this.peer == null) {
            throw new IOException();
        }
        writer.writeSerializable(1, this.peer);
        writer.writeLong(3, this.rid);
        writer.writeLong(4, this.date);
        if (this.message == null) {
            throw new IOException();
        }
        writer.writeBytes(5, this.message.buildContainer());
    }

    @Override
    public void deserialize(StreamReader reader) throws IOException {
        this.peer = reader.readSerializable(1, new ApiInputPeer());
        this.rid = reader.readLong(3);
        this.date = reader.readLong(4);
        this.message = ApiMessage.fromBytes(reader.readByteArray(5));
    }

    @Override
    public String toString() {
        String res = "rpc SendMessage{";
        res += "peer=" + this.peer;
        res += ", rid=" + this.rid;
        res += ", date=" + this.date;
        res += ", message=" + this.message;
        res += "}";
        return res;
    }
}
