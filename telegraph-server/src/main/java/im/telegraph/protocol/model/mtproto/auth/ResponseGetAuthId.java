package im.telegraph.protocol.model.mtproto.auth;

import im.telegraph.engine.serialization.DataInputStream;
import im.telegraph.engine.serialization.DataOutputStream;
import im.telegraph.protocol.model.mtproto.ProtoStruct;

import java.io.IOException;

public class ResponseGetAuthId extends ProtoStruct {

	public static final byte HEADER = (byte) 0x05;

	private long authId;

	public ResponseGetAuthId(DataInputStream inputStream) throws IOException {
		super(inputStream);
	}

	public ResponseGetAuthId(long authId) {
		this.authId = authId;
	}

	public long getAuthId() {
		return authId;
	}

	@Override
	protected byte getHeader() {
		return HEADER;
	}

	@Override
	protected void writeBody(DataOutputStream outputStream) throws IOException {
		outputStream.writeInt64(authId);
	}

	@Override
	protected void readBody(DataInputStream inputStream) throws IOException {
		this.authId = inputStream.readInt64();
	}

	@Override
	public String toString() {
		return "ResponseGetAuthId{" + "authId=" + authId + '}';
	}

}
