package im.telegraph.domain.repository.rdbms;

import im.telegraph.common.rdbms.RdbmsOperations;
import im.telegraph.domain.entity.Id;
import im.telegraph.domain.entity.Presence;
import im.telegraph.domain.repository.PresenceRepository;
import io.r2dbc.spi.*;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * @author Ast3t1s
 */
@Repository
public class RdbmsPresenceRepository extends AbstractRdbmsRepository implements PresenceRepository {

    private static final String COL_USER_ID = "user_id";
    private static final String COL_AUTH_ID = "auth_id";
    private static final String COL_LAST_SEEN_AT  = "last_seen";

    private static final String FIELDS = "user_id, auth_id, last_seen";

    private static final String TABLE_NAME = "presences";

    private static final String GET_PRESENCE_QUERY =
            "SELECT " + FIELDS + " FROM presences WHERE user_id = :userId AND auth_id = :authId";

    private static final String DELETE_BY_PK_QUERY =
            "DELETE FROM presences WHERE user_id = :userId AND auth_id = :authId";

    private static final String DELETE_BY_USER_ID_QUERY =
            "DELETE FROM presences WHERE user_id = :userId";

    private final RdbmsOperations rdbmsOperations;

    private final BiFunction<Row, RowMetadata, Presence> presenceRowMapper = new PresenceRowMapper();
    private final Function<Presence, Map<String, Object>> presenceParameterMapper = new PresenceParameterMapper();

    private final String upsertQuery;

    public RdbmsPresenceRepository(ConnectionFactory connectionFactory) {
        super(connectionFactory);
        this.rdbmsOperations = RdbmsOperations.create(connectionFactory);
        this.upsertQuery = getUpsertQuery();
    }

    @Override
    public Mono<Void> save(Presence presence) {
        return rdbmsOperations.useHandle(handle -> handle
                .createUpdate(upsertQuery)
                .bindMap(presenceParameterMapper.apply(presence))
                .execute());
    }

    @Override
    public Mono<Presence> findByAuthIdAndUserId(Long authId, Id userId) {
        return rdbmsOperations.withHandle(handle -> handle.createQuery(GET_PRESENCE_QUERY)
                .mapRow(presenceRowMapper))
                .next();
    }

    @Override
    public Mono<Boolean> deleteByUserIdAndAuthId(Id userId, Long authId) {
        return rdbmsOperations.withHandle(handle -> handle.createUpdate(DELETE_BY_PK_QUERY)
                .bind("userId", userId.asLong())
                .bind("authId", authId)
                .execute()
                .map(result -> result > 0))
                .next();
    }

    @Override
    public Mono<Long> deleteByUserId(Id userId) {
        return rdbmsOperations.withHandle(handle -> handle.createUpdate(DELETE_BY_USER_ID_QUERY)
                .bind("userId", userId.asLong())
                .execute())
                .next();
    }

    public String getUpsertQuery() {
        Map<String, Object> params = new HashMap<>();
        params.put("user_id", "userId");
        params.put("auth_id", "authId");
        params.put("last_seen", "lastSeenAt");
        return prepareUpsert(TABLE_NAME, params, List.of(COL_USER_ID, COL_AUTH_ID), List.of(COL_LAST_SEEN_AT));
    }

    static final class PresenceParameterMapper implements Function<Presence, Map<String, Object>> {

        @Override
        public Map<String, Object> apply(Presence presence) {
            final Map<String, Object> parameters = new HashMap<>();
            parameters.put("userId", Parameters.in(R2dbcType.BIGINT, presence.getUserId().asLong()));
            parameters.put("authId", Parameters.in(R2dbcType.BIGINT, presence.getAuthId()));
            parameters.put("lastSeenAt", LocalDateTime.ofInstant(presence.getLastSeenAt(), ZoneOffset.UTC));
            return parameters;
        }
    }

    static final class PresenceRowMapper implements BiFunction<Row, RowMetadata, Presence> {

        @Override
        public Presence apply(Row row, RowMetadata metadata) {
            final var authId = row.get("authId", Long.class);
            final var userId = row.get("user_id", Long.class);
            final var lastSeenAt = row.get("last_seen", LocalDateTime.class).toInstant(ZoneOffset.UTC);
            return new Presence(authId, Id.of(userId), lastSeenAt);
        }
    }
}
