package im.telegraph.pubsub.internal;

/**
 * @author Ast3t1s
 */
public class Send extends PubSubPayload<Send.Data> {

    public static final int OP = 5;

    public Send(String topic, Object payload) {
        this(OP, new Data(topic, payload));
    }

    public Send(int op, Data data) {
        super(op, data);
    }

    public static class Data {

        private final String topic;
        private final Object payload;

        public Data(String topic, Object payload) {
            this.topic = topic;
            this.payload = payload;
        }

        public String getTopic() {
            return topic;
        }

        public Object getPayload() {
            return payload;
        }
    }
} 
