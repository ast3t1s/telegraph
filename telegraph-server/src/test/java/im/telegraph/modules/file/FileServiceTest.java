package im.telegraph.modules.file;

import im.telegraph.TelegraphApplication;
import im.telegraph.common.ACLHashGenerator;
import im.telegraph.domain.entity.FileUpload;
import im.telegraph.domain.entity.Id;
import im.telegraph.domain.repository.FileRepository;
import im.telegraph.file.s3.S3FileStorage;
import im.telegraph.service.FileService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.time.Instant;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@SpringBootTest(classes = TelegraphApplication.class)
class FileServiceTest {

    @MockBean
    private FileRepository fileRepository;

    /*@MockBean
    private FilePartRepository filePartRepository;*/

    @MockBean
    private S3FileStorage storeOperations;

    @MockBean
    private ACLHashGenerator hashGenerator;

    @Autowired
    private FileService fileService;

    @Test
    public void test_file_correct() {
        FileUpload fileUpload = mock(FileUpload.class);

        when(fileUpload.getAccessSalt()).thenReturn("some-salt");
        when(this.fileRepository.findById(Id.of(1L))).thenReturn(Mono.just(fileUpload));
        when(this.hashGenerator.generateFileAccessHash(anyLong(), anyString())).thenReturn(100L);

        /*StepVerifier.create(this.fileService.getById(1L, 100L))
                .expectSubscription()
                .assertNext(file -> {

                })
                .verifyComplete();*/

        verify(this.hashGenerator, atLeastOnce()).generateFileAccessHash(anyLong(), anyString());
    }

    @Test
    public void test_file_not_found() {
        when(this.fileRepository.findById(any())).thenReturn(Mono.empty());

       /* StepVerifier.create(this.fileService.getById(1L, 100L))
                .expectSubscription();*/
                //.expectError(FileNotFoundException.class)
               // .verify();
    }

    @Test
    public void test_file_invalid_location() {
        FileUpload test = FileUpload.builder()
                .id(Id.of(1L))
                .size(100L)
                .uploadKey("some-key")
                .accessSalt("some-salt")
                .createdAt(Instant.now())
                .build();

        when(this.fileRepository.findById(any())).thenReturn(Mono.just(test));
        when(this.hashGenerator.generateFileAccessHash(anyLong(), anyString())).thenReturn(200L);

        /*StepVerifier.create(this.fileService.getById(1L, 100L))
                .expectSubscription();*/
                //.expectError(InvalidFileLocationException.class)
             //   .verify();
    }

    @Test
    public void test_get_file_url_correct() {
        FileUpload test = FileUpload.builder()
                .id(Id.of(1L))
                .size(100L)
                .uploadKey("some-key")
                .accessSalt("some-salt")
                .createdAt(Instant.now())
                .build();
        when(this.hashGenerator.nextAccessSalt()).thenReturn("some-salt");
        when(this.fileRepository.insert(any(FileUpload.class))).thenReturn(Mono.just(test));
        when(this.storeOperations.getFileUploadUrl(anyString(), any(Duration.class)));
               // .thenReturn(Mono.just(new FileReference("some-url")));

       /* StepVerifier.create(this.fileService.getById(1000))
                .expectSubscription()
                .assertNext(objects -> {
                    assertThat(objects.getT1()).isEqualTo("some-url");
                    assertThat(objects.getT2()).isEqualTo(test);
                })
                .verifyComplete();*/

        verify(this.fileRepository, atLeastOnce()).insert(any(FileUpload.class));
        verify(this.hashGenerator, atLeastOnce()).nextAccessSalt();
        verify(this.hashGenerator, atLeastOnce()).randomHash();
        verify(this.storeOperations, atLeastOnce()).getFileUploadUrl(anyString(), any(Duration.class));
    }
}