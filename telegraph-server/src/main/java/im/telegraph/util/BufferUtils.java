package im.telegraph.util;

import java.nio.ByteBuffer;
import java.util.List;

public final class BufferUtils {

    private BufferUtils() {
    }

    /**
     * Concatenate a list of buffers into a single {@link ByteBuffer}
     */
    public static ByteBuffer concatBuffers(List<ByteBuffer> buffers) {
        int partSize = 0;
        for (ByteBuffer buffer : buffers) {
            partSize += buffer.remaining();
        }
        ByteBuffer partData = ByteBuffer.allocate(partSize);
        for (ByteBuffer buffer : buffers) {
            partData.put(buffer);
        }
        partData.rewind();
        return partData;
    }

    /**
     * Gets the content of the ByteBuffer as a byte[] without mutating the buffer
     * (so thread safe) and minimizing GC (i.e. object creation)
     */
    public static byte[] asByteArray(ByteBuffer buffer) {
        byte[] result;
        if (buffer.hasArray() && buffer.arrayOffset() == 0 && buffer.capacity() == buffer.remaining()) {
            result = buffer.array();
        } else {
            result = new byte[buffer.remaining()];
            if (buffer.hasArray()) {
                System.arraycopy(buffer.array(), buffer.arrayOffset() + buffer.position(), result, 0, result.length);
            } else {
                ByteBuffer duplicate = buffer.duplicate();
                duplicate.mark();
                duplicate.get(result);
                duplicate.reset();
            }
        }
        return result;
    }

    public static byte[] asByteArrayV2(ByteBuffer buffer) {
        byte[] result;
        int remaining = buffer.remaining();
        if (buffer.hasArray() && buffer.arrayOffset() == 0 && buffer.capacity() == remaining) {
            result = buffer.array();
            buffer.position(buffer.position() + remaining);  // advance the position
        } else {
            result = new byte[remaining];
            if (buffer.hasArray()) {
                System.arraycopy(buffer.array(), buffer.arrayOffset() + buffer.position(), result, 0, result.length);
                buffer.position(buffer.position() + remaining);  // advance the position
            } else {
                ByteBuffer duplicate = buffer.duplicate();
                duplicate.get(result);
                buffer.position(buffer.position() + remaining);  // advance the position
            }
        }
        return result;
    }

} 
