package im.telegraph.common;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DateTimeFactory {

    private static final TimeZone DEFAULT_TZ = TimeZone.getTimeZone(ZoneOffset.UTC);

    private static final String DATE_PATTERN_VALUE = "(\\d\\d\\d\\d)-(\\d\\d)-(\\d\\d)";
    private static final String TIME_PATTERN_VALUE = "(\\d\\d):(\\d\\d):(\\d\\d)";
    private static final String TZ_PATTERN_VALUE = "(([+-]\\d\\d:\\d\\d)|Z)";

    // time zone is required for date times
    private static final Pattern DATE_TIME_PATTERN = Pattern.compile("^" + DATE_PATTERN_VALUE + "T"
            + TIME_PATTERN_VALUE + TZ_PATTERN_VALUE + "$");

    private static final Pattern DATE_PATTERN = Pattern.compile("^" + DATE_PATTERN_VALUE + "$");

    // time zone is optional for times
    private static final Pattern TIME_PATTERN = Pattern.compile("^" + TIME_PATTERN_VALUE + TZ_PATTERN_VALUE + "?$");

    private static final DateTimeFormatter utcDateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'")
            .withZone(DEFAULT_TZ.toZoneId());

    private static final DateTimeFormatter utcDateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
            .withZone(DEFAULT_TZ.toZoneId());

    private static final DateTimeFormatter utcTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss'Z'")
            .withZone(DEFAULT_TZ.toZoneId());

    private volatile Clock clock = Clock.systemUTC();

    public TimeZone getTimeZone() {
        return DEFAULT_TZ;
    }

    public Clock getClock() {
        return clock;
    }

    public void setClock(Clock clock) {
        this.clock = Objects.requireNonNull(clock, "'clock' must not be null");
    }

    public LocalDateTime now() {
        return LocalDateTime.now(getClock());
    }

    public String getDateTimeInUTC(LocalDateTime time) {
        return utcDateTimeFormatter.format(time.toInstant(ZoneOffset.UTC));
    }

    public String getDateTimeInUTC(ZonedDateTime dateTime){
        return getDateTimeInUTC(dateTime.toLocalDateTime());
    }

    public String getDateInUTC(LocalDateTime time) {
        return utcDateFormatter.format(time);
    }

    public String getTimeInUTC(LocalDateTime time) {
        return utcTimeFormatter.format(time);
    }

    /**
     * Parses a date time compliant with ISO-8601.
     * @param time The date time string
     * @return A {@link LocalDateTime} representing the date time, in the
     *  time zone specified by the input string
     * @throws IllegalArgumentException If the input string is not a valid date time
     *  e.g. the time zone is missing
     */
    public ZonedDateTime fromDateTime(String time) {
        Matcher matcher = DATE_TIME_PATTERN.matcher(time);

        if (matcher.find()) {
            int year = Integer.valueOf(matcher.group(1));
            int month = Integer.valueOf(matcher.group(2));
            int day = Integer.valueOf(matcher.group(3));
            int hour = Integer.valueOf(matcher.group(4));
            int minute = Integer.valueOf(matcher.group(5));
            int second = Integer.valueOf(matcher.group(6));
            String tzValue = matcher.group(7);
            TimeZone tz;
            if (tzValue.equals("Z")) {
                tz = DEFAULT_TZ;
            } else {
                tz = TimeZone.getTimeZone("GMT" + tzValue);
            }
            LocalDateTime dateTime = LocalDateTime.of(year, month - 1, day, hour, minute, second);
            return dateTime.atZone(tz.toZoneId());
        } else {
            throw new IllegalArgumentException("Invalid date time: " + time);
        }
    }

    /**
     * Parses a time, compliant with ISO-8601 and XEP-0082.
     * @param time The time string
     * @return A {@link LocalDateTime} representing the time, in the
     *  time zone specified by the input string. If a time zone is not specified
     *  in the input string, the returned {@link LocalDateTime} will be in the UTC time zone
     * @throws IllegalArgumentException If the input string is not a valid time
     */
    public LocalDateTime fromTime(String time) {
        Matcher matcher = TIME_PATTERN.matcher(time);

        if (matcher.find()) {
            int hour = Integer.valueOf(matcher.group(1));
            int minute = Integer.valueOf(matcher.group(2));
            int second = Integer.valueOf(matcher.group(3));
            String tzValue = matcher.group(4);
            TimeZone tz;
            if (tzValue == null || tzValue.equals("Z")) {
                tz = DEFAULT_TZ;
            } else {
                tz = TimeZone.getTimeZone("GMT" + tzValue);
            }
            LocalDateTime localDateTime = LocalDateTime.now(tz.toZoneId());
            return localDateTime.withHour(hour).withMinute(minute).withSecond(second);
        } else {
            throw new IllegalArgumentException("Invalid date time: " + time);
        }
    }

    /**
     * Parses a date, compliant with ISO-8601 and.
     * @param time The date string
     * @return A {@link LocalDateTime} representing the date
     * @throws IllegalArgumentException If the input string is not a valid date
     */
    public LocalDateTime fromDate(String time) {
        Matcher matcher = DATE_PATTERN.matcher(time);

        if (matcher.find()) {
            int year = Integer.parseInt(matcher.group(1));
            int month = Integer.parseInt(matcher.group(2));
            int day = Integer.parseInt(matcher.group(3));

            return LocalDateTime.of(year, month - 1, day, 0, 0);
        } else {
            throw new IllegalArgumentException("Invalid date time: " + time);
        }
    }
} 
