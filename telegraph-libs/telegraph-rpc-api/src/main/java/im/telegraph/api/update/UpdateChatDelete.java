package im.telegraph.api.update;

import static im.telegraph.engine.serialization.StreamUtils.*;

import im.telegraph.api.ApiPeer;
import im.telegraph.engine.serialization.StreamReader;
import im.telegraph.engine.serialization.StreamWriter;
import im.telegraph.engine.serialization.io.IOContext;
import im.telegraph.rpc.base.Update;
import java.io.IOException;
import java.lang.Override;
import java.lang.String;

/**
 * Generated by the Telegraph API Scheme generator. DO NOT EDIT!
 * @author Telegraph Scheme Generator
 */
public class UpdateChatDelete extends Update {
    public static final int CLASS_ID = 0x30;

    protected ApiPeer peer;

    public UpdateChatDelete() {
    }

    public UpdateChatDelete(ApiPeer peer) {
        this.peer = peer;
    }

    public static UpdateChatDelete fromBytes(byte[] data) throws IOException {
        return IOContext.fromByteArray(new UpdateChatDelete(), data);
    }

    public ApiPeer getPeer() {
        return this.peer;
    }

    public void setPeer(ApiPeer peer) {
        this.peer = peer;
    }

    @Override
    public void deserialize(StreamReader reader) throws IOException {
        this.peer = ApiPeer.fromBytes(reader.readByteArray(1));
    }

    @Override
    public void serialize(StreamWriter writer) throws IOException {
        if (this.peer == null) {
            throw new IOException();
        }
        writer.writeBytes(1, this.peer.buildContainer());
    }

    @Override
    public String toString() {
        String res = "update ChatDelete{";
        res += "peer=" + this.peer;
        res += "}";
        return res;
    }

    @Override
    public int getHeader() {
        return CLASS_ID;
    }
}
