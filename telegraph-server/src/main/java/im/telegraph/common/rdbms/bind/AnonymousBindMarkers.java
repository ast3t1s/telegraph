package im.telegraph.common.rdbms.bind;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;

/**
 * @author Ast3t1s
 */
public class AnonymousBindMarkers implements BindMarkers {

    private static final AtomicIntegerFieldUpdater<AnonymousBindMarkers> COUNTER_INCREMENTER =
            AtomicIntegerFieldUpdater.newUpdater(AnonymousBindMarkers.class, "counter");

    private final String placeholder;

    // access via COUNTER_INCREMENTER
    @SuppressWarnings("unused")
    private volatile int counter;

    /**
     * Create a new {@link AnonymousBindMarkers} instance given {@code placeholder}.
     * @param placeholder parameter bind marker
     */
    AnonymousBindMarkers(String placeholder) {
        this.placeholder = placeholder;
    }

    @Override
    public BindMarker next() {
        int index = COUNTER_INCREMENTER.getAndIncrement(this);
        return new IndexedBindMarkers.IndexedBindMarker(placeholder, index);
    }
} 
