package im.telegraph.common.rdbms.bind;

import java.util.Objects;
import java.util.function.Function;

/**
 * @author Ast3t1s
 */
@FunctionalInterface
public interface BindMarkersFactory {
    /**
     * Create a new {@link BindMarkers} instance.
     */
    BindMarkers create();

    /**
     * Return whether the {@link BindMarkersFactory} uses identifiable placeholders:
     * {@code false} if multiple placeholders cannot be distinguished by just the
     * {@link BindMarker#getPlaceholder() placeholder} identifier.
     */
    default boolean identifiablePlaceholders() {
        return true;
    }

    static BindMarkersFactory indexed(String prefix, int beginWith) {
        return () -> new IndexedBindMarkers(prefix, beginWith);
    }

    static BindMarkersFactory anonymous(String placeholder) {
        Objects.requireNonNull(placeholder, "Placeholder must not be empty");
        return new BindMarkersFactory() {
            @Override
            public BindMarkers create() {
                return new AnonymousBindMarkers(placeholder);
            }
            @Override
            public boolean identifiablePlaceholders() {
                return false;
            }
        };
    }

    static BindMarkersFactory named(String prefix, String namePrefix, int maxLength) {
        return named(prefix, namePrefix, maxLength, Function.identity());
    }

    static BindMarkersFactory named(String prefix, String namePrefix, int maxLength,
                                    Function<String, String> hintFilterFunction) {
        return () -> new NamedBindMarkers(prefix, namePrefix, maxLength, hintFilterFunction);
    }
}
