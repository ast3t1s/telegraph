package im.telegraph.util;

public interface ProfileConstants {

    String SPRING_PROFILE_DEVELOPMENT = "dev";

    String SPRING_PROFILE_TEST = "test";

    String SPRING_PROFILE_PRODUCTION = "prod";

    String SPRING_PROFILE_CLOUD = "cloud";

    String SPRING_PROFILE_NO_LIQUIBASE = "no-liquibase";

}
