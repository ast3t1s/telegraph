package im.telegraph.common.property.file;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.boot.convert.DataSizeUnit;
import org.springframework.lang.Nullable;
import org.springframework.util.unit.DataSize;
import org.springframework.util.unit.DataUnit;

/**
 * @author Ast3t1s
 */
@Getter
@Setter
@Accessors(chain = true)
public class S3Properties {

    /**
     * Flag whether or not s3 file storage enabled
     */
    private boolean enabled;

    /**
     * AWS S3 access key.
     */
    private String accessKey;

    /**
     * AWS S3 secret key.
     */
    private String secretKey;

    /**
     * AWS region.
     */
    private String region;

    /**
     * AWS endpoint.
     */
    @Nullable
    private String endpoint;

    /**
     * AWS S3 bucket.
     */
    private String bucket;

    /**
     * Multipart chunk size.
     */
    @DataSizeUnit(DataUnit.MEGABYTES)
    private DataSize chunkSize = DataSize.ofMegabytes(5);
} 
