package im.telegraph.common.email;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public final class Email {

    private String from;
    private String to;
    private String subject;
    private String content;
    private boolean isMultipart;
    private boolean isHtml;
    private Map<String, Object> params = new HashMap<>();

    public Email() {}

    public Email(Email other) {
        this.from = other.getFrom();
        this.to = other.getTo();
        this.subject = other.getSubject();
        this.content = other.getContent();
        this.isMultipart = other.isMultipart();
        this.isHtml = other.isHtml();
        this.params = other.getParams() != null ? new HashMap<>(other.getParams()) : Collections.emptyMap();
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isMultipart() {
        return isMultipart;
    }

    public void setMultipart(boolean multipart) {
        isMultipart = multipart;
    }

    public boolean isHtml() {
        return isHtml;
    }

    public void setHtml(boolean html) {
        isHtml = html;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }

    @Override
    public String toString() {
        return "Email{" +
                "from='" + from + '\'' +
                ", to='" + to + '\'' +
                ", subject='" + subject + '\'' +
                ", content='" + content + '\'' +
                ", isMultipart=" + isMultipart +
                ", isHtml=" + isHtml +
                ", params=" + params +
                '}';
    }

    public static final class EmailBuilder {

        private final Email email = new Email();

        public EmailBuilder from(String from) {
            this.email.setFrom(from);
            return this;
        }

        public EmailBuilder to(String to) {
            this.email.setTo(to);
            return this;
        }

        public EmailBuilder subject(String subject) {
            this.email.setSubject(subject);
            return this;
        }

        public EmailBuilder content(String content) {
            this.email.setContent(content);
            return this;
        }

        public EmailBuilder isMultipart(boolean isMultipart) {
            this.email.setMultipart(isMultipart);
            return this;
        }

        public EmailBuilder isHtml(boolean isHtml) {
            this.email.setHtml(isHtml);
            return this;
        }

        public EmailBuilder params(Map<String, Object> params) {
            this.email.setParams(params);
            return this;
        }

        public Email build() {
            return this.email;
        }
    }
}
