package im.telegraph.protocol.auth;

import im.telegraph.protocol.MTProtoException;

import java.io.Serial;

/**
 * @author Ast3t1s
 */
public final class AuthorizationException extends MTProtoException {

    @Serial
    private static final long serialVersionUID = -6906762176574917782L;

    public AuthorizationException() {
    }

    public AuthorizationException(Throwable cause) {
        super(cause);
    }

    public AuthorizationException(String message) {
        super(message);
    }
}
