package im.telegraph.handlers.dispatch;

import im.telegraph.rpc.base.Request;
import im.telegraph.rpc.base.Response;
import reactor.util.annotation.Nullable;

import java.util.Optional;

/**
 * Context for rpc request's execution.
 *
 * @author Ast3t1s
 */
public class DispatchContext<T extends Request<? extends Response>> {

    private final long authId;
    private final long sessionId;
    @Nullable
    private final Long userId;
    @Nullable
    private final String address;
    private final T request;

    /**
     * Constructs a {@code DispatchContext}
     *
     * @param authId  The authorization key identifier.
     * @param userId  The identifier of the user.
     * @param address The remote address of the user.
     * @param request The request for possible execution.
     */
    private DispatchContext(long authId, long sessionId, @Nullable Long userId, @Nullable String address, T request) {
        this.authId = authId;
        this.sessionId = sessionId;
        this.userId = userId;
        this.address = address;
        this.request = request;
    }

    public static <T extends Request<? extends Response>> DispatchContext<T> of(long authId, long sessionId,
                                                                                @Nullable Long userId,
                                                                                @Nullable String address, T request) {
        return new DispatchContext<>(authId, sessionId, userId, address, request);
    }

    /**
     * Gets the authorization key identifier.
     *
     * @return authorization key identifier.
     */
    public long getAuthId() {
        return authId;
    }

    /**
     * Gets the session identifier.
     *
     * @return session identifier.
     */
    public long getSessionId() {
        return sessionId;
    }

    /**
     * Gets the user identifier.
     *
     * @return user identifier.
     */
    public Optional<Long> getUserId() {
        return Optional.ofNullable(userId);
    }

    /**
     * Gets the remote address of the client.
     *
     * @return the remote address of the client.
     */
    public Optional<String> getAddress() {
        return Optional.ofNullable(address);
    }

    /**
     * Gets the request for execution.
     *
     * @return The request for execution.
     */
    public T getRequest() {
        return request;
    }

    @Override
    public String toString() {
        return "DispatchContext{" +
                "authId=" + authId +
                ", sessionId=" + sessionId +
                ", userId=" + userId +
                ", address='" + address + '\'' +
                ", request=" + request +
                '}';
    }
}
