package im.telegraph.cluster;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author Ast3t1s
 */
public class MembershipState implements Serializable {

    private static final long serialVersionUID = 1L;

    private final Member member;
    private final MemberStatus status;
    private final int incarnation;

    public MembershipState(Member member, MemberStatus status, int incarnation) {
        this.member = member;
        this.status = status;
        this.incarnation = incarnation;
    }

    public Member member() {
        return member;
    }

    public MemberStatus status() {
        return status;
    }

    public boolean isAlive() {
        return status == MemberStatus.ALIVE;
    }

    public boolean isSuspect() {
        return status == MemberStatus.SUSPECT;
    }

    public boolean isLeaving() {
        return status == MemberStatus.LEAVING;
    }

    public boolean isDead() {
        return status == MemberStatus.DEAD;
    }

    public int incarnation() {
        return incarnation;
    }

    /**
     * Checks either this record overrides given record.
     *
     * @param r0 existing record in membership table
     * @return true if this record overrides exiting; false otherwise
     */
    public boolean isOverrides(MembershipState r0) {
        if (r0 == null) {
            return isAlive() || isLeaving();
        }
        if (!Objects.equals(member.getUniqueAddress(), r0.member.getUniqueAddress())) {
            throw new IllegalArgumentException("Can't compare records for different members");
        }
        if (this.equals(r0)) {
            return false;
        }
        if (r0.isDead()) {
            return false;
        }
        if (isDead()) {
            return true;
        }
        if (incarnation == r0.incarnation) {
            return isSuspect() && (r0.isAlive() || r0.isLeaving());
        } else {
            return incarnation > r0.incarnation;
        }
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null || getClass() != that.getClass()) {
            return false;
        }
        MembershipState record = (MembershipState) that;
        return incarnation == record.incarnation
                && Objects.equals(member, record.member)
                && status == record.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(member, status, incarnation);
    }

    public enum MemberStatus {

        /** Member is reachable and responding on messages. */
        ALIVE,

        /** Member can't be reached and marked as suspected to be failed. */
        SUSPECT,

        /** Member want to leave cluster gracefully. */
        LEAVING,

        /**
         * Member declared as dead after being {@link #SUSPECT} for configured time or when node has been
         * gracefully shutdown and left cluster.
         */
        DEAD
    }

    @Override
    public String toString() {
        return "MembershipState{" +
                "member=" + member +
                ", status=" + status +
                ", incarnation=" + incarnation +
                '}';
    }
} 
