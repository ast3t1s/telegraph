package im.telegraph.generator.generator.javapoet;

import com.squareup.javapoet.*;
import im.telegraph.generator.generator.Constants;
import im.telegraph.generator.generator.SchemeContainerGenerator;
import im.telegraph.generator.generator.SchemeGenerator;
import im.telegraph.generator.type.SchemeDefinition;
import im.telegraph.generator.type.SchemeUpdateBox;

import javax.lang.model.element.Modifier;
import java.io.File;
import java.io.IOException;

public class UpdateBoxGenerator implements SchemeGenerator {

    private final SchemeContainerGenerator<TypeSpec.Builder> containerGenerator = new JavaPoetContainerGenerator();

    @Override
    public void generate(SchemeDefinition definition, String out) throws IOException {
        long start = System.currentTimeMillis();
        for (SchemeUpdateBox u : definition.getAllUpdateBoxes()) {
            String javaName = TypeHelper.getUpdateBoxName(u.getName());
            TypeSpec.Builder clazz = TypeSpec.classBuilder(javaName)
                    .addJavadoc(Constants.NOTICE)
                    .addJavadoc(Constants.DEFAULT_AUTHOR)
                    .addModifiers(Modifier.PUBLIC)
                    .superclass(Constants.TYPE_RPC)
                    .addField(FieldSpec.builder(int.class, Constants.HEADER)
                            .addModifiers(Modifier.PUBLIC, Modifier.STATIC, Modifier.FINAL)
                            .initializer("0x$L", Integer.toHexString(u.getHeader()))
                            .build())
                    .addMethod(MethodSpec.methodBuilder("fromBytes")
                            .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                            .addException(IOException.class)
                            .addParameter(byte[].class, "data")
                            .returns(ClassName.get("", javaName))
                            .addStatement("return $T.fromByteArray(new $T(), data)", Constants.TYPE_CONTEXT,
                                    ClassName.get("", javaName))
                            .build());

            containerGenerator.generateFields(clazz, definition, u);

            if (u.getAttributes().size() > 0) {
                containerGenerator.generateConstructor(clazz, definition, u);
            }

            clazz.addMethod(MethodSpec.constructorBuilder().addModifiers(Modifier.PUBLIC)
                    .build());

            containerGenerator.generateGettersAndSetters(clazz, definition, u);
            containerGenerator.generateDeserialization(clazz, definition, u);
            containerGenerator.generateSerialization(clazz, definition, u);
            containerGenerator.generateToString(clazz, definition, u);

            clazz.addMethod(MethodSpec.methodBuilder(Constants.CLASS_ID_METHOD)
                    .addModifiers(Modifier.PUBLIC)
                    .returns(int.class)
                    .addAnnotation(Override.class)
                    .addStatement("return $L", Constants.HEADER)
                    .build());

            JavaFile.builder(Constants.PACKAGE_BASE, clazz.build())
                    .addStaticImport(Constants.TYPE_UTILS, "*")
                    .indent("    ")
                    .build().writeTo(new File(out));
        }
        System.out.println("Total update boxs " + definition.getAllUpdateBoxes().size() + " generated in "
                + (System.currentTimeMillis() - start) + " ms");
    }
}
