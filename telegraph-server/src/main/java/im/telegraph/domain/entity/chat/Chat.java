package im.telegraph.domain.entity.chat;

import im.telegraph.domain.entity.Id;
import lombok.Data;
import reactor.util.annotation.Nullable;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import java.util.Optional;

@Data
public class Chat implements Serializable {

    public static final String ENTITY_NAME = Chat.class.getSimpleName();

    private static final long serialVersionUID = 1L;

    /**
     * Chat identifier.
     */
    private final Id id;
    private final Id ownerUserId;
    private final String title;
    private final String accessSalt;
    @Nullable
    private final String description;
    private final Integer membersCount;
    private final Long permissions;
    @Nullable
    private final ChatPhoto photo;
    private final Instant createdAt;
    private final Instant updatedAt;
    private final Id updatedById;

    /**
     * Construct an {@code Chat instance} with the given params.
     *
     * @param id           Chat identifier.
     * @param ownerUserId       User identifier of the creator of the chat.
     * @param title        Chat title.
     * @param description  Chat description. Updated only after the chat is opened.
     * @param membersCount Number of participants in the chat.
     * @param permissions  Actions that non-administrator chat members are allowed to take in the chat.
     * @param photo        Chat photo; may be null if empty or unknown.
     * @param createdAt    Date (Unix timestamp) when chat was created.
     * @param updatedAt    Date (Unix timestamp) when chat has been updated.
     * @param updatedById  User identifier of the user who update chat last time.
     */
    @lombok.Builder(builderClassName = "Builder", toBuilder = true)
    Chat(final Id id, final Id ownerUserId, final String title, final String accessSalt,
         @Nullable final String description, final Integer membersCount, final Long permissions,
         @Nullable final ChatPhoto photo, final Instant createdAt, final Instant updatedAt, final Id updatedById) {
        this.id = id;
        this.ownerUserId = Objects.requireNonNull(ownerUserId);
        this.title = Objects.requireNonNull(title);
        this.accessSalt = accessSalt;
        this.description = description;
        this.membersCount = membersCount;
        this.permissions = Objects.requireNonNull(permissions);
        this.photo = photo;
        this.createdAt = Objects.requireNonNull(createdAt);
        this.updatedAt = Objects.requireNonNull(updatedAt);
        this.updatedById = Objects.requireNonNull(updatedById);
    }

    public String getTitle() {
        return title;
    }

    public Id getOwnerUserId() {
        return ownerUserId;
    }

    public String getAccessSalt() {
        return accessSalt;
    }

    public PermissionSet getPermissionsSet() {
        return PermissionSet.of(permissions);
    }

    /**
     * Returns the chat description if present, otherwise {@code Optional#empty}
     *
     * @return The chat description, if present
     */
    public Optional<String> getDescription() {
        return Optional.ofNullable(description);
    }

    /**
     * Returns the chat photo if present, otherwise {@code Optional#empty}
     *
     * @return The {@link ChatPhoto} object, if present
     */
    public Optional<ChatPhoto> getChatPhoto() {
        return Optional.ofNullable(photo);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Chat chat = (Chat) o;
        return Objects.equals(id, chat.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    @Override
    public String toString() {
        return "Chat{" +
                "id=" + id +
                ", userId=" + ownerUserId +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", membersCount=" + membersCount +
                ", permissions=" + permissions +
                ", photo=" + photo +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                ", updatedById=" + updatedById +
                '}';
    }
}
