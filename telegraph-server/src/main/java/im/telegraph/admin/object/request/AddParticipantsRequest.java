package im.telegraph.admin.object.request;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Ast3t1s
 */
public class AddParticipantsRequest {

    @JsonProperty("user_id")
    private Integer userId;
    @JsonProperty("inviter_id")
    private Integer inviterId;

    public Integer getUserId() {
        return userId;
    }

    public Integer getInviterId() {
        return inviterId;
    }

    @Override
    public String toString() {
        return "AddParticipantsRequest{" +
                "userId=" + userId +
                ", inviterId=" + inviterId +
                '}';
    }
}
