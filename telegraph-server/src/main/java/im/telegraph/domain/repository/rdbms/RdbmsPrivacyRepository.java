package im.telegraph.domain.repository.rdbms;

import im.telegraph.common.rdbms.RdbmsOperations;
import im.telegraph.domain.entity.Id;
import im.telegraph.domain.entity.Privacy;
import im.telegraph.domain.repository.PrivacyRepository;
import io.r2dbc.spi.*;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * @author Ast3t1s
 */
@Repository
public class RdbmsPrivacyRepository extends AbstractRdbmsRepository implements PrivacyRepository {

    private static final String FIELDS = "user_id, blocked_id, created_at";

    private static final String INSERT_PRIVACY_QUERY =
            "INSERT INTO privacy (" + FIELDS + ") VALUES (:userId, :blockedId, :createdAt)";

    private static final String GET_PRIVACY_QUERY =
            "SELECT " + FIELDS + " FROM privacy WHERE user_id = :userId AND blocked_id = :blockedId";

    private static final String LIST_PRIVACY_QUERY =
            "SELECT " + FIELDS + " FROM privacy WHERE user_id = :userId ORDER BY blocked_id";

    private static final String GET_COUNT_QUERY =
            "SELECT COUNT(*) FROM privacy AS count WHERE user_id = :userId";

    private static final String DELETE_PRIVACY_QUERY =
            "DELETE FROM privacy WHERE user_id = :userId AND blocked_id = :blockedId";

    private final RdbmsOperations rdbmsOperations;

    private final BiFunction<Row, RowMetadata, Privacy> privacyRowMapper = new PrivacyRowMapper();
    private final Function<Privacy, Map<String, Parameter>> privacyParametersMapper = new PrivacyParametersMapper();

    public RdbmsPrivacyRepository(ConnectionFactory connectionFactory) {
        super(connectionFactory);
        this.rdbmsOperations = RdbmsOperations.create(connectionFactory);
    }

    @Override
    public Mono<Void> insert(Privacy privacy) {
        return rdbmsOperations.useHandle(handle -> handle.createUpdate(INSERT_PRIVACY_QUERY)
                .bindMap(privacyParametersMapper.apply(privacy))
                .execute());
    }

    @Override
    public Mono<Privacy> findByUserIdAndBlockedId(Id userId, Id blockedId) {
        return rdbmsOperations.withHandle(handle -> handle.createQuery(GET_PRIVACY_QUERY)
                .bind("userId", userId.asLong())
                .bind("blockedId", blockedId.asLong())
                .mapRow(privacyRowMapper))
                .next();
    }

    @Override
    public Flux<Privacy> findAllByUserId(Id userId, int limit, int offset) {
        return rdbmsOperations.withHandle(handle -> handle
                .createQuery(limitQuery(LIST_PRIVACY_QUERY, limit, offset))
                .mapRow(privacyRowMapper));
    }

    @Override
    public Mono<Long> countByUserId(Id userId) {
        return rdbmsOperations.withHandle(handle -> handle.createQuery(GET_COUNT_QUERY)
                .bind("userId", userId.asLong())
                .mapRow(row -> row.get("count", Long.class)))
                .next();
    }

    @Override
    public Mono<Boolean> deleteByUserIdAndBlockedId(Id userId, Id blockedId) {
        return rdbmsOperations.withHandle(handle -> handle.createUpdate(DELETE_PRIVACY_QUERY)
                .bind("userId", userId.asLong())
                .bind("blockedId", blockedId.asLong())
                .execute()
                .map(updated -> updated > 0))
                .next();
    }

    static final class PrivacyParametersMapper implements Function<Privacy, Map<String, Parameter>> {

        @Override
        public Map<String, Parameter> apply(Privacy privacy) {
            final Map<String, Parameter> parameters = new HashMap<>();
            parameters.put("userId", Parameters.in(R2dbcType.BIGINT, privacy.getUserId().asLong()));
            parameters.put("blockedId", Parameters.in(R2dbcType.BIGINT, privacy.getBlockedId().asLong()));
            parameters.put("createdAt", Parameters.in(R2dbcType.TIMESTAMP,
                    LocalDateTime.ofInstant(privacy.getCreatedAt(), ZoneOffset.UTC)));
            return parameters;
        }
    }

    static final class PrivacyRowMapper implements BiFunction<Row, RowMetadata, Privacy> {

        @Override
        public Privacy apply(Row row, RowMetadata metadata) {
            final var userId = row.get("user_id", Long.class);
            final var blockedId = row.get("blocked_id", Long.class);
            final var createdAt = row.get("created_at", LocalDateTime.class);
            return new Privacy(Id.of(userId), Id.of(blockedId), createdAt.toInstant(ZoneOffset.UTC));
        }
    }
}
