package im.telegraph.generator.type;

public class SchemeResponseAnonymous extends SchemeBaseResponse {
    private SchemeRpc rpc;

    public SchemeResponseAnonymous(int header) {
        super(header);
    }

    public void setRpc(SchemeRpc rpc) {
        this.rpc = rpc;
    }

    public SchemeRpc getRpc() {
        return rpc;
    }
}
