package im.telegraph.generator.type;

public class SchemeEnumType extends SchemeType {
    private String name;

    public SchemeEnumType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }
}