package im.telegraph.internal.remote.serializer;

/**
 * A serializer represents a bimap between object and {@code byte[]} representing that object.
 *
 * @author Ast3t1s
 */
public interface Serializer {

    /**
     * Completely unique value to identify this implementation of Serializer, used to optimize network traffic.
     */
    int serializerId();

    /**
     * Serializes the given object into an Array of Byte.
     *
     * Note that the array must not be mutated by the serializer after it has been returned.
     */
    byte[] toBinary(Object object) throws SerializationException;

    /**
     * Produces an object from an array of bytes, with an optional type-hint;
     */
    Object fromBinary(byte[] array) throws SerializationException;

    /**
     * Produces an object from an array of bytes, with an optional type-hint;
     *
     * @return deserialize object
     */
    default <T> T fromBinary(byte[] array, Class<T> clazz) {
        return clazz.cast(fromBinary(array));
    }
}
