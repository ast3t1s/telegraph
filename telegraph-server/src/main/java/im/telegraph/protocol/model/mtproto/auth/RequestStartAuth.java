package im.telegraph.protocol.model.mtproto.auth;

import im.telegraph.engine.serialization.DataInputStream;
import im.telegraph.engine.serialization.DataOutputStream;
import im.telegraph.protocol.model.mtproto.ProtoStruct;

import java.io.IOException;

public class RequestStartAuth extends ProtoStruct {

	public static final int HEADER = 0x07;

	private long randomId;

	public RequestStartAuth(DataInputStream inputStream) throws IOException {
		super(inputStream);
	}

	public RequestStartAuth(long randomId) {
		this.randomId = randomId;
	}

	public long getRandomId() {
		return randomId;
	}

	@Override
	protected byte getHeader() {
		return (byte) HEADER;
	}

	@Override
	protected void writeBody(DataOutputStream outputStream) throws IOException {
		outputStream.writeInt64(randomId);
	}

	@Override
	protected void readBody(DataInputStream inputStream) throws IOException {
		this.randomId = inputStream.readInt64();
	}

	@Override
	public String toString() {
		return "RequestStartAuth{" + randomId + "}";
	}

}
