package im.telegraph.handlers.internal;

import im.telegraph.api.ApiDifferenceUpdate;

import java.util.ArrayList;
import java.util.List;

public final class CombinedDifference {

    private final List<Long> updatedUsers;
    private final List<Long> updatedChats;
    private final List<ApiDifferenceUpdate> otherUpdates;

    protected CombinedDifference(final Builder builder) {
        this.updatedUsers = builder.updatedUsers;
        this.updatedChats = builder.updatedChats;
        this.otherUpdates = builder.otherUpdates;
    }

    /**
     * Gets a {@link Builder} for {@code CombinedDifference}.
     *
     * @return the builder
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     * Gets the updated users.
     *
     * @return the updated users.
     */
    public List<Long> getUpdatedUsers() {
        return updatedUsers;
    }

    /**
     * Gets the updated chats.
     *
     * @return the updated chats.
     */
    public List<Long> getUpdatedChats() {
        return updatedChats;
    }

    /**
     * Gets the other updates.
     *
     * @return the other updates.
     */
    public List<ApiDifferenceUpdate> getOtherUpdates() {
        return otherUpdates;
    }

    @Override
    public String toString() {
        return "CombinedDifference{" +
                "updatedUsers=" + updatedUsers +
                ", updatedChats=" + updatedChats +
                ", otherUpdates=" + otherUpdates +
                '}';
    }

    /**
     * {@code Builder} for {@link CombinedDifference}.
     */
    public static final class Builder {

        private final List<Long> updatedUsers = new ArrayList<>();
        private final List<Long> updatedChats = new ArrayList<>();
        private final List<ApiDifferenceUpdate> otherUpdates = new ArrayList<>();

        protected Builder() {}

        /**
         * Adds a user ID.
         *
         * @param userId updated user ID.
         * @return this Builder.
         */
        public Builder user(Long userId) {
            this.updatedUsers.add(userId);
            return this;
        }

        /**
         * Adds a chat ID.
         *
         * @param chatId updated user ID.
         * @return this Builder.
         */
        public Builder chat(Long chatId) {
            this.updatedChats.add(chatId);
            return this;
        }

        /**
         * Adds an update.
         *
         * @param update update.
         * @return this Builder.
         */
        public Builder update(ApiDifferenceUpdate update) {
            this.otherUpdates.add(update);
            return this;
        }

        /**
         * Build an instance of {@link CombinedDifference}.
         *
         * @return {@link CombinedDifference} instance.
         */
        public CombinedDifference build() {
            return new CombinedDifference(this);
        }

    }
}
