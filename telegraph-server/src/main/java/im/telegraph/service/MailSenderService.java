package im.telegraph.service;

import im.telegraph.common.email.Email;
import im.telegraph.common.email.EmailSender;
import im.telegraph.common.property.EmailProperties;
import im.telegraph.config.TelegraphProperties;
import im.telegraph.domain.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import java.util.Locale;

@Service
public class MailSenderService {

    private final Logger log = LoggerFactory.getLogger(MailSenderService.class);

    private static final String USER = "user";

    private static final String BASE_URL = "baseUrl";

    private final EmailSender emailSender;

    private final SpringTemplateEngine templateEngine;

    private final EmailProperties emailProperties;

    public MailSenderService(EmailSender emailSender, SpringTemplateEngine templateEngine,
                             TelegraphProperties properties) {
        this.emailSender = emailSender;
        this.templateEngine = templateEngine;
        this.emailProperties = properties.getEmail();
    }

    @Async
    public void sendEmailFromTemplate(final User user, final String template, final String titleKey) {
        if (user.getEmail() == null) {
            log.debug("Email doesn't exists for user '{}'", user.getId());
            return;
        }
        final String senderUrl = emailProperties.getFrom();
        final Locale locale = Locale.forLanguageTag(user.getLanguageCode());
        final Context context = new Context(locale);
        context.setVariable(USER, user);
        context.setVariable(BASE_URL, emailProperties.getBaseUrl());
        final String content = templateEngine.process(template, context);

        final Email email = new Email.EmailBuilder()
                .from(senderUrl)
                .to(user.getEmail())
                .subject(titleKey)
                .content(content)
                .isMultipart(false)
                .isHtml(true)
                .build();

        this.emailSender.sendEmail(email);
    }

    @Async
    public void sendActivationEmail(final User user) {
        log.debug("Sending activation email to '{}'", user.getEmail());
        sendEmailFromTemplate(user, "mail/activation", "email.activation.title");
    }

    @Async
    public void sendCreationEmail(final User user) {
        log.debug("Sending creation email to '{}'", user.getEmail());
        sendEmailFromTemplate(user, "mail/creation", "email.activation.title");
    }

    @Async
    public void sendPasswordResetMail(final User user) {
        log.debug("Sending password reset email to '{}'", user.getEmail());
        sendEmailFromTemplate(user, "mail/passwordReset", "email.reset.title");
    }
}
