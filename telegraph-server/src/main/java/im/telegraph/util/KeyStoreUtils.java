package im.telegraph.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.KeyStore;
import java.util.Base64;

/**
 * @author Ast3t1s
 */
public final class KeyStoreUtils {

    public static final String TYPE_JKS = "JKS";
    public static final String TYPE_PEM = "PEM";
    public static final String TYPE_PKCS12 = "PKCS12";

    public static final String DEFAULT_KEYSTORE_TYPE = TYPE_PKCS12;

    private KeyStoreUtils() {}

    /**
     * Initialize and returns a {@link KeyStore} from a {@link File}.
     *
     * @param keyStoreType the format of the specified keystore. Could be PKCS12 or JKS.
     * @param file the path to the keystore file to initialize.
     * @param password the password required to read the keystore. <code>null</code> if not password.
     * @return the initialized keystore or an {@link IllegalStateException} if the keystore can't be read or initialized.
     */
    public static KeyStore createKeyStore(String keyStoreType, File file, String password) {
        try (InputStream is = new FileInputStream(file)) {
            final KeyStore keyStore = KeyStore.getInstance(keyStoreType);
            keyStore.load(is, passwordToCharArray(password));
            return keyStore;
        } catch (Exception e) {
            throw new IllegalStateException(String.format("Unable to create %s store: %s", keyStoreType, e.getMessage()), e);
        }
    }

    /**
     * Initialize and returns a {@link KeyStore} from an {@link InputStream}.
     *
     * @param keyStoreType the format of the specified keystore. Could be PKCS12 or JKS.
     * @param keyStore the {@link InputStream} the key stream.
     * @param password the password required to read the keystore. <code>null</code> if not password.
     * @return the initialized keystore or an {@link IllegalStateException} if the keystore can't be read or initialized.
     */
    public static KeyStore createKeyStore(String keyStoreType, InputStream keyStore, String password) {
        try {
            final KeyStore store = KeyStore.getInstance(keyStoreType);
            try (keyStore) {
                store.load(keyStore, passwordToCharArray(password));
            };
            return store;
        } catch (Exception e) {
            throw new IllegalStateException(String.format("Unable to create %s store: %s", keyStoreType, e.getMessage()), e);
        }
    }

    /**
     * Initializes and returns a {@link KeyStore} from a base64 encoded content string.
     *
     * @param keyStoreType the format of the specified keystore. Could be PKCS12 or JKS.
     * @param keystore the base64 encoded content representing the keystore to initialize.
     * @param password the password required to read the keystore. <code>null</code> if not password.
     *
     * @return the initialized keystore or an {@link IllegalArgumentException} if the keystore can't be read or initialized.
     */
    public static KeyStore initFromContent(String keyStoreType, String keystore, String password) {
        try {
            final ByteArrayInputStream stream = new ByteArrayInputStream(Base64.getDecoder().decode(keystore));
            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
            keyStore.load(stream, passwordToCharArray(password));
            return keyStore;
        } catch (Exception e) {
            throw new IllegalArgumentException("Unable to get keystore from base64", e);
        }
    }

    /**
     * Convert a password string into a char array.
     * Return an empty char array if the specified password is <code>null</code>
     *
     * @param password the password to convert to char array.
     *
     * @return the corresponding char array.
     */
    public static char[] passwordToCharArray(String password) {
        return password != null ? password.toCharArray() : new char[0];
    }
} 
