package im.telegraph.domain.repository.rdbms;

import im.telegraph.common.rdbms.RdbmsOperations;
import im.telegraph.domain.entity.PushDevice;
import im.telegraph.domain.repository.PushDeviceRepository;
import io.r2dbc.spi.ConnectionFactory;
import io.r2dbc.spi.Row;
import io.r2dbc.spi.RowMetadata;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.util.List;
import java.util.function.BiFunction;

/**
 * @author Ast3t1s
 */
@Repository
public class RdbmsPushDeviceRepository implements PushDeviceRepository {

    private static final String INSERT_DEVICE = "INSERT INTO push_devices (platform, auth_id, enabled, token, " +
            "created_at, updated_at) VALUES (:platform, :authId, :enabled, :token, :createdAt, :updatedAt)";


    private final RdbmsOperations rdbmsOperations;

    public RdbmsPushDeviceRepository(ConnectionFactory connectionFactory) {
        this.rdbmsOperations = RdbmsOperations.create(connectionFactory);
    }

    @Override
    public Mono<PushDevice> create(PushDevice pushDevice) {
        return null;
    }

    @Override
    public Mono<PushDevice> findByToken(String token) {
        return null;
    }

    @Override
    public Flux<PushDevice> findAllByAuthKey(Long authKey) {
        return null;
    }

    @Override
    public Flux<PushDevice> findAllByAuthKeyIn(List<Long> authIds) {
        return null;
    }

    @Override
    public Mono<Void> delete(PushDevice pushDevice) {
        return null;
    }

    private static class PushDeviceRowMapper implements BiFunction<Row, RowMetadata, PushDevice> {

        @Override
        public PushDevice apply(Row row, RowMetadata metadata) {
            return PushDevice.builder()
                    .platform(PushDevice.Platform.of(row.get("platform", String.class)))
                    .token(row.get("token", String.class))
                    .authId(row.get("auth_id", Long.class))
                    .enabled(row.get("enabled", Boolean.class))
                    .createdAt(row.get("created_at", LocalDateTime.class))
                    .updatedAt(row.get("updated_at", LocalDateTime.class))
                    .build();
        }
    }
}
