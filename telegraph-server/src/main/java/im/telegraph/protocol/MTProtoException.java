package im.telegraph.protocol;

import java.io.Serial;

/**
 * @author Ast3t1s
 */
public class MTProtoException extends RuntimeException {

    @Serial
    private static final long serialVersionUID = -7155464614679986191L;

    public MTProtoException(){
    }

    public MTProtoException(Throwable cause) {
        super(cause);
    }

    public MTProtoException(String message) {
        super(message);
    }
}
