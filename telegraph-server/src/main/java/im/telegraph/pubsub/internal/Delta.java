package im.telegraph.pubsub.internal;

import java.util.List;

/**
 * @author Ast3t1s
 */
public class Delta extends PubSubPayload<Delta.Data> {

    public static final int OP = 2;

    public Delta(List<Bucket> buckets) {
        this(OP, new Data(buckets));
    }

    public Delta(int op, Data data) {
        super(op, data);
    }

    public static class Data {

        private final List<Bucket> buckets;

        public Data(List<Bucket> buckets) {
            this.buckets = buckets;
        }

        public List<Bucket> getBuckets() {
            return buckets;
        }

        @Override
        public String toString() {
            return "Data{" +
                    "buckets=" + buckets +
                    '}';
        }
    }
} 
