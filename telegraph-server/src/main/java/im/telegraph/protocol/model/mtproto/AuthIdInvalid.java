package im.telegraph.protocol.model.mtproto;

import im.telegraph.engine.serialization.DataInputStream;
import im.telegraph.engine.serialization.DataOutputStream;

import java.io.IOException;

public class AuthIdInvalid extends ProtoStruct {

    public static final byte HEADER = (byte) 0x11;

    public AuthIdInvalid() {

    }

    public AuthIdInvalid(DataInputStream in) throws IOException {
        super(in);
    }

    @Override
    protected byte getHeader() {
        return HEADER;
    }

    @Override
    protected void writeBody(DataOutputStream outputStream) throws IOException {

    }

    @Override
    protected void readBody(DataInputStream inputStream) throws IOException {

    }
}
