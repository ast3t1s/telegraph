package im.telegraph.common.health;

import com.sun.management.OperatingSystemMXBean;
import im.telegraph.common.property.HealthCheckProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.management.ManagementFactory;

public class CPUHealthCheckIndicator implements HealthCheckIndicator {

    private static final Logger log = LoggerFactory.getLogger(CPUHealthCheckIndicator.class);

    private final OperatingSystemMXBean operatingSystemMXBean;

    private final boolean isCpuHealthCheckAvailable;
    private final int retryCount;
    private final float unhealthyLoadThreshold;

    private volatile boolean isHealthy;

    private int currentUnhealthyTimes;

    public CPUHealthCheckIndicator(HealthCheckProperties properties) {
        this.operatingSystemMXBean = ManagementFactory.getPlatformMXBean(OperatingSystemMXBean.class);
        this.retryCount = properties.getCpu().getRetryCount();
        this.unhealthyLoadThreshold = properties.getCpu().getUnhealthyLoadThreshold();

        boolean isCpuHealthCheckAvailable = true;

        if (operatingSystemMXBean.getCpuLoad() < 0) {
            log.warn("CPU health check can not work the \"recent cpu usage\" for the whole operating environment is unavailable");
            isCpuHealthCheckAvailable = false;
            this.isHealthy = true;
        }
        this.isCpuHealthCheckAvailable = isCpuHealthCheckAvailable;
        updateHealthStatus();
    }

    @Override
    public boolean isHealthy() {
        return this.isHealthy;
    }

    @Override
    public void updateHealthStatus() {
        if (!this.isCpuHealthCheckAvailable) {
            return;
        }

        boolean wasHealthy = this.isHealthy;
        double load = this.operatingSystemMXBean.getCpuLoad();
        if (load > unhealthyLoadThreshold) {
            this.currentUnhealthyTimes++;
            if (this.currentUnhealthyTimes > retryCount) {
                this.isHealthy = false;
            }
        } else {
            this.currentUnhealthyTimes = 0;
            this.isHealthy = true;
        }

        log.debug("CPU load: {}", load);
        if (wasHealthy != this.isHealthy) {
            if (this.isHealthy) {
                log.info("The CPU has become healthy. The current CPU load: {}", load);
            } else {
                log.info("The CPU has become unhealthy. The current CPU load: {}", load);
            }
        }
    }
}
