package im.telegraph.api;

import im.telegraph.engine.serialization.BinaryStreamObject;
import im.telegraph.engine.serialization.io.BinaryStreamReader;
import im.telegraph.engine.serialization.io.BinaryStreamWriter;
import im.telegraph.engine.serialization.io.IOContext;
import im.telegraph.engine.serialization.stream.BinaryInputStream;
import im.telegraph.engine.serialization.stream.BinaryOutputStream;
import java.io.IOException;

/**
 * Generated by the Telegraph API Scheme generator. DO NOT EDIT!
 * @author Telegraph Scheme Generator
 */
public abstract class ApiUserPresence extends BinaryStreamObject {
    public abstract int getHeader();

    public byte[] buildContainer() throws IOException {
        BinaryOutputStream res = new BinaryOutputStream();
        BinaryStreamWriter writer = new BinaryStreamWriter(res);
        writer.writeInt(1, getHeader());
        writer.writeBytes(2, toByteArray());
        return res.toByteArray();
    }

    public static ApiUserPresence fromBytes(byte[] data) throws IOException {
        BinaryStreamReader reader = new BinaryStreamReader(new BinaryInputStream(data));
        int key = reader.readInt(1);
        byte[] content = reader.readByteArray(2);
        switch (key) {
            case 1: return IOContext.fromByteArray(new ApiUserPresenceEmpty(), content);
            case 2: return IOContext.fromByteArray(new ApiUserPresenceOnline(), content);
            case 3: return IOContext.fromByteArray(new ApiUserPresenceOffline(), content);
            default: return new ApiUserPresenceUnsupported(key, content);
        }
    }
}
