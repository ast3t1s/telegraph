package im.telegraph.admin.route;

import im.telegraph.common.OffsetPageRequest;
import io.netty.handler.codec.http.QueryStringDecoder;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * @author Ast3t1s
 */
public class RouteUtils {

    private RouteUtils() {
    }

    public static OffsetPageRequest createPageRequest(final String uri) {
        if (uri == null) {
            return OffsetPageRequest.of(0, 0);
        }
        QueryStringDecoder query = new QueryStringDecoder(uri);
        Integer size = query.parameters().getOrDefault("size", Collections.emptyList())
                .stream().map(Integer::valueOf).findFirst().orElse(20);
        Integer page = query.parameters().getOrDefault("page", Collections.emptyList())
                .stream().map(Integer::valueOf).findFirst().orElse(0);

        return OffsetPageRequest.of(page, size);
    }

    public static Optional<String> queryParam(String parameterName, String uri) {
        return Optional.ofNullable(new QueryStringDecoder(uri)
                .parameters()
                .get(parameterName))
                .stream()
                .flatMap(List::stream)
                .findFirst();
    }
} 
