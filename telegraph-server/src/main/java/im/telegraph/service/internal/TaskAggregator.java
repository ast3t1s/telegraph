package im.telegraph.service.internal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.Disposable;
import reactor.core.Disposables;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;
import reactor.util.retry.Retry;

import java.time.Duration;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * @author Ast3t1s
 */
public final class TaskAggregator<K, T> implements Disposable {

    private static final Logger log = LoggerFactory.getLogger(TaskAggregator.class);

    private static final Supplier<Scheduler> DEFAULT_AGGREGATOR_SCHEDULER = () ->
            Schedulers.newSingle("tl-aggregator", true);

    private final ConcurrentLinkedQueue<K> dispatchQueue = new ConcurrentLinkedQueue<>();
    private final ConcurrentMap<K, T> tasks = new ConcurrentHashMap<>();

    private final BiFunction<T, T, T> aggregator;
    private final Function<T, Mono<Void>> taskCallback;

    private final Disposable.Swap task;
    private final Duration interval;
    private final Scheduler scheduler;

    /**
     * Create a {@link TaskAggregator} that emits ticks on the given {@link Scheduler} upon calling
     * {@link #start()} with the default scheduler.
     *
     * @param interval the tick interval
     * @param aggregator the task aggregator
     * @param taskCallback the task callback
     */
    public TaskAggregator(Duration interval, BiFunction<T, T, T> aggregator, Function<T, Mono<Void>> taskCallback) {
        this(interval, aggregator, taskCallback, DEFAULT_AGGREGATOR_SCHEDULER.get());
    }

    /**
     * Create a {@link TaskAggregator} that emits ticks on the given {@link Scheduler} upon calling
     * {@link #start()}.
     *
     * @param interval the tick interval
     * @param aggregator the task aggregator
     * @param taskCallback the task callback
     * @param scheduler The scheduler for tasks.
     */
    public TaskAggregator(Duration interval, BiFunction<T, T, T> aggregator, Function<T, Mono<Void>> taskCallback,
                          Scheduler scheduler) {
        this.interval = interval;
        this.aggregator = aggregator;
        this.taskCallback = taskCallback;
        this.scheduler = scheduler;
        this.task = Disposables.swap();
    }

    /**
     * Begin producing ticks at the given rate.
     *
     * @see Flux#interval(Duration, Duration, Scheduler)
     */
    public void start() {
        task.replace(Flux.interval(interval, interval, scheduler)
                .onBackpressureDrop()
                .concatMap(tick -> sync())
                .retryWhen(Retry.max(3)
                        .doBeforeRetry((s) -> log.warn("Unexpected error when syncing aggregator", s.failure())))
                .subscribe());
    }

    /**
     * Dispose the current emitter task without completing or cancelling existing subscriptions to {@link #ticks()}.
     */
    public void stop() {
        if (task.get() != null) {
            task.get().dispose();
        }
        dispatchQueue.clear();
        tasks.clear();
    }

    /**
     * Adds or updates a task in the aggregator for the given key.
     * If a task already exists for the key, it merges the new task with the existing one.
     *
     * @param key  The key representing the task.
     * @param task The task to add or update.
     * @return A Mono that completes once the task is added.
     */
    public Mono<Void> putTask(K key, T task) {
        return Mono.create(sink -> {
            final T currentTask = tasks.get(key);
            if (currentTask != null) {
                tasks.put(key, aggregator.apply(currentTask, task));
            } else {
                //No current task for the given key put it to queue
                tasks.put(key, task);
                dispatchQueue.offer(key);
            }
            sink.success();
        });
    }

    protected Mono<Void> sync() {
        return Flux.<K>generate(sink -> {
            final K key = dispatchQueue.poll();
            if (key != null) {
                sink.next(key);
            } else {
                sink.complete();
            }
        })
        .mapNotNull(tasks::remove).flatMap(taskCallback).then();
    }

    @Override
    public void dispose() {
        task.get().dispose();
    }

    @Override
    public boolean isDisposed() {
        return task.isDisposed();
    }
}
