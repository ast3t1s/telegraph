package im.telegraph.common.property;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * @author Ast3t1s
 */
@Getter
@Setter
@Accessors(chain = true)
public class EmailProperties {

    /**
     * The email sender.
     */
    private String from;

    /**
     * The basic url.
     */
    private String baseUrl;
} 
