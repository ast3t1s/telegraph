package im.telegraph.service;

import im.telegraph.common.DateTimeFactory;
import im.telegraph.common.StateHolder;
import im.telegraph.domain.entity.Id;
import im.telegraph.domain.entity.Presence;
import im.telegraph.domain.repository.PresenceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.stereotype.Service;
import reactor.core.Disposable;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuples;

import java.time.Duration;
import java.time.Instant;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

/**
 * @author Ast3t1s
 */
@Service
public class PresenceService implements DisposableBean {

    private static final Logger log = LoggerFactory.getLogger(PresenceService.class);

    private final PresenceRepository presenceRepository;
    private final StateHolder stateHolder;
    private final DateTimeFactory dateFactory;

    private final Map<Tuple2<Id, Long>, Disposable> presenceTimeoutTasks = new ConcurrentHashMap<>();
    private final Scheduler scheduler = Schedulers.newSingle("presence", true);

    public PresenceService(PresenceRepository presenceRepository,
                           StateHolder stateHolder,
                           DateTimeFactory dateFactory) {
        this.presenceRepository = presenceRepository;
        this.stateHolder = stateHolder;
        this.dateFactory = dateFactory;
    }

    /**
     * Get user's device last activity.
     *
     * @param userId Identifier of the user.
     * @param authId Identifier of the device.
     * @return A {@link Mono} where, upon successful completion, emits the {@link Presence}.If an error is received,
     * it is emitted through the {@code Mono}.
     */
    public Mono<Presence> getPresence(final Id userId, final Long authId) {
        return stateHolder.getPresenceStore().find(userId.asLong())
                .switchIfEmpty(Mono.defer(() -> presenceRepository.findByAuthIdAndUserId(authId, userId))
                        .flatMap(toCache -> stateHolder.getPresenceStore()
                                .save(toCache.getUserId().asLong(), toCache)));
    }

    /**
     * Create user presence if absent, otherwise update status.
     *
     * @param userId  Identifier of the user.
     * @param authId  Identifier of the device.
     * @param timeout Timeout of the presence.
     * @return A {@link Mono} where, upon successful completion, emits nothing; indicating the presence set to online.
     * If an error is received, it is emitted through the {@code Mono}.
     */
    public Mono<Void> setOnline(final Id userId, final Long authId, final Duration timeout) {
        final Instant expireAt = dateFactory.getClock().instant().plus(timeout);
        return presenceRepository.save(new Presence(authId, userId, expireAt))
                .then(stateHolder.getPresenceStore().find(userId.asLong())
                        .map(toUpdate -> toUpdate.withLastSeen(expireAt))
                        .flatMap(toCache -> stateHolder.getPresenceStore().save(userId.asLong(), toCache)))
                .then(Mono.fromRunnable(() -> {
                    cancelPresenceTimeoutTask(userId, authId);
                    schedulePresenceTimeoutTask(userId, authId, timeout);
                }));
    }

    /**
     * Set user presence status to offline.
     *
     * @param userId Identifier of the user.
     * @param authId Identifier of the device.
     * @return A {@link Mono} where, upon successful completion, emits nothing; indicating the presence set to offline
     * If an error is received, it is emitted through the {@code Mono}.
     */
    public Mono<Void> setOffline(final Id userId, final Long authId) {
        final Instant lastSeen = dateFactory.getClock().instant();
        return presenceRepository.save(new Presence(authId, userId, lastSeen))
              /*  .flatMap(__ -> stateHolder.getPresenceStore().find(userId.getValue())
                        .map(presence -> presence.withLastSeen(lastSeen))
                        .flatMap(toCache -> stateHolder.getPresenceStore()
                                .save(toCache.getUserId().getValue(), toCache)))
                .then()*/
                .doFinally(s -> cancelPresenceTimeoutTask(userId, authId));
    }

    private void schedulePresenceTimeoutTask(Id userId, Long authId, Duration timeout) {
        presenceTimeoutTasks.computeIfAbsent(Tuples.of(userId, authId), key -> {
            log.debug("[{}] Schedule presence timeout task for device: {}, timeout: {}", userId, authId, timeout);
            return scheduler.schedule(() -> {

            }, timeout.toMillis(), TimeUnit.MILLISECONDS);
        });
    }

    private void cancelPresenceTimeoutTask(Id userId, Long authId) {
        Disposable timeoutTask = presenceTimeoutTasks.remove(Tuples.of(userId, authId));
        if (timeoutTask != null && !timeoutTask.isDisposed()) {
            log.debug("[{}] Cancelled presence timeout task for device: {}", userId, authId);
            timeoutTask.dispose();
        }
    }

    @Override
    public void destroy() throws Exception {
        for (Disposable task : presenceTimeoutTasks.values()) {
            task.dispose();
        }
        presenceTimeoutTasks.clear();
    }
}
