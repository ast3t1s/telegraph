package im.telegraph.cluster.event;

import im.telegraph.cluster.ClusterEvent;
import im.telegraph.internal.remote.Address;

/**
 * @author Ast3t1s
 */
public class ClusterInitialized implements ClusterEvent {

    private final Address selfAddress;

    public ClusterInitialized(Address selfAddress) {
        this.selfAddress = selfAddress;
    }

    public Address getSelfAddress() {
        return selfAddress;
    }

    @Override
    public String toString() {
        return "ClusterInitialized{" +
                "selfAddress=" + selfAddress +
                '}';
    }
}
