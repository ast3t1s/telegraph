package im.telegraph.internal.remote.serializer;

/**
 * @author Ast3t1s
 */
public class NullSerializer implements Serializer {

    private static final byte[] EMPTY = new byte[0];

    @Override
    public byte[] toBinary(Object object) throws SerializationException {
        return EMPTY;
    }

    @Override
    public Object fromBinary(byte[] array) throws SerializationException {
        return null;
    }

    @Override
    public int serializerId() {
        return -1;
    }
}
