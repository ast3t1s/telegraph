package im.telegraph.domain.entity;

import reactor.util.annotation.Nullable;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import java.util.Optional;

/**
 * Entity which stores authorization key and its parameters.
 *
 * @author Vsevolod Bondarenko
 */
public final class AuthKey implements Serializable {

    private static final long serialVersionUID = 1L;

    private final long id;
    private final byte[] publicKeyHash;
    private final Instant createdAt;
    @Nullable
    private final Instant deletedAt;

    /**
     * Constructs an {@link AuthKey} with an associated data.
     *
     * @param id            Identifier of the authorization key.
     * @param publicKeyHash Public key hash using for encryption/decryption.
     * @param createdAt     Point in time (Unix timestamp) when the key was created.
     * @param deletedAt     Point in time (Unix timestamp) when the key has been deleted.
     */
    public AuthKey(final long id, final byte[] publicKeyHash, final Instant createdAt, @Nullable final Instant deletedAt) {
        this.id = id;
        this.publicKeyHash = Objects.requireNonNull(publicKeyHash);
        this.createdAt = createdAt;
        this.deletedAt = deletedAt;
    }

    /**
     * Factory method for creating {@link AuthKey}
     */
    public static AuthKey of(long id, byte[] publicKeyHash, Instant createdAt) {
        return new AuthKey(id, publicKeyHash, createdAt, null);
    }

    /**
     * Factory method for updating auth key delete date.
     *
     * @param deletedAt the date when auth key was deleted.
     */
    public AuthKey withDeletedAt(final Instant deletedAt) {
        return new AuthKey(id, publicKeyHash, createdAt, deletedAt);
    }

    /**
     * Returns the unique identifier of the entity.
     *
     * @return the id of the entity as a Long.
     */
    public Long getId() {
        return id;
    }

    /**
     * Returns the public key hash.
     *
     * @return the publicKeyHash of the entity as a byte array.
     */
    public byte[] getPublicKeyHash() {
        return publicKeyHash;
    }

    /**
     * Returns the creation timestamp of the entity.
     *
     * @return the creation time as an Instant.
     */
    public Instant getCreatedAt() {
        return createdAt;
    }

    /**
     * Returns the deletion timestamp of the entity, if it has been deleted, wrapped in an Optional.
     *
     * @return an Optional containing the deletion time as an Instant if it is not null, otherwise an empty Optional.
     */
    public Optional<Instant> getDeletedAt() {
        return Optional.ofNullable(deletedAt);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AuthKey authKey = (AuthKey) o;
        return Objects.equals(id, authKey.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "AuthKey{" +
                "id=" + id +
                ", createdAt=" + createdAt +
                ", deletedAt=" + deletedAt +
                '}';
    }
}
