package im.telegraph.server.test.auth;

public class PqAuth {

    private final long authKey;

    private final byte[] masterKey;

    public PqAuth(long authKey, byte[] masterKey) {
        this.authKey = authKey;
        this.masterKey = masterKey;
    }

    public long getAuthKey() {
        return authKey;
    }

    public byte[] getMasterKey() {
        return masterKey;
    }

}
