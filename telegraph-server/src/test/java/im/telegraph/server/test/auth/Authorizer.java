package im.telegraph.server.test.auth;

import im.telegraph.crypto.Cryptos;
import im.telegraph.crypto.Curve25519;
import im.telegraph.crypto.Curve25519KeyPair;
import im.telegraph.crypto.primitives.Digest;
import im.telegraph.crypto.primitives.prf.PRF;
import im.telegraph.crypto.primitives.util.ByteStrings;
import im.telegraph.engine.serialization.stream.BinaryInputStream;
import im.telegraph.engine.serialization.stream.BinaryOutputStream;
import im.telegraph.protocol.model.ProtoSerializer;
import im.telegraph.protocol.model.mtproto.ProtoMessage;
import im.telegraph.protocol.model.mtproto.ProtoStruct;
import im.telegraph.protocol.model.mtproto.auth.*;
import im.telegraph.protocol.model.transport.ProtoPackage;
import im.telegraph.rpc.base.PacketTypeConstants;
import im.telegraph.server.TransportFrame;
import im.telegraph.server.test.transport.PlainTcpConnection;
import im.telegraph.util.Crypto;
import im.telegraph.util.ThreadLocalSecureRandom;

import java.io.IOException;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.zip.CRC32;

public class Authorizer {

    private static AtomicInteger packetIndexer = new AtomicInteger(1000);

    private static final int AUTH_ATTEMPT_COUNT = 5;

    private final CRC32 CRC32_ENGINE = new CRC32();

    private PlainTcpConnection tcpConnection;

    public PqAuth doAuth(String host, int port) {
        for (int i = 0; i < AUTH_ATTEMPT_COUNT; i++) {
            try {
                tcpConnection = new PlainTcpConnection(host, port);
                return authAttempt();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (tcpConnection != null) {
                    tcpConnection.destroy();
                    tcpConnection = null;
                }
            }
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                return null;
            }
        }
        return null;
    }

    public PqAuth authAttempt() throws Exception {
        byte[] handshakeData = new byte[32];
        ThreadLocalSecureRandom.current().nextBytes(handshakeData);
        BinaryOutputStream outputStream = new BinaryOutputStream();
        outputStream.writeByte(1);
        outputStream.writeByte(2);
        outputStream.writeByte(1);
        outputStream.writeInt32(handshakeData.length);
        outputStream.writeBytes(handshakeData, 0, handshakeData.length);
        executeMethod(PacketTypeConstants.HEADER_HANDSHAKE_REQUEST, outputStream.toByteArray());

        ThreadLocalRandom random = ThreadLocalRandom.current();
        long randomID = random.nextLong();

        byte[] startAuth = executeMethod(PacketTypeConstants.HEADER_PROTO,
                createAuthMTPackage(new RequestStartAuth(randomID)).toByteArray());
        ResponseStartAuth responseStartAuth = (ResponseStartAuth) ProtoSerializer
                .readMessagePayload(new ProtoPackage(new BinaryInputStream(startAuth)).getPayload().getPayload());
        System.out.println(responseStartAuth.getRandomId() + " " + responseStartAuth.getAvailableKeys().length + " "
                + responseStartAuth.getServerNonce().length);

        byte[] getServerKeysRaw = executeMethod(PacketTypeConstants.HEADER_PROTO,
                createAuthMTPackage(new RequestGetServerKey(responseStartAuth.getAvailableKeys()[0])).toByteArray());
        ResponseGetServerKey responseGetServerKey = (ResponseGetServerKey) ProtoSerializer
                .readMessagePayload(new ProtoPackage(new BinaryInputStream(getServerKeysRaw)).getPayload().getPayload());
        System.out.println("Response get server key..." + responseGetServerKey.getKey().length + "response key id: "
                + responseGetServerKey.getKeyId());

        final byte[] clientNonce = new byte[32];
        Crypto.nextBytes(clientNonce);
        byte[] keyMaterial = new byte[32];
        Crypto.nextBytes(keyMaterial);
        final Curve25519KeyPair clientKeyPair = Curve25519.keyGen(keyMaterial);

        byte[] responseDhRaw = executeMethod(PacketTypeConstants.HEADER_PROTO, createAuthMTPackage(
                new RequestDH(randomID, responseGetServerKey.getKeyId(), clientNonce, clientKeyPair.getPublicKey()))
                .toByteArray());
        ResponseDH responseDH = (ResponseDH) ProtoSerializer
                .readMessagePayload(new ProtoPackage(new BinaryInputStream(responseDhRaw)).getPayload().getPayload());
        if (responseDH.getRandomId() != randomID) {
            throw new IOException("Incorrect RandomId");
        }
        System.out.println("Response get DH..." + responseDH.getRandomId() + "Verify " + responseDH.getVerify().length
                + "Verify sign" + responseDH.getVerifySign().length);

        PRF combinedPrf = Cryptos.PRF_SHA_STREEBOG_256();
        byte[] nonce = ByteStrings.merge(clientNonce, responseStartAuth.getServerNonce());
        byte[] pre_master_secret = Curve25519.calculateAgreement(clientKeyPair.getPrivateKey(),
                responseGetServerKey.getKey());
        byte[] master_secret = combinedPrf.calculate(pre_master_secret, "master secret", nonce, 256);
        byte[] verify = combinedPrf.calculate(master_secret, "client finished", nonce, 256);
        if (!Curve25519.verifySignature(responseGetServerKey.getKey(), verify, responseDH.getVerifySign())) {
            throw new IOException("Incorrect Signature");
        }
        Digest sha256 = Crypto.createSHA256();
        sha256.update(master_secret, 0, master_secret.length);
        byte[] authIdHash = new byte[32];
        sha256.doFinal(authIdHash, 0);
        long authId = ByteStrings.bytesToLong(authIdHash);

        return new PqAuth(authId, master_secret);
    }

    private ProtoPackage createAuthMTPackage(ProtoStruct request) {
        return new ProtoPackage(0, 0, new ProtoMessage(0, request.toByteArray()));
    }

    public <T extends ProtoStruct> T executeMethod(int header, ProtoStruct method) throws Exception {
        final var packageId = packetIndexer.incrementAndGet();
        byte[] data = method.toByteArray();
        CRC32_ENGINE.reset();
        CRC32_ENGINE.update(data, 0, data.length);
        final var checksum = (int) CRC32_ENGINE.getValue();
        TransportFrame transport = new TransportFrame(packageId, header, data.length, data, checksum);
        byte[] response = tcpConnection.executeMethod(transport.toByteArray());

        BinaryInputStream inputStream = new BinaryInputStream(response);
        final var responseIndex = inputStream.readInt();
        final var responseHeader = inputStream.readByte();
        final var responseSize = inputStream.readInt();
        final var responseBody = inputStream.readBytes(responseSize);
        final var responseChecksum = inputStream.readInt();

        return (T) ProtoSerializer.readMessagePayload(responseBody);
    }

    private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();

    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars);
    }

    public byte[] executeMethod(int header, byte[] method) throws Exception {
        final var packageId = packetIndexer.incrementAndGet();
        CRC32_ENGINE.reset();
        CRC32_ENGINE.update(method, 0, method.length);
        final var checksum = (int) CRC32_ENGINE.getValue();
        TransportFrame transport = new TransportFrame(packageId, header, method.length, method, checksum);

        System.out.println("Hex string " + bytesToHex(transport.toByteArray()));

        byte[] response = tcpConnection.executeMethod(transport.toByteArray());

        BinaryInputStream inputStream = new BinaryInputStream(response);
        final var responseIndex = inputStream.readInt();
        final var responseHeader = inputStream.readByte();
        final var responseSize = inputStream.readInt();
        final var responseBody = inputStream.readBytes(responseSize);
        final var responseChecksum = inputStream.readInt();

        return responseBody;
    }

    public byte[] readMessage() throws Exception {
        return tcpConnection.readMessage();
    }

    public void destroy() {
        tcpConnection.destroy();
    }
}
