package im.telegraph.domain.entity;

import org.springframework.util.ConcurrentReferenceHashMap;
import reactor.util.annotation.Nullable;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuples;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;

public final class Peer implements Comparable<Peer> {

    private static final Map<Tuple2<Long, Type>, Peer> CACHE = new ConcurrentReferenceHashMap<>();

    private final Long id;
    private final Type type;
    @Nullable
    private volatile Object context;

    /**
     * Constructs a {@code Peer instance} with the given type and id.
     *
     * @param id   Unique identifier of the user/chat.
     * @param type Type of peer {@link Type}.
     */
    private Peer(final Long id, final Type type) {
        this.id = Objects.requireNonNull(id);
        this.type = Objects.requireNonNull(type);
    }

    public static Peer of(final Long id, final Type type) {
        return CACHE.computeIfAbsent(Tuples.of(id, type), (t2) -> new Peer(t2.getT1(), t2.getT2()));
    }

    /**
     * Constructs a {@code Peer} from the given user ID.
     *
     * @param id the id of the user.
     * @return a constructed {@code Peer} with the given user identifier.
     */
    public static Peer ofUser(final Id id) {
        return CACHE.computeIfAbsent(Tuples.of(id.asLong(), Type.USER), (t2) -> new Peer(t2.getT1(), t2.getT2()));
    }

    /**
     * Constructs a {@code Peer} from the given chat ID.
     *
     * @param id the id of the chat.
     * @return a constructed {@code Peer} with the given chat identifier.
     */
    public static Peer ofChat(final Id id) {
        return CACHE.computeIfAbsent(Tuples.of(id.asLong(), Type.CHAT), (t2) -> new Peer(t2.getT1(), t2.getT2()));
    }

    /**
     * Constructs a {@code Peer} from unique peer identifier.
     *
     * @param uid unique peer identifier.
     * @return a constructed {@code Peer} with the given identifier.
     */
    public static Peer fromUniqueId(final long uid) {
        final long id = (int) (uid & 0xFFFFFFFFL);
        final int type = (int) ((uid >> 32) & 0xFFFFFFFFL);

        return switch (type) {
            default -> new Peer(0L, Type.UNKNOWN);
            case 0 -> new Peer(id, Type.USER);
            case 1 -> new Peer(id, Type.CHAT);
        };
    }

    /**
     * Gets the identifies of the peer (chat or user).
     *
     * @return the identifier of the peer
     */
    public Long getPeerId() {
        return id;
    }

    /**
     * Gets the type of this {@code Peer}.
     *
     * @return The type of this {@code Peer}.
     */
    public Type getType() {
        return type;
    }

    /**
     * Gets the access hash of this id, if present and {@link #getType() type} is not {@link Type#CHAT}.
     *
     * @return The access hash of this id, if present and applicable.
     */
    public Optional<Long> getAccessHash() {
        return context instanceof Long ? Optional.ofNullable((Long) context) : Optional.empty();
    }

    /**
     * Returns the peer represented by {@link Id}.
     */
    public Id asPeerId() {
        return Id.of(this.id);
    }

    /**
     * Gets whether {@link #getAccessHash()} is present.
     *
     * @return {@code true} if {@link #getAccessHash()} is present.
     */
    public boolean isWithAccessInfo() {
        return context != null;
    }

    /**
     * Return this {@link Peer} object in unique long representation.
     *
     * @return a unique long identifier.
     */
    public long asUniqueId() {
        return ((long) id & 0xFFFFFFFFL) + (((long) type.getValue() & 0xFFFFFFFFL) << 32);
    }

    /**
     * Return true if peer is a user.
     *
     * @return true, if peer is a user.
     */
    public boolean isUser() {
        return type == Type.USER;
    }

    /**
     * Return true if peer is a chat.
     *
     * @return true, if peer s a chat.
     */
    public boolean isChat() {
        return type == Type.CHAT;
    }

    /**
     * Return true if peer is unknown.
     *
     * @return true, if peer is unknown.
     */
    public boolean isUnknown() {
        return type == Type.UNKNOWN;
    }

    /**
     * Return a string form of this {@link Peer} using following pattern: {@code type_id}
     *
     * @return a formatted string representation this object.
     */
    public String asString() {
        return Long.toUnsignedString(type.getValue()).concat("_").concat(Long.toUnsignedString(id));
    }

    private void checkType(final Type type) {
        if (getType() != type) {
            throw new IllegalStateException("Wrong peer type#" + type.getValue());
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Peer peer = (Peer) o;
        return Objects.equals(id, peer.id) && type == peer.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, id);
    }

    @Override
    public String toString() {
        return "Peer{" + asString() + "}";
    }

    @Override
    public int compareTo(Peer o) {
        return Long.compare(this.asUniqueId(), o.asUniqueId());
    }

    /**
     * Represent the various types of peer
     */
    public enum Type {

        /**
         * Private peer type.
         */
        USER(0),

        /**
         * Chat peer type.
         */
        CHAT(1),

        /**
         * Unknown peer type.
         */
        UNKNOWN(-1);

        private final int value;

        Type(final int value) {
            this.value = value;
        }

        /**
         * Gets the underlying value as represented by Telegraph.
         *
         * @return The underlying value as represented by Telegraph.
         */
        public int getValue() {
            return value;
        }

        public static Type of(final int value) {
            return switch (value) {
                case 0 -> USER;
                case 1 -> CHAT;
                default -> UNKNOWN;
            };
        }
    }
} 
