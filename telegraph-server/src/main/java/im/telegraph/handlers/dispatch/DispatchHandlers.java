package im.telegraph.handlers.dispatch;

import im.telegraph.api.rpc.*;
import im.telegraph.handlers.*;
import im.telegraph.rpc.base.Request;
import im.telegraph.rpc.base.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;

/**
 * Registry for {@link Request} to {@link Response} mapping operations.
 *
 * @author Ast3t1s
 */
@Component
public class DispatchHandlers implements DispatchServiceMapper {

    private static final Logger log = LoggerFactory.getLogger(DispatchHandlers.class);

    private final Map<Class<?>, DispatchHandler<?, ?>> handlerMap = new HashMap<>();

    public DispatchHandlers(UserDispatchHandlers userHandlers,
                            UpdatesDispatchHandlers updatesHandlers,
                            ContactsDispatchHandlers contactsHandlers,
                            AuthDispatchHandlers authHandlers,
                            PresenceDispatchHandlers presenceHandlers,
                            PrivacyDispatchHandlers privacyHandlers,
                            MessageDispatchHandlers messageHandlers,
                            FileDispatchHandlers fileHandlers,
                            ChatDispatchHandlers chatHandlers) {

        // Auth handlers
        addHandler(RequestAuthSignIn.class, authHandlers::handleSignIn);
        addHandler(RequestAuthSignOut.class, authHandlers::handleSignOut);
        addHandler(RequestAuthSignUp.class, authHandlers::handleSignUp);
        addHandler(RequestAuthGetAuthSessions.class, authHandlers::handleGetAuthSessions);
        addHandler(RequestAuthTerminateSession.class, authHandlers::handleTerminateSession);
        addHandler(RequestAuthTerminateSessions.class, authHandlers::handleTerminateSessions);

        // User handlers
        addHandler(RequestProfileCheckEmail.class, userHandlers::handleCheckEmail);
        addHandler(RequestProfileCheckNickName.class, userHandlers::handleCheckUsername);
        addHandler(RequestProfileGetUserSelf.class, userHandlers::handleGetUserSelf);
        addHandler(RequestProfileGetFullUser.class, userHandlers::handleGetUserFull);
        addHandler(RequestProfileGetUsers.class, userHandlers::handleGetUsers);
        addHandler(RequestProfileEditNickName.class, userHandlers::handleEditUsername);
        addHandler(RequestProfileEditEmail.class, userHandlers::handleEditEmail);
        addHandler(RequestProfileChangePassword.class, userHandlers::handleChangePassword);

        // Photo handlers
        addHandler(RequestPhotosUpdateAvatarPhoto.class, userHandlers::handleUpdateProfilePhoto);
        addHandler(RequestPhotosUploadAvatarPhoto.class, userHandlers::handleUploadProfilePhoto);
        addHandler(RequestPhotosDeleteAvatarPhotos.class, userHandlers::handleDeletePhotos);
        addHandler(RequestPhotosGetAvatarPhotos.class, userHandlers::handleGetUserPhotos);

        // Presence handlers
        addHandler(RequestWeakSetOnline.class, presenceHandlers::handleSetOnline);
        addHandler(RequestWeakGetPresence.class, presenceHandlers::handleGetPresence);

        //Updates handlers
        addHandler(RequestSequenceGetState.class, updatesHandlers::handleGetState);
        addHandler(RequestSequenceGetDifference.class, updatesHandlers::handleGetDifference);

        // Contacts handlers
        addHandler(RequestContactsAddContact.class, contactsHandlers::handleAddContact);
        addHandler(RequestContactsApproveContact.class, contactsHandlers::handleAcceptContact);
        addHandler(RequestContactsRemoveContact.class, contactsHandlers::handleDeleteContact);
        addHandler(RequestContactsGetContacts.class, contactsHandlers::handleGetContacts);
        addHandler(RequestContactsSearchContacts.class, contactsHandlers::handleSearchContacts);

        // Privacy handlers
        addHandler(RequestPrivacyBlockUser.class, privacyHandlers::handleBlockUser);
        addHandler(RequestPrivacyUnblockUser.class, privacyHandlers::handleUnblockUser);
        addHandler(RequestPrivacyLoadBlockedUsers.class, privacyHandlers::handleGetBlockList);

        addHandler(RequestMessagesSendMessage.class, messageHandlers::handleSendMessage);
        addHandler(RequestMessagesMessageRead.class, messageHandlers::handleReadMessage);
        addHandler(RequestMessagesMessageReceived.class, messageHandlers::handleReceivedMessage);
        addHandler(RequestMessagesClearChat.class, messageHandlers::handleClearChat);
        addHandler(RequestMessagesLoadHistory.class, messageHandlers::handleLoadHistory);
        addHandler(RequestMessagesLoadDialogs.class, messageHandlers::handleLoadDialogs);
        addHandler(RequestMessagesTyping.class, messageHandlers::handleTyping);

        addHandler(RequestFilesGetFileUrl.class, fileHandlers::handleGetFileUrl);
        addHandler(RequestFilesGetFileUrls.class, fileHandlers::handleGetFileUrls);
        addHandler(RequestFilesGetFileUploadUrl.class, fileHandlers::handleGetFileUploadUrl);

        addHandler(RequestChatsCreateChat.class, chatHandlers::handleCreateChat);
        addHandler(RequestChatsEditTitle.class, chatHandlers::handleEditChatTitle);
        addHandler(RequestChatsEditAbout.class, chatHandlers::handleEditAbout);
        addHandler(RequestChatsEditPhoto.class, chatHandlers::handleEditChatPhoto);
     //   addHandler(RequestChatsDeleteChat.class, chatHandlers::handleDeleteChat);
        addHandler(RequestChatsAddChatMember.class, chatHandlers::handleAddChatUser);
        addHandler(RequestChatsDeleteChatUser.class, chatHandlers::handleDeleteChatUser);
        addHandler(RequestChatsEditChatAdmin.class, chatHandlers::handleEditChatAdmin);
        addHandler(RequestChatsChatSaveAdminSettings.class, chatHandlers::handleEditAdminSettings);
        addHandler(RequestChatsLoadParticipants.class, chatHandlers::handleLoadChatParticipants);
    }

    private <T extends Request<? extends Response>, R extends Response> void addHandler(final Class<T> dispatchType,
                                                                                        final DispatchHandler<T, R> dispatchHandler) {
        this.handlerMap.put(dispatchType, dispatchHandler);
    }

    /**
     * Process a {@link Request object wrapped with its context to potentially obtain a {@link Response}.
     *
     * @param context the DispatchContext used with this Dispatch object.
     * @param <T> the Request type
     * @param <R> the Response type
     * @return an Response mapped from the given Request, or null if no response produced.
     */
    @Override
    @SuppressWarnings("unchecked")
    public <T extends Request<? extends Response>, R extends Response> Mono<R> dispatch(DispatchContext<T> context) {
        DispatchHandler<T, R> handler = (DispatchHandler<T, R>) this.handlerMap.entrySet()
                .stream()
                .filter(entry -> entry.getKey().isAssignableFrom(context.getRequest().getClass()))
                .map(Map.Entry::getValue)
                .findFirst()
                .orElse(null);
        if (handler == null) {
            log.warn("Handler not found from: {}", context.getRequest().getClass());
            return Mono.empty();
        }
        return Mono.defer(() -> handler.handle(context))
                .checkpoint("Dispatch handled for " + context.getRequest().getClass());
    }
}
