package im.telegraph.admin.object.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import io.netty.handler.codec.http.HttpResponseStatus;

import java.time.Instant;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY;

/**
 * @author Ast3t1s
 */
@JsonInclude(NON_EMPTY)
@JsonPropertyOrder({"timestamp", "status", "error", "message", "path"})
public class ErrorResponse {

    /**
     * Error's timestamp
     */
    private final Instant timestamp;

    @JsonIgnore
    private final HttpResponseStatus httpStatus;

    /**
     * Error's HTTP status
     */
    private final Integer status;

    /**
     * Error's status description
     */
    private final String error;

    /**
     * Error's detailed message
     */
    private final String message;

    /**
     * Request path related to the error
     */
    private final String path;

    public ErrorResponse(Instant timestamp, HttpResponseStatus httpStatus, String message, String path) {
        this.timestamp = timestamp != null ? timestamp : Instant.now();
        this.httpStatus = httpStatus != null ? httpStatus : HttpResponseStatus.INTERNAL_SERVER_ERROR;
        this.status = this.httpStatus.code();
        this.error = this.httpStatus.reasonPhrase();
        this.message = message;
        this.path = path;
    }

    public static Builder builder() {
        return new Builder();
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public HttpResponseStatus getHttpStatus() {
        return httpStatus;
    }

    public Integer getStatus() {
        return status;
    }

    public String getError() {
        return error;
    }

    public String getMessage() {
        return message;
    }

    public String getPath() {
        return path;
    }


    public static final class Builder {

        private Instant timestamp;
        private HttpResponseStatus httpStatus;
        private String message;
        private String path;

        private Builder() {
        }

        public static Builder anErrorResponse() {
            return new Builder();
        }

        public Builder timestamp(Instant timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public Builder httpStatus(HttpResponseStatus httpStatus) {
            this.httpStatus = httpStatus;
            return this;
        }

        public Builder message(String message) {
            this.message = message;
            return this;
        }

        public Builder path(String path) {
            this.path = path;
            return this;
        }

        public ErrorResponse build() {
            return new ErrorResponse(timestamp, httpStatus, message, path);
        }
    }

    @Override
    public String toString() {
        return "ErrorResponse{" +
                "timestamp=" + timestamp +
                ", httpStatus=" + httpStatus +
                ", status=" + status +
                ", error='" + error + '\'' +
                ", message='" + message + '\'' +
                ", path='" + path + '\'' +
                '}';
    }
}
