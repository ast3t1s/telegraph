package im.telegraph.generator.type;

public class SchemeUpdateBox extends SchemeContainer implements SchemeRecord {
    private String name;
    private int header;

    public SchemeUpdateBox(String name, int header) {
        this.name = name;
        this.header = header;
    }

    public String getName() {
        return name;
    }

    public int getHeader() {
        return header;
    }

    @Override
    public String toString() {
        return "update box " + name;
    }
}