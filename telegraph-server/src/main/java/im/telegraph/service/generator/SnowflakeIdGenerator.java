package im.telegraph.service.generator;

import im.telegraph.domain.entity.Id;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author Ast3t1s
 */
public class SnowflakeIdGenerator implements IdGenerator {

    /**
     * 2020-10-13 00:00:00 in UTC
     */
    public static final long EPOCH = 1602547200000L;

    // the mask of sequence (111111111111B = 4095)
    private static final long SEQUENCE_MASK = 4095L;

    // the left shift bits of workerId equals 12 bits
    private static final long WORKER_ID_LEFT_SHIFT_BITS = 12L;

    // the left shift bits of timestamp equals 22 bits (WORKER_ID_LEFT_SHIFT_BITS + workerId)
    private static final long TIMESTAMP_LEFT_SHIFT_BITS = 22L;

    // the max of worker ID is 1024
    private static final long WORKER_ID_MAX_VALUE = 1024L;

    // Used to ensure clock moves forward.
    private final AtomicLong lastTimestamp = new AtomicLong();
    private final AtomicInteger sequenceNumber = new AtomicInteger(0);
    private final long workerId;

    public SnowflakeIdGenerator(final long workerId) {
        if (workerId > WORKER_ID_MAX_VALUE) {
            throw new IllegalArgumentException("The worker ID must be in the range: [0, " + WORKER_ID_MAX_VALUE +") " +
                    "but got: " + workerId);
        }
        this.workerId = workerId;
    }

    @Override
    public Id generateId() {
        final long currentSequenceNumber = sequenceNumber.incrementAndGet() & SEQUENCE_MASK;
        final long currentTimestamp = lastTimestamp.updateAndGet(lastTs -> {
            // Don't let timestamp go backwards at least while this JVM is running.
            long nonBackwardsTimestamp = Math.max(lastTs, System.currentTimeMillis());
            if (currentSequenceNumber == 0) {
                // Always force the clock to increment whenever sequence number is 0,
                // if we have a long time-slip backwards
                nonBackwardsTimestamp++;
            }
            return nonBackwardsTimestamp;
        }) - EPOCH;

        final long currentID = (currentTimestamp << TIMESTAMP_LEFT_SHIFT_BITS)
                | (workerId << WORKER_ID_LEFT_SHIFT_BITS) | currentSequenceNumber;

        return Id.of(currentID);
    }
}
