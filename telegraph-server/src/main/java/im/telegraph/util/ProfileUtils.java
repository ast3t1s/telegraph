package im.telegraph.util;

import org.springframework.boot.SpringApplication;

import java.util.HashMap;
import java.util.Map;

import static im.telegraph.util.ProfileConstants.SPRING_PROFILE_DEVELOPMENT;

public class ProfileUtils {

    private final static String SPRING_PROFILE_DEFAULT = "spring.profiles.default";

    public static void addDefaultProfile(SpringApplication app) {
        Map<String, Object> defProperties = new HashMap<>();
        defProperties.put(SPRING_PROFILE_DEFAULT, SPRING_PROFILE_DEVELOPMENT);
        app.setDefaultProperties(defProperties);
    }

}
