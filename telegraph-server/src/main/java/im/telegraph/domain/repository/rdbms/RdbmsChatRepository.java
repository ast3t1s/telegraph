package im.telegraph.domain.repository.rdbms;

import im.telegraph.common.rdbms.RdbmsOperations;
import im.telegraph.domain.entity.FileReferenceId;
import im.telegraph.domain.entity.Id;
import im.telegraph.domain.entity.chat.Chat;
import im.telegraph.domain.entity.chat.ChatPhoto;
import im.telegraph.domain.repository.ChatRepository;
import im.telegraph.engine.serialization.stream.BinaryInputStream;
import im.telegraph.engine.serialization.stream.BinaryOutputStream;
import io.r2dbc.spi.*;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * @author Ast3t1s
 */
@Repository
public class RdbmsChatRepository extends AbstractRdbmsRepository implements ChatRepository {

    private static final String FIELDS = "id, user_id, title, about, participants_count, permissions, chat_photo, " +
            "created_at, updated_at, updated_by";

    private static final String TABLE_NAME = "chats";

    private static final String SAVE_CHAT_QUERY = "INSERT INTO chats (" + FIELDS + ") VALUES (:id, :userId, :title, " +
            ":about, :participantsCount, :permissions, :chatPhoto, :createdAt, :updatedAt, :updatedBy)";

    private static final String GET_BY_ID_QUERY =
            "SELECT " + FIELDS + " FROM " + TABLE_NAME + " WHERE id = :chatId";

    private static final String LOAD_QUERY =
            "SELECT " + FIELDS + " FROM " + TABLE_NAME + " ORDER BY id";

    private static final String GET_BY_IDS_QUERY =
            "SELECT " + FIELDS + " FROM " + TABLE_NAME + " WHERE id IN (:ids)";

    private static final String GET_BY_USER_ID_QUERY =
            "SELECT " + FIELDS + " FROM " + TABLE_NAME + " WHERE user_id = :userId";

    private static final String GET_COUNT_QUERY =
            "SELECT COUNT(1) FROM " + TABLE_NAME;

    private static final String DELETE_BY_ID_QUERY =
            "DELETE FROM " + TABLE_NAME + " WHERE id = :chatId";

    private static final String UPDATE_CHAT_QUERY = "UPDATE chats SET" +
            " user_id = :userId," +
            " title = :title," +
            " about = :about," +
            " participants_count = :participantsCount," +
            " updated_at = :updatedAt," +
            " permissions = :permissions," +
            " chat_photo = :chatPhoto," +
            " updated_by = :updateBy" +
            " WHERE id = :chatId";

    private final RdbmsOperations rdbmsOperations;

    private final BiFunction<Row, RowMetadata, Chat> chatRowMapper = new ChatRowMapper();
    private final Function<Chat, Map<String, Object>> chatParameterMapper = new ChatParameterMapper();

    public RdbmsChatRepository(ConnectionFactory connectionFactory) {
        super(connectionFactory);
        this.rdbmsOperations = RdbmsOperations.create(connectionFactory);
    }

    @Override
    public Mono<Chat> insert(Chat chat) {
        return rdbmsOperations.withHandle(handle -> handle.createUpdate(SAVE_CHAT_QUERY)
                .bindMap(chatParameterMapper.apply(chat))
                .execute()
                .then(Mono.just(chat)))
                .next();
    }

    @Override
    public Mono<Chat> update(Chat chat) {
        return rdbmsOperations.withHandle(handle -> handle.createUpdate(UPDATE_CHAT_QUERY)
                .bindMap(chatParameterMapper.apply(chat))
                .execute()).then(Mono.just(chat));
    }

    @Override
    public Mono<Chat> findById(Id chatId) {
        return rdbmsOperations.withHandle(handler -> handler.createQuery(GET_BY_ID_QUERY)
                .bind("id", chatId.asLong())
                .mapRow(chatRowMapper))
                .next();
    }

    @Override
    public Flux<Chat> findAllById(Iterable<Id> ids) {
        return Flux.fromIterable(ids)
                .map(Id::asLong)
                .collectList()
                .flatMapMany(rawIds -> rdbmsOperations.withHandle(handle -> handle.createQuery(GET_BY_IDS_QUERY)
                        .bind("ids", rawIds)
                        .mapRow(chatRowMapper)));
    }

    @Override
    public Flux<Chat> findAllByUserId(Id userId) {
        return rdbmsOperations.withHandle(handle -> handle.createQuery(GET_BY_USER_ID_QUERY)
                .bind("userId", userId.asLong())
                .mapRow(chatRowMapper));
    }

    @Override
    public Flux<Chat> findAll(int limit, int offset) {
        return rdbmsOperations.withHandle(handle -> handle.createQuery(limitQuery(LOAD_QUERY, limit, offset))
                .mapRow(chatRowMapper));
    }

    @Override
    public Mono<Void> deleteById(Id chatId) {
        return rdbmsOperations.useHandle(handle -> handle.createUpdate(DELETE_BY_ID_QUERY)
                .bind("id", chatId.asLong())
                .execute());
    }

    @Override
    public Mono<Long> count() {
        return rdbmsOperations.withHandle(handle -> handle.createQuery(GET_COUNT_QUERY)
                .mapRow(row -> row.get(0, Long.class)))
                .next();
    }

    private static byte[] serialize(ChatPhoto chatPhoto) {
        final var stream = new BinaryOutputStream();
        stream.writeInt64(chatPhoto.getSmallPhoto().getFileId());
        stream.writeInt64(chatPhoto.getSmallPhoto().getAccessHash());
        stream.writeInt64(chatPhoto.getLargePhoto().getFileId());
        stream.writeInt64(chatPhoto.getLargePhoto().getAccessHash());
        return stream.toByteArray();
    }

    private static ChatPhoto deserialize(byte[] bytes) {
        try {
            final var stream = new BinaryInputStream(bytes);
            final var smallFileId = stream.readInt64();
            final var smallHash = stream.readInt64();
            final var largeFileId = stream.readInt64();
            final var largeFileHash = stream.readInt64();
            return new ChatPhoto(new FileReferenceId(smallFileId, smallHash), new FileReferenceId(largeFileId, largeFileHash));
        } catch (IOException e) {
            throw new IllegalArgumentException("Unable to deserialize chat photo", e);
        }
    }

    static final class ChatParameterMapper implements Function<Chat, Map<String, Object>> {

        @Override
        public Map<String, Object> apply(Chat chat) {
            final Map<String, Object> parameters = new HashMap<>();
            parameters.put("id", Parameters.in(R2dbcType.BIGINT, chat.getId().asLong()));
            parameters.put("userId", Parameters.in(R2dbcType.BIGINT, chat.getOwnerUserId().asLong()));
            parameters.put("title", Parameters.in(R2dbcType.VARCHAR, chat.getTitle()));
            parameters.put("about", Parameters.in(R2dbcType.VARCHAR, chat.getDescription().orElse(null)));
            parameters.put("participantsCount", Parameters.in(R2dbcType.INTEGER, chat.getMembersCount()));
            parameters.put("permissions", Parameters.in(R2dbcType.BIGINT, chat.getPermissions()));
            parameters.put("chatPhoto",
                    Parameters.in(R2dbcType.BINARY, chat.getChatPhoto().map(RdbmsChatRepository::serialize).orElse(null)));
            parameters.put("createdAt", Parameters.in(R2dbcType.TIMESTAMP,
                    LocalDateTime.ofInstant(chat.getCreatedAt(), ZoneOffset.UTC)));
            parameters.put("updatedAt", Parameters.in(R2dbcType.TIMESTAMP,
                    LocalDateTime.ofInstant(chat.getUpdatedAt(), ZoneOffset.UTC)));
            parameters.put("updatedBy", Parameters.in(R2dbcType.BIGINT, chat.getUpdatedById().asLong()));
            return parameters;
        }
    }

    static final class ChatRowMapper implements BiFunction<Row, RowMetadata, Chat> {

        @Override
        public Chat apply(Row row, RowMetadata metadata) {
            return Chat.builder()
                    .id(Id.of(row.get("id", Long.class)))
                    .ownerUserId(Id.of(row.get("user_id", Long.class)))
                    .title(row.get("title", String.class))
                    .description(row.get("about", String.class))
                    .membersCount(row.get("participants_count", Integer.class))
                    .permissions(row.get("permissions", Long.class))
                    .photo(Optional.ofNullable(row.get("chat_photo", byte[].class))
                            .map(RdbmsChatRepository::deserialize).orElse(null))
                    .createdAt(row.get("created_at", LocalDateTime.class).toInstant(ZoneOffset.UTC))
                    .updatedAt(row.get("updated_at", LocalDateTime.class).toInstant(ZoneOffset.UTC))
                    .updatedById(Id.of(row.get("updated_by", Long.class)))
                    .build();
        }
    }
}
