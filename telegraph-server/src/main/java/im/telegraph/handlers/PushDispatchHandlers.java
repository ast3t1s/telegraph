package im.telegraph.handlers;

import im.telegraph.api.rpc.RequestPushRegisterPushToken;
import im.telegraph.api.rpc.RequestPushUnregisterPushToken;
import im.telegraph.api.rpc.ResponseVoid;
import im.telegraph.domain.entity.PushDevice;
import im.telegraph.handlers.dispatch.DispatchContext;
import im.telegraph.service.PushService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
public class PushDispatchHandlers {

    private static final Logger log = LoggerFactory.getLogger(PushDispatchHandlers.class);

    private final PushService pushService;

    public PushDispatchHandlers(PushService pushService) {
        this.pushService = pushService;
    }

    public Mono<ResponseVoid> handleRegisterPushDevice(DispatchContext<RequestPushRegisterPushToken> context) {
        return pushService.saveToken(context.getAuthId(), PushDevice.Platform.FCM, context.getRequest().getToken())
                .thenReturn(new ResponseVoid());
    }

    public Mono<ResponseVoid> handleUnregisterFirebaseDevice(DispatchContext<RequestPushUnregisterPushToken> context) {
        return pushService.removeToken(context.getAuthId(), PushDevice.Platform.FCM, context.getRequest().getToken())
                .thenReturn(new ResponseVoid());
    }

}
