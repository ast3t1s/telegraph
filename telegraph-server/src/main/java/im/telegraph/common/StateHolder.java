package im.telegraph.common;

import im.telegraph.common.store.Store;
import im.telegraph.common.store.StoreService;
import im.telegraph.common.store.util.IntIntTuple2;
import im.telegraph.domain.entity.Contacts;
import im.telegraph.domain.entity.Presence;
import im.telegraph.domain.entity.Privacy;
import im.telegraph.domain.entity.User;
import im.telegraph.domain.entity.chat.Chat;
import im.telegraph.domain.entity.chat.ChatParticipant;
import im.telegraph.domain.entity.chat.ChatParticipants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Mono;

public class StateHolder {

    private static final Logger log = LoggerFactory.getLogger(StateHolder.class);

    private final StoreService storeService;
    private final Store<Long, User> usersStore;
    private final Store<Long, Chat> chatStore;
    private final Store<Long, Contacts> relationStores;
    private final Store<IntIntTuple2, Privacy> privacyStore;
    private final Store<IntIntTuple2, ChatParticipant> chatParticipantsStore;
    private final Store<Long, Presence> presenceStore;
    private final Store<Long, ChatParticipants> participantsStore;

    public StateHolder(final StoreService service) {
        this.storeService = service;

        this.usersStore = service.provideGenericStore(Long.class, User.class);
        log.debug("User storage: {}", usersStore);

        this.chatStore = service.provideGenericStore(Long.class, Chat.class);
        log.debug("Chat storage: {}", chatStore);

        this.relationStores = service.provideGenericStore(Long.class, Contacts.class);
        log.debug("Contacts storage: {}", relationStores);

        this.privacyStore = service.provideGenericStore(IntIntTuple2.class, Privacy.class);
        log.debug("Privacy storage: {}", privacyStore);

        this.chatParticipantsStore = service.provideGenericStore(IntIntTuple2.class, ChatParticipant.class);
        log.debug("Chat participants storage: {}", chatParticipantsStore);

        this.presenceStore = service.provideGenericStore(Long.class, Presence.class);
        log.debug("Presence storage: {}", presenceStore);

        this.participantsStore = service.provideGenericStore(Long.class, ChatParticipants.class);
        log.debug("Chat participants cache storage: {}", participantsStore);
    }

    public StoreService getStoreService() {
        return storeService;
    }

    public Store<Long, User> getUsersStore() {
        return usersStore;
    }

    public Store<Long, Chat> getChatStore() {
        return chatStore;
    }

    public Store<Long, Contacts> getRelationStores() {
        return relationStores;
    }

    public Store<IntIntTuple2, Privacy> getPrivacyStore() {
        return privacyStore;
    }

    public Store<IntIntTuple2, ChatParticipant> getChatParticipantsStore() {
        return chatParticipantsStore;
    }

    public Store<Long, Presence> getPresenceStore() {
        return presenceStore;
    }

    public Store<Long, ChatParticipants> getParticipantsStore() {
        return participantsStore;
    }

    public Mono<Void> invalidateStore() {
        return usersStore.invalidate()
                .and(chatStore.invalidate())
                .and(relationStores.invalidate())
                .and(privacyStore.invalidate())
                .and(chatParticipantsStore.invalidate())
                .and(presenceStore.invalidate())
                .and(participantsStore.invalidate());
    }
}
