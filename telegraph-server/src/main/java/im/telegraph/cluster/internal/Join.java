package im.telegraph.cluster.internal;

import im.telegraph.cluster.ClusterMessage;
import im.telegraph.cluster.UniqueAddress;

/**
 * @author Ast3t1s
 */
public class Join implements ClusterMessage {

    private static final long serialVersionUID = 1;

    private final UniqueAddress from;
    private final GossipOverview gossipOverview;

    public Join(UniqueAddress from, GossipOverview gossipOverview) {
        this.from = from;
        this.gossipOverview = gossipOverview;
    }

    public UniqueAddress getFrom() {
        return from;
    }

    public GossipOverview getGossipOverview() {
        return gossipOverview;
    }

    @Override
    public String toString() {
        return "Join{" +
                "from=" + from +
                ", gossipOverview=" + gossipOverview +
                '}';
    }
}
