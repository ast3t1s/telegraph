package im.telegraph.generator.type;

public class SchemeResponse extends SchemeBaseResponse {
    private String name;

    public SchemeResponse(String name, int header) {
        super(header);
        this.name = name;
    }

    public String getName() {
        return name;
    }
}