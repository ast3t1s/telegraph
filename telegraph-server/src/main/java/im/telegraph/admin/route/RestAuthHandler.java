package im.telegraph.admin.route;

import im.telegraph.admin.object.UsernamePasswordCredentials;
import reactor.core.publisher.Mono;

/**
 * @author Ast3t1s
 */
@FunctionalInterface
public interface RestAuthHandler {

    Mono<Boolean> verifyCredentials(UsernamePasswordCredentials credentials);
}
