package im.telegraph.protocol.model;

import im.telegraph.engine.serialization.DataInputStream;
import im.telegraph.engine.serialization.stream.BinaryInputStream;
import im.telegraph.protocol.model.mtproto.*;
import im.telegraph.protocol.model.mtproto.auth.*;
import im.telegraph.protocol.model.mtproto.rpc.*;
import im.telegraph.protocol.model.transport.*;
import im.telegraph.server.MTProtoObject;

import java.io.IOException;

public class ProtoSerializer {

    public static ProtoStruct readMessagePayload(byte[] data) throws IOException {
        return readMessagePayload(new BinaryInputStream(data, 0, data.length));
    }

    public static MTProtoObject readTransportPayload(int header, byte[] data) throws IOException {
        return readTransportPayload(header, new BinaryInputStream(data, 0, data.length));
    }

    public static MTProtoObject readTransportPayload(int header, DataInputStream inputStream) throws IOException {
        switch (header) {
            case Ack.HEADER: return new Ack(inputStream);
            case Ping.HEADER: return new Ping(inputStream);
            case Pong.HEADER: return new Pong(inputStream);
            case Drop.HEADER: return new Drop(inputStream);
            case HandshakeRequest.HEADER: return new HandshakeRequest(inputStream);
            case HandshakeResponse.HEADER: return new HandshakeResponse(inputStream);
            case ProtoPackage.HEADER: return new ProtoPackage(inputStream);
        }
        throw new IOException("Unable to read proto object with header #" + header);
    }

    public static ProtoStruct readMessagePayload(DataInputStream inputStream) throws IOException {
        final int header = inputStream.readByte();
        switch (header) {
            case MTUpdateBox.HEADER:
                return new MTUpdateBox(inputStream);
            case Container.HEADER:
                return new Container(inputStream);
            case MessageAck.HEADER:
                return new MessageAck(inputStream);
            case AuthIdInvalid.HEADER:
                return new AuthIdInvalid(inputStream);
            case RequestAuthId.HEADER:
                return new RequestAuthId(inputStream);
            case RequestStartAuth.HEADER:
                return new RequestStartAuth(inputStream);
            case ResponseStartAuth.HEADER:
                return new ResponseStartAuth(inputStream);
            case RequestGetServerKey.HEADER:
                return new RequestGetServerKey(inputStream);
            case ResponseGetServerKey.HEADER:
                return new ResponseGetServerKey(inputStream);
            case RequestDH.HEADER:
                return new RequestDH(inputStream);
            case ResponseDH.HEADER:
                return new ResponseDH(inputStream);
            case MTRpcRequest.HEADER:
                return new MTRpcRequest(inputStream);
            case MTRpcResponse.HEADER:
                return new MTRpcResponse(inputStream);
        }
        throw new IOException("Unable to read proto object with header #" + header);
    }

    public static ProtoStruct readRpcResponsePayload(byte[] data) throws IOException {
        DataInputStream inputStream = new BinaryInputStream(data, 0, data.length);
        final var header = inputStream.readByte();
        switch (header) {
            case RpcOk.HEADER:
                return new RpcOk(inputStream);
            case RpcError.HEADER:
                return new RpcError(inputStream);
            case RpcInternalError.HEADER:
                return new RpcInternalError(inputStream);
        }
        throw new IOException("Unable to read proto object");
    }

    public static ProtoStruct readRpcRequestPayload(byte[] data) throws IOException {
        final DataInputStream inputStream = new BinaryInputStream(data, 0, data.length);
        final var header = inputStream.readByte();
        switch (header) {
            case RpcRequest.HEADER:
                return new RpcRequest(inputStream);
        }
        throw new IOException("Unable to read proto object with header #" + header);
    }

    public static RpcEventPush readUpdate(byte[] bs) throws IOException {
        return readUpdate(new BinaryInputStream(bs, 0, bs.length));
    }

    public static RpcEventPush readUpdate(DataInputStream bs) throws IOException {
        return new RpcEventPush(bs);
    }
}
