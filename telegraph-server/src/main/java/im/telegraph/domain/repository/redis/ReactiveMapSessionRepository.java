package im.telegraph.domain.repository.redis;

import im.telegraph.domain.entity.Id;
import im.telegraph.domain.entity.SessionInfo;
import im.telegraph.domain.repository.SessionRepository;
import reactor.core.publisher.Mono;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Ast3t1s
 */
public class ReactiveMapSessionRepository implements SessionRepository {

    private final Map<Id, SessionInfo> sessions;

    private final ExpiredSessionChecker expiredSessionChecker = new ExpiredSessionChecker();

    public ReactiveMapSessionRepository(Map<Id, SessionInfo> sessions) {
        if (sessions == null) {
            throw new IllegalArgumentException("sessions can not be null");
        }
        this.sessions = sessions;
    }

    @Override
    public Mono<SessionInfo> save(SessionInfo sessionInfo) {
        return Mono.defer(() -> Mono.justOrEmpty(sessionInfo))
                .doOnNext(info -> sessions.put(info.getUserId(), info))
                .switchIfEmpty(Mono.error(() -> new IllegalStateException("Missing session info")));
    }

    @Override
    public Mono<SessionInfo> findById(Id userId) {
        expiredSessionChecker.checkIfNecessary(Instant.now());
        return Mono.defer(() -> Mono.justOrEmpty(this.sessions.get(userId)))
                .filter(session -> session.getDevices().isEmpty())
                .switchIfEmpty(delete(userId).then(Mono.empty()));
    }

    @Override
    public Mono<Void> delete(Id userId) {
        return Mono.defer(() -> Mono.fromRunnable(() -> this.sessions.remove(userId)));
    }

    private class ExpiredSessionChecker {

        private static final int CHECK_PERIOD = 60 * 1000;

        private final ReentrantLock lock = new ReentrantLock();

        private Instant checkTime = Instant.now().plus(CHECK_PERIOD, ChronoUnit.MILLIS);

        public void checkIfNecessary(Instant now) {
            if (this.checkTime.isBefore(now)) {
                removeExpiredDevices(now);
            }
        }

        public void removeExpiredDevices(Instant now) {
            if (sessions.isEmpty()) {
                return;
            }
            if (this.lock.tryLock()) {
                try {
                    for (SessionInfo session : sessions.values()) {
                        session.getDevices().values().removeIf(device -> device.getExpireAt().isBefore(now));
                    }
                } finally {
                    this.checkTime = now.plus(CHECK_PERIOD, ChronoUnit.MILLIS);
                    this.lock.unlock();
                }
            }
        }
    }
}
