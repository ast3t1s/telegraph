package im.telegraph.protocol.model.mtproto.auth;

import im.telegraph.engine.serialization.DataInputStream;
import im.telegraph.engine.serialization.DataOutputStream;
import im.telegraph.protocol.model.mtproto.ProtoStruct;

import java.io.IOException;

public class RequestDH extends ProtoStruct {

	public static final int HEADER = 0xE6;

	private long randomId;
	private long keyId;
	private byte[] clientNonce;
	private byte[] clientKey;

	public RequestDH(DataInputStream inputStream) throws IOException {
		super(inputStream);
	}

	public RequestDH(long randomId, long keyId, byte[] clientNonce, byte[] clientKey) {
		this.randomId = randomId;
		this.keyId = keyId;
		this.clientNonce = clientNonce;
		this.clientKey = clientKey;
	}

	public long getRandomId() {
		return randomId;
	}

	public long getKeyId() {
		return keyId;
	}

	public byte[] getClientNonce() {
		return clientNonce;
	}

	public byte[] getClientKey() {
		return clientKey;
	}

	@Override
	protected byte getHeader() {
		return (byte) HEADER;
	}

	@Override
	protected void writeBody(DataOutputStream outputStream) throws IOException {
		outputStream.writeInt64(randomId);
		outputStream.writeInt64(keyId);
		outputStream.writeProtoBytes(clientNonce);
		outputStream.writeProtoBytes(clientKey);
	}

	@Override
	protected void readBody(DataInputStream inputStream) throws IOException {
		this.randomId = inputStream.readInt64();
		this.keyId = inputStream.readInt64();
		this.clientNonce = inputStream.readProtoBytes();
		this.clientKey = inputStream.readProtoBytes();
	}

}
