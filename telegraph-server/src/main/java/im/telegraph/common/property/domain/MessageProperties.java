package im.telegraph.common.property.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.time.Duration;

/**
 * @author Ast3t1s
 */
@Getter
@Setter
@Accessors(chain = true)
public class MessageProperties {

    /**
     * The maximum allowed size for the text of the message.
     */
    private int maxTextSize = 4096;

    private boolean messageAsync = true;

    private int batchSize = 50;

    private Duration interval = Duration.ofMillis(2_000);

    private boolean dialogsAsync;

    private Duration dialogInterval = Duration.ofSeconds(5);

    private boolean dialogMarkerAsync;

    private Duration dialogMarkerInterval = Duration.ofSeconds(5);
} 
