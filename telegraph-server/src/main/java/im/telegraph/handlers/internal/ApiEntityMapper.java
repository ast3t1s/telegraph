package im.telegraph.handlers.internal;

import im.telegraph.api.*;
import im.telegraph.domain.entity.*;
import im.telegraph.domain.entity.chat.Chat;
import im.telegraph.domain.entity.chat.ChatParticipant;
import im.telegraph.domain.entity.chat.ChatPhoto;
import im.telegraph.domain.entity.photo.Photo;
import reactor.util.annotation.Nullable;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Ast3t1s
 */
public final class ApiEntityMapper {

    private ApiEntityMapper() {
    }

    @Nullable
    public static ApiUserBlocked toApiContactBlocked(Privacy privacy) {
        if (privacy == null) {
            return null;
        }
        final ApiUserBlocked apiUserBlocked = new ApiUserBlocked();
        apiUserBlocked.setUserId(privacy.getBlockedId().asLong());
        apiUserBlocked.setDate(privacy.getCreatedAt().getEpochSecond());
        return apiUserBlocked;
    }

    /**
     * Map {@link Contact} to {@link ApiContactRecord}
     *
     * @param contact the relation entity.
     * @return the instance of {@link ApiContactRecord}
     */
    @Nullable
    public static ApiContactRecord convertToApiContact(final Contact contact) {
        if (contact == null) {
            return null;
        }
        final ApiMutualState mutualState = new ApiMutualState();
        mutualState.setRequestedIn(contact.isRequestedIn());
        mutualState.setRequestedOut(contact.isRequestedOut());
        mutualState.setMutual(contact.isMutual());
        mutualState.setRemove(contact.isRemoved());
        return new ApiContactRecord(contact.getContactId().asLong(), mutualState);
    }

    @Nullable
    public static ApiChatMarker toApiParticipantsMark(@Nullable DialogMarker historyInbox) {
        if (historyInbox == null) {
            return null;
        }
        ApiChatMarker chatMarker = new ApiChatMarker();
        chatMarker.setUserId(historyInbox.getUserId().asLong());
        if (historyInbox.isReceived()) {
            chatMarker.setState(ApiMessageState.RECEIVED);
        } else if (historyInbox.isRead()) {
            chatMarker.setState(ApiMessageState.READ);
        } else {
            chatMarker.setState(ApiMessageState.SENT);
        }
        chatMarker.setDate(historyInbox.getTimestamp().toEpochMilli());
        return chatMarker;
    }

    @Nullable
    public static ApiAvatar convertToApiAvatar(@Nullable final Photo avatar) {
        if (avatar == null) {
            return null;
        }

        final ApiAvatar apiPhoto = new ApiAvatar();
        apiPhoto.setAvatarId(avatar.getId().asLong());
        apiPhoto.setAddedDate((int) avatar.getCreatedAt().getEpochSecond());

        final ApiAvatarImage smallAvatar = new ApiAvatarImage();
        smallAvatar.setWidth(avatar.getPhotoSmall().getWidth());
        smallAvatar.setHeight(avatar.getPhotoSmall().getHeight());
        smallAvatar.setFileLocation(new ApiFileLocation(avatar.getPhotoSmall().getFileReferenceId().getFileId(),
                avatar.getPhotoSmall().getFileReferenceId().getAccessHash()));
        apiPhoto.setAvatarSmall(smallAvatar);

        final ApiAvatarImage bigAvatar = new ApiAvatarImage();
        bigAvatar.setWidth(avatar.getPhotoLarge().getWidth());
        bigAvatar.setWidth(avatar.getPhotoLarge().getHeight());
        bigAvatar.setFileLocation(new ApiFileLocation(avatar.getPhotoLarge().getFileReferenceId().getFileId(),
                avatar.getPhotoLarge().getFileReferenceId().getAccessHash()));
        apiPhoto.setAvatarBig(bigAvatar);

        return apiPhoto;
    }


    /**
     * Map {@link Chat} entity to {@link ApiChat}
     *
     * @param chat the group entity.
     * @return the instance of {@link ApiChat}
     */
    @Nullable
    public static ApiChat toApiChat(Chat chat) {
        if (chat == null) {
            return null;
        }
        final ApiChat apiChat = new ApiChat();
        apiChat.setId(chat.getId().asLong());
        apiChat.setTitle(chat.getTitle());
        apiChat.setPermissions(chat.getPermissions());
        apiChat.setCreateDate(chat.getCreatedAt().getEpochSecond());
        apiChat.setCreatorUid(chat.getOwnerUserId().asLong());
        chat.getDescription().ifPresent(apiChat::setAbout);
        apiChat.setChatAvatarPhoto(convertChatPhoto(chat.getPhoto()));
        return apiChat;
    }

    /**
     * Map {@link Chat} entity and {@link ChatParticipant} to {@link ApiChatFull}
     *
     * @param chat        the group entity.
     * @param chatMembers the list of group members entities.
     * @return the instance of {@link ApiChatFull}
     */
    @Nullable
    public static ApiChatFull toApiChatFull(Chat chat, Collection<ChatParticipant> chatMembers) {
        if (chat == null || chatMembers == null) {
            return null;
        }
        final ApiChatFull apiChatFull = new ApiChatFull();
        apiChatFull.setId(chat.getId().asLong());
        apiChatFull.setTitle(chat.getTitle());
        apiChatFull.setCreateDate(chat.getCreatedAt().getEpochSecond());
        apiChatFull.setCreatorUid(chat.getOwnerUserId().asLong());
        chat.getDescription().ifPresent(apiChatFull::setAbout);
        apiChatFull.setMembersCount(chat.getMembersCount());
        apiChatFull.setChatAvatarPhoto(convertChatPhoto(chat.getPhoto()));
        apiChatFull.setPermissions(chat.getPermissions());

        final List<ApiParticipant> apiParticipants = chatMembers.stream()
                .map(ApiEntityMapper::convertToApiMember).collect(Collectors.toList());
        apiChatFull.setParticipants(apiParticipants);
        return apiChatFull;
    }

    @Nullable
    protected static ApiChatAvatarPhoto convertChatPhoto(ChatPhoto chatPhoto) {
        if (chatPhoto == null) {
            return null;
        }
        final ApiChatAvatarPhoto chatAvatarPhoto = new ApiChatAvatarPhoto();
        chatAvatarPhoto.setSmallPhoto(new ApiFileLocation(chatPhoto.getSmallPhoto().getFileId(),
                chatPhoto.getSmallPhoto().getAccessHash()));
        chatAvatarPhoto.setBigPhoto(new ApiFileLocation(chatPhoto.getLargePhoto().getFileId(),
                chatPhoto.getLargePhoto().getAccessHash()));
        return chatAvatarPhoto;
    }

    @Nullable
    public static ApiParticipant convertToApiMember(ChatParticipant chatMember) {
        if (chatMember == null) {
            return null;
        }
        final ApiParticipant apiParticipant = new ApiParticipant();
        apiParticipant.setUid(chatMember.getUserId().asLong());
        apiParticipant.setInviterUid(chatMember.getInviterId().asLong());
        apiParticipant.setIsAdmin(chatMember.isAdmin());
        if (chatMember.getStatus() == ChatParticipant.Status.OWNER) {
            apiParticipant.setStatus(ApiParticipantStatus.REQUESTED);
        } else if (chatMember.getStatus() == ChatParticipant.Status.PARTICIPANT) {
            apiParticipant.setStatus(ApiParticipantStatus.MEMBER);
        } else if (chatMember.getStatus() == ChatParticipant.Status.BANNED) {
            apiParticipant.setStatus(ApiParticipantStatus.BANNED);
        } else {
            apiParticipant.setStatus(ApiParticipantStatus.UNSUPPORTED_VALUE);
        }
        return apiParticipant;
    }

    /**
     * Map {@link AuthSession} entity to {@link ApiAuthSession}
     *
     * @param session the auth session entity.
     * @return the instance of {@link ApiAuthSession}
     */
    @Nullable
    public static ApiAuthSession convertToApiSession(AuthSession session) {
        if (session == null) {
            return null;
        }

        final ApiAuthSession apiAuthSession = new ApiAuthSession();
        apiAuthSession.setId(session.getId().asLong());
        apiAuthSession.setAppName(session.getAppName());
        apiAuthSession.setAppVersion(session.getAppVersion());
        apiAuthSession.setPlatform(session.getPlatform());
        apiAuthSession.setSystemVersion("Verson 1.0.0");//session.getSystemVersion());
        apiAuthSession.setDeviceModel(session.getDeviceModel());
        apiAuthSession.setCountry("Ukraine");//session.getCountry());
        apiAuthSession.setRegion("Dnipro");//session.getRegion());
        apiAuthSession.setIp("127.0.0.1");//session.getIp());
        apiAuthSession.setCreatedAt((int) session.getCreatedAt().getEpochSecond());
        return apiAuthSession;
    }

    public static ApiPeer toApiPeer(final Peer peer) {
        if (peer.isUser()) {
            return new ApiPeerUser(peer.getPeerId());
        } else if (peer.isChat()) {
            return new ApiPeerChat(peer.getPeerId());
        } else {
            throw new IllegalStateException("Invalid peer type");
        }
    }
} 
