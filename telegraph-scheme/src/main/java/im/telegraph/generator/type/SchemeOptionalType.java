package im.telegraph.generator.type;

public class SchemeOptionalType extends SchemeType {
    private SchemeType type;

    public SchemeOptionalType(SchemeType type) {
        this.type = type;
    }

    public SchemeType getType() {
        return type;
    }

    @Override
    public String toString() {
        return "opt<" + type + ">";
    }
}