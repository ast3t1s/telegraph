package im.telegraph.common.property.file;

import org.springframework.boot.convert.DataSizeUnit;
import org.springframework.boot.convert.DurationUnit;
import org.springframework.util.unit.DataSize;
import org.springframework.util.unit.DataUnit;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

/**
 * @author Ast3t1s
 */
public class FileStoreProperties {

    @DurationUnit(ChronoUnit.SECONDS)
    private Duration expirationTime = Duration.ofSeconds(120);

    @DataSizeUnit(DataUnit.BYTES)
    private DataSize maxFileSize = DataSize.ofBytes(10485760);

    /**
     * The number of maximum retry attempts used for configuring retry spec.
     */
    private Integer retryAttempts = 3;

    /**
     * The backoff period in milliseconds used for configuring retry spec
     */
    @DurationUnit(ChronoUnit.MILLIS)
    private Duration retryBackoff = Duration.ofMillis(2_000);

    private final S3Properties s3 = new S3Properties();

    public DataSize getMaxFileSize() {
        return maxFileSize;
    }

    public void setMaxFileSize(DataSize maxFileSize) {
        this.maxFileSize = maxFileSize;
    }

    public Duration getExpirationTime() {
        return expirationTime;
    }

    public void setExpirationTime(Duration expirationTime) {
        this.expirationTime = expirationTime;
    }

    public Integer getRetryAttempts() {
        return retryAttempts;
    }

    public void setRetryAttempts(Integer retryAttempts) {
        this.retryAttempts = retryAttempts;
    }

    public Duration getRetryBackoff() {
        return retryBackoff;
    }

    public void setRetryBackoff(Duration retryBackoff) {
        this.retryBackoff = retryBackoff;
    }

    public S3Properties getS3() {
        return s3;
    }

} 
