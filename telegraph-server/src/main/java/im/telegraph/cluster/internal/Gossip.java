package im.telegraph.cluster.internal;

import java.io.Serializable;

/**
 * @author Ast3t1s
 */
public class Gossip implements Serializable {

    private static final long serialVersionUID = 1L;

    private final String gossipId;
    private final GossipOverview gossipOverview;

    public Gossip(String gossipId, GossipOverview gossipOverview) {
        this.gossipId = gossipId;
        this.gossipOverview = gossipOverview;
    }

    public String gossipId() {
        return gossipId;
    }

    public GossipOverview overview() {
        return gossipOverview;
    }

    @Override
    public String toString() {
        return "Gossip{" +
                "gossipId='" + gossipId + '\'' +
                ", message=" + gossipOverview +
                '}';
    }
}