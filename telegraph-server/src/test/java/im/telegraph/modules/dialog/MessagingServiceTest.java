package im.telegraph.modules.dialog;

import im.telegraph.TelegraphApplication;
import im.telegraph.api.ApiTextMessage;
import im.telegraph.domain.entity.Contact;
import im.telegraph.domain.entity.Id;
import im.telegraph.domain.entity.Message;
import im.telegraph.domain.entity.Peer;
import im.telegraph.domain.entity.chat.Chat;
import im.telegraph.domain.entity.chat.ChatParticipant;
import im.telegraph.domain.entity.chat.PermissionSet;
import im.telegraph.domain.repository.*;
import im.telegraph.service.ContactsService;
import im.telegraph.service.IdGeneratorManager;
import im.telegraph.service.MessagingService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import reactor.core.publisher.Flux;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

import static im.telegraph.service.IdGeneratorManager.EntityType;

//@ExtendWith(TimingExtension.class)
@SpringBootTest(classes = TelegraphApplication.class)
class MessagingServiceTest {

    @Autowired
    private DialogRepository dialogRepository;

    @Autowired
    private DialogMarkerRepository historyInboxRepository;

    @Autowired
    private MessageRepository historyMessageRepository;

    @Autowired
    private MessagingService messagingService;

    @Autowired
    private ContactsRepository contactsRepository;

    @Autowired
    private ChatRepository chatRepository;

    @Autowired
    private ChatParticipantRepository chatParticipantRepository;

    @Autowired
    private IdGeneratorManager idGeneratorService;

    @Autowired
    private ContactsService contactsService;

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    public void testContacts() {
        contactsService.getContacts(Id.of(1L)).block();
    }

    @RepeatedTest(50)
  // @Test
   public void test_createMessage() {
        messagingService.createMessage(Id.of(111), Peer.ofChat(Id.of(530048650660655105L)), 12312L, Instant.now(),
                new ApiTextMessage("text", Collections.emptyList(), null)).block();
    }

    @Test
    public void testCreateChats() {
        Id chatId = idGeneratorService.generateEntityId(EntityType.CHAT);
        Id ownerId = idGeneratorService.generateEntityId(EntityType.USER);
        Chat chat = Chat.builder()
                .id(chatId)
                .ownerUserId(ownerId)
                .accessSalt("some-salt")
                .description("test-chat")
                .membersCount(200)
                .createdAt(Instant.now())
                .title("Test-chat")
                .permissions(PermissionSet.all().getRawValue())
                .updatedById(ownerId)
                .updatedAt(Instant.now())
                .build();

        chatRepository.insert(chat).block();

        Flux.range(1, 200)
                .map(id -> ChatParticipant
                        .builder()
                        .chatId(chatId)
                        .userId(Id.of(id))
                        .inviterId(ownerId)
                        .createdAt(Instant.now())
                        .status(ChatParticipant.Status.PARTICIPANT)
                        .build())
                .flatMap(chatParticipantRepository::insert)
                .blockLast();
    }

    @Test
    public void retrieveInboxes() {
        List<Peer> peers = new ArrayList<>();
        for (int i = 1; i < 50; i++) {
            peers.add(Peer.ofChat(Id.of(i)));
        }
        historyInboxRepository.findAllByUserIdAndPeerIn(Id.of(1), peers)
                .doOnNext(System.out::println)
                .blockLast();
    }

    @Test
    public void testSaveInbox() {
       /* historyInboxRepository.save(HistoryInbox.builder()
                .userId(UserId.of(1))
                .peer(Peer.ofChat(ChatId.of(2)))
                .ownerLastReadAt(Instant.now())
                .ownerLastReceivedAt(Instant.now())
                .build()).block();*/
    }

    @Test
    public void testRemoveMessages() {
        List<Message> messages = LongStream.range(1, 500)
                .boxed().map(id -> Message.builder()
                      //  .peer(Peer.ofChat(ChatId.of(5)))
                       // .userId(UserId.of(1))
                        .senderId(Id.of(1))
                        .type(Message.Type.TEXT)
                        .createdAt(Instant.now())
                        .content(new ApiTextMessage("text", Collections.emptyList(), null).toByteArray())
                        .randomId(id)
                        .build())
                .collect(Collectors.toList());

       // this.historyMessageRepository.saveAll(messages).blockLast();

        List<Id> delete = LongStream.range(460, 490).boxed()
                .map(Id::of)
                .collect(Collectors.toList());

        List<Id> deleted = this.messagingService.deleteMessages(Id.of(1), Peer.ofChat(Id.of(5)), delete)
                .collectList()
                .block();
        System.out.println(deleted);
    }

    @Test
    public void test_contacts() {
        List<Contact> toSave = IntStream.range(200, 250)
                .boxed()
                .map(id -> new Contact(Id.of(1), Id.of(id), Contact.Status.BOTH, Instant.now()))
                .collect(Collectors.toList());

       // contactsRepository.saveAll(toSave).blockLast();

      /*  List<Contact> contacts = contactsRepository.findAllByUserId(UserId.of(1), OffsetPageRequest.of(50, 0))
                .collectList().block();

        System.out.println(contacts);*/
    }


    // @Test
    @RepeatedTest(value = 10)
    public void test_send_message() {
        /*StepVerifier.create(this.messagingService.sendMessage(12312L, UserId.of(1), Peer.fromUserId(2),
                213131L, Instant.now(), new ApiTextMessage("Some text", Collections.emptyList(), null)))
                .expectSubscription()
                .assertNext(new Consumer<SequenceUpdate>() {
                    @Override
                    public void accept(SequenceUpdate seqState) {
                        System.out.println(seqState);
                    }
                })
                .verifyComplete();*/
    }

    @Test
    public void test_mark_received() {
        Instant now = Instant.now();
        /*StepVerifier.create(this.messagingService.markMessageReceived(UserId.of(1), Peer.fromUserId(2), Instant.now().minus(Duration.ofMinutes(5)), now))
                .expectSubscription()
                .verifyComplete();*/
    }

    @Test
    public void test_mark_read() {
        Instant now = Instant.now();
        /*StepVerifier.create(this.messagingService.markMessageAsRead(UserId.of(1), Peer.fromUserId(2), Instant.now().minus(Duration.ofMinutes(5)), now))
                .expectSubscription()
                .verifyComplete();*/

    }
}