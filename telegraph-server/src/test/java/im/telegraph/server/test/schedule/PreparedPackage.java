package im.telegraph.server.test.schedule;

public class PreparedPackage {

    private byte[] payload;

    private int offset;

    private int length;

    public PreparedPackage(byte[] payload, int offset, int length) {
        this.payload = payload;
        this.offset = offset;
        this.length = length;
    }

    public byte[] getPayload() {
        return payload;
    }

    public int getOffset() {
        return offset;
    }

    public int getLength() {
        return length;
    }

}
