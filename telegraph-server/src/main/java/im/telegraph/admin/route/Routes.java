package im.telegraph.admin.route;

import io.netty.handler.codec.http.HttpMethod;
import reactor.core.publisher.Mono;
import reactor.netty.http.server.HttpServerRequest;
import reactor.netty.http.server.HttpServerResponse;

import java.util.List;
import java.util.Objects;
import java.util.function.BiFunction;

/**
 * @author Ast3t1s
 */
@FunctionalInterface
public interface Routes {

    List<Route> routes();

    static Route.Action corsHeaders(Route.Action action) {
        return (req, res) -> action.handleRequest(req, res
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
                .header("Access-Control-Allow-Headers", "Content-Type, Authorization, Accept")
                .header("Access-Control-Max-Age", "86400"));
    }

    class RouteV2 {

        private final HttpMethod method;
        private final String path;
        private final UriMatcher uriMatcher;
        private final BiFunction<HttpServerRequest, HttpServerResponse, Mono<Void>> handlers;

        public RouteV2(HttpMethod method,
                       String path,
                       UriMatcher uriMatcher,
                       BiFunction<HttpServerRequest, HttpServerResponse, Mono<Void>> handlers) {
            this.method = Objects.requireNonNull(method);
            this.path = Objects.requireNonNull(path);
            this.uriMatcher = uriMatcher;
            this.handlers = handlers;
        }

        public HttpMethod getMethod() {
            return method;
        }

        public String getPath() {
            return path;
        }

        public BiFunction<HttpServerRequest, HttpServerResponse, Mono<Void>> getHandlers() {
            return handlers;
        }

        public boolean matches(HttpMethod method, String uri) {
            return this.method.equals(method) && uriMatcher.matches(uri);
        }
    }
} 
