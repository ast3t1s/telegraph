package im.telegraph.domain.entity.chat;

/**
 * Describes the flags of a group permission settings.
 */
public enum Permission {

    /**
     * The members of the group can see who is admin or not in the group.
     */
    SHOW_ADMINS_TO_MEMBERS(0),

    /**
     * The members of the group can invite other users to the group.
     */
    CAN_MEMBERS_INVITE(1),

    /**
     * The members of the group can edit group info (title, avatar, about, etc.)
     */
    CAN_MEMBERS_EDIT_GROUP_INFO(2),

    /**
     * The admins of the group can edit group info (title, avatar, about, etc.) but participants can't.
     */
    CAN_ADMINS_EDIT_GROUP_INFO(3),

    /**
     * Send service message to all group members when someone leave the group.
     */
    SHOW_JOIN_LEAVE_MESSAGE(4),

    /**
     * Allows the admins of the group can ban participants, the participant can not send messages but receive.
     */
    CAN_ADMINS_BAN_MEMBERS(5);

    private final int value;

    Permission(final int value) {
        this.value = 1 << value;
    }

    public int getValue() {
        return value;
    }
}
