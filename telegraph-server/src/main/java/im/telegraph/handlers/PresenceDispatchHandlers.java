package im.telegraph.handlers;

import im.telegraph.api.ApiUserPresenceEmpty;
import im.telegraph.api.ApiUserPresenceOffline;
import im.telegraph.api.ApiUserPresenceOnline;
import im.telegraph.api.rpc.RequestWeakGetPresence;
import im.telegraph.api.rpc.RequestWeakSetOnline;
import im.telegraph.api.rpc.ResponseVoid;
import im.telegraph.api.rpc.ResponseWeakGetPresence;
import im.telegraph.domain.entity.Id;
import im.telegraph.handlers.dispatch.DispatchContext;
import im.telegraph.service.PresenceService;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.time.Instant;

@Component
public class PresenceDispatchHandlers {

    private final PresenceService presenceService;

    public PresenceDispatchHandlers(PresenceService presenceService) {
        this.presenceService = presenceService;
    }

    public Mono<ResponseVoid> handleSetOnline(DispatchContext<RequestWeakSetOnline> context) {
        return Mono.justOrEmpty(context.getUserId().map(Id::of))
                .flatMap(selfId -> {
                    final Duration timeout = Duration.ofMillis(context.getRequest().getTimeout());
                    final boolean isOnline = context.getRequest().isOnline();
                    final long authId = context.getAuthId();

                    Mono<Void> setPresence = isOnline
                            ? presenceService.setOnline(selfId, authId, timeout)
                            : presenceService.setOffline(selfId, authId);

                    return setPresence.thenReturn(new ResponseVoid());
                });
    }

    public Mono<ResponseWeakGetPresence> handleGetPresence(DispatchContext<RequestWeakGetPresence> context) {
        return Mono.justOrEmpty(context.getUserId().map(Id::of))
                .flatMap(selfId -> presenceService.getPresence(selfId, context.getAuthId()))
                .map(presence -> {
                    final Instant lastSeenAt = presence.getLastSeenAt();
                    if (lastSeenAt.isBefore(Instant.now())) {
                        return new ApiUserPresenceOffline((int) lastSeenAt.getEpochSecond());
                    } else {
                        return new ApiUserPresenceOnline((int) lastSeenAt.getEpochSecond());
                    }
                })
                .defaultIfEmpty(new ApiUserPresenceEmpty())
                .map(ResponseWeakGetPresence::new);
    }
} 
