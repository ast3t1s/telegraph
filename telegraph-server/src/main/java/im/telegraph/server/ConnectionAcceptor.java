package im.telegraph.server;

import org.reactivestreams.Publisher;
import reactor.core.publisher.Mono;

/**
 * Handler for transport session.
 *
 * <p>
 * A server {@code TransportSessionHandler} is mapped to requests
 * </p>
 *
 * <p>
 * Use {@link TransportConnection#receive() connection.receive()} to compose on the inbound message
 * stream, and {@link TransportConnection#send(Publisher)} (Publisher) connection.send(publisher)} for the outbound
 * message stream. Below is an example, combined flow to process inbound and to send
 * outbound messages:
 *
 * <pre class="code">
 *     &#064;Override
 *   	public Mono&lt;Void&gt; handle(TransportSession session) {
 *
 *   		Flux&lt;SocketMessage&gt; output = session.receive()
 *  			.doOnNext(message -> {
 *   				// ...
 *            })
 *   			.concatMap(message -> {
 *   				// ...
 *            })
 *   			.map(value -> session.binaryMessage(("Echo " + value).getBytes());
 *
 *   		return session.send(output);
 *    }
 *   }
 *
 *   <p>If processing inbound and sending outbound messages are independent
 *   streams, they can be joined together with the "zip" operator:
 *
 *   <pre class="code">
 *   class ExampleHandler implements TransportSessionHandler {
 *   	&#064;Override
 *   	public Mono&lt;Void&gt; handle(TransportSession session) {
 *
 *   		Mono&lt;Void&gt; input = session.receive()
 *  			.doOnNext(message -> {
 *   				// ...
 *            })
 *   			.concatMap(message -> {
 *   				// ...
 *            })
 *   			.then();
 *
 *  		Flux&lt;String&gt; source = ... ;
 *   		Mono&lt;Void&gt; output = session.send(source.map(session::binaryMessage));
 *
 *   		return Mono.zip(input, output).then();
 *    }
 *   }
 *
 *   </pre>
 * </p>
 *
 * <p>
 * A {@code TransportSessionHandler} must compose the inbound and outbound streams into a unified
 * flow and return a {@code Mono<Void>} that reflects the completion of that flow. That
 * means there is no need to check if the connection is open, since Reactive Streams
 * signals will terminate activity. The inbound stream receives a completion/error signal,
 * and the outbound stream receives a cancellation signal.
 *
 * @author Ast3t1s
 */
@FunctionalInterface
public interface ConnectionAcceptor {

    /**
     * Accept a new {@code TransportConnection} and returns {@code Publisher} signifying the end of
     * processing of the connection.
     *
     * @param connection New {@code DuplexConnection} to be processed.
     * @return A {@code Publisher} which terminates when the processing of the connection finishes.
     */
    Mono<Void> accept(TransportConnection connection);
}
