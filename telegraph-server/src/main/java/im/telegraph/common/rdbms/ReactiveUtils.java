package im.telegraph.common.rdbms;

import org.reactivestreams.Publisher;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Objects;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * @author Ast3t1s
 */
public class ReactiveUtils {

    private ReactiveUtils() {
    }

    /**
     * Execute the {@link Publisher} provided by a {@link Supplier} and propagate the error that initiated this behavior.
     *
     * @param s   a {@link Supplier} of a {@link Publisher} to execute when an error occurs
     * @param <T> the type passing through the flow
     * @return a {@link Mono#error(Throwable)} with the original error
     */
    public static <T> Function<Throwable, Mono<T>> appendError(Supplier<Publisher<?>> s) {
        Objects.requireNonNull(s);
        return t ->
                Flux.from(s.get())
                        .then(Mono.error(t));
    }

    /**
     * Convert a {@code Publisher<Void>} to a {@code Publisher<T>} allowing for type passthrough behavior.
     *
     * @param s   a {@link Supplier} of a {@link Publisher} to execute
     * @param <T> the type passing through the flow
     * @return {@link Mono#empty()} of the appropriate type
     */
    public static <T> Mono<T> typeSafe(Supplier<Publisher<Void>> s) {
        Objects.requireNonNull(s);
        return Flux.from(s.get())
                .then(Mono.empty());
    }
} 
