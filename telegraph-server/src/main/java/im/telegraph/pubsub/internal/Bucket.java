package im.telegraph.pubsub.internal;

import im.telegraph.internal.remote.Address;

import java.util.Map;

/**
 * @author Ast3t1s
 */
public class Bucket {

    private final Address address;
    private final long version;
    private final Map<String, ValueHolder> content;

    public Bucket(Address address, long version, Map<String, ValueHolder> content) {
        this.address = address;
        this.version = version;
        this.content = content;
    }

    public Address getAddress() {
        return address;
    }

    public long getVersion() {
        return version;
    }

    public Map<String, ValueHolder> getContent() {
        return content;
    }
}
