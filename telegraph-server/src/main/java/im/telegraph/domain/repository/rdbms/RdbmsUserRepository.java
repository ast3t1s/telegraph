package im.telegraph.domain.repository.rdbms;

import im.telegraph.common.rdbms.RdbmsOperations;
import im.telegraph.domain.entity.*;
import im.telegraph.domain.repository.UserRepository;
import im.telegraph.engine.serialization.stream.BinaryInputStream;
import im.telegraph.engine.serialization.stream.BinaryOutputStream;
import io.r2dbc.spi.*;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * @author Ast3t1s
 */
@Repository
public class RdbmsUserRepository extends AbstractRdbmsRepository implements UserRepository {

    private static final String FIELDS = "id, username, display_name, email, password_hash, access_salt, about, role," +
            " enabled, verified, lang_code, verification_key, reset_password_key, reset_password_at, profile_photo," +
            " created_at, deleted_at";

    private static final String INSERT_USER_QUERY = "INSERT INTO users (" + FIELDS +") " +
            "VALUES (:username, :displayName, :email, :password, :accessSalt, :about, :role, :enabled, :verified, " +
            ":langCode, :verificationKey, :resetPasswordKey, :resetPasswordAt, :profilePhoto, :createdAt, :deletedAt)";

    private static final String GET_USER_QUERY =
            "SELECT " + FIELDS + " FROM users WHERE id = :userId";

    private static final String FIND_BY_USERNAME_QUERY =
            "SELECT " + FIELDS + " FROM users WHERE username = :username";

    private static final String FIND_BY_IDS_QUERY =
            "SELECT " + FIELDS + " FROM users WHERE id IN (:ids)";

    private static final String FIND_BY_EMAIL_QUERY =
            "SELECT " + FIELDS + " FROM users WHERE LOWER(email) = LOWER(:email)";

    private static final String GET_EXISTS_BY_EMAIl_QUERY =
            "SELECT EXISTS (SELECT 1 FROM users WHERE email = :email) AS u";

    private static final String GET_EXISTS_BY_USERNAME_QUERY =
            "SELECT EXISTS (SELECT 1 FROM users WHERE username = :username) AS u";

    private static final String FIND_BY_VERIFICATION_KEY_QUERY =
            "SELECT " + FIELDS + " FROM users WHERE verification_key = :key";

    private static final String FIND_BY_RESET_KEY_QUERY =
            "SELECT " + FIELDS + " FROM users WHERE reset_key = :resetKey";

    private static final String UPDATE_USER_QUERY = "UPDATE users SET" +
            " username = :username," +
            " display_name = :displayName," +
            " email = :email," +
            " password_hash = :password," +
            " about = :about," +
            " role = :role," +
            " enabled = :enabled," +
            " verified = :verified," +
            " verification_key = :verificationKey," +
            " reset_password_key = :resetPasswordKey," +
            " reset_password_at = :resetPasswordAt," +
            " profile_photo = :profilePhoto," +
            " deleted_at = :deletedAt" +
            " WHERE id = :id";

    private final RdbmsOperations rdbmsOperations;

    private final BiFunction<Row, RowMetadata, User> userRowMapper = new UserRowMapper();
    private final Function<User, Map<String, Parameter>> userParameterMapper = new UserRowParameterMapper();

    public RdbmsUserRepository(ConnectionFactory connectionFactory) {
        super(connectionFactory);
        this.rdbmsOperations = RdbmsOperations.create(connectionFactory);
    }

    @Override
    public Mono<User> insert(User user) {
        return rdbmsOperations.withHandle(handle -> handle.createUpdate(INSERT_USER_QUERY)
                .bindMap(userParameterMapper.apply(user))
                .execute().then(Mono.just(user)))
                .next();
    }

    @Override
    public Mono<User> update(User user) {
        return rdbmsOperations.withHandle(handle -> handle.createUpdate(UPDATE_USER_QUERY)
                .bindMap(userParameterMapper.apply(user)).execute())
                .then(Mono.just(user));
    }

    @Override
    public Mono<User> findById(Id userId) {
        return rdbmsOperations.withHandle(handle -> handle.createQuery(GET_USER_QUERY)
                .bind("userId", userId.asLong())
                .mapRow(userRowMapper))
                .next();
    }

    @Override
    public Mono<User> findByUsername(String username) {
        return rdbmsOperations.withHandle(handle -> handle
                .createQuery(FIND_BY_USERNAME_QUERY)
                .bind("username", username)
                .mapRow(userRowMapper))
                .next();
    }

    @Override
    public Mono<User> findByEmailIgnoreCase(String email) {
        return rdbmsOperations.withHandle(handle -> handle.createQuery(FIND_BY_EMAIL_QUERY)
                .bind("email", email)
                .mapRow(userRowMapper))
                .next();
    }

    @Override
    public Mono<User> findByVerificationKey(String key) {
        return rdbmsOperations.withHandle(handle -> handle
                .createQuery(FIND_BY_VERIFICATION_KEY_QUERY)
                .bind("key", key)
                .mapRow(userRowMapper))
                .next();
    }

    @Override
    public Mono<User> findByResetKey(String resetKey) {
        return rdbmsOperations.withHandle(handle -> handle.createQuery(FIND_BY_RESET_KEY_QUERY)
                .bind("resetKey", resetKey)
                .mapRow(userRowMapper))
                .next();
    }

    @Override
    public Flux<User> findAllById(Iterable<Id> ids) {
        return Flux.fromIterable(ids)
                .map(Id::asLong)
                .collectList()
                .flatMapMany(converted -> rdbmsOperations.withHandle(handle -> handle.createQuery(FIND_BY_IDS_QUERY)
                        .bind("ids", converted)
                        .mapRow(userRowMapper)));
    }

    @Override
    public Mono<Boolean> existsByUsername(String username) {
        return rdbmsOperations.withHandle(handle -> handle.createQuery(GET_EXISTS_BY_USERNAME_QUERY)
                .bind("username", username)
                .mapRow(row -> row.get("u", Boolean.class)))
                .next();
    }

    @Override
    public Mono<Boolean> existsByEmail(String email) {
        return rdbmsOperations.withHandle(handle -> handle.createQuery(GET_EXISTS_BY_EMAIl_QUERY)
                .bind("email", email)
                .mapRow(row -> row.get("u", Boolean.class)))
                .next();
    }

    @Override
    public Flux<User> findAllBy(int limit, int offset) {
        return rdbmsOperations.withHandle(handle -> handle
                .createQuery(limitQuery(GET_USER_QUERY, limit, offset))
                .mapRow(userRowMapper));
    }

    @Override
    public Flux<User> searchByUsername(String username, int limit) {
        return Flux.empty();
    }

    private static byte[] serialize(ProfilePhoto profilePhoto) {
        BinaryOutputStream outputStream = new BinaryOutputStream();
        outputStream.writeInt64(profilePhoto.getId());
        outputStream.writeInt64(profilePhoto.getPhotoSmall().getFileId());
        outputStream.writeInt64(profilePhoto.getPhotoSmall().getAccessHash());
        outputStream.writeInt64(profilePhoto.getPhotoLarge().getFileId());
        outputStream.writeInt64(profilePhoto.getPhotoLarge().getAccessHash());
        outputStream.writeInt32(profilePhoto.getAddedDate());
        return outputStream.toByteArray();
    }

    private static ProfilePhoto deserialize(byte[] bytes) {
        try {
            BinaryInputStream inputStream = new BinaryInputStream(bytes);
            final var photoId = inputStream.readInt64();
            final var photoSmallFileId = inputStream.readInt64();
            final var photoSmallAccessHash = inputStream.readInt64();
            final var photoLargeFileId = inputStream.readInt64();
            final var photoLargeAccessHash = inputStream.readInt64();
            final var photoAddedDate = inputStream.readInt();

            return new ProfilePhoto(photoId, new FileReferenceId(photoSmallFileId, photoSmallAccessHash),
                    new FileReferenceId(photoLargeFileId, photoLargeAccessHash), photoAddedDate);
        } catch (IOException e) {
            throw new IllegalStateException("Unable to parse profile photo");
        }
    }

    private static class UserRowParameterMapper implements Function<User, Map<String, Parameter>> {

        @Override
        public Map<String, Parameter> apply(User user) {
            final Map<String, Parameter> parameters = new HashMap<>();
            parameters.put("id", Parameters.in(R2dbcType.BIGINT, user.getId().asLong()));
            parameters.put("username", Parameters.in(R2dbcType.VARCHAR, user.getUsername()));
            parameters.put("displayName", Parameters.in(R2dbcType.VARCHAR, user.getDisplayName()));
            parameters.put("email", Parameters.in(R2dbcType.VARCHAR, user.getEmail()));
            parameters.put("password", Parameters.in(R2dbcType.VARCHAR, user.getPassword()));
            parameters.put("accessSalt", Parameters.in(R2dbcType.VARCHAR, user.getAccessSalt()));
            parameters.put("about", Parameters.in(R2dbcType.VARCHAR, user.getAbout()));
            parameters.put("role", Parameters.in(R2dbcType.VARCHAR, user.getRole().getValue()));
            parameters.put("enabled", Parameters.in(R2dbcType.BOOLEAN, user.getEnabled()));
            parameters.put("verified", Parameters.in(R2dbcType.BOOLEAN, user.getVerified()));
            parameters.put("langCode", Parameters.in(R2dbcType.VARCHAR, user.getLanguageCode()));
            parameters.put("verificationKey", Parameters.in(R2dbcType.VARCHAR, user.getVerificationKey()));
            parameters.put("resetPasswordKey", Parameters.in(R2dbcType.VARCHAR, user.getResetKey()));
            parameters.put("resetPasswordAt", Parameters.in(R2dbcType.TIMESTAMP, user.getResetDate()));
            parameters.put("profilePhoto", Parameters.in(R2dbcType.BINARY,
                            user.getProfilePhoto().map(RdbmsUserRepository::serialize).orElse(null)));
            parameters.put("createdAt", Parameters.in(R2dbcType.TIMESTAMP, user.getCreatedAt()));
            parameters.put("deletedAt", Parameters.in(R2dbcType.TIMESTAMP, user.getDeletedAt()));
            return parameters;
        }
    }

    private static class UserRowMapper implements BiFunction<Row, RowMetadata, User> {

        @Override
        public User apply(Row row, RowMetadata metadata) {
            return User.builder()
                    .id(Id.of(row.get("id", Long.class)))
                    .username(row.get("username", String.class))
                    .displayName(row.get("display_name", String.class))
                    .email(row.get("email", String.class))
                    .password(row.get("password_hash", String.class))
                    .accessSalt(row.get("access_salt", String.class))
                    .about(row.get("about", String.class))
                    .role(User.Role.of(row.get("role", String.class)))
                    .enabled(row.get("enabled", Boolean.class))
                    .verified(row.get("verified", Boolean.class))
                    .languageCode(row.get("lang_code", String.class))
                    .verificationKey(row.get("verification_key", String.class))
                    .resetKey(row.get("reset_password_key", String.class))
                    .resetDate(row.get("reset_password_at", LocalDateTime.class).toInstant(ZoneOffset.UTC))
                    .createdAt(row.get("created_at", LocalDateTime.class).toInstant(ZoneOffset.UTC))
                    .profilePhoto(deserialize(row.get("profile_photo", byte[].class)))
                    .deletedAt(row.get("deleted_at", LocalDateTime.class).toInstant(ZoneOffset.UTC))
                    .build();
        }
    }
}
