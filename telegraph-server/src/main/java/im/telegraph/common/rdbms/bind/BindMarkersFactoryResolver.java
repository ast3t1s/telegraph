package im.telegraph.common.rdbms.bind;

import io.r2dbc.spi.ConnectionFactory;
import io.r2dbc.spi.ConnectionFactoryMetadata;
import org.springframework.util.LinkedCaseInsensitiveMap;
import reactor.util.annotation.Nullable;

import java.util.Locale;
import java.util.Map;

/**
 * @author Ast3t1s
 */
public final class BindMarkersFactoryResolver {

    private static final Map<String, BindMarkersFactory> BUILTIN =
            new LinkedCaseInsensitiveMap<>(Locale.ENGLISH);

    static {
        BUILTIN.put("H2", BindMarkersFactory.indexed("$", 1));
        BUILTIN.put("MariaDB", BindMarkersFactory.anonymous("?"));
        BUILTIN.put("Microsoft SQL Server", BindMarkersFactory.named("@", "P", 32,
                BindMarkersFactoryResolver::filterBindMarker));
        BUILTIN.put("MySQL", BindMarkersFactory.anonymous("?"));
        BUILTIN.put("Oracle", BindMarkersFactory.named(":", "P", 32,
                BindMarkersFactoryResolver::filterBindMarker));
        BUILTIN.put("PostgreSQL", BindMarkersFactory.indexed("$", 1));
    }

    @Nullable
    public static BindMarkersFactory resolve(ConnectionFactory connectionFactory) {
        ConnectionFactoryMetadata metadata = connectionFactory.getMetadata();
        BindMarkersFactory r2dbcDialect = BUILTIN.get(metadata.getName());
        if (r2dbcDialect != null) {
            return r2dbcDialect;
        }
        for (String it : BUILTIN.keySet()) {
            if (metadata.getName().contains(it)) {
                return BUILTIN.get(it);
            }
        }
        return null;
    }

    private static String filterBindMarker(CharSequence input) {
        StringBuilder builder = new StringBuilder(input.length());
        for (int i = 0; i < input.length(); i++) {
            char ch = input.charAt(i);
            // ascii letter or digit
            if (Character.isLetterOrDigit(ch) && ch < 127) {
                builder.append(ch);
            }
        }
        if (builder.length() == 0) {
            return "";
        }
        return "_" + builder.toString();
    }
} 
