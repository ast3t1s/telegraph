package im.telegraph.domain.repository;

import im.telegraph.domain.entity.DialogMarker;
import im.telegraph.domain.entity.Id;
import im.telegraph.domain.entity.Peer;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Instant;
import java.util.List;

/**
 * @author Ast3t1s
 */
public interface DialogMarkerRepository {

    /**
     * Saves a history inbox in the repository.
     *
     * @param marker the history inbox entity to save
     * @return A {@link Mono} that completes when the history inbox is successfully saved. If an error occurs during the save,
     * the {@code Mono} emits an error.
     */
    Mono<Void> save(DialogMarker marker);

    /**
     * Finds a history inbox by the user ID and peer.
     *
     * @param userId the ID of the user
     * @param peer the peer associated with the history inbox
     * @return A {@link Mono} that emits the {@link DialogMarker} instance if found, or an empty {@code Mono} if no history inbox
     * is found. If an error occurs during the search, it is emitted through the {@code Mono}.
     */
    Mono<DialogMarker> findByUserIdAndPeer(Id userId, Peer peer);

    /**
     * Finds all history inboxes for a user by the specified list of peers.
     *
     * @param userId the ID of the user
     * @param peers the list of peers to filter by
     * @return A {@link Flux} that emits the {@link DialogMarker} instances matching the criteria.
     * If no history inboxes are found, the {@code Flux} completes empty. If an error occurs during the search, it is emitted
     * through the {@code Flux}.
     */
    Flux<DialogMarker> findAllByUserIdAndPeerIn(Id userId, List<Peer> peers);

    /**
     * Finds all history inboxes byt the given {@code Peer} and timestamp greater then the given
     *
     * @param peer the peer of the box
     * @param timestamp the timestamp of the box
     * @return A {@link Flux} that emits the {@link DialogMarker} instances matching the criteria.
     * If no history inboxes are found, the {@code Flux} completes empty. If an error occurs during the search, it is emitted
     * through the {@code Flux}.
     */
    Flux<DialogMarker> findAllByPeerAndTimestampGreaterThanEqual(Peer peer, Instant timestamp);

    /**
     * Deletes a history inbox by the user ID and peer.
     *
     * @param userId the ID of the user
     * @param peer the peer associated with the history inbox
     * @return A {@link Mono} that completes when the history inbox is successfully deleted. If the history inbox does not exist
     * or an error occurs during deletion, the {@code Mono} emits an error.
     */
    Mono<Void> deleteByUserIdAndPeer(Id userId, Peer peer);
}
