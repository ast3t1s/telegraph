package im.telegraph.protocol.model.mtproto;

import im.telegraph.engine.serialization.DataInputStream;
import im.telegraph.engine.serialization.DataOutputStream;

import java.io.IOException;

public class MTRpcResponse extends ProtoStruct {

    public static final byte HEADER = (byte) 0x04;

    private long messageId;
    private byte[] payload;

    public MTRpcResponse(DataInputStream inputStream) throws IOException {
        super(inputStream);
    }

    public MTRpcResponse(long messageId, byte[] payload) {
        this.messageId = messageId;
        this.payload = payload;
    }

    public long getMessageId() {
        return messageId;
    }

    public byte[] getPayload() {
        return payload;
    }

    @Override
    protected byte getHeader() {
        return HEADER;
    }

    @Override
    protected void writeBody(DataOutputStream outputStream) throws IOException {
        outputStream.writeInt64(messageId);
        outputStream.writeProtoBytes(payload);
    }

    @Override
    protected void readBody(DataInputStream inputStream) throws IOException {
        this.messageId = inputStream.readInt64();
        this.payload = inputStream.readProtoBytes();
    }

    @Override
    public String toString() {
        return "ResponseBox [" + messageId + "]";
    }
}
