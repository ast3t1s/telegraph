package im.telegraph.service;

import im.telegraph.common.*;
import im.telegraph.common.property.domain.UserProperties;
import im.telegraph.common.security.crypto.PasswordEncoder;
import im.telegraph.config.TelegraphProperties;
import im.telegraph.domain.entity.Id;
import im.telegraph.domain.entity.ProfilePhoto;
import im.telegraph.domain.entity.User;
import im.telegraph.domain.entity.photo.Photo;
import im.telegraph.domain.repository.UserRepository;
import im.telegraph.exceptions.RpcErrors;
import im.telegraph.exceptions.RpcException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.annotation.Nullable;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.regex.Pattern;

@Service
public class UserService {

    private static final Logger log = LoggerFactory.getLogger(UserService.class);

    private static final Pattern USERNAME_PATTERN = Pattern.compile("^[0-9a-zA-Z_]{5,32}",
            Pattern.UNICODE_CHARACTER_CLASS);

    private static final Pattern EMAIL_PATTERN = Pattern.compile("^(.+)@(.+)$");

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;
    private final IdGeneratorManager idGenerator;
    private final ACLHashGenerator hashGenerator;
    private final StateHolder stateHolder;
    private final DateTimeFactory dateFactory;
    private final UserProperties userProperties;

    public UserService(UserRepository userRepository,
                       PasswordEncoder passwordEncoder,
                       IdGeneratorManager idGenerator,
                       ACLHashGenerator hashGenerator,
                       StateHolder stateHolder,
                       DateTimeFactory dateFactory,
                       TelegraphProperties properties) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.idGenerator = idGenerator;
        this.hashGenerator = hashGenerator;
        this.stateHolder = stateHolder;
        this.dateFactory = dateFactory;
        this.userProperties = properties.getDomain().getUser();
    }

    /**
     * Create new user.
     *
     * @param nick        the user's nick name.
     * @param email       the user's email.
     * @param password    the user password.
     * @param displayName the user display name.
     * @param role        the role of the user.
     * @return A {@link Mono} where, upon successful completion, emits the {@link User}.If an error is received,
     * it is emitted through the {@code Mono}.
     */
    public Mono<User> create(final String nick, final String email, final String password,
                             @Nullable final String displayName, final String langCode, final User.Role role) {
        if (!StringUtils.hasText(nick) || !isNickValid(nick)) {
            return Mono.error(RpcException.create(RpcErrors.USERNAME_INVALID));
        }
        if (!StringUtils.hasText(email) || !isEmailValid(email)) {
            return Mono.error(RpcException.create(RpcErrors.EMAIL_INVALID));
        }
        if (!checkPasswordLength(password)) {
            return Mono.error(RpcException.create(RpcErrors.PASSWORD_INVALID));
        }
        return userRepository.findByUsername(nick)
                .flatMap(exists -> {
                    log.warn("User with nick: {} already exists", nick);
                    return Mono.error(RpcException.create(RpcErrors.USERNAME_OCCUPIED));
                })
                .then(userRepository.findByEmailIgnoreCase(email))
                .flatMap(exists -> {
                    log.warn("User with email: {} already exists", email);
                    return Mono.error(RpcException.create(RpcErrors.EMAIL_OCCUPIED));
                })
                .then(Mono.fromCallable(() -> {
                    final Instant now = dateFactory.getClock().instant();
                    return User.builder()
                        .id(idGenerator.generateEntityId(User.ENTITY_NAME))
                        .username(nick)
                        .email(email)
                        .role(role)
                        .password(passwordEncoder.encode(password))
                        .accessSalt(hashGenerator.nextAccessSalt())
                        .verificationKey(hashGenerator.random())
                        .verified(false)
                        .enabled(true)
                        .languageCode(langCode)
                        .displayName(displayName)
                        .createdAt(now)
                        .updatedAt(now)
                        .build();
                }))
                .flatMap(userRepository::insert)
                .doOnNext(user -> log.debug("User: {} created", user))
                .flatMap(toCache -> stateHolder.getUsersStore().save(toCache.getId().asLong(), toCache));
    }

    /**
     * Update user's profile.
     *
     * @param userId      the ID of the user.
     * @param displayName the new display name.
     * @return A {@link Mono} where, upon successful completion, emits nothing; indicating the user has been edited.
     * If an error is received, it is emitted through the {@code Mono}.
     */
    public Mono<User> updateProfile(final Id userId, @Nullable final String displayName, @Nullable final String about) {
        if (StringUtils.hasText(about) && about.trim().length() > userProperties.getMaxBioLength()) {
            return Mono.error(() -> RpcException.create(RpcErrors.ABOUT_TOO_LONG));
        }
        return userRepository.findById(userId)
                .map(user -> user.toBuilder()
                        .displayName(displayName)
                        .about(about)
                        .updatedAt(dateFactory.getClock().instant())
                        .build())
                .flatMap(userRepository::update)
                .flatMap(toCache -> stateHolder.getUsersStore().save(toCache.getId().asLong(), toCache));
    }

    /**
     * Update user's nick name.
     *
     * @param userId the ID of the user.
     * @param nick   the user's new nickname.
     * @return A {@link Mono} where, upon successful completion, emits nothing; indicating the user has been edited.
     * If an error is received, it is emitted through the {@code Mono}.
     */
    public Mono<User> editNick(final Id userId, final String nick) {
        if (!StringUtils.hasText(nick) || !isNickValid(nick)) {
            return Mono.error(() -> RpcException.create(RpcErrors.USERNAME_INVALID));
        }

        return userRepository.findByUsername(nick)
                .flatMap(exists -> Mono.error(() -> RpcException.create(RpcErrors.USERNAME_OCCUPIED)))
                .then(userRepository.findById(userId)
                        .switchIfEmpty(Mono.error(() -> RpcException.create(RpcErrors.USER_NOT_FOUND))))
                .map(entity -> entity.toBuilder()
                        .username(nick)
                        .updatedAt(dateFactory.getClock().instant())
                        .build())
                .flatMap(userRepository::update)
                .flatMap(toCache -> stateHolder.getUsersStore().save(toCache.getId().asLong(), toCache));
    }

    /**
     * Update user's email.
     *
     * @param userId the ID of the user.
     * @param email  the user's new email.
     * @return A {@link Mono} where, upon successful completion, emits nothing; indicating the user has been edited.
     * If an error is received, it is emitted through the {@code Mono}.
     */
    public Mono<Void> editEmail(final Id userId, final String email) {
        if (!StringUtils.hasText(email) || !isEmailValid(email)) {
            return Mono.error(() -> RpcException.create(RpcErrors.EMAIL_INVALID));
        }
        return userRepository.findByEmailIgnoreCase(email)
                .flatMap(exists -> Mono.error(() -> RpcException.create(RpcErrors.EMAIL_OCCUPIED)))
                .then(userRepository.findById(userId))
                .switchIfEmpty(Mono.error(RpcException.create(RpcErrors.USER_NOT_FOUND)))
                .flatMap(user -> {
                    if (!user.getVerified()) {
                        log.warn("User's '{}' email is not verified to edit", user);
                        return Mono.error(() -> RpcException.create(RpcErrors.EMAIL_NOT_VERIFIED));
                    }
                    final User toUpdateUser = user.toBuilder()
                            .email(email)
                            .verified(false)
                            .updatedAt(dateFactory.getClock().instant())
                            .build();

                    return userRepository.update(toUpdateUser);
                })
                .flatMap(updated -> stateHolder.getUsersStore().save(updated.getId().asLong(), updated))
                .then();
    }

    /**
     * Update user's avatar.
     *
     * @param userId the ID of the user.
     * @param photo the user's avatar.
     * @return A {@link Mono} where, upon successful completion, emits nothing; indicating the user has been edited.
     * If an error is received, it is emitted through the {@code Mono}.
     */
    public Mono<User> editPhoto(final Id userId, @Nullable final Photo photo) {
        return userRepository.findById(userId)
                .switchIfEmpty(Mono.error(RpcException.create(RpcErrors.USER_NOT_FOUND)))
                .flatMap(user -> {
                    final ProfilePhoto profilePhoto = Optional.ofNullable(photo)
                            .map(entity -> new ProfilePhoto(photo.getId().asLong(),
                                    photo.getPhotoSmall().getFileReferenceId(),
                                    photo.getPhotoLarge().getFileReferenceId(),
                                    (int) photo.getCreatedAt().getEpochSecond()))
                            .orElse(null);

                    final User toUpdateUser = user.toBuilder()
                            .profilePhoto(profilePhoto)
                            .updatedAt(dateFactory.getClock().instant())
                            .build();

                    return userRepository.update(toUpdateUser)
                            .flatMap(toCache -> stateHolder.getUsersStore().save(toCache.getId().asLong(), toCache));
                });
    }

    /**
     * Update user's role.
     *
     * @param userId the ID of the user.
     * @param role the user's role.
     * @return A {@link Mono} where, upon successful completion, emits nothing; indicating the user has been edited.
     * If an error is received, it is emitted through the {@code Mono}.
     */
    public Mono<Void> editRole(final Id userId, final User.Role role) {
        if (role == null || role == User.Role.UNKNOWN) {
            return Mono.empty();
        }
        return userRepository.findById(userId)
                .map(user -> user.toBuilder()
                        .role(role)
                        .updatedAt(dateFactory.getClock().instant())
                        .build())
                .flatMap(userRepository::update)
                .flatMap(toCache -> stateHolder.getUsersStore().save(userId.asLong(), toCache))
                .then();
    }

    /**
     * Update user's password.
     *
     * @param userId      Identifier of the user.
     * @param oldPassword user's old password.
     * @param newPassword user's new password.
     * @return A {@link Mono} where, upon successful completion, emits the {@link User}.If an error is received,
     * it is emitted through the {@code Mono}.
     */
    public Mono<Void> changePassword(final Id userId, final String oldPassword, final String newPassword) {
        return userRepository.findById(userId)
                .map(user -> {
                    final String currentEncryptedPassword = user.getPassword();
                    if (!passwordEncoder.matches(oldPassword, currentEncryptedPassword)) {
                        throw RpcException.create(RpcErrors.PASSWORD_INVALID);
                    }

                    if (!checkPasswordLength(newPassword)) {
                        throw RpcException.create(RpcErrors.PASSWORD_INVALID);
                    }

                    return user.toBuilder()
                            .password(passwordEncoder.encode(newPassword))
                            .updatedAt(dateFactory.getClock().instant())
                            .build();
                })
                .flatMap(userRepository::update)
                .flatMap(toCache -> stateHolder.getUsersStore().save(userId.asLong(), toCache))
                .doOnNext(updated -> log.trace("User: {} password updated", updated))
                .then();
    }

    /**
     * Disable of enable an user.
     *
     * @param userId the ID of the user.
     * @param enabled the flag if user enabled or not.
     * @return A {@link Mono} where, upon successful completion, emits nothing; indicating the user has been enabled
     * or disabled If an error is received, it is emitted through the {@code Mono}.
     */
    public Mono<Void> setEnabled(final Id userId, final Boolean enabled) {
        return userRepository.findById(userId)
                .map(user -> user.toBuilder()
                        .enabled(enabled)
                        .updatedAt(dateFactory.getClock().instant())
                        .build())
                .flatMap(userRepository::update)
                .flatMap(toCache -> stateHolder.getUsersStore().save(userId.asLong(), toCache))
                .then();
    }

    /**
     * Verify user's email.
     *
     * @param userId the ID of the user.
     * @param key    the email key.
     * @return A {@link Mono} where, upon successful completion, emits nothing; indicating the user has been verify
     * email. If an error is received, it is emitted through the {@code Mono}.
     */
    public Mono<Void> verifyEmail(final Id userId, final String key) {
        return userRepository.findByVerificationKey(key)
                .filter(ignored -> Objects.equals(userId, ignored.getId()))
                .switchIfEmpty(Mono.error(RpcException.create(RpcErrors.USER_NOT_FOUND)))
                .map(user -> user.toBuilder()
                        .verified(true)
                        .verificationKey(null)
                        .updatedAt(dateFactory.getClock().instant())
                        .build())
                .flatMap(userRepository::update)
                .flatMap(toCache -> stateHolder.getUsersStore().save(userId.asLong(), toCache))
                .then();
    }

    /**
     * Initialize password reset.
     *
     * @param email the email of the user.
     * @return A {@link Mono} where, upon successful completion, emits nothing; indicating the user has been start
     * password reset. If an error is received, it is emitted through the {@code Mono}.
     */
    public Mono<Void> startPasswordReset(final String email) {
        return userRepository.findByEmailIgnoreCase(email)
                .filter(User::getVerified)
                .switchIfEmpty(Mono.error(RpcException.create(RpcErrors.EMAIL_NOT_VERIFIED)))
                .map(user -> user.toBuilder()
                        .resetKey(hashGenerator.random())
                        .updatedAt(dateFactory.getClock().instant())
                        .build())
                .flatMap(userRepository::update)
                .flatMap(updated -> stateHolder.getUsersStore().save(updated.getId().asLong(), updated))
                .doOnNext(entity -> log.debug("Password reset starting for: {}", email))
                .then();
    }

    /**
     * Update user's nick name.
     *
     * @param key         the ID of the user.
     * @param newPassword the user's new nickname.
     * @return A {@link Mono} where, upon successful completion, emits the {@link User}.If an error is received,
     * it is emitted through the {@code Mono}.
     */
    public Mono<Void> completePasswordReset(final String key, final String newPassword) {
        return userRepository.findByResetKey(key)
                .filter(user -> Objects.nonNull(user.getResetDate()))
                .filter(user -> user.getResetDate().isAfter(Instant.now().minus(1, ChronoUnit.DAYS)))
                .map(user -> user.toBuilder()
                        .password(passwordEncoder.encode(newPassword))
                        .resetKey(null)
                        .resetDate(null)
                        .updatedAt(dateFactory.getClock().instant())
                        .build())
                .flatMap(userRepository::update)
                .then();
    }

    /**
     * Authenticate user by login and password.
     *
     * @param login    the login.
     * @param password the password.
     * @return A {@link Mono} where, upon successful completion, emits the {@link User}.If an error is received,
     * it is emitted through the {@code Mono}.
     */
    public Mono<User> authenticate(final String login, final String password) {
        if (!checkPasswordLength(password)) {
            return Mono.error(() -> RpcException.create(RpcErrors.PASSWORD_INVALID));
        }

        final Supplier<Mono<User>> authenticate = () -> isEmailValid(login)
                ? userRepository.findByEmailIgnoreCase(login)
                : userRepository.findByUsername(login);

        return Mono.defer(authenticate).filter(user -> passwordEncoder.matches(password, user.getPassword()))
                .switchIfEmpty(Mono.error(() -> RpcException.create(RpcErrors.PASSWORD_INVALID)))
                .flatMap(user -> {
                    if (!user.getEnabled()) {
                        return Mono.error(() -> RpcException.create(RpcErrors.USER_DEACTIVATED));
                    }
                    if (user.getDeletedAt().isPresent()) {
                        return Mono.error(() -> RpcException.create(RpcErrors.USER_DEACTIVATED));
                    }
                    return Mono.just(user);
                })
                .doOnNext(user -> log.debug("User: {} authenticated.", user));
    }

    /**
     * Attempt to find user by id.
     *
     * @param userId the ID of the user.
     * @return A {@link Mono} where, upon successful completion, emits the {@link User}.If an error is received,
     * it is emitted through the {@code Mono}.
     */
    public Mono<User> findById(final Id userId) {
        return stateHolder.getUsersStore().find(userId.asLong())
                .switchIfEmpty(userRepository.findById(userId)
                        .flatMap(toCache -> stateHolder.getUsersStore().save(userId.asLong(), toCache)));
    }

    /**
     * Search users by username.
     *
     * @param request the username.
     * @param limit   the limit of records.
     * @return A {@link Flux} where, upon successful completion, emits the {@link User}.If an error is received,
     * it is emitted through the {@code Mono}.
     */
    public Flux<User> searchUsers(final String request, final int limit) {
        return userRepository.searchByUsername(request, limit);
    }

    /**
     * Update user's nick name.
     *
     * @param ids the set of user ids.
     * @return A {@link Mono} where, upon successful completion, emits the {@link User}.If an error is received,
     * it is emitted through the {@code Mono}.
     */
    public Flux<User> getUsers(final Iterable<Id> ids) {
        return Flux.fromIterable(ids)
                .distinct()
                .buffer(100)
                .flatMap(userRepository::findAllById);
    }

    /**
     * Retrieves the {@link Page} of the {@link User} users.
     *
     * @param page   the page limit, offset.
     * @return A {@link Mono} that continually emits the {@link Page<  User  > participants} of the user.
     * If an error is received, it is emitted through the {@code Flux}.
     */
    public Mono<Page<User>> getUsers(final OffsetPageRequest page) {
        /*return this.userRepository.findAllBy(page)
                .collectList()
                .zipWith(this.userRepository.count())
                .map(TupleUtils.function((content, count) -> new PageImpl<>(content, page, count)));*/
        return Mono.empty();
    }

    /**
     * Delete the given user.
     *
     * @param userId Identifier of the user.
     * @return A {@link Mono} where, upon successful completion, emits the {@link User}.If an error is received,
     * it is emitted through the {@code Mono}.
     */
    public Mono<Void> deleteUser(final Id userId) {
        return userRepository.findById(userId)
                .map(user -> user.toBuilder()
                        .enabled(false)
                        .deletedAt(dateFactory.getClock().instant())
                        .build())
                .flatMap(userRepository::update)
                .then();
    }

    /**
     * Check user access hash.
     *
     * @param userId     Identifier of the user.
     * @param authId     Identifier of the device.
     * @param accessHash User access hash to check.
     * @return A {@link Mono} where, upon successful completion, emits the {@link User}.If an error is received,
     * it is emitted through the {@code Mono}.
     */
    public Mono<Boolean> checkAccessHash(final Id userId, final long authId, final long accessHash) {
        return findCachedUser(userId)
                .map(user -> hashGenerator.generateUserAccessHash(authId, userId.asLong(), user.getAccessSalt()) == accessHash)
                .defaultIfEmpty(false);
    }

    protected Mono<User> findCachedUser(final Id userId) {
        return stateHolder.getUsersStore().find(userId.asLong())
                .switchIfEmpty(userRepository.findById(userId)
                        .flatMap(user -> stateHolder.getUsersStore().save(userId.asLong(), user)));
    }

    /**
     * Check if the given username is already used.
     *
     * @param nick the username to check.
     * @return a {@link Mono} that, upon subscription, returns whether the username is busy or not.
     */
    public Mono<Boolean> checkUsernameExists(final String nick) {
        return userRepository.existsByUsername(nick);
    }

    /**
     * Check if the given email is already used.
     *
     * @param email the email to check.
     * @return a {@link Mono} that, upon subscription, returns whether the email is busy or not.
     */
    public Mono<Boolean> checkEmailExists(final String email) {
        return userRepository.existsByEmail(email);
    }

    private static boolean isNickValid(final String nick) {
        return USERNAME_PATTERN.matcher(nick).matches();
    }

    private static boolean isEmailValid(final String email) {
        return EMAIL_PATTERN.matcher(email).matches();
    }

    private boolean checkPasswordLength(String password) {
        return StringUtils.hasText(password)
                || password.length() < userProperties.getMinPasswordLength()
                || password.length() > userProperties.getMaxPasswordLength();
    }

}
