package im.telegraph.common.property.server;

import im.telegraph.common.property.TlsProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.boot.convert.DataSizeUnit;
import org.springframework.boot.convert.DurationUnit;
import org.springframework.util.unit.DataSize;
import org.springframework.util.unit.DataUnit;

import java.net.InetAddress;
import java.time.Duration;
import java.time.temporal.ChronoUnit;

/**
 * @author Ast3t1s
 */
public abstract class BaseServerProperties {

    /**
     * Flag whether or not server is enabled
     */
    private boolean enabled;

    /**
     * Max frame size.
     */
    @DataSizeUnit(value = DataUnit.MEGABYTES)
    private DataSize maxFrameSize = DataSize.ofMegabytes(2);

    /**
     * Bind server address.
     */
    private InetAddress bindAddress;

    /**
     * Port for server.
     */
    private int port;

    /**
     * Server connection timeout.
     */
    @DurationUnit(ChronoUnit.SECONDS)
    private Duration connectionTimeout = Duration.ofSeconds(30);

    /**
     * SSl config
     */
    @NestedConfigurationProperty
    private TlsProperties ssl = new TlsProperties();

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public DataSize getMaxFrameSize() {
        return maxFrameSize;
    }

    public void setMaxFrameSize(DataSize maxFrameSize) {
        this.maxFrameSize = maxFrameSize;
    }

    public InetAddress getBindAddress() {
        return bindAddress;
    }

    public void setBindAddress(InetAddress bindAddress) {
        this.bindAddress = bindAddress;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public Duration getConnectionTimeout() {
        return connectionTimeout;
    }

    public void setConnectionTimeout(Duration connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }

    public TlsProperties getSsl() {
        return ssl;
    }

    public void setSsl(TlsProperties ssl) {
        this.ssl = ssl;
    }
} 
