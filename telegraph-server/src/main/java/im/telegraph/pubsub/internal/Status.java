package im.telegraph.pubsub.internal;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import im.telegraph.internal.remote.Address;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Ast3t1s
 */
public class Status extends PubSubPayload<Status.Data> {

    public static final int OP = 1;

    public Status(Map<Address, Long> versions, boolean replyToStatus) {
        this(OP, new Data(convert(versions), replyToStatus));
    }

    public Status(int op, Data data) {
        super(op, data);
    }

    private static List<Data.Inner> convert(Map<Address, Long> versions) {
        return versions.entrySet().stream()
                .map(entry -> new Data.Inner(entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());
    }

    public static class Data {

        private final List<Inner> versions;
        private final boolean replyToStatus;

        public Data(List<Inner> versions, boolean replyToStatus) {
            this.versions = versions;
            this.replyToStatus = replyToStatus;
        }

        public List<Inner> getVersions() {
            return versions;
        }

        public boolean isReplyToStatus() {
            return replyToStatus;
        }

        @Override
        public String toString() {
            return "Data{" +
                    "versions=" + versions +
                    ", replyToStatus=" + replyToStatus +
                    '}';
        }

        public static class Inner {

            private final Address address;
            private final long timestamp;

            public Inner(Address address, long timestamp) {
                this.address = address;
                this.timestamp = timestamp;
            }

            public Address getAddress() {
                return address;
            }

            public long getTimestamp() {
                return timestamp;
            }

            @Override
            public String toString() {
                return "Inner{" +
                        "address=" + address +
                        ", timestamp=" + timestamp +
                        '}';
            }
        }
    }
} 
