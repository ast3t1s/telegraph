package im.telegraph.pubsub;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

/**
 * @author Ast3t1s
 */
public class ScheduleEventBust {

    private static final Logger log = LoggerFactory.getLogger(ScheduleEventBust.class);

    private final PubSubMediator eventBus;

    private Disposable firstTopic;
    private Disposable secondTopic;

    public ScheduleEventBust(PubSubMediator eventBus) {
        this.eventBus = eventBus;
    }

    public void start() {
        Scheduler scheduler = Schedulers.newSingle("eventbus", true);
        this.firstTopic = Flux.from(eventBus.receive("test-1", String.class))
                .subscribeOn(scheduler)
                .doOnNext(res -> log.info("Received event bust topic: " + "test-1 message: "  + res))
                .subscribe();

        this.secondTopic = Flux.from(eventBus.receive("test-2", String.class))
                .subscribeOn(scheduler)
                .doOnNext(res ->  log.info("Received event bust topic: " + "test-2 message: "  + res))
                .subscribe();
    }

    public void stop() {
        if (firstTopic != null) {
            firstTopic.dispose();
        }
        if (secondTopic != null) {
            secondTopic.dispose();
        }
    }
}
