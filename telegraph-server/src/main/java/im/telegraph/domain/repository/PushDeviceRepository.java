package im.telegraph.domain.repository;

import im.telegraph.domain.entity.PushDevice;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

/**
 * @author Ast3t1s
 */
public interface PushDeviceRepository {

    /**
     * Creates a new push device entry.
     *
     * @param pushDevice the push device to create
     * @return A {@link Mono} that emits the created {@link PushDevice} upon successful completion. If an error occurs,
     * it is emitted through the {@code Mono}.
     */
    Mono<PushDevice> create(PushDevice pushDevice);

    /**
     * Finds a push device by its token.
     *
     * @param token the token of the push device to find
     * @return A {@link Mono} that emits the {@link PushDevice} if found. If an error occurs or if no push device is found,
     * it is emitted through the {@code Mono}.
     */
    Mono<PushDevice> findByToken(String token);

    /**
     * Finds all push devices associated with the given auth key.
     *
     * @param authKey the auth key to search for
     * @return A {@link Flux} that emits the {@link PushDevice} instances associated with the given auth key. If an error occurs,
     * it is emitted through the {@code Flux}.
     */
    Flux<PushDevice> findAllByAuthKey(Long authKey);

    /**
     * Finds all push devices associated with the given list of auth keys.
     *
     * @param authIds the list of auth keys to search for
     * @return A {@link Flux} that emits the {@link PushDevice} instances associated with the given auth keys. If an error occurs,
     * it is emitted through the {@code Flux}.
     */
    Flux<PushDevice> findAllByAuthKeyIn(List<Long> authIds);

    /**
     * Deletes the given push device.
     *
     * @param pushDevice the push device to delete
     * @return A {@link Mono} that completes when the deletion is done. If an error occurs,
     * it is emitted through the {@code Mono}.
     */
    Mono<Void> delete(PushDevice pushDevice);

}
