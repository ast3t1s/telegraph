package im.telegraph.config;

import im.telegraph.common.SSLFactory;
import im.telegraph.common.property.server.ServerProperties;
import im.telegraph.common.property.server.TcpServerProperties;
import im.telegraph.common.property.server.WebsocketServerProperties;
import im.telegraph.server.ConnectionAcceptor;
import im.telegraph.server.Server;
import im.telegraph.server.reactor.NettyTcpServer;
import im.telegraph.server.reactor.NettyWebsocketServer;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.SmartLifecycle;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Collections;
import java.util.List;

/**
 * @author Ast3t1s
 */
@Configuration
@EnableConfigurationProperties({ServerProperties.class})
public class ServerConfiguration {

    @Bean
    public ServerBootstrap serverBootstrap(ObjectProvider<List<Server>> severs) {
        return new ServerBootstrap(severs.getIfAvailable(Collections::emptyList));
    }

    @Configuration(proxyBeanMethods = false)
    public static class ReactorServerConfiguration {

        private final ServerProperties properties;

        public ReactorServerConfiguration(ServerProperties properties) {
            this.properties = properties;
        }

        @Bean
        @ConditionalOnProperty(prefix = "telegraph.server.tcp", name = "enabled")
        public Server tcpServerTransport(ConnectionAcceptor handler) {
            TcpServerProperties properties = this.properties.getTcp();
            NettyTcpServer.Builder builder = NettyTcpServer.builder();
            builder.host(properties.getBindAddress());
            builder.port(properties.getPort());
            builder.handler(handler);
            builder.lifecycleTimeout(properties.getConnectionTimeout());
            if (properties.getSsl() != null && properties.getSsl().isEnabled()) {
                builder.ssl(SSLFactory.buildSslServerContext(properties.getSsl()));
            }
            return builder.build();
        }

        @Bean
        @ConditionalOnProperty(prefix = "telegraph.server.websocket", name = "enabled")
        public Server websocketTransport(ConnectionAcceptor handler) {
            WebsocketServerProperties properties = this.properties.getWebsocket();
            NettyWebsocketServer.Builder builder = NettyWebsocketServer.builder();
            builder.host(properties.getBindAddress());
            builder.port(properties.getPort());
            builder.handler(handler);
            builder.lifecycleTimeout(properties.getConnectionTimeout());
            properties.getHeaders().forEach(builder::httpHeader);
            if (properties.getSsl() != null && properties.getSsl().isEnabled()) {
                builder.ssl(SSLFactory.buildSslServerContext(properties.getSsl()));
            }
            return builder.build();
        }
    }

    public static class ServerBootstrap implements SmartLifecycle {

        private volatile boolean running;

        private final List<Server> servers;

        public ServerBootstrap(List<Server> servers) {
            this.servers = servers;
        }

        @Override
        public void start() {
            for (Server server : servers) {
                server.start();
            }
            this.running = true;
        }

        @Override
        public void stop() {
            for (Server server : servers) {
                server.stop();
            }
            this.running = false;
        }

        @Override
        public boolean isRunning() {
            return running;
        }
    }
} 
