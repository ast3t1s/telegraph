package im.telegraph.cluster.event;

import im.telegraph.cluster.ClusterEvent;

/**
 * @author Ast3t1s
 */
public class ClusterShuttingDown implements ClusterEvent {

    public static final ClusterEvent INSTANCE = new ClusterShuttingDown();

    private ClusterShuttingDown() {}

    @Override
    public String toString() {
        return "ClusterShuttingDown";
    }
} 
