package im.telegraph.pubsub;

import org.springframework.lang.Nullable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * An interface that provided  Pub/Sub for publish and subscribe methods.
 * @author Ast3t1s
 */
public interface PubSubMediator {

    /**
     * Broadcast message on the given topic to all subscribers of the topic, in distributed version
     * across the whole cluster.
     *
     * @param topic the topic to broadcast to, ie "user:123"
     * @param payload the payload of the broadcast
     * @return a {@link Mono} that can be used to signal a response.
     */
    Mono<Void> publish(String topic, Object payload);

    /**
     * Send message on the given topic to the given destination, in distributed version destination is the identifier
     * of the cluster node which message will be send to. If destination is {@code null} it will be chose automatically
     * by round-robin algorithm.
     *
     * @param topic the topic to broadcast to, ie "user:123"
     * @param destination the target destination (in distributed version identifier of the node).
     * @param payload the payload of the broadcast
     * @return a {@link Mono} that can be used to signal a response.
     */
    Mono<Void> send(String topic, @Nullable String destination, Object payload);

    /**
     * Send message on the given topic to all subscribers of the topic, in distributed version across the whole cluster.
     * If flag {@code allButSelf} is {@code true} the message will be send to all nodes exception the current one.
     *
     * @param topic the topic to broadcast to, ie "user:123"
     * @param payload the payload of the broadcast
     * @param allButSelf the flag whether or not send message to local node.
     * @return a {@link Mono} that can be used to signal a response.
     */
    Mono<Void> sendToAll(String topic, Object payload, boolean allButSelf);

    /**
     * Subscribe to the caller.
     *
     * @param topic the topic to subscribe to, for example "user:123"
     * @param type the type to which the payload of the Pub/Sub message should be converted
     * @param <T> the type of the payload
     * @return a {@link Flux} that emits Pub/Sub messages converted to the given type
     */
    <T> Flux<T> receive(String topic, Class<T> type);
}
