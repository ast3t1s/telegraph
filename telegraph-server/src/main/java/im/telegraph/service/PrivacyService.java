package im.telegraph.service;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import im.telegraph.common.DateTimeFactory;
import im.telegraph.common.Page;
import im.telegraph.domain.entity.Id;
import im.telegraph.domain.entity.Privacy;
import im.telegraph.domain.repository.PrivacyRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.function.TupleUtils;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuples;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.function.BiFunction;

@Service
public class PrivacyService {

    private static final Logger log = LoggerFactory.getLogger(PrivacyService.class);

    private final PrivacyRepository privacyRepository;
    private final DateTimeFactory dateFactory;

    private final Cache<Tuple2<Id, Id>, Boolean> blockedCached;

    public PrivacyService(PrivacyRepository privacyRepository, DateTimeFactory dateFactory) {
        this.privacyRepository = privacyRepository;
        this.dateFactory = dateFactory;

        this.blockedCached = Caffeine.newBuilder()
                .expireAfterWrite(60, TimeUnit.SECONDS)
                .build();
    }

    /**
     * Add user to privacy list (block). Not allowing user to send or received message from blocked user.
     *
     * @param userId the ID of the user who block.
     * @param blockId the ID of the user who has been blocked.
     * @return A {@link Mono} where, upon successful completion, emits nothing; indicating the user has been blocked.
     * If an error is received, it is emitted through the {@code Mono}.
     */
    public Mono<Boolean> blockUser(final Id userId, final Id blockId) {
        return privacyRepository.findByUserIdAndBlockedId(userId, blockId)
                .flatMap(exists -> Mono.just(Boolean.FALSE))
                .switchIfEmpty(Mono.defer(() -> Mono.fromCallable(() -> Privacy.of(userId, blockId, dateFactory.getClock().instant())))
                        .flatMap(privacyRepository::insert)
                        .doOnSuccess(__ -> blockedCached.put(Tuples.of(userId, blockId), true))
                        .thenReturn(true));
    }

    /**
     * Check if user blocked by another user.
     *
     * @param userId the id of the user who check the block.
     * @param blockedId the if of the user whom blocked or not.
     * @return A {@link Mono} where, upon successful completion, emits nothing; indicating the user has been unblocked.
     * If an error is received, it is emitted through the {@code Mono}.
     */
    public Mono<Boolean> isUserBlocked(final Id userId, final Id blockedId) {
        return Mono.justOrEmpty(Optional.ofNullable(blockedCached.getIfPresent(Tuples.of(userId, blockedId))))
                .switchIfEmpty(Mono.defer(() -> privacyRepository.findByUserIdAndBlockedId(userId, blockedId))
                .hasElement().doOnNext(blocked -> blockedCached.put(Tuples.of(userId, blockedId), blocked)));
    }

    /**
     * Remove user from privacy list (unblock). Allowing user to send or received message from user.
     *
     * @param userId the ID of the user who unblock.
     * @param blockedId the ID of the user who has been unblocked.
     * @return A {@link Mono} where, upon successful completion, emits nothing; indicating the user has been unblocked.
     * If an error is received, it is emitted through the {@code Mono}.
     */
    public Mono<Boolean> unblockUser(final Id userId, final Id blockedId) {
        return privacyRepository.deleteByUserIdAndBlockedId(userId, blockedId)
                .doOnNext(deleted -> {
                    if (deleted) {
                        blockedCached.invalidate(Tuples.of(userId, blockedId));
                    }
                })
                .defaultIfEmpty(false);
    }

    /**
     * Load page of blocked users.
     *
     * @param userId the id of the user.
     * @return A {@link Flux} where, upon successful completion, emits nothing; indicating the user has been unblocked.
     * If an error is received, it is emitted through the {@code Flux}.
     */
    public Mono<Page<Privacy>> getBlockList(final Id userId, final int limit, final int offset) {
        return privacyRepository.findAllByUserId(userId, limit, offset)
                .doOnNext(privacy -> blockedCached.put(Tuples.of(privacy.getUserId(), privacy.getBlockedId()), true))
                .collectList()
                .zipWith(privacyRepository.countByUserId(userId))
                .map(TupleUtils.function((BiFunction<List<Privacy>, Long, Page<Privacy>>) Page::new));
    }
} 
