package im.telegraph.admin.route;

import com.fasterxml.jackson.databind.ObjectMapper;
import im.telegraph.admin.object.response.ErrorResponse;
import im.telegraph.admin.object.UsernamePasswordCredentials;
import im.telegraph.common.JacksonResources;
import im.telegraph.exceptions.RpcException;
import io.netty.handler.codec.http.DefaultHttpHeaders;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.ssl.SslContext;
import org.reactivestreams.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.lang.Nullable;
import org.springframework.util.StringUtils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.netty.DisposableServer;
import reactor.netty.channel.MicrometerChannelMetricsRecorder;
import reactor.netty.http.server.HttpServer;
import reactor.netty.http.server.HttpServerRequest;
import reactor.netty.http.server.HttpServerResponse;
import reactor.netty.http.server.HttpServerRoutes;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import static io.netty.channel.ChannelOption.*;
import static io.netty.handler.codec.http.HttpResponseStatus.*;

/**
 * @author Ast3t1s
 */
public class AdminApiServer {

    private static final Logger log = LoggerFactory.getLogger(AdminApiServer.class);

    private final HttpServer httpServer;
    private final List<Routes> routes;

    private final RestAuthHandler authHandler;
    private final AdminApiServerHandler handler;

    private final ObjectMapper objectMapper;

    @Nullable
    private volatile DisposableServer disposableServer;

    public AdminApiServer(HttpServer httpServer, List<Routes> routes, RestAuthHandler authHandler) {
        this.httpServer = Objects.requireNonNull(httpServer, "httpServer");
        this.routes = Objects.requireNonNull(routes, "routes");
        this.handler = new AdminApiServerHandler();
        this.authHandler = authHandler;
        this.objectMapper = new JacksonResources().getObjectMapper();
    }

    /**
     * Returns a new builder for {@link AdminApiServer}.
     *
     * @return a new builder
     */
    public static Builder builder() {
        return new Builder();
    }

    private Route.Action handleRoute(HttpServerRequest request) {
        return retrieveMatchingAction(request, routes.stream().flatMap(handler -> handler.routes().stream())
                .collect(Collectors.toList()));
    }

    private Route.Action retrieveMatchingAction(HttpServerRequest request, List<Route> routes) {
        return routes.stream()
                .filter(route -> route.matches(request))
                .map(Route::getAction)
                .findFirst()
                .orElse((req, res) -> res.status(NOT_FOUND));
    }

    protected DisposableServer startHttpServer() {
        Function<HttpServer, HttpServer> extendServer = server -> server.wiretap(true)
                .handle(handler);
        HttpServer server = extendServer.apply(this.httpServer);
        return server.bindNow(Duration.ofSeconds(30));
    }

    /**
     * And alias for a Reactor Netty server route.
     */
    private class AdminApiServerHandler
            implements BiFunction<HttpServerRequest, HttpServerResponse, Publisher<Void>> {

        public static final String AUTHORIZATION = "Authorization";

        public static final String BASIC = "Basic ";

        private Charset credentialsCharset = StandardCharsets.UTF_8;

        @Override
        public Publisher<Void> apply(HttpServerRequest request, HttpServerResponse response) {
            HttpHeaders httpHeaders = request.requestHeaders();
            String authorization = httpHeaders.get(AUTHORIZATION);
            if (!StringUtils.startsWithIgnoreCase(authorization, "basic ")) {
                return response.status(UNAUTHORIZED).send();
            }
            String credentials = (authorization.length() <= BASIC.length()) ? "" : authorization.substring(BASIC.length());
            String decodes = new String(base64Decode(credentials), this.credentialsCharset);
            String[] parts = decodes.split(":", 2);
            if (parts.length != 2) {
                return response.status(UNAUTHORIZED).send();
            }
            UsernamePasswordCredentials usernamePasswordCredentials = new UsernamePasswordCredentials(parts[0], parts[1]);
            return Mono.defer(() -> authHandler.verifyCredentials(usernamePasswordCredentials)
                    .flatMap(verified -> verified ? Flux.from(handleRoute(request)
                            .handleRequest(request, response))
                            .onErrorResume(ex -> handleException(request, response, ex))
                            .then() : response.status(UNAUTHORIZED).send()));
        }

        private Mono<Void> handleException(HttpServerRequest request, HttpServerResponse response, Throwable cause) {
            ErrorResponse.Builder errorBuilder = ErrorResponse.builder();
            errorBuilder.timestamp(Instant.now());
            errorBuilder.path(request.path());
            if (cause instanceof RpcException) {
                RpcException rpcException = (RpcException) cause;
                if (log.isTraceEnabled()) {
                    log.trace("Handle RPC exception by admin server", rpcException);
                }
                errorBuilder.httpStatus(HttpResponseStatus.BAD_REQUEST);
                errorBuilder.message(rpcException.getErrorMessage());
            }
            errorBuilder.httpStatus(HttpResponseStatus.INTERNAL_SERVER_ERROR);
            errorBuilder.message("Internal Server Error");

            return response.status(BAD_REQUEST).addHeader("content-type", "application/json")
                    .sendString(Mono.fromCallable(() -> objectMapper.writeValueAsString(errorBuilder.build()))).then();
        }

        private byte[] base64Decode(String value) {
            try {
                return Base64.getDecoder().decode(value);
            }
            catch (Exception ex) {
                return new byte[0];
            }
        }
    }

    public void start() {
        if (this.disposableServer == null) {
            disposableServer = startHttpServer();
            log.info("******************************************************************");
            log.info("Admin Server started at {}:{}", disposableServer.host(), disposableServer.port());
            log.info("******************************************************************");
        }
    }

    public void stop() {
        if (disposableServer != null) {
            disposableServer.dispose();
            disposableServer = null;
        }
    }

    public static class Builder {

        private InetAddress address;
        private int port = 9090;
        private SslContext sslContext;
        private final HttpHeaders headers = new DefaultHttpHeaders();
        private final List<Routes> apiRoutes = new ArrayList<>();
        private Consumer<HttpServerRoutes> routesCustomizer;
        private RestAuthHandler authHandler;

        public Builder host(InetAddress address) {
            this.address = address;
            return this;
        }

        public Builder port(int port) {
            if (port <= 0 || port > 65535) {
                throw new IllegalArgumentException("Invalid port: " + port);
            }
            this.port = port;
            return this;
        }

        /**
         * Sets the SSL configuration that will be applied to the server's default connector.
         *
         * @param sslContext the SSL configuration
         * @return this builder
         */
        public Builder ssl(SslContext sslContext) {
            this.sslContext = sslContext;
            return this;
        }

        /**
         * Add a header and value(s) to set response of WebSocket handshakes.
         *
         * @param name   the header name
         * @param values the header value(s)
         * @return this builder
         */
        public Builder httHeader(String name, String... values) {
            if (values != null) {
                Arrays.stream(values).forEach(value -> this.headers.add(name, value));
            }
            return this;
        }

        public Builder routes(List<Routes> routes) {
            this.apiRoutes.addAll(routes);
            return this;
        }

        public Builder authHandle(RestAuthHandler authHandler) {
            this.authHandler = authHandler;
            return this;
        }

        public Builder route(Consumer<HttpServerRoutes> routesCustomizer) {
            if (this.routesCustomizer == null) {
                this.routesCustomizer = routesCustomizer;
            } else {
                this.routesCustomizer = this.routesCustomizer.andThen(routesCustomizer);
            }
            return this;
        }

        private InetSocketAddress getListenAddress() {
            if (this.address != null) {
                return new InetSocketAddress(this.address.getHostAddress(), this.port);
            }
            return new InetSocketAddress(this.port);
        }

        private HttpServer configureSsl(HttpServer server) {
            return server.secure(spec -> spec.sslContext(sslContext));
        }

        public AdminApiServer build() {
            HttpServer httpServer = HttpServer.create()
                    .metrics(true, () -> new MicrometerChannelMetricsRecorder("admin-api-server", "http"))
                    .option(SO_REUSEADDR, true)
                    .childOption(SO_REUSEADDR, true)
                    .childOption(SO_LINGER, 0)
                    .childOption(TCP_NODELAY, true)
                    .bindAddress(this::getListenAddress);
                 //   .route(this.routesCustomizer);

            Function<HttpServer, HttpServer> sslConfigurer = server ->
                    this.sslContext == null ? server : configureSsl(server);

            if (authHandler == null) {
                this.authHandler = credentials -> Mono.fromCallable(() -> true);
            }

            return new AdminApiServer(sslConfigurer.apply(httpServer), apiRoutes, authHandler);
        }
    }
} 
