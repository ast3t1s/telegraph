package im.telegraph.cluster.event;

import im.telegraph.cluster.ClusterEvent;
import im.telegraph.cluster.Member;

/**
 * @author Ast3t1s
 */
public final class ClusterMembershipEvent implements ClusterEvent {

    public enum Type {
        ADDED,
        REMOVED,
        LEAVING,
        UPDATED
    }

    private final Type type;
    private final Member member;

    public ClusterMembershipEvent(Type type, Member member) {
        this.type = type;
        this.member = member;
    }

    /**
     * Creates REMOVED membership event with cluster member and its metadata (optional).
     *
     * @param member cluster member; not null
     * @return membership event
     */
    public static ClusterMembershipEvent createRemoved(Member member) {
        return new ClusterMembershipEvent(Type.REMOVED, member);
    }

    /**
     * Creates ADDED membership event with cluster member and its metadata.
     *
     * @param member cluster member; not null
     * @return membership event
     */
    public static ClusterMembershipEvent createAdded(Member member) {
        return new ClusterMembershipEvent(Type.ADDED, member);
    }

    public static ClusterMembershipEvent createLeaving(Member member) {
        return new ClusterMembershipEvent(Type.LEAVING, member);
    }

    /**
     * Creates UPDATED membership event.
     *
     * @param member cluster member; not null
     * @return membership event
     */
    public static ClusterMembershipEvent createUpdated(Member member) {
        return new ClusterMembershipEvent(Type.UPDATED, member);
    }

    public Type type() {
        return type;
    }

    public boolean isAdded() {
        return type == Type.ADDED;
    }

    public boolean isRemoved() {
        return type == Type.REMOVED;
    }

    public boolean isUpdated() {
        return type == Type.UPDATED;
    }

    public Member member() {
        return member;
    }

    @Override
    public String toString() {
        return "MembershipEvent{" +
                "type=" + type +
                ", member=" + member +
                '}';
    }

} 
