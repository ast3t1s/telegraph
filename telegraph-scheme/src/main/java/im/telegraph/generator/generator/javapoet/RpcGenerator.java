package im.telegraph.generator.generator.javapoet;

import com.squareup.javapoet.*;
import im.telegraph.generator.generator.Constants;
import im.telegraph.generator.generator.SchemeContainerGenerator;
import im.telegraph.generator.generator.SchemeGenerator;
import im.telegraph.generator.type.*;

import javax.lang.model.element.Modifier;
import java.io.File;
import java.io.IOException;
import java.util.AbstractMap;
import java.util.List;

public class RpcGenerator implements SchemeGenerator {

    private final SchemeContainerGenerator<TypeSpec.Builder> containerGenerator = new JavaPoetContainerGenerator();

    @Override
    public void generate(SchemeDefinition definition, String out) throws IOException {
        long start = System.currentTimeMillis();
        for (AbstractMap.SimpleEntry<String, List<SchemeRpc>> pairRpc : definition.getAllRpc()) {

            for (SchemeRpc rpc : pairRpc.getValue()) {
                String javaName = TypeHelper.getRequestName(pairRpc.getKey(), rpc.getName());

                String responseJavaName;
                if (rpc.getResponse() instanceof SchemeRpc.RefResponse) {
                    responseJavaName = TypeHelper.getResponseName(((SchemeRpc.RefResponse) rpc.getResponse()).getName());
                } else {
                    responseJavaName = TypeHelper.getAnonymousResponseName(pairRpc.getKey(), rpc.getName());
                }

                TypeSpec.Builder clazz = TypeSpec.classBuilder(javaName)
                        .addJavadoc(Constants.NOTICE)
                        .addJavadoc(Constants.DEFAULT_AUTHOR)
                        .addModifiers(Modifier.PUBLIC)
                        .superclass(ParameterizedTypeName.get(Constants.TYPE_RPC_REQUEST,
                                ClassName.get(Constants.PACKAGE_RPC, responseJavaName)))
                        .addField(FieldSpec.builder(Constants.HEADER_TYPE, Constants.HEADER)
                                .addModifiers(Modifier.PUBLIC, Modifier.STATIC, Modifier.FINAL)
                                .initializer("0x$L", Integer.toHexString(rpc.getHeader()))
                                .build())
                        .addMethod(MethodSpec.methodBuilder("fromBytes")
                                .addParameter(byte[].class, "data")
                                .returns(ClassName.get("", javaName))
                                .addStatement("return $T.fromByteArray(new $L(), data)", Constants.TYPE_CONTEXT, javaName)
                                .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                                .addException(IOException.class)
                                .build());

                //Generate fields
                containerGenerator.generateFields(clazz, definition, rpc);

                //Generate default constructor
                clazz.addMethod(MethodSpec.constructorBuilder()
                        .addModifiers(Modifier.PUBLIC)
                        .build());

                if (rpc.getAttributes().size() > 0) {
                    containerGenerator.generateConstructor(clazz, definition, rpc);
                }

                //Generate header method
                clazz.addMethod(MethodSpec.methodBuilder(Constants.CLASS_ID_METHOD)
                        .addModifiers(Modifier.PUBLIC)
                        .addAnnotation(Override.class)
                        .returns(int.class)
                        .addStatement("return CLASS_ID")
                        .build());

                containerGenerator.generateGettersAndSetters(clazz, definition, rpc);
                containerGenerator.generateSerialization(clazz, definition, rpc);
                containerGenerator.generateDeserialization(clazz, definition, rpc);
                containerGenerator.generateToString(clazz, definition, rpc);

                JavaFile.builder(Constants.PACKAGE_RPC, clazz.build())
                        .addStaticImport(Constants.TYPE_UTILS, "*")
                        .indent("    ")
                        .build().writeTo(new File(out));
            }
        }

        for (AbstractMap.SimpleEntry<String, List<SchemeBaseResponse>> entry : definition.getResponses()) {
            for (SchemeBaseResponse u : entry.getValue()) {
                String javaName;
                if (u instanceof SchemeResponse) {
                    javaName = TypeHelper.getResponseName(((SchemeResponse) u).getName());
                } else {
                    SchemeResponseAnonymous anonymous = (SchemeResponseAnonymous) u;
                    javaName = TypeHelper.getAnonymousResponseName(entry.getKey(), anonymous.getRpc().getName());
                }

                TypeSpec.Builder clazz = TypeSpec.classBuilder(javaName)
                        .addJavadoc(Constants.NOTICE)
                        .addJavadoc(Constants.DEFAULT_AUTHOR)
                        .addModifiers(Modifier.PUBLIC)
                        .superclass(Constants.TYPE_RPC_RESPONSE)
                        .addField(FieldSpec.builder(int.class, "CLASS_ID")
                                .addModifiers(Modifier.PUBLIC, Modifier.STATIC, Modifier.FINAL)
                                .initializer("0x$L", Integer.toHexString(u.getHeader()))
                                .build())
                        .addMethod(MethodSpec.methodBuilder("fromBytes")
                                .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                                .returns(ClassName.get("", javaName))
                                .addParameter(byte[].class, "data")
                                .addException(IOException.class)
                                .addStatement("return $T.fromByteArray(new $T(), data)", Constants.TYPE_CONTEXT,
                                        ClassName.get("", javaName))
                                .build());

                containerGenerator.generateFields(clazz, definition, u);

                clazz.addMethod(MethodSpec.constructorBuilder()
                        .addModifiers(Modifier.PUBLIC)
                        .build());

                if (u.getAttributes().size() > 0) {
                    containerGenerator.generateConstructor(clazz, definition, u);
                }

                containerGenerator.generateGettersAndSetters(clazz, definition, u);
                containerGenerator.generateDeserialization(clazz, definition, u);
                containerGenerator.generateSerialization(clazz, definition, u);
                containerGenerator.generateToString(clazz, definition, u);

                clazz.addMethod(MethodSpec.methodBuilder(Constants.CLASS_ID_METHOD)
                        .returns(int.class)
                        .addModifiers(Modifier.PUBLIC)
                        .addAnnotation(Override.class)
                        .addStatement("return $L", Constants.HEADER)
                        .build());

                JavaFile.builder(Constants.PACKAGE_RPC, clazz.build())
                        .addStaticImport(Constants.TYPE_UTILS, "*")
                        .indent("    ")
                        .build().writeTo(new File(out));
            }
        }

       /* for (SchemeRpc rpc : definition.getAllRpc()) {
            String javaName = TypeHelper.getRequestName(rpc.getName());

            String responseJavaName;
            if (rpc.getResponse() instanceof SchemeRpc.RefResponse) {
                responseJavaName = TypeHelper.getResponseName(((SchemeRpc.RefResponse) rpc.getResponse()).getName());
            } else {
                responseJavaName = TypeHelper.getAnonymousResponseName(rpc.getName());
            }

            TypeSpec.Builder clazz = TypeSpec.classBuilder(javaName)
                    .addJavadoc(Constants.NOTICE)
                    .addJavadoc(Constants.DEFAULT_AUTHOR)
                    .addModifiers(Modifier.PUBLIC)
                    .superclass(ParameterizedTypeName.get(Constants.TYPE_RPC_REQUEST,
                            ClassName.get(Constants.PACKAGE_RPC, responseJavaName)))
                    .addField(FieldSpec.builder(Constants.HEADER_TYPE, Constants.HEADER)
                            .addModifiers(Modifier.PUBLIC, Modifier.STATIC, Modifier.FINAL)
                            .initializer("0x$L", Integer.toHexString(rpc.getHeader()))
                            .build())
                    .addMethod(MethodSpec.methodBuilder("fromBytes")
                            .addParameter(byte[].class, "data")
                            .returns(ClassName.get("", javaName))
                            .addStatement("return $T.fromByteArray(new $L(), data)", Constants.TYPE_CONTEXT, javaName)
                            .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                            .addException(IOException.class)
                            .build());

            //Generate fields
            containerGenerator.generateFields(clazz, definition, rpc);

            //Generate default constructor
            clazz.addMethod(MethodSpec.constructorBuilder()
                    .addModifiers(Modifier.PUBLIC)
                    .build());

            if (rpc.getAttributes().size() > 0) {
                containerGenerator.generateConstructor(clazz, definition, rpc);
            }

            //Generate header method
            clazz.addMethod(MethodSpec.methodBuilder(Constants.CLASS_ID_METHOD)
                    .addModifiers(Modifier.PUBLIC)
                    .addAnnotation(Override.class)
                    .returns(int.class)
                    .addStatement("return CLASS_ID")
                    .build());

            containerGenerator.generateGettersAndSetters(clazz, definition, rpc);
            containerGenerator.generateSerialization(clazz, definition, rpc);
            containerGenerator.generateDeserialization(clazz, definition, rpc);
            containerGenerator.generateToString(clazz, definition, rpc);

            JavaFile.builder(Constants.PACKAGE_RPC, clazz.build())
                    .addStaticImport(Constants.TYPE_UTILS, "*")
                    .indent("    ")
                    .build().writeTo(new File(out));
        }*/

        /*for (SchemeBaseResponse u : definition.getAllResponses()) {
            String javaName;
            if (u instanceof SchemeResponse) {
                javaName = TypeHelper.getResponseName(((SchemeResponse) u).getName());
            } else {
                SchemeResponseAnonymous anonymous = (SchemeResponseAnonymous) u;
                javaName = TypeHelper.getAnonymousResponseName(anonymous.getRpc().getName());
            }

            TypeSpec.Builder clazz = TypeSpec.classBuilder(javaName)
                    .addJavadoc(Constants.NOTICE)
                    .addJavadoc(Constants.DEFAULT_AUTHOR)
                    .addModifiers(Modifier.PUBLIC)
                    .superclass(Constants.TYPE_RPC_RESPONSE)
                    .addField(FieldSpec.builder(int.class, "CLASS_ID")
                            .addModifiers(Modifier.PUBLIC, Modifier.STATIC, Modifier.FINAL)
                            .initializer("0x$L", Integer.toHexString(u.getHeader()))
                            .build())
                    .addMethod(MethodSpec.methodBuilder("fromBytes")
                            .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                            .returns(ClassName.get("", javaName))
                            .addParameter(byte[].class, "data")
                            .addException(IOException.class)
                            .addStatement("return $T.fromByteArray(new $T(), data)", Constants.TYPE_CONTEXT,
                                    ClassName.get("", javaName))
                            .build());

            containerGenerator.generateFields(clazz, definition, u);

            clazz.addMethod(MethodSpec.constructorBuilder()
                    .addModifiers(Modifier.PUBLIC)
                    .build());

            if (u.getAttributes().size() > 0) {
                containerGenerator.generateConstructor(clazz, definition, u);
            }

            containerGenerator.generateGettersAndSetters(clazz, definition, u);
            containerGenerator.generateDeserialization(clazz, definition, u);
            containerGenerator.generateSerialization(clazz, definition, u);
            containerGenerator.generateToString(clazz, definition, u);

            clazz.addMethod(MethodSpec.methodBuilder(Constants.CLASS_ID_METHOD)
                    .returns(int.class)
                    .addModifiers(Modifier.PUBLIC)
                    .addAnnotation(Override.class)
                    .addStatement("return $L", Constants.HEADER)
                    .build());

            JavaFile.builder(Constants.PACKAGE_RPC, clazz.build())
                    .addStaticImport(Constants.TYPE_UTILS, "*")
                    .indent("    ")
                    .build().writeTo(new File(out));
        }*/
        System.out.println("Total rpcs " + definition.getAllRpc().size() + " generated in "
                + (System.currentTimeMillis() - start) + " ms");
    }
}
