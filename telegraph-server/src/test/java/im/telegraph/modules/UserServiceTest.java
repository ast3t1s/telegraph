package im.telegraph.modules;

import im.telegraph.TelegraphApplication;
import im.telegraph.common.ACLHashGenerator;
import im.telegraph.common.StateHolder;
import im.telegraph.common.security.crypto.PasswordEncoder;
import im.telegraph.domain.entity.Id;
import im.telegraph.domain.entity.User;
import im.telegraph.domain.repository.ChatRepository;
import im.telegraph.domain.repository.UserRepository;
import im.telegraph.service.UserService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import reactor.test.StepVerifier;
import reactor.tools.shaded.net.bytebuddy.utility.RandomString;

import java.time.Instant;
import java.util.Locale;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = TelegraphApplication.class)
class UserServiceTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private StateHolder stateHolder;

    @Autowired
    private ACLHashGenerator hashGenerator;

    @Autowired
    private UserService userService;

    @Autowired
    private ChatRepository chatRepository;

    @Autowired
    private PasswordEncoder encoder;

    @BeforeEach
    public void setUp() {
      //  userRepository.deleteAll().block();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void testSqlQuot() {
        //String query = buildQuery(JdbcClusterMemberRepository.TABLE_NAME, JdbcClusterMemberRepository.columns);

        //System.out.println(query);
    }

    @Test
    public void testCreateUserValid() {
        StepVerifier.create(userService.create("newUser", "newEmail@gmail.com", "password",
                "Display name",  Locale.US.getCountry(), User.Role.ROLE_USER))
                .expectSubscription()
                .assertNext(created -> {
                    assertThat(created.getId()).isNotNull();
                    assertThat(created.getUsername()).isEqualTo("newUser");
                    assertThat(created.getEmail()).isEqualTo("newEmail@gmail.com");
                    assertThat(created.getDisplayName()).isEqualTo("Display name");
                })
                .verifyComplete();

        StepVerifier.create(userRepository.findByUsername("newUser"))
                .expectSubscription()
                .assertNext(saved -> {
                    assertThat(saved.getUsername()).isEqualTo("newUser");
                    assertThat(saved.getDeletedAt()).isNull();
                    assertThat(saved.getRole()).isEqualTo(User.Role.ROLE_USER);
                })
                .verifyComplete();
    }

    @Test
    public void testCreateUserInvalidNick() {
        StepVerifier.create(userService.create("1233", "newEmail@gmail.com", "password",
                "Display name", Locale.US.getCountry(), User.Role.ROLE_USER))
                .expectSubscription()
                //.expectError(UserException.class)
                .verifyComplete();

        StepVerifier.create(userRepository.findByUsername("12"))
                .expectSubscription()
                .expectNextCount(0L)
                .verifyComplete();
    }

    @Test
    public void testCreateUserInvalidEmail() {
       /* StepVerifier.create(userService.createUser("testUser", "wrong email", "password",
                "Display name", false))
                .expectSubscription()
                .expectError(UserException.class)
                .verify();-*/

        StepVerifier.create(userRepository.findByEmailIgnoreCase("wrong email"))
                .expectSubscription()
                .expectNextCount(0L)
                .verifyComplete();
    }

    @Test
    public void testCreateUserInvalidPassword() {
       /* StepVerifier.create(userService.createUser("testUser", "test@gmail", "123",
                "Display name", false))
                .expectSubscription()
                .expectError(UserException.class)
                .verify();*/

        StepVerifier.create(userRepository.findByUsername("testUser"))
                .expectSubscription()
                .expectNextCount(0L)
                .verifyComplete();

        StepVerifier.create(chatRepository.findById(Id.of(1)))
                .expectSubscription()
                .expectNextCount(0l)
                .verifyComplete();
    }

    @Test
    public void testUpdateUserDisplayName() {
        User entity = User.builder()
                .username("testUser")
                .email("test@gmail.com")
                .password(encoder.encode("password"))
                .accessSalt(hashGenerator.nextAccessSalt())
                .displayName("test name")
                .verified(false)
                .enabled(true)
                .role(User.Role.ROLE_USER)
                .verificationKey(hashGenerator.random())
                .createdAt(Instant.now())
                .updatedAt(Instant.now())
                .build();

        User saved = userRepository.insert(entity).block();

        StepVerifier.create(userService.updateProfile(saved.getId(), "new name", null))
                .expectSubscription()
                .verifyComplete();

        StepVerifier.create(userRepository.findByUsername("testUser"))
                .expectSubscription()
                .assertNext(user -> assertThat(user.getDisplayName()).isEqualTo("new name"))
                .verifyComplete();

       // userRepository.de(saved).block();
    }

    @Test
    public void testUpdateUserAboutValid() {
        User entity = User.builder()
                .username("testUser")
                .email("test@gmail.com")
                .password(encoder.encode("password"))
                .accessSalt(hashGenerator.nextAccessSalt())
                .displayName("test name")
                .verified(false)
                .enabled(true)
                .languageCode(Locale.ENGLISH.getCountry())
                .role(User.Role.ROLE_USER)
                .verificationKey(hashGenerator.random())
                .createdAt(Instant.now())
                .updatedAt(Instant.now())
                .build();

        User saved = userRepository.insert(entity).block();

        StepVerifier.create(userService.updateProfile(saved.getId(), null, "some description"))
                .expectSubscription()
                .verifyComplete();

        StepVerifier.create(userRepository.findByUsername("testUser"))
                .expectSubscription()
                .assertNext(user -> assertThat(user.getAbout()).isEqualTo("some description"))
                .verifyComplete();

        //userRepository.delete(saved).block();
    }

    @Test
    public void testUpdateUserAboutTooLong() {
        User entity = User.builder()
                .username("testUser")
                .email("test@gmail.com")
                .password(encoder.encode("password"))
                .accessSalt(hashGenerator.nextAccessSalt())
                .displayName("test name")
                .verified(false)
                .enabled(true)
                .role(User.Role.ROLE_USER)
                .verificationKey(hashGenerator.random())
                .createdAt(Instant.now())
                .updatedAt(Instant.now())
                .build();

        User saved = userRepository.insert(entity).block();

        StepVerifier.create(userService.updateProfile(saved.getId(), RandomString.make(270), null))
                .expectSubscription()
               // .expectError(UserException.class)
                .verifyComplete();

        //userRepository.delete(saved).block();
    }

    @Test
    void testCheckAccessHash() {
        User entity = User.builder()
                .username("testUser")
                .email("test@gmail.com")
                .password(encoder.encode("password"))
                .accessSalt(hashGenerator.nextAccessSalt())
                .displayName("test name")
                .verified(false)
                .enabled(true)
                .role(User.Role.ROLE_USER)
                .verificationKey(hashGenerator.random())
                .createdAt(Instant.now())
                .updatedAt(Instant.now())
                .build();

        User saved = userRepository.insert(entity).block();

        long authId = 321312L;
        long accessHash = hashGenerator.generateUserAccessHash(authId, saved.getId().asLong(), saved.getAccessSalt());

        StepVerifier.create(userService.checkAccessHash(saved.getId(), authId, accessHash))
                .expectSubscription()
                .expectNext(true)
                .verifyComplete();


        StepVerifier.create(userService.checkAccessHash(saved.getId(), 1111L, accessHash))
                .expectSubscription()
                .expectNext(false)
                .verifyComplete();

        //userRepository.delete(saved).block();
    }
}