package im.telegraph.protocol.cryptography;

import im.telegraph.crypto.primitives.util.ByteStrings;
import im.telegraph.util.ThreadLocalSecureRandom;

import java.util.Arrays;
import java.util.Objects;

/**
 * Represents a trusted server key.
 *
 * @author Vsevolod Bondarenko
 */
public class ServerTrustedKey {

    private final long id;
    private final byte[] publicKey;
    private final byte[] privateKey;

    private ServerTrustedKey(byte[] publicKey, byte[] privateKey) {
        this.publicKey = publicKey;
        this.privateKey = privateKey;

        byte[] hash = ThreadLocalSecureRandom.current().generateSeed(32);
        this.id = ByteStrings.bytesToLong(Arrays.copyOfRange(hash, 0, Long.BYTES));
    }

    public static ServerTrustedKey of(byte[] publicKey, byte[] privateKey) {
        return new ServerTrustedKey(publicKey, privateKey);
    }

    /**
     * Returns the identifier of the trusted key.
     *
     * @return the identifier of the trusted key
     */
    public long getId() {
        return id;
    }

    /**
     * Returns the trusted public key.
     *
     * @return the trusted public key
     */
    public byte[] getPublicKey() {
        return publicKey;
    }

    /**
     * Returns the trusted private key
     *
     * @return the trusted private key
     */
    public byte[] getPrivateKey() {
        return privateKey;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ServerTrustedKey that = (ServerTrustedKey) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "ServerTrustedKey{" +
                "id=" + getId() +
                '}';
    }
}
