package im.telegraph.domain.repository;

import im.telegraph.domain.entity.AuthKey;
import reactor.core.publisher.Mono;

/**
 * @author Ast3t1s
 */
public interface AuthKeyRepository {

    /**
     * Creates a new AuthKey object.
     *
     * @param authKey the AuthKey object to create
     * @return A {@link Mono} where, upon successful completion, emits the {@link AuthKey}.If an error is received,
     * it is emitted through the {@code Mono}.
     */
    Mono<AuthKey> insert(AuthKey authKey);

    /**
     * Updates an existing AuthKey object.
     *
     * @param authKey the AuthKey object to update
     * @return A {@link Mono} where, upon successful completion, emits the {@link AuthKey}.If an error is received,
     * it is emitted through the {@code Mono}.
     */
    Mono<AuthKey> update(AuthKey authKey);

    /**
     * Find an auth key by authorization identifier.
     *
     * @param authId the authorization identifier
     * @return A {@link Mono} where, upon successful completion, emits the {@link AuthKey}.If an error is received,
     * it is emitted through the {@code Mono}.
     */
    Mono<AuthKey> findByAuthIdAndDeletedAtIsNull(Long authId);

    Mono<Boolean> deleteById(Long authKeyId);
}
