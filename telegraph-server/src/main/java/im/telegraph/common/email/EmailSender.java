package im.telegraph.common.email;

@FunctionalInterface
public interface EmailSender {

    /**
     * Send email with the given params.
     *
     * @param email the email instance.
     */
    void sendEmail(Email email);
}
