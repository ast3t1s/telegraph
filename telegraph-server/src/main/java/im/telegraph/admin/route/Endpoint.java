package im.telegraph.admin.route;

import io.netty.handler.codec.http.HttpMethod;
import reactor.netty.http.server.HttpServerRequest;

import java.util.Objects;

/**
 * @author Ast3t1s
 */
public class Endpoint {

    public static Endpoint ofFixedPath(HttpMethod method, String path) {
        return new Endpoint(method, path, new UriMatcher.Fixed(path));
    }

    private final HttpMethod method;
    private final String path;
    private final UriMatcher uriMatcher;

    public Endpoint(HttpMethod method, String path) {
        this.method = method;
        this.path = path;
        this.uriMatcher = UriMatcher.Impl.of(path);
    }

    private Endpoint(HttpMethod method, String path, UriMatcher uriMatcher) {
        this.method = method;
        this.path = path;
        this.uriMatcher = uriMatcher;
    }

    public HttpMethod getMethod() {
        return method;
    }

    public String getPath() {
        return path;
    }

    public boolean matches(HttpServerRequest request) {
        return method.equals(request.method())
                && uriMatcher.matches(request.uri());
    }

    UriMatcher getUriMatcher() {
        return uriMatcher;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        Endpoint endpoint = (Endpoint) object;
        return Objects.equals(method, endpoint.method) &&
                Objects.equals(path, endpoint.path);
    }

    @Override
    public int hashCode() {
        return Objects.hash(method, path);
    }
}
