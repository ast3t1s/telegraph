package im.telegraph.server.reactor;

import im.telegraph.server.ConnectionAcceptor;
import im.telegraph.server.Server;
import im.telegraph.server.TransportConnection;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelOption;
import io.netty.handler.codec.http.DefaultHttpHeaders;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.websocketx.PongWebSocketFrame;
import io.netty.handler.ssl.SslContext;
import io.netty.util.ReferenceCountUtil;
import org.reactivestreams.Publisher;
import reactor.netty.Connection;
import reactor.netty.DisposableServer;
import reactor.netty.channel.MicrometerChannelMetricsRecorder;
import reactor.netty.http.server.HttpServer;
import reactor.netty.http.server.HttpServerRequest;
import reactor.netty.http.server.HttpServerRoutes;
import reactor.netty.http.websocket.WebsocketInbound;
import reactor.netty.http.websocket.WebsocketOutbound;
import reactor.netty.resources.LoopResources;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.time.Duration;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.*;

/**
 * An implementation of {@link Server} for http connection.
 *
 * @author Ast3t1s
 */
public final class NettyWebsocketServer extends NettyBaseServer {

    private static final AtomicInteger ID = new AtomicInteger();

    private static final Supplier<LoopResources> DEFAULT_WEBSOCKET_RESOURCES =
            () -> LoopResources.create("tl-websocket-" + ID.incrementAndGet());

    private final Predicate<HttpServerRequest> requestFilter;
    private final HttpServer httpServer;
    private final HttpHeaders httpHeaders;
    private final String mappingPath;
    private final ConnectionAcceptor handler;
    private final Duration lifecycleTimeout;
    private final WebsocketHandlerAdapter handlerAdapter = new WebsocketHandlerAdapter();

    /**
     * Created a new reactor netty http server with the given parameters.
     *
     * @param httpServer       the {@link HttpServer} the should use to handle HTTP requests.
     * @param requestFilter    the request filter.
     * @param handler          the {@link ConnectionAcceptor} acceptor to handle server connections.
     * @param lifecycleTimeout the lifecycle timeout.
     */
    public NettyWebsocketServer(HttpServer httpServer,
                                HttpHeaders httpHeaders,
                                String mappingPath,
                                Predicate<HttpServerRequest> requestFilter,
                                ConnectionAcceptor handler,
                                Duration lifecycleTimeout) {
        this.httpServer = Objects.requireNonNull(httpServer, "httpServer");
        this.handler = Objects.requireNonNull(handler, "handler");
        this.mappingPath = Objects.requireNonNull(mappingPath, "mappingPath");
        this.httpHeaders = httpHeaders;
        this.requestFilter = requestFilter != null ? requestFilter : request -> true;
        this.lifecycleTimeout = lifecycleTimeout;
    }

    /**
     * Return a new builder for {@link NettyWebsocketServer}.
     *
     * @return a new builder
     */
    public static NettyWebsocketServer.Builder builder() {
        return new Builder();
    }

    /**
     * Gets the configured {@link HttpServer}.
     *
     * @return the configured {@link HttpServer}
     */
    public final HttpServer getHttpServer() {
        return httpServer;
    }

    protected DisposableServer startHttpServer() {
        Function<HttpServer, HttpServer> extendServer = httpServer -> httpServer.handle((req, res) -> {
                    res.headers(httpHeaders);
                    return res.sendWebsocket(handlerAdapter);
                }).route(routes -> routes.ws(mappingPath, handlerAdapter));

        HttpServer httpServer = extendServer.apply(this.httpServer);
        if (this.lifecycleTimeout != null) {
            return httpServer.bindNow(this.lifecycleTimeout);
        }
        return httpServer.bindNow();
    }

    @Override
    protected final String getServerName() {
        return "Websocket Telegraph Netty Server";
    }

    @Override
    protected final DisposableServer bind() {
        return startHttpServer();
    }

    /**
     * An alias for a Reactor Netty server route.
     */
    private class WebsocketHandlerAdapter
            implements BiFunction<WebsocketInbound, WebsocketOutbound, Publisher<Void>> {

        @Override
        public Publisher<Void> apply(WebsocketInbound inbound, WebsocketOutbound outbound) {
            outbound.withConnection(conn -> {
                if (log.isDebugEnabled()) {
                    log.debug("Connected from " + conn.address());
                }
            });

            TransportConnection transportConnection = new WebsocketTransportConnection((Connection) inbound);
            if (log.isDebugEnabled()) {
                log.debug("Websocket transport connection initialized {}", transportConnection);
            }
            return handler.accept(transportConnection).checkpoint("Websocket session bound " + transportConnection)
                    .doOnError(ex -> log.trace(transportConnection + " Failed to complete: " + ex.getMessage()))
                    .doOnSuccess(__ -> log.trace(transportConnection + " Handling complete"));
        }
    }

    @ChannelHandler.Sharable
    private static class FrameHandler extends ChannelInboundHandlerAdapter {

        @Override
        public void channelRead(ChannelHandlerContext ctx, Object msg) {
            if (msg instanceof PongWebSocketFrame) {
                ReferenceCountUtil.safeRelease(msg);
                ctx.read();
            } else {
                ctx.fireChannelRead(msg);
            }
        }
    }

    /**
     * A builder to create {@link Server} instances.
     */
    public static class Builder implements Server.Builder {

        private Predicate<HttpServerRequest> requestFilter = httpServerRequest -> true;
        private final HttpHeaders headers = new DefaultHttpHeaders();
        private Consumer<HttpServerRoutes> routesCustomizer;
        private ConnectionAcceptor handler;
        private InetAddress address;
        private int port = 9091; //Default ws port
        private String mappingPath = "/";
        private SslContext sslContext;
        private Duration lifecycleTimeout;
        private LoopResources loopResources;

        @Override
        public Builder handler(ConnectionAcceptor handler) {
            this.handler = Objects.requireNonNull(handler);
            return this;
        }

        @Override
        public Builder host(InetAddress address) {
            this.address = address;
            return this;
        }

        @Override
        public Builder port(int port) {
            if (port <= 0 || port > 65535) {
                throw new IllegalArgumentException("Invalid port:" + port);
            }
            this.port = port;
            return this;
        }

        @Override
        public Builder lifecycleTimeout(Duration lifecycleTimeout) {
            this.lifecycleTimeout = lifecycleTimeout;
            return this;
        }

        /**
         * Sets the SSL configuration that will be applied to the server's default connector.
         *
         * @param sslContext the SSL configuration
         * @return this builder
         */
        public Builder ssl(SslContext sslContext) {
            this.sslContext = sslContext;
            return this;
        }

        /**
         * Sets the path under which server handles requests
         *
         * @param mappingPath the mapping path
         * @return this builder
         */
        public Builder mappingPath(String mappingPath) {
            this.mappingPath = mappingPath;
            return this;
        }

        /**
         * Add a header and value(s) to set response of WebSocket handshakes.
         *
         * @param name   the header name
         * @param values the header value(s)
         * @return this builder
         */
        public Builder httpHeader(String name, String... values) {
            if (values != null) {
                Arrays.stream(values).forEach(value -> this.headers.add(name, value));
            }
            return this;
        }

        /**
         * Sets the http request filter. By the default server allows any request.
         *
         * @param requestFilter the http request filter
         * @return this builder
         */
        public Builder requestFilter(Predicate<HttpServerRequest> requestFilter) {
            this.requestFilter = Objects.requireNonNull(requestFilter);
            return this;
        }

        /**
         * Set the loop resources for http server.
         *
         * @param loopResources the loop resources
         * @return this builder
         */
        public Builder loopResources(LoopResources loopResources) {
            this.loopResources = loopResources;
            return this;
        }

        /**
         * Set the route customization for the reactor-netty HTTP server.
         *
         * @param routesConsumer a lambda to include additional HTTP routes to the server
         * @return this builder
         */
        public Builder route(Consumer<HttpServerRoutes> routesConsumer) {
            if (this.routesCustomizer == null) {
                this.routesCustomizer = routesConsumer;
            } else {
                this.routesCustomizer = this.routesCustomizer.andThen(routesConsumer);
            }
            return this;
        }

        private InetSocketAddress getListenAddress() {
            if (this.address != null) {
                return new InetSocketAddress(this.address.getHostAddress(), this.port);
            }
            return new InetSocketAddress(this.port);
        }

        private HttpServer configureSsl(HttpServer server) {
            return server.secure(spec -> spec.sslContext(sslContext));
        }

        @Override
        public Server build() {
            if (this.loopResources == null) {
                this.loopResources = DEFAULT_WEBSOCKET_RESOURCES.get();
            }

            HttpServer httpServer = HttpServer.create()
                    .runOn(this.loopResources)
                    .metrics(true, () -> new MicrometerChannelMetricsRecorder("ws-server", "ws"))
                    .option(ChannelOption.SO_BACKLOG, 128)
                    .option(ChannelOption.SO_REUSEADDR, true)
                    .childOption(ChannelOption.SO_REUSEADDR, true)
                    .childOption(ChannelOption.SO_LINGER, 0)
                    .childOption(ChannelOption.TCP_NODELAY, true)
                    .bindAddress(this::getListenAddress);

            if (this.routesCustomizer != null) {
                httpServer = httpServer.route(this.routesCustomizer);
            }
            Function<HttpServer, HttpServer> sslConfigurer = server ->
                    this.sslContext == null ? server : configureSsl(server);

            return new NettyWebsocketServer(sslConfigurer.apply(httpServer), headers, mappingPath, requestFilter,
                    handler, lifecycleTimeout);
        }
    }
}
