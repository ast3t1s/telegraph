package im.telegraph.service;

import im.telegraph.domain.event.Event;
import org.reactivestreams.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.lang.Nullable;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

/**
 * @author Ast3t1s
 */
public abstract class AbstractEventHandler<T extends Event> {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private final Publisher<Event> publisher;

    private final Class<T> eventType;

    @Nullable
    private Disposable subscription;

    @Nullable
    private Scheduler scheduler;

    protected AbstractEventHandler(Publisher<Event> publisher, Class<T> eventType) {
        this.publisher = publisher;
        this.eventType = eventType;
    }

    public void start() {
        this.scheduler = Schedulers.newSingle(this.getClass().getSimpleName(), true);
        this.subscription = Flux.from(this.publisher)
                .subscribeOn(this.scheduler)
                .doOnSubscribe((s) -> this.log.debug("Subscribed to {} events", this.eventType))
                .ofType(eventType)
                .transform(this::handle)
                .onErrorContinue((throwable, o) -> this.log.warn("Unexpected error", throwable))
                .subscribe();
    }

    protected abstract Publisher<Void> handle(Flux<T> publisher);

    public void stop() {
        if (this.subscription != null) {
            this.subscription.dispose();
            this.subscription = null;
        }
        if (this.scheduler != null) {
            this.scheduler.dispose();
            this.scheduler = null;
        }
    }
} 
