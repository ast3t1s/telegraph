package im.telegraph.server.reactor;

import im.telegraph.server.AbstractTransportConnection;
import im.telegraph.server.TransportFrame;
import im.telegraph.server.TransportFrameTransformer;
import io.netty.buffer.ByteBuf;
import io.netty.handler.codec.http.websocketx.BinaryWebSocketFrame;
import io.netty.util.ReferenceCountUtil;
import org.reactivestreams.Publisher;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.netty.Connection;

import java.net.SocketAddress;

/**
 * @author Ast3t1s
 */
public final class WebsocketTransportConnection extends AbstractTransportConnection<Connection> {

    private static final int TRANSPORT_FRAME_SIZE = 1024 * 1024; //1 Mb
    private static final int MIN_PACKET_SIZE = 9; // 9 bytes

    private final TransportFrameTransformer transportPacketTransformer;

    public WebsocketTransportConnection(Connection delegate) {
        super(delegate);
        this.transportPacketTransformer = new TransportFrameTransformer(TRANSPORT_FRAME_SIZE, MIN_PACKET_SIZE, getLogPrefix());
    }

    @Override
    protected Mono<Void> sendInternal(Publisher<? extends byte[]> packets) {
        Flux<BinaryWebSocketFrame> frames = Flux.from(packets)
                .map(payload -> {
                    ByteBuf byteBuf = getDelegate().outbound().alloc().buffer();
                    byteBuf.writeBytes(payload);
                    return byteBuf;
                })
                .map(BinaryWebSocketFrame::new)
                .doOnDiscard(ByteBuf.class, ReferenceCountUtil::safeRelease);
        return getDelegate().outbound().sendObject(frames).then();
    }

    @Override
    protected Flux<TransportFrame> receiveInternal() {
        return getDelegate().inbound().receive().asByteBuffer()
                .concatMapIterable(transportPacketTransformer);
    }

    @Override
    public SocketAddress remoteAddress() {
        return getDelegate().channel().remoteAddress();
    }

    @Override
    public boolean isOpen() {
        return getDelegate().isDisposed();
    }

    @Override
    public Mono<Void> onClose() {
        return getDelegate().onTerminate();
    }

    @Override
    public Mono<Void> close() {
        return Mono.fromRunnable(() -> getDelegate().disposeNow());
    }

    @Override
    public String toString() {
        return "WebsocketTransportConnection{" +
                "Created date=" + createdDate() +
                ", Last access time=" + getLastAccessTime() +
                ", Remote address=" + remoteAddress() +
                ", Received packets=" + getReceivedPackets() +
                ", Sent packets=" + getSentPackets() +
                ", Id='" + getId() + '\'' +
                '}';
    }
}
