package im.telegraph.handlers.dispatch;

import im.telegraph.rpc.base.Request;
import im.telegraph.rpc.base.Response;
import reactor.core.publisher.Mono;

/**
 * Handle for the rpc dispatch requests.
 *
 * @param <T> the inbound request type.
 * @param <R> the outbound response type.
 * @author Ast3t1s
 */
@FunctionalInterface
public interface DispatchHandler<T extends Request<? extends Response>, R extends Response> {

    /**
     * Executes a dispatch request with its context, which contains client information.
     *
     * @param context the dispatch context
     * @return a Flux of responses that are derived from the given dispatch context
     */
    Mono<R> handle(DispatchContext<T> context);
}
