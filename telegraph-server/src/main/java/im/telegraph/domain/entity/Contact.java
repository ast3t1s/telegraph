package im.telegraph.domain.entity;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

public class Contact implements Serializable {

    private static final long serialVersionUID = 1L;

    private final Id userId;
    private final Id contactId;
    private final Status status;
    private final Instant createdAt;

    /**
     * Constructs an {@link Contact instance} with an associated data.
     *
     * @param userId    Identifier of the user.
     * @param contactId Identifier of the user contact.
     * @param status    Status of the contact.
     * @param createdAt Point in time (Unix timestamp) when contact record was created.
     */
    public Contact(final Id userId, final Id contactId, final Status status, final Instant createdAt) {
        this.userId = Objects.requireNonNull(userId);
        this.contactId = Objects.requireNonNull(contactId);
        this.status = Objects.requireNonNull(status);
        this.createdAt = Objects.requireNonNull(createdAt);
    }

    /**
     * Factory method for updating contact status.
     *
     * @param status the new mutual state.
     */
    public Contact withStatus(final Status status) {
        return new Contact(userId, contactId, status, createdAt);
    }

    /**
     * Return the user ID.
     *
     * @return the id of the user.
     */
    public Id getUserId() {
        return userId;
    }

    /**
     * Return the user id related to.
     *
     * @return the id of the user related to.
     */
    public Id getContactId() {
        return contactId;
    }

    /**
     * Return the subscription flag.
     *
     * @return the subscription flag.
     */
    public Status getStatus() {
        return status;
    }

    /**
     * Return the date when relation was created.
     *
     * @return the date when relation was created.
     */
    public Instant getCreatedAt() {
        return createdAt;
    }

    public boolean isRequestedIn() {
        return getStatus() == Status.RECV_TO;
    }

    public boolean isRequestedOut() {
        return getStatus() == Status.RECV_FROM;
    }

    public boolean isMutual() {
        return getStatus() == Status.BOTH;
    }

    public boolean isRemoved() {
        return getStatus() == Status.RECV_FROM;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Contact that = (Contact) o;
        return Objects.equals(userId, that.userId) &&
                Objects.equals(contactId, that.contactId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, contactId);
    }

    public enum Status {
        /**
         * The relation owner has a subscription to the relation item's presence.
         */
        RECV_TO(1),

        /**
         * The relation item has a subscription to the relation owner's presence.
         */
        RECV_FROM(2),

        /**
         * The relation item and owner have a mutual subscription.
         */
        BOTH(3),
        /**
         * Indicates the relation item should be removed.
         */
        REMOVE(4),

        /**
         * The relation owner has a subscription to the relation item's presence.
         */
        NONE(-1);

        private final int value;

        Status(final int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        /**
         * Gets the contact status.
         *
         * @param value The underlying value as represented by storage
         * @return The {@link Status} of the contact
         */
        public static Status of(final int value) {
            return switch (value) {
                case 1 -> RECV_TO;
                case 2 -> RECV_FROM;
                case 3 -> BOTH;
                case 4 -> REMOVE;
                default -> NONE;
            };
        }
    }

    @Override
    public String toString() {
        return "Contact{" +
                "userId=" + userId +
                ", contactId=" + contactId +
                ", status=" + status +
                ", createdAt=" + createdAt +
                '}';
    }
}
