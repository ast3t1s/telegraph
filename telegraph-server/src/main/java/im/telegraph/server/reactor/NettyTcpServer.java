package im.telegraph.server.reactor;

import im.telegraph.server.TransportConnection;
import im.telegraph.server.ConnectionAcceptor;
import im.telegraph.server.Server;
import im.telegraph.server.TransportFrameTransformer;
import io.netty.channel.ChannelOption;
import io.netty.handler.ssl.SslContext;
import reactor.core.publisher.Mono;
import reactor.netty.Connection;
import reactor.netty.DisposableServer;
import reactor.netty.channel.MicrometerChannelMetricsRecorder;
import reactor.netty.resources.LoopResources;
import reactor.netty.tcp.TcpServer;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.time.Duration;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * An implementation of {@link Server} for tcp connection.
 *
 * @author Ast3t1s
 */
public final class NettyTcpServer extends NettyBaseServer {

    private static final AtomicInteger ID = new AtomicInteger();

    private static final Supplier<LoopResources> DEFAULT_TCP_RESOURCE =
            () -> LoopResources.create("tl-tcp-" + ID.incrementAndGet());

    private final TcpServer tcpServer;
    private final ConnectionAcceptor handler;
    private final Duration lifecycleTimeout;

    /**
     * Creates a new reactor netty tcp server with the given parameters.
     *
     * @param tcpServer        the {@link reactor.netty.tcp.TcpServer} instance.
     * @param handler          the session hand;er.
     * @param lifecycleTimeout the lifecycle timeout.
     */
    public NettyTcpServer(TcpServer tcpServer, ConnectionAcceptor handler, Duration lifecycleTimeout) {
        this.tcpServer = Objects.requireNonNull(tcpServer, "tcpServer");
        this.handler = Objects.requireNonNull(handler, "handler");
        this.lifecycleTimeout = lifecycleTimeout;
    }

    @Override
    protected final String getServerName() {
        return "Tcp Telegraph Netty Server";
    }

    @Override
    protected final DisposableServer bind() {
        Function<TcpServer, TcpServer> extendServer = tcpServer -> tcpServer.wiretap(true)
                .doOnConnection(new SocketHandlerAdapter());

        TcpServer server = extendServer.apply(this.tcpServer);
        if (this.lifecycleTimeout != null) {
            return server.bindNow(this.lifecycleTimeout);
        }
        return server.bindNow();
    }

    /**
     * Return a new default {@link NettyTcpServer}.
     *
     * @param handler the connection acceptor.
     * @return a new tcp transport server
     */
    public static Server create(ConnectionAcceptor handler) {
        return builder().handler(handler).build();
    }

    /**
     * Return a new builder for {@link NettyTcpServer}.
     *
     * @return a new builder
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     * Gets the configured {@link reactor.netty.tcp.TcpServer}.
     *
     * @return the configured {@link reactor.netty.tcp.TcpServer}
     */
    public final TcpServer getTcpServer() {
        return tcpServer;
    }

    /**
     * An alias for a Reactor Netty server route.
     */
    private final class SocketHandlerAdapter implements Consumer<Connection> {

        @Override
        public void accept(Connection connection) {
            if (log.isDebugEnabled()) {
                log.debug("Connected from " + connection.address());
            }

            TransportConnection transportConnection = new TcpTransportConnection(connection);
            if (log.isDebugEnabled()) {
                log.debug("Tcp transport connection initialized {}", transportConnection);
            }
            handler.accept(transportConnection).checkpoint("Socket connection bound" + connection.toString())
                    .doOnError(ex -> log.trace(transportConnection + " Failed to completed: " + ex.getMessage()))
                    .doOnSuccess(__ -> log.trace(transportConnection + " Handling complete"))
                    .then(Mono.<Void>never())
                    .subscribe(connection.disposeSubscriber());
        }
    }

    /**
     * A builder to create {@link Server} instances.
     */
    public static class Builder implements Server.Builder {

        private ConnectionAcceptor acceptor;
        private InetAddress address;
        private int port = 443; //Default tcp port
        private SslContext sslContext;
        private Duration lifecycleTimeout;
        private LoopResources loopResources;

        protected Builder() {
        }

        @Override
        public Builder handler(ConnectionAcceptor acceptor) {
            this.acceptor = Objects.requireNonNull(acceptor);
            return this;
        }

        @Override
        public Builder host(InetAddress address) {
            this.address = address;
            return this;
        }

        @Override
        public Builder port(int port) {
            if (port <= 0 || port > 65535) {
                throw new IllegalArgumentException("Invalid port:" + port);
            }
            this.port = port;
            return this;
        }

        @Override
        public Builder lifecycleTimeout(Duration lifecycleTimeout) {
            this.lifecycleTimeout = lifecycleTimeout;
            return this;
        }

        /**
         * Sets the SSL configuration that will be applied to the server's default connector.
         *
         * @param sslContext the SSL configuration
         * @return this builder
         */
        public Builder ssl(SslContext sslContext) {
            this.sslContext = sslContext;
            return this;
        }

        /**
         * Sets the {@link LoopResources} for netty server.
         *
         * @param loopResources the loop resources to set
         * @return this builder.
         */
        public Builder loopResource(LoopResources loopResources) {
            this.loopResources = loopResources;
            return this;
        }

        private InetSocketAddress getListenAddress() {
            if (this.address != null) {
                return new InetSocketAddress(this.address.getHostAddress(), this.port);
            }
            return new InetSocketAddress(this.port);
        }

        private TcpServer configureSsl(TcpServer server) {
            return server.secure(sslContextSpec -> sslContextSpec.sslContext(sslContext));
        }

        @Override
        public NettyTcpServer build() {
            if (this.loopResources == null) {
                this.loopResources = DEFAULT_TCP_RESOURCE.get();
            }

            TcpServer tcpServer = TcpServer.create()
                    .runOn(this.loopResources)
                    .doOnConnection(conn -> conn.addHandlerFirst(
                            new ReactorTransportFrameDecoder(new TransportFrameTransformer(1024 * 1024, 9, ""))))
                    .metrics(true, () -> new MicrometerChannelMetricsRecorder("tcp-server", "tcp"))
                    .option(ChannelOption.SO_BACKLOG, 128)
                    .option(ChannelOption.SO_REUSEADDR, true)
                    .childOption(ChannelOption.SO_REUSEADDR, true)
                    .childOption(ChannelOption.SO_LINGER, 0)
                    .childOption(ChannelOption.TCP_NODELAY, true)
                    .bindAddress(this::getListenAddress);

            Function<TcpServer, TcpServer> sslConfigurer =
                    server -> this.sslContext == null ? server : configureSsl(server);

            return new NettyTcpServer(sslConfigurer.apply(tcpServer), acceptor, lifecycleTimeout);
        }
    }
}
