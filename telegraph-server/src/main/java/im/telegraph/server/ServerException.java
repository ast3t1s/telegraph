package im.telegraph.server;

import java.util.function.Consumer;

/**
 * Exception thrown by server.
 *
 * @author Vsevolod Bondarenko
 */
@SuppressWarnings("serial")
public class ServerException extends RuntimeException {

    /**
     * Creates a new server exception for the given parameters
     * @param message the exception message;
     */
    public ServerException(String message) {
        super(message);
    }

    /**
     * Creates a new server exception for the given parameters
     * @param message the exception message;
     * @param cause the cause of the exception
     */
    public ServerException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Perform an action if the given exception was caused by a specific exception type.
     * @param <E> the cause exception type
     * @param ex the source exception
     * @param causedBy the required cause type
     * @param action the action to perform
     */
    @SuppressWarnings("unchecked")
    public static <E extends Exception> void ifCausedBy(Exception ex, Class<E> causedBy, Consumer<E> action) {
        Throwable candidate = ex;
        while (candidate != null) {
            if (causedBy.isInstance(candidate)) {
                action.accept((E) candidate);
                return;
            }
            candidate = candidate.getCause();
        }
    }
} 
