package im.telegraph.cluster.fd;

/**
 * @author Ast3t1s
 */
class PhiFailureDetectorTest {

   /* private final double phiThreshold = 1;
    private final long minStdDev = 100;
    private final long acceptableHeartbeatPause = 1000;
    private final Clock clock = Clock.systemUTC();

    private volatile Instant lastHeartbeat = Instant.EPOCH;

    private final PhiFailureDetector failureDetector = new PhiFailureDetector(phiThreshold, 100, Duration.ofMillis(minStdDev),
            Duration.ofMillis(acceptableHeartbeatPause), Duration.ofMillis(minStdDev), clock);

    @Test
    public void test_failure() throws Exception{
        failureDetector.heartbeat();
        System.out.println(failureDetector.isAvailable());
        Thread.sleep(acceptableHeartbeatPause * 3);
        System.out.println(failureDetector.isAvailable());

        System.out.println(failureDetector.lastHeartbeat());

        failureDetector.heartbeat();
        System.out.println(failureDetector.lastHeartbeat());

        Thread.sleep(250);

        failureDetector.heartbeat();
        System.out.println(failureDetector.lastHeartbeat());
    }

    @Test
    public void testheartBeat() {
        long timestamp = getAndSetLastHeartbeat(clock.millis());
        System.out.println(timestamp);
    }

   /* private long getAndSetLastHeartbeat(long timestampMillis) {
        long lastTimestampMillis = lastHeartbeat.toEpochMilli();
        lastHeartbeat = Instant.ofEpochMilli(timestampMillis);
        return lastTimestampMillis;
    }*/

  /*  private long getAndSetLastHeartbeat(long timestampMillis) {
        long lastTimestampMillis = lastHeartbeat.toEpochMilli();
        lastHeartbeat = Instant.ofEpochMilli(timestampMillis);
        return lastTimestampMillis;
    }

    @Test
    public void member_isAssumedAlive_beforeFirstHeartbeat() {
        assertTrue(failureDetector.isAvailable());
    }

    @Test
    public void member_isAlive_whenHeartbeat() {
        failureDetector.heartbeat();
        assertTrue(failureDetector.isAvailable());
    }

    @Test
    public void member_isAlive_beforeHeartbeatTimeout() {
        failureDetector.heartbeat();
        assertTrue(failureDetector.isAlive(clock.millis() + acceptableHeartbeatPause / 2));
    }

    @Test
    public void member_isNotAlive_afterHeartbeatTimeout() {
        failureDetector.heartbeat();
        long ts = clock.millis() + acceptableHeartbeatPause * 2;
        assertFalse(failureDetector.isAlive(ts), "Suspicion level: " + failureDetector.suspicionLevel(ts));
    }

    @Test
    public void lastHeartbeat_whenNoHeartbeat() {
        long lastHeartbeat = failureDetector.lastHeartbeat();
        assertEquals(PhiFailureDetector.NO_HEARTBEAT_TIMESTAMP, Instant.ofEpochMilli(lastHeartbeat));
    }

    @Test
    public void lastHeartbeat() {
        long timestamp = clock.millis();
        failureDetector.heartbeat();

        long lastHeartbeat = failureDetector.lastHeartbeat();
        assertEquals(timestamp, lastHeartbeat);
    }

    @Test
    public void nonSuspected_beforeFirstHeartbeat() {
       // double suspicionLevel = failureDetector.suspicionLevel(Clock.currentTimeMillis());

        //assertEquals(0, suspicionLevel, 0d);
    }

    @Test
    public void suspicionLevel_whenHeartbeat() {
        long timestamp = clock.millis();
        failureDetector.heartbeat();

        double suspicionLevel = failureDetector.suspicionLevel(timestamp);
        assertEquals(0, suspicionLevel, 0d);
    }

    @Test
    public void suspicionLevel_beforeHeartbeatTimeout() {
     /*   long timestamp = Clock.currentTimeMillis();
        failureDetector.heartbeat(timestamp);

        double suspicionLevel = failureDetector.suspicionLevel(timestamp + acceptableHeartbeatPause / 2);*/

      //  assertThat(suspicionLevel).isLessThan(phiThreshold);
  /*  }

    @Test
    public void suspicionLevel_afterHeartbeatTimeout() {
     /*   long timestamp = Clock.currentTimeMillis();
        failureDetector.heartbeat(timestamp);

        double suspicionLevel = failureDetector.suspicionLevel(timestamp + acceptableHeartbeatPause * 2);

        assertThat(suspicionLevel).isGreaterThanOrEqualTo(phiThreshold);*/
 /*   }

    @Test
    public void construct_withNegativeThreshold() {
        assertThrows(IllegalArgumentException.class,
                () -> new PhiFailureDetector(-1, 1, Duration.ofMillis(1), Duration.ofMillis(1), Duration.ofMillis(1)));
    }

    @Test
    public void construct_withZeroThreshold() {
        assertThrows(IllegalArgumentException.class,
                () -> new PhiFailureDetector(0, 1, Duration.ofMillis(1), Duration.ofMillis(1), Duration.ofMillis(1)));
    }

    @Test
    public void construct_withNegativeMinStdDev() {
        assertThrows(IllegalArgumentException.class,
                () -> new PhiFailureDetector(1, -1, Duration.ofMillis(1), Duration.ofMillis(1), Duration.ofMillis(1)));
    }

    @Test
    public void construct_withZeroMinStdDev() {
        assertThrows(IllegalArgumentException.class,
                () -> new PhiFailureDetector(1, 0, Duration.ofMillis(1), Duration.ofMillis(1), Duration.ofMillis(1)));
    }

    @Test
    public void construct_withNegativeFirstHeartbeatEstimation() {
        assertThrows(IllegalArgumentException.class,
                () -> new PhiFailureDetector(1, 1, Duration.ofMillis(1), Duration.ofMillis(1), Duration.ofMillis(-1)));
    }

    @Test
    public void construct_withZeroFirstHeartbeatEstimation() {
        assertThrows(IllegalArgumentException.class,
                () -> new PhiFailureDetector(1, 1, Duration.ofMillis(1), Duration.ofMillis(1), Duration.ofMillis(0)));
    }

    @Test
    public void construct_withNegativeAcceptableHeartbeatPause() {
        assertThrows(IllegalArgumentException.class,
                () -> new PhiFailureDetector(1, 1, Duration.ofMillis(1), Duration.ofMillis(-1), Duration.ofMillis(1)));
    }*/
}