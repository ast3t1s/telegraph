package im.telegraph.cluster.internal;

import im.telegraph.cluster.ClusterMessage;
import im.telegraph.cluster.UniqueAddress;

/**
 * @author Ast3t1s
 */
public class Welcome implements ClusterMessage {

    private static final long serialVersionUID = 1L;

    private final UniqueAddress from;
    private final GossipOverview gossipOverview;

    public Welcome(UniqueAddress from, GossipOverview gossipOverview) {
        this.from = from;
        this.gossipOverview = gossipOverview;
    }

    public UniqueAddress getFrom() {
        return from;
    }

    public GossipOverview getGossipOverview() {
        return gossipOverview;
    }

    @Override
    public String toString() {
        return "Welcome{" +
                "from=" + from +
                ", gossipOverview=" + gossipOverview +
                '}';
    }
}
