package im.telegraph.modules.avatar;

import com.sksamuel.scrimage.format.FormatDetector;
import im.telegraph.TelegraphApplication;
import im.telegraph.common.ACLHashGenerator;
import im.telegraph.domain.repository.FileRepository;
import im.telegraph.domain.repository.PhotoRepository;
import im.telegraph.service.FileService;
import im.telegraph.service.PhotoService;
import im.telegraph.util.ImageUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Ast3t1s
 */
@SpringBootTest(classes = TelegraphApplication.class)
class PhotoServiceTest {

    @Autowired
    private FileService fileService;

    @Autowired
    private PhotoService avatarService;

    @Autowired
    private ACLHashGenerator generator;

    @Autowired
    private FileRepository fileRepository;

    @Autowired
    private PhotoRepository photoRepository;

    @Test
    public void testPhoto() {

    }

    @Test
    public void test_delete_photos() throws Exception {

        Path largeFile = Paths.get("d:\\Test\\test_pic.jpg");

        byte[] rawData = Files.readAllBytes(largeFile);

        byte[] res = ImageUtils.resizeTo(rawData, 96);
        System.out.println(res.length);

        // Path to the file where the byte array will be written
        String filePath = "d:\\Test\\output.jpg";

        // Write byte array to file
        try (FileOutputStream fos = new FileOutputStream(filePath)) {
            fos.write(res);
            System.out.println("Byte array successfully written to " + filePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testUploadMultipart() throws Exception {
        /*HttpClient client = HttpClient.create().wiretap(true)
                .secure().responseTimeout(Duration.ofSeconds(30));*/

        Path largeFile = Paths.get("d:\\Test\\test_pic.jpg");

        byte[] rawData = Files.readAllBytes(largeFile);

        System.out.println(FormatDetector.detect(rawData).get());

       // List<byte[]> chunks = divideArray(rawData, 512 * 1024);

       // FileService.FileUploadUrl uploadUrl = fileService.getFileUploadUrl(rawData.length).block();

      /*  System.out.println(response.responseHeaders());
        System.out.println(response.status());
        System.out.println(response.toString());*/

       /* Flux.fromIterable(chunks).index()
                .concatMap(TupleUtils.function((index, chunk) -> fileService*.getFileUploadPartUrl(uploadUrl.getUploadKey(), index.intValue(), chunk.length)
                        .flatMap(url -> client.put().uri(url.getUrl().toExternalForm())
                                .send(new BiFunction<HttpClientRequest, NettyOutbound, Publisher<Void>>() {
                                    @Override
                                    public Publisher<Void> apply(HttpClientRequest httpClientRequest, NettyOutbound outbound) {
                                        httpClientRequest.header("Content-Type", "application/octet-stream");
                                        return outbound.sendByteArray(Mono.just(chunk));
                                    }
                                })
                                .response())
                        .doOnNext(respone -> System.out.println("Response " + respone)))).blockLast();
        FileService.FileLocation fileLocation = fileService.commitFileUpload(uploadUrl.getUploadKey(), null).block();
        System.out.println(fileLocation);*/
    }

    public static List<byte[]> divideArray(byte[] source, int chunksize) {

        List<byte[]> result = new ArrayList<>();
        int start = 0;
        while (start < source.length) {
            int end = Math.min(source.length, start + chunksize);
            result.add(Arrays.copyOfRange(source, start, end));
            start += chunksize;
        }

        return result;
    }

    @Test
    public void testUploadAvatar() throws Exception {
        fileService.uploadFile("avatar-test", Paths.get("d:\\Test\\test_image.jpg"))
                .doOnNext(location -> System.out.println(location.getAccessHash()))
                .block();

        /*Photo avatar = fileRepository.findById(2L)
                .flatMap(fileUpload -> avatarService.uploadPhoto(Peer.fromUserId(1), fileUpload.getId(),
                        generator.generateFileAccessHash(fileUpload.getId(), fileUpload.getAccessSalt())))
                .block(); */

      //  System.out.println("Uploaded avatar: " + avatar);

      //  System.out.println("Upload url: " + fileService.getFileUploadUrl(1222).block());


        /*System.out.println(fileRepository.findById(1L)
                .flatMap(fileUpload -> fileService.getFileDownloadUrl(fileUpload.getId(),
                        generator.generateFileAccessHash(fileUpload.getId(), fileUpload.getAccessSalt())))
                .block());*/

       /*  this.fileService.downloadFile(1L)
                 .collectList()
                 .map(BufferUtils::concatBuffers)
                 .doOnNext((result) -> System.out.println("Size: " + result.remaining())).block();*/
               // .doOnNext((buffer) -> System.out.println("Chunk + " + buffer))
              //  .map(buffer -> new DefaultDataBufferFactory().wrap(buffer)).blockLast();

        File downloaded = Paths.get("d:\\Test\\image").resolve("downloaded").toFile();

        WritableByteChannel channel = Channels.newChannel(new FileOutputStream(downloaded));

       // DataBufferUtils.write(flux, channel).blockLast();

//        fileService.getFileDownloadUrl(1. )
    }

}