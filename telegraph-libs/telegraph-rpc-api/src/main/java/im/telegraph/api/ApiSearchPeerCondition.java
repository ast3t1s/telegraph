package im.telegraph.api;

import static im.telegraph.engine.serialization.StreamUtils.*;

import im.telegraph.engine.serialization.StreamReader;
import im.telegraph.engine.serialization.StreamWriter;
import java.io.IOException;
import java.lang.Override;
import java.lang.String;

/**
 * Generated by the Telegraph API Scheme generator. DO NOT EDIT!
 * @author Telegraph Scheme Generator
 */
public class ApiSearchPeerCondition extends ApiSearchCondition {
    protected ApiInputPeer peer;

    public ApiSearchPeerCondition() {
    }

    public ApiSearchPeerCondition(ApiInputPeer peer) {
        this.peer = peer;
    }

    public int getHeader() {
        return 5;
    }

    public ApiInputPeer getPeer() {
        return this.peer;
    }

    public void setPeer(ApiInputPeer peer) {
        this.peer = peer;
    }

    @Override
    public void deserialize(StreamReader reader) throws IOException {
        this.peer = reader.readSerializable(1, new ApiInputPeer());
    }

    @Override
    public void serialize(StreamWriter writer) throws IOException {
        if (this.peer == null) {
            throw new IOException();
        }
        writer.writeSerializable(1, this.peer);
    }

    @Override
    public String toString() {
        String res = "struct SearchPeerCondition{";
        res += "peer=" + this.peer;
        res += "}";
        return res;
    }
}
