package im.telegraph.common.rdbms;

import io.r2dbc.spi.Result;
import org.reactivestreams.Publisher;
import reactor.core.publisher.Flux;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;

/**
 * @author Ast3t1s
 */
public final class Batch implements ResultBearing {

    private final io.r2dbc.spi.Batch batch;

    private final List<String> parts = new ArrayList<>();

    Batch(io.r2dbc.spi.Batch batch) {
        this.batch = Objects.requireNonNull(batch);
    }

    /**
     * Add a statement to this batch.
     *
     * @param sql the statement to add
     * @return this {@link Batch}
     */
    public Batch add(String sql) {
        Objects.requireNonNull(sql);
        batch.add(sql);
        return this;
    }

    public <T> Flux<T> mapResult(Function<Result, ? extends Publisher<? extends T>> mappingFunction) {
        Objects.requireNonNull(mappingFunction);
        return Flux.from(batch.execute())
                .flatMap(mappingFunction);
    }

    @Override
    public String toString() {
        return "Batch{" +
                "batch=" + batch +
                '}';
    }
} 
