package im.telegraph.internal.remote.reactor;

import im.telegraph.common.property.cluster.transport.RemoteTransportProperties;
import im.telegraph.internal.remote.*;
import im.telegraph.internal.remote.serializer.Serialization;
import io.netty.channel.Channel;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Sinks;
import reactor.netty.Connection;
import reactor.netty.ConnectionObserver;
import reactor.netty.DisposableChannel;
import reactor.netty.resources.LoopResources;
import reactor.netty.tcp.TcpClient;
import reactor.netty.tcp.TcpServer;
import reactor.util.retry.Retry;

import java.net.ConnectException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.channels.ClosedChannelException;
import java.time.Duration;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiConsumer;
import java.util.function.Predicate;

/**
 * @author Ast3t1s
 */
public class NettyRemoteTransport implements RemoteTransport {

    private static final Logger log = LoggerFactory.getLogger(NettyRemoteTransport.class);

    private final static Predicate<? super Throwable> CONNECTION_ERROR_PREDICATE = e ->
            e instanceof ClosedChannelException || e instanceof ConnectException;

    private final LoopResources loopResources = LoopResources.create("tcp-remote-transport", 1, true);

    private final AtomicBoolean started = new AtomicBoolean(false);

    private final Sinks.Many<RemoteMessage> dispatchSink = Sinks.many().multicast().directBestEffort();

    private final Map<Address, Mono<? extends Connection>> connections = new ConcurrentHashMap<>();

    private volatile DisposableChannel server;
    private final AtomicReference<Address> address = new AtomicReference<>();

    private final RemoteTransportProperties properties;
    private final Serialization serializer;

    public NettyRemoteTransport(RemoteTransportProperties properties, Serialization serializer) {
        this.properties = properties;
        this.serializer = serializer;
    }

    public RemoteTransport startAwait() {
        return start().block();
    }

    @Override
    public boolean isRunning() {
        return started.get();
    }

    @Override
    public Address address() {
        return address.get();
    }

    @Override
    public Mono<RemoteTransport> start() {
        if (isRunning()) {
            return Mono.just(this);
        }
        return Mono.defer(() -> setupTcpServer(new InetSocketAddress(properties.getPort())))
                .doOnNext(this::init)
                .doOnSuccess(t -> log.info("[{}] Bound remote transport", t.address()))
                .doOnError(ex -> log.warn("[{}] Unexpected error due starting remote transport", address, ex))
                .thenReturn(this)
                .cast(RemoteTransport.class);
    }

    private void init(DisposableChannel server) {
        this.server = server;
        this.address.set(prepareAddress(server));
        this.started.set(true);
    }

    private Address prepareAddress(DisposableChannel channel) {
        final InetSocketAddress serverAddress = (InetSocketAddress) channel.address();
        final InetAddress inetAddress = serverAddress.getAddress();
        final String host = Optional.ofNullable(properties.getExternalHost())
                .orElse(inetAddress.getHostAddress());
        final int port = Optional.ofNullable(properties.getExternalPort())
                .orElse(serverAddress.getPort());
        if (inetAddress.isAnyLocalAddress()) {
            return Address.create(Address.getLocalIpAddress().getHostAddress(), port);
        } else {
            return Address.create(host, port);
        }
    }

    @Override
    public Mono<Void> send(Address address, Object message) {
        Objects.requireNonNull(address, "address can't be null");
        Objects.requireNonNull(message, "message can't be null");

        return Mono.defer(() -> connections.computeIfAbsent(address, this::connect))
                .flatMap(conn -> conn.outbound()
                        .sendByteArray(MessageSerializer.serialize(serializer, new RemoteMessage(address(), message)))
                        .then())
                .onErrorResume(ex -> handleSendError(ex, message, address))
                .then();
    }

    private Mono<Void> handleSendError(Throwable ex, Object message, Address address) {
        if (ex instanceof RemoteTransportException) {
            return Mono.error(ex);
        }
        log.warn("Unexpected error due producing transport remote message: {} to: {}", message, address, ex);
        return Mono.error(new RemoteTransportException("Error sending transport message", ex));
    }

    @Override
    public Flux<RemoteMessage> receive() {
        return dispatchSink.asFlux().onBackpressureBuffer();
    }

    @Override
    public Mono<Void> shutdown() {
        if (!started.compareAndSet(true, false)) {
            return Mono.empty();
        }
        return Mono.defer(() -> {
            log.info("[{}] Stopping netty remote transport", address());
            // Complete incoming messages observable
            dispatchSink.tryEmitComplete();
            return Flux.concatDelayError(shutdownServer(), disposeLoopResources())
                    .then()
                    .doOnSuccess(__ -> log.info("[{}] Remote transport stopped", address()))
                    .doFinally(s -> {
                        connections.clear();
                        started.set(false);
                    });
        });
    }

    private Mono<? extends Connection> connect(Address address) {
        return setupClient(address).connect()
                .doOnSuccess(connection -> {
                    connection.onDispose().doOnTerminate(() -> connections.remove(address)).subscribe();
                    log.info("Successfully connected to: {}", address);
                })
                .onErrorResume(ex -> Mono.fromRunnable(() -> {
                    log.warn("[{}] Unexpected error due connection to {}", address(), address);
                    connections.remove(address);
                }))
                .retryWhen(connectionRetry(address))
                .cache();
    }

    private Retry connectionRetry(Address address) {
        return Retry.backoff(properties.getRetry().getMaxRetries(), Duration.ofSeconds(2))
                .maxBackoff(properties.getRetry().getBackoff())
                .filter(CONNECTION_ERROR_PREDICATE)
                .doBeforeRetry(signal -> log.debug("[{}] Reconnecting to server (attempt {}): {}",
                        address, signal.totalRetriesInARow() + 1, signal.failure().toString()));
    }

    private TcpClient setupClient(Address address) {
        TcpClient client = TcpClient.create()
                .runOn(loopResources)
                .host(address.getHost())
                .port(address.getPort())
                .option(ChannelOption.TCP_NODELAY, true)
                .option(ChannelOption.SO_KEEPALIVE, true)
                .option(ChannelOption.SO_REUSEADDR, true)
                .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, (int) properties.getConnectionTimeout().toMillis())
                .doOnChannelInit((connectionObserver, channel, remoteAddress) ->
                        new TcpChannelInitializer((int) properties.getMaxFrameSize().toBytes())
                                .accept(connectionObserver, channel));

        return address.isSecure() ? client.secure() : client;
    }

    /**
     * Bootstraps a server.
     *
     * @return a future to be completed once the server has been bound to all interfaces
     */
    private Mono<DisposableChannel> setupTcpServer(InetSocketAddress address) {
        TcpServer tcpServer = TcpServer.create()
                .runOn(loopResources)
                .bindAddress(() -> address)
                .handle((inbound, outbound) -> inbound.receive()
                        .retain().asByteArray()
                        .flatMap(message -> MessageSerializer.deserialize(serializer, message))
                        .doOnNext(dispatchSink::tryEmitNext)
                        .then())
                .doOnChannelInit((connectionObserver, channel, remoteAddress) ->
                        new TcpChannelInitializer((int) properties.getMaxFrameSize().toBytes())
                                .accept(connectionObserver, channel))
                .option(ChannelOption.SO_BACKLOG, 128)
                .option(ChannelOption.SO_REUSEADDR, true)
                .childOption(ChannelOption.TCP_NODELAY, true)
                .childOption(ChannelOption.SO_KEEPALIVE, true)
                .childOption(ChannelOption.SO_REUSEADDR, true);

        return tcpServer.bind().cast(DisposableChannel.class);
    }

    protected Mono<Void> shutdownServer() {
        return Mono.defer(() -> {
            if (server == null) {
                return Mono.empty();
            }
            log.info("[{}] Closing remote transport server", address());
            return Mono.fromRunnable(server::dispose)
                    .then(server.onDispose())
                    .doOnSuccess(__ -> log.info("[{}] Remove transport server is closed", address))
                    .doOnError(t -> log.warn("[{}] Exception due closing remote transport server", address, t));
        });
    }

    protected Mono<Void> disposeLoopResources() {
        return Mono.fromRunnable(loopResources::dispose).then(loopResources.disposeLater());
    }

    @Override
    public String toString() {
        return "NettyRemoteTransport{" + "address=" + address() + '}';
    }

    private static class TcpChannelInitializer implements BiConsumer<ConnectionObserver, Channel> {
        private static final int LENGTH_FIELD_LENGTH = 4;

        private final int maxFrameLength;

        TcpChannelInitializer(int maxFrameLength) {
            this.maxFrameLength = maxFrameLength;
        }

        @Override
        public void accept(ConnectionObserver connectionObserver, Channel channel) {
            ChannelPipeline pipeline = channel.pipeline();
            pipeline.addFirst(
                    new LengthFieldBasedFrameDecoder(
                            maxFrameLength, 0, LENGTH_FIELD_LENGTH, 0, LENGTH_FIELD_LENGTH));
            pipeline.addFirst(new LengthFieldPrepender(LENGTH_FIELD_LENGTH));
        }
    }
}
