package im.telegraph.handlers;

import im.telegraph.api.ApiAbsUser;
import im.telegraph.api.ApiUserBlocked;
import im.telegraph.api.rpc.*;
import im.telegraph.domain.entity.Id;
import im.telegraph.domain.entity.Privacy;
import im.telegraph.handlers.dispatch.DispatchContext;
import im.telegraph.handlers.internal.ApiEntityMapper;
import im.telegraph.handlers.mapper.UserConverter;
import im.telegraph.service.PrivacyService;
import im.telegraph.service.UserService;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.function.TupleUtils;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Component
public class PrivacyDispatchHandlers {

    private final PrivacyService privacyService;
    private final UserService userService;

    private final UserConverter userConverter;

    public PrivacyDispatchHandlers(PrivacyService privacyService,
                                   UserService userService,
                                   UserConverter userConverter) {
        this.privacyService = privacyService;
        this.userService = userService;
        this.userConverter = userConverter;
    }

    public Mono<ResponseBool> handleBlockUser(DispatchContext<RequestPrivacyBlockUser> context) {
        final Id userId = Id.of(context.getRequest().getPeer().getUid());
        final long accessHash = context.getRequest().getPeer().getAccessHash();

        return Mono.justOrEmpty(context.getUserId().map(Id::of))
                .filterWhen(selfId -> userService.checkAccessHash(selfId, context.getAuthId(), accessHash))
                .flatMap(selfId -> privacyService.blockUser(selfId, userId))
                .defaultIfEmpty(false)
                .map(ResponseBool::new);
    }

    public Mono<ResponseBool> handleUnblockUser(DispatchContext<RequestPrivacyUnblockUser> context) {
        final Id userId = Id.of(context.getRequest().getPeer().getUid());
        final long accessHash = context.getRequest().getPeer().getAccessHash();

        return Mono.justOrEmpty(context.getUserId().map(Id::of))
                .filterWhen(selfId -> userService.checkAccessHash(userId, context.getAuthId(), accessHash))
                .flatMap(selfId -> privacyService.unblockUser(selfId, userId))
                .defaultIfEmpty(false)
                .map(ResponseBool::new);
    }

    public Mono<ResponsePrivacyLoadBlockedUsers> handleGetBlockList(DispatchContext<RequestPrivacyLoadBlockedUsers> context) {
        final int limit = context.getRequest().getLimit();
        final int offset = context.getRequest().getOffset();

        return Mono.justOrEmpty(context.getUserId().map(Id::of))
                .flatMap(selfId -> privacyService.getBlockList(selfId, limit, offset))
                .flatMap(privacyList -> {
                    Mono<Integer> totalCount = Mono.justOrEmpty((int) privacyList.getTotalCount())
                            .defaultIfEmpty(0);

                    Mono<List<ApiUserBlocked>> blocked = Flux.fromIterable(privacyList)
                            .map(privacy -> Optional.ofNullable(ApiEntityMapper.toApiContactBlocked(privacy)))
                            .filter(Optional::isPresent)
                            .map(Optional::get)
                            .collectList()
                            .defaultIfEmpty(Collections.emptyList());

                    Mono<List<ApiAbsUser>> users = Flux.fromIterable(privacyList)
                            .map(Privacy::getBlockedId)
                            .collectList()
                            .flatMapMany(userService::getUsers)
                            .map(user -> Optional.ofNullable(userConverter.convert(context.getAuthId(), user)))
                            .filter(Optional::isPresent)
                            .map(Optional::get)
                            .collectList()
                            .defaultIfEmpty(Collections.emptyList());

                    return Mono.zip(totalCount, blocked, users);
                })
                .map(TupleUtils.function(ResponsePrivacyLoadBlockedUsers::new));
    }
}
