package im.telegraph.server.reactor;

import im.telegraph.server.Server;
import im.telegraph.server.ServerException;
import io.netty.channel.unix.Errors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.netty.ChannelBindException;
import reactor.netty.DisposableChannel;
import reactor.netty.DisposableServer;
import reactor.util.annotation.Nullable;

import java.net.InetSocketAddress;

/**
 * Basic implementation for {@link Server}.
 *
 * @author Ast3t1s
 */
abstract class NettyBaseServer implements Server {

    protected final Logger log = LoggerFactory.getLogger(getClass());

    private static final int ERROR_NO_EACCES = -13;

    @Nullable
    private volatile DisposableChannel disposable;

    @Override
    public void start() throws ServerException {
        if (this.disposable == null) {
            try {
                this.disposable = bind();
            } catch (Exception ex) {
                ServerException.ifCausedBy(ex, ChannelBindException.class, (bindException) -> {
                    if (bindException.localPort() > 0 && !isPermissionDenied(bindException.getCause())) {
                        throw new ServerException("Port " + bindException.localPort() + " is already in use",
                                bindException);
                    }
                });
                throw new ServerException("Unable to start: " + getServerName(), ex);
            }
            if (this.disposable != null) {
                log.info("*************************************************************");
                log.info("{} started at {}:{}", getServerName(), getAddress().getHostString(), getAddress().getPort());
                log.info("*************************************************************");
            }
            startAwaitThread(this.disposable);
        }
    }

    private boolean isPermissionDenied(Throwable bindExceptionCause) {
        try {
            if (bindExceptionCause instanceof Errors.NativeIoException) {
                return ((Errors.NativeIoException) bindExceptionCause).expectedErr() == ERROR_NO_EACCES;
            }
        } catch (Throwable ignored) {
        }
        return false;
    }

    @Override
    public void stop() throws ServerException {
        if (this.disposable != null) {
            this.disposable.dispose();
            this.disposable = null;
        }
    }

    protected void startAwaitThread(final DisposableChannel disposableServer) {
        Thread awaitThread = new Thread(() -> disposableServer.onDispose().block(), "server#" + hashCode());
        awaitThread.setDaemon(false);
        awaitThread.start();
    }

    /**
     * Returns the server name.
     */
    protected abstract String getServerName();

    /**
     * Bind the fully configured {@link DisposableServer}.
     */
    protected abstract DisposableChannel bind();

    @Nullable
    @Override
    public InetSocketAddress getAddress() {
        if (this.disposable != null) {
            return (InetSocketAddress) this.disposable.address();
        }
        return null;
    }
}
