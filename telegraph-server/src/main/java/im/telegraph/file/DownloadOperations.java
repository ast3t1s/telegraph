package im.telegraph.file;

import org.reactivestreams.Publisher;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.file.Path;
import java.time.Duration;
import java.time.Instant;

/**
 * A file store that only supports read operations
 *
 * @author Vsevolod Bondarenko
 */
public interface DownloadOperations {

    /**
     * Get file download url with expire time associated the provided file ky.
     *
     * @param fileKey the file key
     * @param expire the expiration time
     * @return A {@link Mono} which may contain or not an file url.
     */
    Mono<Tuple2<URL, Instant>> getDownloadFileUrl(String fileKey, Duration expire);

    /**
     * Attempt to download file with the provided file key.
     *
     * @param fileKey The file key.
     * @return A {@link Publisher} with file content.
     */
    Flux<ByteBuffer> downloadFile(String fileKey);

    /**
     * Attempt to download file to the given directory.
     *
     * @param fileKey the name of the file
     * @param destination the target directory to download
     * @return a {@link Mono} that can be used to signal a response.
     */
    Mono<Void> downloadFile(String fileKey, Path destination);

    /**
     * Attempt to download directory.
     *
     * @param fileKey the file key.
     * @param prefix the storage directory prefix
     * @param destination the target directory to download
     * @return a {@link Mono} that can be used to signal a response.
     */
    Mono<Void> downloadDirectory(String fileKey, String prefix, Path destination);
}
