^\Q/*\E$
^\Q * Copyright (c) 2021 the original author or authors.\E$
^\Q *\E$
^\Q * Permission is hereby granted, free of charge, to any person obtaining a copy\E$
^\Q * of this software and associated documentation files (the "Software"), to deal\E$
^\Q * in the Software without restriction, including without limitation the rights\E$
^\Q * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell\E$
^\Q * copies of the Software, and to permit persons to whom the Software is\E$
^\Q * furnished to do so, subject to the following conditions:\E$
^\Q *\E$
^\Q * The above copyright notice and this permission notice shall be included in all\E$
^\Q * copies or substantial portions of the Software.\E$
^\Q *\E$
^\Q * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR\E$
^\Q * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,\E$
^\Q * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE\E$
^\Q * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER\E$
^\Q * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,\E$
^\Q * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE\E$
^\Q * SOFTWARE.\E$
^\Q */\E$
^$
^.*$
