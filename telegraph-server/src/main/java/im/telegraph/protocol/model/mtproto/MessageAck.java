package im.telegraph.protocol.model.mtproto;

import im.telegraph.engine.serialization.DataInputStream;
import im.telegraph.engine.serialization.DataOutputStream;

import java.io.IOException;
import java.util.Arrays;

public class MessageAck extends ProtoStruct {

	public static final byte HEADER = (byte) 0x06;

	private long[] messagesIds;

	public MessageAck(DataInputStream inputStream) throws IOException {
		super(inputStream);
	}

	public MessageAck(long[] messagesIds) {
		this.messagesIds = messagesIds;
	}

	public long[] getMessagesIds() {
		return messagesIds;
	}

	@Override
	protected byte getHeader() {
		return HEADER;
	}

	@Override
	protected void writeBody(DataOutputStream outputStream) throws IOException {
		outputStream.writeHeader(messagesIds.length);
		for (long messageId : messagesIds) {
			outputStream.writeInt64(messageId);
		}
	}

	@Override
	protected void readBody(DataInputStream inputStream) throws IOException {
		long len = inputStream.readHeader();
		if (len < 0) {
			throw new IOException();
		}
		long[] res = new long[(int) len];
		for (int i = 0; i < res.length; i++) {
			res[i] = inputStream.readInt64();
		}
		System.arraycopy(res, 0, messagesIds, 0, res.length);
	}

	@Override
	public String toString() {
		return "Ack " + Arrays.toString(messagesIds) + "";
	}

}
