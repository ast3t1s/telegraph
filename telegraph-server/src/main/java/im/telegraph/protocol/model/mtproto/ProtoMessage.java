package im.telegraph.protocol.model.mtproto;

import im.telegraph.engine.serialization.DataInputStream;
import im.telegraph.engine.serialization.DataOutputStream;

import java.io.IOException;

public class ProtoMessage extends ProtoObject {

	private long messageId;
	private byte[] payload;

	public ProtoMessage(DataInputStream inputStream) throws IOException {
		super(inputStream);
	}

	public ProtoMessage(long messageId, byte[] payload) {
		this.messageId = messageId;
		this.payload = payload;
	}

	public long getMessageId() {
		return messageId;
	}

	public byte[] getPayload() {
		return payload;
	}

	@Override
	public void writeObject(DataOutputStream outputStream) throws IOException {
		outputStream.writeInt64(messageId);
		outputStream.writeProtoBytes(payload);
	}

	@Override
	public ProtoObject readObject(DataInputStream inputStream) throws IOException {
		this.messageId = inputStream.readInt64();
		this.payload = inputStream.readProtoBytes();
		return this;
	}

	@Override
	public String toString() {
		return "ProtoMessage [#" + messageId + "]";
	}

}
