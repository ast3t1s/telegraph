package im.telegraph.protocol;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

/**
 * A simple {@link MTProtoSessionRegistry} that tracks local {@link MTProtoSession} instances.
 */
@Component
public class MTProtoSessionRegistry {

    private static final Logger log = LoggerFactory.getLogger(MTProtoSessionRegistry.class);

    private final Map<SessionId, MTProtoSession> sessions = new ConcurrentHashMap<>();

    /**
     * Return the current {@link MTProtoSession} this registry holds for a given {@code id}.
     *
     * @param id the session ID to fetch current MTProto session.
     * @return a {@link Mono} of {@link MTProtoSession} for the given guild if present, empty otherwise.
     * */
    public Mono<MTProtoSession> getSession(SessionId id) {
        return Mono.fromCallable(() -> sessions.get(id))
                .switchIfEmpty(Mono.<MTProtoSession>empty()
                        .doOnSubscribe((s) -> log.debug("No proto context in registry to id {}", id)));
    }

    /**
     * Register a {@link MTProtoSession} for a given {@code id}, replacing any existing one.
     *
     * @param session the {@link MTProtoSession} to register.
     * @return a {@link Mono} of {@link MTProtoSession} for the given guild if present, empty otherwise.
     */
    public Mono<MTProtoSession> register(MTProtoSession session) {
        return Mono.defer(() -> Mono.justOrEmpty(session)
                .flatMap(next -> {
                    MTProtoSession previous = sessions.put(next.getId(), next);
                    if (Objects.nonNull(previous)) {
                        log.debug("Removing previous MTProto session {} from registry", previous.getId());
                        return previous.destroy().thenReturn(next);
                    }
                    return Mono.just(next);
                })
                .doFinally(signal -> log.debug("Proto context registry done after {}", signal))
                .switchIfEmpty(Mono.defer(() -> Mono.error(new IllegalStateException("Missing proto context")))));
    }

    /**
     * Get a {@link Flux} with all registered {@link MTProtoSession} instances.
     *
     * @return a {@link Flux} of {@link MTProtoSession} for if present, empty otherwise.
     */
    public Flux<MTProtoSession> getSessions() {
        return Flux.defer(() -> Flux.fromIterable(sessions.values()));
    }

    /**
     * Disconnect a {@link MTProtoSession} for given {@code id} and remove it from the registry. If no session
     * was present for the id, this method does nothing.
     *
     * @param id the session ID to disconnect and remove a MTProto session
     * @return a {@link Mono} indicating completion of the disconnection process, if an error happens it is emitted
     * through the {@link Mono}.
     */
    public Mono<Void> deregister(SessionId id) {
        return getSession(id)
                .flatMap((session) -> session.destroy()
                        .doFinally(signal -> {
                            sessions.remove(id);
                            log.debug("Proto session {} unregister after {}", id, signal);
                        }));
    }
}
