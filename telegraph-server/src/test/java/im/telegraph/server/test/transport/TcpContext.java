package im.telegraph.server.test.transport;

import im.telegraph.engine.serialization.stream.BinaryInputStream;
import im.telegraph.engine.serialization.stream.BinaryOutputStream;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.cert.X509Certificate;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class TcpContext {

    private static final int CONNECTION_TIMEOUT = 5 * 1000;

    private static final AtomicInteger contextLastId = new AtomicInteger(1);

    private final String host;
    private final int port;

    private int sentPackets;
    private int receivedPackets;

    private boolean isClosed;
    private boolean isBroken;

    private Socket socket;

    private ReaderThread readerThread;
    private WriterThread writerThread;

    private final im.telegraph.server.test.transport.TcpCallback callback;

    private final int contextId;

    TrustManager[] trustAllCerts = new TrustManager[] {
            new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }
                public void checkClientTrusted(
                        java.security.cert.X509Certificate[] certs, String authType) {
                }
                public void checkServerTrusted(
                        java.security.cert.X509Certificate[] certs, String authType) {
                }
            }
    };

    public TcpContext(String host, int port, TcpCallback callback) throws Exception {
        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, new java.security.SecureRandom());
        SSLSocketFactory socketFactory = (SSLSocketFactory) sc.getSocketFactory();

        this.contextId = contextLastId.incrementAndGet();
        this.host = host;
        this.port = port;
        this.callback = callback;
        this.socket = new Socket();
        this.socket.connect(new InetSocketAddress(host, port), CONNECTION_TIMEOUT);
        this.socket.setKeepAlive(true);
        this.socket.setTcpNoDelay(true);
        this.isBroken = false;
        this.isClosed = false;

        socket = socketFactory.createSocket(socket, host, port, true);

        this.readerThread = new ReaderThread();
        this.writerThread = new WriterThread();
        this.readerThread.start();
        this.writerThread.start();
        this.callback.onConnected(this);
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public int getSentPackets() {
        return sentPackets;
    }

    public int getReceivedPackets() {
        return receivedPackets;
    }

    public boolean isClosed() {
        return isClosed;
    }

    public boolean isBroken() {
        return isBroken;
    }

    public int getContextId() {
        return contextId;
    }

    public void postMessage(byte[] data) {
        writerThread.pushPackage(data);
    }

    private synchronized void breakContext() {
        if (!isClosed) {
            isClosed = true;
            isBroken = true;
            try {
                readerThread.interrupt();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                writerThread.interrupt();
            } catch (Exception e) {
                e.printStackTrace();

            }
        }
        callback.onBroken(this);
    }

    public synchronized void close() {
        if (!isClosed) {
            isClosed = true;
            isBroken = false;
            try {
                readerThread.interrupt();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                writerThread.interrupt();
            } catch (Exception e) {
                e.printStackTrace();
            }

            readerThread = null;
            writerThread = null;

            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                socket = null;
            }
            callback.onBroken(this);
        }
    }

    private class ReaderThread extends Thread {

        public ReaderThread() {
            setName("#Reader" + contextId);
        }

        @Override
        public void run() {
            try {
                while (!isClosed && !isInterrupted()) {
                    if (socket.isClosed()) {
                        breakContext();
                        return;
                    }
                    if (!socket.isConnected()) {
                        breakContext();
                        return;
                    }
                    InputStream inputStream = socket.getInputStream();
                    byte[] header = readBytes(inputStream, 9);
                    BinaryInputStream binaryInputStream = new BinaryInputStream(header);
                    int index = binaryInputStream.readInt();
                    int headerVal = binaryInputStream.readByte();
                    int size = binaryInputStream.readInt();

                    if (size > 1024 * 1024) {
                        throw new IOException("incorrect size");
                    }

                    byte[] body = readBytes(inputStream, size + 4);
                    BinaryOutputStream outputStream = new BinaryOutputStream();
                    outputStream.writeBytes(header, 0, header.length);
                    outputStream.writeBytes(body, 0, body.length);
                    callback.onRawMessage(outputStream.toByteArray(), TcpContext.this);
                    receivedPackets++;
                }
            } catch (Exception e) {
                e.printStackTrace();
                breakContext();
            }
        }

        private byte[] readBytes(InputStream inputStream, int count) throws IOException {
            byte[] res = new byte[count];
            int offset = 0;
            while (offset < res.length) {
                int readed = inputStream.read(res, offset, res.length - offset);
                if (readed > 0) {
                    offset += readed;
                } else if (readed < 0) {
                    throw new IOException();
                } else {
                    Thread.yield();
                }
            }
            return res;
        }

    }

    private class WriterThread extends Thread {

        private final ConcurrentLinkedQueue<byte[]> packages = new ConcurrentLinkedQueue<>();

        public WriterThread() {
            setName("#Writer" + contextId);
        }

        public void pushPackage(final byte[] packet) {
            packages.add(packet);
            synchronized (packages) {
                packages.notifyAll();
            }
        }

        @Override
        public void run() {
            try {
                while (!isBroken) {
                    byte[] packet;
                    synchronized (packages) {
                        packet = packages.poll();
                        if (packet == null) {
                            try {
                                packages.wait();
                            } catch (InterruptedException e) {
                                return;
                            }
                            packet = packages.poll();
                        }
                    }
                    if (packet == null) {
                        continue;
                    }
                    OutputStream outputStream = socket.getOutputStream();
                    outputStream.write(packet);
                    outputStream.flush();
                    sentPackets++;
                }
            } catch (Exception e) {
                e.printStackTrace();
                breakContext();
            }
        }

    }

}
