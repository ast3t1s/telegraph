package im.telegraph.domain.entity;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

public final class Presence implements Serializable {

    private static final long serialVersionUID = 1L;

    private final Long authId;
    private final Id userId;
    private final Instant lastSeenAt;

    /**
     * Constructs an {@link Presence instance} with the given params.
     *
     * @param authId     Identifier of the device represented by {@link AuthKey} id.
     * @param userId     Identifier of the user.
     * @param lastSeenAt Point in time (Unix timestamp) to expiration of the current online status.
     */
    public Presence(final Long authId, final Id userId, final Instant lastSeenAt) {
        this.authId = Objects.requireNonNull(authId);
        this.userId = Objects.requireNonNull(userId);
        this.lastSeenAt = Objects.requireNonNull(lastSeenAt);
    }

    /**
     * Factory method for updating contact expire time.
     *
     * @param lastSeenAt the new mutual state.
     */
    public Presence withLastSeen(final Instant lastSeenAt) {
        return new Presence(authId, userId, lastSeenAt);
    }

    /**
     * Gets the ID of the device.
     *
     * @return the ID of the device.
     */
    public Long getAuthId() {
        return authId;
    }

    /**
     * Gets the identifier of the user.
     *
     * @return the identifier of the user.
     */
    public Id getUserId() {
        return userId;
    }

    /**
     * Gets time to expiration of the current online status.
     *
     * @return time to expiration of the current online status.
     */
    public Instant getLastSeenAt() {
        return lastSeenAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Presence presence = (Presence) o;
        return Objects.equals(authId, presence.authId)
                && Objects.equals(userId, presence.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(authId, userId);
    }

    @Override
    public String toString() {
        return "Presence{" +
                "authId=" + authId +
                ", userId=" + userId +
                ", lastSeenAt=" + lastSeenAt +
                '}';
    }
}
