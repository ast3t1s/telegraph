package im.telegraph.common.rdbms;

import io.r2dbc.spi.Result;
import org.reactivestreams.Publisher;
import reactor.core.publisher.Flux;

import java.util.function.Function;

/**
 * A wrapper for {@link io.r2dbc.spi.Statement} providing additional convenience APIs for running updates such as {@code INSERT} and {@code DELETE}.
 *
 * @author Ast3t1s
 */
public final class Update extends StatementSpec<Update> {

    Update(Handle handle, String sql) {
        super(handle, sql);
    }

    public Flux<Long> execute() {
        return Flux
                .from(prepareStatement(this::getSql).execute())
                .flatMap(Result::getRowsUpdated);
    }

    public <T> Flux<T> execute(Function<Result, ? extends Publisher<? extends T>> mappingFunction) {
        return Flux.from(prepareStatement(this::getSql).execute()).flatMap(mappingFunction);
    }

    public ResultBearing executeAndReturnGeneratedKeys(String... generatedKeyColumnNames) {
        return new ResultBearing() {
            @Override
            public <T> Flux<T> mapResult(Function<Result, ? extends Publisher<? extends T>> mappingFunction) {
                return Flux.from(prepareStatement(() -> getSql()).returnGeneratedValues(generatedKeyColumnNames).execute())
                        .flatMap(mappingFunction);
            }
        };
    }

    @Override
    public String toString() {
        return "Update{" +
                "statement=" + getSql() +
                '}';
    }
}
