package im.telegraph.admin;

import im.telegraph.admin.object.UpdatesBean;
import im.telegraph.admin.object.UpdatesStateBean;
import im.telegraph.admin.route.AbstractRoute;
import im.telegraph.admin.route.Endpoint;
import im.telegraph.admin.route.Route;
import im.telegraph.domain.entity.Id;
import im.telegraph.service.UpdatesService;
import io.netty.handler.codec.http.HttpMethod;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.unit.DataSize;
import reactor.core.publisher.Mono;
import reactor.netty.http.server.HttpServerRequest;
import reactor.netty.http.server.HttpServerResponse;

import java.time.Instant;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

import static io.netty.handler.codec.http.HttpResponseStatus.OK;

/**
 * @author Ast3t1s
 */
public class UpdatesRouterHandler extends AbstractRoute {

    private final UpdatesService updatesService;

    @Value(value = "${telegraph.domain.updates.difference-size:60KB}")
    private DataSize maxDiffSize;

    public UpdatesRouterHandler(UpdatesService updatesService) {
        this.updatesService = updatesService;
    }

    @Override
    public List<Route> routes() {
        return List.of(
                Route.builder()
                        .endpoint(Endpoint.ofFixedPath(HttpMethod.GET, "/updates/{userId}/state"))
                        .action(this::handleGetUsersUpdatesState)
                        .corsHeaders(),
                Route.builder()
                        .endpoint(Endpoint.ofFixedPath(HttpMethod.GET, "/updates/{userId}/{seq}"))
                        .action(this::handleGetUsersUpdates)
                        .corsHeaders());
    }

    protected Mono<Void> handleGetUsersUpdatesState(HttpServerRequest request, HttpServerResponse response) {
        return Mono.justOrEmpty(Optional.ofNullable(request.param("userId")))
                .map(Integer::parseInt).map(Id::of)
                .flatMap(updatesService::findLastSeqUpdate)
                .map(seqUpdate -> new UpdatesStateBean(seqUpdate.getSequence(), seqUpdate.getTimestamp().toEpochMilli()))
                .defaultIfEmpty(new UpdatesStateBean(0, Instant.EPOCH.toEpochMilli()))
                .flatMap(updateState -> writeJson(response.status(OK), updateState));
    }

    private Mono<Void> handleGetUsersUpdates(HttpServerRequest request, HttpServerResponse response) {
        final Id userId = Optional.ofNullable(request.param("userId"))
                .map(Integer::parseInt).map(Id::of).orElse(null);
        final int sequence = Optional.ofNullable(request.param("seq"))
                .map(Integer::parseInt).orElse(0);

        return updatesService.getDifference(userId, null, sequence, maxDiffSize.toBytes())
                .flatMapIterable(UpdatesService.DiffAcc::getUpdates)
                .map(update -> new UpdatesBean(
                        update.getUserId().asLong(),
                        update.getSequence(),
                        update.getTimestamp().toEpochMilli(),
                        Base64.getEncoder().encodeToString(update.getMapping())
                ))
                .collectList()
                .flatMap(updates -> writeJson(response.status(OK), updates));
    }
}
