package im.telegraph.internal.remote.serializer;

import im.telegraph.internal.remote.MessageSerializer;

/**
 * Thrown if a {@link MessageSerializer} is unable to perform serialization or deserialization of a given object.
 *
 * @author Ast3t1s
 */
@SuppressWarnings("serial")
public class SerializationException extends RuntimeException {

    public SerializationException(String message) {
        super(message);
    }

    public SerializationException(String message, Throwable cause) {
        super(message, cause);
    }
}
