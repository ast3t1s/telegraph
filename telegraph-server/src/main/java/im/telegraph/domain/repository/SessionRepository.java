package im.telegraph.domain.repository;

import im.telegraph.domain.entity.Id;
import im.telegraph.domain.entity.SessionInfo;
import reactor.core.publisher.Mono;

import java.util.function.BiFunction;

/**
 * Repository for storing user presence status {@link SessionInfo}
 *
 * @author Ast3t1s
 */
public interface SessionRepository {

    /**
     * Saves the {@link SessionInfo} instance to storage.
     *
     * @param sessionInfo the session info to save.
     * @return A {@link Mono} where, upon successful completion, emits the {@link SessionInfo}.If an error is received,
     * it is emitted through the {@code Mono}.
     */
    Mono<SessionInfo> save(SessionInfo sessionInfo);

    /**
     * Attempt to find user's session info by user id {@code UserId}.
     *
     * @param userId the ID of the user.
     * @return A {@link Mono} where, upon successful completion, emits the {@link SessionInfo}.If an error is received,
     * it is emitted through the {@code Mono}.
     */
    Mono<SessionInfo> findById(Id userId);

    /**
     * Remove user's session info from storage.
     *
     * @param userId the ID of the user.
     * @return A {@link Mono} where, upon successful completion, emits nothing.If an error is received,
     * it is emitted through the {@code Mono}.
     */
    Mono<Void> delete(Id userId);

    /**
     * Updates the presence associated with the id of the user using remapping function. If there
     * is no associated presence the function will be called with the id and null.
     *
     * @param userId the id of the user tu update
     * @param remappingFunc function to apply
     * @return A {@link Mono} where, upon successful completion, emits the {@link SessionInfo}.If an error is received,
     * it is emitted through the {@code Mono}.
     */
    default Mono<SessionInfo> compute(Id userId, BiFunction<Id, SessionInfo, Mono<SessionInfo>> remappingFunc) {
        return findById(userId).flatMap((sessionInfo) -> remappingFunc.apply(userId, sessionInfo))
                .switchIfEmpty(Mono.defer(() -> remappingFunc.apply(userId, null))).flatMap(this::save);
    }
}
