package im.telegraph.api.update;

import static im.telegraph.engine.serialization.StreamUtils.*;

import im.telegraph.engine.serialization.StreamReader;
import im.telegraph.engine.serialization.StreamWriter;
import im.telegraph.engine.serialization.io.IOContext;
import im.telegraph.rpc.base.Update;
import java.io.IOException;
import java.lang.Override;
import java.lang.String;

/**
 * Generated by the Telegraph API Scheme generator. DO NOT EDIT!
 * @author Telegraph Scheme Generator
 */
public class UpdateChatParticipantDelete extends Update {
    public static final int CLASS_ID = 0x17;

    protected long chatId;

    protected long userId;

    public UpdateChatParticipantDelete() {
    }

    public UpdateChatParticipantDelete(long chatId, long userId) {
        this.chatId = chatId;
        this.userId = userId;
    }

    public static UpdateChatParticipantDelete fromBytes(byte[] data) throws IOException {
        return IOContext.fromByteArray(new UpdateChatParticipantDelete(), data);
    }

    public long getChatId() {
        return this.chatId;
    }

    public void setChatId(long chatId) {
        this.chatId = chatId;
    }

    public long getUserId() {
        return this.userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    @Override
    public void deserialize(StreamReader reader) throws IOException {
        this.chatId = reader.readLong(1);
        this.userId = reader.readLong(2);
    }

    @Override
    public void serialize(StreamWriter writer) throws IOException {
        writer.writeLong(1, this.chatId);
        writer.writeLong(2, this.userId);
    }

    @Override
    public String toString() {
        String res = "update ChatParticipantDelete{";
        res += "chatId=" + this.chatId;
        res += "}";
        return res;
    }

    @Override
    public int getHeader() {
        return CLASS_ID;
    }
}
