package im.telegraph.internal.remote;

import java.io.Serializable;
import java.util.Objects;

/**
 * Remote message is wrapper around the message that has come in over the wire,
 *
 *
 * @author Ast3t1s
 */
public class RemoteMessage implements Serializable {

    private static final long serialVersionUID = 1L;

    private final Address sender;
    private final Object payload;

    public RemoteMessage(Address sender, Object payload) {
        this.sender = sender;
        this.payload = payload;
    }

    /**
     * Get the {@link Address} address of the sender.
     *
     * @return the address of the sender
     */
    public Address getSender() {
        return sender;
    }

    /**
     * Get the payload of the message.
     *
     * @return the payload of the message
     */
    public Object getPayload() {
        return payload;
    }

    /**
     * Get the {@code Class<?>} of the message payload.
     *
     * @param <T> the type of the payload class
     * @return the class of the message payload
     */
    @SuppressWarnings("unchecked")
    public <T> Class<T> payloadClass() {
        if (payload == null) {
            return null;
        }
        return (Class<T>) payload.getClass();
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        RemoteMessage that = (RemoteMessage) object;
        return Objects.equals(sender, that.sender) &&
                Objects.equals(payload, that.payload);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sender, payload);
    }

    @Override
    public String toString() {
        return "RemoteMessage: " + payloadClass() + " from " + sender;
    }
}
