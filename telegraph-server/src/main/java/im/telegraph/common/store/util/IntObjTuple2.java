package im.telegraph.common.store.util;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import reactor.util.annotation.NonNull;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuples;

import java.util.*;

/**
 * Copy of Reactor's Tuple2 but accepting a int as the first object.
 *
 * @param <T> The second object type.
 * @see IntObjTuple2#of(int, Object)
 * @see Tuple2
 */
public class IntObjTuple2 <T> implements Iterable<Object>, Comparable<IntObjTuple2<T>> {
    // copied from reactor.util.function.Tuple2, it would be extended instead but it has a private constructor

    /**
     * Create a {@link IntObjTuple2} with the given objects.
     *
     * @param t1 The first value in the tuple. Not null.
     * @param t2 The second value in the tuple. Not null.
     * @param <T> The type of the second value.
     * @return The new {@link IntObjTuple2}.
     */
    public static <T> IntObjTuple2<T> of(int t1, T t2) {
        return new IntObjTuple2<>(t1, t2);
    }

    /**
     * Converts a {@link Tuple2} to a {@link IntObjTuple2}.
     *
     * @param tuple2 The {@link Tuple2} to convert.
     * @param <T> The type of the second value.
     * @return The new converted {@link IntObjTuple2}.
     */
    public static <T> IntObjTuple2<T> from(Tuple2<Integer, T> tuple2) {
        return of(tuple2.getT1(), tuple2.getT2());
    }

    /**
     * Converts a {@link LongObjTuple2} to a {@link Tuple2}.
     *
     * @param tuple The {@link LongObjTuple2} to convert.
     * @param <T> The type of the second value.
     * @return The new converted {@link Tuple2}.
     */
    public static <T> Tuple2<Integer, T> convert(IntObjTuple2<T> tuple) {
        return Tuples.of(tuple.getT1(), tuple.getT2());
    }

    private static final long serialVersionUID = 6977984978741213834L;

    final int t1;
    @NonNull
    final T t2;

    @JsonCreator
    IntObjTuple2(@JsonProperty("t1") int t1, @JsonProperty("t2") T t2) {
        this.t1 = t1;
        this.t2 = Objects.requireNonNull(t2, "t2");
    }

    /**
     * Type-safe way to get the fist object of this {@link Tuples}.
     *
     * @return The first object
     */
    public int getT1() {
        return t1;
    }

    /**
     * Type-safe way to get the second object of this {@link Tuples}.
     *
     * @return The second object
     */
    public T getT2() {
        return t2;
    }


    /**
     * Get the object at the given index.
     *
     * @param index The index of the object to retrieve. Starts at 0.
     * @return The object or {@literal null} if out of bounds.
     */
    public Object get(int index) {
        switch (index) {
            case 0:
                return t1;
            case 1:
                return t2;
            default:
                return null;
        }
    }

    /**
     * Turn this {@literal Tuples} into a plain Object list.
     *
     * @return A new Object list.
     */
    public List<Object> toList() {
        return Arrays.asList(toArray());
    }

    /**
     * Turn this {@literal Tuples} into a plain Object array.
     *
     * @return A new Object array.
     */
    public Object[] toArray() {
        return new Object[]{t1, t2};
    }

    @Override
    public Iterator<Object> iterator() {
        return Collections.unmodifiableList(toList()).iterator();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || (getClass() != o.getClass() && !o.getClass().equals(Tuple2.class))) {
            return false;
        }

        if (o.getClass().equals(Tuple2.class)) {
            Tuple2<?, ?> tuple2 = (Tuple2<?, ?>) o;

            return tuple2.getT1().equals(t1) && t2.equals(tuple2.getT2());
        } else {
            IntObjTuple2<?> tuple2 = (IntObjTuple2<?>) o;

            return tuple2.t1 == t1 && t2.equals(tuple2.getT2());
        }
    }

    @Override
    public int hashCode() {
        int result = size();
        result = 31 * result + Long.hashCode(t1);
        result = 31 * result + t2.hashCode();
        return result;
    }

    /**
     * Return the number of elements in this {@literal Tuples}.
     *
     * @return The size of this {@literal Tuples}.
     */
    public int size() {
        return 2;
    }

    /**
     * A Tuple String representation is the comma separated list of values, enclosed
     * in square brackets.
     *
     * @return the Tuple String representation
     */
    @Override
    public final String toString() {
        return tupleStringRepresentation(toArray()).insert(0, '[').append(']').toString();
    }

    /**
     * Prepare a string representation of the values suitable for a Tuple of any
     * size by accepting an array of elements. This builds a {@link StringBuilder}
     * containing the String representation of each object, comma separated. It manages
     * nulls as well by putting an empty string and the comma.
     *
     * @param values the values of the tuple to represent
     * @return a {@link StringBuilder} initialized with the string representation of the
     * values in the Tuple.
     */
    static StringBuilder tupleStringRepresentation(Object... values) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < values.length; i++) {
            Object t = values[i];
            if (i != 0) {
                sb.append(',');
            }
            if (t != null) {
                sb.append(t);
            }
        }
        return sb;
    }

    @Override
    public int compareTo(IntObjTuple2<T> o) { //Considers first object to have priority
        if (o.t1 != t1) {
            return t1 < o.t1 ? -1 : 1;
        }

        if (t2 instanceof Comparable) {
            return ((Comparable) t2).compareTo(o.t2);
        }

        return 0;
    }
} 
