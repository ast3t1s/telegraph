package im.telegraph.api;

import static im.telegraph.engine.serialization.StreamUtils.*;

import im.telegraph.engine.serialization.StreamReader;
import im.telegraph.engine.serialization.StreamWriter;
import java.io.IOException;
import java.lang.Override;
import java.lang.String;

/**
 * Generated by the Telegraph API Scheme generator. DO NOT EDIT!
 * @author Telegraph Scheme Generator
 */
public class ApiSearchSenderIdConfition extends ApiSearchCondition {
    protected long senderId;

    public ApiSearchSenderIdConfition() {
    }

    public ApiSearchSenderIdConfition(long senderId) {
        this.senderId = senderId;
    }

    public int getHeader() {
        return 7;
    }

    public long getSenderId() {
        return this.senderId;
    }

    public void setSenderId(long senderId) {
        this.senderId = senderId;
    }

    @Override
    public void deserialize(StreamReader reader) throws IOException {
        this.senderId = reader.readLong(1);
    }

    @Override
    public void serialize(StreamWriter writer) throws IOException {
        writer.writeLong(1, this.senderId);
    }

    @Override
    public String toString() {
        String res = "struct SearchSenderIdConfition{";
        res += "senderId=" + this.senderId;
        res += "}";
        return res;
    }
}
