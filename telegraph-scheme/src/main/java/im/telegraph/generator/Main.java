package im.telegraph.generator;

import im.telegraph.generator.generator.CompositeGenerator;
import im.telegraph.generator.generator.javapoet.*;
import im.telegraph.generator.type.SchemeDefinition;
import im.telegraph.generator.type.SchemeFactory;

import java.net.URL;

public class Main {

    public static void main(String[] args) throws Exception {
        ClassLoader classLoader = new Main().getClass().getClassLoader();
        URL resource = classLoader.getResource("actor.json");
        SchemeDefinition definition = SchemeFactory.fromFile(resource.toURI().getPath());

        CompositeGenerator generator = CompositeGenerator.builder()
                .add(new RpcGenerator())
                .add(new EnumGenerator())
                .add(new StructGenerator())
                .add(new UpdateGenerator())
                .add(new RpcParserGenerator())
                .add(new UpdateParserGenerator())
                .add(new UpdateBoxGenerator())
                .build();
        generator.generate(definition, "D:\\Pets\\telegraph-im\\telegraph-libs\\telegraph-rpc-api\\src\\main\\java");
        //generator.generate(definition, "D:\\Test\\rpc");

    }
} 
