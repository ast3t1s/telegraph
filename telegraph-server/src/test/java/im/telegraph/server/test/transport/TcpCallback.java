package im.telegraph.server.test.transport;

public interface TcpCallback {

    void onConnected(TcpContext context);

    void onRawMessage(byte[] data, TcpContext context);

    void onBroken(TcpContext context);

}
