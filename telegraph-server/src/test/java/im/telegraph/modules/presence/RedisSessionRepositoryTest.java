package im.telegraph.modules.presence;

import im.telegraph.TelegraphApplication;
import im.telegraph.domain.entity.Id;
import im.telegraph.domain.entity.SessionInfo;
import im.telegraph.domain.repository.redis.RedisSessionRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import reactor.test.StepVerifier;

import java.time.Duration;
import java.time.Instant;
import java.util.Collections;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Ast3t1s
 */
@SpringBootTest(classes = TelegraphApplication.class)
class RedisSessionRepositoryTest {

    @Autowired
    private RedisSessionRepository sessionRepository;

    @Test
    void test_saveSession() {
      /*  UserId userId = UserId.of(1);
        Instant expireAt = Instant.now().plus(Duration.ofMinutes(5));
        UserDevice firstDevice = UserDevice.of(1L, "node-1", expireAt);
        UserDevice secondDevice = UserDevice.of(2L, "node-2", expireAt);

        SessionInfo sessionInfo = new SessionInfo(userId, Map.of(firstDevice.getAuthId(), firstDevice,
                secondDevice.getAuthId(), secondDevice));
        sessionRepository.save(sessionInfo).block();

        StepVerifier.create(sessionRepository.findById(userId))
                .expectSubscription()
                .assertNext(retrieved -> {
                    assertThat(retrieved.getUserId()).isEqualTo(sessionInfo.getUserId());
                    assertThat(retrieved.getDevices().size()).isEqualTo(sessionInfo.getDevices().size());
                    assertThat(retrieved.getDevices()).isEqualTo(sessionInfo.getDevices());
                })
                .verifyComplete();*/
    }

    @Test
    void test_saveEmptySession() {
        Id userId = Id.of(1);

        SessionInfo sessionInfo = new SessionInfo(userId, Collections.emptyMap());
        sessionRepository.save(sessionInfo).block();

        StepVerifier.create(sessionRepository.findById(userId))
                .expectSubscription()
                .expectComplete()
                .verify();
    }

    @Test
    void test_updateSession() {
        Id userId = Id.of(1);
        Instant expireAt = Instant.now().plus(Duration.ofMinutes(5));
        SessionInfo.UserDevice firstDevice = new SessionInfo.UserDevice(1L, "node-1", expireAt);

        SessionInfo sessionInfo = new SessionInfo(userId, Map.of(firstDevice.getAuthId(), firstDevice));
        sessionRepository.save(sessionInfo).block();

        SessionInfo updatedSession = sessionInfo.addDevice(2L, "node-2", expireAt);

        sessionRepository.save(updatedSession).block();

        StepVerifier.create(sessionRepository.findById(userId))
                .expectSubscription()
                .assertNext(retrieved -> {
                    assertThat(retrieved.getUserId()).isEqualTo(sessionInfo.getUserId());
                    assertThat(retrieved.getDevices().size()).isEqualTo(2);
                })
                .verifyComplete();
    }

    @Test
    void test_deleteSession() {
        Id userId = Id.of(1);
        Instant expireAt = Instant.now().plus(Duration.ofMinutes(5));
        SessionInfo.UserDevice firstDevice = new SessionInfo.UserDevice(1L, "node-1", expireAt);
        SessionInfo.UserDevice secondDevice = new SessionInfo.UserDevice(2L, "node-2", expireAt);

        SessionInfo sessionInfo = new SessionInfo(userId, Map.of(firstDevice.getAuthId(), firstDevice,
                secondDevice.getAuthId(), secondDevice));
        sessionRepository.save(sessionInfo).block();

        sessionRepository.delete(userId).block();

        StepVerifier.create(sessionRepository.findById(userId))
                .expectSubscription()
                .expectComplete()
                .verify();
    }
}