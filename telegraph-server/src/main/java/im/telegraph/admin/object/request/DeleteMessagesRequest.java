package im.telegraph.admin.object.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ast3t1s
 */
public class DeleteMessagesRequest {

    @JsonProperty("ids")
    private List<Long> rids = new ArrayList<>();

    public List<Long> getRids() {
        return rids;
    }

    @Override
    public String toString() {
        return "DeleteMessagesRequest{" +
                "rids=" + rids +
                '}';
    }
}
