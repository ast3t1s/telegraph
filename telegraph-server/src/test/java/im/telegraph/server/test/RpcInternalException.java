package im.telegraph.server.test;

public class RpcInternalException extends RpcException {

    public RpcInternalException() {
        super("INTERNAL_ERROR", 500, "Internal server error");
    }

}
