package im.telegraph.admin.object.request;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Ast3t1s
 */
public class SendMessageRequest {

    private long peer;
    @JsonProperty("random_id")
    private Long randomId;
    @JsonProperty("sender_id")
    private Integer userId;
    private String text;

    public long getPeer() {
        return peer;
    }

    public Long getRandomId() {
        return randomId;
    }

    public Integer getUserId() {
        return userId;
    }

    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        return "SendMessageRequest{" +
                "peer=" + peer +
                ", randomId=" + randomId +
                ", userId=" + userId +
                ", text='" + text + '\'' +
                '}';
    }
}
