package im.telegraph.config;

import im.telegraph.cluster.AccrualFailureDetector;
import im.telegraph.cluster.Cluster;
import im.telegraph.cluster.ClusterMessage;
import im.telegraph.cluster.FailureDetector;
import im.telegraph.cluster.serialization.BinaryClusterMessageSerializer;
import im.telegraph.common.property.cluster.ClusterProperties;
import im.telegraph.domain.event.Event;
import im.telegraph.internal.remote.RemoteTransport;
import im.telegraph.internal.remote.reactor.NettyRemoteTransport;
import im.telegraph.internal.remote.serializer.JdkSerializer;
import im.telegraph.internal.remote.serializer.Serialization;
import im.telegraph.pubsub.PubSubMessage;
import im.telegraph.pubsub.serialization.BinaryDistributedPubSubMessageSerializer;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

/**
 * @author Ast3t1s
 */
@Configuration
public class ClusterConfiguration implements ApplicationListener<ApplicationReadyEvent> {

    private final ClusterProperties properties;

    public ClusterConfiguration(TelegraphProperties properties) {
        this.properties = properties.getCluster();
    }

    @Bean
    public Serialization serialization() {
        Serialization payloadSerialization = Serialization.builder()
                .registerSerializer(Event.class, new JdkSerializer())
                .build();

        return Serialization.builder()
                .registerSerializer(ClusterMessage.class, new BinaryClusterMessageSerializer())
                .registerSerializer(PubSubMessage.class, new BinaryDistributedPubSubMessageSerializer(payloadSerialization))
                .build();
    }

    @Bean
    public RemoteTransport transport(Serialization serializer) {
        return new NettyRemoteTransport(properties.getTransport(), serializer).startAwait();
    }

    @Bean
    public FailureDetector failureDetector() {
        return new AccrualFailureDetector(properties.getFailureDetector().getThreshold(),
                properties.getFailureDetector().getMaxSampleSize(),
                properties.getFailureDetector().getMinStdDeviation(),
                properties.getFailureDetector().getAcceptableHeartbeatPause(),
                properties.getFailureDetector().getAcceptableHeartbeatPause());
    }

    @Bean
    @Order(100)
    public Cluster cluster(FailureDetector failureDetector, RemoteTransport transport) {
        return new Cluster(properties, failureDetector, transport);
    }

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        Cluster cluster = event.getApplicationContext().getBean(Cluster.class);
        cluster.join().subscribe();
    }
}
