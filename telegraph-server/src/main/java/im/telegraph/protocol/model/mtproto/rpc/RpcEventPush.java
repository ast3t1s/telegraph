package im.telegraph.protocol.model.mtproto.rpc;

import im.telegraph.engine.serialization.DataInputStream;
import im.telegraph.engine.serialization.DataOutputStream;
import im.telegraph.protocol.model.mtproto.ProtoStruct;

import java.io.IOException;

public class RpcEventPush extends ProtoStruct {

	public static final byte HEADER = (byte) 0x00;

	private int updateType;

	private byte[] body;

	public RpcEventPush(DataInputStream inputStream) throws IOException {
		super(inputStream);
	}

	public RpcEventPush(int updateType, byte[] body) {
		this.updateType = updateType;
		this.body = body;
	}

	public int getUpdateType() {
		return updateType;
	}

	public byte[] getBody() {
		return body;
	}

	@Override
	protected byte getHeader() {
		return HEADER;
	}

	@Override
	protected void writeBody(DataOutputStream outputStream) throws IOException {
		outputStream.writeInt32(updateType);
		outputStream.writeProtoBytes(body);
	}

	@Override
	protected void readBody(DataInputStream inputStream) throws IOException {
		this.updateType = inputStream.readInt();
		this.body = inputStream.readProtoBytes();
	}

	@Override
	public String toString() {
		return "Update[" + updateType + "]";
	}

}
