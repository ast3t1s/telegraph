package im.telegraph.service;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import im.telegraph.common.*;
import im.telegraph.common.property.domain.ChatProperties;
import im.telegraph.common.store.util.IntIntTuple2;
import im.telegraph.config.TelegraphProperties;
import im.telegraph.domain.entity.Id;
import im.telegraph.domain.entity.chat.*;
import im.telegraph.domain.repository.ChatParticipantRepository;
import im.telegraph.domain.repository.ChatRepository;
import im.telegraph.exceptions.RpcErrors;
import im.telegraph.exceptions.RpcException;
import im.telegraph.util.PaginationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.function.TupleUtils;
import reactor.util.annotation.Nullable;

import java.time.Instant;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.function.BiFunction;
import java.util.function.Function;

@Service
public class ChatService {

    private static final Logger log = LoggerFactory.getLogger(ChatService.class);

    private final ChatRepository chatRepository;
    private final ChatParticipantRepository chatParticipantRepository;
    private final PrivacyService privacyService;
    private final UserService userService;
    private final IdGeneratorManager idGenerator;
    private final ACLHashGenerator hashGenerator;

    private final StateHolder stateHolder;
    private final DateTimeFactory dateFactory;
    private final ChatProperties chatProperties;

    private final Cache<Id, List<ChatParticipant>> participantsCache = Caffeine.newBuilder()
            .expireAfterWrite(60, TimeUnit.SECONDS)
            .build();

    public ChatService(ChatRepository chatRepository,
                       ChatParticipantRepository chatParticipantRepository,
                       PrivacyService privacyService,
                       UserService userService,
                       IdGeneratorManager idGenerator,
                       ACLHashGenerator hashGenerator,
                       StateHolder stateHolder,
                       DateTimeFactory dateFactory,
                       TelegraphProperties properties) {
        this.chatRepository = chatRepository;
        this.chatParticipantRepository = chatParticipantRepository;

        this.privacyService = privacyService;
        this.userService = userService;
        this.idGenerator = idGenerator;
        this.hashGenerator = hashGenerator;
        this.stateHolder = stateHolder;
        this.dateFactory = dateFactory;
        this.chatProperties = properties.getDomain().getChat();
    }

    /**
     * Create a new group chat.
     *
     * @param createById  Identifier of the user, who create the chat.
     * @param title       Title of the new basic chat; 1-128 characters.
     * @param description New chat description; 0 - 255 characters.
     * @return A {@link Mono} where, upon successful completion, emits the created {@link Chat}. If an error is
     * received, it is emitted through the {@code Mono}.
     */
    public Mono<Chat> create(final Id createById, final String title, @Nullable final String description,
                             final PermissionSet permissions) {
        if (!StringUtils.hasText(title) || title.length() > chatProperties.getMaxTitleLength())
            return Mono.error(() -> RpcException.create(RpcErrors.CHAT_TITLE_INVALID));

        if (StringUtils.hasText(description) && description.trim().length() > chatProperties.getMaxAboutLength())
            return Mono.error(() -> RpcException.create(RpcErrors.CHAT_ABOUT_TOO_LONG));

        final PermissionSet defaultPermissions = Optional.ofNullable(permissions)
                .orElse(PermissionSet.of(Permission.SHOW_JOIN_LEAVE_MESSAGE, Permission.SHOW_ADMINS_TO_MEMBERS));

        final Instant now = dateFactory.getClock().instant();

        final Chat entity = Chat.builder()
                .id(idGenerator.generateEntityId(Chat.ENTITY_NAME))
                .title(title)
                .accessSalt(hashGenerator.nextAccessSalt())
                .ownerUserId(createById)
                .permissions(defaultPermissions.getRawValue())
                .createdAt(now)
                .updatedAt(now)
                .updatedById(createById)
                .membersCount(1)
                .build();

        final Function<Chat, Mono<Void>> saveOwner = chat -> Mono.fromCallable(
                () -> ChatParticipant.builder()
                        .chatId(chat.getId())
                        .userId(createById)
                        .inviterId(createById)
                        .status(ChatParticipant.Status.OWNER)
                        .createdAt(now)
                        .build())
                .flatMap(chatParticipantRepository::insert);

        return chatRepository.insert(entity)
                .flatMap(chat -> saveOwner.apply(chat).thenReturn(chat))
                .doOnSuccess(chat -> log.debug("New chat: '[{}]' successfully created.", chat))
                .flatMap(toCache -> stateHolder.getChatStore().save(toCache.getId().asLong(), toCache));
    }

    /**
     * Delete a chat.
     *
     * @param chatId     the ID of the chat.
     * @param deleteById the ID of the chat owner.
     * @return A {@link Mono} where, upon successful completion, emits nothing; indicating the chat has been deleted.
     * If an error is received, it is emitted through the {@code Mono}.
     */
    public Mono<Void> delete(final Id chatId, final Id deleteById) {
        return chatRepository.findById(chatId)
                .switchIfEmpty(Mono.error(RpcException.create(RpcErrors.CHAT_INVALID)))
                .flatMap(chat -> chatParticipantRepository.findByChatIdAndUserId(chatId, deleteById)
                        .switchIfEmpty(Mono.error(RpcException.create(RpcErrors.USER_NOT_PARTICIPANT)))
                        .flatMap(participant -> {
                            if (!canDeleteChat(participant)) {
                                return Mono.error(RpcException.create(RpcErrors.NO_PERMISSION));
                            }
                            return chatParticipantRepository.delete(participant)
                                    .then(chatRepository.deleteById(chat.getId()))
                                    .then(stateHolder.getChatStore().remove(chatId.asLong()));
                        }));
    }

    /**
     * Get information about the chat by its identifier.
     *
     * @param chatId Identifier of the chat.
     * @return A {@link Mono} where, upon successful completion, emits the {@link Chat}. If an error is
     * received, it is emitted through the {@code Mono}.
     */
    public Mono<Chat> getChat(final Id chatId) {
        return stateHolder.getChatStore().find(chatId.asLong())
                .switchIfEmpty(chatRepository.findById(chatId)
                        .flatMap(chat -> stateHolder.getChatStore().save(chat.getId().asLong(), chat)));
    }

    /**
     * Get information about the chats by the given identifiers.
     *
     * @param ids the identifiers of the chats.
     * @return A {@link Flux} that continually emits the {@link Chat chats}. If an error is received,
     * it is emitted through the {@code Flux}.
     */
    public Flux<Chat> getChats(final Iterable<Id> ids) {
        return Flux.fromIterable(ids)
                .distinct()
                .buffer(100)
                .flatMap(chatRepository::findAllById)
                .flatMap(chat -> stateHolder.getChatStore().save(chat.getId().asLong(), chat));
    }

    /**
     * Changes the chat title. Requires {@link Permission#CAN_ADMINS_EDIT_GROUP_INFO} permissions if user is not owner.
     *
     * @param chatId     Identifier of the chat.
     * @param updateById Identifier of the user, who try to update chat title.
     * @param newTitle   New title of the chat; 1 - 128 characters.
     * @return A {@link Mono} where, upon successful completion, emits nothing; indicating the chat title has been edited.
     * If an error is received, it is emitted through the {@code Mono}.
     */
    public Mono<Void> editChatTitle(final Id chatId, final Id updateById, final String newTitle) {
        if (!StringUtils.hasText(newTitle) || newTitle.length() > chatProperties.getMaxTitleLength())
            return Mono.error(() -> RpcException.create(RpcErrors.CHAT_TITLE_INVALID));

        return chatRepository.findById(chatId)
                .switchIfEmpty(Mono.error(RpcException.create(RpcErrors.CHAT_INVALID)))
                .flatMap(chat -> chatParticipantRepository.findByChatIdAndUserId(chatId, updateById)
                        .switchIfEmpty(Mono.error(RpcException.create(RpcErrors.USER_NOT_PARTICIPANT)))
                        .flatMap(participant -> {
                            if (!canEditChatInfo(participant, chat.getPermissionsSet())) {
                                return Mono.error(RpcException.create(RpcErrors.NO_PERMISSION));
                            }

                            final Chat toUpdateChat = chat.toBuilder()
                                    .title(newTitle)
                                    .updatedById(updateById)
                                    .updatedAt(dateFactory.getClock().instant())
                                    .build();

                            return chatRepository.update(toUpdateChat)
                                    .flatMap(toCache -> stateHolder.getChatStore()
                                            .save(toCache.getId().asLong(), toCache));
                        })
                        .then());
    }

    /**
     * Change information about a chat. Requires {@link Permission#CAN_ADMINS_EDIT_GROUP_INFO} permission.
     *
     * @param chatId      Identifier of the chat.
     * @param updateById  Identifier of the user, who try to update description.
     * @param description New chat description; 0 - 255 characters.
     * @return A {@link Mono} where, upon successful completion, emits nothing; indicating the chat description
     * has been edited. If an error is received, it is emitted through the {@code Mono}.
     */
    public Mono<Chat> editChatAbout(final Id chatId, final Id updateById, @Nullable final String description) {
        if (StringUtils.hasText(description) && description.trim().length() > chatProperties.getMaxAboutLength()) {
            return Mono.error(() -> RpcException.create(RpcErrors.CHAT_ABOUT_TOO_LONG));
        }

        return chatRepository.findById(chatId)
                .switchIfEmpty(Mono.error(RpcException.create(RpcErrors.CHAT_INVALID)))
                .flatMap(chat -> chatParticipantRepository.findByChatIdAndUserId(chatId, updateById)
                        .switchIfEmpty(Mono.error(RpcException.create(RpcErrors.USER_NOT_PARTICIPANT)))
                        .flatMap(participant -> {
                            if (!canEditChatInfo(participant, chat.getPermissionsSet())) {
                                return Mono.error(RpcException.create(RpcErrors.NO_PERMISSION));
                            }

                            final Chat toUpdateChat = chat.toBuilder()
                                    .description(description)
                                    .updatedById(updateById)
                                    .updatedAt(dateFactory.getClock().instant())
                                    .build();

                            return chatRepository.update(toUpdateChat)
                                    .flatMap(toCache -> stateHolder.getChatStore()
                                            .save(toCache.getId().asLong(), toCache));
                        }));
    }

    /**
     * Changes the of the chat. Requires {@link Permission#CAN_ADMINS_EDIT_GROUP_INFO} permission for admins.
     *
     * @param chatId     Identifier of the chat.
     * @param updateById Identifier of the user, who try to update chat photo.
     * @param chatPhoto     New chat photo; pass null to delete chat photo.
     * @return A {@link Mono} where, upon successful completion, emits nothing; indicating the chat photo has been edited.
     * If an error is received, it is emitted through the {@code Mono}.
     */
    public Mono<Void> editChatPhoto(final Id chatId, final Id updateById, @Nullable final ChatPhoto chatPhoto) {
        return chatRepository.findById(chatId)
                .switchIfEmpty(Mono.error(RpcException.create(RpcErrors.CHAT_INVALID)))
                .flatMap(chat -> chatParticipantRepository.findByChatIdAndUserId(chat.getId(), updateById)
                        .switchIfEmpty(Mono.error(RpcException.create(RpcErrors.USER_NOT_PARTICIPANT)))
                        .flatMap(participant -> {
                            if (!canEditChatInfo(participant, chat.getPermissionsSet())) {
                                return Mono.error(RpcException.create(RpcErrors.NO_PERMISSION));
                            }

                            final Chat toUpdateChat = chat.toBuilder()
                                    .photo(chatPhoto)
                                    .updatedById(updateById)
                                    .updatedAt(dateFactory.getClock().instant())
                                    .build();

                            return chatRepository.update(toUpdateChat);
                        })
                        .flatMap(updated -> stateHolder.getChatStore().save(updated.getId().asLong(), updated))
                        .then());
    }

    /**
     * Adds a new user to a chat. Require {@link Permission#CAN_MEMBERS_INVITE} permission.
     *
     * @param chatId    Identifier of the chat.
     * @param userId    Identifier of the user to be added.
     * @param inviterId Identifier of the user which invites to a chat.
     * @return A {@link Mono} where, upon successful completion, emits nothing; indicating the specified user was added
     * to the chat. If an error is received, it is emitted through the {@code Mono}.
     */
    public Mono<Void> addChatUser(final Id chatId, final Id userId, final Id inviterId) {
        return chatRepository.findById(chatId)
                .switchIfEmpty(Mono.error(RpcException.create(RpcErrors.CHAT_INVALID)))
                .flatMap(chat -> {
                    if (chat.getMembersCount() >= chatProperties.getMaxParticipants()) {
                        return Mono.error(() -> RpcException.create(RpcErrors.USERS_TOO_MUCH));
                    }

                    Mono<Void> isUserActive = userService.findById(userId)
                            .switchIfEmpty(Mono.error(() -> RpcException.create(RpcErrors.USER_ID_INVALID)))
                            .filter(user -> !user.getEnabled() || user.getDeletedAt().isPresent())
                            .flatMap(__ -> Mono.error(RpcException.create(RpcErrors.USER_DEACTIVATED)))
                            .then();

                    Mono<Void> isInvitedBanned = privacyService.isUserBlocked(userId, inviterId)
                            .filter(banned -> banned)
                            .flatMap(__ -> Mono.error(RpcException.create(RpcErrors.USER_IS_BLOCKED)));

                    Mono<Void> canInviteParticipants = chatParticipantRepository.findByChatIdAndUserId(chatId, inviterId)
                            .filter(inviter -> canInviteNewMembers(inviter, chat.getPermissionsSet()))
                            .switchIfEmpty(Mono.error(RpcException.create(RpcErrors.NO_PERMISSION)))
                            .then();

                    ChatParticipant newChatMember = ChatParticipant.builder()
                            .chatId(chatId)
                            .userId(userId)
                            .inviterId(inviterId)
                            .status(ChatParticipant.Status.PARTICIPANT)
                            .createdAt(dateFactory.getClock().instant())
                            .build();

                    Mono<Void> saveChatMember = chatParticipantRepository.insert(newChatMember)
                           /* .onErrorMap(DataIntegrityViolationException.class,
                            ex -> RpcException.create(RpcErrors.ALREADY_PARTICIPANT))*/
                            .then(stateHolder.getChatParticipantsStore()
                                    .save(IntIntTuple2.of(newChatMember.getChatId().asLong(),
                                            newChatMember.getUserId().asLong()), newChatMember))
                            .then();

                    return Mono.when(isUserActive, isInvitedBanned, canInviteParticipants, saveChatMember)
                            .thenReturn(chat);
                })
                .map(chat -> chat.toBuilder()
                        .membersCount(chat.getMembersCount() + 1)
                        .build())
                .flatMap(chatRepository::update)
                .flatMap(updated -> stateHolder.getChatStore().save(updated.getId().asLong(), updated))
                .then();
    }

    /**
     * Delete the specified user from the given chat. Requires admin permissions.
     *
     * @param chatId       Identifier of the chat.
     * @param userId       Identifier of the user to kick from the chat.
     * @param deletedBy   Identifier of the user user who kick.
     * @return A {@link Mono} where, upon successful completion, emits nothing; indicating the specified user was kicked
     * from the chat. If an error is received, it is emitted through the {@code Mono}.
     */
    public Mono<Void> deleteChatUser(final Id chatId, final Id userId, final Id deletedBy) {
        return chatRepository.findById(chatId)
                .switchIfEmpty(Mono.error(RpcException.create(RpcErrors.CHAT_INVALID)))
                .flatMap(chat -> chatParticipantRepository.findByChatIdAndUserId(chat.getId(), deletedBy)
                        .switchIfEmpty(Mono.error(RpcException.create(RpcErrors.USER_NOT_PARTICIPANT)))
                        .flatMap(participant -> {
                            if (Objects.equals(userId, deletedBy)) {
                                if (!canMemberLeaveChat(participant)) {
                                    return Mono.error(() -> RpcException.create(RpcErrors.NO_PERMISSION));
                                }
                            } else {
                                if (!canKickMembers(participant)) {
                                    return Mono.error(RpcException.create(RpcErrors.NO_PERMISSION));
                                }
                            }
                            return chatParticipantRepository.findByChatIdAndUserId(chatId, userId)
                                    .flatMap(chatParticipantRepository::delete);
                        })
                        .then(stateHolder.getChatParticipantsStore()
                                .remove(IntIntTuple2.of(chatId.asLong(), deletedBy.asLong())))
                        .thenReturn(chat))
                .map(chat -> chat.toBuilder()
                        .membersCount(chat.getMembersCount() - 1)
                        .build())
                .flatMap(chatRepository::update)
                .then();
    }

    /**
     * Makes the given user admin.
     *
     * @param chatId      Identifier of the chat.
     * @param userId      Identifier of the user.
     * @param updatedById Identifier of the chat owner.
     * @return A {@link Mono} where, upon successful completion, emits nothing; indicating the specified user has gotten
     * admin privileges in the chat. If an error is received, it is emitted through the {@code Mono}.
     */
    public Mono<Void> editChatAdmin(final Id chatId, final Id userId, final Id updatedById, final boolean admin) {
        return chatRepository.findById(chatId)
                .switchIfEmpty(Mono.error(RpcException.create(RpcErrors.CHAT_INVALID)))
                .flatMap(chat -> {
                    if (!Objects.equals(chat.getOwnerUserId(), updatedById)) {
                        return Mono.error(RpcException.create(RpcErrors.NO_PERMISSION));
                    }

                    return chatParticipantRepository.findByChatIdAndUserId(chatId, userId)
                            .switchIfEmpty(Mono.error(RpcException.create(RpcErrors.USER_NOT_PARTICIPANT)))
                            .flatMap(participant -> {
                                final ChatParticipant.Status newStatus = admin ? ChatParticipant.Status.ADMIN :
                                        ChatParticipant.Status.PARTICIPANT;

                                final ChatParticipant toUpdate = participant.toBuilder()
                                        .status(newStatus)
                                        .build();

                                return chatParticipantRepository.update(toUpdate)
                                        .then(stateHolder.getChatParticipantsStore()
                                                .save(IntIntTuple2.of(chatId.asLong(), userId.asLong()), toUpdate));
                            })
                            .then();
                });
    }

    /**
     * Set admin privileges to a chat.
     *
     * @param chatId      Identifier of the chat.
     * @param userId      Identifier of the user.
     * @param permissions Chat permissions to set.
     * @return A {@link Mono} where, upon successful completion, emits nothing; indicating the chat privileges was set
     * to the chat. If an error is received, it is emitted through the {@code Mono}.
     */
    public Mono<Void> editChatRights(final Id chatId, final Id userId, final PermissionSet permissions) {
        return chatRepository.findById(chatId)
                .switchIfEmpty(Mono.error(RpcException.create(RpcErrors.CHAT_INVALID)))
                .flatMap(chat -> chatParticipantRepository.findByChatIdAndUserId(chatId, userId)
                        .switchIfEmpty(Mono.error(RpcException.create(RpcErrors.USER_NOT_PARTICIPANT)))
                        .flatMap(chatMember -> {
                            if (!canMemberEditAdminPermissions(chatMember)) {
                                return Mono.error(RpcException.create(RpcErrors.NO_PERMISSION));
                            }

                            final Chat toUpdateChat = chat.toBuilder()
                                    .permissions(permissions.getRawValue())
                                    .updatedById(userId)
                                    .updatedAt(dateFactory.getClock().instant())
                                    .build();

                            return chatRepository.update(toUpdateChat);
                        })
                        .flatMap(toCache -> stateHolder.getChatStore().save(toCache.getId().asLong(), toCache))
                        .then());
    }

    /**
     * Retrieves the {@link Page} of the chats.
     *
     * @param page   the page limit, offset.
     * @return A {@link Mono} that continually emits the {@link Page<Chat> participants} of the chat.
     * If an error is received, it is emitted through the {@code Flux}.
     */
    public Mono<Page<Chat>> getChats(final OffsetPageRequest page) {
        return chatRepository.findAll(page.getPageSize(), (int) page.getOffset())
                .collectList()
                .zipWith(chatRepository.count())
                .map(TupleUtils.function((BiFunction<List<Chat>, Long, Page<Chat>>) Page::new));
    }

    /**
     * Gets whether the user is a chat member.
     *
     * @param chatId Identifier of the chat.
     * @param userId Identifier of the user.
     * @return A {@link Flux} where, upon successful completion, emits nothing; indicating the user is the chat member
     * If an error is received, it is emitted through the {@code Flux}.
     */
    public Mono<Boolean> isChatParticipant(final Id chatId, final Id userId) {
        return stateHolder.getChatParticipantsStore().find(IntIntTuple2.of(chatId.asLong(), userId.asLong()))
                .switchIfEmpty(chatParticipantRepository.findByChatIdAndUserId(chatId, userId)
                        .flatMap(participant -> stateHolder.getChatParticipantsStore()
                                .save(IntIntTuple2.of(participant.getChatId().asLong(),
                                        participant.getUserId().asLong()), participant)))
                .hasElement();
    }

    /**
     * Retrieves the members of the chat.
     *
     * @param chatId Identifier of the chat.
     * @param userId Identifier of the user.
     * @return A {@link Mono} that continually emits the {@link Page<  ChatParticipant  > participants} of the chat.
     * If an error is received, it is emitted through the {@code Mono}.
     */
    public Mono<ChatParticipant> getChatParticipant(final Id chatId, final Id userId) {
        return stateHolder.getChatParticipantsStore().find(IntIntTuple2.of(chatId.asLong(), userId.asLong()))
                .switchIfEmpty(chatParticipantRepository.findByChatIdAndUserId(chatId, userId)
                        .flatMap(participant -> stateHolder.getChatParticipantsStore()
                                .save(IntIntTuple2.of(chatId.asLong(), userId.asLong()), participant)));
    }

    public Flux<ChatParticipant> getParticipants(final Id chatId, final int limit, final int offset) {
        return chatParticipantRepository.findAllByChatId(chatId, limit, offset);
    }

    /**
     * Retrieves the {@link Page} of the chat participants.
     *
     * @param chatId the ID of the chat.
     * @return A {@link Mono} that continually emits the {@link Page<  ChatParticipant  > participants} of the chat.
     * If an error is received, it is emitted through the {@code Flux}.
     */
    public Mono<Page<ChatParticipant>> getChatParticipants(final Id chatId, final int limit, final int offset) {
        return chatParticipantRepository.findAllByChatId(chatId, limit, offset)
                .collectList()
                .zipWith(chatParticipantRepository.countByChatId(chatId))
                .map(TupleUtils.function((content, count) -> new Page<>(content, count)));
    }

    /**
     * Retrieves the full participants of the chat.
     *
     * @param chatId the ID of the chat.
     * @return A {@link Flux} that continually emits the {@link Page<  ChatParticipant  > participants} of the chat.
     * If an error is received, it is emitted through the {@code Flux}.
     */
    public Flux<ChatParticipant> getChatParticipants(final Id chatId) {
        final BiFunction<Integer, Integer, Flux<ChatParticipant>> chunk = (offset, limit) -> chatParticipantRepository
                .findAllByChatId(chatId, limit, offset)
                .doFinally(__ -> log.debug("Chat: '{}' members chunk successfully loaded", chatId));

        Flux<ChatParticipant> participants;
        if (participantsCache.getIfPresent(chatId) != null) {
            participants = Flux.fromIterable(participantsCache.getIfPresent(chatId));
        } else {
            participants = chatParticipantRepository.countByChatId(chatId)
                    .flatMapMany(count -> PaginationUtils.paginate(chunk, 0, count))
                    .collectList()
                    .doOnNext(toCache -> participantsCache.put(chatId, toCache))
                    .flatMapIterable(cached -> cached);
        }
        return chatParticipantRepository.countByChatId(chatId)
                .flatMapMany(count -> PaginationUtils.paginate(chunk, 0, count));
    }

    /**
     * Check if chat participant can delete chat room.
     *
     * @param participant the chat participant.
     * @return {@code true} if the participant user can delete chat room.
     */
    private static boolean canDeleteChat(final ChatParticipant participant) {
        return participant.isOwner();
    }

    /**
     * Check if chat member can leave chat room.
     *
     * @param chatMember the chat participant.
     * @return {@code true} if the participant user can leave chat room.
     */
    private static boolean canMemberLeaveChat(ChatParticipant chatMember) {
        return !chatMember.isOwner();
    }

    /**
     * Check if chat member can edit admins permissions.
     *
     * @param chatMember the chat participant.
     * @return {@code true} if chat participant can edit admins permissions.
     */
    private static boolean canMemberEditAdminPermissions(ChatParticipant chatMember) {
        return chatMember.isOwner();
    }

    /**
     * Check whether or not chat member kick other users from chat.
     */
    private static boolean canKickMembers(ChatParticipant member) {
        return member.isOwner() || member.isAdmin();
    }

    private static boolean canBanMembers(ChatParticipant chatMember, PermissionSet permissions) {
        return chatMember.isOwner()
                || (chatMember.isAdmin() && permissions.contains(Permission.CAN_ADMINS_BAN_MEMBERS));
    }

    /**
     * Check if chat member can edit chat info like change title, about, photo etc.
     *
     * @param member the chat participant.
     * @param permissions the chat permissions.
     * @return {@code true} if the given participant can edit chat info.
     */
    private static boolean canEditChatInfo(ChatParticipant member, PermissionSet permissions) {
        return member.isOwner() || (member.isAdmin() && permissions.contains(Permission.CAN_MEMBERS_EDIT_GROUP_INFO))
                || permissions.contains(Permission.CAN_MEMBERS_EDIT_GROUP_INFO);
    }

    /**
     * Check if chat member can invite another users to the chat.
     *
     * @param member the chat participant.
     * @param permissions the chat permissions.
     * @return {@code true} if the participant user can invite users to the chat.
     */
    private static boolean canInviteNewMembers(ChatParticipant member, PermissionSet permissions) {
        return member.isOwner() || member.isAdmin() || permissions.contains(Permission.CAN_MEMBERS_INVITE);
    }
}
