package im.telegraph.generator.type;

public class SchemeListType extends SchemeType {
    private SchemeType type;

    public SchemeListType(SchemeType type) {
        this.type = type;
    }

    public SchemeType getType() {
        return type;
    }

    @Override
    public String toString() {
        return "list<" + type + ">";
    }
}