package im.telegraph.protocol.model.mtproto;

import im.telegraph.engine.serialization.DataInputStream;
import im.telegraph.engine.serialization.DataOutputStream;
import im.telegraph.engine.serialization.stream.BinaryOutputStream;

import java.io.IOException;

public abstract class ProtoObject {

	protected ProtoObject(DataInputStream inputStream) throws IOException {
		readObject(inputStream);
	}

	public ProtoObject() {

	}

	public abstract void writeObject(DataOutputStream outputStream) throws IOException;

	public abstract ProtoObject readObject(DataInputStream inputStream) throws IOException;

	public byte[] toByteArray() {
		DataOutputStream outputStream = new BinaryOutputStream();
		try {
			writeObject(outputStream);
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		return outputStream.toByteArray();
	}

}
