package im.telegraph.common;

import java.io.Serializable;
import java.util.Objects;

public class OffsetPageRequest implements Serializable {

    private static final long serialVersionUID = 6933361693518764295L;

    private final int limit;
    private final long offset;

    /**
     * Creates a new {@link OffsetPageRequest} with sort parameters applied.
     *
     * @param limit  the size of the elements to be returned.
     * @param offset zero-based offset.
     */
    private OffsetPageRequest(long offset, int limit) {
        if (limit < 1) {
            throw new IllegalArgumentException("Limit must not be less than one!");
        }
        if (offset < 0) {
            throw new IllegalArgumentException("Offset must not be less than zero!");
        }
        this.limit = limit;
        this.offset = offset;
    }

    /**
     * Created a new unsorted {@link OffsetPageRequest}.
     *
     * @param limit the size of the elements to be returned.
     * @param offset zero-based offset.
     */
    public static OffsetPageRequest of(int limit, int offset) {
        return new OffsetPageRequest(limit, offset);
    }

    public int getPageNumber() {
        return Math.toIntExact(offset / limit);
    }

    public int getPageSize() {
        return limit;
    }

    public long getOffset() {
        return offset;
    }

    public OffsetPageRequest next() {
        return new OffsetPageRequest(getOffset() + getPageSize(), getPageSize());
    }

    public OffsetPageRequest previous() {
        return hasPrevious() ? new OffsetPageRequest(getOffset() - getPageSize(), getPageSize()) : this;
    }

    public OffsetPageRequest previousOrFirst() {
        return hasPrevious() ? previous() : first();
    }

    public OffsetPageRequest first() {
        return new OffsetPageRequest(0, getPageSize());
    }

    public boolean hasPrevious() {
        return offset > limit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        OffsetPageRequest that = (OffsetPageRequest) o;
        return limit == that.limit && offset == that.offset;
    }

    @Override
    public int hashCode() {
        return Objects.hash(limit, offset);
    }

    @Override
    public String toString() {
        return "OffsetPageRequest{" +
                "limit=" + limit +
                ", offset=" + offset +
                '}';
    }
}
