package im.telegraph.handlers;

import im.telegraph.api.ApiAbsUser;
import im.telegraph.api.ApiContactRecord;
import im.telegraph.api.rpc.*;
import im.telegraph.domain.entity.Contact;
import im.telegraph.domain.entity.Id;
import im.telegraph.handlers.dispatch.DispatchContext;
import im.telegraph.handlers.internal.ApiEntityMapper;
import im.telegraph.handlers.mapper.UserConverter;
import im.telegraph.service.ContactsService;
import im.telegraph.service.UserService;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.function.TupleUtils;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Component
public class ContactsDispatchHandlers {

    private final ContactsService contactsService;
    private final UserService userService;

    private final UserConverter userConverter;

    public ContactsDispatchHandlers(ContactsService contactsService,
                                    UserService userService,
                                    UserConverter userConverter) {
        this.contactsService = contactsService;
        this.userService = userService;
        this.userConverter = userConverter;
    }

    public Mono<ResponseVoid> handleAddContact(DispatchContext<RequestContactsAddContact> context) {
        final Id userId = Id.of(context.getRequest().getUid());
        final long accessHash = context.getRequest().getAccessHash();

        return Mono.justOrEmpty(context.getUserId().map(Id::of))
                .filterWhen(selfId -> userService.checkAccessHash(userId, context.getAuthId(), accessHash))
                .flatMap(selfId -> contactsService.addContact(selfId, userId))
                .thenReturn(new ResponseVoid());
    }

    public Mono<ResponseVoid> handleAcceptContact(DispatchContext<RequestContactsApproveContact> context) {
        final Id userId = Id.of(context.getRequest().getUid());
        final long accessHash = context.getRequest().getAccessHash();

        return Mono.justOrEmpty(context.getUserId().map(Id::of))
                .filterWhen(selfId -> userService.checkAccessHash(userId, context.getAuthId(), accessHash))
                .flatMap(selfId -> contactsService.acceptContact(selfId, userId))
                .thenReturn(new ResponseVoid());
    }

    public Mono<ResponseVoid> handleDeleteContact(DispatchContext<RequestContactsRemoveContact> context) {
        final Id userId = Id.of(context.getRequest().getUid());
        final long accessHash = context.getRequest().getAccessHash();

        return Mono.justOrEmpty(context.getUserId().map(Id::of))
                .filterWhen(selfId -> userService.checkAccessHash(userId, context.getAuthId(), accessHash))
                .flatMap(selfId -> contactsService.deleteContact(selfId, userId))
                .thenReturn(new ResponseVoid());
    }

    public Mono<ResponseContactsGetContacts> handleGetContacts(DispatchContext<RequestContactsGetContacts> context) {
        return Mono.justOrEmpty(context.getUserId().map(Id::of))
                .flatMap(selfId -> contactsService.getContacts(selfId).defaultIfEmpty(Collections.emptyList()))
                .flatMap(contacts -> {
                    Mono<Boolean> hasChanged = Mono.just(Boolean.FALSE);

                    Mono<List<ApiContactRecord>> records = Flux.fromIterable(contacts)
                            .map(contact -> Optional.ofNullable(ApiEntityMapper.convertToApiContact(contact)))
                            .filter(Optional::isPresent)
                            .map(Optional::get)
                            .collectList()
                            .defaultIfEmpty(Collections.emptyList());

                    Mono<List<ApiAbsUser>> users = Flux.fromIterable(contacts)
                            .map(Contact::getContactId)
                            .collectList()
                            .flatMapMany(userService::getUsers)
                            .map(user -> Optional.ofNullable(userConverter.convert(context.getAuthId(), user)))
                            .filter(Optional::isPresent)
                            .map(Optional::get)
                            .collectList()
                            .defaultIfEmpty(Collections.emptyList());

                    return Mono.zip(records, users, hasChanged);
                })
                .map(TupleUtils.function(ResponseContactsGetContacts::new));
    }

    public Mono<ResponseContactsSearchContacts> handleSearchContacts(DispatchContext<RequestContactsSearchContacts> context) {
        return userService.searchUsers(context.getRequest().getRequest(), context.getRequest().getLimit())
                .map(user -> Optional.ofNullable(userConverter.convert(context.getAuthId(), user)))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collectList()
                .map(ResponseContactsSearchContacts::new);
    }
}
