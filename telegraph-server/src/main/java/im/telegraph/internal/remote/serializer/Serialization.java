package im.telegraph.internal.remote.serializer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import static im.telegraph.internal.remote.serializer.PrimitiveSerializers.*;

/**
 * @author Ast3t1s
 */
public final class Serialization {

    private static final Logger log = LoggerFactory.getLogger(Serialization.class);

    private final Map<Class<?>, Serializer> mappingByClass = new ConcurrentHashMap<>();
    private final Map<Integer, Serializer> mappingById = new ConcurrentHashMap<>();

    private final List<ClassSerializer> binding;

    private final Serializer nullSerializer;

    // Cache to eliminate lots of instanceof checks
    private final Class<?> objectType = Object.class;

    private Serialization(List<ClassSerializer> bindings) {
        this.binding = bindings;
        this.nullSerializer = new NullSerializer();
        this.mappingById.put(nullSerializer.serializerId(), nullSerializer);

        bindings.forEach(classSerializer -> register(classSerializer.type, classSerializer.serializer));

        register(byte[].class, new ByteArraySerializer());
        register(Integer.class, new IntSerializer());
        register(Long.class, new LongSerializer());
        register(String.class, new StringSerializer());
        register(Boolean.class, new BooleanSerializer());
    }

    /**
     * Initializes a new builder for an {@link Serialization}.
     *
     * @return a new builder
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     * Serialize the given object into an array of bytes using whatever serialize if currently configured
     * if serialize is not found found for the specific object {@link JdkSerializer} will be used.
     *
     * @param object the object to serialize
     * @return a byte array containing the serialized message
     */
    public byte[] serialize(final Object object) {
        if (object == null) {
            return nullSerializer.toBinary(object);
        }
        return findSerializerFor(object.getClass()).toBinary(object);
    }

    /**
     * Retrieve the {@link Serializer} to the given object based on its concrete type
     *
     * @param type the type of the object
     * @return the Serializer fot the given object type, throws {@link SerializationException} if it not found
     */
    public Serializer findSerializerFor(final Class<?> type) {
        Serializer serializer = mappingByClass.get(type);

        if (serializer == null) {
            serializer = mappingByClass.get(objectType);
        }

        if (serializer == null) {
            // bindings are ordered from most specific to least specific
            List<ClassSerializer> possibilities = binding.stream()
                    .filter(entry -> entry.getType().isAssignableFrom(type))
                    .collect(Collectors.toList());

            if (possibilities.isEmpty()) {
                throw new SerializationException("Can't find serializer for type " + type);
            }

            serializer = getUniqueSerializer(possibilities, type);
            register(type, serializer);
        }

        return serializer;
    }

    private Serializer getUniqueSerializer(List<ClassSerializer> possibilities, Class<?> type) {
        if (isUnique(possibilities)) {
            return possibilities.get(0).getSerializer();
        } else {
            List<ClassSerializer> filteredPossibilities = possibilities.stream()
                    .filter(entry -> !(entry.getSerializer() instanceof JdkSerializer))
                    .collect(Collectors.toList());

            if (filteredPossibilities.isEmpty()) {
                throw new SerializationException("More than one jdk serializer registered for " + type.getSimpleName());
            }

            if (!isUnique(filteredPossibilities)) {
                log.warn("Multiple serializers found for [" + type.getSimpleName() + "], choosing first of: [" +
                        filteredPossibilities.stream()
                                .map(entry -> entry.getSerializer().getClass().getName())
                                .collect(Collectors.joining(", ")) + "]");
            }

            return filteredPossibilities.get(0).getSerializer();
        }
    }

    private void register(Class<?> type, Serializer serializer) {
        this.mappingByClass.put(type, serializer);
        this.mappingById.put(serializer.serializerId(), serializer);
    }

    private boolean isUnique(List<ClassSerializer> possibilities) {
        return possibilities.size() == 1
                || possibilities.stream().allMatch(entry -> entry.getType().isAssignableFrom(possibilities.get(0).getType()))
                || possibilities.stream().allMatch(entry -> entry.getSerializer().equals(possibilities.get(0).getSerializer()));
    }

    /**
     * Deserialize the given array of bytes using specific {@link Serializer#serializerId()}
     *
     * @param bytes the bytes to deserialize
     * @param serializerId the identifier of the specific {@link Serializer}
     * @return the resulting object
     */
    public Object deserialize(final byte[] bytes, final int serializerId) {
        return Optional.ofNullable(mappingById.get(serializerId))
                .map(serializer -> serializer.fromBinary(bytes))
                .orElseThrow(() -> new SerializationException("Can't find serializer with id " + serializerId ));
    }

    /**
     * Deserialize the given array of bytes using specific {@link Serializer#serializerId()}
     *
     * @param bytes the bytes to deserialize
     * @param serializerId the identifier of the specific {@link Serializer}
     * @param type T the type of the object
     * @return the resulting object
     */
    public <T> T deserialize(final byte[] bytes, final int serializerId, Class<T> type) {
        return Optional.ofNullable(mappingById.get(serializerId))
                .map(serializer -> serializer.fromBinary(bytes, type))
                .orElseThrow(() -> new SerializationException("Can't find serializer with id " + serializerId));
    }

    /* Internal holder for pair of type and serializer */
    private static class ClassSerializer {

        private final Class<?> type;
        private final Serializer serializer;

        ClassSerializer(Class<?> type, Serializer serializer) {
            this.type = type;
            this.serializer = serializer;
        }

        public Class<?> getType() {
            return type;
        }

        public Serializer getSerializer() {
            return serializer;
        }
    }

    public static class Builder {

        private final List<ClassSerializer> bindings = new ArrayList<>();

        protected Builder() {
        }

        /**
         * Maps a specific type to a serializer.
         *
         * @param type the type
         * @param serializer the serializer to map
         * @param <T> the type of the mapping class
         * @return this {@link Builder} enriched with the added serializer
         */
        public <T> Builder registerSerializer(Class<T> type, Serializer serializer) {
            Objects.requireNonNull(type);
            Objects.requireNonNull(serializer);
            bindings.add(new ClassSerializer(type, serializer));
            return this;
        }

        /**
         * Builds an {@link Serialization} with all declared mappings.
         *
         * @return a new {@link Serialization}
         */
        public Serialization build() {
            return new Serialization(bindings);
        }
    }
} 
