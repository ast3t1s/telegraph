Create or replace function random_string(length integer) returns text as
$$
declare
  chars text[] := '{0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z}';
  result text := '';
  i integer := 0;
begin
  if length < 0 then
    raise exception 'Given length cannot be less than 0';
  end if;
  for i in 1..length loop
    result := result || chars[1+random()*(array_length(chars, 1)-1)];
  end loop;
  return result;
end;
$$ language plpgsql;

CREATE OR REPLACE FUNCTION timestamp_id(table_name text)
RETURNS bigint AS $$
	DECLARE
		time_part bigint;
		sequence_base bigint;
		tail bigint;
	BEGIN
		time_part := (
		-- Get the time in milliseconds
		((date_part('epoch', now()) * 1000))::bigint
		-- Add shift it over two bytes
		<< 16);

		sequence_base := (
			'x' ||
			-- Take the first two bytes (four hex characters)
			substr(
				--Of the MD5 hash of the data we documented
				md5(table_name || random_string(32) || time_part::text),
				1, 4
			)
			-- And turn it into a bigint
		)::bit(16)::bigint;

		 -- Finally, add our sequence number to our base, and chop
         -- it to the last two bytes
        tail := (
            (sequence_base + nextval(table_name || '_id_seq'))
            & 65535);

        	-- Return the time part and the sequence part. OR appears
            -- faster here than addition, but they're equivalent:
            -- time_part has no trailing two bytes, and tail is only
            -- the last two bytes.
            RETURN time_part | tail;
          END
    $$ LANGUAGE plpgsql VOLATILE;