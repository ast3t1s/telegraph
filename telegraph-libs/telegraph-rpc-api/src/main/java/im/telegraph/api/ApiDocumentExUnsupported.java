package im.telegraph.api;

import im.telegraph.engine.serialization.StreamReader;
import im.telegraph.engine.serialization.StreamWriter;
import java.io.IOException;
import java.lang.Override;

/**
 * @author Telegraph Scheme Generator
 */
public class ApiDocumentExUnsupported extends ApiDocumentEx {
    protected int key;

    protected byte[] content;

    public ApiDocumentExUnsupported(int key, byte[] content) {
        this.key = key;
        this.content = content;
    }

    @Override
    public int getHeader() {
        return this.key;
    }

    @Override
    public void deserialize(StreamReader reader) throws IOException {
        throw new IOException("Parsing is unsupported");
    }

    @Override
    public void serialize(StreamWriter writer) throws IOException {
        writer.writeRaw(content);
    }
}
