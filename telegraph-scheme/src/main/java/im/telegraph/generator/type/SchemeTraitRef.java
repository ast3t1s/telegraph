package im.telegraph.generator.type;

public class SchemeTraitRef {
    private int key;
    private String trait;

    public SchemeTraitRef(int key, String trait) {
        this.key = key;
        this.trait = trait;
    }

    public int getKey() {
        return key;
    }

    public String getTrait() {
        return trait;
    }
} 
