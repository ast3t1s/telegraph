package im.telegraph.server.test.transport;

import im.telegraph.engine.serialization.stream.BinaryInputStream;
import im.telegraph.engine.serialization.stream.BinaryOutputStream;
import im.telegraph.rpc.base.PacketTypeConstants;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.cert.X509Certificate;

public class PlainTcpConnection {

    private Socket socket;

    TrustManager[] trustAllCerts = new TrustManager[] {
            new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }
                public void checkClientTrusted(
                        java.security.cert.X509Certificate[] certs, String authType) {
                }
                public void checkServerTrusted(
                        java.security.cert.X509Certificate[] certs, String authType) {
                }
            }
    };

    public PlainTcpConnection(String host, int port) throws Exception {
        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, new java.security.SecureRandom());
        SSLSocketFactory socketFactory = (SSLSocketFactory) sc.getSocketFactory();
        this.socket = new Socket();
        this.socket.setKeepAlive(true);
        this.socket.setTcpNoDelay(true);

        socket.connect(new InetSocketAddress(host, port));

        socket = socketFactory.createSocket(socket, host, port, true);
    }

    public void execute(byte[] request) throws IOException {
        writeMessage(request);
    }

    public byte[] executeMethod(byte[] request) throws IOException {
        writeMessage(request);
        return readMessage();
    }

    private void writeMessage(byte[] request) throws IOException {
        final var outputStream = new BinaryOutputStream();
        outputStream.writeBytes(request, 0, request.length);
        socket.getOutputStream().write(outputStream.toByteArray());
        socket.getOutputStream().flush();
    }

    public byte[] readMessage() throws IOException {
        final var inputStream = socket.getInputStream();
        byte[] header = readBytes(inputStream, 9);
        BinaryInputStream binaryInputStream = new BinaryInputStream(header);
        int index = binaryInputStream.readInt();
        int headerVal = binaryInputStream.readByte();
        int size = binaryInputStream.readInt();
        if (size > 1024 * 1024) {
            throw new IOException("Incorrect size");
        }
        byte[] body = readBytes(inputStream, size + 4);
        if (headerVal == PacketTypeConstants.HEADER_ACK) {
            // Skip ack packets
            return readMessage();
        }
        BinaryOutputStream outputStream = new BinaryOutputStream();
        outputStream.writeBytes(header, 0, header.length);
        outputStream.writeBytes(body, 0, body.length);
        return outputStream.toByteArray();
    }

    private int readByte(InputStream stream) throws IOException {
        int res = stream.read();
        if (res < 0) {
            throw new IOException();
        }
        return res;
    }

    public void destroy() {
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private byte[] readBytes(InputStream inputStream, int count) throws IOException {
        byte[] res = new byte[count];
        int offset = 0;
        while (offset < res.length) {
            int readed = inputStream.read(res, offset, res.length - offset);
            if (readed > 0) {
                offset += readed;
            } else if (readed < 0) {
                throw new IOException();
            } else {
                Thread.yield();
            }
        }
        return res;
    }

}
