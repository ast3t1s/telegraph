package im.telegraph.common.rdbms;

import im.telegraph.common.rdbms.bind.BindMarkersFactory;
import im.telegraph.common.rdbms.bind.BindMarkersFactoryResolver;
import io.r2dbc.spi.ConnectionFactory;
import org.reactivestreams.Publisher;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Objects;
import java.util.function.Function;

/**
 * @author Ast3t1s
 */
public class RdbmsOperations {

    private final ParameterExpander parameterExpander = new ParameterExpander();

    private final ConnectionFactory connectionFactory;
    private final BindMarkersFactory bindMarkersFactory;

    private RdbmsOperations(ConnectionFactory connectionFactory) {
        this.connectionFactory = Objects.requireNonNull(connectionFactory);
        this.bindMarkersFactory = Objects.requireNonNull(BindMarkersFactoryResolver.resolve(connectionFactory));
    }

    public static RdbmsOperations create(ConnectionFactory connectionFactory) {
        return new RdbmsOperations(connectionFactory);
    }

    public ConnectionFactory getConnectionFactory() {
        return connectionFactory;
    }

    /**
     * Execute behavior within a transaction returning results..
     *
     * @param resourceFunction   a {@link Function} that takes a {@link Handle} and returns a {@link Publisher} of results
     * @param <T> the type of results
     * @return a {@link Flux} of results
     */
    public <T> Flux<T> inTransaction(Function<Handle, ? extends Publisher<? extends T>> resourceFunction) {
        Objects.requireNonNull(resourceFunction);
        return withHandle(handle -> handle.inTransaction(resourceFunction));
    }

    /**
     * Open a {@link Handle} and return it for use.  Note that you the caller is responsible for closing the handle otherwise connections will be leaked.
     *
     * @return a new {@link Handle}, ready to use
     * @see Handle#close()
     */
    public Mono<Handle> open() {
        return Mono.from(connectionFactory.create())
                .map(connection -> new Handle(connection, bindMarkersFactory, parameterExpander));
    }

    /**
     * Execute behavior with a {@link Handle} not returning results.
     *
     * @param resourceFunction a {@link Function} that takes a {@link Handle} and returns a {@link Publisher} of results.
     * @return a {@link Mono} that execution is complete
     */
    public Mono<Void> useHandle(Function<Handle, ? extends Publisher<?>> resourceFunction) {
        Objects.requireNonNull(resourceFunction);
        return withHandle(resourceFunction).then();
    }

    /**
     * Execute behavior within a transaction not returning results.
     * @param resourceFunction a {@link Function} that takes a {@link Handle} and returns a {@link Publisher} of results.
     * @return a {@link Mono} that execution is complete
     */
    public Mono<Void> useTransaction(Function<Handle, ? extends Publisher<?>> resourceFunction) {
        Objects.requireNonNull(resourceFunction);
        return useHandle(handle -> handle.useTransaction(resourceFunction));
    }

    /**
     * Execute behavior with a {@link Handle} returning results.
     *
     * @param resourceFunction   a {@link Function} that takes a {@link Handle} and returns a {@link Publisher} of results
     * @param <T> the type of results
     */
    public <T> Flux<T> withHandle(Function<Handle, ? extends Publisher<? extends T>> resourceFunction) {
        Objects.requireNonNull(resourceFunction);
        return open()
                .flatMapMany(handle -> Flux.from(resourceFunction.apply(handle))
                        .concatWith(ReactiveUtils.typeSafe(handle::close))
                        .onErrorResume(ReactiveUtils.appendError(handle::close)));
    }

    @Override
    public String toString() {
        return "R2dbc{" +
                "connectionFactory=" + connectionFactory +
                '}';
    }
}
