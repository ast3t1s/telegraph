package im.telegraph.protocol;

import im.telegraph.protocol.model.mtproto.ProtoMessage;
import im.telegraph.protocol.model.mtproto.ProtoStruct;
import org.reactivestreams.Publisher;
import reactor.core.publisher.Mono;
import reactor.util.annotation.Nullable;

import java.net.InetSocketAddress;
import java.time.Duration;
import java.time.Instant;

/**
 * Provides the basic level of MTProto protocol {@link ProtoMessage}
 */
public interface MTProtoSession {

    /**
     * Obtain the id of the proto session.
     *
     * @return The if of the session.
     */
    SessionId getId();

    /**
     * Configure the max amount of time that may elapse after the session is closed before a session
     * is considered expired. A negative value indicates the session should not expire.
     */
    void setEmptySessionTtl(Duration duration);

    /**
     * Return the address of the remote client.
     */
    @Nullable
    InetSocketAddress getRemoteAddress();

    /**
     * Get the session principal
     *
     * @return the session principal
     */
    @Nullable
    Object getPrincipal();

    /**
     * Set the principal of the session.
     *
     * @param principal the session principal
     */
    void setPrincipal(Object principal);

    /**
     * Return the session cipher for message encrypting and decrypting.
     *
     * @return the session cipher.
     */
    MTProtoCipher getCipher();

    /**
     * Give a source of outgoing messages, write the messages and return a
     * {@code Mono<Void>} that completes when the source completes and writing is done.
     *
     * @param messages the {@code Publisher} of messages.
     * @return A {@link Mono} where, upon successful completion, emits nothing; indicating the message is sent.
     * If an error is received, it is emitted through the {@code Mono}.
     */
    Mono<Void> sendRpcMessage(Publisher<ProtoStruct> messages);

    /**
     * Attempt to resend messages from {@link Publisher} steam if there are any presence if session cache.
     *
     * @param ids the {@code Publisher} of message id's.
     * @return A {@link Mono} where, upon successful completion, emits nothing; indicating the message is sent.
     * If an error is received, it is emitted through the {@code Mono}.
     */
    Mono<Void> resend(Publisher<Long> ids);

    /**
     * Acknowledge outgoing messages and remove from session cache.
     *
     * @param messageIds the {@code Publisher} of message ID's
     * @return A {@link Mono} where, upon successful completion, emits nothing; indicating the message is acknowledged.
     * If an error is received, it is emitted through the {@code Mono}.
     */
    Mono<Void> acknowledge(Publisher<Long> messageIds);

    /**
     * Cancel outgoing RPC message if it was not sent.
     *
     * @param messagesIds the {@code Publisher} of message ID's
     * @return A {@link Mono} where, upon successful completion, emits nothing; indicating the message is canceled.
     * If an error is received, it is emitted through the {@code Mono}.
     */
    Mono<Void> cancelRpc(Publisher<Long> messagesIds);

    /**
     * Return the time since session was last active, or otherwise
     * if the session is new, then the time since the session was created.
     */
    @Nullable
    Instant getTimeSinceLastActive();

    /**
     * Return {@code true} if the session is expired.
     */
    boolean isExpired();

    Mono<Void> destroy();
}
