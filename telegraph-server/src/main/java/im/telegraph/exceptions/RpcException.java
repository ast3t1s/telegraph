package im.telegraph.exceptions;

import im.telegraph.protocol.MTProtoException;

import java.io.Serial;
import java.util.EnumMap;
import java.util.StringJoiner;

/**
 * @author Ast3t1s
 */
public final class RpcException extends MTProtoException {

    private static final EnumMap<RpcErrors, RpcException> CACHE = new EnumMap<>(RpcErrors.class);

    @Serial
    private static final long serialVersionUID = -2677458116933352077L;

    static {
        for (final RpcErrors error : RpcErrors.VALUES) {
            CACHE.put(error, create(error));
        }
    }

    private final int errorCode;
    private final String errorTag;
    private final String errorMessage;

    /**
     * Create a new {@link RpcException instance}.
     *
     * @param errorCode    the rpc error code.
     * @param errorTag     the rpc error tag.
     * @param errorMessage the rpc error message.
     */
    public RpcException(int errorCode, String errorTag, String errorMessage) {
        super(errorMessage);
        this.errorCode = errorCode;
        this.errorTag = errorTag;
        this.errorMessage = errorMessage;
    }

    public static RpcException create(RpcErrors error) {
        return CACHE.computeIfAbsent(error,
                errors -> new RpcException(error.getErrorCode(), error.getErrorTag(), error.getErrorMsg()));
    }

    /**
     * Returns the rpc error code.
     *
     * @return tpc error code.
     */
    public int getErrorCode() {
        return errorCode;
    }

    /**
     * Returns the rpc error tag.
     *
     * @return rpc error tag.
     */
    public String getErrorTag() {
        return errorTag;
    }

    /**
     * Returns the rpc error description.
     *
     * @return rpc error description.
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    @Override
    public String getMessage() {
        StringJoiner errorDetails = new StringJoiner(", ", "(", ")");
        errorDetails.add("Rpc Code: " + getErrorCode());
        errorDetails.add("Rpc Tag: " + getErrorTag());
        errorDetails.add("Rpc Message: " + getErrorMessage());
        String message = super.getMessage();
        if (message != null) {
            return message + " " + errorDetails;
        }
        return errorDetails.toString();
    }

    @Override
    public String toString() {
        return "RpcException{" +
                "errorCode=" + errorCode +
                ", errorTag='" + errorTag + '\'' +
                ", errorMessage='" + errorMessage + '\'' +
                '}';
    }
}
