package im.telegraph.domain.entity;

import java.io.Serializable;
import java.time.Instant;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author Ast3t1s
 */
public final class SessionInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    private final Id userId;
    private final Map<Long, UserDevice> devices;

    public SessionInfo(Id userId, Map<Long, UserDevice> devices) {
        this.userId = userId;
        this.devices = devices;
    }

    public static SessionInfo single(Id userId, long authId, String node, Instant expireAt) {
        return new SessionInfo(userId, Collections.singletonMap(authId,
                new UserDevice(authId, node, expireAt)));
    }

    public Id getUserId() {
        return userId;
    }

    public Map<Long, UserDevice> getDevices() {
        return Collections.unmodifiableMap(devices);
    }

    public boolean hasActive(Instant now) {
        return devices.values().stream()
                .anyMatch(device -> device.getExpireAt().isBefore(now));
    }

    public SessionInfo addDevice(long authId, String node, Instant expireAt) {
        UserDevice device = new UserDevice(authId, node, expireAt);
        HashMap<Long, UserDevice> newDevices = new HashMap<>(devices);
        newDevices.put(authId, device);
        return new SessionInfo(userId, newDevices);
    }

    public SessionInfo removeDevice(long authId) {
        HashMap<Long, UserDevice> newDevices = new HashMap<>(devices);
        newDevices.remove(authId);
        return new SessionInfo(userId, newDevices);
    }

    public static class UserDevice implements Serializable {

        private static final long serialVersionUID = 1L;

        private final long authId;
        private final String node;
        private final Instant expireAt;

        public UserDevice(long authId, String node, Instant expireAt) {
            this.authId = authId;
            this.node = node;
            this.expireAt = expireAt;
        }

        public long getAuthId() {
            return authId;
        }

        public String getNode() {
            return node;
        }

        public Instant getExpireAt() {
            return expireAt;
        }

        @Override
        public boolean equals(Object object) {
            if (this == object) {
                return true;
            }
            if (object == null || getClass() != object.getClass()) {
                return false;
            }
            UserDevice that = (UserDevice) object;
            return authId == that.authId &&
                    Objects.equals(node, that.node) &&
                    Objects.equals(expireAt, that.expireAt);
        }

        @Override
        public int hashCode() {
            return Objects.hash(authId, node, expireAt);
        }

        @Override
        public String toString() {
            return "UserDevice{" +
                    "authId=" + authId +
                    ", node='" + node + '\'' +
                    ", expireAt=" + expireAt +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "SessionInfo{" +
                "userId=" + userId +
                ", devices=" + devices +
                '}';
    }
}
