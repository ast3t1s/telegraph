package im.telegraph.domain.repository.rdbms;

import im.telegraph.common.rdbms.RdbmsOperations;
import im.telegraph.domain.entity.Contact;
import im.telegraph.domain.entity.Id;
import im.telegraph.domain.repository.ContactsRepository;
import io.r2dbc.spi.*;
import org.reactivestreams.Publisher;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * @author Ast3t1s
 */
@Repository
public class RdbmsContactsRepository extends AbstractRdbmsRepository implements ContactsRepository {

    private static final String FIELDS = "user_id, contact_id, status, created_at";

    private static final String INSERT_CONTACT_QUERY =
            "INSERT INTO contacts (" + FIELDS + ") VALUES (:userId, :contactId, :status, :createdAt)";

    private static final String INSERT_CONTACT_BULK_QUERY =
            "INSERT INTO contacts (" + FIELDS + ") VALUES :tuples";

    private static final String GET_CONTACT_QUERY =
            "SELECT " + FIELDS + " FROM contacts WHERE user_id = :userId AND contact_id = :contactId";

    private static final String GET_CONTACTS_QUERY =
            "SELECT " + FIELDS + " FROM contacts WHERE user_id = :userId ORDER BY contact_id ";

    private static final String GET_COUNT_USER_CONTACTS_QUERY =
            "SELECT COUNT(*) FROM contacts AS count WHERE user_id = :userId";

    private static final String UPDATE_CONTACT_QUERY =
            "UPDATE contacts SET status = :status WHERE user_id = :userId AND contact_id = :contactId";

    private static final String DELETE_CONTACT_QUERY =
            "DELETE FROM contacts WHERE user_id = :userId AND contact_id = :contactId";

    private final RdbmsOperations rdbmsOperations;

    private final Function<Contact, Map<String, Parameter>> contactParametersMapper = new ContactParametersMapper();
    private final BiFunction<Row, RowMetadata, Contact> contactRowMapper = new ContactRowMapper();

    public RdbmsContactsRepository(ConnectionFactory connectionFactory) {
        super(connectionFactory);
        this.rdbmsOperations = RdbmsOperations.create(connectionFactory);
    }

    @Override
    public Mono<Contact> insert(Contact contact) {
       return rdbmsOperations.withHandle(handle -> handle.createUpdate(INSERT_CONTACT_QUERY)
               .bindMap(contactParametersMapper.apply(contact))
               .execute().next()).then(Mono.just(contact));
    }

    @Override
    public Flux<Contact> bulkInsert(Publisher<Contact> contacts) {
        return Flux.from(contacts)
                .map(contact -> new Object[]{contact.getUserId().asLong(), contact.getContactId().asLong(),
                        contact.getStatus().getValue(), contact.getCreatedAt()})
                .collectList()
                .flatMapMany(tuples -> rdbmsOperations.inTransaction(handle -> handle
                        .createUpdate(INSERT_CONTACT_BULK_QUERY)
                        .bind("tuples", tuples)
                        .execute()
                        .thenMany(contacts)));
    }

    @Override
    public Mono<Contact> update(Contact contact) {
        return rdbmsOperations.withHandle(handle -> handle.createUpdate(UPDATE_CONTACT_QUERY)
                .bindMap(contactParametersMapper.apply(contact))
                .execute()).then(Mono.just(contact));
    }

    @Override
    public Mono<Void> deleteByUserIdAndContactId(Id userId, Id contactId) {
        return rdbmsOperations.useHandle(handle -> handle.createUpdate(DELETE_CONTACT_QUERY)
                .bind("userId", userId.asLong())
                .bind("contactId", contactId.asLong())
                .execute());
    }

    @Override
    public Mono<Contact> findByUserIdAndContactId(Id userId, Id contactId) {
        return rdbmsOperations.withHandle(handle -> handle.createQuery(GET_CONTACT_QUERY)
                .bind("userId", userId.asLong())
                .bind("contactId", contactId.asLong())
                .mapRow(contactRowMapper))
                .next();
    }

    @Override
    public Flux<Contact> findAllByUserId(Id userId, int limit, int offset) {
        return rdbmsOperations.withHandle(handle -> handle
                .createQuery(limitQuery(GET_CONTACTS_QUERY, limit, offset))
                .bind("userId", userId.asLong())
                .mapRow(contactRowMapper));
    }

    @Override
    public Mono<Long> countByUserId(Id userId) {
        return rdbmsOperations.withHandle(handle -> handle.createQuery(GET_COUNT_USER_CONTACTS_QUERY)
                .bind("userId", userId.asLong())
                .mapRow(row -> row.get("count", Long.class)))
                .next();
    }

    private static class ContactParametersMapper implements Function<Contact, Map<String, Parameter>> {

        @Override
        public Map<String, Parameter> apply(Contact contact) {
            final Map<String, Parameter> parameters = new HashMap<>();
            parameters.put("userId", Parameters.in(R2dbcType.BIGINT, contact.getUserId().asLong()));
            parameters.put("contactId", Parameters.in(R2dbcType.BIGINT, contact.getContactId().asLong()));
            parameters.put("status", Parameters.in(R2dbcType.SMALLINT, contact.getStatus().getValue()));
            parameters.put("createdAt", Parameters.in(R2dbcType.TIMESTAMP,
                    LocalDateTime.ofInstant(contact.getCreatedAt(), ZoneOffset.UTC)));
            return parameters;
        }
    }

    private static class ContactRowMapper implements BiFunction<Row, RowMetadata, Contact> {

        @Override
        public Contact apply(Row row, RowMetadata metadata) {
            final var userId = row.get("user_id", Long.class);
            final var contactId = row.get("contact_id", Long.class);
            final var status = row.get("status", Short.class);
            final var createdAt = row.get("created_at", LocalDateTime.class);
            return new Contact(Id.of(userId), Id.of(contactId), Contact.Status.of(status), createdAt.toInstant(ZoneOffset.UTC));
        }
    }
} 
