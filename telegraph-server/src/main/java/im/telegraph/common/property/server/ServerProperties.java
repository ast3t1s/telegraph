package im.telegraph.common.property.server;

import im.telegraph.server.Server;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

/**
 * Net Server properties {@link Server}.
 *
 * @author Ast3t1s
 */
@ConfigurationProperties(prefix = "telegraph.server")
public class ServerProperties {

    /**
     * Tcp server properties.
     */
    @NestedConfigurationProperty
    private final TcpServerProperties tcp = new TcpServerProperties();

    /**
     * Websocket server properties.
     */
    @NestedConfigurationProperty
    private final WebsocketServerProperties websocket = new WebsocketServerProperties();

    public TcpServerProperties getTcp() {
        return tcp;
    }

    public WebsocketServerProperties getWebsocket() {
        return websocket;
    }
}
