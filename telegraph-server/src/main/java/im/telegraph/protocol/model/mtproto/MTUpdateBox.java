package im.telegraph.protocol.model.mtproto;

import im.telegraph.engine.serialization.DataInputStream;
import im.telegraph.engine.serialization.DataOutputStream;

import java.io.IOException;

public class MTUpdateBox extends ProtoStruct {

	public static final byte HEADER = 0x05;

	private byte[] payload;

	public MTUpdateBox(DataInputStream inputStream) throws IOException {
		super(inputStream);
	}

	public MTUpdateBox(byte[] payload) {
		this.payload = payload;
	}

	public byte[] getPayload() {
		return payload;
	}

	@Override
	protected byte getHeader() {
		return (byte) HEADER;
	}

	@Override
	protected void writeBody(DataOutputStream outputStream) throws IOException {
		outputStream.writeProtoBytes(payload);
	}

	@Override
	protected void readBody(DataInputStream inputStream) throws IOException {
		this.payload = inputStream.readProtoBytes();
	}

	@Override
	public String toString() {
		return "UpdateBox";
	}

}
