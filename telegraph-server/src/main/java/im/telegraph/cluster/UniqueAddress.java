package im.telegraph.cluster;

import im.telegraph.internal.remote.Address;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author Ast3t1s
 */
public final class UniqueAddress implements Serializable, Comparable<UniqueAddress> {

    private static final long serialVersionUID = 1L;

    private final Address address;
    private final String id;

    public UniqueAddress(Address address, String id) {
        this.address = address;
        this.id = id;
    }

    public Address getAddress() {
        return address;
    }

    public String getId() {
        return id;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        UniqueAddress that = (UniqueAddress) object;
        return Objects.equals(address, that.address) &&
                Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(address, id);
    }

    @Override
    public int compareTo(UniqueAddress o) {
        final int result = address.compareTo(o.getAddress());
        if (result == 0) {
            return id.compareTo(o.getId());
        }
        return result;
    }

    @Override
    public String toString() {
        return "UniqueAddress{" +
                "address=" + address +
                ", id='" + id + '\'' +
                '}';
    }
}
