package im.telegraph.domain.repository;

import im.telegraph.domain.entity.Id;
import im.telegraph.domain.entity.photo.Photo;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Collection;

/**
 * @author Ast3t1s
 */
public interface PhotoRepository {

    /**
     * Inserts a new photo entry into the repository.
     *
     * @param photo the photo entity to insert
     * @return A {@link Mono} that emits the inserted {@link Photo} instance. If an error occurs during the insertion,
     * the {@code Mono} emits an error.
     */
    Mono<Photo> insert(Photo photo);

    /**
     * Finds a photo by its ID.
     *
     * @param id the ID of the photo
     * @return A {@link Mono} that emits the {@link Photo} instance if found, or an empty {@code Mono} if no photo
     * is found with the given ID. If an error occurs during the search, it is emitted through the {@code Mono}.
     */
    Mono<Photo> findById(Id id);

    /**
     * Finds all photos by their IDs.
     *
     * @param ids the IDs of the photos to find
     * @return A {@link Flux} that emits the {@link Photo} instances matching the given IDs. If an error occurs during
     * the search, it is emitted through the {@code Flux}.
     */
    Flux<Photo> findAllByIdIn(Collection<Id> ids);

    /**
     * Finds photos by the given user ID with pagination.
     *
     * @param userId the ID of the user to search for
     * @param limit the maximum number of photos to return
     * @param offset the starting position in the result set
     * @return A {@link Flux} that emits the {@link Photo} instances for the given {@link Id}. If an error occurs,
     * it is emitted through the {@code Flux}.
     */
    Flux<Photo> findByUserId(Id userId, int limit, int offset);

    /**
     * Counts the number of photos for the given user ID.
     *
     * @param userId the ID of the user to count photos for
     * @return A {@link Mono} that emits the count of photos for the given {@link Id}. If an error occurs, it is
     * emitted through the {@code Mono}.
     */
    Mono<Long> countByUserId(Id userId);

    /**
     * Deletes photos by their IDs.
     *
     * @param ids the IDs of the photos to delete
     * @return A {@link Mono} that completes when the photos are successfully deleted, emitting the count of deleted
     * photos. If an error occurs during deletion, it is emitted through the {@code Mono}.
     */
    Mono<Long> deleteAllByIdIn(Collection<Id> ids);
}