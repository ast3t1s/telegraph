package im.telegraph.server;

import reactor.util.annotation.Nullable;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.time.Duration;

/**
 * Simple interface that represents a fully configured transport server(for example Reactor Netty
 * or another implementation). Allows to use different protocols such as tcp, http, udp.
 * Allows server to be {@link #start() started} and {@link #stop() stopped}.
 *
 * @author Ast3t1s
 */
public interface Server {

    /**
     * Starts the transport server. Calling this method on an already started server has no
     * effect.
     * @throws ServerException if the server cannot be started
     */
    void start() throws ServerException;

    /**
     * Stops the transport server. Calling this method on an already stopped server has not effect.
     * @throws ServerException if the serve cannot be started
     */
    void stop() throws ServerException;

    /**
     * Return the address this server is listening on.
     * @return the address
     */
    @Nullable
    InetSocketAddress getAddress();

    /**
     * Builder to create a transport server.
     */
    interface Builder {

        /**
         * Sets the specific session handler that accepts the connection created by server when client try
         * to listen it.
         *
         * @param handler the session handler
         * @return this builder
         */
        Builder handler(ConnectionAcceptor handler);

        /**
         * Sets the specific network address that the server should bind to.
         *
         * @param address tha address to set (default to {@code null}
         * @return this builder
         */
        Builder host(InetAddress address);

        /**
         * Sets the port that server should listen on. If not specified port '443' for tcp and '9091' for http
         * will be used. Use port -1 to disable server auto-start (i.e. start the server application context
         * but not have it listen to any port).
         *
         * @param port the port to set
         * @return this builder
         */
        Builder port(int port);

        /**
         * Set the maximum amount of time that should be waited when starting or stopping the server.
         *
         * @param lifecycleTimeout the lifecycle timeout
         * @return this builder
         */
        Builder lifecycleTimeout(Duration lifecycleTimeout);

        /**
         * Create the {@link Server}
         *
         * @return an {@link Server} with the configured parameters.
         */
        Server build();
    }
}
