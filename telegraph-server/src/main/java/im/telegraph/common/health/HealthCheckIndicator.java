package im.telegraph.common.health;

public interface HealthCheckIndicator {

    /**
     * Check whether or not instance is healthy.
     *
     */
    boolean isHealthy();

    /**
     * Update the status of current instance health.
     */
    void updateHealthStatus();
}
