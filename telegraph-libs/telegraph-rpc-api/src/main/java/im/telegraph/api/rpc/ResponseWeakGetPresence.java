package im.telegraph.api.rpc;

import static im.telegraph.engine.serialization.StreamUtils.*;

import im.telegraph.api.ApiUserPresence;
import im.telegraph.engine.serialization.StreamReader;
import im.telegraph.engine.serialization.StreamWriter;
import im.telegraph.engine.serialization.io.IOContext;
import im.telegraph.rpc.base.Response;
import java.io.IOException;
import java.lang.Override;
import java.lang.String;

/**
 * Generated by the Telegraph API Scheme generator. DO NOT EDIT!
 * @author Telegraph Scheme Generator
 */
public class ResponseWeakGetPresence extends Response {
    public static final int CLASS_ID = 0x20;

    protected ApiUserPresence presence;

    public ResponseWeakGetPresence() {
    }

    public ResponseWeakGetPresence(ApiUserPresence presence) {
        this.presence = presence;
    }

    public static ResponseWeakGetPresence fromBytes(byte[] data) throws IOException {
        return IOContext.fromByteArray(new ResponseWeakGetPresence(), data);
    }

    public ApiUserPresence getPresence() {
        return this.presence;
    }

    public void setPresence(ApiUserPresence presence) {
        this.presence = presence;
    }

    @Override
    public void deserialize(StreamReader reader) throws IOException {
        this.presence = ApiUserPresence.fromBytes(reader.readByteArray(1));
    }

    @Override
    public void serialize(StreamWriter writer) throws IOException {
        if (this.presence == null) {
            throw new IOException();
        }
        writer.writeBytes(1, this.presence.buildContainer());
    }

    @Override
    public String toString() {
        String res = "tuple GetPresence{";
        res += "}";
        return res;
    }

    @Override
    public int getHeader() {
        return CLASS_ID;
    }
}
