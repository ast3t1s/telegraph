package im.telegraph.service;

import im.telegraph.common.DateTimeFactory;
import im.telegraph.domain.entity.AuthKey;
import im.telegraph.domain.entity.AuthSession;
import im.telegraph.domain.entity.Id;
import im.telegraph.domain.repository.AuthSessionRepository;
import im.telegraph.exceptions.RpcErrors;
import im.telegraph.exceptions.RpcException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author Ast3t1s
 */
@Service
public class AuthService {

    private static final Logger log = LoggerFactory.getLogger(AuthService.class);

    private final AuthSessionRepository authSessionRepository;

    private final IdGeneratorManager idGenerator;
    private final DateTimeFactory dateFactory;

    public AuthService(AuthSessionRepository authSessionRepository,
                       IdGeneratorManager idGenerator,
                       DateTimeFactory dateFactory) {
        this.authSessionRepository = authSessionRepository;
        this.idGenerator = idGenerator;
        this.dateFactory = dateFactory;
    }

    /**
     * Create a new authorization session and notify other user's devices.
     *
     * @param authKeyId the auth key identifier {@link AuthKey}.
     * @param deviceTitle the title of the device.
     * @param remoteAddress the remote address of the client (can be null).
     * @param userId the ID of the user.
     * @return A {@link Mono} where, upon successful completion, emits the created {@link AuthSession}.If an error is
     * received, it is emitted through the {@code Mono}.
     */
    public Mono<AuthSession> create(final Long authKeyId, final String deviceTitle, final String remoteAddress, final Id userId) {
        return authSessionRepository.findByAuthIdAndDeletedAtIsNull(authKeyId)
                .flatMap(exists -> {
                    log.warn("There is already authorization session bound to key: {}", authKeyId);
                    return Mono.error(RpcException.create(RpcErrors.AUTH_KEY_BOUND));
                })
                .then(Mono.fromCallable(() -> AuthSession.builder()
                        .id(idGenerator.generateEntityId(AuthSession.ENTITY_NAME))
                        .userId(userId)
                        .authId(authKeyId)
                        .appName(deviceTitle)
                        .ip(remoteAddress)
                        .createdAt(dateFactory.getClock().instant())
                        .build()))
                .flatMap(authSessionRepository::insert)
                .doOnNext(session -> log.debug("Authorization session: {} created", session));
    }

    /**
     * Gets the session authorized by the auth key ID that the session is bound to.
     *
     * @param authKeyId the authorization key id
     * @return A {@link Mono} that continually emits the user's {@link AuthSession authorization session}.
     * If an error is received, it is emitted through the {@code Mono}.
     */
    public Mono<AuthSession> getByAuthKeyId(final Long authKeyId) {
        return authSessionRepository.findByAuthIdAndDeletedAtIsNull(authKeyId);
    }

    /**
     * Get list of authorization sessions bound to a specific user.
     *
     * @param userId the ID of the {@code User user}.
     * @return A {@link Flux} that continually emits the user's {@link AuthSession authorization sessions}.
     * If an error is received, it is emitted through the {@code Flux}.
     */
    public Flux<AuthSession> getUserAuthorizations(final Id userId) {
        return authSessionRepository.findAllByUserIdAndDeletedAtIsNull(userId);
    }

    /**
     * Deletes a session associated with a given user and session hash if it hasn't already been deleted.
     *
     * @param userId the ID of the user attempting to delete the session.
     * @param hash the session hash to be deleted.
     * @return A {@link Mono} that continually emits whether the session was successfully marked as deleted (true) or
     * not (false). whether the session was successfully marked as deleted (true) or not (false)
     */
    public Mono<Boolean> delete(final Id userId, final Id hash) {
        return authSessionRepository.findById(hash)
                .flatMap(authSession -> {
                    if (authSession.getDeletedAt().isPresent()) {
                        return Mono.error(RpcException.create(RpcErrors.CANT_TERMINATE_SESSION));
                    }
                    if (!Objects.equals(userId, authSession.getUserId())) {
                        return Mono.just(false);
                    }

                   /* return Mono.fromCallable(() -> authSession.toBuilder()
                            .deletedAt(dateFactory.now())
                            .build())
                            .flatMap(authSessionRepository::update)
                            .hasElement();*/
                    return Mono.fromSupplier(() -> Boolean.TRUE);
                })
                .switchIfEmpty(Mono.just(false));
    }

    /**
     * Deletes all active (non-deleted) sessions for a given user that are not associated with a given auth key.
     *
     * @param userId the ID of the user whose sessions are being deleted.
     * @param authKeyId the authentication key ID that is used to filter out sessions to be retained.
     * @return A {@link Flux} that continually emits the IDs of the sessions that were successfully deleted
     */
    public Flux<Id> deleteSessions(final Id userId, final Long authKeyId) {
        return authSessionRepository.findAllByUserIdAndDeletedAtIsNull(userId)
                .collectList()
                .flatMapMany(allSessions -> {
                    final List<AuthSession> eligibleSessions = new ArrayList<>(0);
                    final Collection<AuthSession> ineligibleSessions = new ArrayList<>(0);

                    for (final AuthSession session : allSessions) {
                        if (!Objects.equals(session.getUserId(), userId)) {
                            ineligibleSessions.add(session);
                        } else {
                            eligibleSessions.add(session);
                        }
                    }

                    if (ineligibleSessions.size() == allSessions.size()) {
                        return Flux.error(() -> RpcException.create(RpcErrors.FAILED_TERMINATES_SESSIONS));
                    }

                    final Collection<Id> eligibleIds = eligibleSessions.stream()
                            .distinct()
                            .filter(session -> !Objects.equals(session.getAuthId(), authKeyId))
                            .map(AuthSession::getId)
                            .collect(Collectors.toList());

                    return authSessionRepository.deleteByIdsIn(eligibleIds)
                            .thenMany(Flux.fromIterable(eligibleIds));
                });
    }

    public static class DeletedAuthorizations {

        private final List<Id> deletedSessions;
        private final List<Id> activeSessions;

        DeletedAuthorizations(List<Id> deletedSessions, List<Id> activeSessions) {
            this.deletedSessions = deletedSessions;
            this.activeSessions = activeSessions;
        }

        public static DeletedAuthorizations of(List<Id> deletedSessions, List<Id> activeSessions) {
            return new DeletedAuthorizations(deletedSessions, activeSessions);
        }

        public List<Id> getDeletedSessions() {
            return deletedSessions;
        }

        public List<Id> getActiveSessions() {
            return activeSessions;
        }
    }
}
