package im.telegraph.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public abstract class FileUtils {

    public static void deleteFile(Path path) {
        try {
            Files.deleteIfExists(path);
        } catch (IOException e) {
            //ignore
        }
    }

    /**
     * Create temporary directory.
     *
     * @return temp directory
     */
    public static Path createTempDir() throws IOException {
        return Files.createTempDirectory("file-utils");
    }

    /**
     * Create temporary file.
     *
     * @return temp file
     */
    public static Path createTempFile() throws IOException {
        return Files.createTempFile("file", "");
    }

    /**
     * Concat list of file into one directory.
     *
     * @param dir directory to store.
     * @param fileNames the list of file names.
     * @return concatenated files
     */
    public static File concat(File dir, List<String> fileNames) throws IOException {
        Path dirPath = dir.toPath();
        File contactFiles = dirPath.resolve("concatenated").toFile();

        try (FileOutputStream out = new FileOutputStream(contactFiles)) {
            for (String fileName : fileNames) {
                Files.copy(dirPath.resolve(fileName), out);
            }
        }
        return contactFiles;
    }
} 
