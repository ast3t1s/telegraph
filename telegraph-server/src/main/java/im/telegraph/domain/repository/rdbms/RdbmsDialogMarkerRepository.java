package im.telegraph.domain.repository.rdbms;

import im.telegraph.common.rdbms.RdbmsOperations;
import im.telegraph.domain.entity.DialogMarker;
import im.telegraph.domain.entity.Id;
import im.telegraph.domain.entity.Peer;
import im.telegraph.domain.repository.DialogMarkerRepository;
import io.r2dbc.spi.*;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Ast3t1s
 */
@Repository
public class RdbmsDialogMarkerRepository extends AbstractRdbmsRepository implements DialogMarkerRepository {

    private static final String FIELDS = "peer_id, peer_type, user_id, state, timestamp";

    private static final String TABLE_NAME = "history_inbox";

    private static final String PK_FILTER = "user_id = :userId AND peer_id = :peerId AND peer_type = :peerType";

    private static final String GET_INBOX_QUERY =
            "SELECT " + FIELDS + " FROM " + TABLE_NAME + " WHERE " + PK_FILTER;

    private static final String GET_BY_PEER_QUERY =
            "SELECT " + FIELDS + " FROM " + TABLE_NAME + " WHERE peer_id = :peerId AND peer_type = :peerType AND " +
                    "timestamp >= :timestamp";

    private static final String DELETE_BY_USER_ID_QUERY =
            "DELETE from " + TABLE_NAME + " WHERE " + PK_FILTER;

    private static final String LOAD_INBOX_LIST_QUERY =
            "SELECT " + FIELDS + " FROM " + TABLE_NAME + " WHERE (peer_id, peer_type, user_id) IN (:tuples)";

    private final RdbmsOperations rdbmsOperations;

    private final BiFunction<Row, RowMetadata, DialogMarker> dialogMarkerRowMapper = new HistoryInboxRowMapper();
    private final Function<DialogMarker, Map<String, Object>> dialogMarkerParameterMapper = new HistoryInboxParametersMapper();

    private final String upsertQuery;

    public RdbmsDialogMarkerRepository(ConnectionFactory connectionFactory) {
        super(connectionFactory);
        this.rdbmsOperations = RdbmsOperations.create(connectionFactory);
        this.upsertQuery = getUpsertQuery();
    }

    @Override
    public Mono<Void> save(DialogMarker inbox) {
        return rdbmsOperations.useHandle(handle -> handle.createUpdate(upsertQuery)
                .bindMap(dialogMarkerParameterMapper.apply(inbox))
                .execute());
    }

    @Override
    public Mono<DialogMarker> findByUserIdAndPeer(Id userId, Peer peer) {
        return rdbmsOperations.withHandle(handle -> handle.createQuery(GET_INBOX_QUERY)
                .bind("userId", userId.asLong())
                .bind("peerId", peer.getPeerId())
                .bind("peerType", peer.getType().getValue())
                .mapRow(dialogMarkerRowMapper))
                .next();
    }

    @Override
    public Flux<DialogMarker> findAllByUserIdAndPeerIn(Id userId, List<Peer> peers) {
        return rdbmsOperations.withHandle(handle -> handle.createQuery(LOAD_INBOX_LIST_QUERY)
                .bind("tuples", peers.stream()
                        .map(peer -> new Object[]{
                                Parameters.in(R2dbcType.BIGINT, peer.getPeerId()),
                                Parameters.in(R2dbcType.SMALLINT, peer.getType().getValue()),
                                Parameters.in(R2dbcType.BIGINT, userId.asLong())})
                        .collect(Collectors.toList()))
                .mapRow(dialogMarkerRowMapper));
    }

    @Override
    public Flux<DialogMarker> findAllByPeerAndTimestampGreaterThanEqual(Peer peer, Instant timestamp) {
        return rdbmsOperations.withHandle(handle -> handle.createQuery(GET_BY_PEER_QUERY)
                .bind("peerId", peer.getPeerId())
                .bind("peerType", peer.getType().getValue())
                .bind("timestamp", LocalDateTime.ofInstant(timestamp, ZoneOffset.UTC))
                .mapRow(dialogMarkerRowMapper));
    }

    @Override
    public Mono<Void> deleteByUserIdAndPeer(Id userId, Peer peer) {
        return rdbmsOperations.useHandle(handle -> handle.createUpdate(DELETE_BY_USER_ID_QUERY)
                .bind("userId", userId.asLong())
                .bind("peerId", peer.getPeerId())
                .bind("peerType", peer.getType())
                .execute());
    }

    private String getUpsertQuery() {
        Map<String, Object> params = new LinkedHashMap<>();
        params.put("peer_id", "peerId");
        params.put("peer_type", "peerType");
        params.put("user_id", "userId");
        params.put("state", "state");
        params.put("timestamp", "timestamp");
        return prepareUpsert(TABLE_NAME, params, List.of("peer_id", "peer_type", "user_id"), List.of("state", "timestamp"));
    }

    private static class HistoryInboxParametersMapper implements Function<DialogMarker, Map<String, Object>> {

        @Override
        public Map<String, Object> apply(DialogMarker historyInbox) {
            final Map<String, Object> parameters = new HashMap<>();
            parameters.put("peerId", Parameters.in(R2dbcType.BIGINT, historyInbox.getPeer().getPeerId()));
            parameters.put("peerType", Parameters.in(R2dbcType.SMALLINT, historyInbox.getPeer().getType().getValue()));
            parameters.put("userId", Parameters.in(R2dbcType.BIGINT, historyInbox.getUserId().asLong()));
            parameters.put("state", Parameters.in(historyInbox.getState().getValue()));
            parameters.put("timestamp", Parameters.in(LocalDateTime.ofInstant(historyInbox.getTimestamp(),
                    ZoneOffset.UTC)));
            return parameters;
        }
    }

    private static class HistoryInboxRowMapper implements BiFunction<Row, RowMetadata, DialogMarker> {

        @Override
        public DialogMarker apply(Row row, RowMetadata metadata) {
            final var peerId = row.get("peer_id", Long.class);
            final var peerType = row.get("peer_type", Integer.class);
            final var userId = row.get("user_id", Long.class);
            final var state = row.get("state", Integer.class);
            final var timestamp = row.get("timestamp", LocalDateTime.class);

            return DialogMarker.of(Id.of(userId), Peer.of(peerId, Peer.Type.of(peerType)), DialogMarker.State.of(state),
                    timestamp.toInstant(ZoneOffset.UTC));
        }
    }
}
