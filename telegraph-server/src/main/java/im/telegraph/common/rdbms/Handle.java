package im.telegraph.common.rdbms;

import im.telegraph.common.rdbms.bind.BindMarkersFactory;
import io.r2dbc.spi.Connection;
import io.r2dbc.spi.IsolationLevel;
import org.reactivestreams.Publisher;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Objects;
import java.util.function.Function;
import java.util.stream.IntStream;

import static im.telegraph.common.rdbms.ReactiveUtils.appendError;
import static im.telegraph.common.rdbms.ReactiveUtils.typeSafe;

/**
 * A wrapper for a {@link Connection} providing additional convenience APIs.
 *
 * @author Ast3t1s
 */
public final class Handle {

    private final Connection connection;
    private final BindMarkersFactory bindMarkers;
    private final ParameterExpander expander;

    Handle(Connection connection, BindMarkersFactory bindMarkers, ParameterExpander parameterExpander) {
        this.connection = Objects.requireNonNull(connection);
        this.bindMarkers = Objects.requireNonNull(bindMarkers);
        this.expander = Objects.requireNonNull(parameterExpander);
    }

    /**
     * Get the R2dbc {@link Connection} this Handle user.
     *
     * @return the R2dbc {@link Connection} this Handle uses.
     */
    public Connection getConnection() {
        return connection;
    }

    /**
     * Get the Bind marker factory
     *
     * @return the bind marker factory
     */
    public BindMarkersFactory getBindMarkers() {
        return bindMarkers;
    }

    /**
     * Get parameter expander.
     *
     * @return parameter expander
     */
    public ParameterExpander getExpander() {
        return expander;
    }

    /**
     * Begins a new transaction.
     *
     * @return a {@link Publisher} that indicates that the transaction is open
     */
    public Publisher<Void> beginTransaction() {
        return connection.beginTransaction();
    }

    /**
     * Release any resources held by the {@link Handle}.
     *
     * @return a {@link Publisher} that termination is complete
     */
    public Publisher<Void> close() {
        return connection.close();
    }

    /**
     * Commits the current transaction.
     *
     * @return a {@link Publisher} that indicates that a transaction has been committed
     */
    public Publisher<Void> commitTransaction() {
        return connection.commitTransaction();
    }

    /**
     * Creates a new {@link Batch} instance for building a batched request.
     *
     * @return a new {@link Batch} instance
     */
    public Batch createBatch() {
        return new Batch(connection.createBatch());
    }

    /**
     * Creates a new {@link Query} instance for building a request.
     *
     * @param sql the SQL of the query
     * @return a new {@link Query} instance
     */
    public Query createQuery(String sql) {
        Objects.requireNonNull(sql);
        return new Query(this, sql);
    }

    /**
     * Creates a savepoint in the current transaction.
     *
     * @param name the name of the savepoint to create
     * @return a {@link Publisher} that indicates that a savepoint has been created
     */
    public Publisher<Void> createSavepoint(String name) {
        Objects.requireNonNull(name, "name must not be null");
        return connection.createSavepoint(name);
    }

    /**
     * Create a new {@link Update} instance for building an updating request.
     *
     * @param sql the SQL of the update
     * @return a new {@link Update} instance
     */
    public Update createUpdate(String sql) {
        Objects.requireNonNull(sql, "sql must not be null");
        return new Update(this, sql);
    }

    /**
     * A convenience method for building and executing an {@link Update}, binding an ordered set of parameters.
     *
     * @param sql        the SQL of the update
     * @param parameters the parameters to bind
     * @return the number of rows that were updated
     */
    public Flux<Long> execute(String sql, Object... parameters) {
        Objects.requireNonNull(sql);
        Objects.requireNonNull(parameters);

        Update update = createUpdate(sql);

        IntStream.range(0, parameters.length)
                .forEach(i -> update.bind(i, parameters[i]));

        return update.execute();
    }

    /**
     * Execute behavior within a transaction returning results.
     *
     * @param resourceFunction a {@link Function} that takes a {@link Handle} and returns a {@link Publisher} of results
     * @param <T>              the type of results
     * @return a {@link Flux} of results
     */
    @SuppressWarnings("unchecked")
    public <T> Flux<T> inTransaction(Function<Handle, ? extends Publisher<? extends T>> resourceFunction) {
        Objects.requireNonNull(resourceFunction);
        return Mono.from(beginTransaction())
                .thenMany((Publisher<T>) resourceFunction.apply(this))
                .concatWith(typeSafe(this::commitTransaction))
                .onErrorResume(appendError(this::rollbackTransaction));
    }

    /**
     * Execute behavior within a transaction returning results.
     *
     * @param isolationLevel   the isolation level of the transaction
     * @param resourceFunction a {@link Function} that takes a {@link Handle} and returns a {@link Publisher} of results
     * @param <T>              the type of results
     * @return a {@link Flux} of results
     */
    @SuppressWarnings("unchecked")
    public <T> Flux<T> inTransaction(IsolationLevel isolationLevel, Function<Handle, ? extends Publisher<? extends T>> resourceFunction) {
        Objects.requireNonNull(isolationLevel);
        Objects.requireNonNull(resourceFunction);
        return inTransaction(handle -> Flux.from(handle
                .setTransactionIsolationLevel(isolationLevel))
                .thenMany((Publisher<T>) resourceFunction.apply(this)));
    }

    /**
     * Releases a savepoint in the current transaction.
     *
     * @param name the name of the savepoint to release
     * @return a {@link Publisher} that indicates that a savepoint has been released
     */
    public Publisher<Void> releaseSavepoint(String name) {
        Objects.requireNonNull(name);
        return connection.releaseSavepoint(name);
    }

    /**
     * Rolls back the current transaction.
     *
     * @return a {@link Publisher} that indicates that a transaction has been rolled back
     */
    public Publisher<Void> rollbackTransaction() {
        return connection.rollbackTransaction();
    }

    /**
     * Rolls back to a savepoint in the current transaction.
     *
     * @param name the name of the savepoint to rollback to
     * @return a {@link Publisher} that indicates that a savepoint has been rolled back to
     */
    public Publisher<Void> rollbackTransactionToSavepoint(String name) {
        Objects.requireNonNull(name);
        return connection.rollbackTransactionToSavepoint(name);
    }

    /**
     * A convenience method for building a {@link Query}, binding an ordered set of parameters.
     *
     * @param sql        the SQL of the query
     * @param parameters the parameters to bind
     * @return a new {@link Query} instance
     */
    public Query select(String sql, Object... parameters) {
        Objects.requireNonNull(sql);
        Objects.requireNonNull(parameters);

        Query query = createQuery(sql);

        IntStream.range(0, parameters.length)
                .forEach(i -> query.bind(i, parameters[i]));

        return query;
    }

    /**
     * Configures the isolation level for the current transaction.
     *
     * @param isolationLevel the isolation level for this transaction
     * @return a {@link Publisher} that indicates that a transaction level has been configured
     */
    public Publisher<Void> setTransactionIsolationLevel(IsolationLevel isolationLevel) {
        Objects.requireNonNull(isolationLevel);
        return connection.setTransactionIsolationLevel(isolationLevel);
    }

    /**
     * Execute behavior within a transaction not returning results.
     *
     * @param resourceFunction a {@link Function} that takes a {@link Handle} and returns a {@link Publisher} of results.
     * @return a {@link Mono} that execution is complete
     */
    public Mono<Void> useTransaction(Function<Handle, ? extends Publisher<?>> resourceFunction) {
        Objects.requireNonNull(resourceFunction);
        return inTransaction(resourceFunction).then();
    }

    /**
     * Execute behavior within a transaction not returning results.
     *
     * @param isolationLevel   the isolation level of the transaction
     * @param resourceFunction a {@link Function} that takes a {@link Handle} and returns a {@link Publisher} of results.
     * @return a {@link Mono} that execution is complete
     */
    public Mono<Void> useTransaction(IsolationLevel isolationLevel, Function<Handle, ? extends Publisher<?>> resourceFunction) {
        Objects.requireNonNull(isolationLevel);
        Objects.requireNonNull(resourceFunction);
        return inTransaction(isolationLevel, resourceFunction).then();
    }

    @Override
    public String toString() {
        return "Handle{" +
                "connection=" + connection +
                '}';
    }
} 
