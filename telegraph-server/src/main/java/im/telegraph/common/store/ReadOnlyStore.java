package im.telegraph.common.store;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

/**
 * A store that only supports read operations.
 *
 * @param <K> The key type which provides a 1:1 mapping to the value type. This type is also expected to be
 * {@link Comparable} in order to allow for range operations.
 * @param <V> The value type, these follow
 * <a href="https://en.wikipedia.org/wiki/JavaBeans#JavaBean_conventions">JavaBean</a> conventions.
 */
public interface ReadOnlyStore<K extends Comparable<K>, V> {
    /**
     * Attempts to find the value associated with the provided id.
     *
     * @param key the key to search with.
     * @return A mono, which may or may not contain an associated value.
     */
    Mono<V> find(K key);

    /**
     * Return the amount of stored values in the data source currently.
     *
     * @return A mono which provides the amount of stored values.
     */
    Mono<Long> count();

    /**
     * Gets a stream of all keys in the data source.
     *
     * @return The stream of keys stored.
     */
    Flux<K> keys();

    /**
     * Gets a stream of all values in the data source.
     *
     * @return The stream of values stored.
     */
    Flux<V> values();

    /**
     * Gets a stream of all entries in the data source.
     *
     * @return The stream of all entries stored.
     */
    default Flux<Tuple2<K, V>> entries() {
        return keys().zipWith(values());
    }
} 
