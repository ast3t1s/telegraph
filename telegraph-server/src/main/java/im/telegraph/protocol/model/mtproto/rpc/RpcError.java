package im.telegraph.protocol.model.mtproto.rpc;

import im.telegraph.engine.serialization.DataInputStream;
import im.telegraph.engine.serialization.DataOutputStream;
import im.telegraph.protocol.model.mtproto.ProtoStruct;

import java.io.IOException;

public class RpcError extends ProtoStruct {

	public static final byte HEADER = (byte) 0x02;

	public int errorCode;

	public String errorTag;

	public String userMessage;

	public RpcError(DataInputStream stream) throws IOException {
		super(stream);
	}

	public RpcError(int errorCode, String errorTag, String userMessage) {
		this.errorCode = errorCode;
		this.errorTag = errorTag;
		this.userMessage = userMessage;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public String getErrorTag() {
		return errorTag;
	}

	public String getUserMessage() {
		return userMessage;
	}

	@Override
	protected byte getHeader() {
		return HEADER;
	}

	@Override
	protected void writeBody(DataOutputStream outputStream) throws IOException {
		outputStream.writeInt32(errorCode);
		outputStream.writeProtoString(errorTag);
		outputStream.writeProtoString(userMessage);
	}

	@Override
	protected void readBody(DataInputStream inputStream) throws IOException {
		this.errorCode = inputStream.readInt();
		this.errorTag = inputStream.readProtoString();
		this.userMessage = inputStream.readProtoString();
	}

	@Override
	public String toString() {
		return "RpcError [#" + errorCode + " " + errorTag + "]";
	}

}
