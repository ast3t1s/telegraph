package im.telegraph.cluster.internal;

import im.telegraph.cluster.ClusterMessage;
import im.telegraph.internal.remote.Address;

/**
 * @author Ast3t1s
 */
public class Heartbeat implements ClusterMessage {

    private static final long serialVersionUID = 1;

    private final Address from;
    private final long seq;
    private final long timestamp;

    public Heartbeat(Address from, long seq, long timestamp) {
        this.from = from;
        this.seq = seq;
        this.timestamp = timestamp;
    }

    public Address getFrom() {
        return from;
    }

    public long getSeq() {
        return seq;
    }

    public long getTimestamp() {
        return timestamp;
    }

    @Override
    public String toString() {
        return "Heartbeat{" +
                "from=" + from +
                ", seq=" + seq +
                ", timestamp=" + timestamp +
                '}';
    }
} 
