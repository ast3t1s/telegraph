package im.telegraph.common.store.caffeine;

import com.github.benmanes.caffeine.cache.Caffeine;
import im.telegraph.common.store.Store;
import im.telegraph.common.store.StoreService;
import reactor.core.publisher.Mono;

import java.util.function.Function;

/**
 * {@link Store} factory that creates instances backed by {@link Caffeine} caches.
 */
public class CaffeineStoreService implements StoreService {

    private final Function<Caffeine<Object, Object>, Caffeine<Object, Object>> mapper;

    public CaffeineStoreService() {
        this(builder -> builder);
    }

    public CaffeineStoreService(Function<Caffeine<Object, Object>, Caffeine<Object, Object>> mapper) {
        this.mapper = mapper;
    }

    @Override
    public <K extends Comparable<K>, V> Store<K, V> provideGenericStore(Class<K> keyClass, Class<V> valueClass) {
        return new CaffeineStore<K, V>(mapper.apply(Caffeine.newBuilder()).build());
    }

    @Override
    public Mono<Void> dispose() {
        return Mono.empty();
    }
}
