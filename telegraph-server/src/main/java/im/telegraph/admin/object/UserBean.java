package im.telegraph.admin.object;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Ast3t1s
 */
public class UserBean {

    private final Long id;
    private final String username;
    private final String email;
    private final String about;
    private final boolean enabled;
    private final boolean verified;
    private final String role;
    @JsonProperty("lang_code")
    private final String langCode;
    @JsonProperty("created_at")
    private final Long createdAt;

    public UserBean(Long id, String username, String email, String about, boolean enabled, boolean verified,
                    String role, String langCode, Long createdAt) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.about = about;
        this.enabled = enabled;
        this.verified = verified;
        this.role = role;
        this.langCode = langCode;
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return "UserBean{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", about='" + about + '\'' +
                ", enabled=" + enabled +
                ", verified=" + verified +
                ", role='" + role + '\'' +
                ", langCode='" + langCode + '\'' +
                ", createdAt=" + createdAt +
                '}';
    }
}
