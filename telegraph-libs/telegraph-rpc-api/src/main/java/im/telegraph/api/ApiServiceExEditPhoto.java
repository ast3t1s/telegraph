package im.telegraph.api;

import static im.telegraph.engine.serialization.StreamUtils.*;

import im.telegraph.engine.serialization.StreamReader;
import im.telegraph.engine.serialization.StreamWriter;
import java.io.IOException;
import java.lang.Override;
import java.lang.String;

/**
 * Generated by the Telegraph API Scheme generator. DO NOT EDIT!
 * @author Telegraph Scheme Generator
 */
public class ApiServiceExEditPhoto extends ApiServiceEx {
    protected ApiAvatar avatar;

    public ApiServiceExEditPhoto() {
    }

    public ApiServiceExEditPhoto(ApiAvatar avatar) {
        this.avatar = avatar;
    }

    public int getHeader() {
        return 6;
    }

    public ApiAvatar getAvatar() {
        return this.avatar;
    }

    public void setAvatar(ApiAvatar avatar) {
        this.avatar = avatar;
    }

    @Override
    public void deserialize(StreamReader reader) throws IOException {
        this.avatar = reader.readOptionalSerializable(1, new ApiAvatar());
    }

    @Override
    public void serialize(StreamWriter writer) throws IOException {
        if (this.avatar != null) {
            writer.writeSerializable(1, this.avatar);
        }
    }

    @Override
    public String toString() {
        String res = "struct ServiceExEditPhoto{";
        res += "avatar=" + (this.avatar != null ? "set" : "empty");
        res += "}";
        return res;
    }
}
