package im.telegraph.handlers;

import im.telegraph.api.ApiFileUrlDescription;
import im.telegraph.api.rpc.*;
import im.telegraph.domain.entity.Id;
import im.telegraph.handlers.dispatch.DispatchContext;
import im.telegraph.service.FileService;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Instant;
import java.util.Collections;
import java.util.Optional;

@Component
public class FileDispatchHandlers {

    private final FileService fileService;

    public FileDispatchHandlers(FileService fileService) {
        this.fileService = fileService;
    }

    public Mono<ResponseFilesGetFileUrl> handleGetFileUrl(DispatchContext<RequestFilesGetFileUrl> context) {
        final long fileId = context.getRequest().getFile().getFileId();
        final long accessHash = context.getRequest().getFile().getAccessHash();

        return fileService.getFileDownloadUrl(Id.of(fileId), accessHash)
                .map(file -> new ResponseFilesGetFileUrl(file.getUrl(), Optional.of(file.getExpireTime())
                        .map(Instant::getEpochSecond)
                        .map(Math::toIntExact)
                        .orElse(Math.toIntExact(Instant.EPOCH.getEpochSecond()))));
    }

    public Mono<ResponseFilesGetFileUrls> handleGetFileUrls(DispatchContext<RequestFilesGetFileUrls> context) {
        return Flux.fromIterable(context.getRequest().getFiles())
                .flatMap(fileLocation -> fileService.getFileDownloadUrl(Id.of(fileLocation.getFileId()), fileLocation.getAccessHash())
                        .map(file-> new ApiFileUrlDescription(fileLocation.getFileId(), file.getUrl(),
                                Math.toIntExact(file.getExpireTime().getEpochSecond()), null)))
                .collectList()
                .defaultIfEmpty(Collections.emptyList())
                .map(ResponseFilesGetFileUrls::new);
    }

    public Mono<ResponseFilesGetFileUploadUrl> handleGetFileUploadUrl(DispatchContext<RequestFilesGetFileUploadUrl> context) {
        return fileService.getFileUploadUrl(context.getRequest().getExpectedSize())
                .map(uploadUrl -> new ResponseFilesGetFileUploadUrl(uploadUrl.getUrl(), uploadUrl.getUploadKey()));
    }
}
