package im.telegraph.protocol.model.mtproto.auth;

import im.telegraph.engine.serialization.DataInputStream;
import im.telegraph.engine.serialization.DataOutputStream;
import im.telegraph.protocol.model.mtproto.ProtoStruct;

import java.io.IOException;

public class ResponseDH extends ProtoStruct {

	public static final int HEADER = 0xE7;

	private long randomId;
	private byte[] verify;
	private byte[] verifySign;

	public ResponseDH(DataInputStream inputStream) throws IOException {
		super(inputStream);
	}

	public ResponseDH(long randomId, byte[] verify, byte[] verifySign) {
		this.randomId = randomId;
		this.verify = verify;
		this.verifySign = verifySign;
	}

	public long getRandomId() {
		return randomId;
	}

	public byte[] getVerify() {
		return verify;
	}

	public byte[] getVerifySign() {
		return verifySign;
	}

	@Override
	protected byte getHeader() {
		return (byte) HEADER;
	}

	@Override
	protected void writeBody(DataOutputStream outputStream) throws IOException {
		outputStream.writeInt64(randomId);
		outputStream.writeProtoBytes(verify);
		outputStream.writeProtoBytes(verifySign);
	}

	@Override
	protected void readBody(DataInputStream inputStream) throws IOException {
		this.randomId = inputStream.readInt64();
		this.verify = inputStream.readProtoBytes();
		this.verifySign = inputStream.readProtoBytes();
	}

}
