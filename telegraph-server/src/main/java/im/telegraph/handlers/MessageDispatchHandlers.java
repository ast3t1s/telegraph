package im.telegraph.handlers;

import im.telegraph.api.*;
import im.telegraph.api.rpc.*;
import im.telegraph.api.update.UpdateChatClear;
import im.telegraph.api.update.UpdateTyping;
import im.telegraph.common.Page;
import im.telegraph.domain.entity.Dialog;
import im.telegraph.domain.entity.DialogMarker;
import im.telegraph.domain.entity.Id;
import im.telegraph.domain.entity.Peer;
import im.telegraph.handlers.dispatch.DispatchContext;
import im.telegraph.handlers.internal.ApiEntityMapper;
import im.telegraph.handlers.internal.GetExtractedUsers;
import im.telegraph.handlers.mapper.DialogConverter;
import im.telegraph.handlers.mapper.UserConverter;
import im.telegraph.service.*;
import org.reactivestreams.Publisher;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.function.TupleUtils;

import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Component
public class MessageDispatchHandlers {

    private final DialogMarkerService dialogMarkerService;
    private final MessagingService messagingService;
    private final UserService userService;
    private final ChatService chatService;
    private final UpdatesService updatesService;

    private final DialogConverter dialogConverter;
    private final UserConverter userConverter;

    public MessageDispatchHandlers(DialogMarkerService dialogMarkerService,
                                   MessagingService messagingService,
                                   UserService userService,
                                   ChatService chatService,
                                   UpdatesService updatesService,
                                   DialogConverter dialogConverter,
                                   UserConverter userConverter) {
        this.dialogMarkerService = dialogMarkerService;
        this.messagingService = messagingService;
        this.userService = userService;
        this.chatService = chatService;
        this.updatesService = updatesService;
        this.dialogConverter = dialogConverter;
        this.userConverter = userConverter;
    }

    /**
     * Invoked when the client attempt to send a message.
     *
     * @param context the incoming request instance.
     * @return a {@link Publisher} that completes when this listener has done processing the incoming request, for
     * example returning any {@link Mono}, {@link Flux} or  synchronous code using {@link Mono#fromRunnable(Runnable)}.
     */
    public Mono<ResponseMessagesSendMessage> handleSendMessage(DispatchContext<RequestMessagesSendMessage> context) {
        final long randomId = context.getRequest().getRid();
        final Instant date = Instant.ofEpochMilli(context.getRequest().getDate());
        final ApiMessage apiMessage = context.getRequest().getMessage();
        final Peer peer = Peer.fromUniqueId(context.getRequest().getPeer().getPeerId());

        return Mono.justOrEmpty(context.getUserId().map(Id::of))
                .flatMap(selfId -> Mono.fromCallable(apiMessage::buildContainer)
                        .flatMap(message -> messagingService.createMessage(selfId, peer, randomId, date,
                                apiMessage.getHeader(), message)))
                .map(message -> new ResponseMessagesSendMessage(message.getId().asLong(), message.getCreatedAt().getEpochSecond()));
    }

    /**
     * Invoked when the client attempt to mark messages as read.
     *
     * @param context the incoming request instance.
     * @return a {@link Publisher} that completes when this listener has done processing the incoming request, for
     * example returning any {@link Mono}, {@link Flux} or  synchronous code using {@link Mono#fromRunnable(Runnable)}.
     */
    public Mono<ResponseVoid> handleReadMessage(DispatchContext<RequestMessagesMessageRead> context) {
        final Instant timestamp = Instant.ofEpochMilli(context.getRequest().getDate());
        final Peer peer = Peer.fromUniqueId(context.getRequest().getPeer().getPeerId());

        return Mono.justOrEmpty(context.getUserId().map(Id::of))
                .flatMap(selfId -> dialogMarkerService.setDialogMarker(selfId, peer, DialogMarker.State.READ, timestamp)
                        .then(messagingService.resetUnreadCount(selfId, peer, null, timestamp)))
                .thenReturn(new ResponseVoid());
    }

    /**
     * Invoked when the client attempt to mark messages as received.
     *
     * @param context the incoming request instance.
     * @return a {@link Publisher} that completes when this listener has done processing the incoming request, for
     * example returning any {@link Mono}, {@link Flux} or synchronous code using {@link Mono#fromRunnable(Runnable)}.
     */
    public Mono<ResponseVoid> handleReceivedMessage(DispatchContext<RequestMessagesMessageReceived> context) {
        final Peer peer = Peer.fromUniqueId(context.getRequest().getPeer().getPeerId());
        final Instant timestamp = Instant.ofEpochMilli(context.getRequest().getDate());

        return Mono.justOrEmpty(context.getUserId().map(Id::of))
                .flatMap(selfId -> dialogMarkerService.setDialogMarker(selfId, peer, DialogMarker.State.RECEIVED, timestamp))
                .thenReturn(new ResponseVoid());
    }

    public Mono<ResponseMessagesGetDialogMarkers> handleGetDialogMarkers(DispatchContext<RequestMessagesGetDialogMarkers> context) {
        final Peer peer = Peer.fromUniqueId(context.getRequest().getPeer().getPeerId());
        final Instant timestamp = Instant.ofEpochSecond(context.getRequest().getDate());

        return Mono.justOrEmpty(context.getUserId().map(Id::of))
                .flatMapMany(selfId -> dialogMarkerService.getDialogMarkers(selfId, peer, timestamp))
                .map(inbox -> Optional.ofNullable(ApiEntityMapper.toApiParticipantsMark(inbox)))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collectList()
                .defaultIfEmpty(Collections.emptyList())
                .map(ResponseMessagesGetDialogMarkers::new);
    }

    /**
     * Invoked when the client attempt to clear a chat.
     *
     * @param context the incoming request instance.
     * @return a {@link Publisher} that completes when this listener has done processing the incoming request, for
     * example returning any {@link Mono}, {@link Flux} or synchronous code using {@link Mono#fromRunnable(Runnable)}.
     */
    public Mono<ResponseSeq> handleClearChat(DispatchContext<RequestMessagesClearChat> context) {
        return Mono.justOrEmpty(context.getUserId().map(Id::of))
                .flatMap(selfId -> {
                    final Peer peer = Peer.fromUniqueId(context.getRequest().getPeer().getPeerId());

                    final UpdateChatClear update = new UpdateChatClear();
                    if (peer.isUser()) {
                        update.setPeer(new ApiPeerUser(peer.getPeerId()));
                    } else if (peer.isChat()) {
                        update.setPeer(new ApiPeerChat(peer.getPeerId()));
                    }

                    return messagingService.deleteHistory(selfId, peer, 0)
                            .then(updatesService.forwardSeqUpdate(selfId, update));
                })
                .map(seqUpdate -> new ResponseSeq(seqUpdate.getSequence(), seqUpdate.getTimestamp().toEpochMilli()));
    }

    /**
     * Invoked when the client attempt to retrieve chat history.
     *
     * @param context the incoming request instance.
     * @return a {@link Publisher} that completes when this listener has done processing the incoming request, for
     * example returning any {@link Mono}, {@link Flux} or  synchronous code using {@link Mono#fromRunnable(Runnable)}.
     */
    public Mono<ResponseMessagesLoadHistory> handleLoadHistory(DispatchContext<RequestMessagesLoadHistory> context) {
        final Instant mindDate = Instant.ofEpochMilli(context.getRequest().getMinDate());
        final int limit = context.getRequest().getLimit();
        final Peer peer = Peer.fromUniqueId(context.getRequest().getPeer().getPeerId());

        return Mono.justOrEmpty(context.getUserId().map(Id::of))
                .flatMapMany(selfId -> messagingService.getHistoryMessages(selfId, peer, mindDate, limit))
                .map(message -> Optional.ofNullable(dialogConverter.convert( message)))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collectList()
                .defaultIfEmpty(Collections.emptyList())
                .zipWhen(apiMessages -> Flux.fromIterable(GetExtractedUsers.extract(apiMessages))
                        .distinct()
                        .mapNotNull(Id::of)
                        .collectList()
                        .flatMapMany(userService::getUsers)
                        .map(user -> Optional.ofNullable(userConverter.convert(context.getAuthId(), user)))
                        .filter(Optional::isPresent)
                        .map(Optional::get)
                        .collectList()
                        .defaultIfEmpty(Collections.emptyList()))
                .map(TupleUtils.function(ResponseMessagesLoadHistory::new));
    }

    /**
     * Invoked when the client attempt to retrieve slice of dialogs.
     *
     * @param context the incoming request instance.
     * @return a {@link Publisher} that completes when this listener has done processing the incoming request, for
     * example returning any {@link Mono}, {@link Flux} or  synchronous code using {@link Mono#fromRunnable(Runnable)}.
     */
    public Mono<ResponseMessagesLoadDialogs> handleLoadDialogs(DispatchContext<RequestMessagesLoadDialogs> context) {
        final Instant minDate = Instant.ofEpochMilli(context.getRequest().getMinDate());
        final int limit = context.getRequest().getLimit();

        return Mono.justOrEmpty(context.getUserId().map(Id::of))
                .flatMap(selfId -> messagingService.getUserDialogs(selfId, minDate, limit)
                        .defaultIfEmpty(new Page<>(Collections.emptyList()))
                        .flatMap(dialogs -> {
                            Mono<Integer> count = Mono.justOrEmpty((int) dialogs.getTotalCount())
                                    .defaultIfEmpty(0);

                            Mono<List<ApiAbsUser>> apiUsers = Flux.fromIterable(dialogs)
                                    .filter(dialog -> dialog.getPeer().isUser())
                                    .map(Dialog::getPeer)
                                    .map(Peer::asPeerId)
                                    .collectList()
                                    .flatMapMany(userService::getUsers)
                                    .map(user -> Optional.ofNullable(userConverter.convert(context.getAuthId(), user)))
                                    .filter(Optional::isPresent)
                                    .map(Optional::get)
                                    .collectList()
                                    .defaultIfEmpty(Collections.emptyList());

                            Mono<List<ApiChat>> apiChats = Flux.fromIterable(dialogs)
                                    .filter(dialog -> dialog.getPeer().isChat())
                                    .map(Dialog::getPeer)
                                    .map(Peer::asPeerId)
                                    .collectList()
                                    .flatMapMany(chatService::getChats)
                                    .map(chat -> Optional.ofNullable(ApiEntityMapper.toApiChat(chat)))
                                    .filter(Optional::isPresent)
                                    .map(Optional::get)
                                    .collectList()
                                    .defaultIfEmpty(Collections.emptyList());

                            Mono<List<ApiDialog>> apiDialogs = Flux.fromIterable(dialogs)
                                    .map(dialog -> Optional.ofNullable(dialogConverter.convert(dialog)))
                                    .filter(Optional::isPresent)
                                    .map(Optional::get)
                                    .collectList()
                                    .defaultIfEmpty(Collections.emptyList());

                            Mono<List<ApiHistoryMessage>> apiMessages = Flux.fromIterable(dialogs)
                                    .map(Dialog::getMessageId)
                                    .collectList()
                                    .flatMapMany(messagingService::getMessages)
                                    .map(message -> Optional.ofNullable(dialogConverter.convert(message)))
                                    .filter(Optional::isPresent)
                                    .map(Optional::get)
                                    .collectList()
                                    .defaultIfEmpty(Collections.emptyList());

                            return Mono.zip(count, apiChats, apiUsers, apiMessages, apiDialogs);
                        })
                        .map(TupleUtils.function(ResponseMessagesLoadDialogs::new)));
    }

    /**
     * Invoked when the client attempt to send typing status.
     *
     * @param context the incoming request instance.
     * @return a {@link Publisher} that completes when this listener has done processing the incoming request, for
     * example returning any {@link Mono}, {@link Flux} or  synchronous code using {@link Mono#fromRunnable(Runnable)}.
     */
    public Mono<ResponseVoid> handleTyping(DispatchContext<RequestMessagesTyping> context) {
        return Mono.justOrEmpty(context.getUserId()).map(Id::of)
                .flatMap(userId -> {
                    final Peer peer = Peer.fromUniqueId(context.getRequest().getPeer().getPeerId());
                    if (peer.isChat()) {
                        // Don't support typing state for chats yet, because of overhead. //TODO needs to implement
                        return Mono.just(new ResponseVoid());
                    } else if (peer.isUser()) {
                        // Allow sending typing status to recipients without checking their relationships for better
                        // performance. The application itself should check their relationships on the client side.
                        final UpdateTyping typing = new UpdateTyping();
                        typing.setPeer(new ApiPeerUser(peer.getPeerId()));
                        typing.setUid(userId.asLong());
                        typing.setTyping(true);

                        return updatesService.forwardWeakUpdate(peer.asPeerId(), typing, Collections.emptySet())
                                .thenReturn(new ResponseVoid());
                    } else {
                        return Mono.just(new ResponseVoid());
                    }
                });
    }
}
