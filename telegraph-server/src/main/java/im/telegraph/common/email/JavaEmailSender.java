package im.telegraph.common.email;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.nio.charset.StandardCharsets;

/**
 * Java mail implementations for sending emails.
 */
@Component
public class JavaEmailSender implements EmailSender {

    private final Logger log = LoggerFactory.getLogger(JavaEmailSender.class);

    private final JavaMailSender javaMailSender;

    public JavaEmailSender(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Override
    public void sendEmail(final Email email) {
        log.debug("Send email['{}']", email);

        // Prepare message using a Spring helper
        final MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, email.isMultipart(), StandardCharsets.UTF_8.name());
            message.setTo(email.getTo());
            message.setFrom(email.getFrom());
            message.setSubject(email.getSubject());
            message.setText(email.getContent(), email.isHtml());
            javaMailSender.send(mimeMessage);
            log.debug("Sent email to User '{}'", email.getTo());
        } catch (MailException | MessagingException e) {
            log.warn("Email could not be sent to user '{}'", email.getTo(), e);
        }
    }
}
