package im.telegraph.server.test.api;

import im.telegraph.rpc.base.Update;

public interface ApiCallback {

    void onApiDie(ProtoApi api);

    void onUpdate(Update update);

}
