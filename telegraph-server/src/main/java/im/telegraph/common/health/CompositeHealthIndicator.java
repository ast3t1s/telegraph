package im.telegraph.common.health;

import java.util.List;
import java.util.Objects;

public class CompositeHealthIndicator implements HealthCheckIndicator {

    private final List<HealthCheckIndicator> delegates;

    public CompositeHealthIndicator(List<HealthCheckIndicator> delegates) {
        this.delegates = Objects.requireNonNull(delegates, "'delegates'");
    }

    @Override
    public boolean isHealthy() {
        return this.delegates.stream().allMatch(HealthCheckIndicator::isHealthy);
    }

    @Override
    public void updateHealthStatus() {
        this.delegates.forEach(HealthCheckIndicator::updateHealthStatus);
    }
}
