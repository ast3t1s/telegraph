package im.telegraph.protocol.cryptography;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * An interface that provides a storage for {@link ServerTrustedKey} that using for encrypt/decrypt
 * incoming and outgoing messages.
 *
 * @author Ast3t1s
 */
public interface TrustedKeyStore {

    /**
     * Returns the {@link ServerTrustedKey} by the provided {@code id}.
     *
     * @param id the trusted key id
     * @return the server trusted key with the specified id;
     */
    Mono<ServerTrustedKey> find(Long id);

    /**
     * Returns the sequence of {@link ServerTrustedKey}.
     *
     * @return the sequence of server trusted keys.
     */
    Flux<ServerTrustedKey> findAll();
}
