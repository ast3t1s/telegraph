package im.telegraph.domain.entity;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * Entity which stores dialog info.
 *
 * @author Vsevolod Bondarenko
 */
public class Dialog implements Serializable {

    private static final long serialVersionUID = 1L;

    private final Peer peer;
    private final Id userId;
    private final Id messageId;
    private final Instant timestamp;
    private final int unreadCount;

    /**
     * Constructs an {@code DialogCommon} with an associated data.
     *
     * @param peer            Identifier of the entity
     * @param userId          Identifier of the dialog {@link Id}.
     * @param timestamp       Point in time (Unix timestamp) when message was sent.
     */
    @lombok.Builder(builderClassName = "Builder")
    Dialog(final Peer peer, final Id userId, final Id messageId, final Instant timestamp, final int unreadCount) {
        this.peer = Objects.requireNonNull(peer);
        this.userId = Objects.requireNonNull(userId);
        this.messageId = Objects.requireNonNull(messageId);
        this.timestamp = Objects.requireNonNull(timestamp);
        this.unreadCount = unreadCount;
    }

    /**
     * Gets the identifier of the common dialog entity.
     *
     * @return identifier of the common dialog entity
     */
    public Peer getPeer() {
        return peer;
    }

    /**
     * Gets the identifier of the user.
     *
     * @return identifier of the user
     */
    public Id getUserId() {
        return userId;
    }

    /**
     * Gets the identifier of the message.
     *
     * @return identifier of the message
     */
    public Id getMessageId() {
        return messageId;
    }

    /**
     * Gets the timestamp of the last message.
     *
     * @return timestamp of the last message
     */
    public Instant getTimestamp() {
        return timestamp;
    }

    /**
     * Gets the count of the unread messages.
     *
     * @return the count of the unread messages
     */
    public int getUnreadCount() {
        return unreadCount;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        Dialog that = (Dialog) object;
        return Objects.equals(peer, that.peer) &&
                Objects.equals(userId, that.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(peer, userId);
    }

    @Override
    public String toString() {
        return "Dialog{" +
                "peer=" + peer +
                ", userId=" + userId +
                ", messageId=" + messageId +
                ", timestamp=" + timestamp +
                ", unreadCount=" + unreadCount +
                '}';
    }
}
