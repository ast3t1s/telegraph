package im.telegraph.pubsub.internal;

/**
 * @author Ast3t1s
 */
public class Publish extends PubSubPayload<Publish.Data> {

    public static final int OP = 4;

    public Publish(String topic, Object payload) {
        this(OP, new Data(topic, payload));
    }

    public Publish(int op, Data data) {
        super(op, data);
    }

    public static class Data {

        private final String topic;
        private final Object payload;

        public Data(String topic, Object payload) {
            this.topic = topic;
            this.payload = payload;
        }

        public String getTopic() {
            return topic;
        }

        public Object getPayload() {
            return payload;
        }

        @Override
        public String toString() {
            return "Data{" +
                    "topic='" + topic + '\'' +
                    ", payload=" + payload +
                    '}';
        }
    }
} 
