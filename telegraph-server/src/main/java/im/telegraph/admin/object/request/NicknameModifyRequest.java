package im.telegraph.admin.object.request;

/**
 * @author Ast3t1s
 */
public class NicknameModifyRequest {

    private String nick;

    public String getNick() {
        return nick;
    }

    @Override
    public String toString() {
        return "NicknameModifyRequest{" +
                "nick='" + nick + '\'' +
                '}';
    }
}
