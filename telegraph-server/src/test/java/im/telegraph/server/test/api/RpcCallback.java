package im.telegraph.server.test.api;

import im.telegraph.server.test.RpcException;
import im.telegraph.rpc.base.Response;

public interface RpcCallback<T extends Response> {

    void onResult(T result);

    void onError(RpcException exception);

}
