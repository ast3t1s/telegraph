package im.telegraph.domain.repository.rdbms;

import im.telegraph.common.rdbms.RdbmsOperations;
import im.telegraph.domain.entity.AuthSession;
import im.telegraph.domain.entity.Id;
import im.telegraph.domain.repository.AuthSessionRepository;
import io.r2dbc.spi.*;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Ast3t1s
 */
@Repository
public class RdbmsAuthSessionRepository extends AbstractRdbmsRepository implements AuthSessionRepository {

    private static final String FIELDS = "id, user_id, auth_id, device_model, platform, system_version, app_name," +
            " app_version, created_at, ip_address, country, region, deleted_at";

    private static final String INSERT_AUTH_SESSION_QUERY = "INSERT INTO (" + FIELDS + ") " +
            "VALUES (:id, :userId, :authId, :deviceModel, :platform, :systemVersion, :appName, :appVersion, :createdAt," +
            " :ipAddress, :count, :region, :deletedAt)";

    private static final String GET_SESSION_QUERY =
            "SELECT " + FIELDS + " FROM authorizations WHERE id = :id";

    private static final String GET_DEVICE_SESSION_QUERY = "SELECT " + FIELDS + " FROM authorizations WHERE user_id =" +
            " :userId AND auth_id = :authId AND deleted_at IS NULL";

    private static final String GET_USER_SESSIONS_QUERY = "SELECT " + FIELDS + " FROM authorizations " +
            "WHERE user_id = :userId AND deleted_at IS NULL";

    private static final String FIND_DEVICE_SESSION = "SELECT " + FIELDS + " FROM authorizations a WHERE " +
            "a.auth_id = :authId AND deleted_at IS NULL";

    private static final String DELETE_SESSIONS_QUERY = "DELETE FROM auth_sessions WHERE id IN (:ids)";

    private final RdbmsOperations rdbmsOperations;

    private final BiFunction<Row, RowMetadata, AuthSession> rowMapper = new AuthSessionRowMapper();
    private final Function<AuthSession, Map<String, Object>> parameterMapper = new AuthSessionParameterMapper();

    public RdbmsAuthSessionRepository(ConnectionFactory connectionFactory) {
        super(connectionFactory);
        this.rdbmsOperations = RdbmsOperations.create(connectionFactory);
    }

    @Override
    public Mono<AuthSession> insert(AuthSession authSession) {
        return rdbmsOperations.withHandle(handle -> handle.createUpdate(INSERT_AUTH_SESSION_QUERY)
                .bindMap(parameterMapper.apply(authSession))
                .execute()
                .then(Mono.just(authSession)))
                .next();
    }

    @Override
    public Mono<AuthSession> findById(Id hash) {
        return rdbmsOperations.withHandle(handle -> handle.createQuery(GET_SESSION_QUERY)
                .bind("id", hash.asLong())
                .mapRow(rowMapper))
                .next();
    }

    @Override
    public Mono<AuthSession> findByAuthIdAndUserId(Long authId, Id userId) {
        return rdbmsOperations.withHandle(handle -> handle.createQuery(GET_DEVICE_SESSION_QUERY)
                .bind("authId", authId)
                .bind("userId", userId.asLong())
                .mapRow(rowMapper))
                .next();
    }

    @Override
    public Mono<AuthSession> findByAuthIdAndDeletedAtIsNull(Long authId) {
        return rdbmsOperations.withHandle(handle -> handle.createQuery(FIND_DEVICE_SESSION)
                .bind("authId", authId)
                .mapRow(rowMapper))
                .next();
    }

    @Override
    public Flux<AuthSession> findAllByUserIdAndDeletedAtIsNull(Id userId) {
        return rdbmsOperations.withHandle(handle -> handle.createQuery(GET_USER_SESSIONS_QUERY)
                .bind("userId", userId.asLong())
                .mapRow(rowMapper));
    }

    @Override
    public Mono<Long> deleteByIdsIn(Collection<Id> ids) {
        return rdbmsOperations.withHandle(handle -> handle.createUpdate(DELETE_SESSIONS_QUERY)
                .bind("ids", ids.stream().map(Id::asLong).collect(Collectors.toList()))
                .execute())
                .next();
    }

    private static class AuthSessionParameterMapper implements Function<AuthSession, Map<String, Object>> {

        @Override
        public Map<String, Object> apply(AuthSession session) {
            final Map<String, Object> parameters = new HashMap<>();
            parameters.put("id", Parameters.in(R2dbcType.BIGINT, session.getId().asLong()));
            parameters.put("authId", Parameters.in(R2dbcType.BIGINT, session.getAuthId()));
            parameters.put("userId", Parameters.in(R2dbcType.BIGINT, session.getUserId().asLong()));
            parameters.put("deviceModel", Parameters.in(R2dbcType.VARCHAR, session.getDeviceModel()));
            parameters.put("platform", Parameters.in(R2dbcType.VARCHAR, session.getPlatform()));
            parameters.put("systemVersion", Parameters.in(R2dbcType.VARCHAR, session.getSystemVersion()));
            parameters.put("appName", Parameters.in(R2dbcType.VARCHAR, session.getAppName()));
            parameters.put("appVersion", Parameters.in(R2dbcType.VARCHAR, session.getAppVersion()));
            parameters.put("country", Parameters.in(R2dbcType.VARCHAR, session.getCountry()));
            parameters.put("region", Parameters.in(R2dbcType.VARCHAR, session.getRegion()));
            parameters.put("ipAddress", Parameters.in(R2dbcType.VARCHAR, session.getIp()));
            parameters.put("createdAt", Parameters.in(R2dbcType.TIMESTAMP,
                    LocalDateTime.ofInstant(session.getCreatedAt(), ZoneOffset.UTC)));
            parameters.put("deletedAt", Parameters.in(R2dbcType.TIMESTAMP,
                    session.getDeletedAt().map(ts -> LocalDateTime.ofInstant(ts, ZoneOffset.UTC)).orElse(null)));
            return parameters;
        }
    }

    private static class AuthSessionRowMapper implements BiFunction<Row, RowMetadata, AuthSession> {

        @Override
        public AuthSession apply(Row row, RowMetadata metadata) {
            return AuthSession.builder()
                    .id(Id.of(row.get("id", Long.class)))
                    .authId(row.get("auth_id", Long.class))
                    .userId(Id.of(row.get("user_id", Long.class)))
                    .deviceModel(row.get("device_model", String.class))
                    .platform(row.get("platform", String.class))
                    .systemVersion(row.get("system_version", String.class))
                    .appName(row.get("app_name", String.class))
                    .appVersion(row.get("app_version", String.class))
                    .country(row.get("county", String.class))
                    .region(row.get("region", String.class))
                    .ip(row.get("ip_address", String.class))
                    .createdAt(row.get("created_at", LocalDateTime.class).toInstant(ZoneOffset.UTC))
                    .deletedAt(row.get("deleted_at", LocalDateTime.class).toInstant(ZoneOffset.UTC))
                    .build();
        }
    }
}
