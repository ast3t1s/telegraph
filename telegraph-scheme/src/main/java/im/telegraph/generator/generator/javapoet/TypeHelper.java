package im.telegraph.generator.generator.javapoet;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.TypeName;
import im.telegraph.generator.generator.Constants;
import im.telegraph.generator.type.SchemeEnum;
import im.telegraph.generator.type.SchemeUpdate;

/**
 * @author Vsevolod Bondarenko
 */
public class TypeHelper {

    public static TypeName getStructName(String struct) {
        return ClassName.get(Constants.PACKAGE_API, Constants.ENTITY_PREFIX + struct);
    }

    public static String getClassStructName(String e) {
        return Constants.ENTITY_PREFIX + e;
    }

    public static String getEnumName(String e) {
        return Constants.ENTITY_PREFIX + e;
    }

    public static String getEnumRecordName(SchemeEnum.Record record) {
        // TODO: Implement dividing to words
        return record.getName().toUpperCase();
    }

    public static String getUpdateName(SchemeUpdate e) {
        return "Update" + e.getName();
    }

    public static String getUpdateBoxName(String e) {
        return e;
    }

    public static String getResponseName(String name) {
        return "Response" + name;
    }

    public static String getResponseName(String category, String name) {
        return "Response" + capitalizeFirstLetter(category) + name;
    }

    public static String getAnonymousResponseName(String methodName) {
        if (methodName.startsWith("Request")) {
            return "Response" + methodName.substring("Request".length());
        }
        return "Response" + methodName;
    }

    public static String getAnonymousResponseName(String category, String methodName) {
        if (methodName.startsWith("Request")) {
            return "Response" + capitalizeFirstLetter(category) + methodName.substring("Request".length());
        }
        return "Response" + capitalizeFirstLetter(category) + methodName;
    }

    public static String getRequestName(String name) {
        if (name.startsWith("Request")) {
            return name;
        }
        return "Request" + name;
    }

    public static String getRequestName(String section, String name) {
        if (name.startsWith("Request")) {
            return name;
        }
        return "Request" + capitalizeFirstLetter(section) + name;
    }

    private static String capitalizeFirstLetter(String str) {
        return str != null ? (str.substring(0, 1).toUpperCase() + str.substring(1).toLowerCase()) : null;
    }

    public static String getRequestsName(String v) {
        return v.substring(0, 1).toLowerCase() + v.substring(1);
    }

    public static String getGetterName(String v) {
        return "get" + v.substring(0, 1).toUpperCase() + v.substring(1);
    }

    public static String getSetterName(String v) {
        return "set" + v.substring(0, 1).toUpperCase() + v.substring(1);
    }

    public static String getBoolGetterName(String v) {
        return v;
    }
} 
