package im.telegraph.admin.route;

import org.reactivestreams.Publisher;
import reactor.netty.http.server.HttpServerRequest;
import reactor.netty.http.server.HttpServerResponse;

/**
 * @author Ast3t1s
 */
public class Route {

    @FunctionalInterface
    public interface Action {

        Publisher<Void> handleRequest(HttpServerRequest request, HttpServerResponse response);
    }

    public static class Builder {

        @FunctionalInterface
        public interface RequireEndpoint {

            RequireAction endpoint(Endpoint endpoint);
        }

        public interface RequireAction {
            ReadyToBuild action(Action action);
        }

        public static class ReadyToBuild {

            private final Endpoint endpoint;

            private final Action action;

            ReadyToBuild(Endpoint endpoint, Action action) {
                this.endpoint = endpoint;
                this.action = action;
            }

            public Route corsHeaders() {
                return build(Routes.corsHeaders(action));
            }

            private Route build(Action action) {
                return new Route(endpoint, actionWithParameterResolving(action));
            }

            Action actionWithParameterResolving(Action action) {
                return (req, res) ->
                        action.handleRequest(
                                req.paramsResolver(s -> endpoint.getUriMatcher().match(s)),
                                res);
            }
        }
    }

    public static Builder.RequireEndpoint builder() {
        return endpoint -> action -> new Builder.ReadyToBuild(endpoint, action);
    }

    private final Endpoint endpoint;
    private final Action action;

    private Route(Endpoint endpoint, Action action) {
        this.endpoint = endpoint;
        this.action = action;
    }

    public Endpoint getEndpoint() {
        return endpoint;
    }

    public Action getAction() {
        return action;
    }

    public boolean matches(HttpServerRequest request) {
        return endpoint.matches(request);
    }
} 
