package im.telegraph.service;

import im.telegraph.common.property.domain.MessageProperties;
import im.telegraph.config.TelegraphProperties;
import im.telegraph.domain.entity.DialogMarker;
import im.telegraph.domain.entity.Id;
import im.telegraph.domain.entity.Message;
import im.telegraph.domain.entity.Peer;
import im.telegraph.domain.repository.DialogMarkerRepository;
import im.telegraph.exceptions.RpcErrors;
import im.telegraph.exceptions.RpcException;
import im.telegraph.service.internal.TaskAggregator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuples;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.time.Instant;

/**
 * @author Ast3t1s
 */
@Service
public class DialogMarkerService {

    private static final Logger log = LoggerFactory.getLogger(DialogMarkerService.class);

    private final DialogMarkerRepository dialogMarkerRepository;
    private final TaskAggregator<Tuple2<Id, Peer>, DialogMarkerTask> dialogMarkerTaskAggregator;

    private final MessageProperties properties;

    public DialogMarkerService(DialogMarkerRepository dialogMarkerRepository, TelegraphProperties properties) {
        this.dialogMarkerRepository = dialogMarkerRepository;
        this.properties = properties.getDomain().getMessage();
        this.dialogMarkerTaskAggregator = new TaskAggregator<>(properties.getDomain().getMessage()
                .getDialogMarkerInterval(), DialogMarkerService::aggregate, this::flushDialogMarkerTask);
    }

    @PostConstruct
    public void start() {
        dialogMarkerTaskAggregator.start();
    }

    @PreDestroy
    public void stop() {
        dialogMarkerTaskAggregator.stop();
    }

    /**
     * Get information about dialog markers
     *
     * @return A {@link Flux} that continually emits the {@link Message messages}. If an error is received,
     * it is emitted through the {@code Flux}.
     */
    public Flux<DialogMarker> getDialogMarkers(final Id userId, final Peer peer, final Instant timestamp) {
        if (peer.isChat()) {
            return dialogMarkerRepository.findAllByPeerAndTimestampGreaterThanEqual(peer, timestamp);
        } else if (peer.isUser()) {
            return dialogMarkerRepository.findByUserIdAndPeer(userId, peer)
                    .concatWith(dialogMarkerRepository.findByUserIdAndPeer(peer.asPeerId(), Peer.ofUser(userId)));
        } else {
            return Flux.error(RpcException.create(RpcErrors.INVALID_PEER));
        }
    }

    /**
     * Sets the cursor in a dialog. This method moves the cursor in a dialog.
     *
     * @param userId User identifier who set the cursor.
     * @param peer   the {@link Peer} of the dialog.
     * @param date   the unique identifier of message you want marked as received in this dialog.
     * @return A {@link Mono} where, upon successful completion, emits nothing; indicating the user has been start
     * password reset. If an error is received, it is emitted through the {@code Mono}.
     */
    public Mono<Void> setDialogMarker(final Id userId, final Peer peer, final DialogMarker.State state, final Instant date) {
        if (properties.isDialogMarkerAsync()) {
            return dialogMarkerTaskAggregator.putTask(Tuples.of(userId, peer),
                    new DialogMarkerTask(userId, peer, state, date));
        } else {
            return dialogMarkerRepository.save(DialogMarker.of(userId, peer, state, date));
        }
    }

    private Mono<Void> flushDialogMarkerTask(final DialogMarkerTask task) {
        final DialogMarker dialogMarker;
        if (task.getState() == DialogMarker.State.RECEIVED) {
            dialogMarker = DialogMarker.received(task.getUserId(), task.getPeer(), task.getTimestamp());
        }  else if (task.getState() == DialogMarker.State.READ) {
            dialogMarker = DialogMarker.read(task.getUserId(), task.getPeer(), task.getTimestamp());
        } else {
            dialogMarker = DialogMarker.of(task.getUserId(), task.getPeer(), task.getState(), task.getTimestamp());
        }
        return dialogMarkerRepository.save(dialogMarker);
    }

    private static DialogMarkerTask aggregate(final DialogMarkerTask currentTask,
                                              final DialogMarkerTask nextTask) {
        return nextTask; // Simply return the new task (replace the old one)
    }

    private static class DialogMarkerTask {

        private final Id userId;
        private final Peer peer;
        private final DialogMarker.State state;
        private final Instant timestamp;

        DialogMarkerTask(Id userId, Peer peer, DialogMarker.State state, Instant timestamp) {
            this.userId = userId;
            this.peer = peer;
            this.state = state;
            this.timestamp = timestamp;
        }

        public Id getUserId() {
            return userId;
        }

        public Peer getPeer() {
            return peer;
        }

        public DialogMarker.State getState() {
            return state;
        }

        public Instant getTimestamp() {
            return timestamp;
        }
    }
} 
