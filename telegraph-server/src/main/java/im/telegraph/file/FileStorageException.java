package im.telegraph.file;

/**
 * {@link FileStorageException} indicates that a object store operation could not be performed.
 *
 * @author Vsevolod Bondarenko
 */
@SuppressWarnings("serial")
public class FileStorageException extends RuntimeException {

    /**
     * Constructs a new {@link FileStorageException} with the specified message.
     *
     * @param message The detail message.
     */
    public FileStorageException(String message) {
        super(message);
    }

    /**
     * Constructs a new {@link FileStorageException} with the specified message and the {@link Throwable}
     * that cause the object store operation to fail.
     *
     * @param message The detail message.
     * @param cause   The cause.
     */
    public FileStorageException(String message, Throwable cause) {
        super(message, cause);
    }
} 
