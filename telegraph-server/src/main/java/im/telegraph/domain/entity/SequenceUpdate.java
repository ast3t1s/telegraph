package im.telegraph.domain.entity;

import im.telegraph.handlers.internal.SerialMapping;

import java.io.Serializable;
import java.time.Instant;
import java.util.Arrays;
import java.util.Objects;

public class SequenceUpdate implements Serializable {

    private static final long serialVersionUID = 1L;

    private final Id userId;
    private final Integer sequence;
    private final Long timestamp;
    private final byte[] mapping;

    /**
     * Constructs a {@code SeqStateUpdate} with an associated data.
     *
     * @param mapping   Update data, in bytes.
     * @param userId    Identifier of the user.
     * @param sequence  Sequence of the update.
     * @param timestamp Point in time (Unix timestamp) when update was created.
     */
    public SequenceUpdate(final Id userId, final Integer sequence, final Long timestamp, final byte[] mapping) {
        this.userId = Objects.requireNonNull(userId);
        this.sequence = Objects.requireNonNull(sequence);
        this.timestamp = Objects.requireNonNull(timestamp);
        this.mapping = Objects.requireNonNull(mapping);
    }

    /**
     * Factory method for create a {@code SeqStateUpdate} object.
     *
     * @param userId    the ID of the user.
     * @param sequence  the sequence of the update.
     * @param timestamp the timestamp.
     * @param mapping   the update body.
     */
    public static SequenceUpdate of(Id userId, Integer sequence, Instant timestamp, SerialMapping mapping) {
        return new SequenceUpdate(userId, sequence, timestamp.toEpochMilli(), mapping.toByteArray());
    }

    public static SequenceUpdate of(Id userId, Integer sequence, Instant timestamp, byte[] mapping) {
        return new SequenceUpdate(userId, sequence, timestamp.toEpochMilli(), mapping);
    }

    /**
     * Gets the ID of the user.
     *
     * @return the ID of the user.
     */
    public Id getUserId() {
        return userId;
    }

    /**
     * Gets the sequence of the update.
     *
     * @return the sequence of the update.
     */
    public Integer getSequence() {
        return sequence;
    }

    /**
     * Gets the timestamp of the update.
     *
     * @return the timestamp of the update.
     */
    public Instant getTimestamp() {
        return Instant.ofEpochMilli(timestamp);
    }

    /**
     * Gets the body of the update.
     *
     * @return the body of the update.
     */
    public byte[] getMapping() {
        return mapping;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        SequenceUpdate that = (SequenceUpdate) object;
        return Objects.equals(userId, that.userId) &&
                Objects.equals(sequence, that.sequence);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, sequence);
    }

    @Override
    public String toString() {
        return "SequenceUpdate{" +
                "userId=" + userId +
                ", sequence=" + sequence +
                ", timestamp=" + timestamp +
                ", mapping=" + Arrays.toString(mapping) +
                '}';
    }
}
