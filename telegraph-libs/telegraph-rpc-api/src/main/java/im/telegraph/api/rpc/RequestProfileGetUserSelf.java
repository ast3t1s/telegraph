package im.telegraph.api.rpc;

import static im.telegraph.engine.serialization.StreamUtils.*;

import im.telegraph.engine.serialization.StreamReader;
import im.telegraph.engine.serialization.StreamWriter;
import im.telegraph.engine.serialization.io.IOContext;
import im.telegraph.rpc.base.Request;
import java.io.IOException;
import java.lang.Override;
import java.lang.String;

/**
 * Generated by the Telegraph API Scheme generator. DO NOT EDIT!
 * @author Telegraph Scheme Generator
 */
public class RequestProfileGetUserSelf extends Request<ResponseProfileGetUserSelf> {
    public static final int CLASS_ID = 0xde;

    public RequestProfileGetUserSelf() {
    }

    public static RequestProfileGetUserSelf fromBytes(byte[] data) throws IOException {
        return IOContext.fromByteArray(new RequestProfileGetUserSelf(), data);
    }

    @Override
    public int getHeader() {
        return CLASS_ID;
    }

    @Override
    public void serialize(StreamWriter writer) throws IOException {
    }

    @Override
    public void deserialize(StreamReader reader) throws IOException {
    }

    @Override
    public String toString() {
        String res = "rpc GetUserSelf{";
        res += "}";
        return res;
    }
}
