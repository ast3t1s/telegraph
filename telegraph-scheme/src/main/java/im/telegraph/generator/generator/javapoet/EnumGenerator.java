package im.telegraph.generator.generator.javapoet;

import com.squareup.javapoet.*;
import im.telegraph.generator.generator.Constants;
import im.telegraph.generator.generator.SchemeGenerator;
import im.telegraph.generator.type.SchemeDefinition;
import im.telegraph.generator.type.SchemeEnum;

import javax.lang.model.element.Modifier;
import java.io.File;
import java.io.IOException;

public class EnumGenerator implements SchemeGenerator {

    @Override
    public void generate(SchemeDefinition definition, String out) throws IOException {
        long start = System.currentTimeMillis();
        for (SchemeEnum e : definition.getAllEnums()) {
            String javaName = TypeHelper.getEnumName(e.getName());

            TypeSpec.Builder clazz = TypeSpec.enumBuilder(javaName)
                    .addJavadoc(Constants.NOTICE)
                    .addJavadoc(Constants.DEFAULT_AUTHOR)
                    .addModifiers(Modifier.PUBLIC);

            for (SchemeEnum.Record r : e.getRecord()) {
                clazz.addEnumConstant(TypeHelper.getEnumRecordName(r),
                        TypeSpec.anonymousClassBuilder("$L", r.getId()).build());
            }
            clazz.addEnumConstant("UNSUPPORTED_VALUE", TypeSpec.anonymousClassBuilder("$L", -1)
                    .build());

            clazz.addField(FieldSpec.builder(int.class, "value", Modifier.PRIVATE, Modifier.FINAL)
                    .build());

            clazz.addMethod(MethodSpec.constructorBuilder()
                    .addParameter(int.class, "value")
                    .addStatement("this.$N = $N", "value", "value")
                    .build());

            clazz.addMethod(MethodSpec.methodBuilder("getValue")
                    .addModifiers(Modifier.PUBLIC)
                    .returns(int.class)
                    .addStatement("return value")
                    .build());

            MethodSpec.Builder parseBuilder  = MethodSpec.methodBuilder("parse")
                    .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                    .returns(ClassName.get("", javaName))
                    .addParameter(int.class, "value")
                    .addException(IOException.class)
                    .beginControlFlow("switch (value)");

            for (SchemeEnum.Record r : e.getRecord()) {
                parseBuilder.addStatement("case $L: return $L.$L", r.getId(), javaName, TypeHelper.getEnumRecordName(r));
            }
            parseBuilder.addStatement("default: return $L.UNSUPPORTED_VALUE", javaName);
            parseBuilder.endControlFlow();

            clazz.addMethod(parseBuilder.build());

            JavaFile.builder(Constants.PACKAGE_API, clazz.build())
                    .indent("    ")
                    .build().writeTo(new File(out));
        }
        System.out.println("Total enums " + definition.getAllEnums().size() + " generated in "
                + (System.currentTimeMillis() - start) + " ms");
    }
}
