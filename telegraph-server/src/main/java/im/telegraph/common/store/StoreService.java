package im.telegraph.common.store;

import reactor.core.publisher.Mono;

/**
 * Store abstract factory that can be manually created or loaded through a {@link StoreServiceLoader} as a Java service.
 *
 * @see NoOpStoreService
 */
public interface StoreService {

    /**
     * Returns an arbitrary order for this service. This is used for automated service discovery in the case
     * that multiple services are present. Conventions: 0 = neutral priority,
     * {
     * @link Integer#MAX_VALUE} = lowest priority, {@link Integer#MIN_VALUE} = highest priority.
     *
     * @return The priority of this service, 0 by default.
     */
    default int order() {
        return 0;
    }

    /**
     * This is called to provide a new store instance for the provided configuration.
     *
     * @param keyClass The class of the keys.
     * @param valueClass The class of the values.
     * @param <K> The key type which provides a 1:1 mapping to the value type. This type is also expected to be
     * {@link Comparable} in order to allow for range operations.
     * @param <V> The value type, these follow
     * @return The instance of the store.
     */
    <K extends Comparable<K>, V> Store<K, V> provideGenericStore(Class<K> keyClass, Class<V> valueClass);

    /**
     * This is a lifecycle method called to signal that a store should dispose of any resources due to
     * an abrupt close (hard reconnect or disconnect).
     *
     * @return A mono, whose completion signals that resources have been released successfully.
     */
    Mono<Void> dispose();
} 
