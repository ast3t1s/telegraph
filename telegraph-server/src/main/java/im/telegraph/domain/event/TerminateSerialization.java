package im.telegraph.domain.event;

import im.telegraph.internal.remote.serializer.SerializationException;
import im.telegraph.internal.remote.serializer.Serializer;

/**
 * @author Ast3t1s
 */
public class TerminateSerialization implements Serializer {

    @Override
    public int serializerId() {
        return 31;
    }

    @Override
    public byte[] toBinary(Object object) throws SerializationException {
        return new byte[0];
    }

    @Override
    public Object fromBinary(byte[] array) throws SerializationException {
        return new TerminateSessionEvent(99999L, 11111L);
    }
}
