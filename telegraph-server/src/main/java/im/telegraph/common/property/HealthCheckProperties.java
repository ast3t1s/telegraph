package im.telegraph.common.property;

import org.springframework.boot.convert.DataSizeUnit;
import org.springframework.boot.convert.DurationUnit;
import org.springframework.util.unit.DataSize;
import org.springframework.util.unit.DataUnit;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.time.Duration;
import java.time.temporal.ChronoUnit;

public class HealthCheckProperties {

    /**
     * Properties for CPU heath check indicator.
     */
    private final CPUHealthProperties cpu = new CPUHealthProperties();

    /**
     * Properties for memory check indicator.
     */
    private final MemoryHealthProperties memory = new MemoryHealthProperties();

    /**
     *  Interval for rerunning the HealthCheck scheduler
     */
    @DurationUnit(value = ChronoUnit.SECONDS)
    private Duration checkInterval = Duration.ofSeconds(25);

    public CPUHealthProperties getCpu() {
        return cpu;
    }

    public MemoryHealthProperties getMemory() {
        return memory;
    }

    public Duration getCheckInterval() {
        return checkInterval;
    }

    public void setCheckInterval(Duration checkInterval) {
        this.checkInterval = checkInterval;
    }

    public static class CPUHealthProperties {

        /**
         * Unhealthy Load threshold in percentage.
         */
        @Min(1)
        @Max(100)
        private int unhealthyLoadThreshold = 95;

        /**
         * Retry count.
         */
        @Min(0)
        private int retryCount;

        public int getUnhealthyLoadThreshold() {
            return unhealthyLoadThreshold;
        }

        public void setUnhealthyLoadThreshold(int unhealthyLoadThreshold) {
            this.unhealthyLoadThreshold = unhealthyLoadThreshold;
        }

        public int getRetryCount() {
            return retryCount;
        }

        public void setRetryCount(int retryCount) {
            this.retryCount = retryCount;
        }
    }

    public static class MemoryHealthProperties {

        /**
         * Max available memory in percentage.
         */
        @Max(100)
        @Min(1)
        private int maxAvailableMemory = 95;

        /**
         * Max available direct memory in percentage.
         */
        @Max(100)
        @Min(1)
        private int maxAvailableDirectMemory = 95;

        /**
         * Min free memory.
         */
        @DataSizeUnit(value = DataUnit.MEGABYTES)
        private DataSize minFreeSystemMemory = DataSize.ofMegabytes(128);

        /**
         * Memory usage warning in percentage.
         */
        private int directMemoryWarningThreshold = 50;

        /**
         * Memory warning interval.
         */
        private Duration memoryWarningInterval = Duration.ofSeconds(10);

        /**
         * If the used memory has used the reserved memory specified by maxAvailableMemoryPercentage and minFreeSystemMemoryBytes,
         * try to start GC when the used heap memory exceeds the max heap memory of the percentage.
         */
        private int heapMemoryGCThreshold = 60;

        /**
         * Min heap memory GC interval.
         */
        private Duration minHeapMemoryGCInterval = Duration.ofSeconds(10);

        public int getMaxAvailableMemory() {
            return maxAvailableMemory;
        }

        public void setMaxAvailableMemory(int maxAvailableMemory) {
            this.maxAvailableMemory = maxAvailableMemory;
        }

        public int getMaxAvailableDirectMemory() {
            return maxAvailableDirectMemory;
        }

        public void setMaxAvailableDirectMemory(int maxAvailableDirectMemory) {
            this.maxAvailableDirectMemory = maxAvailableDirectMemory;
        }

        public DataSize getMinFreeSystemMemory() {
            return minFreeSystemMemory;
        }

        public void setMinFreeSystemMemory(DataSize minFreeSystemMemory) {
            this.minFreeSystemMemory = minFreeSystemMemory;
        }

        public int getDirectMemoryWarningThreshold() {
            return directMemoryWarningThreshold;
        }

        public void setDirectMemoryWarningThreshold(int directMemoryWarningThreshold) {
            this.directMemoryWarningThreshold = directMemoryWarningThreshold;
        }

        public Duration getMemoryWarningInterval() {
            return memoryWarningInterval;
        }

        public void setMemoryWarningInterval(Duration memoryWarningInterval) {
            this.memoryWarningInterval = memoryWarningInterval;
        }

        public int getHeapMemoryGCThreshold() {
            return heapMemoryGCThreshold;
        }

        public void setHeapMemoryGCThreshold(int heapMemoryGCThreshold) {
            this.heapMemoryGCThreshold = heapMemoryGCThreshold;
        }

        public Duration getMinHeapMemoryGCInterval() {
            return minHeapMemoryGCInterval;
        }

        public void setMinHeapMemoryGCInterval(Duration minHeapMemoryGCInterval) {
            this.minHeapMemoryGCInterval = minHeapMemoryGCInterval;
        }
    }
}
