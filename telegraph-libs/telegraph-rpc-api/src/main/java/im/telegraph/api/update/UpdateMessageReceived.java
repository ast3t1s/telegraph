package im.telegraph.api.update;

import static im.telegraph.engine.serialization.StreamUtils.*;

import im.telegraph.api.ApiPeer;
import im.telegraph.engine.serialization.StreamReader;
import im.telegraph.engine.serialization.StreamWriter;
import im.telegraph.engine.serialization.io.IOContext;
import im.telegraph.rpc.base.Update;
import java.io.IOException;
import java.lang.Override;
import java.lang.String;

/**
 * Generated by the Telegraph API Scheme generator. DO NOT EDIT!
 * @author Telegraph Scheme Generator
 */
public class UpdateMessageReceived extends Update {
    public static final int CLASS_ID = 0x36;

    protected ApiPeer peer;

    protected long startDate;

    public UpdateMessageReceived() {
    }

    public UpdateMessageReceived(ApiPeer peer, long startDate) {
        this.peer = peer;
        this.startDate = startDate;
    }

    public static UpdateMessageReceived fromBytes(byte[] data) throws IOException {
        return IOContext.fromByteArray(new UpdateMessageReceived(), data);
    }

    public ApiPeer getPeer() {
        return this.peer;
    }

    public void setPeer(ApiPeer peer) {
        this.peer = peer;
    }

    public long getStartDate() {
        return this.startDate;
    }

    public void setStartDate(long startDate) {
        this.startDate = startDate;
    }

    @Override
    public void deserialize(StreamReader reader) throws IOException {
        this.peer = ApiPeer.fromBytes(reader.readByteArray(1));
        this.startDate = reader.readLong(2);
    }

    @Override
    public void serialize(StreamWriter writer) throws IOException {
        if (this.peer == null) {
            throw new IOException();
        }
        writer.writeBytes(1, this.peer.buildContainer());
        writer.writeLong(2, this.startDate);
    }

    @Override
    public String toString() {
        String res = "update MessageReceived{";
        res += "peer=" + this.peer;
        res += ", startDate=" + this.startDate;
        res += "}";
        return res;
    }

    @Override
    public int getHeader() {
        return CLASS_ID;
    }
}
