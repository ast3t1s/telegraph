package im.telegraph.common.property.cluster.transport;

import im.telegraph.common.property.TlsProperties;
import im.telegraph.common.property.cluster.FailureDetectorProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.boot.convert.DataSizeUnit;
import org.springframework.util.unit.DataSize;
import org.springframework.util.unit.DataUnit;

import java.time.Duration;

/**
 * @author Ast3t1s
 */
@Getter
@Setter
@Accessors(chain = true)
public class RemoteTransportProperties {

    /**
     * The external host of the current cluster member
     */
    private String externalHost;

    /**
     * The external port of the current cluster member.
     */
    private Integer externalPort;

    /**
     * Hostname clients should connect to. Can be set to an ip, hostname or one of the following special values:
     */
    private String hostname = "0.0.0.0";

    /**
     * The default remote server port clients should connect to.
     * Default is 7515, use 0 if you want a random available port
     */
    private Integer port = 7516;

    /**
     * Time to wait for WS/TCP to bind
     */
    private Duration bindTimeout = Duration.ofMillis(3_000);

    /**
     * Timeout of establishing outbound connections.
     */
    private Duration connectionTimeout = Duration.ofMillis(5_000);

    /**
     * Receive timeout - the task returned by the send and receive
     * method will be canceled when this timeout expires. If {@code Duration}
     * is zero or negative means task will never expire.
     */
    private Duration receiveTimeout = Duration.ofMillis(30_000);

    /**
     * Maximum serialized message size for the large messages, including header data.
     */
    @DataSizeUnit(DataUnit.MEGABYTES)
    private DataSize maxFrameSize = DataSize.ofMegabytes(2);

    @NestedConfigurationProperty
    private final FailureDetectorProperties failureDetector = new FailureDetectorProperties();

    @NestedConfigurationProperty
    private final Retry retry = new Retry();

    @NestedConfigurationProperty
    private TlsProperties ssl = new TlsProperties();

    public static class Retry {

        /**
         * Whether or not retry is enabled.
         */
        private boolean enabled = false;

        /**
         * Number of retries.
         */
        private long maxRetries = 3;

        /**
         * Properties for Reactor Retry backoffs in router connection.
         */
        private Duration backoff = Duration.ofSeconds(10);

        public boolean isEnabled() {
            return enabled;
        }

        public void setEnabled(boolean enabled) {
            this.enabled = enabled;
        }

        public long getMaxRetries() {
            return maxRetries;
        }

        public void setMaxRetries(long maxRetries) {
            this.maxRetries = maxRetries;
        }

        public Duration getBackoff() {
            return backoff;
        }

        public void setBackoff(Duration backoff) {
            this.backoff = backoff;
        }
    }
} 
