package im.telegraph.pubsub;

import java.io.Serializable;

/**
 * Marker interface for PubSub messages.
 *
 * @author Ast3t1s
 */
public interface PubSubMessage extends Serializable {

}
