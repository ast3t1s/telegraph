package im.telegraph.generator.generator.javapoet;

import com.squareup.javapoet.*;
import im.telegraph.generator.generator.Constants;
import im.telegraph.generator.generator.SchemeGenerator;
import im.telegraph.generator.type.*;

import javax.lang.model.element.Modifier;
import java.io.File;
import java.io.IOException;
import java.util.AbstractMap;
import java.util.List;

public class RpcParserGenerator implements SchemeGenerator {

    @Override
    public void generate(SchemeDefinition definition, String out) throws IOException {
        long start = System.currentTimeMillis();

        TypeSpec.Builder clazz = TypeSpec.classBuilder("RpcParser")
                .addJavadoc(Constants.NOTICE)
                .addJavadoc(Constants.DEFAULT_AUTHOR)
                .addModifiers(Modifier.PUBLIC)
                .superclass(ParameterizedTypeName.get(Constants.BASE_PARSER, Constants.TYPE_RPC));

        MethodSpec.Builder readBuilder = MethodSpec.methodBuilder("read")
                .addModifiers(Modifier.PUBLIC)
                .addAnnotation(Override.class)
                .returns(Constants.TYPE_RPC)
                .addParameter(int.class, "type")
                .addParameter(byte[].class, "payload")
                .addException(IOException.class)
                .beginControlFlow("switch (type)");

        for (AbstractMap.SimpleEntry<String, List<SchemeRpc>> pairs : definition.getAllRpc()) {
            for (SchemeRpc rpc : pairs.getValue()) {
                String javaName = TypeHelper.getRequestName(pairs.getKey(), rpc.getName());
                readBuilder.addStatement("case $L: return $T.fromBytes(payload)", rpc.getHeader(),
                        ClassName.get(Constants.PACKAGE_RPC, javaName));
            }
        }
        /*for (SchemeRpc rpc : definition.getAllRpc()) {
            String javaName = TypeHelper.getRequestName(rpc.getName());
            readBuilder.addStatement("case $L: return $T.fromBytes(payload)", rpc.getHeader(),
                    ClassName.get(Constants.PACKAGE_RPC, javaName));
        }*/
        for (AbstractMap.SimpleEntry<String, List<SchemeBaseResponse>> entry : definition.getResponses()) {
            for (SchemeBaseResponse rpc : entry.getValue()) {
                String javaName;
                if (rpc instanceof SchemeResponse) {
                    javaName = TypeHelper.getResponseName(((SchemeResponse) rpc).getName());
                } else {
                    javaName = TypeHelper.getAnonymousResponseName(entry.getKey(), ((SchemeResponseAnonymous) rpc).getRpc().getName());
                }
                readBuilder.addStatement("case $L: return $T.fromBytes(payload)", rpc.getHeader(),
                        ClassName.get(Constants.PACKAGE_RPC, javaName));
            }
        }
        /*for (SchemeBaseResponse rpc : definition.getAllResponses()) {
            String javaName;
            if (rpc instanceof SchemeResponse) {
                javaName = TypeHelper.getResponseName(((SchemeResponse) rpc).getName());
            } else {
                javaName = TypeHelper.getAnonymousResponseName(((SchemeResponseAnonymous) rpc).getRpc().getName());
            }
            readBuilder.addStatement("case $L: return $T.fromBytes(payload)", rpc.getHeader(),
                    ClassName.get(Constants.PACKAGE_RPC, javaName));
        }*/
        for (SchemeUpdateBox u : definition.getAllUpdateBoxes()) {
            String javaName = TypeHelper.getUpdateBoxName(u.getName());
            readBuilder.addStatement("case $L: return $T.fromBytes(payload)", u.getHeader(),
                    ClassName.get(Constants.PACKAGE_BASE, javaName));
        }
        readBuilder.endControlFlow("throw new IOException()");

        clazz.addMethod(readBuilder.build());

        JavaFile.builder(Constants.PACKAGE_PARSER, clazz.build())
                .indent("    ")
                .build().writeTo(new File(out));

        System.out.println("Rpc parser generated in " + (System.currentTimeMillis() - start) + " ms");
    }
}
