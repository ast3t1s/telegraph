package im.telegraph.internal.remote.serializer;

import java.nio.charset.StandardCharsets;

/**
 * @author Ast3t1s
 */
public abstract class PrimitiveSerializers {

    public static class LongSerializer implements Serializer {

        @Override
        public int serializerId() {
            return 5;
        }

        @Override
        public byte[] toBinary(Object object) throws SerializationException {
            byte[] result = new byte[8];
            long value = (Long) object;
            for (int i = 0; i < 8; i++) {
                result[i] = (byte) (value & 0xFF);
                value >>>= 8;
            }
            return result;
        }

        @Override
        public Object fromBinary(byte[] array) throws SerializationException {
            long result = 0;
            for (int i = 7; i >= 0; i--) {
                result <<= 8;
                result |= (array[i] & 0xFF);
            }
            return result;
        }
    }

    public static class IntSerializer implements Serializer {

        @Override
        public int serializerId() {
            return 7;
        }

        @Override
        public byte[] toBinary(Object object) throws SerializationException {
            byte[] result = new byte[4];
            int value = (Integer) object;
            for (int i = 0; i < 4; i++) {
                result[i] = (byte) (value & 0xFF);
                value >>>= 8;
            }
            return result;
        }

        @Override
        public Object fromBinary(byte[] array) throws SerializationException {
            int result = 0;
            for (int i = 3; i >= 0; i--) {
                result <<= 8;
                result |= (array[i] & 0xFF);
            }
            return result;
        }
    }

    public static class StringSerializer implements Serializer {

        @Override
        public int serializerId() {
            return 8;
        }

        @Override
        public byte[] toBinary(Object object) throws SerializationException {
            return ((String) object).getBytes(StandardCharsets.UTF_8);
        }

        @Override
        public Object fromBinary(byte[] array) throws SerializationException {
            return new String(array, StandardCharsets.UTF_8);
        }
    }

    public static class BooleanSerializer implements Serializer {

        private static final byte FALSE = 0x00;
        private static final byte TRUE = 0x01;

        @Override
        public int serializerId() {
            return 9;
        }

        @Override
        public byte[] toBinary(Object object) throws SerializationException {
            byte flag;
            if (Boolean.TRUE.equals(object)) {
                flag = TRUE;
            } else if (Boolean.FALSE.equals(object)) {
                flag = FALSE;
            } else {
                throw new IllegalArgumentException("Non boolean flag: " + object);
            }
            return new byte[] { flag };
        }

        @Override
        public Object fromBinary(byte[] array) throws SerializationException {
            byte flag = array[0];
            if (flag == TRUE) {
                return Boolean.TRUE;
            } else if (flag == FALSE) {
                return Boolean.FALSE;
            } else {
                throw new IllegalArgumentException("Non boolean flag byte: " + flag);
            }
        }
    }
}
