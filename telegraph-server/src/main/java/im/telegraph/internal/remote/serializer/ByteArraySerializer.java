package im.telegraph.internal.remote.serializer;

/**
 * @author Ast3t1s
 */
public class ByteArraySerializer implements Serializer {

    @Override
    public int serializerId() {
        return 11;
    }

    @Override
    public byte[] toBinary(Object object) throws SerializationException {
        if (object == null) {
            return null;
        }
        if (object instanceof byte[]) {
            return (byte[]) object;
        }
        throw new SerializationException("The object to convert is not a byte array");
    }

    @Override
    public Object fromBinary(byte[] array) throws SerializationException {
        return array;
    }
}
