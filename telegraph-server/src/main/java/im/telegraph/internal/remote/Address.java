package im.telegraph.internal.remote;

import java.io.Serializable;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.URI;
import java.net.UnknownHostException;
import java.util.Objects;
import java.util.Optional;

/**
 * @author Ast3t1s
 */
public final class Address implements Comparable<Address>, Serializable {

    private static final long serialVersionUID = 1;

    private final String host;
    private final int port;
    private boolean secure;

    /**
     * Constructs an {@code Address} with an associated data.
     *
     * @param host the host address.
     * @param port the port.
     */
    private Address(final String host, final int port) {
        this.host = host;
        this.port = port;
    }

    /**
     * Create a default {@link Address} using the given host and port.
     *
     * @param host the host.
     * @param port the port.
     * @return a default address.
     */
    public static Address create(final String host, final int port) {
        return new Address(host, port);
    }

    public static Address create(final URI uri) {
        if (uri == null) {
            throw new IllegalArgumentException("URI can not be null");
        }
        return new Address(uri.getHost(), uri.getPort());
    }

    /**
     * Create a default {@link Address} using the given address in format "host:port".
     *
     * @param address the address to parse.
     * @return a default address.
     */
    public static Address create(final String address) {
        if (address == null || address.isEmpty()) {
            throw new IllegalArgumentException("Address is empty.");
        }

        final URI uri = URI.create(address);

        /*final String[] tokens = address.split(":", 3);

        String host = tokens[0];
        if (host == null || host.isEmpty()) {
            throw new IllegalArgumentException("can't parse host from: " + address);
        }

        int port;
        try {
            port = Integer.parseInt(tokens[1]);
        } catch (NumberFormatException ex) {
            throw new IllegalArgumentException("can't parse port from: " + address, ex);
        }*/

        return new Address(uri.getHost(), uri.getPort());
    }

    /**
     * Creates {@link URI} from the given host and port.
     *
     * @return URI of the form (secure)?tls-tcp + "host:port". Scheme port default used
     * if port not set.
     */
    public URI getUri() {
        final String scheme = isSecure() ? "tls-tcp" : "tcp";
        String uri = String.format("%s://%s:%s", scheme, getHost(), getPort());
        return URI.create(uri);
    }

    public boolean isSecure() {
        return secure;
    }

    /**
     * Returns the address represented by {@link InetSocketAddress}.
     *
     * @return the current address represented by {@link InetSocketAddress} instance
     */
    public InetSocketAddress getInetSocketAddress() {
        return new InetSocketAddress(host, port);
    }

    /**
     * Getting local IP address by the address of local host. <b>NOTE:</b> returned IP address is
     * expected to be a publicly visible IP address.
     *
     * @throws RuntimeException wrapped {@link UnknownHostException}
     */
    public static InetAddress getLocalIpAddress() {
        try {
            return InetAddress.getLocalHost();
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Checks whether given host string is one of localhost variants. i.e. {@code 127.0.0.1}, {@code
     * 127.0.1.1} or {@code localhost}, and if so - then node's public IP address will be resolved and
     * returned.
     *
     * @param host host string
     * @return local ip address if given host is localhost
     */
    private static String convertIfLocalhost(String host) {
        String result;
        switch (host) {
            case "localhost":
            case "127.0.0.1":
            case "127.0.1.1":
                result = getLocalIpAddress().getHostAddress();
                break;
            default:
                result = host;
        }
        return result;
    }

    /**
     * Returns the hostname.
     *
     * @return the hostname.
     */
    public String getHost() {
        return host;
    }

    /**
     * Returns the port.
     *
     * @return the port.
     */
    public int getPort() {
        return port;
    }

    //Using only for internal
    private String getOpt(String str) {
        return Optional.ofNullable(str).orElse("");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Address address = (Address) o;
        return port == address.port
                && Objects.equals(host, address.host);
    }

    @Override
    public int hashCode() {
        return Objects.hash(host, port);
    }

    @Override
    public String toString() {
        return getUri().toString();
    }

    @Override
    public int compareTo(Address o) {
        if (this == o) {
            return 0;
        }
        if (!host.equals(o.host)) {
            return getOpt(host).compareTo(getOpt(o.getHost()));
        }
        if (port != o.getPort()) {
            return Integer.compare(port, o.port);
        }
        return 0;
    }
}
