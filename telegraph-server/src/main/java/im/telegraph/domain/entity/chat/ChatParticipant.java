package im.telegraph.domain.entity.chat;

import im.telegraph.domain.entity.Id;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

public class ChatParticipant implements Serializable {

    private static final long serialVersionUID = 1L;

    private final Id chatId;
    private final Id userId;
    private final Id inviterId;
    private final Status status;
    private final Instant createdAt;

    /**
     * Constructs an {@link ChatParticipant instance} with the given params.
     *
     * @param chatId          Identifier of the chat associated with a chat.
     * @param userId          Identifier of the chat member.
     * @param inviterId       Identifier of the user that invited/promoted this member in the chat.
     * @param status          Status of the member in the chat.
     * @param createdAt       Point in time (Unix timestamp) when user was created.
     */
    @lombok.Builder(builderClassName = "Builder", toBuilder = true)
    ChatParticipant(final Id chatId, final Id userId, final Id inviterId, final Status status, final Instant createdAt) {
        this.chatId = Objects.requireNonNull(chatId);
        this.userId = Objects.requireNonNull(userId);
        this.inviterId = Objects.requireNonNull(inviterId);
        this.status = Objects.requireNonNull(status);
        this.createdAt = Objects.requireNonNull(createdAt);
    }

    /**
     * Gets the identifier of the {@code Chat}.
     *
     * @return the identifier of the chat
     */
    public Id getChatId() {
        return chatId;
    }

    /**
     * Gets the identifier of the {@code User}.
     *
     * @return the identifier of the user
     */
    public Id getUserId() {
        return userId;
    }

    /**
     * Gets the identifier of the {@code User} who invited current user to the {@code Chat}.
     *
     * @return the identifier of the chat
     */
    public Id getInviterId() {
        return inviterId;
    }

    /**
     * Gets the status {@link Status} of the participants.
     *
     * @return the status of the participant
     */
    public Status getStatus() {
        return status;
    }

    /**
     * Gets the date when user joined to the {@code Chat}.
     *
     * @return the date when user joined to the chat
     */
    public Instant getCreatedAt() {
        return createdAt;
    }

    /**
     * Flag whether or not user is admin.
     *
     * @return flag whether or not user is admin.
     */
    public final boolean isAdmin() {
        return getStatus() == Status.ADMIN;
    }

    /**
     * Flag whether or not user is owner.
     *
     * @return flag whether or not user is owner.
     */
    public final boolean isOwner() {
        return getStatus() == Status.OWNER;
    }

    /**
     * Flag whether or not user is participant.
     *
     * @return flag whether or not user is participant.
     */
    public final boolean isMember() {
        return getStatus() == Status.PARTICIPANT;
    }

    /**
     * Flag whether or not user is banned.
     *
     * @return flag whether or not user is banned.
     */
    public final boolean isBanned() {
        return getStatus() == Status.BANNED;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        ChatParticipant that = (ChatParticipant) object;
        return Objects.equals(chatId, that.chatId) &&
                Objects.equals(userId, that.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(chatId, userId);
    }

    /**
     * Represents the various types of chat member status.
     */
    public enum Status {
        /**
         * Unknown status.
         */
        UNKNOWN(-1),

        /**
         * Chat member has owner role.
         */
        OWNER(1),

        /**
         * Chat member has participant role.
         */
        PARTICIPANT(2),

        /**
         * Chat member has admin role.
         */
        ADMIN(3),

        /**
         * Status which indicates a restricted participant.
         */
        BANNED(4);

        /**
         * The underlying value.
         */
        private final int value;

        /**
         * Constructs a {@code MemberStatus}.
         */
        Status(final int value) {
            this.value = value;
        }

        /**
         * Gets the underlying value.
         *
         * @return The underlying value.
         */
        public int getValue() {
            return this.value;
        }

        /**
         * Gets the status of the chat member.
         *
         * @param value The underlying value.
         * @return The status of the chat member.
         */
        public static Status of(final int value) {
            return switch (value) {
                case 1 -> OWNER;
                case 2 -> PARTICIPANT;
                case 3 -> ADMIN;
                case 4 -> BANNED;
                default -> UNKNOWN;
            };
        }
    }

    @Override
    public String toString() {
        return "ChatParticipant{" +
                "chatId=" + chatId +
                ", userId=" + userId +
                ", inviterId=" + inviterId +
                ", status=" + status +
                ", createdAt=" + createdAt +
                '}';
    }
}
