package im.telegraph.protocol.cryptography;

import im.telegraph.common.property.protocol.crypto.CryptoProperties;
import im.telegraph.common.property.protocol.crypto.TrustedKeyProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ResourceUtils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * @author Ast3t1s
 */
public class ResourceTrustedKeyStore implements TrustedKeyStore {

    private static final Logger log = LoggerFactory.getLogger(ResourceTrustedKeyStore.class);

    private final Map<Long, ServerTrustedKey> keys;

    private final CryptoProperties properties;

    public ResourceTrustedKeyStore(CryptoProperties properties) {
        this.properties = properties;
        this.keys = toUnmodifiableConcurrentMap(loadTrustedKeys());
    }

    private List<ServerTrustedKey> loadTrustedKeys() {
        return properties.getTrustedKeys().values()
                .stream()
                .map(this::toSeverKey)
                .collect(Collectors.toList());
    }

    @Override
    public Mono<ServerTrustedKey> find(Long id) {
        return Mono.fromCallable(() -> keys.get(id));
    }

    @Override
    public Flux<ServerTrustedKey> findAll() {
        return Flux.fromIterable(keys.values());
    }

    private ServerTrustedKey toSeverKey(TrustedKeyProperties keys) {
        try {
            byte[] publicKey = ResourceUtils.getURL(keys.getPublicKey()).openStream().readAllBytes();
            byte[] privateKey = ResourceUtils.getURL(keys.getPrivateKey()).openStream().readAllBytes();
            return ServerTrustedKey.of(publicKey, privateKey);
        } catch (IOException e) {
            log.error("Unexpected error due loading trusted key", e);
            throw new RuntimeException("Error loading trusted key");
        }
    }

    private static Map<Long, ServerTrustedKey> toUnmodifiableConcurrentMap(List<ServerTrustedKey> trustedKeys) {
        if (trustedKeys.isEmpty()) {
            throw new IllegalStateException("Server trusted keys can not be empty");
        }
        ConcurrentHashMap<Long, ServerTrustedKey> result = new ConcurrentHashMap<>();
        for (ServerTrustedKey trustedKey : trustedKeys) {
            if (result.containsKey(Objects.requireNonNull(trustedKey).getId())) {
                throw new IllegalStateException(String.format("Duplicate key %d", trustedKey.getId()));
            }
            result.put(trustedKey.getId(), trustedKey);
        }
        return Collections.unmodifiableMap(result);
    }
}
