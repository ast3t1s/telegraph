package im.telegraph.api;

import java.io.IOException;

/**
 * Generated by the Telegraph API Scheme generator. DO NOT EDIT!
 * @author Telegraph Scheme Generator
 */
public enum ApiSearchContentType {
    ANY(1),

    TEXT(2),

    LINKS(3),

    DOCUMENTS(4),

    PHOTOS(5),

    UNSUPPORTED_VALUE(-1);

    private final int value;

    ApiSearchContentType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static ApiSearchContentType parse(int value) throws IOException {
        switch (value) {
            case 1: return ApiSearchContentType.ANY;
            case 2: return ApiSearchContentType.TEXT;
            case 3: return ApiSearchContentType.LINKS;
            case 4: return ApiSearchContentType.DOCUMENTS;
            case 5: return ApiSearchContentType.PHOTOS;
            default: return ApiSearchContentType.UNSUPPORTED_VALUE;
        }
    }
}
