package im.telegraph.admin.object;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.lang.Nullable;

/**
 * @author Ast3t1s
 */
public class ChatBean {

    private Long id;
    private final String title;
    @Nullable
    private final String description;
    @JsonProperty("owner_id")
    private final Long ownerId;
    private final Long permissions;
    @JsonProperty("participants_count")
    private final int membersCount;
    @JsonProperty("created_at")
    private final Long createdAt;

    public ChatBean(Long id, String title, @Nullable String description, Long ownerId, Long permissions,
                    int membersCount, Long createdAt) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.ownerId = ownerId;
        this.permissions = permissions;
        this.membersCount = membersCount;
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return "ChatBean{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", ownerId=" + ownerId +
                ", permissions=" + permissions +
                ", membersCount=" + membersCount +
                ", createdAt=" + createdAt +
                '}';
    }
}
