package im.telegraph.protocol.model.transport;

import im.telegraph.engine.serialization.DataInputStream;
import im.telegraph.engine.serialization.DataOutputStream;
import im.telegraph.server.MTProtoObject;

import java.io.IOException;

public class Drop extends MTProtoObject {

	public static final int HEADER = 0x0D;

	private long msgId;
	private int errorCode;
	private String message;

	public Drop(DataInputStream inputStream) throws IOException {
		super(inputStream);
	}

	public Drop(long msgId, int errorCode, String message) {
		this.msgId = msgId;
		this.errorCode = errorCode;
		this.message = message;
	}

	public long getMsgId() {
		return msgId;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public String getMessage() {
		return message;
	}

	@Override
	public int getHeader() {
		return HEADER;
	}

	@Override
	public void writeObject(DataOutputStream outputStream) throws IOException {
		outputStream.writeInt64(msgId);
		outputStream.writeByte(errorCode);
		outputStream.writeProtoString(message);
	}

	@Override
	public MTProtoObject readObject(DataInputStream inputStream) throws IOException {
		this.msgId = inputStream.readInt64();
		this.errorCode = inputStream.readByte();
		this.message = inputStream.readProtoString();
		return this;
	}

	@Override
	public String toString() {
		return "Drop[" + message + "]";
	}

}
