package im.telegraph.api.rpc;

import static im.telegraph.engine.serialization.StreamUtils.*;

import im.telegraph.engine.serialization.StreamReader;
import im.telegraph.engine.serialization.StreamWriter;
import im.telegraph.engine.serialization.io.IOContext;
import im.telegraph.rpc.base.Request;
import java.io.IOException;
import java.lang.Override;
import java.lang.String;

/**
 * Generated by the Telegraph API Scheme generator. DO NOT EDIT!
 * @author Telegraph Scheme Generator
 */
public class RequestProfileCheckNickName extends Request<ResponseBool> {
    public static final int CLASS_ID = 0xce;

    protected String nickname;

    public RequestProfileCheckNickName() {
    }

    public RequestProfileCheckNickName(String nickname) {
        this.nickname = nickname;
    }

    public static RequestProfileCheckNickName fromBytes(byte[] data) throws IOException {
        return IOContext.fromByteArray(new RequestProfileCheckNickName(), data);
    }

    @Override
    public int getHeader() {
        return CLASS_ID;
    }

    public String getNickname() {
        return this.nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @Override
    public void serialize(StreamWriter writer) throws IOException {
        if (this.nickname == null) {
            throw new IOException();
        }
        writer.writeString(1, this.nickname);
    }

    @Override
    public void deserialize(StreamReader reader) throws IOException {
        this.nickname = reader.readString(1);
    }

    @Override
    public String toString() {
        String res = "rpc CheckNickName{";
        res += "nickname=" + this.nickname;
        res += "}";
        return res;
    }
}
