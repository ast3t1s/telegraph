package im.telegraph.common.store.caffeine;

import com.github.benmanes.caffeine.cache.Cache;
import im.telegraph.common.store.Store;
import org.reactivestreams.Publisher;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

public class CaffeineStore<K extends Comparable<K>, V> implements Store<K,V> {

    private final Cache<K, V> cache;

    public CaffeineStore(Cache<K, V> cache) {
        this.cache = cache;
    }

    @Override
    public Mono<V> save(K key, V value) {
        return Mono.fromRunnable(() -> cache.put(key, value)).thenReturn(value);
    }

    @Override
    public Mono<Void> save(Publisher<Tuple2<K, V>> entryStream) {
        return Flux.from(entryStream).doOnNext(t -> cache.put(t.getT1(), t.getT2())).then();
    }

    @Override
    public Mono<Void> remove(K key) {
        return Mono.fromRunnable(() -> cache.invalidate(key));
    }

    @Override
    public Mono<Void> remove(Publisher<K> keys) {
        return Flux.from(keys).doOnNext(cache::invalidate).then();
    }

    @Override
    public Mono<Void> removeAll() {
        return Mono.fromRunnable(cache::invalidateAll);
    }

    @Override
    public Mono<Void> invalidate() {
        return Mono.fromRunnable(cache::invalidateAll);
    }

    @Override
    public Mono<V> find(K key) {
        return Mono.fromCallable(() -> cache.get(key, id -> null));
    }

    @Override
    public Mono<Long> count() {
        return Mono.fromCallable(cache::estimatedSize);
    }

    @Override
    public Flux<K> keys() {
        return Flux.fromIterable(cache.asMap().keySet());
    }

    @Override
    public Flux<V> values() {
        return Flux.fromIterable(cache.asMap().values());
    }

    @Override
    public String toString() {
        return "CaffeineStore{" +
                "cache=" + cache.getClass() +
                '}';
    }
}
