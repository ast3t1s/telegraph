package im.telegraph.service;

import im.telegraph.api.ApiAbsUser;
import im.telegraph.api.ApiChat;
import im.telegraph.api.base.FatSeqUpdate;
import im.telegraph.api.base.SeqUpdate;
import im.telegraph.api.base.WeakUpdate;
import im.telegraph.domain.entity.Id;
import im.telegraph.handlers.internal.ApiEntityMapper;
import im.telegraph.handlers.mapper.UserConverter;
import im.telegraph.domain.event.SequenceUpdateEvent;
import im.telegraph.domain.event.Event;
import im.telegraph.domain.event.WeakUpdateEvent;
import im.telegraph.protocol.model.mtproto.MTUpdateBox;
import im.telegraph.protocol.model.mtproto.rpc.RpcEventPush;
import im.telegraph.pubsub.PubSubMediator;
import im.telegraph.rpc.base.Rpc;
import im.telegraph.handlers.internal.SerialMapping;
import im.telegraph.handlers.internal.SerialUpdate;
import org.reactivestreams.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.function.TupleUtils;

import java.io.IOException;
import java.util.Base64;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Ast3t1s
 */
@Component
public class UpdatesHandler extends AbstractEventHandler<Event> {

    private static final Logger log = LoggerFactory.getLogger(UpdatesHandler.class);

    private static final String UPDATES_TOPIC = "gio:bus:updates";

    private final UserService userService;
    private final ChatService chatService;
    private final SessionService sessionService;

    private final UserConverter userMapper;

    public UpdatesHandler(UserService userService,
                          ChatService chatService,
                          SessionService sessionService,
                          UserConverter userMapper,
                          PubSubMediator pubSubMediator) {
        super(pubSubMediator.receive(UPDATES_TOPIC, Event.class), Event.class);
        this.sessionService = sessionService;
        this.userService = userService;
        this.chatService = chatService;
        this.userMapper = userMapper;
    }

    @Override
    protected Publisher<Void> handle(Flux<Event> publisher) {
        return publisher.filter(event -> event instanceof SequenceUpdateEvent || event instanceof WeakUpdateEvent)
                .flatMap(this::handleUpdate);
    }

    public Mono<Void> handleUpdate(Event event) {
        if (event instanceof SequenceUpdateEvent) {
            return notifySequenceUpdate((SequenceUpdateEvent) event);
        } else if (event instanceof WeakUpdateEvent) {
            return notifyWeakUpdate((WeakUpdateEvent) event);
        }
        return Mono.empty();
    }

    protected SerialMapping getSerialMapping(final byte[] payload) {
        try {
            return SerialMapping.fromBytes(payload);
        } catch (IOException ex) {
            throw new IllegalStateException("Error decoding 'serialMapping'", ex);
        }
    }

    protected Mono<Void> notifySequenceUpdate(SequenceUpdateEvent event) {
        if (log.isTraceEnabled()) {
            log.trace("Notify sequence update: {} to local sessions", event);
        }

        final var userId = event.getUserId();
        final var excluded = (event.getExcluded() == null) ? Collections.emptySet() : event.getExcluded();
        final var sequence = event.getSeq();
        final var mapping = getSerialMapping(Base64.getDecoder().decode(event.getPayload()));

        return sessionService.getSessions(userId)
                .filter(session -> !excluded.contains(session.getId().getAuthId()))
                .flatMap(session -> {
                    final SerialUpdate customUpdate = mapping.getCustomOrDefault(session.getId().getAuthId());

                    if (isFatUpdate(customUpdate)) {
                        Mono<List<ApiAbsUser>> apiUsers = Mono.justOrEmpty(customUpdate.getUsers())
                                .map(uids -> uids.stream().map(Id::of)
                                        .collect(Collectors.toList()))
                                .flatMapMany(userService::getUsers)
                                .map(user -> Optional.ofNullable(userMapper.convert(session.getId().getAuthId(), user)))
                                .filter(Optional::isPresent)
                                .map(Optional::get)
                                .collectList()
                                .defaultIfEmpty(Collections.emptyList());

                        Mono<List<ApiChat>> apiChats = Mono.justOrEmpty(customUpdate.getGroups())
                                .map(guids -> guids.stream().map(Id::of)
                                        .collect(Collectors.toList()))
                                .flatMapMany(chatService::getChats)
                                .map(chat -> Optional.ofNullable(ApiEntityMapper.toApiChat(chat)))
                                .filter(Optional::isPresent)
                                .map(Optional::get)
                                .collectList()
                                .defaultIfEmpty(Collections.emptyList());

                        return Mono.zip(apiUsers, apiChats)
                                .flatMap(TupleUtils.function((users, chats) -> {
                                    final FatSeqUpdate fatSeqUpdate = new FatSeqUpdate();
                                    fatSeqUpdate.setSeq(sequence);
                                    fatSeqUpdate.setUpdateHeader(customUpdate.getHeader());
                                    fatSeqUpdate.setUpdate(customUpdate.getBody());
                                    fatSeqUpdate.setUsers(users);
                                    fatSeqUpdate.setChats(chats);

                                    return session.sendRpcMessage(Mono.just(toUpdateBox(fatSeqUpdate)));
                                }))
                                .then();
                    } else {
                        final SeqUpdate seqUpdate = new SeqUpdate();
                        seqUpdate.setSeq(sequence);
                        seqUpdate.setUpdateHeader(customUpdate.getHeader());
                        seqUpdate.setUpdate(customUpdate.toByteArray());

                        return session.sendRpcMessage(Mono.just(toUpdateBox(seqUpdate)));
                    }
                })
                .onErrorContinue((ex, o) -> log.warn("Unexpected error due notifying sequence update: {} to local sessions", event, ex))
                .then();
    }

    private boolean isFatUpdate(SerialUpdate serialUpdate) {
        if (serialUpdate.getUsers() != null && serialUpdate.getUsers().size() > 0) {
            return true;
        }
        if (serialUpdate.getGroups() != null && serialUpdate.getGroups().size() > 0) {
            return true;
        }
        return false;
    }

    protected Mono<Void> notifyWeakUpdate(WeakUpdateEvent event) {
        return Mono.defer(() -> {
            if (log.isTraceEnabled()) {
                log.trace("Notify weak update: {} to local sessions", event);
            }

            final var userId = event.getUserId();
            final var timestamp = event.getTimestamp();
            final var excludedSessions = event.getExcluded();
            final var header = event.getHeader();
            final var body = Base64.getDecoder().decode(event.getBody());

            final WeakUpdate weakUpdate = new WeakUpdate();
            weakUpdate.setUpdateHeader(header);
            weakUpdate.setUpdate(body);
            weakUpdate.setDate(timestamp);

            return sessionService.getSessions(userId)
                    .filter(session -> !excludedSessions.contains(session.getId().getAuthId()))
                    .flatMap(session -> session.sendRpcMessage(Mono.just(toUpdateBox(weakUpdate))))
                    .onErrorContinue((ex, o) -> log.warn("Unexpected error due notifying weak update: {} to local sessions", event, ex))
                    .then();
        });
    }

    private static MTUpdateBox toUpdateBox(final Rpc rpc) {
        final RpcEventPush eventPush = new RpcEventPush(rpc.getHeader(), rpc.toByteArray());
        return new MTUpdateBox(eventPush.toByteArray());
    }
}
