package im.telegraph.admin.route;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Ast3t1s
 */
public interface UriMatcher {

    class Impl implements UriMatcher {
        public static UriMatcher of(String uriPattern) {
            return new UriMatcher.Impl(uriPattern);
        }

        private static final Pattern FULL_SPLAT_PATTERN =
                Pattern.compile("[\\*][\\*]");
        private static final String FULL_SPLAT_REPLACEMENT = ".*";

        private static final Pattern NAME_SPLAT_PATTERN =
                Pattern.compile("\\{([^/]+?)\\}[\\*][\\*]");

        private static final Pattern NAME_PATTERN = Pattern.compile("\\{([^/]+?)\\}");
        // JDK 6 doesn't support named capture groups

        private final List<String> pathVariables = new ArrayList<>();

        private final Pattern uriPattern;

        private static String getNameSplatReplacement(String name) {
            return "(?<" + name + ">.*)";
        }

        private static String getNameReplacement(String name) {
            return "(?<" + name + ">[^\\/]*)";
        }

        static String filterQueryParams(String uri) {
            int hasQuery = uri.lastIndexOf('?');
            if (hasQuery != -1) {
                return uri.substring(0, hasQuery);
            } else {
                return uri;
            }
        }

        /**
         * Creates a new {@code UriPathTemplate} from the given {@code uriPattern}.
         *
         * @param uriPattern The pattern to be used by the template
         */
        Impl(String uriPattern) {
            String s = "^" + filterQueryParams(uriPattern);

            Matcher m = NAME_SPLAT_PATTERN.matcher(s);
            while (m.find()) {
                for (int i = 1; i <= m.groupCount(); i++) {
                    String name = m.group(i);
                    pathVariables.add(name);
                    s = m.replaceFirst(getNameSplatReplacement(name));
                    m.reset(s);
                }
            }

            m = NAME_PATTERN.matcher(s);
            while (m.find()) {
                for (int i = 1; i <= m.groupCount(); i++) {
                    String name = m.group(i);
                    pathVariables.add(name);
                    s = m.replaceFirst(getNameReplacement(name));
                    m.reset(s);
                }
            }

            m = FULL_SPLAT_PATTERN.matcher(s);
            while (m.find()) {
                s = m.replaceAll(FULL_SPLAT_REPLACEMENT);
                m.reset(s);
            }

            this.uriPattern = Pattern.compile(s + "$");
        }

        /**
         * Tests the given {@code uri} against this template, returning {@code true} if
         * the uri matches the template, {@code false} otherwise.
         *
         * @param uri The uri to match
         * @return {@code true} if there's a match, {@code false} otherwise
         */
        public boolean matches(String uri) {
            return matcher(uri).matches();
        }

        private Matcher matcher(String uri) {
            uri = filterQueryParams(uri);
            return uriPattern.matcher(uri);
        }

        /**
         * Matches the template against the given {@code uri} returning a map of path
         * parameters extracted from the uri, keyed by the names in the template. If the
         * uri does not match, or there are no path parameters, an empty map is returned.
         *
         * @param uri The uri to match
         * @return the path parameters from the uri. Never {@code null}.
         */
        public Map<String, String> match(String uri) {
            Map<String, String> pathParameters = new HashMap<>();
            Matcher m = matcher(uri);
            if (m.matches()) {
                int i = 1;
                for (String name : pathVariables) {
                    String val = m.group(i++);
                    pathParameters.put(name, val);
                }
            }

            return pathParameters;
        }
    }

    class Fixed implements UriMatcher {
        private static final String QUERY_CHARACTER = "?";

        private final String fixedPath;

        public Fixed(String fixedPath) {
            this.fixedPath = fixedPath;
        }

        @Override
        public boolean matches(String uri) {
            int indexOfQueryCharacter = uri.indexOf(QUERY_CHARACTER);
            if (indexOfQueryCharacter != -1) {
                return uri.substring(0, indexOfQueryCharacter).equals(fixedPath);
            } else {
                return uri.equals(fixedPath);
            }
        }

        @Override
        public Map<String, String> match(String uri) {
            return Map.of();
        }
    }

    boolean matches(String uri);

    /**
     * Matches the template against the given {@code uri} returning a map of path
     * parameters extracted from the uri, keyed by the names in the template. If the
     * uri does not match, or there are no path parameters, an empty map is returned.
     *
     * @param uri The uri to match
     * @return the path parameters from the uri. Never {@code null}.
     */
    Map<String, String> match(String uri);
}
