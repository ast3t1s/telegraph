package im.telegraph.domain.entity;

import java.util.Objects;

/**
 * @author Ast3t1s
 */
public final class FileReferenceId {

    private final long fileId;
    private final long accessHash;

    public FileReferenceId(long fileId, long accessHash) {
        this.fileId = fileId;
        this.accessHash = accessHash;
    }

    public long getFileId() {
        return fileId;
    }

    public long getAccessHash() {
        return accessHash;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        FileReferenceId that = (FileReferenceId) object;
        return fileId == that.fileId &&
                accessHash == that.accessHash;
    }

    @Override
    public int hashCode() {
        return Objects.hash(fileId, accessHash);
    }

    @Override
    public String toString() {
        return "FileReferenceId{" +
                "fileId=" + fileId +
                ", accessHash=" + accessHash +
                '}';
    }
}
