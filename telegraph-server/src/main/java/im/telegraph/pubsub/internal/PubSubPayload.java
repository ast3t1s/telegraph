package im.telegraph.pubsub.internal;

import im.telegraph.pubsub.PubSubMessage;

/**
 * @author Ast3t1s
 */
public abstract class PubSubPayload<T> implements PubSubMessage {

    private final int op;
    private final T data;

    public PubSubPayload(int op, T data) {
        this.op = op;
        this.data = data;
    }

    public int getOp() {
        return op;
    }

    public T getData() {
        return data;
    }

    @Override
    public String toString() {
        return "PubSubPayload{" +
                "op=" + op +
                ", data=" + data +
                '}';
    }
}
