package im.telegraph.protocol.model.transport;

import im.telegraph.engine.serialization.DataInputStream;

import java.io.IOException;
import java.util.Arrays;

/**
 * @author Ast3t1s
 */
public class HandshakeRequest extends Handshake {

    public static final int HEADER = 0xFF;

    public HandshakeRequest(DataInputStream inputStream) throws IOException {
        super(inputStream);
    }

    public HandshakeRequest(int mtprotoVersion, int apiMajorVersion, int apiMinorVersion, byte[] handshakeData) {
        super(mtprotoVersion, apiMajorVersion, apiMinorVersion, handshakeData);
    }

    @Override
    public int getHeader() {
        return HEADER;
    }

    @Override
    public String toString() {
        return "HandshakeRequest{" +
                "mtprotoVersion=" + getMtprotoVersion() +
                ", apiMajorVersion=" + getApiMajorVersion() +
                ", apiMinorVersion=" + getApiMinorVersion() +
                ", handshakeData=" + Arrays.toString(getHandshakeData()) +
                '}';
    }
}
