package im.telegraph.protocol.model.mtproto;

import im.telegraph.engine.serialization.DataInputStream;
import im.telegraph.engine.serialization.DataOutputStream;

import java.io.IOException;

public class MTRpcRequest extends ProtoStruct {

	public static final byte HEADER = (byte) 0x03;

	public byte[] payload;

	public MTRpcRequest(DataInputStream stream) throws IOException {
		super(stream);
	}

	public MTRpcRequest(byte[] payload) {
		this.payload = payload;
	}

	public byte[] getPayload() {
		return payload;
	}

	@Override
	protected byte getHeader() {
		return HEADER;
	}

	@Override
	protected void writeBody(DataOutputStream outputStream) throws IOException {
		outputStream.writeProtoBytes(payload);
	}

	@Override
	protected void readBody(DataInputStream inputStream) throws IOException {
		payload = inputStream.readProtoBytes();
	}

	@Override
	public String toString() {
		return "RequestBox";
	}

}
