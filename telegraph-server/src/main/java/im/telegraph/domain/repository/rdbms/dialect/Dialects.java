package im.telegraph.domain.repository.rdbms.dialect;

import io.r2dbc.spi.ConnectionFactory;
import io.r2dbc.spi.ConnectionFactoryMetadata;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * @author Ast3t1s
 */
public class Dialects {

    private static final Map<String, Dialect> DIALECTS = new HashMap<>();

    static {
        DIALECTS.put("Microsoft SQL Server", new SqlServerDialect());
        DIALECTS.put("MySQL", new MySqlDialect());
        DIALECTS.put("PostgreSQL", new PostgresDialect());
    }

    public static Optional<Dialect> getDialect(ConnectionFactory connectionFactory) {
        ConnectionFactoryMetadata metadata = connectionFactory.getMetadata();
        Dialect dialect = DIALECTS.get(metadata.getName());

        if (dialect != null) {
            return Optional.of(dialect);
        }

        return DIALECTS.keySet().stream()
                .filter(it -> metadata.getName().contains(it))
                .map(DIALECTS::get)
                .findFirst();
    }
} 
