package im.telegraph.protocol.model.mtproto.auth;

import im.telegraph.engine.serialization.DataInputStream;
import im.telegraph.engine.serialization.DataOutputStream;
import im.telegraph.protocol.model.mtproto.ProtoStruct;

import java.io.IOException;

public class RequestAuthId extends ProtoStruct {

	public static final int HEADER = 0xE0;

	public RequestAuthId(DataInputStream inputStream) throws IOException {
		super(inputStream);
	}

	public RequestAuthId() {
	}

	@Override
	protected byte getHeader() {
		return (byte) HEADER;
	}

	@Override
	protected void writeBody(DataOutputStream outputStream) throws IOException {

	}

	@Override
	protected void readBody(DataInputStream inputStream) throws IOException {

	}

}
