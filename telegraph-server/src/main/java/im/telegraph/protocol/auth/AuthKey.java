package im.telegraph.protocol.auth;

import im.telegraph.crypto.primitives.util.ByteStrings;
import im.telegraph.util.Crypto;
import reactor.util.annotation.Nullable;

import java.time.Instant;
import java.util.Arrays;
import java.util.Objects;

/**
 * Value-based tuple of auth key and it's details.
 *
 * @author Ast3t1s
 */ final class AuthKey {

    private final byte[] materKey;
    private final long id;
    @Nullable
    private final Instant expiresAt;

    private Object details;

    public AuthKey(final byte[] masterKey, @Nullable final Instant expiresAt) {
        this.materKey = Objects.requireNonNull(masterKey);
        final var sha256 = Crypto.SHA256(masterKey);
        final var authKeyIdData = Arrays.copyOfRange(sha256, 0, Long.BYTES);
        this.id = ByteStrings.bytesToLong(authKeyIdData);
        this.expiresAt = expiresAt;
    }

    public byte[] getMaterKey() {
        return materKey;
    }

    public long getId() {
        return id;
    }

    public void setDetails(Object details) {
        this.details = details;
    }

    @Nullable
    public Instant getExpiresAt() {
        return expiresAt;
    }
}
