package im.telegraph.cluster;

import im.telegraph.internal.remote.Address;

import java.io.Serializable;
import java.util.*;

/**
 * @author Ast3t1s
 */
public final class Member implements Serializable, Comparable<Member> {

    private static final long serialVersionUID = 1;

    private final UniqueAddress uniqueAddress;
    private final Set<String> roles;
    private final Version version;

    public Member(String id, Address address, Set<String> roles, Version version) {
        this.uniqueAddress = new UniqueAddress(address, id);
        this.roles = (roles == null) ? Collections.emptySet() : new HashSet<>(roles);
        this.version = version;
    }

    /**
     * Create a new builder for {@link Member}.
     *
     * @return a new builder
     */
    public static Builder builder() {
        return new Builder();
    }

    public UniqueAddress getUniqueAddress() {
        return uniqueAddress;
    }

    public Address getAddress() {
        return uniqueAddress.getAddress();
    }

    public Set<String> getRoles() {
        return Collections.unmodifiableSet(roles);
    }

    public boolean hasRole(String role) {
        return roles.contains(role);
    }

    public Version getVersion() {
        return version;
    }

    /**
     * Return a string form of this {@link Member} using following pattern {@code namespace:alias:id:address}
     *
     * @return a formatted string representing this object
     */
    public String format() {
        StringJoiner stringJoiner = new StringJoiner(":");
        return stringJoiner.add(stringifyId(uniqueAddress.getId()) + "@" + uniqueAddress.getAddress()).toString();
    }

    private static String stringifyId(String id) {
        try {
            final UUID uuid = UUID.fromString(id);
            return Long.toHexString(uuid.getMostSignificantBits() & Long.MAX_VALUE);
        } catch (Exception ex) {
            return id;
        }
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        Member member = (Member) object;
        return Objects.equals(uniqueAddress, member.getUniqueAddress());
    }

    @Override
    public int hashCode() {
        return uniqueAddress.hashCode();
    }

    @Override
    public int compareTo(Member o) {
        int ha = hashCode();
        int hb = o.hashCode();
        if (ha < hb || (ha == hb && uniqueAddress.compareTo(o.getUniqueAddress()) < 0)) {
            return -1;
        } else if (ha == hb) {
            return 0;
        } else {
            return 1;
        }
    }

    /**
     * Builder to create {@link Member}.
     */
    public static class Builder {

        private String id;
        private Address address;
        private final Set<String> roles = new HashSet<>();
        private Version version;

        protected Builder() {
        }

        /**
         * Set the identifier of the cluster member.
         *
         * @param id the identifier of the cluster member.
         * @return this builder.
         */
        public Builder id(String id) {
            this.id = id;
            return this;
        }

        /**
         * Set the {@link Address} of the cluster member.
         *
         * @param address the address of the member.
         * @return this builder.
         */
        public Builder address(Address address) {
            this.address = address;
            return this;
        }

        /**
         * Set the host and port of the cluster member.
         *
         * @param host the host of the member's address.
         * @param port the port of the member's address.
         * @return this builder.
         */
        public Builder address(String host, int port) {
            this.address = Address.create(host, port);
            return this;
        }

        /**
         * Add role to set of member roles.
         *
         * @param role the role to add.
         * @return this builder.
         */
        public Builder role(String role) {
            this.roles.add(role);
            return this;
        }

        /**
         * Add set of roles to member.
         *
         * @param roles the set of roles.
         * @return this builder.
         */
        public Builder roles(Set<String> roles) {
            this.roles.addAll(roles);
            return this;
        }

        /**
         * Set version of the member.
         *
         * @param version the version of the member.
         * @return this builder.
         */
        public Builder version(Version version) {
            this.version = version;
            return this;
        }

        /**
         * Construct the member.
         *
         * @return a build {@link Member}
         */
        public Member build() {
            return new Member(id, address, roles, version);
        }
    }

    @Override
    public String toString() {
        return "Member{" +
                "uniqueAddress=" + uniqueAddress +
                ", roles=" + roles +
                ", version=" + version +
                '}';
    }
}
