package im.telegraph.handlers.mapper;

import im.telegraph.api.*;
import im.telegraph.common.ACLHashGenerator;
import im.telegraph.domain.entity.AuthKey;
import im.telegraph.domain.entity.ProfilePhoto;
import im.telegraph.domain.entity.User;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class UserConverter {

    private final ACLHashGenerator hashGenerator;

    public UserConverter(ACLHashGenerator hashGenerator) {
        this.hashGenerator = hashGenerator;
    }

    /**
     * Map {@link User} entity to ApiUser.
     *
     * @param authId the {@link AuthKey} id
     * @param user   the user entity.
     * @return the instance of {@link ApiUser}
     */
    @Nullable
    public ApiAbsUser convert(long authId, User user) {
        if (user == null) {
            return null;
        }
        if (user.getDeletedAt().isPresent()) {
            ApiUserDeleted apiUserDeleted = new ApiUserDeleted();
            apiUserDeleted.setId(user.getId().asLong());
            apiUserDeleted.setNick(user.getUsername());
            apiUserDeleted.setName(user.getDisplayName().orElse(null));
            return apiUserDeleted;
        }
        ApiUser apiUser = new ApiUser();
        apiUser.setId(user.getId().asLong());
        apiUser.setAccessHash(hashGenerator.generateUserAccessHash(authId, user.getId().asLong(), user.getAccessSalt()));
        apiUser.setNick(user.getUsername());
        apiUser.setLangCode(user.getLanguageCode());
        apiUser.setName(user.getDisplayName().orElse(null));
        apiUser.setAbout(user.getAbout().orElse(null));
        user.getProfilePhoto().ifPresent(photo -> apiUser.setAvatarPhoto(convertToPhoto(photo)));
        return apiUser;
    }

    /**
     * Map {@link User} entity to {@link ApiUserFull}
     *
     * @param authId the {@link AuthKey} id.
     * @param user   the user entity.
     * @return the instance of {@link ApiUserFull}
     */
    @Nullable
    public ApiAbsUser toApiUserFull(long authId, User user) {
        if (user == null) {
            return null;
        }
        ApiUserFull apiUserFull = new ApiUserFull();
        apiUserFull.setId(user.getId().asLong());
        apiUserFull.setAccessHash(hashGenerator.generateUserAccessHash(authId, user.getId().asLong(), user.getAccessSalt()));
        apiUserFull.setNick(user.getUsername());
        apiUserFull.setLangCode(user.getLanguageCode());
        apiUserFull.setName(user.getDisplayName().orElse(null));
        apiUserFull.setAbout(user.getAbout().orElse(null));
        user.getProfilePhoto().ifPresent(photo -> apiUserFull.setAvatarPhoto(convertToPhoto(photo)));
        apiUserFull.setEmail(user.getEmail());
        apiUserFull.setVerified(user.getVerified());
        apiUserFull.setRegisteredAt((int) user.getCreatedAt().getEpochSecond());
        return apiUserFull;
    }

    @Nullable
    public ApiUserAvatarPhoto convertToPhoto(final ProfilePhoto photo) {
        if (photo == null) {
            return null;
        }
        final ApiUserAvatarPhoto apiUserAvatarPhoto = new ApiUserAvatarPhoto();
        apiUserAvatarPhoto.setPhotoId(photo.getId());
        apiUserAvatarPhoto.setSmallPhoto(new ApiFileLocation(photo.getPhotoSmall().getFileId(),
                photo.getPhotoSmall().getAccessHash()));
        apiUserAvatarPhoto.setSmallPhoto(new ApiFileLocation(photo.getPhotoLarge().getFileId(),
                photo.getPhotoLarge().getAccessHash()));
        apiUserAvatarPhoto.setAddedDate(photo.getAddedDate());
        return apiUserAvatarPhoto;
    }
}
