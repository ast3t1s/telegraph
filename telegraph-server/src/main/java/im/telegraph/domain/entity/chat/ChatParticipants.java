package im.telegraph.domain.entity.chat;

import im.telegraph.domain.entity.Id;
import org.springframework.lang.Nullable;

import java.io.Serializable;
import java.util.*;

public class ChatParticipants implements Iterable<Id>, Serializable {

    private final Set<Id> participants;

    private static final ChatParticipants EMPTY = new ChatParticipants(Collections.emptySet());

    private ChatParticipants(Collection<Id> participants) {
        if (participants.isEmpty()) {
            this.participants = Collections.emptySet();
        } else {
            this.participants = new HashSet<>(participants);
        }
    }

    public static ChatParticipants of(@Nullable Collection<Id> participants) {
        if (participants == null || participants.isEmpty()) {
            return empty();
        }
        return new ChatParticipants(participants);
    }

    public static ChatParticipants empty() {
        return EMPTY;
    }

    public ChatParticipants addMember(Id userId) {
        HashSet<Id> members = new HashSet<>(this.participants);
        members.add(userId);
        return new ChatParticipants(members);
    }

    public ChatParticipants removeMember(Id userId) {
        HashSet<Id> members = new HashSet<>(this.participants);
        members.remove(userId);
        return new ChatParticipants(members);
    }

    @Override
    public Iterator<Id> iterator() {
        return new ReadOnlyIterator<>(this.participants.iterator());
    }

    private static final class ReadOnlyIterator<T> implements Iterator<T> {

        private final Iterator<T> delegate;

        private ReadOnlyIterator(Iterator<T> delegate) {
            this.delegate = delegate;
        }

        @Override
        public boolean hasNext() {
            return this.delegate.hasNext();
        }

        @Override
        public T next() {
            return this.delegate.next();
        }
    }
}
