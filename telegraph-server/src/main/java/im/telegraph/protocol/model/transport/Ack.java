package im.telegraph.protocol.model.transport;

import im.telegraph.engine.serialization.DataInputStream;
import im.telegraph.engine.serialization.DataOutputStream;
import im.telegraph.server.MTProtoObject;

import java.io.IOException;

public class Ack extends MTProtoObject {

	public static final int HEADER = 0x6;

	private int packetIndex;

	public Ack(DataInputStream inputStream) throws IOException {
		super(inputStream);
	}

	public Ack(int packetIndex) {
		this.packetIndex = packetIndex;
	}

	public int getPacketIndex() {
		return packetIndex;
	}

	@Override
	public int getHeader() {
		return HEADER;
	}

	@Override
	public void writeObject(DataOutputStream outputStream) throws IOException {
		outputStream.writeInt32(packetIndex);
	}

	@Override
	public MTProtoObject readObject(DataInputStream inputStream) throws IOException {
		this.packetIndex = inputStream.readInt();
		return this;
	}

	@Override
	public String toString() {
		return "Ack{" + "packetIndex=" + packetIndex + '}';
	}

}
