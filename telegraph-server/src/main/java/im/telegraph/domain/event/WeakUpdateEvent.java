package im.telegraph.domain.event;

import im.telegraph.domain.entity.Id;

import java.util.Set;

/**
 * @author Ast3t1s
 */
public class WeakUpdateEvent implements Event {

    private final int header;
    private final String body;
    private final Id userId;
    private final long timestamp;
    private final Set<Long> excluded;

    public WeakUpdateEvent(int header, String body, Id userId, long timestamp, Set<Long> excluded) {
        this.header = header;
        this.body = body;
        this.userId = userId;
        this.timestamp = timestamp;
        this.excluded = excluded;
    }

    public int getHeader() {
        return header;
    }

    public String getBody() {
        return body;
    }

    public Id getUserId() {
        return userId;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public Set<Long> getExcluded() {
        return excluded;
    }

    @Override
    public String toString() {
        return "WeakUpdateEvent{" +
                "header=" + header +
                ", body='" + body + '\'' +
                ", userId=" + userId +
                ", timestamp=" + timestamp +
                ", excluded=" + excluded +
                '}';
    }
}
