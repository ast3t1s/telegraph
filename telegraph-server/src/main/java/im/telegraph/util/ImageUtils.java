package im.telegraph.util;

import com.sksamuel.scrimage.ImmutableImage;
import com.sksamuel.scrimage.Position;
import com.sksamuel.scrimage.nio.JpegWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class ImageUtils {

    private static final Logger log = LoggerFactory.getLogger(ImageUtils.class);

    public static byte[] resizeTo(byte[] source, int side) {
        return resizeTo(new ByteArrayInputStream(source), side);
    }

    public static byte[] resizeTo(InputStream source, int side) {
        try {
            final byte[] scaled = scaleTo(source, side);
            ImmutableImage image = ImmutableImage.loader().fromBytes(scaled);
            return image.resizeTo(side, side, Position.Center).forWriter(JpegWriter.Default).bytes();
        } catch (IOException e) {
            log.error("Exception to resize image", e);
            return null;
        }
    }

    public static byte[] bufferedImageToBytes(BufferedImage source, String format) {
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            ImageIO.write(source, format, baos);
            return baos.toByteArray();
        } catch (IOException e) {
            log.error("Unexpected error while converting buffered image to byte array", e);
            return null;
        }
    }

    public static byte[] scaleTo(InputStream source, int side) {
        try {
            ImmutableImage image = ImmutableImage.loader().fromStream(source);
            double scaleFactor = side * 1.0 / Math.min(image.width, image.height);
            return image.scale(scaleFactor).forWriter(JpegWriter.Default).bytes();
        } catch (IOException e) {
            log.error("Exception to scale image", e);
            return null;
        }
    }
} 
