package im.telegraph.admin;

import im.telegraph.admin.object.ContactBean;
import im.telegraph.admin.object.request.AddContactRequest;
import im.telegraph.admin.route.AbstractRoute;
import im.telegraph.admin.route.Endpoint;
import im.telegraph.admin.route.Route;
import im.telegraph.domain.entity.Id;
import im.telegraph.service.ContactsService;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpResponseStatus;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.netty.http.server.HttpServerRequest;
import reactor.netty.http.server.HttpServerResponse;

import java.util.List;
import java.util.Optional;

import static io.netty.handler.codec.http.HttpResponseStatus.CREATED;

/**
 * @author Ast3t1s
 */
@Component
public class ContactsRouterHandler extends AbstractRoute {

    private final ContactsService contactsService;

    public ContactsRouterHandler(ContactsService contactsService) {
        this.contactsService = contactsService;
    }

    @Override
    public List<Route> routes() {
        return List.of(
                Route.builder()
                        .endpoint(Endpoint.ofFixedPath(HttpMethod.POST, "/contacts/{userId}"))
                        .action(this::handleAddContact)
                        .corsHeaders(),
                Route.builder()
                        .endpoint(Endpoint.ofFixedPath(HttpMethod.GET, "/contacts/{userId}"))
                        .action(this::handleGetUserContacts)
                        .corsHeaders(),
                Route.builder()
                        .endpoint(Endpoint.ofFixedPath(HttpMethod.DELETE, "/contacts/{userId}/{contactId}"))
                        .action(this::handleRemoveContact)
                        .corsHeaders()
        );
    }

    protected Mono<Void> handleGetUserContacts(HttpServerRequest request, HttpServerResponse response) {
        return Mono.justOrEmpty(request.param("userId"))
                .map(Integer::valueOf).map(Id::of)
                .flatMap(contactsService::getContacts)
                .flatMapIterable(contacts -> contacts)
                .map(contact -> new ContactBean(
                        contact.getUserId().asString(),
                        contact.getContactId().asString(),
                        contact.getStatus().getValue(),
                        contact.getCreatedAt().toEpochMilli()))
                .collectList()
                .flatMap(contacts -> writeJson(response, contacts));
    }

    protected Mono<Void> handleAddContact(HttpServerRequest request, HttpServerResponse response) {
        return readJson(request, AddContactRequest.class)
                .flatMap(req -> {
                    final Id userId = Optional.ofNullable(request.param("userId"))
                            .map(Integer::valueOf)
                            .map(Id::of)
                            .orElse(null);

                    if (userId == null) {
                        return response.sendNotFound();
                    }

                    return contactsService.addContact(userId, Id.of(req.getContactId()))
                            .then(response.status(CREATED).send());
                });
    }

    protected Mono<Void> handleRemoveContact(HttpServerRequest request, HttpServerResponse response) {
        final Id userId = Optional.ofNullable(request.param("userId"))
                .map(Integer::parseInt).map(Id::of)
                .orElse(null);

        final Id contactId = Optional.ofNullable(request.param("contactId"))
                .map(Integer::parseInt).map(Id::of)
                .orElse(null);

        if (userId == null || contactId == null) {
            return response.sendNotFound();
        }

        return contactsService.deleteContact(userId, contactId)
                .then(response.status(HttpResponseStatus.NO_CONTENT).send());
    }
}
