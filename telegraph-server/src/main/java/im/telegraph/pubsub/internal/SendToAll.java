package im.telegraph.pubsub.internal;

/**
 * @author Ast3t1s
 */
public class SendToAll extends PubSubPayload<SendToAll.Data> {

    public static final int OP = 7;

    public SendToAll(String topic, Object payload, boolean excludeSelf) {
        this(OP, new Data(topic, payload, excludeSelf));
    }

    public SendToAll(int op, Data data) {
        super(op, data);
    }

    public static class Data {

        private final String topic;
        private final Object payload;
        private final boolean excludeSelf;

        public Data(String topic, Object payload, boolean excludeSelf) {
            this.topic = topic;
            this.payload = payload;
            this.excludeSelf = excludeSelf;
        }

        public String getTopic() {
            return topic;
        }

        public Object getPayload() {
            return payload;
        }

        public boolean isExcludeSelf() {
            return excludeSelf;
        }
    }
} 
