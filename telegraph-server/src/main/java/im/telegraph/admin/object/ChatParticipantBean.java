package im.telegraph.admin.object;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Ast3t1s
 */
public class ChatParticipantBean {

    @JsonProperty("chat_id")
    private final Long chatId;
    @JsonProperty("inviter_id")
    private final Long inviterId;
    private final int status;

    public ChatParticipantBean(Long chatId, Long inviterId, int status) {
        this.chatId = chatId;
        this.inviterId = inviterId;
        this.status = status;
    }

    @Override
    public String toString() {
        return "ChatParticipantBean{" +
                "chatId=" + chatId +
                ", inviterId=" + inviterId +
                ", status=" + status +
                '}';
    }
}
