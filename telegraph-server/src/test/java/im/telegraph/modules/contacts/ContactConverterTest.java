package im.telegraph.modules.contacts;

import im.telegraph.api.ApiContactRecord;
import im.telegraph.api.ApiUserBlocked;
import im.telegraph.domain.entity.Contact;
import im.telegraph.domain.entity.Id;
import im.telegraph.domain.entity.Privacy;
import im.telegraph.handlers.internal.ApiEntityMapper;
import org.junit.jupiter.api.Test;

import java.time.Instant;

import static org.assertj.core.api.Assertions.assertThat;

public class ContactConverterTest {

   // private final PrivacyConverter privacyMapper = new PrivacyConverter();

    @Test
    public void test_map_block_record() {
        Privacy entity = Privacy.of(Id.of(1), Id.of(2), Instant.now());

        ApiUserBlocked apiUserBlocked = ApiEntityMapper.toApiContactBlocked(entity);

        assertThat(entity.getBlockedId()).isEqualTo(apiUserBlocked.getUserId());
        assertThat(entity.getCreatedAt().getEpochSecond()).isEqualTo(apiUserBlocked.getDate());
        assertThat(ApiEntityMapper.toApiContactBlocked(null)).isNull();
    }

    @Test
    public void test_map_contact_record() {
        Contact entity = new Contact(Id.of(1), Id.of(2), Contact.Status.BOTH, Instant.now());

        ApiContactRecord apiContactRecord = ApiEntityMapper.convertToApiContact(entity);

        assertThat(entity.getContactId()).isEqualTo(apiContactRecord.getUserId());
        assertThat(entity.isMutual()).isEqualTo(apiContactRecord.getState().mutual());
        assertThat(ApiEntityMapper.convertToApiContact(null)).isNull();
    }
}