package im.telegraph.domain.repository;

import im.telegraph.domain.entity.FileUpload;
import im.telegraph.domain.entity.Id;
import reactor.core.publisher.Mono;

/**
 * @author Ast3t1s
 */
public interface FileRepository {

    /**
     * Inserts a new file upload entry into the repository.
     *
     * @param fileUpload the file upload entity to insert
     * @return A {@link Mono} that emits the inserted {@link FileUpload} instance. If an error occurs during the insertion,
     * the {@code Mono} emits an error.
     */
    Mono<FileUpload> insert(FileUpload fileUpload);

    /**
     * Finds a file upload by its ID.
     *
     * @param id the ID of the file upload
     * @return A {@link Mono} that emits the {@link FileUpload} instance if found, or an empty {@code Mono} if no file upload
     * is found with the given ID. If an error occurs during the search, it is emitted through the {@code Mono}.
     */
    Mono<FileUpload> findById(Id id);

    /**
     * Deletes a file upload entry by its ID.
     *
     * @param id the ID of the file upload to delete
     * @return A {@link Mono} that completes when the file upload is successfully deleted. If the file upload does not exist or
     * an error occurs during deletion, the {@code Mono} emits an error.
     */
    Mono<Void> deleteById(Id id);
}
