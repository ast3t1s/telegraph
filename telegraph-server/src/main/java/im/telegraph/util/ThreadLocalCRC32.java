package im.telegraph.util;

import io.netty.util.concurrent.FastThreadLocal;

import java.util.zip.CRC32;

/**
 * Utility class that provide thread safe crc32
 */
public final class ThreadLocalCRC32 {

    private static final FastThreadLocal<CRC32> CRC32_ENGINE = new FastThreadLocal<>() {

        @Override
        protected CRC32 initialValue() {
            return new CRC32();
        }
    };

    public static CRC32 getCrc32() {
        return CRC32_ENGINE.get();
    }

    public static int calculateChecksum(byte[] data) {
        final CRC32 crc32 = getCrc32();
        crc32.reset();
        crc32.update(data, 0, data.length);
        return (int) crc32.getValue();
    }

}
