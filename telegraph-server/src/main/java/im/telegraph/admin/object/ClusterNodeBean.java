package im.telegraph.admin.object;

import java.util.Set;

/**
 * @author Ast3t1s
 */
public class ClusterNodeBean {

    private final String id;
    private final String host;
    private final int port;
    private final String version;
    private final Set<String> roles;

    public ClusterNodeBean(String id, String host, int port, String version, Set<String> roles) {
        this.id = id;
        this.host = host;
        this.port = port;
        this.version = version;
        this.roles = roles;
    }

    @Override
    public String toString() {
        return "ClusterNodeBean{" +
                "id='" + id + '\'' +
                ", host='" + host + '\'' +
                ", port=" + port +
                ", version='" + version + '\'' +
                ", roles=" + roles +
                '}';
    }
}
