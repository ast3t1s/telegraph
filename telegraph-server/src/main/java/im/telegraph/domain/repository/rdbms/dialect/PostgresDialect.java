package im.telegraph.domain.repository.rdbms.dialect;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Ast3t1s
 */
public class PostgresDialect implements Dialect {

    @Override
    public String getLimit(int limit) {
        return "LIMIT " + limit;
    }

    @Override
    public String getOffset(int offset) {
        return "OFFSET " + offset;
    }

    @Override
    public String getLimitOffset(int limit, int offset) {
        return String.format("LIMIT %d OFFSET %d", limit, offset);
    }

    @Override
    public String buildUpsertQuery(String table, Map<String, Object> insertSpec, List<String> uniqueColumns,
                                   List<String> updateColumns, String... incrementFields) {
        String updateExpression = updateColumns.stream()
                .map(column -> column + " = EXCLUDED." + column)
                .collect(Collectors.joining(", "));

        String insertExpression = insertSpec.values().stream()
                .map(value -> (String) value)
                .map(value -> ":" + value)
                .collect(Collectors.joining(", "));

        String replaceExpression = uniqueColumns.stream()
                .collect(Collectors.joining(", "));

        StringBuilder query = new StringBuilder("INSERT INTO ")
                .append(table).append("(").append(String.join(", ", insertSpec.keySet()))
                .append(" ) VALUES ( ").append(insertExpression).append(") ON CONFLICT (")
                .append(replaceExpression).append(") DO UPDATE SET ").append(updateExpression);

        if (incrementFields.length > 0) {
            String incrementExpression = Arrays.stream(incrementFields)
                    .map(value -> value + " = " + table + "." + value + " EXCLUDED. " + value)
                    .collect(Collectors.joining(", "));

            query.append(", ").append(incrementExpression);
        }

        return query.toString();
    }

    @Override
    public String buildUpsertQueryMany(String table, List<String> insertSpec, String batchName,
                                       List<String> uniqueColumns, List<String> updateColumns, String... incrementFields) {
        String updateExpression = updateColumns.stream()
                .map(column -> column + " = EXCLUDED." + column)
                .collect(Collectors.joining(", "));

        String replaceExpression = uniqueColumns.stream()
                .collect(Collectors.joining(", "));

        StringBuilder query = new StringBuilder("INSERT INTO ")
                .append(table).append("(").append(String.join(", ", insertSpec))
                .append(" ) VALUES :").append(batchName).append(" ON CONFLICT (").append(replaceExpression)
                .append(") DO UPDATE SET ").append(updateExpression);

        if (incrementFields.length > 0) {
            String incrementExpression = Arrays.stream(incrementFields)
                    .map(value -> value + " = " + table + "." + value + " + EXCLUDED." + value)
                    .collect(Collectors.joining(", "));

            query.append(", ").append(incrementExpression);
        }

        return query.toString();
    }
}
