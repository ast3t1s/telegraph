package im.telegraph.rpc.base;

import im.telegraph.engine.serialization.BinaryStreamObject;

public abstract class HeaderProtoObject extends BinaryStreamObject {

	public abstract int getHeader();

}
