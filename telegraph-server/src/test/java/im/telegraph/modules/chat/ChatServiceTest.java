package im.telegraph.modules.chat;

import im.telegraph.EntityUtils;
import im.telegraph.TelegraphApplication;
import im.telegraph.TransactionalRollbackHelper;
import im.telegraph.domain.entity.Id;
import im.telegraph.domain.entity.chat.Chat;
import im.telegraph.domain.entity.chat.ChatParticipant;
import im.telegraph.domain.entity.chat.PermissionSet;
import im.telegraph.domain.repository.ChatParticipantRepository;
import im.telegraph.domain.repository.ChatRepository;
import im.telegraph.domain.repository.FileRepository;
import im.telegraph.domain.repository.DialogMarkerRepository;
import im.telegraph.domain.repository.rdbms.RdbmsPresenceRepository;
import im.telegraph.service.ChatService;
import io.r2dbc.spi.ConnectionFactory;
import org.assertj.core.internal.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.r2dbc.core.DatabaseClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.Instant;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = TelegraphApplication.class)
class ChatServiceTest {

    private static final String DEFAULT_TITLE = "Chat title";
    private static final String DEFAULT_ABOUT = "Chat about";

    @Autowired
    private ChatRepository chatRepository;

    @Autowired
    private ChatParticipantRepository chatParticipantRepository;

    @Autowired
    private ChatService chatService;

    @Autowired
    private TransactionalRollbackHelper rollbackHelper;

    @Autowired
    private ConnectionFactory connectionFactory;

    private DatabaseClient databaseClient;

    @Autowired
    private FileRepository fileRepository;

    @Autowired
    private DialogMarkerRepository historyInboxRepository;

    @Autowired
    private RdbmsPresenceRepository presenceRepository;

    @BeforeEach
    void setUp() {
        databaseClient = DatabaseClient.builder()
                .connectionFactory(connectionFactory)
                .build();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    public void testInsertChat() {
        /*fileRepository.insert(FileUpload.builder()
                .accessSalt("some-salt")
                .size(79879L)
                .createdAt(LocalDateTime.now())
                .uploadKey("someFileKey").build())
                .doOnNext(System.out::println)
                .block();*/

      /*  Flux.range(60, 500)
                .map(id -> HistoryInbox.builder()
                        .peer(Peer.of(id, Peer.Type.CHAT))
                        .userId(UserId.of(1))
                        .ownerLastReadAt(Instant.now())
                        .ownerLastReceivedAt(Instant.now())
                        .build())
                .flatMap(inbox -> historyInboxRepository.save(inbox))
                .blockLast();*/

      /*  List<Peer> peers = new ArrayList<>();
        for (int i = 1; i < 20; i++) {
            peers.add(Peer.ofChat(ChatId.of(i)));
        }

        historyInboxRepository.findAllByUserIdAndPeerIn(UserId.of(1), peers)
                .doOnNext(System.out::println)
                .blockLast();*/

       /* presenceRepository.save(new Presence(31321L, UserId.of(1313), Instant.now()))
                .block();*/

       // System.out.println(presenceRepository.getUpsertQuery());

      /*  databaseClient.sql("INSERT INTO files (access_salt, upload_key) VALUES (:accessSalt, :uploadKey)")
                .bind("accessSalt", "someSalt")
                .bind("uploadKey", "someKey")
                .filter((statement, next) -> next.execute(statement.returnGeneratedValues("id")))
                .map(row -> row.get(0, Long.class))
                .first()
                .doOnNext(System.out::println)
                .block();*/
    }

    @Test
    void test_CreateChatShouldOk() {

        chatService.create(Id.of(25), "Chat title", "About", PermissionSet.all())
                .as(rollbackHelper::withRollback)
                .as(StepVerifier::create)
                .expectSubscription()
                .assertNext(chat -> {
                    assertThat(chat.getId()).isNotNull();
                    assertThat(chat.getTitle()).isEqualTo("Chat title");
                    assertThat(chat.getDescription()).isEqualTo("Test");
                    //assertThat(chat.getChatPermission()).contains(Permission.SHOW_ADMINS_TO_MEMBERS, Permission.SHOW_JOIN_LEAVE_MESSAGE);
                    //assertThat(chat.getChatPermission()).doesNotContain(Permission.CAN_ADMINS_EDIT_GROUP_INFO, Permission.CAN_MEMBERS_INVITE);
                    assertThat(chat.getCreatedAt()).isNotNull();
                    assertThat(chat.getUpdatedAt()).isNotNull();
                })
                .verifyComplete();
    }

    @Test
    void test_CreateChatWrongTitle() {
        Set<Id> members = IntStream.range(1, 20)
                .boxed()
                .map(Id::of)
                .collect(Collectors.toSet());

        StepVerifier.create(chatService.create(Id.of(25),"", "About", PermissionSet.all()))
                .expectSubscription()
                .expectError()
                .verify();
    }

    @Test
    void test_CreateChatAboutTooLong() {
        Set<Id> members = IntStream.range(1, 20)
                .boxed()
                .map(Id::of)
                .collect(Collectors.toSet());

        String tooLongAbout = RandomString.make(500);
        StepVerifier.create(chatService.create(Id.of(25),"", "About", PermissionSet.all()))
                .expectSubscription()
                .expectError()
                .verify();
    }

    @Test
    void test_CreateChatEmptyParticipants() {
        StepVerifier.create(chatService.create(Id.of(25),"", "About", PermissionSet.all()))
                .expectSubscription()
                .expectError()
                .verify();
    }

    @Test
    void test_CreateChatTooManyParticipants() {
        Set<Id> members = IntStream.range(1, 20)
                .boxed()
                .map(Id::of)
                .collect(Collectors.toSet());
        StepVerifier.create(chatService.create(Id.of(25),"", "About", PermissionSet.all()))
                .expectSubscription()
                .expectError()
                .verify();
    }

    @Test
    void test_EditChatTitle() {
        Set<Id> members = IntStream.range(1, 20)
                .boxed()
                .map(Id::of)
                .collect(Collectors.toSet());
        Chat chat = chatService.create(Id.of(25),"Chat title", "About", PermissionSet.all())
                .block();

        StepVerifier.create(chatService.editChatTitle(chat.getId(), chat.getOwnerUserId(), "New Title"))
                .expectSubscription()
                .verifyComplete();

        Mono<Chat> getChat = chatRepository.findById(chat.getId()).cache();

        StepVerifier.create(getChat)
                .expectSubscription()
                .assertNext(updated -> assertThat(updated.getTitle()).isEqualTo("New Title"))
                .verifyComplete();

        //chatParticipantRepository.deleteAll().block();

        chatRepository.deleteById(chat.getId()).block();

        System.out.println("Chat: " + getChat.block());
    }

    @Test
    void test_EditChatWithNoPermissions() {
        Chat chat = chatRepository.insert(Chat.builder()
                .ownerUserId(Id.of(155))
                .title(DEFAULT_TITLE)
                .description(DEFAULT_ABOUT)
                .membersCount(10)
                .updatedById(Id.of(155))
                .permissions(PermissionSet.all().getRawValue())
                .createdAt(Instant.now())
                .updatedAt(Instant.now())
                .build()).block();

        StepVerifier.create(chatService.editChatTitle(chat.getId(), Id.of(155), "New Title"))
                .expectSubscription()
                .expectError()
                .verify();

        StepVerifier.create(chatRepository.findById(chat.getId()))
                .expectSubscription()
                .assertNext(c -> assertThat(c.getTitle()).isEqualTo("Chat title"))
                .verifyComplete();

        chatRepository.deleteById(chat.getId()).block();
    }

    @Test
    void testAddParticipant() {
        Chat chat = chatRepository.insert(Chat.builder()
                .ownerUserId(Id.of(155))
                .title(DEFAULT_TITLE)
                .description(DEFAULT_ABOUT)
                .membersCount(10)
                .updatedById(Id.of(155))
                .permissions(PermissionSet.all().getRawValue())
                .createdAt(Instant.now())
                .updatedAt(Instant.now())
                .build()).block();

        chatParticipantRepository.insert(ChatParticipant.builder()
                .chatId(chat.getId())
                .userId(Id.of(155))
                .createdAt(Instant.now())
                .status(ChatParticipant.Status.OWNER)
                .inviterId(Id.of(155))
                .build()).block();

        StepVerifier.create(chatService.addChatUser(chat.getId(), Id.of(123), chat.getOwnerUserId()))
                .expectSubscription()
                .verifyComplete();

        StepVerifier.create(chatParticipantRepository.findByChatIdAndUserId(chat.getId(), Id.of(123)))
                .expectSubscription()
                .assertNext(participant -> {
                    assertThat(participant.getChatId()).isEqualTo(chat.getId());
                    assertThat(participant.getUserId()).isEqualTo(123);
                    assertThat(participant.getInviterId()).isEqualTo(chat.getOwnerUserId());
                    assertThat(participant.getStatus()).isEqualTo(ChatParticipant.Status.PARTICIPANT);
                })
                .verifyComplete();

        //chatParticipantRepository.deleteAll().block();
        chatRepository.deleteById(chat.getId()).block();
    }

    @Test
    void testKickParticipantFromChat() {
        Chat chat = chatRepository.insert(Chat.builder()
                .ownerUserId(Id.of(155))
                .title(DEFAULT_TITLE)
                .description(DEFAULT_ABOUT)
                .membersCount(10)
                .updatedById(Id.of(155))
                .permissions(PermissionSet.all().getRawValue())
                .createdAt(Instant.now())
                .updatedAt(Instant.now())
                .build()).block();

         chatParticipantRepository.insert(ChatParticipant.builder()
                .chatId(chat.getId())
                .userId(Id.of(155))
                .createdAt(Instant.now())
                .status(ChatParticipant.Status.OWNER)
                .inviterId(Id.of(155))
                .build()).block();

        chatParticipantRepository.insert(ChatParticipant.builder()
                .chatId(chat.getId())
                .userId(Id.of(152))
                .createdAt(Instant.now())
                .status(ChatParticipant.Status.PARTICIPANT)
                .inviterId(Id.of(155))
                .build()).block();

        StepVerifier.create(chatParticipantRepository.findByChatIdAndUserId(chat.getId(), Id.of(152)))
                .expectSubscription()
                .expectNextCount(0)
                .verifyComplete();

    }

    @Test
    void test_LeaveChat() {
        Chat chat = chatRepository.insert(Chat.builder()
                .ownerUserId(Id.of(155))
                .title(DEFAULT_TITLE)
                .description(DEFAULT_ABOUT)
                .membersCount(10)
                .updatedById(Id.of(155))
                .permissions(PermissionSet.all().getRawValue())
                .createdAt(Instant.now())
                .updatedAt(Instant.now())
                .build()).block();

        chatParticipantRepository
                .insert(EntityUtils.createChatOwner(chat.getOwnerUserId().asLong(), chat.getId().asLong())).block();

        chatParticipantRepository
                .insert(EntityUtils.createChatParticipant(Id.of(152).asLong(), chat.getOwnerUserId().asLong(),
                        chat.getId().asLong())).block();

        /*StepVerifier.create(chatParticipantRepository.findByChatIdAndUserId(chat.getId(), participant.getUserId()))
                .expectSubscription()
                .expectNextCount(0)
                .verifyComplete();*/

       // chatParticipantRepository.delete(owner).block();
        chatRepository.deleteById(chat.getId()).block();
    }

    @Test
    void test_LeaveChatOwner() {
        Chat chat = chatRepository.insert(Chat.builder()
                .ownerUserId(Id.of(155))
                .title(DEFAULT_TITLE)
                .description(DEFAULT_ABOUT)
                .membersCount(10)
                .updatedById(Id.of(155))
                .permissions(PermissionSet.all().getRawValue())
                .createdAt(Instant.now())
                .updatedAt(Instant.now())
                .build()).block();

        chatParticipantRepository.insert(ChatParticipant.builder()
                .chatId(chat.getId())
                .userId(Id.of(155))
                .createdAt(Instant.now())
                .status(ChatParticipant.Status.OWNER)
                .inviterId(Id.of(155))
                .build()).block();

        /*StepVerifier.create(chatParticipantRepository.findByChatIdAndUserId(chat.getId(), owner.getUserId()))
                .expectSubscription()
                .expectNextCount(1)
                .verifyComplete();*/
    }

    @Test
    void test_LoadFullChatParticipants() {
        Chat chat = chatRepository.insert(Chat.builder()
                .ownerUserId(Id.of(155))
                .title(DEFAULT_TITLE)
                .description(DEFAULT_ABOUT)
                .membersCount(10)
                .updatedById(Id.of(155))
                .permissions(PermissionSet.all().getRawValue())
                .createdAt(Instant.now())
                .updatedAt(Instant.now())
                .build()).block();

        Flux.range(1, 135)
                .map(id -> ChatParticipant.builder()
                        .chatId(chat.getId())
                        .userId(Id.of(id))
                        .createdAt(Instant.now())
                        .status(ChatParticipant.Status.PARTICIPANT)
                        .inviterId(Id.of(155))
                        .build())
                .concatMap(chatParticipantRepository::insert)
                .collectList()
                .block();

        StepVerifier.create(chatService.getChatParticipants(chat.getId()).collectList())
                .expectSubscription()
                .assertNext(participants -> {
                    System.out.println(participants);
                    assertThat(participants.size()).isEqualTo(135);
                })
                .verifyComplete();
    }
}