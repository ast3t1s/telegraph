package im.telegraph.domain.repository;

import im.telegraph.domain.entity.Id;
import im.telegraph.domain.entity.SequenceUpdate;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * @author Ast3t1s
 */
public interface UpdatesRepository {

    /**
     * Create new sequence update.
     *
     * @param update the sequence update to insert
     * @return A {@link Mono} where, upon successful completion, emits the {@link SequenceUpdate}.If an error is received,
     * it is emitted through the {@code Mono}.
     */
    Mono<SequenceUpdate> insert(SequenceUpdate update);

    /**
     * Create new sequence update.
     *
     * @param update the sequence update to insert
     * @return A {@link Mono} where, upon successful completion, emits the {@link SequenceUpdate}.If an error is received,
     * it is emitted through the {@code Mono}.
     */
    Mono<SequenceUpdate> insert(Id userId, Long timestamp, byte[] update);

    /**
     * Retrieve the last sequence update of the user.
     *
     * @param userId the Identifier of the user
     * @return A {@link Mono} where, upon successful completion, emits the {@link SequenceUpdate}.If an error is received,
     * it is emitted through the {@code Mono}.
     */
    Mono<SequenceUpdate> findMaxSeq(Id userId);

    /**
     * Retrieve stream of user's updates by the given user ID and sequence
     *
     * @param userId the Identifier of the user
     * @param sequence the sequence from which updates will be retrieved
     * @param limit the limit of the updates
     * @return A {@link Flux} where, upon successful completion, emits the {@link SequenceUpdate}.If an error is received,
     * it is emitted through the {@code Mono}.
     */
    Flux<SequenceUpdate> findSequenceAfter(Id userId, int sequence, int limit);
}
