package im.telegraph.generator.type;

public enum ParameterCategory {
    HIDDEN, VISIBLE, DANGER, COMPACT
} 
