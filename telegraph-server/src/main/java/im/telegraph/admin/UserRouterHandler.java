package im.telegraph.admin;

import com.fasterxml.jackson.databind.JsonNode;
import im.telegraph.admin.object.UserBean;
import im.telegraph.admin.object.request.RegisterUserRequest;
import im.telegraph.admin.route.AbstractRoute;
import im.telegraph.admin.route.Endpoint;
import im.telegraph.admin.route.Route;
import im.telegraph.admin.route.RouteUtils;
import im.telegraph.common.Page;
import im.telegraph.domain.entity.Id;
import im.telegraph.domain.entity.User;
import im.telegraph.service.UserService;
import io.netty.handler.codec.http.HttpMethod;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.netty.http.server.HttpServerRequest;
import reactor.netty.http.server.HttpServerResponse;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static io.netty.handler.codec.http.HttpResponseStatus.NO_CONTENT;
import static io.netty.handler.codec.http.HttpResponseStatus.OK;

/**
 * @author Ast3t1s
 */
@Component
public class UserRouterHandler extends AbstractRoute {

    private final UserService userService;

    public UserRouterHandler(UserService userService) {
        this.userService = userService;
    }

    @Override
    public List<Route> routes() {
        return List.of(
                Route.builder()
                        .endpoint(Endpoint.ofFixedPath(HttpMethod.POST, "/users/register"))
                        .action(this::handleCreateUser)
                        .corsHeaders(),
                Route.builder()
                        .endpoint(Endpoint.ofFixedPath(HttpMethod.PUT, "/users/{userId}/enable"))
                        .action(this::handleEnableUser)
                        .corsHeaders(),
                Route.builder()
                        .endpoint(Endpoint.ofFixedPath(HttpMethod.GET, "/users"))
                        .action(this::handleGetUsers)
                        .corsHeaders(),
                Route.builder()
                        .endpoint(Endpoint.ofFixedPath(HttpMethod.GET, "/users/search"))
                        .action(this::handleSearchUsers)
                        .corsHeaders(),
                Route.builder()
                        .endpoint(Endpoint.ofFixedPath(HttpMethod.DELETE, "/users/{userId}"))
                        .action(this::handleDeleteUser)
                        .corsHeaders());
    }

    protected Mono<Void> handleCreateUser(HttpServerRequest request, HttpServerResponse response) {
        return readJson(request, RegisterUserRequest.class)
                .flatMap(req -> userService.create(req.getUsername(), req.getEmail(), req.getPassword(),
                        req.getDisplayName(), req.getLangCode(), User.Role.of(req.getRole())))
                .map(this::toUserBean)
                .flatMap(res -> writeJson(response, res));
    }

    protected Mono<Void> handleEnableUser(HttpServerRequest request, HttpServerResponse response) {
        final Id userId = Optional.ofNullable(request.param("userId"))
                .map(Integer::valueOf).map(Id::of).orElse(null);

        if (userId == null) {
            return response.sendNotFound();
        }

        return request.receive().aggregate().asString()
                .flatMap(json -> Mono.fromCallable(() -> getObjectMapper().readTree(json)))
                .flatMap(node -> Mono.justOrEmpty(node.get("enabled"))
                        .map(JsonNode::asBoolean)
                        .flatMap(enabled -> userService.setEnabled(userId, enabled))
                        .then(response.status(NO_CONTENT).send()));
    }

    protected Mono<Void> handleGetUsers(HttpServerRequest request, HttpServerResponse response) {
        return userService.getUsers(RouteUtils.createPageRequest(request.uri()))
                .defaultIfEmpty(new Page<>(Collections.emptyList()))
                .map(userPage -> userPage.stream().map(this::toUserBean))
                .flatMap(users -> writeJson(response, users));
    }

    protected Mono<Void> handleSearchUsers(HttpServerRequest request, HttpServerResponse response) {
        String searchQuery = RouteUtils.queryParam("query", request.uri())
                .orElse(null);
        Integer limit = RouteUtils.queryParam("limit", request.uri())
                .map(Integer::parseInt).orElse(20);

        if (searchQuery == null || searchQuery.isEmpty()) {
            return response.status(OK).send();
        }

        return userService.searchUsers(searchQuery, limit)
                .map(this::toUserBean)
                .collectList()
                .flatMap(users -> writeJson(response, users));
    }

    protected Mono<Void> handleDeleteUser(HttpServerRequest request, HttpServerResponse response) {
        return Mono.justOrEmpty(request.param("userId"))
                .map(raw -> Id.of(Integer.valueOf(raw)))
                .flatMap(userService::deleteUser)
                .then(response.status(NO_CONTENT).send());
    }

    private UserBean toUserBean(final User user) {
        return new UserBean(user.getId().asLong(),
                user.getUsername(),
                user.getEmail(),
                user.getAbout().orElse(null),
                user.getEnabled(),
                user.getVerified(),
                user.getRole().getValue(),
                user.getLanguageCode(),
                user.getCreatedAt().toEpochMilli());
    }
}
