package im.telegraph.server.test;

import javax.validation.constraints.NotNull;

public class RpcException extends Exception {

    @NotNull
    private String tag;

    private int code;

    @NotNull
    private String message;

    public RpcException(@NotNull String tag, int code, @NotNull String message) {
        this.tag = tag;
        this.code = code;
        this.message = message;
    }

    @NotNull
    public String getTag() {
        return tag;
    }

    public int getCode() {
        return code;
    }

    @Override
    @NotNull
    public String getMessage() {
        return message;
    }

}
