package im.telegraph.server;

import im.telegraph.engine.serialization.DataInputStream;
import im.telegraph.protocol.model.mtproto.ProtoObject;

import java.io.IOException;

/**
 * @author Ast3t1s
 */
public abstract class MTProtoObject extends ProtoObject {

    protected MTProtoObject(DataInputStream inputStream) throws IOException {
        super(inputStream);
    }

    protected MTProtoObject() {

    }

    public abstract int getHeader();
}
