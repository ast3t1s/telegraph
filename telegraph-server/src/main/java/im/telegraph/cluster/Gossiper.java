package im.telegraph.cluster;

import im.telegraph.cluster.event.ClusterMembershipEvent;
import im.telegraph.cluster.internal.Gossip;
import im.telegraph.cluster.internal.GossipEnvelope;
import im.telegraph.cluster.internal.GossipOverview;
import im.telegraph.internal.remote.RemoteMessage;
import im.telegraph.internal.remote.RemoteTransport;
import im.telegraph.common.ResettableInterval;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.Disposable;
import reactor.core.Disposables;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.MonoSink;
import reactor.core.publisher.Sinks;
import reactor.core.scheduler.Scheduler;

import java.time.Duration;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

/**
 * @author Ast3t1s
 */
class Gossiper {

    private static final Logger log = LoggerFactory.getLogger(Gossiper.class);

    private final Map<String, GossipState> gossips = new ConcurrentHashMap<>();
    private final Map<String, MonoSink<String>> tasks = new ConcurrentHashMap<>();

    private final AtomicLong period = new AtomicLong(0);
    private final AtomicInteger gossipSequence = new AtomicInteger(0);

    private final List<Member> members = new CopyOnWriteArrayList<>();

    private final Sinks.Many<Gossip> gossipsSink = Sinks.many().multicast().directBestEffort();
    private final Disposable.Composite actionsDisposables = Disposables.composite();

    private final Member localMember;
    private final Scheduler scheduler;
    private final RemoteTransport transport;
    private final Sinks.Many<ClusterEvent> eventDispatcher;

    private final int gossipFanout;
    private final int gossipRepeatMult;
    private final ResettableInterval gossipTick;

    Gossiper(Member localMember, Scheduler scheduler, Duration gossipInterval, int gossipFanout, int gossipRepeatFactor,
             RemoteTransport transport, Sinks.Many<ClusterEvent> eventDispatcher) {
        this.localMember = localMember;
        this.scheduler = scheduler;
        this.transport = transport;
        this.eventDispatcher = eventDispatcher;
        this.gossipFanout = gossipFanout;
        this.gossipRepeatMult = gossipRepeatFactor;

        this.gossipTick = new ResettableInterval(scheduler, gossipInterval, gossipInterval,
                tick -> Mono.fromRunnable(this::gossip));

        startListeners();
    }

    void start() {
        gossipTick.start();
    }

    void stop() {
        gossipTick.stop();
        actionsDisposables.dispose();
    }

    Mono<String> spread(GossipOverview gossipOverview) {
        return Mono.justOrEmpty(gossipOverview)
                .publishOn(scheduler)
                .flatMap(overview -> Mono.create(sink -> tasks.put(createAndPutGossip(overview), sink)));
    }

    Flux<Gossip> listen() {
        return gossipsSink.asFlux().onBackpressureBuffer();
    }

    private void startListeners() {
        actionsDisposables.add(eventDispatcher.asFlux()
                .ofType(ClusterMembershipEvent.class)
                .publishOn(scheduler)
                .doOnNext(this::onMemberEvent)
                .subscribe(null, t -> log.warn("Membership event stream terminated with an error", t)));

        actionsDisposables.add(transport.receive()
                .publishOn(scheduler)
                .filter(message -> message.getPayload() instanceof GossipEnvelope)
                .doOnNext(this::onGossipReq)
                .subscribe(null, t -> log.warn("Gossip message stream terminated with an error", t)));
    }

    private void gossip() {
        final long period = this.period.incrementAndGet();
        if (gossips.isEmpty()) {
            return;
        }

        try {
            selectMembersForGossip().forEach(this::gossipTo);
            // Sweep gossips
            sweepGossips();
        } catch (Exception e) {
            log.warn("Unexpected error due gossips round[{}] spreading", period, e);
        }
    }

    private void onGossipReq(RemoteMessage message) {
        GossipEnvelope gossipEnvelope = (GossipEnvelope) message.getPayload();
        for (Gossip gossip : gossipEnvelope.getGossips()) {
            GossipState gossipState = gossips.get(gossip.gossipId());
            if (gossipState == null) {
                gossipState = new GossipState(gossip, period.get());
                gossips.put(gossip.gossipId(), gossipState);
                gossipsSink.tryEmitNext(gossip);
            }
            gossipState.addToInfected(gossipEnvelope.getFrom());
        }
    }

    private void onMemberEvent(ClusterMembershipEvent event) {
        if (event.isAdded()) {
            members.add(event.member());
        } else if (event.isRemoved()) {
            members.remove(event.member());
        }
    }

    private String createAndPutGossip(GossipOverview overview) {
        Gossip gossip = new Gossip(generateGossipId(), overview);
        GossipState state = new GossipState(gossip, period.get());
        gossips.put(gossip.gossipId(), state);
        return gossip.gossipId();
    }

    private String generateGossipId() {
        return localMember.getUniqueAddress().getId() + "_" + gossipSequence.incrementAndGet();
    }

    private void gossipTo(Member member) {
        List<Gossip> gossipToSpread = selectGossipsToSend(member);
        if (gossipToSpread.isEmpty()) {
            return;
        }
        // Send gossip request
        transport.send(member.getAddress(), new GossipEnvelope(localMember.getUniqueAddress(), gossipToSpread))
                .subscribe(null, t -> log.warn("Unexpected error do spreading gossips to :{}", member, t));
    }

    private List<Gossip> selectGossipsToSend(Member member) {
        final int periodsToSpread = gossipPeriodsToSpread(gossipRepeatMult, members.size() + 1);
        return gossips.values().stream()
                .filter(state -> state.infectionPeriod() + periodsToSpread >= period.get())
                .filter(state -> !state.isInfected(member.getUniqueAddress()))
                .map(GossipState::gossip)
                .collect(Collectors.toList());
    }

    private List<Member> selectMembersForGossip() {
        if (members.isEmpty()) {
            return Collections.emptyList();
        }
        List<Member> candidates = new ArrayList<>(members);
        Collections.shuffle(candidates);
        boolean selectAll = candidates.size() < gossipFanout;
        return selectAll ? candidates : candidates.subList(0, gossipFanout);
    }

    private void sweepGossips() {
        // Select gossips to sweep
        int periodsToSweep = gossipPeriodsToSweep(gossipRepeatMult, members.size() + 1);
        Set<GossipState> gossipsToRemove = gossips.values()
                .stream()
                .filter(gossipState -> period.get() > gossipState.infectionPeriod() + periodsToSweep)
                .collect(Collectors.toSet());

        // Check if anything selected
        if (gossipsToRemove.isEmpty()) {
            return; // nothing to sweep
        }

        // Sweep gossips
        log.debug("Sweep gossips: {}", gossipsToRemove);
        for (GossipState gossipState : gossipsToRemove) {
            gossips.remove(gossipState.gossip().gossipId());
            MonoSink<String> sink = tasks.remove(gossipState.gossip().gossipId());
            if (sink != null) {
                sink.success(gossipState.gossip().gossipId());
            }
        }
    }

    private static int gossipPeriodsToSpread(int repeatFactor, int clusterSize) {
        return repeatFactor * (32 - Integer.numberOfLeadingZeros(clusterSize));
    }

    private static int gossipPeriodsToSweep(int repeatFactor, int clusterSize) {
        int periodToSweep = gossipPeriodsToSpread(repeatFactor, clusterSize);
        return 2 * (periodToSweep + 1);
    }

    private static class GossipState {

        private final Gossip gossip;
        private final long infectionPeriod;
        private final Set<UniqueAddress> infected = new HashSet<>();

        private GossipState(Gossip gossip, long infectionPeriod) {
            this.gossip = gossip;
            this.infectionPeriod = infectionPeriod;
        }

        Gossip gossip() {
            return gossip;
        }

        long infectionPeriod() {
            return infectionPeriod;
        }

        void addToInfected(UniqueAddress memberId) {
            infected.add(memberId);
        }

        boolean isInfected(UniqueAddress memberId) {
            return infected.contains(memberId);
        }
    }
} 
