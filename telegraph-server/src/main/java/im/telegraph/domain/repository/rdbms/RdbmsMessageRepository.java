package im.telegraph.domain.repository.rdbms;

import im.telegraph.common.rdbms.RdbmsOperations;
import im.telegraph.domain.entity.Id;
import im.telegraph.domain.entity.Message;
import im.telegraph.domain.entity.Peer;
import im.telegraph.domain.repository.MessageRepository;
import io.r2dbc.spi.*;
import org.reactivestreams.Publisher;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Ast3t1s
 */
@Repository
public class RdbmsMessageRepository extends AbstractRdbmsRepository implements MessageRepository {

    private static final String FIELDS = "id, peer_id, peer_type, user_id, sender_user_id, random_id, content_header," +
            " content_data, date, deleted_at";

    private static final String INSERT_MESSAGE_QUERY = "INSERT INTO history_messages (" + FIELDS +") VALUES (:id, " +
            ":peerId, :peerType, :userId, :senderId, :randomId, :header, :content, :date, :deletedAt)";

    private static final String INSERT_MESSAGE_BULK_QUERY =
            "INSERT INTO history_messages (" + FIELDS + ") VALUES :tuples";

    private static final String GET_MESSAGES_BY_IDS_QUERY =
            "SELECT " + FIELDS + " FROM history_messages WHERE id IN (:ids) AND deleted_at IS NULL";

    private static final String GET_MESSAGES_PEER_QUERY =
            "SELECT " + FIELDS + " FROM history_messages WHERE peer_id = :peerId AND peer_type = :peerType " +
                    "AND user_id = :userId AND date > :timestamp AND deleted_at IS NULL";

    private static final String GET_PEER_MESSAGES_BY_IDS_QUERY =
            "SELECT " + FIELDS + " FROM history_messages WHERE peer_id = :peerId AND peer_type = :peerType AND " +
                    "user_id = :userId AND id IN (:ids) AND deleted_at IS NULL";

    private static final String DELETE_MESSAGES_QUERY =
            "DELETE FROM history_messages WHERE is IN (:ids)";

    private final RdbmsOperations rdbmsOperations;

    private final Function<Message, Map<String, Parameter>> messageParameterMapper =
            new HistoryMessageParameterMapper();

    private final BiFunction<Row, RowMetadata, Message> historyMessageRowMapper = new HistoryMessageRowMapper();

    public RdbmsMessageRepository(ConnectionFactory connectionFactory) {
        super(connectionFactory);
        this.rdbmsOperations = RdbmsOperations.create(connectionFactory);
    }

    @Override
    public Mono<Message> insert(Message message) {
        return rdbmsOperations.withHandle(handle -> handle.createUpdate(INSERT_MESSAGE_QUERY)
                .bindMap(messageParameterMapper.apply(message))
                .execute()
                .then(Mono.just(message)))
                .next();
    }

    @Override
    public Mono<Void> save(Publisher<Message> messages) {
        return Flux.from(messages)
                .map(message -> new Object[]{message.getId().asLong(),
                        message.getPeer().getPeerId(),
                        message.getPeer().getType().getValue(),
                        message.getUserId().asLong(),
                        message.getSenderId().asLong(),
                        message.getRandomId(),
                        message.getType().getValue(),
                        message.getContent(),
                        message.getCreatedAt(),
                        Parameters.in(R2dbcType.TIMESTAMP, message.getDeletedAt().orElse(null))})
                .collectList()
                .flatMap(tuples -> rdbmsOperations.useTransaction(handle -> handle
                        .createUpdate(INSERT_MESSAGE_BULK_QUERY)
                        .bind("tuples", tuples)
                        .execute()));
    }

    @Override
    public Flux<Message> findAllByIdInAndDeletedAtIsNull(Collection<Id> ids) {
        return rdbmsOperations.withHandle(handle -> handle.createQuery(GET_MESSAGES_BY_IDS_QUERY)
                .bind("ids", ids.stream().map(Id::asLong).collect(Collectors.toList()))
                .mapRow(historyMessageRowMapper));
    }

    @Override
    public Flux<Message> findAllByUserIdAndPeerDateLessThanAndDeletedAtIsNull(Id userId, Peer peer, Instant date, int limit) {
        return rdbmsOperations.withHandle(handle -> handle.createQuery(limitQuery(GET_MESSAGES_PEER_QUERY, limit))
                .bind("peerId", peer.getPeerId())
                .bind("peerType", peer.getType().getValue())
                .bind("userId", userId.asLong())
                .bind("timestamp", LocalDateTime.ofInstant(date, ZoneOffset.UTC))
                .mapRow(historyMessageRowMapper));
    }

    @Override
    public Flux<Message> findAllByUserIdAndPeerAndIdInAndDeletedAtIsNull(Id userId, Peer peer, List<Id> ids) {
        return rdbmsOperations.withHandle(handle -> handle.createQuery(GET_PEER_MESSAGES_BY_IDS_QUERY)
                .bind("peerId", peer.getPeerId())
                .bind("peerType", peer.getType().getValue())
                .bind("userId", userId.asLong())
                .bind("ids", ids.stream().map(Id::asLong).collect(Collectors.toList()))
                .mapRow(historyMessageRowMapper));
    }

    @Override
    public Flux<Message> findAllByUserIdAndPeerAndDeletedAtIsNullOrderByCreatedAtDesc(Id userId, Peer peer, int limit, int offset) {
        return null;
    }

    @Override
    public Mono<Long> deleteByIdIn(Collection<Id> ids) {
        return rdbmsOperations.withHandle(handle -> handle.createUpdate(DELETE_MESSAGES_QUERY)
                .bind("ids", ids.stream().map(Id::asLong).collect(Collectors.toList()))
                .execute())
                .next();
    }

    private static class HistoryMessageParameterMapper implements Function<Message, Map<String, Parameter>> {

        @Override
        public Map<String, Parameter> apply(Message message) {
            final Map<String, Parameter> parameters = new HashMap<>();
            parameters.put("id", Parameters.in(R2dbcType.BIGINT, message.getId().asLong()));
            parameters.put("peerId", Parameters.in(R2dbcType.BIGINT, message.getPeer().getPeerId()));
            parameters.put("peerType", Parameters.in(R2dbcType.SMALLINT, message.getPeer().getType().getValue()));
            parameters.put("userId", Parameters.in(R2dbcType.BIGINT, message.getUserId().asLong()));
            parameters.put("senderId", Parameters.in(R2dbcType.BIGINT, message.getSenderId().asLong()));
            parameters.put("randomId", Parameters.in(R2dbcType.BIGINT, message.getRandomId()));
            parameters.put("header", Parameters.in(R2dbcType.SMALLINT, message.getType().getValue()));
            parameters.put("content", Parameters.in(R2dbcType.BINARY, message.getContent()));
            parameters.put("date", Parameters.in(R2dbcType.TIMESTAMP,
                    LocalDateTime.ofInstant(message.getCreatedAt(), ZoneOffset.UTC)));
            parameters.put("deletedAt", Parameters.in(R2dbcType.TIMESTAMP, message.getDeletedAt()
                    .map(deletedAt -> LocalDateTime.ofInstant(deletedAt, ZoneOffset.UTC)).orElse(null)));
            return parameters;
        }
    }

    private static class HistoryMessageRowMapper implements BiFunction<Row, RowMetadata, Message> {

        @Override
        public Message apply(Row row, RowMetadata metadata) {
            return Message.builder()
                    .id(Id.of(row.get("id", Long.class)))
                    .randomId(row.get("random_id", Long.class))
                    .peer(Peer.of(row.get("peer_id", Long.class),
                            Peer.Type.of(row.get("peer_type", Integer.class))))
                    .senderId(Id.of(row.get("sender_user_id", Long.class)))
                    .type(Message.Type.of(row.get("content_header", Integer.class)))
                    .content(row.get("content_data", byte[].class))
                    .createdAt(row.get("date", LocalDateTime.class).toInstant(ZoneOffset.UTC))
                    .deletedAt(Optional.ofNullable(row.get("deleted_at", LocalDateTime.class))
                            .map(time -> time.toInstant(ZoneOffset.UTC)).orElse(null))
                    .build();
        }
    }
}
