package im.telegraph.common.rdbms.bind;

import io.r2dbc.spi.Statement;

/**
 * @author Ast3t1s
 */
public interface BindMarker {

    /**
     * Return the database-specific placeholder for a given substitution.
     */
    String getPlaceholder();

    /**
     * Bind the given {@code value} to the {@link Statement} using the underlying binding strategy.
     * @param bindOperation the target to bind the value to
     * @param value the actual value (must not be {@code null};
     * use {@link #bindNull(BindOperation, Class)} for {@code null} values)
     * @see Statement#bind
     */
    void bind(BindOperation bindOperation, Object value);

    /**
     * Bind a {@code null} value to the {@link Statement} using the underlying binding strategy.
     * @param bindOperation the target to bind the value to
     * @param valueType the value type (must not be {@code null})
     * @see Statement#bindNull
     */
    void bindNull(BindOperation bindOperation, Class<?> valueType);
}
