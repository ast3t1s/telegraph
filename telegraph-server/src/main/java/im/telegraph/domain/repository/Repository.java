package im.telegraph.domain.repository;

import reactor.core.publisher.Mono;

/**
 * The base repository interface
 *
 * @author Ast3t1s
 */
public interface Repository<T, ID> {

    /**
     * Finds an entity by its identifier.
     *
     * @param id the identifier of the entity
     * @return a Mono emitting the entity if found, or empty if not found
     */
    default Mono<T> findById(ID id) {
        return Mono.empty();
    }

    /**
     * Creates a new entity.
     *
     * @param item the entity to create
     * @return a Mono emitting the created entity
     */
    default Mono<T> create(T item) {
        return Mono.empty();
    }

    /**
     * Updates an existing entity.
     *
     * @param item the entity to update
     * @return a Mono emitting the updated entity
     */
    default Mono<T> update(T item) {
        return Mono.empty();
    }

    /**
     * Deletes an existing entity.
     *
     * @param item the entity to delete
     * @return a Mono emitting an empty result upon successful deletion
     */
    default Mono<Void> delete(T item) {
        return Mono.empty();
    }
}
