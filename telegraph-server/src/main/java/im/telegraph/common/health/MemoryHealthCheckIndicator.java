package im.telegraph.common.health;

import com.sun.management.HotSpotDiagnosticMXBean;
import com.sun.management.OperatingSystemMXBean;
import com.sun.management.VMOption;
import im.telegraph.common.property.HealthCheckProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.management.BufferPoolMXBean;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.time.Instant;

public class MemoryHealthCheckIndicator implements HealthCheckIndicator {

    private static final Logger log = LoggerFactory.getLogger(MemoryHealthCheckIndicator.class);

    private final OperatingSystemMXBean operatingSystemMXBean;
    private final BufferPoolMXBean directBufferPoolBean;
    private final MemoryMXBean memoryMXBean;

    private final long maxAvailableMemory;
    private final long maxAvailableDirectMemory;
    private final long maxDirectMemory;
    private final long maxHeapMemory;
    private final int minFreeSystemMemory;
    private final long totalPhysicalMemorySize;

    private long usedAvailableMemory;
    private long usedDirectMemory;
    private long usedHeapMemory;
    private long usedNonHeapMemory;
    private long usedSystemMemory;
    private long freeSystemMemory;

    private final int directMemoryWarningThresholdPercentage;
    private final int heapMemoryWarningThresholdPercentage;
    private final int minMemoryWarningIntervalMillis;

    private long lastDirectMemoryWarningTimestamp;
    private long lastHeapMemoryWarningTimestamp;

    private final int heapMemoryGcThresholdPercentage;
    private final int minHeapMemoryGcIntervalMillis;

    private long lastHeapMemoryGcTimestamp;

    public MemoryHealthCheckIndicator(HealthCheckProperties properties) {
        this.operatingSystemMXBean = ManagementFactory.getPlatformMXBean(OperatingSystemMXBean.class);

        HealthCheckProperties.MemoryHealthProperties memoryHealthProperties = properties.getMemory();

        HotSpotDiagnosticMXBean diagnosticBean = ManagementFactory.getPlatformMXBean(HotSpotDiagnosticMXBean.class);
        VMOption disableExplicitGC = diagnosticBean.getVMOption("DisableExplicitGC");
        if (!Boolean.FALSE.toString().equals(disableExplicitGC.getValue())) {
            throw new IllegalStateException("\"DisableExplicitGC\" is enabled while it should be disabled");
        }

        this.directBufferPoolBean = ManagementFactory.getPlatformMXBeans(BufferPoolMXBean.class)
                .stream()
                .filter(bean -> "direct".equals(bean.getName()))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("Can not find the direct buffer pool management"));

        this.memoryMXBean = ManagementFactory.getMemoryMXBean();

        // "-XX:MaxDirectMemorySize" or "Runtime.getRuntime().maxMemory()"
        this.maxDirectMemory = Runtime.getRuntime().maxMemory();
        if (this.maxDirectMemory < 0) {
            throw new IllegalStateException("Can not direct the max memory: " + maxDirectMemory);
        }

        this.maxAvailableDirectMemory = (long) (this.maxDirectMemory * (memoryHealthProperties.getMaxAvailableDirectMemory() / 100F));
        this.maxHeapMemory = this.memoryMXBean.getHeapMemoryUsage().getMax();
        if (this.maxHeapMemory < 0) {
            throw new IllegalStateException("Can not direct max heap memory: " + maxHeapMemory);
        }

        this.totalPhysicalMemorySize = this.operatingSystemMXBean.getTotalMemorySize();
        this.maxAvailableMemory = (long) (totalPhysicalMemorySize * (memoryHealthProperties.getMaxAvailableMemory() / 100F));

        final int minAvailableMemory = 1000 * 1024 * 1024;

        if (this.maxAvailableMemory < minAvailableMemory) {
            throw new IllegalStateException(String.format("The max available memory is too small to run. Actual: %s. Expected: >= %s",
                    maxAvailableMemory, minAvailableMemory));
        }

        if (this.maxAvailableMemory < this.maxHeapMemory) {
            throw new IllegalStateException(String.format("The max available memory %s should not be less than max heap memory %s",
                    maxAvailableMemory, maxAvailableMemory));
        }

        final int estimatedMaxNonHeapMemory = 256 * 1024 * 1024;
        if (this.maxAvailableMemory > this.maxAvailableDirectMemory + this.maxHeapMemory + estimatedMaxNonHeapMemory) {
            log.warn("The max available memory {} is larger than total of the available direct memory {}," +
                            " the max heap memory {}, and the estimated max non-heap memory {} which indicates" +
                            " that some memory will never be used by the server",
                    maxAvailableMemory, maxAvailableDirectMemory, maxHeapMemory, estimatedMaxNonHeapMemory);
        }

        this.directMemoryWarningThresholdPercentage = memoryHealthProperties.getDirectMemoryWarningThreshold();
        this.heapMemoryWarningThresholdPercentage = memoryHealthProperties.getHeapMemoryGCThreshold();
        this.minMemoryWarningIntervalMillis = (int) memoryHealthProperties.getMinHeapMemoryGCInterval().toMillis();
        this.minFreeSystemMemory = (int) memoryHealthProperties.getMinFreeSystemMemory().toBytes();
        this.heapMemoryGcThresholdPercentage = memoryHealthProperties.getHeapMemoryGCThreshold();
        this.minHeapMemoryGcIntervalMillis = (int) memoryHealthProperties.getMinHeapMemoryGCInterval().toMillis();
    }

    @Override
    public boolean isHealthy() {
        return (this.usedAvailableMemory < this.maxAvailableMemory)
                || (this.usedDirectMemory < this.maxAvailableDirectMemory)
                || (this.freeSystemMemory > this.minFreeSystemMemory);
    }

    @Override
    public void updateHealthStatus() {
        // No need to call "UnpooledByteBufAllocator.DEFAULT.metric().usedDirectMemory()"
        // and "PooledByteBufAllocator.DEFAULT.metric().usedDirectMemory()"
        // because we have requested Netty to create DirectBuffer instances via its constructor with the counter supported by JDK
        this.usedDirectMemory = this.directBufferPoolBean.getMemoryUsed();
        this.usedHeapMemory = this.memoryMXBean.getHeapMemoryUsage().getUsed();
        // Non-heap memory pools: [CodeHeap 'non-nmethods', CodeHeap 'non-profiled nmethods', CodeHeap 'profiled nmethods',
        // Compressed Class Space, Metaspace]
        // via ManagementFactory.getMemoryPoolMXBeans().stream().filter(bean -> bean.getType() == MemoryType.NON_HEAP)
        // .map(MemoryPoolMXBean::getName).sorted().toList().toString()
        this.usedNonHeapMemory = this.memoryMXBean.getNonHeapMemoryUsage().getUsed();
        this.usedAvailableMemory = this.usedDirectMemory + this.usedHeapMemory + this.usedNonHeapMemory;
        this.freeSystemMemory = this.operatingSystemMXBean.getFreeMemorySize();
        this.usedSystemMemory = this.totalPhysicalMemorySize - this.freeSystemMemory;

        logMemoryStatus();
    }

    private void logMemoryStatus() {
        final String memoryPrintLog = String.format("Used system memory: %s/%s; \n" +
                        " Used available memory: %s/%s; \n" +
                        " Used direct memory: %s/%s/%s; \n" +
                        " Used heap memory: %s/%s \n" +
                        " Used non heap memory: %s",
                asMbString(this.usedSystemMemory), asMbString(this.totalPhysicalMemorySize),
                asMbString(this.usedAvailableMemory), asMbString(this.maxAvailableMemory),
                asMbString(this.usedDirectMemory), asMbString(this.maxAvailableDirectMemory), asMbString(this.maxDirectMemory),
                asMbString(this.usedHeapMemory), asMbString(this.maxHeapMemory),
                asMbString(this.usedNonHeapMemory));

        if (this.isHealthy()) {
            log.debug(memoryPrintLog);
        } else {
            log.warn(memoryPrintLog);
        }

        final Instant now = Instant.now();

        float usedMemoryPercentage = 100F * this.usedDirectMemory / this.maxDirectMemory;
        if (this.directMemoryWarningThresholdPercentage > 0
                && this.directMemoryWarningThresholdPercentage < usedMemoryPercentage
                && this.minMemoryWarningIntervalMillis < (now.toEpochMilli() - lastDirectMemoryWarningTimestamp)) {
            this.lastDirectMemoryWarningTimestamp = now.toEpochMilli();
            log.warn("The used direct has exceeded the warning threshold: {}/{}/{}/{}",
                    asMbString(this.usedDirectMemory), asMbString(this.maxDirectMemory), usedMemoryPercentage, this.directMemoryWarningThresholdPercentage);
        }
        usedMemoryPercentage = 100F * this.usedHeapMemory / this.maxHeapMemory;
        if (this.heapMemoryWarningThresholdPercentage > 0 && this.heapMemoryWarningThresholdPercentage < usedMemoryPercentage
                && this.minMemoryWarningIntervalMillis < (now.toEpochMilli() - this.lastHeapMemoryWarningTimestamp)) {
            this.lastHeapMemoryWarningTimestamp = now.toEpochMilli();
            log.warn("The used heap memory has exceeded the warning threshold: {}/{}/{}/{}",
                    asMbString(this.usedHeapMemory), asMbString(this.maxHeapMemory), usedMemoryPercentage, this.heapMemoryWarningThresholdPercentage);
        }

        // TODO: add tests
        if (!isHealthy() && this.heapMemoryGcThresholdPercentage > 0 && this.heapMemoryGcThresholdPercentage < usedMemoryPercentage
                && this.minHeapMemoryGcIntervalMillis < (now.toEpochMilli() - this.lastHeapMemoryGcTimestamp)) {
            this.lastHeapMemoryGcTimestamp = now.toEpochMilli();
            log.info("Trying to start GC because the available memory has exceeded and the used heap memory has exceeded the GC threshold: {}/{}/{}/{}",
                    asMbString(usedHeapMemory), asMbString(maxHeapMemory), usedMemoryPercentage, heapMemoryGcThresholdPercentage);
            System.gc();
        }
    }

    private String asMbString(long bytes) {
        return bytes / 1024 / 1024 + "MB";
    }
}
