package im.telegraph.admin.object.request;

/**
 * @author Ast3t1s
 */
public class RegisterUserRequest {

    private String username;
    private String email;
    private String password;
    private String displayName;
    private String langCode;
    private String role;

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getLangCode() {
        return langCode;
    }

    public String getRole() {
        return role;
    }

    @Override
    public String toString() {
        return "RegisterUserRequest{" +
                "username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", displayName='" + displayName + '\'' +
                ", langCode='" + langCode + '\'' +
                ", role='" + role + '\'' +
                '}';
    }
}
