package im.telegraph.domain.repository.rdbms.dialect;

import java.util.List;
import java.util.Map;

/**
 * @author Ast3t1s
 */
public interface Dialect {

    /**
     * Returns the {@code LIMIT} clause to limit results.
     *
     * @param limit the maximum number of lines returned when the resulting SQL snippet is used.
     * @return rendered limit clause.
     * @see #getLimitOffset(int, int)
     */
    String getLimit(int limit);

    /**
     * Returns the {@code OFFSET} clause to consume rows at a given offset.
     *
     * @param offset the numbers of rows that get skipped when the resulting SQL snippet is used.
     * @return rendered offset clause.
     * @see #getLimitOffset(int, int)
     */
    String getOffset(int offset);

    /**
     * Returns a combined {@code LIMIT/OFFSET} clause that limits results and starts consumption at the given
     * {@code offset}.
     *
     * @param limit  the maximum number of lines returned when the resulting SQL snippet is used.
     * @param offset the numbers of rows that get skipped when the resulting SQL snippet is used.
     * @return rendered limit clause.
     * @see #getLimit(int)
     * @see #getOffset(int)
     */
    String getLimitOffset(int limit, int offset);

    /**
     * Build an upsert query, if record exists update, otherwise insert.
     *
     * @param table the table name
     * @param insertSpec the insert spec, {@code Map} where key is column name and value is bindable name, for
     *                   example (column name display_name, bindable value displayName)
     * @param uniqueColumns the unique columns, list of unique columns which identify record
     * @param updateColumns the list of columns to update
     * @param incrementFields the increment fields, optional values for fields that should be incremented like count + 1
     * @return built upsert query
     */
    String buildUpsertQuery(String table, Map<String, Object> insertSpec, List<String> uniqueColumns,
                            List<String> updateColumns, String... incrementFields);


    /**
     * Build an upsert query for batch, if records exist update, otherwise insert.
     *
     * @param table the table name
     * @param insertSpec the insert spec, {@code Map} where key is column name and value is bindable name, for
     *                   example (column name display_name, bindable value displayName)
     * @param uniqueColumns the unique columns, list of unique columns which identify record
     * @param updateColumns the list of columns to update
     * @param incrementFields the increment fields, optional values for fields that should be incremented like count + 1
     * @return built upsert query
     */
    String buildUpsertQueryMany(String table, List<String> insertSpec, String batchName, List<String> uniqueColumns,
                                List<String> updateColumns, String... incrementFields);

}
