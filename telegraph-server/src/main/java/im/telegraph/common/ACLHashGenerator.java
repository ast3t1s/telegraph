package im.telegraph.common;

import im.telegraph.util.Crypto;
import im.telegraph.util.ThreadLocalSecureRandom;

import java.nio.ByteBuffer;

public class ACLHashGenerator {

    private final ThreadLocalSecureRandom random = ThreadLocalSecureRandom.current();

    private final String secretKey;

    public ACLHashGenerator(String secretKey) {
        this.secretKey = secretKey;
    }

    public long generateUserAccessHash(long authId, long userId, String accessSalt) {
        return hash(String.format("%d:%d:%s:%s", authId, userId, accessSalt, secretKey));
    }

    public long generateFileAccessHash(final long fileId, final String accessSalt) {
        return hash(String.format("%d:%s:%s", fileId, accessSalt, secretKey));
    }

    public long generatePhotoAccessHash(final long photoId, final String accessSalt) {
        return hash(String.format("%d:%s:%s", photoId, accessSalt, secretKey));
    }

    public long generateChatAccessHash(final long chatId, final String accessSalt) {
        return hash(String.format("%d:%s:%s", chatId, accessSalt, secretKey));
    }

    public long randomHash() {
        return random.nextLong();
    }

    public String nextAccessSalt() {
        return String.valueOf(random.nextLong());
    }

    public String random() {
        return String.valueOf(random.nextLong());
    }

    private Long hash(String s) {
        return ByteBuffer.wrap(Crypto.SHA256(s.getBytes())).getLong();
    }
} 
