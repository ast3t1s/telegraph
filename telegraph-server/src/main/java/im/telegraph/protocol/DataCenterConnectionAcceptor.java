package im.telegraph.protocol;

import im.telegraph.domain.entity.Id;
import im.telegraph.handlers.RpcRequestDispatcher;
import im.telegraph.protocol.auth.AuthKeyHandler;
import im.telegraph.protocol.auth.AuthorizationException;
import im.telegraph.protocol.cryptography.TrustedKeyStore;
import im.telegraph.protocol.model.ProtoSerializer;
import im.telegraph.protocol.model.mtproto.*;
import im.telegraph.protocol.model.transport.*;
import im.telegraph.rpc.base.Request;
import im.telegraph.rpc.base.Response;
import im.telegraph.server.ConnectionAcceptor;
import im.telegraph.server.MTProtoObject;
import im.telegraph.server.TransportConnection;
import im.telegraph.server.TransportFrame;
import im.telegraph.service.AuthKeyService;
import im.telegraph.service.AuthService;
import im.telegraph.util.Crypto;
import im.telegraph.util.ThreadLocalCRC32;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;
import reactor.function.TupleUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Stream;

@Component
public class DataCenterConnectionAcceptor implements ConnectionAcceptor {

    private static final Logger log = LoggerFactory.getLogger(DataCenterConnectionAcceptor.class);

    enum State {AWAIT_HANDSHAKE, PASSING}

    private final Set<Byte> protoVersions = Set.of((byte) 1, (byte) 2, (byte) 3);
    private final Set<Byte> majorVersions = Set.of((byte) 1);

    private final AuthKeyService authKeyService;
    private final AuthService authService;
    private final MTProtoSessionRegistry sessionRegistry;
    private final RpcRequestDispatcher rpcRequestAdapter;
    private final AuthKeyHandler authorizationManager;

    private final Scheduler scheduler = Schedulers.newParallel("transport", Schedulers.DEFAULT_POOL_SIZE, true);

    public DataCenterConnectionAcceptor(AuthKeyService authKeyService,
                                        AuthService authService,
                                        TrustedKeyStore trustedKeyStore,
                                        MTProtoSessionRegistry sessionRegistry,
                                        RpcRequestDispatcher rpcRequestAdapter) {
        this.authKeyService = authKeyService;
        this.authService = authService;
        this.sessionRegistry = sessionRegistry;
        this.rpcRequestAdapter = rpcRequestAdapter;
        this.authorizationManager = new AuthKeyHandler(trustedKeyStore) {

            @Override
            protected Mono<Void> storeAuthKey(byte[] masterKey) {
                return authKeyService.create(masterKey).then();
            }
        };
    }

    @Override
    public Mono<Void> accept(TransportConnection connection) {
        return Mono.defer(() -> {
            if (log.isDebugEnabled()) {
                log.info("Transport connection: {} is created", connection);
            }
            AtomicReference<State> handshake = new AtomicReference<>();

            return connection.receive()
                    .doOnSubscribe(sub -> {
                        handshake.set(State.AWAIT_HANDSHAKE);
                        if (log.isDebugEnabled()) {
                            log.debug("Subscribe to {} socket session context", Integer.toHexString(sub.hashCode()));
                        }
                    })
                    .doOnNext(packet -> {
                        if (log.isTraceEnabled()) {
                            log.trace("Received transport packet: {}", packet);
                        }
                    })
                    .publishOn(scheduler)
                    .concatMap(packet -> handleInternal(handshake, connection, packet))
                    .onBackpressureBuffer()
                    .doOnError(ex -> log.warn("Unexpected error while listening connection {} stream", connection, ex))
                    .doFinally(signal -> {
                        if (log.isDebugEnabled()) {
                            log.debug("Subscription {} to {} disposed due to {}",
                                    Integer.toHexString(handshake.get().hashCode()), connection, signal);
                        }
                    })
                    .onErrorResume(error ->  handleError(connection, error))
                    .then();
        });
    }

    protected Mono<Void> handleInternal(AtomicReference<State> handshake, TransportConnection connection, TransportFrame frame) {
        return Mono.defer(() -> {
            if (log.isTraceEnabled()) {
                log.info("Handle transport packet: {} from connection :{}", frame, connection);
            }
            final var packetIndex = frame.getPackageIndex();
            final var header = frame.getHeader();
            final var bodySize = frame.getSize();
            final var content = frame.getPayload();
            final var crc32 = frame.getChecksum();

            final var checksum = ThreadLocalCRC32.calculateChecksum(content);

            if (bodySize != content.length) {
                log.warn("Incorrect packet size got: " + content.length + " expected: " + bodySize);
                return connection.send(Mono.just(new Drop(0, 0, "Incorrect payload size")));
            }

            if (checksum != crc32) {
                log.warn("Incorrect CRC32 hash got: " + checksum + " expected: " + crc32);
                return connection.send(Mono.just(new Drop(0, 0, String.format("Package %d has incorrect crc32", packetIndex))));
            }

            if (header != HandshakeRequest.HEADER && handshake.get() != State.PASSING) {
                log.warn("Expect handshake packet, but got: " + header);
                return connection.send(Mono.just(new Drop(0, 0, String.format("Expected handshake request but got %d", header))));
            }

            MTProtoObject mtProtoObject;
            try {
                mtProtoObject = ProtoSerializer.readTransportPayload(header, content);
            } catch (IOException e) {
                log.warn("Unable to parse proto frame {}", header, e);
                return connection.send(Mono.just(new Drop(0, 0, String.format("Error parsing package %d", header))));
            }

            if (mtProtoObject instanceof HandshakeRequest) {
                HandshakeRequest request = (HandshakeRequest) mtProtoObject;

                final var protoVersion = request.getMtprotoVersion();
                final var apiMajor = request.getApiMajorVersion();
                final var apiMinor = request.getApiMinorVersion();
                final var handshakeData = request.getHandshakeData();

                if (log.isTraceEnabled()) {
                    log.trace("Received client: {} handshake packet protoVersion: {}, major: {}, minor: {}",
                            connection.getId(), protoVersion, apiMajor, apiMinor);
                }

                final var mtprotoVersion = protoVersions.contains((byte) protoVersion) ? protoVersion : 0;
                final var apiMajorVersion = majorVersions.contains((byte) apiMajor) ? apiMajor : 0;
                final var apiMinorVersion = apiMajorVersion == 0 ? 0 : apiMinor;

                return connection.send(Mono.just(new HandshakeResponse(mtprotoVersion, apiMajorVersion, apiMinorVersion, Crypto.SHA256(handshakeData))))
                        .doOnSuccess(__ -> handshake.set(State.PASSING));
            } else if (mtProtoObject instanceof Ping) {
                Ping ping = (Ping) mtProtoObject;
                return connection.send(Mono.just(new Pong(ping.getRandomId())));
            } else if (mtProtoObject instanceof Ack) {
                if (log.isTraceEnabled()) {
                    log.trace("Received ack message: {} from connection: {}", packetIndex, connection);
                }
                return Mono.empty();
            } else if (mtProtoObject instanceof Pong) {
                return connection.send(Mono.just(new Ack(packetIndex)));
            } else if (mtProtoObject instanceof ProtoPackage) {
                ProtoPackage protoPackage = (ProtoPackage) mtProtoObject;

                final var authId = protoPackage.getAuthId();
                final var sessionId = protoPackage.getSessionId();
                final var payload = protoPackage.getPayload();

                if (log.isTraceEnabled()) {
                    log.trace("Received proto packet: [auth ID: {}, session ID: {}, message ID: {}, payload size: {}]",
                            authId, sessionId, payload.getMessageId(), payload.getPayload().length);
                }

                Mono<Void> handleProtoPacket = Mono.defer(() -> {
                    if (authId == 0) {
                        return Mono.fromCallable(() -> ProtoSerializer.readMessagePayload(payload.getPayload()))
                                .flatMap(request -> authorizationManager.handle(connection, request))
                                .then();
                    }

                    return sessionRegistry.getSession(SessionId.create(authId, sessionId))
                            .switchIfEmpty(Mono.defer(() -> createSession(connection, authId, sessionId)))
                            .flatMap(session -> {
                                if (protoPackage.getAuthId() != session.getId().getAuthId()) {
                                    log.warn("Auth ID has changed expected: {} but got: {}",
                                            session.getId().getAuthId(), protoPackage.getAuthId());
                                    return Mono.error(new MTProtoException("Auth ID has changed"));
                                }
                                if (protoPackage.getSessionId() != session.getId().getSessionId()) {
                                    log.warn("Session ID has changed expected: {} but got: {}",
                                            session.getId().getSessionId(), protoPackage.getSessionId());
                                    return Mono.error(new MTProtoException("Session ID has changed"));
                                }

                                return Mono.from(session.getCipher().decrypt(protoPackage.getPayload()))
                                        .flatMap(message -> processProtoMessage(session, message));
                            });
                });
                return handleProtoPacket.and(connection.send(Mono.just(new Ack(packetIndex))));
            }

            return connection.send(Mono.just(new Drop(0, 0, String.format("Unknown package header %d", header))));
        });
    }

    protected Mono<Void> processProtoMessage(MTProtoSession session, ProtoMessage message) {
        return Mono.from(RpcContentExtractor.read(message.getPayload())).flatMap(payload -> {
            if (payload instanceof MTRpcRequest) {
                MTRpcRequest rpcRequest = (MTRpcRequest) payload;
                return Mono.from(RpcContentExtractor.readRpcRequestPayload(rpcRequest.getPayload()))
                        .flatMap(request -> Mono.from(RpcContentExtractor.readRpcRequest(request.getRequestType(), request.getPayload())))
                        .flatMap(request -> Mono.from(rpcRequestAdapter
                                .dispatch(session, message.getMessageId(), (Request<? extends Response>) request)))
                        .then();
            } else if (payload instanceof Container) {
                Container container = (Container) payload;
                return Flux.fromArray(container.getMessages())
                        .concatMap(internal -> processProtoMessage(session, internal))
                        .then(Mono.defer(() -> {
                            long[] acks = Stream.of(container.getMessages())
                                    .mapToLong(ProtoMessage::getMessageId)
                                    .toArray();
                            return session.sendRpcMessage(Mono.just(new MessageAck(acks)));
                        }));
            } else if (payload instanceof MessageAck) {
                MessageAck messageAck = (MessageAck) payload;
                Stream<Long> acks = Arrays.stream(messageAck.getMessagesIds()).boxed();
                return Flux.fromStream(acks)
                        .transform(session::acknowledge)
                        .then();
            } else if (payload instanceof RequestResend) {
                RequestResend requestResend = (RequestResend) payload;
                return Mono.fromCallable(requestResend::getMessageId).transform(session::resend);
            }
            return Mono.error(new MTProtoException("Unknown message type#" + message.getMessageId()));
        });
    }

    private Mono<MTProtoSession> createSession(TransportConnection connection, long authKeyId, long sessionId) {
        return getAuthData(authKeyId, sessionId)
                .switchIfEmpty(onAuthKeyInvalid(connection, authKeyId, sessionId).then(Mono.empty()))
                .flatMap(authData -> {
                    DcMTProtoSession session = new DcMTProtoSession(authKeyId, sessionId, authData.getMasterKey());
                    session.setPrincipal(authData.getUserId());
                    session.initializeNativeSession(connection);
                    return sessionRegistry.register(session);
                });
    }

    protected Mono<AuthData> getAuthData(final long authKeyId, final long sessionId) {
        return authService.getByAuthKeyId(authKeyId)
                .zipWhen(session -> authKeyService.getAuthKey(authKeyId))
                .map(TupleUtils.function((session, key) -> new AuthData(key.getPublicKeyHash(), session.getUserId())))
                .switchIfEmpty(Mono.defer(() -> authKeyService.getAuthKey(authKeyId)
                        .map(key -> new AuthData(key.getPublicKeyHash(), null))));
    }

    private Mono<Void> onAuthKeyInvalid(TransportConnection connection, long authId, long sessionId) {
        ProtoMessage protoMessage = new ProtoMessage(Long.MAX_VALUE, new AuthIdInvalid().toByteArray());
        return connection.send(Mono.just(new ProtoPackage(authId, sessionId, protoMessage)));
    }

    private Mono<Void> handleError(TransportConnection connection, Throwable cause) {
        if (cause instanceof AuthorizationException) {
            log.warn("Authorization exception caught", cause);
            AuthorizationException authorizationException = (AuthorizationException) cause;
            return connection.send(Mono.just(new Drop(0, 0, authorizationException.getMessage())));
        }
        log.warn("Unexpected error due processing session: {} package", connection.getId(), cause);
        return connection.send(Mono.just(new Drop(0, 0,"Internal server error")));
    }

    private static class AuthData {

        private final byte[] masterKey;
        private final Id userId;

        public AuthData(byte[] masterKey, Id userId) {
            this.masterKey = masterKey;
            this.userId = userId;
        }

        public byte[] getMasterKey() {
            return masterKey;
        }

        public Id getUserId() {
            return userId;
        }
    }
}
