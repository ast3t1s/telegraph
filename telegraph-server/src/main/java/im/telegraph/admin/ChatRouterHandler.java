package im.telegraph.admin;

import im.telegraph.admin.object.ChatBean;
import im.telegraph.admin.object.ChatParticipantBean;
import im.telegraph.admin.object.request.AddParticipantsRequest;
import im.telegraph.admin.route.AbstractRoute;
import im.telegraph.admin.route.Endpoint;
import im.telegraph.admin.route.Route;
import im.telegraph.admin.route.RouteUtils;
import im.telegraph.api.update.UpdateChatParticipantAdd;
import im.telegraph.common.OffsetPageRequest;
import im.telegraph.common.Page;
import im.telegraph.domain.entity.Id;
import im.telegraph.domain.entity.chat.Chat;
import im.telegraph.domain.entity.chat.ChatParticipant;
import im.telegraph.rpc.base.Update;
import im.telegraph.service.ChatService;
import im.telegraph.service.MessagingService;
import im.telegraph.service.UpdatesService;
import io.netty.handler.codec.http.HttpMethod;
import org.springframework.stereotype.Component;
import reactor.core.Disposable;
import reactor.core.publisher.Mono;
import reactor.netty.http.server.HttpServerRequest;
import reactor.netty.http.server.HttpServerResponse;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static io.netty.handler.codec.http.HttpResponseStatus.CREATED;
import static io.netty.handler.codec.http.HttpResponseStatus.OK;

/**
 * @author Ast3t1s
 */
@Component
public class ChatRouterHandler extends AbstractRoute {

    private final ChatService chatService;
    private final UpdatesService updatesService;
    private final MessagingService messagingService;

    public ChatRouterHandler(ChatService chatService, UpdatesService updatesService, MessagingService messagingService) {
        this.chatService = chatService;
        this.updatesService = updatesService;
        this.messagingService = messagingService;
    }

    @Override
    public List<Route> routes() {
        return List.of(
                Route.builder()
                        .endpoint(Endpoint.ofFixedPath(HttpMethod.POST, "/chat/create"))
                        .action(this::handleCreateChat)
                        .corsHeaders(),
                Route.builder()
                        .endpoint(Endpoint.ofFixedPath(HttpMethod.GET, "/chat"))
                        .action(this::handleGetChats)
                        .corsHeaders(),
                Route.builder()
                        .endpoint(Endpoint.ofFixedPath(HttpMethod.GET, "/chat/{chatId}/participants"))
                        .action(this::handleGetParticipants)
                        .corsHeaders(),
                Route.builder()
                        .endpoint(Endpoint.ofFixedPath(HttpMethod.POST, "/chat/{chatId}/participants"))
                        .action(this::handleAddChatParticipant)
                        .corsHeaders(),
                Route.builder()
                        .endpoint(Endpoint.ofFixedPath(HttpMethod.DELETE, "/chat/{chatId}/{userId}/{kickedBy}/participants"))
                        .action(this::handleDeleteChatParticipants)
                        .corsHeaders()
        );
    }

    protected Mono<Void> handleCreateChat(HttpServerRequest request, HttpServerResponse response) {
        return Mono.empty();
      /*  return readJson(request, CreateChatRequest.class)
                .flatMap(req -> {
                    final UserId userId = Optional.ofNullable(req.getOwnerId())
                            .map(UserId::of).orElse(null);
                    final PermissionSet permissions = Optional.ofNullable(req.getPermissions())
                            .map(PermissionSet::of).orElse(null);

                    return chatService.create(userId, req.getTitle(), req.getDescription(), permissions);
                })
                .zipWhen(chat -> {
                    final Peer peer = Peer.ofChat(chat.getId());
                    final long randomId = ThreadLocalRandom.current().nextLong();

                    return Mono.empty();

                   /* return messagingService.sendMessage(null, chat.getUserId(), peer, randomId,
                            ChatServiceMessage.CHAT_CREATED.getServiceMessage(null));*/
            /*    })
                .map(TupleUtils.function((chat, update) -> new UpdatesResponse(update.getSequence(), update.getTimestamp().toEpochMilli(),
                        toChatBean(chat))))
                .flatMap(chatResponse -> writeJson(response.status(CREATED), chatResponse));*/
    }

    public Mono<Void> handleGetChats(HttpServerRequest request, HttpServerResponse response) {
        return chatService.getChats(RouteUtils.createPageRequest(request.uri()))
                .defaultIfEmpty(new Page<>(Collections.emptyList()))
                .map(chatPage -> chatPage.stream().map(this::toChatBean))
                .flatMap(chats -> writeJson(response.status(OK), chats));
    }

    protected Mono<Void> handleAddChatParticipant(HttpServerRequest request, HttpServerResponse response) {
        return readJson(request, AddParticipantsRequest.class)
                .flatMap(req -> {
                    final Id chatId = Optional.ofNullable(request.param("chat_id"))
                            .map(Long::valueOf).map(Id::of).orElse(null);
                    final Id userId = Optional.ofNullable(req.getUserId())
                            .map(Id::of).orElse(null);
                    final Id inviterId = Optional.ofNullable(req.getInviterId())
                            .map(Id::of).orElse(null);

                    if (chatId == null || userId == null || inviterId == null) {
                        return response.sendNotFound();
                    }

                    return chatService.addChatUser(chatId, userId, inviterId)
                            .then(Mono.defer(() -> {
                                UpdateChatParticipantAdd update = new UpdateChatParticipantAdd();
                                update.setChatId(chatId.asLong());
                                update.setUid(userId.asLong());
                                update.setInviterUid(inviterId.asLong());
                                return relayUpdate(chatId, update);
                            }))
                            .then(response.status(CREATED).send());
                });
    }

    protected Mono<Void> handleGetParticipants(HttpServerRequest request, HttpServerResponse response) {
        return Mono.justOrEmpty(request.param("chatId"))
                .map(Integer::valueOf)
                .map(Id::of)
                .flatMap(chatId -> {
                    OffsetPageRequest pageRequest = RouteUtils.createPageRequest(request.uri());
                    return chatService.getChatParticipants(chatId, pageRequest.getPageSize(),
                            (int) pageRequest.getOffset());
                })
                .defaultIfEmpty(new Page<>(Collections.emptyList()))
                .map(participants -> participants.stream().map(this::toParticipantBean))
                .flatMap(participants -> writeJson(response.status(OK), participants));
    }

    protected Mono<Void> handleDeleteChatParticipants(HttpServerRequest request, HttpServerResponse response) {
        final Id userId = Optional.ofNullable(request.param("userId"))
                .map(Integer::parseInt).map(Id::of).orElse(null);
        final Id chatId = Optional.ofNullable(request.param("chatId"))
                .map(Integer::valueOf).map(Id::of).orElse(null);
        final Id kickedBy = Optional.ofNullable(request.param("kickedBy"))
                .map(Integer::valueOf).map(Id::of).orElse(null);

        if (userId == null || chatId == null || kickedBy == null) {
            return response.sendNotFound();
        }

        /*return chatService.kick(chatId, userId, kickedBy)
                .then(relayUpdate(chatId, new UpdateChatParticipantDelete(chatId.getValue(), userId.getValue())))
                .then(response.status(NO_CONTENT).send());*/
        return Mono.empty();
    }

    protected Mono<Void> relayUpdate(Id chatId, Update update) {
        return Mono.create(sink -> sink.onRequest(__ -> {
            Disposable membersChunkTask = chatService.getChatParticipants(chatId)
                    .map(ChatParticipant::getUserId)
                    .collect(Collectors.toSet())
                    .flatMap(memberIds -> updatesService.forwardSeqUpdate(memberIds, update))
                    .subscribe();
            sink.onCancel(membersChunkTask);
            sink.success();
        }));
    }

    private ChatBean toChatBean(Chat chat) {
        return new ChatBean(chat.getId().asLong(),
                chat.getTitle(),
                chat.getDescription().orElse(null),
                chat.getOwnerUserId().asLong(),
                chat.getPermissions(),
                chat.getMembersCount(),
                chat.getCreatedAt().toEpochMilli());
    }

    private ChatParticipantBean toParticipantBean(ChatParticipant chatMember) {
        return new ChatParticipantBean(chatMember.getChatId().asLong(), chatMember.getInviterId().asLong(), chatMember.getStatus().getValue());
    }
}
