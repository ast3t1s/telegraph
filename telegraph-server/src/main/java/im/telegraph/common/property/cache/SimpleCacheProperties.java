package im.telegraph.common.property.cache;

import java.time.Duration;

/**
 * @author Ast3t1s
 */
public class SimpleCacheProperties {

    /**
     * This cache size specifies the maximum number of entries the cache may contain.
     * Note that the cache may evict an entry before this limit is exceeded or temporarily
     * exceed the threshold while evicting. As the cache size grows close to the maximum,
     * the cache evicts entries that are less likely to be used again. For example, the
     * cache may evict an entry because it hasn't been used recently or very often.
     * Note: to disable the cache, you may choose a cache size of {@code 0}.
     */
    private long cacheSize = 10_000L;

    /**
     * This cache capacity sets the minimum total size for the internal data structures.
     * Providing a large enough estimate at construction time avoids the need for expensive resizing
     * operations later, but setting this value unnecessarily high wastes memory.
     */
    private int initialCapacity = 1_000;

    /**
     * The time of a cached item to keep in cache. Once it’s expired, the corresponding item is erased from cache.
     */
    private Duration cacheLifeTime = Duration.ofSeconds(30);

    /**
     * Whether or not to cache missed lookups. When there is an attempt to lookup for a value in a database
     * and this value is not found and the option is set to true, this attempt will be cached and no attempts
     * will be performed until the cache expires (see cache_life_time). Usually you don’t want to change it.
     */
    private boolean cachedMissed = true;

    public long getCacheSize() {
        return cacheSize;
    }

    public void setCacheSize(long cacheSize) {
        this.cacheSize = cacheSize;
    }

    public int getInitialCapacity() {
        return initialCapacity;
    }

    public void setInitialCapacity(int initialCapacity) {
        this.initialCapacity = initialCapacity;
    }

    public Duration getCacheLifeTime() {
        return cacheLifeTime;
    }

    public void setCacheLifeTime(Duration cacheLifeTime) {
        this.cacheLifeTime = cacheLifeTime;
    }

    public boolean isCachedMissed() {
        return cachedMissed;
    }

    public void setCachedMissed(boolean cachedMissed) {
        this.cachedMissed = cachedMissed;
    }
}
