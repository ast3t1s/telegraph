package im.telegraph.generator.generator.javapoet;

import com.squareup.javapoet.*;
import im.telegraph.generator.generator.Constants;
import im.telegraph.generator.generator.SchemeContainerGenerator;
import im.telegraph.generator.generator.SchemeGenerator;
import im.telegraph.generator.type.SchemeDefinition;
import im.telegraph.generator.type.SchemeStruct;
import im.telegraph.generator.type.SchemeTrait;

import javax.lang.model.element.Modifier;
import java.io.File;
import java.io.IOException;

public class StructGenerator implements SchemeGenerator {

    private final SchemeContainerGenerator<TypeSpec.Builder> containerGenerator = new JavaPoetContainerGenerator();

    @Override
    public void generate(SchemeDefinition definition, String out) throws IOException {
        long start = System.currentTimeMillis();

        for (SchemeTrait u : definition.getAllTraits()) {
            String javaName = TypeHelper.getClassStructName(u.getName());
            TypeSpec.Builder clazz = TypeSpec.classBuilder(javaName)
                    .addJavadoc(Constants.NOTICE)
                    .addJavadoc(Constants.DEFAULT_AUTHOR)
                    .addModifiers(Modifier.PUBLIC, Modifier.ABSTRACT)
                    .superclass(Constants.TYPE_STREAM_OBJ);

            MethodSpec.Builder parseMethod = MethodSpec.methodBuilder("fromBytes");

            if (u.isContainer()) {
                parseMethod.addParameter(byte[].class, "data")
                        .returns(ClassName.get("", javaName))
                        .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                        .addException(IOException.class)
                        .addStatement("$T reader = new $T(new $T(data))", Constants.TYPE_BINARY_READER,
                                Constants.TYPE_BINARY_READER, Constants.TYPE_IN_STREAM)
                        .addStatement("int key = reader.readInt(1)")
                        .addStatement("byte[] content = reader.readByteArray(2)");
            } else {
                parseMethod.returns(ClassName.get("", javaName))
                        .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                        .addException(IOException.class)
                        .addParameter(int.class, "key")
                        .addParameter(byte[].class, "source");
            }
            parseMethod.beginControlFlow("switch (key)");
            for (SchemeStruct struct : definition.getTraitedStructs(u.getName())) {
                parseMethod.addStatement("case $L: return $T.fromByteArray(new $T(), content)",
                        struct.getTraitRef().getKey(), Constants.TYPE_CONTEXT, TypeHelper.getStructName(struct.getName()));
            }
            parseMethod.addStatement("default: return new $TUnsupported(key, content)", TypeHelper.getStructName(u.getName()));
            parseMethod.endControlFlow();

            clazz.addMethod(MethodSpec.methodBuilder(Constants.CLASS_ID_METHOD)
                    .addModifiers(Modifier.PUBLIC, Modifier.ABSTRACT)
                    .returns(int.class)
                    .build());

            clazz.addMethod(MethodSpec.methodBuilder("buildContainer")
                    .addModifiers(Modifier.PUBLIC)
                    .returns(byte[].class)
                    .addException(IOException.class)
                    .addStatement("$T res = new $T()", Constants.TYPE_OUT_STREAM, Constants.TYPE_OUT_STREAM)
                    .addStatement("$T writer = new $T(res)", Constants.TYPE_BINARY_WRITER, Constants.TYPE_BINARY_WRITER)
                    .addStatement("writer.writeInt(1, getHeader())")
                    .addStatement("writer.writeBytes(2, toByteArray())")
                    .addStatement("return res.toByteArray()")
                    .build());

            clazz.addMethod(parseMethod.build());
            JavaFile.builder(Constants.PACKAGE_API, clazz.build())
                    .indent("    ")
                    .build().writeTo(new File(out));

            clazz = TypeSpec.classBuilder(javaName + "Unsupported")
                    .addJavadoc(Constants.DEFAULT_AUTHOR)
                    .addModifiers(Modifier.PUBLIC)
                    .superclass(ClassName.get("", javaName))
                    .addField(FieldSpec.builder(int.class, "key")
                            .addModifiers(Modifier.PROTECTED)
                            .build())
                    .addField(FieldSpec.builder(byte[].class, "content")
                            .addModifiers(Modifier.PROTECTED)
                            .build())
                    .addMethod(MethodSpec.constructorBuilder()
                            .addModifiers(Modifier.PUBLIC)
                            .addParameter(int.class, "key")
                            .addParameter(byte[].class, "content")
                            .addStatement("this.$N = $N", "key", "key")
                            .addStatement("this.$N = $N", "content", "content")
                            .build())
                    .addMethod(MethodSpec.methodBuilder(Constants.CLASS_ID_METHOD)
                            .addModifiers(Modifier.PUBLIC)
                            .addAnnotation(Override.class)
                            .returns(int.class)
                            .addStatement("return this.key")
                            .build())
                    .addMethod(MethodSpec.methodBuilder("deserialize")
                            .addModifiers(Modifier.PUBLIC)
                            .addAnnotation(Override.class)
                            .addException(IOException.class)
                            .addParameter(Constants.TYPE_READER, "reader")
                            .addStatement("throw new IOException(\"Parsing is unsupported\")")
                            .build())
                    .addMethod(MethodSpec.methodBuilder("serialize")
                            .addModifiers(Modifier.PUBLIC)
                            .addAnnotation(Override.class)
                            .addException(IOException.class)
                            .addParameter(Constants.TYPE_WRITER, "writer")
                            .addStatement("writer.writeRaw(content)")
                            .build());

            JavaFile.builder(Constants.PACKAGE_API, clazz.build())
                    .indent("    ")
                    .build().writeTo(new File(out));
        }

        for (SchemeStruct u : definition.getAllStructs()) {
            String javaName = TypeHelper.getClassStructName(u.getName());
            TypeSpec.Builder clazz = TypeSpec.classBuilder(javaName)
                    .addJavadoc(Constants.NOTICE)
                    .addJavadoc(Constants.DEFAULT_AUTHOR)
                    .addModifiers(Modifier.PUBLIC);
            if (u.getTraitRef() != null) {
                clazz.superclass(TypeHelper.getStructName(u.getTraitRef().getTrait()));
            } else {
                clazz.superclass(Constants.TYPE_STREAM_OBJ);
            }

            containerGenerator.generateFields(clazz, definition, u);

            //Default constructor
            clazz.addMethod(MethodSpec.constructorBuilder()
                    .addModifiers(Modifier.PUBLIC)
                    .build());
            if (u.getAttributes().size() > 0) {
                containerGenerator.generateConstructor(clazz, definition, u);
            }

            if (u.getTraitRef() != null) {
                clazz.addMethod(MethodSpec.methodBuilder(Constants.CLASS_ID_METHOD)
                        .addModifiers(Modifier.PUBLIC)
                        .returns(int.class)
                        .addStatement("return $L", u.getTraitRef().getKey())
                        .build());
            }

            containerGenerator.generateGettersAndSetters(clazz, definition, u);
            containerGenerator.generateDeserialization(clazz, definition, u);
            containerGenerator.generateSerialization(clazz, definition, u);
            containerGenerator.generateToString(clazz, definition, u);

            JavaFile.builder(Constants.PACKAGE_API, clazz.build())
                    .addStaticImport(Constants.TYPE_UTILS, "*")
                    .indent("    ")
                    .build().writeTo(new File(out));
        }

        System.out.println("Total structs " + definition.getAllStructs().size()
                + " generated in " + (System.currentTimeMillis() - start) + " ms");
    }
}
