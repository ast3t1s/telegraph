package im.telegraph.protocol.model.mtproto;

import im.telegraph.engine.serialization.DataInputStream;
import im.telegraph.engine.serialization.DataOutputStream;

import java.io.IOException;

public abstract class ProtoStruct extends ProtoObject {

	protected ProtoStruct(DataInputStream inputStream) throws IOException {
		super(inputStream);
	}

	protected ProtoStruct() {

	}

	protected abstract byte getHeader();

	protected abstract void writeBody(DataOutputStream outputStream) throws IOException;

	protected abstract void readBody(DataInputStream inputStream) throws IOException;

	@Override
	public final void writeObject(DataOutputStream outputStream) throws IOException {
		byte header = getHeader();
		if (header != 0) {
			outputStream.writeByte(header);
		}
		writeBody(outputStream);
	}

	@Override
	public final ProtoObject readObject(DataInputStream inputStream) throws IOException {
		readBody(inputStream);
		return this;
	}

}
