package im.telegraph.generator.generator;

import im.telegraph.generator.type.SchemeDefinition;

import java.io.IOException;

@FunctionalInterface
public interface SchemeGenerator {

    void generate(SchemeDefinition definition, String out) throws IOException;

}
