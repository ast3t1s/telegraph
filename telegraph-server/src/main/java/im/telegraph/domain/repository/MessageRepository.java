package im.telegraph.domain.repository;

import im.telegraph.domain.entity.Id;
import im.telegraph.domain.entity.Message;
import im.telegraph.domain.entity.Peer;
import org.reactivestreams.Publisher;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Instant;
import java.util.Collection;
import java.util.List;

/**
 * @author Ast3t1s
 */
public interface MessageRepository {

    /**
     * Create a history message
     *
     * @param message the message to save
     * @return A {@link Mono} where, upon successful completion, emits the {@link Message}.If an error is received,
     * it is emitted through the {@code Mono}.
     */
    Mono<Message> insert(Message message);

    Mono<Void> save(Publisher<Message> messages);

    Flux<Message> findAllByIdInAndDeletedAtIsNull(Collection<Id> ids);

    /**
     * Find all history messages for a given dialog ID after the given date.
     *
     * @param date the date offset
     * @param limit the limit of the messages
     * @return A {@link Flux} where, upon successful completion, emits the {@link Message}.If an error is received,
     * it is emitted through the {@code Flux}.
     */
    Flux<Message> findAllByUserIdAndPeerDateLessThanAndDeletedAtIsNull(Id userId, Peer peer, Instant date, int limit);

    /**
     * Find all history messages for a given dialog ID and random id's
     *
     * @param ids the list of ids
     * @return A {@link Flux} where, upon successful completion, emits the {@link Message}.If an error is received,
     * it is emitted through the {@code Flux}.
     */
    Flux<Message> findAllByUserIdAndPeerAndIdInAndDeletedAtIsNull(Id userId, Peer peer, List<Id> ids);

    /**
     * Find all history messages for a given {@link Id} and {@link Peer} ordered by {@link Message#getCreatedAt()}
     *
     * @param limit the limit of fetched messages
     * @param offset the offset
     * @return A {@link Flux} where, upon successful completion, emits the {@link Message}.If an error is received,
     * it is emitted through the {@code Flux}.
     */
    Flux<Message> findAllByUserIdAndPeerAndDeletedAtIsNullOrderByCreatedAtDesc(Id userId, Peer peer, int limit,
                                                                               int offset);
    /**
     * Delete history messages by the given ids
     *
     * @param ids the ids of the history messages
     * @return A {@link Mono} where, upon successful completion, emits the count of deleted history messages.If an error is received,
     * it is emitted through the {@code Mono}.
     */
    Mono<Long> deleteByIdIn(Collection<Id> ids);
}
