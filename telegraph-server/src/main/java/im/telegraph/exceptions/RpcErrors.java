package im.telegraph.exceptions;

/**
 * This class contains all the client-server errors that must be sent from server to the client. These are
 * thus part of MTProto protocol. The names can be changes bu the error tag cannot.
 *
 *  @author Ast3t1s
 */
public enum RpcErrors {

    /** Common errors*/
    REQUEST_NOT_SUPPORTED(400, "REQUEST_NOT_SUPPORTED", "Request is not supported."),
    INVALID_ACCESS_HASH(403, "INVALID_ACCESS_HASH", "The provided contact access hash is invalid."),
    UNAUTHORIZED(401, "USER_NOT_AUTHORIZED", "You are not authorized."),
    USER_IS_BLOCKED(400, "USER_IS_BLOCKED", "User is blocked."),
    YOU_BLOCKED_USER(400, "YOU_BLOCKED_USER", "You were blocked by user."),
    USER_DEACTIVATED(400, "INPUT_USER_DEACTIVATED", "The specified user was deleted."),
    USER_ID_INVALID(400, "USER_ID_INVALID", "The provided user ID is invalid."),
    INVALID_PEER(400, "INVALID_PEER", "The provided peer is invalid."),

    /** Auth errors*/
    AUTH_KEY_BOUND(400, "AUTH_KEY_ALREADY_BOUND", "The passed authorization key is already bound."),
    CANT_TERMINATE_SESSION(400, "CANT_TERMINATE_SESSION", "The provided session can't be terminated."),
    AUTH_KEY_NOT_FOUND(404, "AUTH_KEY_NOT_FOUND", "The provided authorization key is not found."),
    FAILED_TERMINATES_SESSIONS(400, "FAILED_TERMINATE_SESSIONS", "None of the sessions were terminated"),

    /** User errors */
    USER_NOT_FOUND(404, "USER_NOT_FOUND", "User with the given ID not found."),
    USERNAME_OCCUPIED(400, "USERNAME_OCCUPIED", "The provided username is already occupied."),
    EMAIL_OCCUPIED(400, "EMAIL_OCCUPIED", "The provided email is already occupied."),
    USERNAME_INVALID(400, "USERNAME_INVALID", "The provided username is invalid."),
    EMAIL_INVALID(400,  "EMAIL_INVALID", "The provided email is invalid."),
    PASSWORD_INVALID(400, "PASSWORD_INVALID", "The provided password is invalid."),
    ABOUT_TOO_LONG(400, "USER_ABOUT_TOO_LONG", "The provided user about is invalid."),
    EMAIL_NOT_VERIFIED(400, "EMAIL_NOT_VERIFIED", "The provided email is not verified."),

    /** Contact errors */
    CONTACT_NOT_FOUND(404, "CONTACT_NOT_FOUND", "The provided user is not in contact list"),
    CONTACT_REQUEST_NOT_FOUND(404, "CONTACT_REQUEST_NOT_FOUND", "You have no contact requests from the given user"),
    CONTACT_EXISTS(400, "CONTACT_ALREADY_EXISTS", "User already in contact list"),

    /** Privacy errors */
    ALREADY_BLOCKED(400, "USER_ALREADY_BLOCKED", "The provided user is already blocked."),
    NOT_BLOCKED(400, "USER_NOT_BLOCKED", "The provided user is not blocked."),

    /** Chat and participants errors*/
    CHAT_NOT_FOUND(404, "CHAT_NOT_FOUND", "rpc.error.message.group.notfound"),
    CHAT_INVALID(400, "CHAT_INVALID", "The provided chat id is invalid"),
    CHAT_WRITE_FORBIDDEN(403, "CHAT_WRITE_FORBIDDEN", "You can't write in the chat"),
    USERS_TOO_FEW(400, "USERS_TOO_FEW", "Not enough users"),
    USERS_TOO_MUCH(400, "USERS_TOO_MUCH", "The maximum number of users has been exceeded"),
    ALREADY_INVITED(400, "USER_ALREADY_INVITED", "User already invited to the chat"),
    ALREADY_PARTICIPANT(400, "USER_ALREADY_PARTICIPANT", "User already a member of this chat"),
    USER_NOT_PARTICIPANT(400, "USER_NOT_PARTICIPANT", "You are not a member of this chat"),
    CHAT_TITLE_EMPTY(400, "CHAT_TITLE_EMPTY", "No chat title provided"),
    CHAT_TITLE_INVALID(400, "CHAT_TITLE_INVALID", "The provided chat title invalid"),
    CHAT_ABOUT_TOO_LONG(400, "CHAT_ABOUT_TOO_LONG", "The provided about is too long"),
    USER_ALREADY_ADMIN(400, "USER_ALREADY_ADMIN", "User is already admin of this chat"),
    USER_NOT_ADMIN(400, "USER_NOT_ADMIN", "You have no admin permissions of this chat"),
    CHAT_RESTRICTED(400, "CHAT_RESTRICTED", "You can't send messages in this chat, you were restricted"),
    NO_PERMISSION(400, "CHAT_PERMISSION_REQUIRED", "You have no permissions in this chat to do this"),

    /** Message errors*/
    MESSAGE_EMPTY(400, "MESSAGE_EMPTY", "The provided message is empty"),
    INVALID_MSG_ID(400, "INVALID_MSG_ID", "Invalid message ID"),
    MESSAGE_FORBIDDEN(400, "MESSAGE_DELETE_FORBIDDEN", "You can't delete one of the messages you tried to delete," +
            "most likely because it is a service message"),
    RANDOM_ID_DUPLICATE(500, "RANDOM_ID_DUPLICATE", "You provided a random ID that was already used"),

    /** Image errors*/
    IMAGE_PROCESS_FAILED(400, "IMAGE_PROCESS_FAILED", "Failure while processing image."),
    IMAGE_CROP_SIZE_SMALL(400, "IMAGE_CROP_SIZE_SMALL", "Image is too small."),

    /** File storage errors*/
    FILE_URL_GENERATE_FAILED(500, "FILE_URL_GENERATE_FAILED", "Failure while generating the file url"),
    FILE_NOT_FOUND(404, "FILE_NOT_FOUND", "File with the given ID not found"),
    FILE_TOO_LARGE(400, "FILE_TOO_LARGE", "File size is too big"),
    FILE_LOCATION_INVALID(400, "LOCATION_INVALID", "The provided location is invalid"),
    FILE_PARTS_INVALID(400, "FILE_PARTS_INVALID", "Invalid number of parts"),
    FILE_PART_INVALID(400, "FILE_PART_INVALID", "The file part number is invalid"),
    FILE_PART_TOO_BIG(400, "FILE_PART_TOO_BIG", "The size limit (512 KB) for the content of the file part has been exceeded");

    public static final RpcErrors[] VALUES;

    static {
        VALUES = values();
    }

    private final int errorCode;
    private final String errorTag;
    private final String errorMsg;

    RpcErrors(int errorCode, String errorTag, String errorMsg) {
        this.errorCode = errorCode;
        this.errorTag = errorTag;
        this.errorMsg = errorMsg;
    }

    /**
     * The error code for rpc exception.
     */
    public int getErrorCode() {
        return errorCode;
    }

    /**
     * Get a rpc error tag.
     *
     * @return rpc error tag.
     */
    public String getErrorTag() {
        return errorTag;
    }

    /**
     * Get a friendly description of the rpc error.
     *
     * @return error description.
     */
    public String getErrorMsg() {
        return errorMsg;
    }
}
