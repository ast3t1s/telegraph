package im.telegraph.admin.object;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Ast3t1s
 */
public class ContactBean {

    @JsonProperty("user_id")
    private final String userId;
    @JsonProperty("contact_id")
    private final String contactId;
    private final int status;
    private final long timestamp;

    public ContactBean(String userId, String contactId, int status, long timestamp) {
        this.userId = userId;
        this.contactId = contactId;
        this.status = status;
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "ContactBean{" +
                "userId=" + userId +
                ", contactId=" + contactId +
                ", status=" + status +
                ", timestamp=" + timestamp +
                '}';
    }
}
