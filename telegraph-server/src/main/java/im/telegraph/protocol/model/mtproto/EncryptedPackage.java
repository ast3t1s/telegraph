package im.telegraph.protocol.model.mtproto;

import im.telegraph.engine.serialization.DataInputStream;
import im.telegraph.engine.serialization.DataOutputStream;

import java.io.IOException;

public class EncryptedPackage extends ProtoObject {

	private long seqNumber;
	private byte[] encryptedPackage;

	public EncryptedPackage(DataInputStream inputStream) throws IOException {
		super(inputStream);
	}

	public EncryptedPackage(long seqNumber, byte[] encryptedPackage) {
		this.seqNumber = seqNumber;
		this.encryptedPackage = encryptedPackage;
	}

	public long getSeqNumber() {
		return seqNumber;
	}

	public byte[] getEncryptedPackage() {
		return encryptedPackage;
	}

	@Override
	public void writeObject(DataOutputStream outputStream) throws IOException {
		outputStream.writeInt64(seqNumber);
		outputStream.writeProtoBytes(encryptedPackage);
	}

	@Override
	public ProtoObject readObject(DataInputStream inputStream) throws IOException {
		seqNumber = inputStream.readInt64();
		encryptedPackage = inputStream.readProtoBytes();
		return this;
	}

}
