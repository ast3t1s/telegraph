package im.telegraph.handlers;

import im.telegraph.api.ApiAbsUser;
import im.telegraph.api.ApiConfig;
import im.telegraph.api.ApiUserEmpty;
import im.telegraph.api.rpc.*;
import im.telegraph.domain.entity.Id;
import im.telegraph.domain.entity.User;
import im.telegraph.handlers.dispatch.DispatchContext;
import im.telegraph.handlers.internal.ApiEntityMapper;
import im.telegraph.handlers.mapper.UserConverter;
import im.telegraph.protocol.SessionId;
import im.telegraph.service.*;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.function.TupleUtils;

import java.util.Optional;

@Component
public class AuthDispatchHandlers {

    private final UserService userService;
    private final MailSenderService mailSenderService;
    private final AuthService authService;
    private final AuthKeyService authKeyService;
    private final SessionService sessionService;

    private final UserConverter userConverter;

    public AuthDispatchHandlers(AuthService authService,
                                UserService userService,
                                MailSenderService mailSenderService,
                                AuthKeyService authKeyService,
                                SessionService sessionService,
                                UserConverter userConverter) {
        this.authService = authService;
        this.userService = userService;
        this.mailSenderService = mailSenderService;
        this.authKeyService = authKeyService;
        this.sessionService = sessionService;
        this.userConverter = userConverter;
    }

    public Mono<ResponseAuth> handleSignIn(DispatchContext<RequestAuthSignIn> context) {
        return userService.authenticate(context.getRequest().getUsername(), context.getRequest().getPassword())
                .zipWhen(user -> authService.create(context.getAuthId(),
                        context.getRequest().getDeviceTitle(),
                        context.getAddress().orElse(null), user.getId()))
                .flatMap(TupleUtils.function((user, authorization) -> {
                    ApiAbsUser apiAbsUser = Optional.ofNullable(userConverter.convert(context.getAuthId(), user))
                            .orElse(new ApiUserEmpty(authorization.getUserId().asLong()));
                    ApiConfig apiConfig = new ApiConfig();

                    return sessionService.bindSession(user.getId(), SessionId.create(context.getAuthId(), context.getSessionId()))
                            .thenReturn(new ResponseAuth(apiAbsUser, apiConfig));
                }));
    }

    public Mono<ResponseAuth> handleSignUp(DispatchContext<RequestAuthSignUp> context) {
        final String login = context.getRequest().getLogin();
        final String email = context.getRequest().getEmail();
        final String password = context.getRequest().getPassword();
        final String displayName = context.getRequest().getName();
        final String langCode = context.getRequest().getLangCode();

        return userService.create(login, email, password, displayName, langCode, User.Role.ROLE_USER)
                .zipWhen(user -> authService.create(context.getAuthId(), context.getRequest().getDeviceTitle(),
                        context.getAddress().orElse(null), user.getId()))
                .flatMap(TupleUtils.function((user, authorization) -> {
                    ApiAbsUser apiAbsUser = Optional.ofNullable(userConverter.convert(context.getAuthId(), user))
                            .orElse(new ApiUserEmpty(user.getId().asLong()));
                    ApiConfig apiConfig = new ApiConfig();

                    return sessionService.bindSession(user.getId(), SessionId.create(context.getAuthId(), context.getSessionId()))
                            .thenReturn(new ResponseAuth(apiAbsUser, apiConfig));
                    //  .then(Mono.fromRunnable(() -> mailSenderService.sendCreationEmail(user)));
                }));
    }

    public Mono<ResponseBool> handleSignOut(DispatchContext<RequestAuthSignOut> context) {
        return Mono.justOrEmpty(context.getUserId().map(Id::of))
                .flatMap(selfId -> authService.delete(selfId, Id.of(context.getAuthId()))
                        .flatMap(deleted -> deleted ? authKeyService.delete(context.getAuthId()) : Mono.just(false)))
                .map(ResponseBool::new);
    }

    public Mono<ResponseAuthGetAuthSessions> handleGetAuthSessions(DispatchContext<RequestAuthGetAuthSessions> context) {
        return Mono.justOrEmpty(context.getUserId().map(Id::of))
                .flatMap(selfIf -> authService.getUserAuthorizations(selfIf)
                        .map(authorization -> Optional.ofNullable(ApiEntityMapper.convertToApiSession(authorization)))
                        .filter(Optional::isPresent)
                        .map(Optional::get)
                        .collectList())
                .map(ResponseAuthGetAuthSessions::new);
    }

    public Mono<ResponseBool> handleTerminateSession(DispatchContext<RequestAuthTerminateSession> context) {
        return Mono.justOrEmpty(context.getUserId().map(Id::of))
                .flatMap(selfId -> authService.delete(selfId, Id.of(context.getRequest().getHash())))
                .flatMap(deleted -> deleted ? authKeyService.delete(context.getAuthId()) : Mono.just(false))
                .map(ResponseBool::new);
    }

    public Mono<ResponseBool> handleTerminateSessions(DispatchContext<RequestAuthTerminateSessions> context) {
        return Mono.justOrEmpty(context.getUserId().map(Id::of))
                .flatMapMany(selfId -> authService.deleteSessions(selfId, context.getAuthId()))
                .collectList()
                .map(deleted -> deleted.size() > 0)
                .map(ResponseBool::new);
    }
}
