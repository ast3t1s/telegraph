package im.telegraph.domain.repository;

import im.telegraph.domain.entity.Id;
import im.telegraph.domain.entity.chat.Chat;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * @author Ast3t1s
 */
public interface ChatRepository {

    /**
     * Inserts a new chat into the repository.
     *
     * @param chat the chat entity to insert
     * @return A {@link Mono} that emits the inserted {@link Chat} instance. If an error occurs during insertion,
     * it is emitted through the {@code Mono}.
     */
    Mono<Chat> insert(Chat chat);

    /**
     * Updates an existing chat in the repository.
     *
     * @param chat the chat entity to update
     * @return A {@link Mono} that emits the updated {@link Chat} instance. If the chat does not exist or an error occurs
     * during the update, the {@code Mono} emits an error.
     */
    Mono<Chat> update(Chat chat);

    /**
     * Finds a chat by its ID.
     *
     * @param chatId the ID of the chat to find
     * @return A {@link Mono} that emits the {@link Chat} instance if found, or an empty {@code Mono} if the chat does not exist.
     * If an error occurs during the search, it is emitted through the {@code Mono}.
     */
    Mono<Chat> findById(Id chatId);

    /**
     * Find all chats by the given IDs.
     *
     * @param ids the IDs of the chats
     * @return A {@link Flux} that emits the {@link Chat} instances for the given {@link Id}. If an error occurs,
     * it is emitted through the {@code Flux}.
     */
    Flux<Chat> findAllById(Iterable<Id> ids);

    /**
     * Finds all chats by the given user ID.
     *
     * @param userId the ID of the user
     * @return A {@link Flux} that emits the {@link Chat} instances for the given {@link Id}. If an error occurs,
     * it is emitted through the {@code Flux}.
     */
    Flux<Chat> findAllByUserId(Id userId);

    /**
     * Finds all chats with pagination.
     *
     * @return A {@link Flux} that emits the {@link Chat} instances. If an error occurs,
     * it is emitted through the {@code Flux}.
     */
    Flux<Chat> findAll(int limit, int offset);

    /**
     * Deletes a chat by its ID.
     *
     * @param chatId the ID of the chat to delete
     * @return A {@link Mono} that completes when the chat is successfully deleted. If the chat does not exist or an error
     * occurs during deletion, the {@code Mono} emits an error.
     */
    Mono<Void> deleteById(Id chatId);

    /**
     * Counts the total number of chats in the repository.
     *
     * @return A {@link Mono} that emits the total number of chats as a {@link Long}. If an error occurs during the count,
     * it is emitted through the {@code Mono}.
     */
    Mono<Long> count();
}
