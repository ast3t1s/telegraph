package im.telegraph.admin.object;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Ast3t1s
 */
public class MessageBean {

    private final Long id;
    @JsonProperty("sender_id")
    private final String senderId;
    @JsonProperty("random_id")
    private final Long randomId;
    private final int type;
    private final String body;

    public MessageBean(Long id, String senderId, Long randomId, int type, String body) {
        this.id = id;
        this.senderId = senderId;
        this.randomId = randomId;
        this.type = type;
        this.body = body;
    }

    @Override
    public String toString() {
        return "MessageBean{" +
                "id=" + id +
                ", senderId=" + senderId +
                ", randomId=" + randomId +
                ", type=" + type +
                ", body='" + body + '\'' +
                '}';
    }
}
