package im.telegraph.modules.update;

import im.telegraph.TelegraphApplication;
import im.telegraph.api.update.UpdateUserName;
import im.telegraph.common.rdbms.Handle;
import im.telegraph.common.rdbms.RdbmsOperations;
import im.telegraph.domain.entity.AuthKey;
import im.telegraph.domain.entity.Contact;
import im.telegraph.domain.entity.Id;
import im.telegraph.domain.entity.SequenceUpdate;
import im.telegraph.domain.entity.chat.ChatParticipant;
import im.telegraph.domain.repository.*;
import im.telegraph.handlers.internal.CombinedDifference;
import im.telegraph.handlers.internal.GetDiffCombiner;
import im.telegraph.handlers.internal.SerialMapping;
import im.telegraph.handlers.internal.SerialUpdate;
import im.telegraph.rpc.base.Update;
import im.telegraph.service.PrivacyService;
import im.telegraph.service.UpdatesService;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.unit.DataSize;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuples;

import java.io.IOException;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = TelegraphApplication.class)
class UpdatesServiceTest {

    @Autowired
    private UpdatesService updateService;

    @Autowired
    private PrivacyService privacyService;

    @Autowired
    private UpdatesRepository updatesRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PrivacyRepository privacyRepository;

    @Value(value = "${server.config.sequence.max-difference-size}")
    private DataSize maxDiffSize;

    @Autowired
    private AuthKeyRepository authKeyRepository;

    @Autowired
    private RdbmsOperations r2dbcOperations;

    @Autowired
    private ContactsRepository contactsRepository;

    @Autowired
    private ChatParticipantRepository chatMemberRepository;

    @Test
    public void testBulkInsert() {
        List<Object[]> tuples = new ArrayList<>();

        for (int i = 200; i < 250; i++) {
            tuples.add(new Object[]{i, 1, 1515, Instant.now(), Instant.now()});
        }

        r2dbcOperations.inTransaction(handle -> handle
                .createUpdate("INSERT INTO history_inbox (peer_id, peer_type, user_id, owner_last_received_at, " +
                        "owner_last_read_at) " +
                        "VALUES :tuples")
                .bind("tuples", tuples)
                .executeAndReturnGeneratedKeys("*")
                .mapRow(row -> row.get("peer_id", Long.class))
                .doOnNext(System.out::println))
                .blockLast();
    }

    @Test
    public void testCreateMember() {
        chatMemberRepository.insert(ChatParticipant.builder()
                .chatId(Id.of(1))
                .userId(Id.of(1))
                .inviterId(Id.of(2))
                .status(ChatParticipant.Status.PARTICIPANT)
                .createdAt(Instant.now())
               // .joinedAt(LocalDateTime.now())
                .build())
                .doOnNext(System.out::println)
                .block();
    }

    @Test
    public void testUser() {
       /* userRepository.findAllById(List.of(UserId.of(1), UserId.of(2), UserId.of(3)))
                .doOnNext(System.out::println)
                .blockLast();*/

        userRepository.findAllBy(10, 0)
                .doOnNext(System.out::println)
                .blockLast();
    }

    @Test
    public void testContactRepo() {
        Contact contact = contactsRepository.insert(new Contact(Id.of(8), Id.of(9), Contact.Status.BOTH,
                Instant.now())).block();
        System.out.println(contact);
    }

    @Test
    public void testPrivacyRepo() {

    }

    @Test
    void testR2dbc() {
        r2dbcOperations.inTransaction(new Function<Handle, Publisher<?>>() {
            @Override
            public Publisher<?> apply(Handle handle) {
                Mono<Void> test1 = handle.createQuery("select * from auth_keys where user_id = :userId")
                        .bind("userId", 1)
                        .mapRow(row -> row.get("auth_id", Long.class))
                        .doOnNext(System.out::println)
                        .then();

                Mono<Void> test2 = handle.createQuery("select * from auth_keys where user_id = :userId")
                        .bind("userId", 1)
                        .mapRow(row -> row.get("user_id", Long.class))
                        .doOnNext(System.out::println)
                        .then();


                return Mono.when(test1, test2);
            }
        }).blockLast();
    }

    @Test
    void testAuthSaveKey() {
        byte[] bytes = new byte[32];
        ThreadLocalRandom.current().nextBytes(bytes);
        authKeyRepository.insert(AuthKey.of(999931111L, bytes, Instant.now())).block();
    }

    @Test
    void testUpdates(){
        SerialMapping serialMapping =  buildMapping(new UpdateUserName(1, "somte", null), Collections.emptyMap());
        SequenceUpdate sequenceUpdate = SequenceUpdate.of(Id.of(1), 1, Instant.now(), serialMapping);
        SequenceUpdate persisted = updatesRepository.insert(sequenceUpdate).block();
        System.out.println(persisted);

        /*System.out.println(updatesRepository.findMaxSeq(UserId.of(1)).block());

        updatesRepository.findSequenceAfter(UserId.of(1), 1, 50)
                .doOnNext(System.out::println)
                .blockLast();*/
    }

    @Test
    void test_privacy() {
       // Privacy privacy = privacyRepository.create(Privacy.of(UserId.of(1), UserId.of(2), LocalDateTime.now())).block();
       // System.out.println(privacy);

       /* privacyRepository.findAllByUserId(UserId.of(1), 20, 0)
                .doOnNext(System.out::println)
                .blockLast();*/

        privacyRepository.countByUserId(Id.of(1))
                .doOnNext(System.out::println)
                .block();

       /* privacyRepository.create(Privacy.of(UserId.of(5), UserId.of(6), LocalDateTime.now()))
                .doOnNext(System.out::println)
                .block();*/
    }

    //@Test
    @RepeatedTest(value = 10)
    void test_fetch_seq() {

       /* sequenceRepository.saveUpdate(UserId.of(1), System.currentTimeMillis(),
                buildMapping(new UpdateUserName(1, "somte", null), Collections.emptyMap()).toByteArray()).block();
        StepVerifier.create(updatesRepository.findMaxSeq(UserId.of(1)))
                .expectSubscription()
                .assertNext(seqState -> {
                    System.out.println(seqState);
                    System.out.println(fromBytes(seqState.getMapping()).getUpdate());
                })
                .verifyComplete();*/
    }

    private static SerialMapping buildMapping(Update update, Map<Long, Update> custom) {
        final Map<Long, SerialUpdate> customUpdates = custom.entrySet()
                .stream()
                .map(entry -> Tuples.of(entry.getKey(), buildSerialUpdate(entry.getValue())))
                .collect(Collectors.toMap(Tuple2::getT1, Tuple2::getT2));
        return new SerialMapping(buildSerialUpdate(update), customUpdates);
    }

    private static SerialUpdate buildSerialUpdate(Update update) {
        final CombinedDifference difference = GetDiffCombiner.buildDiff(update);

        final SerialUpdate serialUpdate = new SerialUpdate();
        serialUpdate.setHeader(update.getHeader());
        serialUpdate.setBody(update.toByteArray());
        serialUpdate.setUsers(difference.getUpdatedUsers());
        serialUpdate.setGroups(difference.getUpdatedChats());
        return serialUpdate;
    }

    private static SerialMapping fromBytes(byte[] source) {
        try {
            return SerialMapping.fromBytes(source);
        } catch (IOException e) {
            return new SerialMapping();
        }
    }

    @RepeatedTest(value = 10)
    void test_bulkAsyncUpdate() {
        Flux<Integer> ids = Flux.range(1, 201);

        Set<Id> users = IntStream.range(1, 201)
                .boxed()
                .map(Id::of)
                .collect(Collectors.toSet());

        StepVerifier.create(updateService.forwardSeqUpdate(users, new UpdateUserName()))
                .expectSubscription()
                .verifyComplete();

       /* StepVerifier.create(updateService.broadcastBulkUpdate(1, 1L, ids, new UpdateUserNameChanged(1, "New name")))
                .expectSubscription()
                .assertNext(seqState -> {
                    System.out.println(seqState.getSequence());
                    System.out.println(seqState.getTimestamp());
                })
                .verifyComplete();*/
    }

    //@Test
    @RepeatedTest(value = 1)
    void test_bulkAsyncUpdates() {
        Flux.range(1, 200)
                 .flatMap(id -> updateService.forwardSeqUpdate(Id.of(id), new UpdateUserName(1, "somte", null)))
                .blockLast();

       // updateService.forwardSeqUpdate(UserId.of(1), new UpdateUserName(1, "somte", null)).block();
       /*= StepVerifier.create(updateService.forwardSeqUpdate(UserId.of(1), new UpdateUserName(1, "somte", null)))
                .expectSubscription()
                .verifyComplete();*/

        List<Integer> vals = IntStream.range(1, 50)
                .boxed()
                .collect(Collectors.toList());

     /*   StepVerifier.create(updatesRepository.insert(vals, 200L, new byte[32]))
                .expectSubscription()
                .verifyComplete();*/

     /*   StepVerifier.create(updateService.broadcastBulkUpdate(ids, new UpdateUserNameChanged(1, "New name"), null))
                .expectSubscription()
                .verifyComplete();*/
    }

    @Test
    void test_GetDifference() {
        StepVerifier.create(updateService.getDifference(Id.of(1), 0L, 20, maxDiffSize.toBytes()))
                .expectSubscription()
                .assertNext(diffAcc -> {
                    System.out.println(diffAcc.getUpdates().size());
                    System.out.println(diffAcc.isHasMore());
                })
                .verifyComplete();
    }

    @Test
    void test_serializeMapping() throws Exception {
        UpdateUserName update = new UpdateUserName();
        update.setUserId(1);
        update.setUsername("name");

        SerialUpdate serialUpdate = new SerialUpdate(update.getHeader(), update.toByteArray(),
                Collections.emptyList(), Collections.emptyList());

        SerialMapping without_custom = new SerialMapping(serialUpdate, null);
        SerialMapping with_custom = new SerialMapping(serialUpdate, Map.of(1L, serialUpdate));

        byte[] serialized_without_custom = without_custom.toByteArray();
        byte[] serialized_custom = with_custom.toByteArray();

        SerialMapping deserializedDef = SerialMapping.fromBytes(serialized_without_custom);
        SerialMapping deserializedCustom = SerialMapping.fromBytes(serialized_custom);

        assertThat(deserializedDef.getUpdate().getBody()).isEqualTo(serialUpdate.getBody());
        assertThat(deserializedCustom.getCustomOrDefault(1L)).isNotNull();
        assertThat(deserializedCustom.getCustomOrDefault(1L).getBody()).isEqualTo(serialUpdate.getBody());
    }
}