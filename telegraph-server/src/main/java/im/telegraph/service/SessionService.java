package im.telegraph.service;

import im.telegraph.cluster.Cluster;
import im.telegraph.common.DateTimeFactory;
import im.telegraph.domain.entity.AuthKey;
import im.telegraph.domain.entity.Id;
import im.telegraph.domain.entity.SessionInfo;
import im.telegraph.domain.entity.User;
import im.telegraph.domain.event.TerminateSessionEvent;
import im.telegraph.domain.repository.SessionRepository;
import im.telegraph.protocol.MTProtoSession;
import im.telegraph.protocol.MTProtoSessionRegistry;
import im.telegraph.protocol.SessionId;
import im.telegraph.pubsub.PubSubMediator;
import im.telegraph.rpc.base.Update;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.time.Instant;
import java.util.Objects;

/**
 * @author Ast3t1s
 */
@Service
public class SessionService {

    private static final Logger log = LoggerFactory.getLogger(SessionService.class);

    private final MTProtoSessionRegistry sessionRegistry;
    private final SessionRepository sessionRepository;
    private final DateTimeFactory dateFactory;
    private final PubSubMediator pubSubMediator;
    private final Cluster cluster;

    public SessionService(MTProtoSessionRegistry sessionRegistry,
                          SessionRepository sessionRepository,
                          DateTimeFactory dateFactory,
                          PubSubMediator pubSubMediator,
                          Cluster cluster) {
        this.sessionRegistry = sessionRegistry;
        this.sessionRepository = sessionRepository;
        this.dateFactory = dateFactory;
        this.pubSubMediator = pubSubMediator;
        this.cluster = cluster;
    }

    public Mono<Void> bindSession(final Id userId, final SessionId sessionId) {
        return registerDevice(userId, sessionId.getAuthId(), Duration.ofSeconds(60))
                .then(sessionRegistry.getSession(sessionId).doOnNext(session -> session.setPrincipal(userId.asLong())))
                .then();
    }

    /**
     * Register session device. It means that device gets the opportunity to receive {@link Update}
     * ie.e. presences, messages, etc.
     *
     * @param userId the identifier of the user.
     * @param authId the identifier of the device auth key.
     * @param timeout the device session timeout.
     * @return A {@link Mono} where, upon successful completion, emits nothing; indicating the session's device registered
     * If an error is received, it is emitted through the {@code Mono}.
     */
    public Mono<Void> registerDevice(final Id userId, final Long authId, final Duration timeout) {
        return sessionRepository.compute(userId, (id, sessionInfo) -> {
            final String nodeId = cluster.getUid().toString();
            final Instant expireAt = dateFactory.getClock().instant().plus(timeout);
            if (sessionInfo == null) {
                return Mono.just(SessionInfo.single(userId, authId, nodeId, expireAt));
            }
            return Mono.just(sessionInfo.addDevice(authId, nodeId, expireAt));
        }).then();
    }

    /**
     * Unregister session's device. It means the device no longer receive {@link Update}
     * ie.e. presences, messages, etc.
     *
     * @param userId the identifier of the user.
     * @param authId the identifier of the device auth key.
     * @return A {@link Mono} where, upon successful completion, emits nothing; indicating the session's device registered
     * If an error is received, it is emitted through the {@code Mono}.
     */
    public Mono<Void> unregisterDevice(final Id userId, final Long authId) {
        return sessionRepository.compute(userId, (id, sessionInfo) -> {
            if (sessionInfo == null) {
                return Mono.empty();
            }
            return Mono.just(sessionInfo.removeDevice(authId));
        }).then();
    }

    /**
     * Get the information about user's session if present.
     *
     * @param userId the identifier of the user.
     * @return A {@link Mono} where, upon successful completion, emits {@link SessionInfo}; indicating the sessions have been closed.
     * If an error is received, it is emitted through the {@code Mono}.
     */
    public Mono<SessionInfo> getSessionInfo(final Id userId) {
        return sessionRepository.findById(userId);
    }

    /**
     * Close the local device session on current node.
     *
     * @param userId the {@link User} identifier.
     * @return A {@link Mono} where, upon successful completion, emits nothing; indicating the sessions have been closed.
     * If an error is received, it is emitted through the {@code Mono}.
     */
    public Mono<Void> setLocalSessionOffline(Id userId, Long authId, Instant date) {
        // Don't close the real session (connection) first and then remove session from {@link SessionRepository}
        // because it will make problem if a client logins again while the session status in repository been removed
        return Mono.empty();
    }

    public Mono<Void> terminateSession(final Id userId, final Long authKeyId) {
        return sessionRepository.findById(userId)
                .flatMapIterable(sessionInfo -> sessionInfo.getDevices().values())
                .filter(device -> Objects.equals(device.getAuthId(), authKeyId))
                .flatMap(device -> pubSubMediator.send("sessions", device.getNode(),
                        new TerminateSessionEvent(userId.asLong(), device.getAuthId())))
                .then();
    }

    /**
     * Retrieves the full sessions of the user on the current node.
     *
     * @param userId the ID of the user.
     * @return A {@link Flux} that continually emits the {@link MTProtoSession session} of the user.
     * If an error is received, it is emitted through the {@code Flux}.
     */
    public Flux<MTProtoSession> getSessions(final Id userId) {
        /*return Flux.defer(() -> {
            List<SessionId> sessions = sessionBindings.get(userId);
            if (sessions == null) {
                return Flux.empty();
            }
            return sessionRegistry.getSessions()
                    .filter(session -> sessions.contains(session.getId()));
        });*/
        return Flux.empty();
    }

    public Mono<Void> closeLocalSession(final SessionId sessionId) {
        return sessionRegistry.deregister(sessionId)
                .doFinally(signal -> {
                    log.debug("Unbind local session {}", sessionId);
                });
    }

    /**
     * Close local session of the devices on the current node.
     *
     * @param authId the {@link AuthKey} the identifier.
     * @return A {@link Mono} where, upon successful completion, emits nothing; indicating the sessions have been closed.
     * If an error is received, it is emitted through the {@code Mono}.
     */
    public Mono<Void> closeLocalSession(final long authId) {
        return sessionRegistry.getSessions()
                .filter(session -> session.getId().getAuthId() == authId)
                .flatMap(session -> sessionRegistry.deregister(session.getId()))
                .then();
    }

    /**
     * Close all sessions of the {@link User} on the current node.
     *
     * @param userId the identifier of the user.
     * @return A {@link Mono} where, upon successful completion, emits nothing; indicating the sessions have been closed.
     * If an error is received, it is emitted through the {@code Mono}.
     */
    public Mono<Void> closeLocalSession(final Id userId) {
        return sessionRegistry.getSessions()
                .filter(session -> Objects.equals(userId.asLong(), session.getPrincipal()))
                .flatMap(session -> sessionRegistry.deregister(session.getId()))
                .then();
    }

    /**
     * Close all local session on current node.
     *
     * @return A {@link Mono} where, upon successful completion, emits nothing; indicating the sessions have been closed.
     * If an error is received, it is emitted through the {@code Mono}.
     */
    public Mono<Void> closeAllLocalSessions() {
        return sessionRegistry.getSessions()
                .flatMap(session -> sessionRegistry.deregister(session.getId()))
                .onErrorResume(ex -> Mono.fromRunnable(
                        () -> log.warn("Unexpected error due closing local session", ex)))
                .then();
    }
}
