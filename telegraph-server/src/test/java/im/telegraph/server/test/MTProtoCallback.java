package im.telegraph.server.test;

public interface MTProtoCallback {

    void onResponseMessage(long messageId, byte[] content, MTProto proto);

    void onUpdate(byte[] update);

}
