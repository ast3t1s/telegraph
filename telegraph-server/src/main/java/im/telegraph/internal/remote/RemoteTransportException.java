package im.telegraph.internal.remote;

/**
 * @author Ast3t1s
 */
@SuppressWarnings("serial")
public class RemoteTransportException extends RuntimeException {

    public RemoteTransportException(String message) {
        super(message);
    }

    public RemoteTransportException(String message, Throwable cause) {
        super(message, cause);
    }
}
