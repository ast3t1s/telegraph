package im.telegraph.generator.generator.javapoet;

import com.squareup.javapoet.*;
import im.telegraph.generator.generator.Constants;
import im.telegraph.generator.generator.SchemeContainerGenerator;
import im.telegraph.generator.type.*;

import javax.lang.model.element.Modifier;
import java.io.IOException;

public class JavaPoetContainerGenerator implements SchemeContainerGenerator<TypeSpec.Builder> {

    @Override
    public void generateFields(TypeSpec.Builder generator, SchemeDefinition definition,
                               SchemeContainer container) throws IOException {
        for (SchemeAttribute attribute : container.getFilteredAttributes()) {
            generator.addField(FieldSpec.builder(convertType(definition, attribute.getType()), attribute.getName())
                    .addModifiers(Modifier.PROTECTED)
                    .build());
        }
    }

    @Override
    public void generateConstructor(TypeSpec.Builder generator, SchemeDefinition definition,
                                    SchemeContainer container) throws IOException {
        MethodSpec.Builder constructorBuilder = MethodSpec.constructorBuilder()
                .addModifiers(Modifier.PUBLIC);
        generateConstructorArgs(constructorBuilder, definition, container);
        for (SchemeAttribute attribute : container.getFilteredAttributes()) {
            constructorBuilder.addStatement("this.$N = $N", attribute.getName(), attribute.getName());
        }
        generator.addMethod(constructorBuilder.build());
    }

    @Override
    public void generateGettersAndSetters(TypeSpec.Builder generator, SchemeDefinition definition,
                                          SchemeContainer container) throws IOException {
        for (SchemeAttribute attribute : container.getFilteredAttributes()) {
            TypeName type = convertType(definition, attribute.getType());
            String setterName = TypeHelper.getSetterName(attribute.getName());
            String getterName = (((type.isPrimitive() && type.equals(TypeName.BOOLEAN)))
                    || (type.isBoxedPrimitive() && type.unbox() == TypeName.BOOLEAN))
                    ? TypeHelper.getBoolGetterName(attribute.getName()) : TypeHelper.getGetterName(attribute.getName());

            MethodSpec getter = MethodSpec.methodBuilder(getterName)
                    .addModifiers(Modifier.PUBLIC)
                    .returns(type)
                    .addStatement("return this.$L", attribute.getName())
                    .build();

            MethodSpec setter = MethodSpec.methodBuilder(setterName)
                    .addModifiers(Modifier.PUBLIC)
                    .addParameter(type, attribute.getName())
                    .addStatement("this.$L = $L", attribute.getName(), attribute.getName())
                    .build();

            generator.addMethod(getter);
            generator.addMethod(setter);
        }
    }

    @Override
    public void generateDeserialization(TypeSpec.Builder generator, SchemeDefinition definition,
                                        SchemeContainer container) throws IOException {
        generateDeserialization(generator, container, definition, false);
    }

    public void generateDeserialization(TypeSpec.Builder generator, SchemeContainer container,
                                        SchemeDefinition definition, boolean isExpandable) throws IOException {
        MethodSpec.Builder parseBuilder = MethodSpec.methodBuilder("deserialize")
                .addModifiers(Modifier.PUBLIC)
                .addParameter(Constants.TYPE_READER, "reader")
                .addAnnotation(Override.class)
                .addException(IOException.class);
        for (SchemeAttribute attribute : container.getFilteredAttributes()) {
            generateDeserialization(parseBuilder, attribute.getId(), attribute.getName(), attribute.getType(), definition);
        }
        if (isExpandable) {
            parseBuilder.beginControlFlow("if (reader.hasRemaining())");
            parseBuilder.addStatement("setUnmappedObjects(reader.buildRemaining())");
            parseBuilder.endControlFlow();
        }
        generator.addMethod(parseBuilder.build());
    }

    private void generateDeserialization(MethodSpec.Builder builder, int attributeId, String attributeName,
                                         SchemeType type, SchemeDefinition definition) throws IOException {
        type = reduceAlias(type, definition);
        if (type instanceof SchemePrimitiveType) {
            SchemePrimitiveType primitiveType = (SchemePrimitiveType) type;
            if (primitiveType.getName().equals("int32")) {
                builder.addStatement("this.$L = reader.readInt($L)", attributeName, attributeId);
            } else if (primitiveType.getName().equals("int64")) {
                builder.addStatement("this.$L = reader.readLong($L)", attributeName, attributeId);
            } else if (primitiveType.getName().equals("bool")) {
                builder.addStatement("this.$L = reader.readBool($L)", attributeName, attributeId);
            } else if (primitiveType.getName().equals("bytes")) {
                builder.addStatement("this.$L = reader.readByteArray($L)", attributeName, attributeId);
            } else if (primitiveType.getName().equals("string")) {
                builder.addStatement("this.$L = reader.readString($L)", attributeName, attributeId);
            } else if (primitiveType.getName().equals("double")) {
                builder.addStatement("this.$L = reader.readDouble($L)", attributeName, attributeId);
            } else {
                throw new IOException();
            }
        } else if (type instanceof SchemeStructType) {
            builder.addStatement("this.$L = reader.readSerializable($L, new $T())", attributeName, attributeId,
                    getStructName(((SchemeStructType) type).getType()));
        } else if (type instanceof SchemeEnumType) {
            SchemeEnumType enumType = (SchemeEnumType) type;
            builder.addStatement("this.$L = $N.parse(reader.readInt($L))", attributeName,
                    TypeHelper.getEnumName(enumType.getName()), attributeId);
        } else if (type instanceof SchemeOptionalType) {
            SchemeOptionalType optType = (SchemeOptionalType) type;
            SchemeType childType = reduceAlias(optType.getType(), definition);
            if (childType instanceof SchemePrimitiveType) {
                SchemePrimitiveType primitiveType = (SchemePrimitiveType) childType;
                if (primitiveType.getName().equals("int32")) {
                    builder.addStatement("this.$L = reader.readOptionalInt($L)", attributeName, attributeId);
                } else if (primitiveType.getName().equals("int64")) {
                    builder.addStatement("this.$L = reader.readOptionalLong($L)", attributeName, attributeId);
                } else if (primitiveType.getName().equals("bool")) {
                    builder.addStatement("this.$L = reader.readOptionalBoolean($L)", attributeName, attributeId);
                } else if (primitiveType.getName().equals("bytes")) {
                    builder.addStatement("this.$L = reader.readOptionalBytes($L)", attributeName, attributeId);
                } else if (primitiveType.getName().equals("string")) {
                    builder.addStatement("this.$L = reader.readOptionalString($L)", attributeName, attributeId);
                } else if (primitiveType.getName().equals("double")) {
                    builder.addStatement("this.$L = reader.readOptionalDouble($L)", attributeName, attributeId);
                } else {
                    throw new IOException();
                }
            } else if (childType instanceof SchemeStructType) {
                builder.addStatement("this.$L = reader.readOptionalSerializable($L, new $T())",
                        attributeName, attributeId, getStructName(((SchemeStructType) childType).getType()));
            } else if (childType instanceof SchemeEnumType) {
                builder.addStatement("int val_$L = reader.readInt($L, 0)", attributeName, attributeId)
                        .beginControlFlow("if (val_$L != 0)", attributeName)
                        .addStatement("this.$L = $N.parse(val_$L)", attributeName,
                                TypeHelper.getEnumName(((SchemeEnumType) childType).getName()), attributeName)
                        .endControlFlow();
            } else if (childType instanceof SchemeTraitType) {
                builder.beginControlFlow("if (reader.readOptionalBytes($L) != null)", attributeId);
                String traitName = ((SchemeTraitType) childType).getTraitName();
                SchemeTrait trait = definition.getTrait(traitName);
                if (trait.isContainer()) {
                    builder.addStatement("this.$L = $T.fromBytes(reader.readByteArray($L))", attributeName,
                            getStructName(traitName), attributeId);
                } else {
                    builder.addStatement("this.$L = $T.fromBytes(reader.readInt($L))", attributeId - 1,
                            getStructName(traitName), attributeId);
                }
                builder.endControlFlow();
            } else {
                throw new IOException();
            }
        } else if (type instanceof SchemeListType) {
            SchemeListType listType = (SchemeListType) type;
            SchemeType childType = reduceAlias(listType.getType(), definition);
            if (childType instanceof SchemePrimitiveType) {
                SchemePrimitiveType primitiveType = (SchemePrimitiveType) childType;
                if (primitiveType.getName().equals("int32")) {
                    builder.addStatement("this.$L = reader.readIntegerList($L)", attributeName, attributeId);
                } else if (primitiveType.getName().equals("int64")) {
                    builder.addStatement("this.$L = reader.readLongList($L)", attributeName, attributeId);
                } else if (primitiveType.getName().equals("bool")) {
                    builder.addStatement("this.$L = reader.readBoolList($L)", attributeName, attributeId);
                } else if (primitiveType.getName().equals("bytes")) {
                    builder.addStatement("this.$L = reader.readByteArrayList($L)", attributeName, attributeId);
                } else if (primitiveType.getName().equals("string")) {
                    builder.addStatement("this.$L = reader.readStringList($L)", attributeName, attributeId);
                } else if (primitiveType.getName().equals("double")) {
                    builder.addStatement("this.$L = reader.readDoubleList($L)", attributeName, attributeId);
                } else {
                    throw new IOException();
                }
            } else if (childType instanceof SchemeStructType) {
                TypeName typeName = getStructName(((SchemeStructType) childType).getType());
                ClassName listTypeName = ClassName.get("java.util", "List");
                ClassName arrayListName = ClassName.get("java.util", "ArrayList");
                TypeName listOfObjs = ParameterizedTypeName.get(listTypeName, typeName);
                builder.addStatement("$T _$L = new $T<>()", listOfObjs, attributeName, arrayListName)
                        .beginControlFlow("for (int i = 0; i < reader.getListSize($L); i++)", attributeId)
                        .addStatement("_$L.add(new $T())", attributeName, typeName)
                        .endControlFlow()
                        .addStatement("this.$L = reader.readSerializableList($L, _$L)", attributeName, attributeId, attributeName);
            } else if (childType instanceof SchemeTraitType) {
                SchemeTraitType traitType = (SchemeTraitType) childType;
                TypeName typeName = getStructName(traitType.getTraitName());
                ClassName arrayListName = ClassName.get("java.util", "ArrayList");
                builder.addStatement("this.$L = new $T<>()", attributeName, arrayListName)
                        .beginControlFlow("for (byte[] b : reader.readByteArrayList($L))", attributeId)
                        .addStatement("$L.add($T.fromBytes(b))", attributeName, typeName)
                        .endControlFlow();
            } else if (childType instanceof SchemeEnumType) {
                SchemeEnumType enumType = (SchemeEnumType) childType;
                String enumName = TypeHelper.getEnumName(enumType.getName());
                ClassName arrayListName = ClassName.get("java.util", "ArrayList");
                builder.addStatement("this.$L = new $T<>()", attributeName, arrayListName)
                        .beginControlFlow("for (int b : reader.readIntegerList($L))", attributeId)
                        .addStatement("$L.add($L.parse(b))", attributeName, enumName)
                        .endControlFlow();
            } else {
                throw new IOException();
            }
        } else if (type instanceof SchemeTraitType) {
            String traitName = ((SchemeTraitType) type).getTraitName();
            SchemeTrait trait = definition.getTrait(traitName);
            if (trait.isContainer()) {
                builder.addStatement("this.$L = $T.fromBytes(reader.readByteArray($L))", attributeName,
                        getStructName(traitName), attributeId);
            } else {
                builder.addStatement("this.$L = $T.fromBytes(reader.readInt($L), reader.readByteArray($L))",
                        attributeName, getStructName(traitName), attributeId - 1, attributeId);
            }
        } else {
            throw new IOException();
        }
    }

    @Override
    public void generateSerialization(TypeSpec.Builder generator, SchemeDefinition definition,
                                      SchemeContainer container) throws IOException {
        generateSerialization(generator, container, definition, false);
    }

    @Override
    public void generateToString(TypeSpec.Builder generator, SchemeDefinition definition,
                                 SchemeContainer container) throws IOException {
        MethodSpec.Builder toStringBuilder = MethodSpec.methodBuilder("toString")
                .addModifiers(Modifier.PUBLIC)
                .returns(String.class)
                .addAnnotation(Override.class);
        if (container instanceof SchemeUpdate) {
            toStringBuilder.addStatement("String res = \"update $L{\"", ((SchemeUpdate) container).getName());
        } else if (container instanceof SchemeResponse) {
            toStringBuilder.addStatement("String res = \"response $L{\"", ((SchemeResponse) container).getName());
        } else if (container instanceof SchemeRpc) {
            toStringBuilder.addStatement("String res = \"rpc $L{\"", ((SchemeRpc) container).getName());
        } else if (container instanceof SchemeResponseAnonymous) {
            toStringBuilder.addStatement("String res = \"tuple $L{\"", ((SchemeResponseAnonymous) container).getRpc().getName());
        } else if (container instanceof SchemeUpdateBox) {
            toStringBuilder.addStatement("String res = \"update box $L{\"", ((SchemeUpdateBox) container).getName());
        } else if (container instanceof SchemeStruct) {
            toStringBuilder.addStatement("String res = \"struct $L{\"", ((SchemeStruct) container).getName());
        } else {
            throw new IOException();
        }
        boolean isFirst = true;
        for (SchemeAttribute attribute : container.getFilteredAttributes()) {
            ParameterCategory category = container.getParameterCategory(attribute.getName());
            if (category == ParameterCategory.HIDDEN ||
                    category == ParameterCategory.DANGER) {
                continue;
            }
            String append = generateToStringAppend(attribute.getName(), attribute.getType(), definition, category);
            if (isFirst) {
                isFirst = false;
                toStringBuilder.addStatement("res += \"$L=\" + $L", attribute.getName(), append);
            } else {
                toStringBuilder.addStatement("res += \", $L=\" + $L", attribute.getName(), append);
            }

        }
        toStringBuilder.addStatement("res += \"}\"");
        toStringBuilder.addStatement("return res");
        generator.addMethod(toStringBuilder.build());
    }

    public void generateConstructorArgs(MethodSpec.Builder constructor, SchemeDefinition definition,
                                        SchemeContainer container) throws IOException {
        for (SchemeAttribute attribute : container.getFilteredAttributes()) {
            SchemeType schemeType = reduceAlias(attribute.getType(), definition);

            constructor.addParameter(convertType(definition, attribute.getType()), attribute.getName());
        }
    }

    public TypeName convertType(SchemeDefinition definition, SchemeType type) throws IOException {
        type = reduceAlias(type, definition);
        if (type instanceof SchemePrimitiveType) {
            return getPrimitiveType((SchemePrimitiveType) type);
        } else if (type instanceof SchemeStructType) {
            return getStructName(((SchemeStructType) type).getType());
        } else if (type instanceof SchemeListType) {
            SchemeListType listType = (SchemeListType) type;
            SchemeType childType = reduceAlias(listType.getType(), definition);
            if (childType instanceof SchemePrimitiveType) {
                ClassName listTypeName = ClassName.get("java.util", "List");
                return ParameterizedTypeName.get(listTypeName, mapPrimitiveReferenceType((SchemePrimitiveType) childType));
            } else if (childType instanceof SchemeStructType) {
                ClassName listTypeName = ClassName.get("java.util", "List");
                return ParameterizedTypeName.get(listTypeName,
                        getStructName(((SchemeStructType) childType).getType()));
            } else if (childType instanceof SchemeTraitType) {
                SchemeTraitType traitType = (SchemeTraitType) childType;
                if (!definition.getTrait(traitType.getTraitName()).isContainer()) {
                    throw new IOException("List item type can't be trait");
                }
                ClassName listTypeName = ClassName.get("java.util", "List");
                return ParameterizedTypeName.get(listTypeName, getStructName(traitType.getTraitName()));
            } else if (childType instanceof SchemeEnumType) {
                ClassName listTypeName = ClassName.get("java.util", "List");
                return ParameterizedTypeName.get(listTypeName,
                        getEnumRecordName(((SchemeEnumType) childType).getName()));
            } else {
                throw new IOException("Unsupported list item type: " + type);

            }
        } else if (type instanceof SchemeOptionalType) {
            SchemeOptionalType optionalType = (SchemeOptionalType) type;
            SchemeType childType = reduceAlias(optionalType.getType(), definition);
            if (childType instanceof SchemePrimitiveType) {
                return mapPrimitiveReferenceType((SchemePrimitiveType) childType);
            } else if (childType instanceof SchemeStructType) {
                return getStructName(((SchemeStructType) childType).getType());
            } else if (childType instanceof SchemeEnumType) {
                return getEnumRecordName(((SchemeEnumType) childType).getName());
            } else if (childType instanceof SchemeTraitType) {
                return getStructName(((SchemeTraitType) childType).getTraitName());
            } else {
                throw new IOException();
            }
        } else if (type instanceof SchemeEnumType) {
            return getEnumRecordName(((SchemeEnumType) type).getName());
        } else if (type instanceof SchemeTraitType) {
            return getStructName(((SchemeTraitType) type).getTraitName());
        } else {
            throw new IOException();
        }
    }

    private void generateSerialization(TypeSpec.Builder generator, SchemeContainer container,
                                       SchemeDefinition definition, boolean isExpandable) throws IOException {
        MethodSpec.Builder serialize = MethodSpec.methodBuilder(Constants.SERIALIZE_NAME)
                .addModifiers(Modifier.PUBLIC)
                .addAnnotation(Override.class)
                .addException(IOException.class)
                .addParameter(Constants.TYPE_WRITER, "writer");

        for (SchemeAttribute attribute : container.getFilteredAttributes()) {
            generateSerialization(serialize, attribute.getId(), attribute.getName(), attribute.getType(), definition);
        }
        if (isExpandable) {
            serialize.beginControlFlow("if (this.getUnmappedObjects() != null)")
                    .addStatement("SparseArray<Object> unmapped = this.getUnmappedObjects();")
                    .beginControlFlow("for (int i = 0; i < unmapped.size(); i++)")
                    .addStatement("int key = unmapped.keyAt(i)")
                    .addStatement("writer.writeUnmapped(key, unmapped.get(key))")
                    .endControlFlow()
                    .endControlFlow();
        }
        generator.addMethod(serialize.build());
    }

    private void generateSerialization(MethodSpec.Builder builder, int attributeId, String attributeName, SchemeType type,
                                       SchemeDefinition definition) throws IOException {
        type = reduceAlias(type, definition);
        if (type instanceof SchemePrimitiveType) {
            SchemePrimitiveType primitiveType = (SchemePrimitiveType) type;
            if (primitiveType.getName().equals("int32")) {
                builder.addStatement("writer.writeInt($L, this.$L)", attributeId, attributeName);
            } else if (primitiveType.getName().equals("int64")) {
                builder.addStatement("writer.writeLong($L, this.$L)", attributeId, attributeName);
            } else if (primitiveType.getName().equals("bool")) {
                builder.addStatement("writer.writeBool($L, this.$L)", attributeId, attributeName);
            } else if (primitiveType.getName().equals("bytes")) {
                builder.beginControlFlow("if (this.$L == null)", attributeName)
                        .addStatement("throw new IOException()")
                        .endControlFlow()
                        .addStatement("writer.writeBytes($L, this.$L)", attributeId, attributeName);
            } else if (primitiveType.getName().equals("string")) {
                builder.beginControlFlow("if (this.$L == null)", attributeName)
                        .addStatement("throw new IOException()")
                        .endControlFlow()
                        .addStatement("writer.writeString($L, this.$L)", attributeId, attributeName);
            } else if (primitiveType.getName().equals("double")) {
                builder.addStatement("writer.writeDouble($L, this.$L)", attributeId, attributeName);
            } else {
                throw new IOException();
            }
        } else if (type instanceof SchemeStructType) {
            builder.beginControlFlow("if (this.$L == null)", attributeName)
                    .addStatement("throw new IOException()")
                    .endControlFlow()
                    .addStatement("writer.writeSerializable($L, this.$L)", attributeId, attributeName);
        } else if (type instanceof SchemeEnumType) {
            SchemeEnumType e = (SchemeEnumType) type;
            builder.beginControlFlow("if (this.$L == null)", attributeName)
                    .addStatement("throw new IOException()")
                    .endControlFlow()
                    .addStatement("writer.writeInt($L, this.$L.getValue())", attributeId, attributeName);
        } else if (type instanceof SchemeOptionalType) {
            SchemeOptionalType optType = (SchemeOptionalType) type;
            SchemeType childType = reduceAlias(optType.getType(), definition);
            if (childType instanceof SchemePrimitiveType) {
                SchemePrimitiveType primitiveType = (SchemePrimitiveType) childType;
                if (primitiveType.getName().equals("int32")) {
                    builder.beginControlFlow("if (this.$L != null)", attributeName)
                            .addStatement("writer.writeInt($L, this.$L)", attributeId, attributeName)
                            .endControlFlow();
                } else if (primitiveType.getName().equals("int64")) {
                    builder.beginControlFlow("if (this.$L != null)", attributeName)
                            .addStatement("writer.writeLong($L, this.$L)", attributeId, attributeName)
                            .endControlFlow();
                } else if (primitiveType.getName().equals("bool")) {
                    builder.beginControlFlow("if (this.$L != null)", attributeName)
                            .addStatement("writer.writeBool($L, this.$L)", attributeId, attributeName)
                            .endControlFlow();
                } else if (primitiveType.getName().equals("bytes")) {
                    builder.beginControlFlow("if (this.$L != null)", attributeName)
                            .addStatement("writer.writeBytes($L, this.$L)", attributeId, attributeName)
                            .endControlFlow();
                } else if (primitiveType.getName().equals("string")) {
                    builder.beginControlFlow("if (this.$L != null)", attributeName)
                            .addStatement("writer.writeString($L, this.$L)", attributeId, attributeName)
                            .endControlFlow();
                } else if (primitiveType.getName().equals("double")) {
                    builder.beginControlFlow("if (this.$L != null)", attributeName)
                            .addStatement("writer.writeDouble($L, this.$L)", attributeId, attributeName)
                            .endControlFlow();
                } else {
                    throw new IOException();
                }
            } else if (childType instanceof SchemeStructType) {
                builder.beginControlFlow("if (this.$L != null)", attributeName)
                        .addStatement("writer.writeSerializable($L, this.$L)", attributeId, attributeName)
                        .endControlFlow();
            } else if (childType instanceof SchemeEnumType) {
                builder.beginControlFlow("if (this.$L != null)", attributeName)
                        .addStatement("writer.writeInt($L, this.$L.getValue())", attributeId, attributeName)
                        .endControlFlow();
            } else if (childType instanceof SchemeTraitType) {
                builder.beginControlFlow("if (this.$L != null)", attributeName);
                String traitName = ((SchemeTraitType) childType).getTraitName();
                SchemeTrait trait = definition.getTrait(traitName);
                if (trait.isContainer()) {
                    builder.addStatement("writer.writeBytes($L, this.$L.buildContainer())", attributeId, attributeName);
                } else {
                    builder.addStatement("writer.writeInt($L, this.$T.getHeader())", attributeId - 1, attributeName)
                            .addStatement("writer.writeBytes($L, this.$T.toByteArray())", attributeId, attributeName);
                }
                builder.endControlFlow();
            } else {
                throw new IOException();
            }
        } else if (type instanceof SchemeListType) {
            SchemeListType listType = (SchemeListType) type;
            SchemeType childType = reduceAlias(listType.getType(), definition);
            if (childType instanceof SchemePrimitiveType) {
                SchemePrimitiveType primitiveType = (SchemePrimitiveType) childType;
                if (primitiveType.getName().equals("int32")) {
                    builder.addStatement("writer.writeIntegers($L, this.$L)", attributeId, attributeName);
                } else if (primitiveType.getName().equals("int64")) {
                    builder.addStatement("writer.writeLongs($L, this.$L)", attributeId, attributeName);
                } else if (primitiveType.getName().equals("bool")) {
                    builder.addStatement("writer.writeBooleans($L, this.$L)", attributeId, attributeName);
                } else if (primitiveType.getName().equals("bytes")) {
                    builder.addStatement("writer.writeByteArrays($L, this.$L)", attributeId, attributeName);
                } else if (primitiveType.getName().equals("string")) {
                    builder.addStatement("writer.writeStrings($L, this.$L)", attributeId, attributeName);
                } else if (primitiveType.getName().equals("double")) {
                    builder.addStatement("writer.writeDoubles($L, this.$L)", attributeId, attributeName);
                } else {
                    throw new IOException();
                }
            } else if (childType instanceof SchemeStructType) {
                builder.addStatement("writer.writeSerializableCollection($L, this.$L)", attributeId, attributeName);
            } else if (childType instanceof SchemeTraitType) {
                TypeName traitTypeName = getStructName(((SchemeTraitType) childType).getTraitName());
                builder.beginControlFlow("for ($T i : this.$L)", traitTypeName, attributeName)
                        .addStatement("writer.writeBytes($L, i.buildContainer())", attributeId)
                        .endControlFlow();
            } else if (childType instanceof SchemeEnumType) {
                String enumName = TypeHelper.getEnumName(((SchemeEnumType) childType).getName());
                builder.beginControlFlow("for ($L i : this.$L)", enumName, attributeName)
                        .addStatement("writer.writeInt($L, i.getValue())", attributeId)
                        .endControlFlow();
            } else {
                throw new IOException();
            }
        } else if (type instanceof SchemeTraitType) {
            String traitName = ((SchemeTraitType) type).getTraitName();
            SchemeTrait trait = definition.getTrait(traitName);
            builder.beginControlFlow("if (this.$L == null)", attributeName)
                    .addStatement("throw new IOException()")
                    .endControlFlow();
            if (trait.isContainer()) {
                builder.addStatement("writer.writeBytes($L, this.$L.buildContainer())", attributeId, attributeName);
            } else {
                builder.addStatement("writer.writeInt($L, this.$L.getHeader())", attributeId - 1, attributeName)
                        .addStatement("writer.writeBytes($L, this.$L.toByteArray())", attributeId, attributeName);
            }
        } else {
            throw new IOException();
        }
    }

    public static TypeName getEnumRecordName(String name) {
        return ClassName.get(Constants.PACKAGE_API, Constants.ENTITY_PREFIX + name);
    }

    public static TypeName mapPrimitiveReferenceType(SchemePrimitiveType primitiveType) throws IOException {
        if (primitiveType.getName().equals("int32")) {
            return ClassName.get(Integer.class);
        } else if (primitiveType.getName().equals("int64")) {
            return ClassName.get(Long.class);
        } else if (primitiveType.getName().equals("bytes")) {
            return TypeName.get(byte[].class);
        } else if (primitiveType.getName().equals("string")) {
            return ClassName.get(String.class);
        } else if (primitiveType.getName().equals("double")) {
            return ClassName.get(Double.class);
        } else if (primitiveType.getName().equals("bool")) {
            return ClassName.get(Boolean.class);
        } else {
            throw new IOException();
        }
    }

    public TypeName getPrimitiveType(SchemePrimitiveType primitiveType) throws IOException {
        if (primitiveType.getName().equals("int32")) {
            return TypeName.INT;
        } else if (primitiveType.getName().equals("int64")) {
            return TypeName.LONG;
        } else if (primitiveType.getName().equals("bytes")) {
            return TypeName.get(byte[].class);
        } else if (primitiveType.getName().equals("string")) {
            return TypeName.get(String.class);
        } else if (primitiveType.getName().equals("double")) {
            return TypeName.DOUBLE;
        } else if (primitiveType.getName().equals("bool")) {
            return TypeName.BOOLEAN;
        } else {
            throw new IOException();
        }
    }

    public TypeName getStructName(String struct) {
        return ClassName.get(Constants.PACKAGE_API, Constants.ENTITY_PREFIX + struct);
    }

    public SchemeType reduceAlias(SchemeType type, SchemeDefinition definition) {
        if (type instanceof SchemeAliasType) {
            return reduceAlias(definition.getAliases().get(((SchemeAliasType) type).getName()), definition);
        } else {
            return type;
        }
    }

    private String generateToStringAppend(String attributeName, SchemeType type, SchemeDefinition definition,
                                          ParameterCategory category) throws IOException {
        type = reduceAlias(type, definition);
        if (type instanceof SchemePrimitiveType) {
            SchemePrimitiveType primitiveType = (SchemePrimitiveType) type;
            if (primitiveType.getName().equals("bytes")) {
                if (category == ParameterCategory.COMPACT) {
                    return "convertByteToString(this." + attributeName + ")";
                } else {
                    return "convertByteToString(this." + attributeName + ")";
                }
            } else {
                return "this." + attributeName;
            }
        } else if (type instanceof SchemeStructType) {
            if (category == ParameterCategory.COMPACT) {
                return "(this." + attributeName + " != null ? \"set\" : \"empty\")";
            } else {
                return "this." + attributeName;
            }
        } else if (type instanceof SchemeEnumType) {
            return "this." + attributeName;
        } else if (type instanceof SchemeOptionalType) {
            SchemeOptionalType optType = (SchemeOptionalType) type;
            SchemeType childType = reduceAlias(optType.getType(), definition);

            if (category == ParameterCategory.COMPACT) {
                if (childType instanceof SchemePrimitiveType) {
                    SchemePrimitiveType primitiveType = (SchemePrimitiveType) childType;
                    if (primitiveType.getName().equals("bytes")) {
                        return "convertByteToString(this." + attributeName + ")";
                    } else {
                        return "this." + attributeName;
                    }
                } else if (childType instanceof SchemeStructType) {
                    return "(this." + attributeName + " != null ? \"set\" : \"empty\")";
                } else if (childType instanceof SchemeEnumType) {
                    return "this." + attributeName;
                } else if (childType instanceof SchemeTraitType) {
                    return "(this." + attributeName + " != null ? \"set\" : \"empty\")";
                }
            } else {
                return "this." + attributeName;
            }
        } else if (type instanceof SchemeListType) {

            // TODO: Implement
            if (category == ParameterCategory.COMPACT) {
                return "this." + attributeName + ".size()";
            } else {
                return "this." + attributeName;
            }
        } else if (type instanceof SchemeTraitType) {
            if (category == ParameterCategory.COMPACT) {
                return "(this." + attributeName + " != null ? \"set\" : \"empty\")";
            } else {
                return "this." + attributeName;
            }
        } else {
            throw new IOException();
        }
        throw new IOException();
    }

}
