package im.telegraph.protocol.model.mtproto.rpc;

import im.telegraph.engine.serialization.DataInputStream;
import im.telegraph.engine.serialization.DataOutputStream;
import im.telegraph.protocol.model.mtproto.ProtoStruct;

import java.io.IOException;

public class RpcOk extends ProtoStruct {

	public static final byte HEADER = (byte) 0x01;

	public int responseType;

	public byte[] payload;

	public RpcOk(DataInputStream stream) throws IOException {
		super(stream);
	}

	public RpcOk(int responseType, byte[] payload) {
		this.responseType = responseType;
		this.payload = payload;
	}

	public int getResponseType() {
		return responseType;
	}

	public byte[] getPayload() {
		return payload;
	}

	@Override
	protected byte getHeader() {
		return HEADER;
	}

	@Override
	protected void writeBody(DataOutputStream outputStream) throws IOException {
		outputStream.writeInt32(responseType);
		outputStream.writeProtoBytes(payload);
	}

	@Override
	protected void readBody(DataInputStream bs) throws IOException {
		responseType = bs.readInt();
		payload = bs.readProtoBytes();
	}

	@Override
	public String toString() {
		return "RpcOk{" + responseType + "]";
	}

}
