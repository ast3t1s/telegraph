package im.telegraph.admin.object.request;

/**
 * @author Ast3t1s
 */
public class RemoveClusterNodeRequest {

    private String address;

    public String getAddress() {
        return address;
    }

    @Override
    public String toString() {
        return "RemoveClusterNode{" +
                "address='" + address + '\'' +
                '}';
    }
}
