package im.telegraph.cluster;

import im.telegraph.internal.remote.Address;

/**
 * Interface for failure detector
 *
 * @author Ast3t1s
 */
public interface FailureDetector {

    /**
     * Returns {@code true} if the address if considered to be up and healthy and returns false otherwise.
     *
     * @param address the address to check
     * @return {@code true} if the member if considered to be up and healthy and returns false otherwise
     */
    boolean isAvailable(Address address);

    /**
     * Returns true if the failure detector has received any heartbeats and started monitoring
     * of the resource.
     *
     * @param address the address to check
     * @return true if the failure detector has received any heartbeats and started monitoring of the resource.
     */
    boolean isMonitoring(Address address);

    /**
     * Records a heartbeat for a address.
     *
     * @param address a address for record
     */
    void heartbeat(Address address);

    /**
     * Removes the heartbeat management for a connection.
     */
    void remove(Address address);

    /**
     * Removes all connections and starts over.
     */
    void reset();

}
