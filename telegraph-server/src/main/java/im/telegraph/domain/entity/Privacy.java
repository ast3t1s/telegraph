package im.telegraph.domain.entity;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

public class Privacy implements Serializable {

    private static final long serialVersionUID = 1L;

    private final Id userId;
    private final Id blockedId;
    private final Instant createdAt;

    /**
     * Constructs a {@code Privacy} with an associated data.
     *
     * @param userId    Identifier of the user who blocked the user.
     * @param blockedId Identifier of the user whom blocked by.
     * @param createdAt Point in time (Unix timestamp) when user has been blocked.
     */
    public Privacy(final Id userId, final Id blockedId, final Instant createdAt) {
        this.userId = Objects.requireNonNull(userId);
        this.blockedId = Objects.requireNonNull(blockedId);
        this.createdAt = Objects.requireNonNull(createdAt);
    }

    /**
     * Factory method for create a privacy object.
     *
     * @param userId    the ID of the user.
     * @param blockedId the ID of the block user.
     * @param createdAt the date when user is blocked.
     */
    public static Privacy of(Id userId, Id blockedId, Instant createdAt) {
        return new Privacy(userId, blockedId, createdAt);
    }

    /**
     * Gets the ID of the user who block.
     *
     * @return the ID of the user who block.
     */
    public Id getUserId() {
        return userId;
    }

    /**
     * Gets the ID of the user who is blocked.
     *
     * @return the ID of the user who is blocked.
     */
    public Id getBlockedId() {
        return blockedId;
    }

    /**
     * Gets the date when user was blocked.
     *
     * @return the date when user was blocked.
     */
    public Instant getCreatedAt() {
        return createdAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Privacy privacy = (Privacy) o;
        return Objects.equals(userId, privacy.userId)
                && Objects.equals(blockedId, privacy.blockedId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, blockedId);
    }

    @Override
    public String toString() {
        return "Privacy{" +
                "userId=" + userId +
                ", blockedId=" + blockedId +
                ", createdAt=" + createdAt +
                '}';
    }
}
