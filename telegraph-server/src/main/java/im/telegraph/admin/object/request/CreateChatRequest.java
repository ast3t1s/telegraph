package im.telegraph.admin.object.request;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Ast3t1s
 */
public class CreateChatRequest {

    @JsonProperty("owner_id")
    private Integer ownerId;
    private String title;
    private String description;
    private Long permissions;

    public Integer getOwnerId() {
        return ownerId;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public Long getPermissions() {
        return permissions;
    }

    @Override
    public String toString() {
        return "CreateChatRequest{" +
                "ownerId=" + ownerId +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", permissions=" + permissions +
                '}';
    }
}
