/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package im.telegraph.engine.serialization;

import java.io.IOException;
import java.util.List;

public interface StreamReader {

	long readOptionalLong(int index) throws IOException;

	int readOptionalInt(int index) throws IOException;

	double readOptionalDouble(int index) throws IOException;

	boolean readOptionalBoolean(int index) throws IOException;

	byte[] readOptionalBytes(int index) throws IOException;

	String readOptionalString(int index) throws IOException;

	long readLong(int index, long defValue) throws IOException;

	double readDouble(int index) throws IOException;

	int readInt(int index, int defValue) throws IOException;

	boolean readBool(int index) throws IOException;

	String readString(int index) throws IOException;

	String readString(int index, String defValue) throws IOException;

	byte[] readByteArray(int index) throws IOException;

	byte[] readByteArray(int index, byte[] defValue) throws IOException;

	long readLong(int index) throws IOException;

	int readInt(int index) throws IOException;

	<T extends StreamSerializable> T readOptionalSerializable(int index, T defaultValue) throws IOException;

	<T extends StreamSerializable> T readSerializable(int index, T defaultValue) throws IOException;

	List<Integer> readIntegerList(int index) throws IOException;

	List<byte[]> readByteArrayList(int index) throws IOException;

	List<Long> readLongList(int index) throws IOException;

	List<String> readStringList(int index) throws IOException;

	int getListSize(int index) throws IOException;

	<T extends StreamSerializable> List<T> readSerializableList(int index, List<T> defValue) throws IOException;

}
