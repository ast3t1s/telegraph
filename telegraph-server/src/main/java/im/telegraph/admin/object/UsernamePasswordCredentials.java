package im.telegraph.admin.object;

/**
 * @author Ast3t1s
 */
public class UsernamePasswordCredentials {

    private final String username;
    private final String password;

    public UsernamePasswordCredentials(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
