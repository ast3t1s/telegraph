/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package im.telegraph.engine.serialization;

import java.io.IOException;
import java.util.Collection;

public interface StreamWriter {

	void writeBytes(int index, byte[] value) throws IOException;

	void writeString(int index, String value) throws IOException;

	void writeBool(int index, boolean value) throws IOException;

	void writeInt(int index, int value) throws IOException;

	void writeDouble(int index, double value) throws IOException;

	void writeLong(int index, long value) throws IOException;

	void writeLongs(int index, Collection<Long> values) throws IOException;

	void writeIntegers(int index, Collection<Integer> values) throws IOException;

	void writeBooleans(int index, Collection<Boolean> values) throws IOException;

	void writeStrings(int index, Collection<String> values) throws IOException;

	void writeByteArrays(int index, Collection<byte[]> values) throws IOException;

	<T extends StreamSerializable> void writeSerializableCollection(int index, Collection<T> values) throws IOException;

	void writeUnmapped(int index, Object value) throws IOException;

	<T extends StreamSerializable> void writeSerializable(int index, T value) throws IOException;

	void writeRaw(byte[] raw) throws IOException;

}
