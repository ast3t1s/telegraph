package im.telegraph.server.test.api;

import im.telegraph.api.parser.RpcParser;
import im.telegraph.rpc.base.Request;
import im.telegraph.rpc.base.Response;
import im.telegraph.protocol.model.ProtoSerializer;
import im.telegraph.protocol.model.mtproto.ProtoStruct;
import im.telegraph.protocol.model.mtproto.rpc.RpcError;
import im.telegraph.protocol.model.mtproto.rpc.RpcInternalError;
import im.telegraph.protocol.model.mtproto.rpc.RpcOk;
import im.telegraph.protocol.model.mtproto.rpc.RpcRequest;
import im.telegraph.server.test.MTProtoCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class ProtoApi {

    private static final Logger log = LoggerFactory.getLogger(ProtoApi.class);

    private static final int DEFAULT_TIMEOUT_CHECK = 15000;

    private static final int DEFAULT_TIMEOUT = 150000;

    private boolean isClosed;

    private final im.telegraph.server.test.MTProto mainProto;

     private final RpcParser rpcParser = new RpcParser();

    private final TreeMap<Long, Long> timeoutTimes = new TreeMap<>();

    private final Map<Long, ProtoStruct> requestedMethods = Collections.synchronizedMap(new HashMap<>());

    private final Map<Long, RpcCallbackWrapper> callbacks = Collections.synchronizedMap(new HashMap<>());

    private TimeoutThread timeoutThread;

    public ProtoApi(Long authKey, byte[] masterKey) {
        this.timeoutThread = new TimeoutThread();
        this.timeoutThread.start();
        this.mainProto = new im.telegraph.server.test.MTProto(authKey, masterKey, new ProtoCallback());
    }

    public boolean isClosed() {
        return this.isClosed;
    }

    public void close() {
        if (!this.isClosed) {
            this.isClosed = true;
            if (this.timeoutThread != null) {
                this.timeoutThread.interrupt();
                this.timeoutThread = null;
            }
            this.mainProto.close();
        }
    }

    public void clearKey() {
        if (mainProto != null) {
            mainProto.eraseKey();
        }
    }


    private <T extends Response> void doRpcCall(Request method, int timeout, RpcCallback<T> callback,
                                                im.telegraph.server.test.MTProto mainproto) {
        if (isClosed) {
            if (callback != null) {
                callback.onError(null);
            }
            return;
        }
        synchronized (callbacks) {
            long rpcId = mainproto.sendMessage(new RpcRequest(method.getHeader(), method.toByteArray()).toByteArray());
            if (callback != null) {
                RpcCallbackWrapper wrapper = new RpcCallbackWrapper(callback);
                callbacks.put(rpcId, wrapper);
                long timeoutTime = System.nanoTime() + timeout * 1000 * 1000L;
                synchronized (timeoutTimes) {
                    while (timeoutTimes.containsKey(timeoutTime)) {
                        timeoutTime++;
                    }
                    timeoutTimes.put(timeoutTime, rpcId);
                    timeoutTimes.notifyAll();
                }
                wrapper.timeoutTime = timeoutTime;
            }
        }
    }

    public <T extends Response> T doRpcCall(Request request) throws im.telegraph.server.test.RpcException {
        return doRpcCall(request, DEFAULT_TIMEOUT);
    }

    public <T extends Response> T doRpcCall(Request request, int timeout) throws im.telegraph.server.test.RpcException {
        return doRpcCall(request, timeout, mainProto);
    }

    public <T extends Response> T doRpcCall(Request request, int timeout, im.telegraph.server.test.MTProto proto) throws im.telegraph.server.test.RpcException {
        final Object waitObj = new Object();
        final Object[] res = new Object[3];
        doRpcCall(request, timeout, new RpcCallback<Response>() {
            @Override
            public void onResult(Response result) {
                synchronized (waitObj) {
                    res[0] = result;
                    res[1] = null;
                    res[2] = null;
                    waitObj.notifyAll();
                }
            }

            @Override
            public void onError(im.telegraph.server.test.RpcException exception) {
                synchronized (waitObj) {
                    res[0] = null;
                    res[1] = exception.getCode();
                    res[2] = exception.getMessage();
                    waitObj.notifyAll();
                }
            }

        }, proto);

        synchronized (waitObj) {
            try {
                waitObj.wait(timeout);
            } catch (InterruptedException e) {
                e.printStackTrace();
                throw new RuntimeException();
            }
        }
        if (res[0] == null) {
            if (res[1] != null) {
                Integer code = (Integer) res[1];
                if (code == 0) {
                    throw new im.telegraph.server.test.RpcTimeoutException();
                } else {
                    throw new im.telegraph.server.test.RpcException("RPC", (Integer) res[1], (String) res[2]);
                }
            } else {
                throw new im.telegraph.server.test.RpcException("RPC", 0, null);
            }
        } else {
            return (T) res[0];
        }
    }

    private class ProtoCallback implements MTProtoCallback {

        @Override
        public void onResponseMessage(long messageId, byte[] content, im.telegraph.server.test.MTProto proto) {

            ProtoStruct struct;
            try {
                struct = ProtoSerializer.readRpcResponsePayload(content);
            } catch (IOException e) {
                e.printStackTrace();
                log.warn("Broken response mid#{}", messageId);
                return;
            }

            RpcCallbackWrapper currentCallback = callbacks.remove(messageId);

            if (struct instanceof RpcOk) {
                RpcOk result = (RpcOk) struct;

                Response response;
                try {
                    response = (Response) rpcParser.read(result.getResponseType(), result.getPayload());
                } catch (Exception e) {
                    e.printStackTrace();
                    currentCallback.callback.onError(new im.telegraph.server.test.RpcTimeoutException());
                    return;
                }
                if (currentCallback == null) {
                    throw new IllegalStateException("Error could not find callback");
                }
                currentCallback.callback.onResult(response);
            } else if (struct instanceof RpcError) {
                RpcError error = (RpcError) struct;
                requestedMethods.remove(messageId);

                currentCallback.callback.onError(new im.telegraph.server.test.RpcException(error.errorTag, error.errorCode, error.userMessage));
            } else if (struct instanceof RpcInternalError) {
                log.warn("Internal RPC exception");
                RpcInternalError e = ((RpcInternalError) struct);
                currentCallback.callback.onError(new im.telegraph.server.test.RpcInternalException());
            } else {
                log.debug("Unknown package#" + messageId);
            }
        }

        @Override
        public void onUpdate(byte[] update) {

        }

    }

    private class TimeoutThread extends Thread {

        public TimeoutThread() {
            setName("Timeout#" + hashCode());
        }

        @Override
        public void run() {
            while (!isClosed) {
                log.debug("Timeout Iteration");
                Long key = null;
                Long id = null;
                synchronized (timeoutTimes) {

                    if (timeoutTimes.isEmpty()) {
                        key = null;
                    } else {
                        try {
                            key = timeoutTimes.firstKey();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    if (key == null) {
                        try {
                            timeoutTimes.wait(DEFAULT_TIMEOUT_CHECK);
                        } catch (InterruptedException e) {
                            // e.printStackTrace();
                        }
                        continue;
                    }

                    long delta = (key - System.nanoTime()) / (1000 * 1000);
                    if (delta > 0) {
                        try {
                            timeoutTimes.wait(delta);
                        } catch (InterruptedException e) {
                            // e.printStackTrace();
                        }
                        continue;
                    }

                    id = timeoutTimes.remove(key);
                    if (id == null) {
                        continue;
                    }
                }

                RpcCallbackWrapper currentCallback;
                synchronized (callbacks) {
                    currentCallback = callbacks.remove(id);
                }
                if (currentCallback != null) {
                    synchronized (currentCallback) {
                        if (currentCallback.isCompleted) {
                            log.debug("RPC #" + id + ": Timeout ignored");
                            return;
                        } else {
                            currentCallback.isCompleted = true;
                        }
                    }
                    log.debug("RPC #" + id + ": Timeout (" + currentCallback.elapsed() + " ms)");
                    currentCallback.callback.onError(new im.telegraph.server.test.RpcTimeoutException());
                } else {
                    log.debug("RPC #" + id + ": Timeout ignored2");
                }
            }
            synchronized (timeoutTimes) {
                for (Map.Entry<Long, Long> entry : timeoutTimes.entrySet()) {
                    RpcCallbackWrapper currentCallback;
                    synchronized (callbacks) {
                        currentCallback = callbacks.remove(entry.getValue());
                    }
                    if (currentCallback != null) {
                        synchronized (currentCallback) {
                            if (currentCallback.isCompleted) {
                                return;
                            } else {
                                currentCallback.isCompleted = true;
                            }
                        }
                        log.debug("RPC #" + entry.getValue() + ": Timeout (" + currentCallback.elapsed() + " ms)");
                        currentCallback.callback.onError(new im.telegraph.server.test.RpcTimeoutException());
                    }
                }
            }
        }

    }

    private class RpcCallbackWrapper {

        public long requestTime = System.currentTimeMillis();

        public boolean isCompleted = false;

        public boolean isConfirmed = false;

        public RpcCallback callback;

        public long timeoutTime;

        private RpcCallbackWrapper(RpcCallback callback) {
            this.callback = callback;
        }

        public long elapsed() {
            return System.currentTimeMillis() - requestTime;
        }

    }

}
