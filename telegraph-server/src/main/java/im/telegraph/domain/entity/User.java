package im.telegraph.domain.entity;

import lombok.Data;
import reactor.util.annotation.Nullable;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import java.util.Optional;

@Data
public class User implements Serializable {

    public static final String ENTITY_NAME = User.class.getSimpleName();

    private static final long serialVersionUID = 1L;

    private final Id id;
    private final String username;
    @Nullable
    private final String displayName;
    private final String email;
    private final String password;
    private final String accessSalt;
    @Nullable
    private final String about;
    private final Role role;
    private final Boolean enabled;
    private final Boolean verified;
    private final String languageCode;
    private final String verificationKey;
    @Nullable
    private final String resetKey;
    @Nullable
    private final Instant resetDate;
    private final Instant createdAt;
    private final Instant updatedAt;
    @Nullable
    private final ProfilePhoto profilePhoto;
    @Nullable
    private final Instant deletedAt;

    /**
     * Represents a user.
     *
     * @param id              User identifier.
     * @param username        Username of the user; must not be null.
     * @param displayName     Display of the user.
     * @param email           Email of the user; must not be null.
     * @param password        Password for the current user; must not be null.
     * @param accessSalt      Access salt of the user account.
     * @param about           Biography of the user; may be null.
     * @param role            Role of the user.
     * @param enabled         True, if the user's account is enabled.
     * @param verified        True, if user's account is verified.
     * @param languageCode    IETF language tag of the user's language.
     * @param verificationKey Verification key of the user; using for email verification.
     * @param resetKey        Reset key of the user; using for password reset.
     * @param resetDate       Date when the user's account password has been reset.
     * @param createdAt       Date when the user's account was created.
     * @param updatedAt       Date when the user has been updated.
     * @param profilePhoto    Profile photo of the user; may be null.
     * @param deletedAt       Date when the user has been deleted.
     */
    @lombok.Builder(builderClassName = "Builder", toBuilder = true)
    User(Id id, String username, @Nullable String displayName, String email, String password, String accessSalt,
         @Nullable String about, Role role, Boolean enabled, Boolean verified, String languageCode,
         @Nullable String verificationKey, @Nullable String resetKey, @Nullable Instant resetDate, Instant createdAt,
         Instant updatedAt, @Nullable ProfilePhoto profilePhoto, @Nullable Instant deletedAt) {
        this.id = id;
        this.username = Objects.requireNonNull(username);
        this.displayName = displayName;
        this.email = Objects.requireNonNull(email);
        this.password = Objects.requireNonNull(password);
        this.accessSalt = Objects.requireNonNull(accessSalt);
        this.about = about;
        this.role = role;
        this.enabled = enabled;
        this.verified = verified;
        this.languageCode = languageCode;
        this.verificationKey = verificationKey;
        this.resetKey = resetKey;
        this.resetDate = resetDate;
        this.createdAt = Objects.requireNonNull(createdAt);
        this.updatedAt = updatedAt;
        this.profilePhoto = profilePhoto;
        this.deletedAt = deletedAt;
    }

    public Id getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getAccessSalt() {
        return accessSalt;
    }

    public Optional<String> getDisplayName() {
        return Optional.ofNullable(displayName);
    }

    public Optional<String> getAbout() {
        return Optional.ofNullable(about);
    }

    public Optional<String> getResetKey() {
        return Optional.ofNullable(resetKey);
    }

    public Optional<Instant> getResetAt() {
        return Optional.ofNullable(resetDate);
    }

    public Optional<ProfilePhoto> getProfilePhoto() {
        return Optional.ofNullable(profilePhoto);
    }

    public Optional<Instant> getDeletedAt() {
        return Optional.ofNullable(deletedAt);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        User user = (User) o;
        return Objects.equals(id, user.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", displayName='" + displayName + '\'' +
                ", email='" + email + '\'' +
                ", about='" + about + '\'' +
                ", role=" + role +
                ", enabled=" + enabled +
                ", verified=" + verified +
                ", languageCode='" + languageCode + '\'' +
                ", resetDate=" + resetDate +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                ", deletedAt=" + deletedAt +
                '}';
    }

    public enum Role {

        ROLE_ADMIN("admin"),

        ROLE_USER("user"),

        UNKNOWN("unknown");

        private final String value;

        Role(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public static Role of(final String value) {
            return switch (value) {
                case "admin" -> ROLE_ADMIN;
                case "user" -> ROLE_USER;
                default -> UNKNOWN;
            };
        }
    }

    public static class Builder {

    }
}
