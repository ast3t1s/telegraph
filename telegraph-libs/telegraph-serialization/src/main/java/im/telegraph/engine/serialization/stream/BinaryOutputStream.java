/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package im.telegraph.engine.serialization.stream;

import im.telegraph.engine.serialization.DataOutputStream;

import java.nio.charset.StandardCharsets;

public class BinaryOutputStream implements DataOutputStream {

	private byte[] buffer = new byte[16];

	private int offset;

	private void expand(int size) {
		int newSize = buffer.length;
		while (newSize < size) {
			newSize = newSize <= 4 ? 8 : newSize << 1;
		}

		byte[] newData = new byte[newSize];
		System.arraycopy(buffer, 0, newData, 0, offset);
		buffer = newData;
	}

	@Override
	public void writeInt32(int value) {
		if (buffer.length <= offset + 4) {
			expand(offset + 4);
		}

		buffer[offset++] = (byte) ((value >> 24) & 0xFF);
		buffer[offset++] = (byte) ((value >> 16) & 0xFF);
		buffer[offset++] = (byte) ((value >> 8) & 0xFF);
		buffer[offset++] = (byte) (value & 0xFF);
	}

	@Override
	public void writeInt64(long value) {
		if (buffer.length <= offset + 8) {
			expand(offset + 8);
		}
		buffer[offset++] = (byte) ((value >> 56) & 0xFF);
		buffer[offset++] = (byte) ((value >> 48) & 0xFF);
		buffer[offset++] = (byte) ((value >> 40) & 0xFF);
		buffer[offset++] = (byte) ((value >> 32) & 0xFF);
		buffer[offset++] = (byte) ((value >> 24) & 0xFF);
		buffer[offset++] = (byte) ((value >> 16) & 0xFF);
		buffer[offset++] = (byte) ((value >> 8) & 0xFF);
		buffer[offset++] = (byte) (value & 0xFF);
	}

	@Override
	public void writeByte(byte v) {
		if (buffer.length <= offset + 1) {
			expand(offset + 1);
		}
		buffer[offset++] = v;
	}

	@Override
	public void writeByte(int value) {
		if (value < 0) {
			throw new IllegalArgumentException("Value must not be negative");
		}
		if (value > 255) {
			throw new IllegalArgumentException("Value must not be more than 255");
		}
		if (buffer.length <= offset + 1) {
			expand(offset + 1);
		}
		buffer[offset++] = (byte) value;
	}

	@Override
	public void writeHeader(long value) {
		while ((value & 0xffffffffffffff80L) != 0L) {
			writeByte((int) ((value & 0x7f) | 0x80));
			value >>>= 7;
		}

		writeByte((int) (value & 0x7f));
	}

	@Override
	public void writeBytes(byte[] data, int offset, int length) {
		if (length > 1024 * 1024) {
			throw new IllegalArgumentException("Unable to write more than 1 MB");
		}
		if (length < 0) {
			throw new IllegalArgumentException("Length must not be negative");
		}
		if (offset < 0) {
			throw new IllegalArgumentException("Offset must not be negative");
		}
		if (offset + length > data.length) {
			throw new IllegalArgumentException("Inconsistent sizes");
		}

		if (buffer.length < this.offset + data.length) {
			expand(this.offset + data.length);
		}
		for (int i = 0; i < length; i++) {
			buffer[this.offset++] = data[i + offset];
		}
	}

	@Override
	public void writeProtoString(String string) {
		byte[] data = string.getBytes(StandardCharsets.UTF_8);
		writeProtoBytes(data);
	}

	@Override
	public void writeProtoBytes(byte[] data) {
		writeHeader(data.length);
		writeBytes(data, 0, data.length);
	}

	@Override
	public byte[] toByteArray() {
		byte[] res = new byte[offset];
		System.arraycopy(buffer, 0, res, 0, offset);
		return res;
	}

}
