package im.telegraph.service;

import im.telegraph.common.ACLHashGenerator;
import im.telegraph.common.DateTimeFactory;
import im.telegraph.common.property.domain.FileProperties;
import im.telegraph.config.TelegraphProperties;
import im.telegraph.domain.entity.FileReferenceId;
import im.telegraph.domain.entity.FileUpload;
import im.telegraph.domain.entity.Id;
import im.telegraph.domain.repository.FileRepository;
import im.telegraph.exceptions.RpcErrors;
import im.telegraph.exceptions.RpcException;
import im.telegraph.file.FileStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.function.TupleUtils;
import reactor.util.annotation.Nullable;
import reactor.util.function.Tuple2;

import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;

@Service
public class FileService {

    private static final Logger log = LoggerFactory.getLogger(FileService.class);

    private final FileRepository fileRepository;
    private final FileStorage fileStoreOperations;
    private final IdGeneratorManager idGenerator;
    private final ACLHashGenerator hashing;
    private final DateTimeFactory dateFactory;
    private final FileProperties fileProperties;

    public FileService(FileRepository fileRepository,
                       FileStorage fileStoreOperations,
                       IdGeneratorManager idGenerator,
                       ACLHashGenerator hashing,
                       DateTimeFactory dateFactory,
                       TelegraphProperties properties) {
        this.fileRepository = fileRepository;
        this.fileStoreOperations = fileStoreOperations;
        this.idGenerator = idGenerator;
        this.hashing = hashing;
        this.dateFactory = dateFactory;
        this.fileProperties = properties.getDomain().getFile();
    }

    /**
     * Get file upload and check its validity.
     *
     * @param fileId         the id of the file
     * @param accessHash the file access hash
     * @param maxSize    the maximum size of the file
     * @return A {@link Mono} where, upon successful completion, emits the {@link FileUpload file}, if present.
     * If an error is received, it is emitted through the {@code Mono}.
     */
    public Mono<FileUpload> withFileLocation(final Id fileId, final long accessHash, final Long maxSize) {
        return fileRepository.findById(fileId)
                .switchIfEmpty(Mono.error(RpcException.create(RpcErrors.FILE_NOT_FOUND)))
                .flatMap(file -> {
                    if (!checkAccessHash(file, accessHash)) {
                        return Mono.error(() -> RpcException.create(RpcErrors.FILE_LOCATION_INVALID));
                    } else if (file.getSize() > maxSize) {
                        return Mono.error(() -> RpcException.create(RpcErrors.FILE_TOO_LARGE));
                    } else {
                        return Mono.just(file);
                    }
                });
    }

    /**
     * Get file download url.
     *
     * @param fileId         the if of the file
     * @param accessHash the file access hash
     * @return A {@link Mono} where, upon successful completion, emits the {@link Tuple2 file}, if present.
     * If an error is received, it is emitted through the {@code Mono}.
     */
    public Mono<FileDownloadUrl> getFileDownloadUrl(final Id fileId, final long accessHash) {
        return fileRepository.findById(fileId)
                .switchIfEmpty(Mono.error(() -> RpcException.create(RpcErrors.FILE_NOT_FOUND)))
                .flatMap(file -> {
                    if (!checkAccessHash(file, accessHash)) {
                        return Mono.error(RpcException.create(RpcErrors.FILE_LOCATION_INVALID));
                    } else {
                        return Mono.just(file);
                    }
                })
                .flatMap(file -> fileStoreOperations.getDownloadFileUrl(file.getUploadKey(), fileProperties.getExpireUrl()))
                .map(TupleUtils.function((url, expire) -> new FileDownloadUrl(url.toExternalForm(), expire)));
    }

    /**
     * Get file upload url.
     *
     * @param expectedSize the file expected size
     * @return A {@link Mono} where, upon successful completion, emits the {@link FileUploadUrl file}, if present.
     * If an error is received, it is emitted through the {@code Mono}.
     */
    public Mono<FileUploadUrl> getFileUploadUrl(final Integer expectedSize) {
        final String uploadKey = formatUploadKey(hashing.randomHash());

        return fileStoreOperations.getFileUploadUrl(uploadKey, fileProperties.getExpireUrl())
                .flatMap(TupleUtils.function((url, expire) -> {

                    final FileUpload fileUpload = FileUpload.builder()
                            .accessSalt(hashing.nextAccessSalt())
                            .uploadKey(uploadKey)
                            .size((long) expectedSize)
                            .createdAt(dateFactory.getClock().instant())
                            .build();

                    return fileRepository.insert(fileUpload)
                            .map(file -> FileUploadUrl.of(file.getUploadKey(), url.toExternalForm(), expire));
                }))
                .onErrorResume(ex -> Mono.error(RpcException.create(RpcErrors.FILE_URL_GENERATE_FAILED)));
    }

    /**
     * Download file by the given file ID.
     *
     * @param fileId the if of the file
     * @return A {@link Flux} that continually emits {@link ByteBuffer chunks} the file chunk. If an
     * error is received, it is emitted through the {@code Flux}.
     */
    public Flux<ByteBuffer> downloadFile(final Id fileId) {
        return fileRepository.findById(fileId)
                .switchIfEmpty(Mono.error(() -> RpcException.create(RpcErrors.FILE_NOT_FOUND)))
                .flatMapMany(file -> fileStoreOperations.downloadFile(file.getUploadKey()));
    }

    /**
     * Upload the given file {@code Path path} to storage.
     *
     * @param fileName the name of the file
     * @param file     the file path to upload
     * @return A {@link Mono} where, upon successful completion, emits the {@link FileReferenceId location}, if present.
     * If an error is received, it is emitted through the {@code Mono}.
     */
    public Mono<FileReferenceId> uploadFile(@Nullable final String fileName, final Path file) {
        return Mono.fromCallable(() -> Files.size(file))
                .flatMap(size -> doUploadFile0(fileName, size, fileStoreOperations.upload(fileName, file)));
    }

    /**
     * Upload the given {@code byte[]} to storage
     *
     * @param fileName the name of the file
     * @param data     the raw data to upload
     * @return A {@link Mono} where, upon successful completion, emits the {@link FileReferenceId location}, if present.
     * If an error is received, it is emitted through the {@code Mono}.
     */
    public Mono<FileReferenceId> uploadFile(@Nullable final String fileName, final byte[] data) {
        return doUploadFile0(fileName, (long) data.length, fileStoreOperations.upload(fileName, data));
    }

    protected Mono<FileReferenceId> doUploadFile0(String fileName, Long expectedSize, Mono<Void> uploadAction) {
        final var fileId = hashing.randomHash();
        final var accessSalt = hashing.nextAccessSalt();

        FileUpload file = FileUpload.builder()
                .id(idGenerator.generateEntityId(FileUpload.ENTITY_NAME))
                .accessSalt(accessSalt)
                .uploadKey(formatFileKey(fileId, fileName))
                .size(expectedSize)
                .createdAt(dateFactory.getClock().instant())
                .build();

        return fileRepository.insert(file)
                .then(uploadAction.thenReturn(file))
                .map(file1 -> new FileReferenceId(file1.getId().asLong(), generateAccessHash(file1)));
    }

    protected long generateAccessHash(FileUpload file) {
        return hashing.generateFileAccessHash(file.getId().asLong(), file.getAccessSalt());
    }

    protected boolean checkAccessHash(FileUpload file, long accessHash) {
        return hashing.generateFileAccessHash(file.getId().asLong(), file.getAccessSalt()) == accessHash;
    }

    /**
     * Format upload key
     */
    private static String formatUploadKey(Long id) {
        return String.format("upload_%d", id);
    }

    /**
     * Format file key
     */
    private static String formatFileKey(Long id, String name) {
        if (StringUtils.hasText(name)) {
            return String.format("file_%d/%s", id, name);
        } else {
            return String.format("file_%d", id);
        }
    }

    public static final class FileDownloadUrl {

        private final String url;
        private final Instant expireTime;

        FileDownloadUrl(String url, Instant expireTime) {
            this.url = url;
            this.expireTime = expireTime;
        }

        public String getUrl() {
            return url;
        }

        public Instant getExpireTime() {
            return expireTime;
        }
    }

    public static final class FileUploadUrl {

        private final String uploadKey;
        private final String url;
        private final Instant expireTime;

        FileUploadUrl(String uploadKey, String url, Instant expireTime) {
            this.uploadKey = uploadKey;
            this.url = url;
            this.expireTime = expireTime;
        }

        public static FileUploadUrl of(String uploadKey, String url, Instant expireTime) {
            return new FileUploadUrl(uploadKey, url, expireTime);
        }

        public String getUploadKey() {
            return uploadKey;
        }

        public String getUrl() {
            return url;
        }

        public Instant getExpireTime() {
            return expireTime;
        }

        @Override
        public String toString() {
            return "FileUploadUrl{" +
                    "uploadKey='" + uploadKey + '\'' +
                    ", url='" + url + '\'' +
                    ", expireTime=" + expireTime +
                    '}';
        }
    }
}
