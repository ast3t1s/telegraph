package im.telegraph.common.health;

import im.telegraph.common.ResettableInterval;
import im.telegraph.common.property.HealthCheckProperties;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

public class HealthTracker {

    private final HealthCheckIndicator indicator;

    private final ResettableInterval intervalCheck;

    public HealthTracker(HealthCheckProperties properties, HealthCheckIndicator indicator) {
        this.indicator = indicator;
        this.intervalCheck = new ResettableInterval(Schedulers.newSingle("health-indicator", true),
                properties.getCheckInterval(), properties.getCheckInterval(),
                tick -> Mono.fromRunnable(indicator::updateHealthStatus));
    }

    public boolean isInstanceHealthy() {
        return this.indicator.isHealthy();
    }

    public void start() {
        this.intervalCheck.start();
    }

    public void stop() {
        this.intervalCheck.stop();
    }
}
