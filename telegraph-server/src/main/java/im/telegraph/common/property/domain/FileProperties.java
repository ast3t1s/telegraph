package im.telegraph.common.property.domain;

import org.springframework.util.unit.DataSize;

import java.time.Duration;

/**
 * @author Ast3t1s
 */
public class FileProperties {

    /**
     * The time of a url to keep available.
     */
    private Duration expireUrl = Duration.ofHours(1);

    /**
     * The size of file's part which has to be uploaded.
     */
    private DataSize maxPartSize = DataSize.ofMegabytes(1);

    public Duration getExpireUrl() {
        return expireUrl;
    }

    public void setExpireUrl(Duration expireUrl) {
        this.expireUrl = expireUrl;
    }

    public DataSize getMaxPartSize() {
        return maxPartSize;
    }

    public void setMaxPartSize(DataSize maxPartSize) {
        this.maxPartSize = maxPartSize;
    }
}
