package im.telegraph.common.rdbms.bind;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;

/**
 * @author Ast3t1s
 */
public class IndexedBindMarkers implements BindMarkers {

    private static final AtomicIntegerFieldUpdater<IndexedBindMarkers> COUNTER_INCREMENTER =
            AtomicIntegerFieldUpdater.newUpdater(IndexedBindMarkers.class, "counter");

    private final int offset;
    private final String prefix;

    // access via COUNTER_INCREMENTER
    @SuppressWarnings("unused")
    private volatile int counter;

    IndexedBindMarkers(String prefix, int beginWith) {
        this.counter = 0;
        this.prefix = prefix;
        this.offset = beginWith;
    }

    @Override
    public BindMarker next() {
        int index = COUNTER_INCREMENTER.getAndIncrement(this);
        return new IndexedBindMarker(prefix + "" + (index + offset), index);
    }

    /**
     * A single indexed bind marker.
     * @author Mark Paluch
     */
    static class IndexedBindMarker implements BindMarker {

        private final String placeholder;
        private final int index;

        IndexedBindMarker(String placeholder, int index) {
            this.placeholder = placeholder;
            this.index = index;
        }

        @Override
        public String getPlaceholder() {
            return placeholder;
        }

        @Override
        public void bind(BindOperation bindOperation, Object value) {
            bindOperation.bind(index, value);
        }

        @Override
        public void bindNull(BindOperation target, Class<?> valueType) {
            target.bindNull(index, valueType);
        }

        public int getIndex() {
            return index;
        }
    }
} 
