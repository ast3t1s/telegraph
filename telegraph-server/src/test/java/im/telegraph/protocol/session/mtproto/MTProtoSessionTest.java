package im.telegraph.protocol.session.mtproto;

import im.telegraph.protocol.DcMTProtoSession;
import im.telegraph.server.TransportConnection;
import im.telegraph.util.ThreadLocalSecureRandom;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Mono;

import java.time.Duration;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MTProtoSessionTest {

    @Test
    public void testInitNativeSession() {
        byte[] masterKey = ThreadLocalSecureRandom.getSeed(256);

        TransportConnection transportSession = mock(TransportConnection.class);

        when(transportSession.close()).thenReturn(Mono.empty());

        DcMTProtoSession session = new DcMTProtoSession(1L, 1L, masterKey);
        session.initializeNativeSession(transportSession);

        assertThat(session.getTimeSinceLastActive()).isNull();

        session.destroy().block();

        assertThat(session.getTimeSinceLastActive()).isNotNull();
    }

    @Test
    public void testResumeNativeSession() {
        byte[] masterKey = ThreadLocalSecureRandom.getSeed(256);

        TransportConnection transportSession = mock(TransportConnection.class);

        when(transportSession.close()).thenReturn(Mono.empty());

        DcMTProtoSession session = new DcMTProtoSession(1L, 1L, masterKey);
        session.initializeNativeSession(transportSession);

        assertThat(session.getTimeSinceLastActive()).isNull();

        session.destroy().block();

        assertThat(session.getTimeSinceLastActive()).isNotNull();

        session.initializeNativeSession(transportSession);

        assertThat(session.getTimeSinceLastActive()).isNull();
    }

    @Test
    public void testCheckSessionExpire() {
        byte[] masterKey = ThreadLocalSecureRandom.getSeed(256);

        TransportConnection transportSession = mock(TransportConnection.class);

        when(transportSession.close()).thenReturn(Mono.empty());

        DcMTProtoSession session = new DcMTProtoSession(1L, 1L, masterKey);
        session.setEmptySessionTtl(Duration.ofSeconds(-1));
        session.initializeNativeSession(transportSession);

        assertThat(session.isExpired()).isFalse();

        session.destroy().block();
    }

    @Test
    public void testSessionIsExpired() throws InterruptedException {
        byte[] masterKey = ThreadLocalSecureRandom.getSeed(256);

        TransportConnection transportSession = mock(TransportConnection.class);

        when(transportSession.close()).thenReturn(Mono.empty());

        DcMTProtoSession session = new DcMTProtoSession(1L, 1L, masterKey);
        session.setEmptySessionTtl(Duration.ofMillis(100)); //Make session ttl is minimum
        session.initializeNativeSession(transportSession);

        assertThat(session.isExpired()).isFalse();

        session.destroy().block();

        Thread.sleep(200);

        assertThat(session.isExpired()).isTrue();
    }
}