package im.telegraph.cluster;

import im.telegraph.cluster.event.ClusterMembershipEvent;
import im.telegraph.cluster.event.ClusterReachabilityEvent;
import im.telegraph.cluster.internal.Heartbeat;
import im.telegraph.cluster.internal.HeartbeatResponse;
import im.telegraph.internal.remote.RemoteTransport;
import im.telegraph.common.ResettableInterval;
import im.telegraph.common.property.cluster.FailureDetectorProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.Disposable;
import reactor.core.Disposables;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Sinks;
import reactor.core.scheduler.Scheduler;

import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * ClusterHeartbeat manages the heartbeat sending and receiving
 * process of cluster node.
 * <p>
 * It doesn't specify that particular node is failed, but just provide information that either it is
 * suspected or trusted at current moment of time. So it is up to cluster membership or other top
 * level component to define when suspected member is actually failed.
 * {@link ClusterHeartbeat#start} Starts running failure detection algorithm.
 * After started it begins to receive and send ping messages.
 * {@link ClusterHeartbeat#stop} Stops running failure detection algorithm and releases occupied resources.
 * </p>
 * @author Ast3t1s
 */
class ClusterHeartbeat {

    private static final Logger log = LoggerFactory.getLogger(ClusterHeartbeat.class);

    private final Disposable.Composite forCleanUp = Disposables.composite();

    private final AtomicLong heartbeatCounter = new AtomicLong(0);

    private final UniqueAddress self;
    private final Scheduler scheduler;
    private final FailureDetector failureDetector;
    private final RemoteTransport transport;
    private final Sinks.Many<ClusterEvent> eventDispatcher;

    private final Duration heartbeatInterval;
    private final ResettableInterval heartbeatTick;

    private volatile Instant heartbeatTimestamp;

    private final AtomicReference<State> state = new AtomicReference<>();

    ClusterHeartbeat(UniqueAddress self, Scheduler scheduler, FailureDetector failureDetector,
                     FailureDetectorProperties properties, RemoteTransport transport,
                     Sinks.Many<ClusterEvent> eventDispatcher) {
        this.self = self;
        this.transport = transport;
        this.scheduler = scheduler;
        this.eventDispatcher = eventDispatcher;
        this.failureDetector = failureDetector;
        this.heartbeatInterval = properties.getHeartbeatInterval();

        this.heartbeatTick = new ResettableInterval(scheduler, heartbeatInterval, heartbeatInterval,
                tick -> Mono.fromRunnable(this::doHeartbeat));

        this.heartbeatTimestamp = Instant.now().plus(heartbeatInterval);
        this.state.set(new State(new HeartbeatNodeRing(self, Collections.singleton(self),
                Collections.emptySet(), properties.getMonitoredMembers()), Collections.emptySet(), failureDetector));

        startListeners();
    }

    void start() {
        heartbeatTick.start();
    }

    void stop() {
        heartbeatTick.stop();
        forCleanUp.dispose();
        heartbeatCounter.set(0);
        failureDetector.reset();
    }

    private void startListeners() {
        forCleanUp.add(transport.receive()
                .publishOn(scheduler)
                .doOnNext(message -> {
                    if (message.getPayload() instanceof Heartbeat) {
                        onHeartbeat((Heartbeat) message.getPayload());
                    } else if (message.getPayload() instanceof HeartbeatResponse) {
                        doHeartbeatRsp((HeartbeatResponse) message.getPayload());
                    }
                })
                .subscribe(null, t -> log.warn("Heartbeat listener terminated with an error", t)));

        forCleanUp.add(eventDispatcher.asFlux()
                .ofType(ClusterMembershipEvent.class)
                .publishOn(scheduler)
                .flatMap(this::handleMembershipChanged)
                .subscribe(null, t -> log.warn("Heartbeat membership listener terminated with and error", t)));

        forCleanUp.add(eventDispatcher.asFlux()
                .ofType(ClusterReachabilityEvent.class)
                .publishOn(scheduler)
                .flatMap(this::handleReachabilityChanged)
                .subscribe(null, t -> log.warn("Heartbeat unreachable listener terminated with and error", t)));
    }

    private void doHeartbeat() {
        state.get().getActiveReceivers().forEach(member -> {
            if (failureDetector.isMonitoring(member.getAddress())) {
                log.info("Cluster Node [{}] - Heartbeat to to [{}]", self, member);
            } else {
                log.info("Cluster Node [{}] - First Heartbeat to [{}]", self, member);
                scheduler.schedule(() -> triggerFirstHeartbeat(member), heartbeatTimestamp.toEpochMilli(), TimeUnit.MILLISECONDS);
            }
            transport.send(member.getAddress(), new Heartbeat(self.getAddress(), heartbeatCounter.incrementAndGet(), System.nanoTime()))
                    .subscribe(null, ex -> log.warn("Unexpected error due sending heartbeat to [{}]", member, ex));
        });
        checkTimeInterval();
    }

    private void triggerFirstHeartbeat(UniqueAddress from) {
        if (state.get().getActiveReceivers().contains(from) && !failureDetector.isMonitoring(from.getAddress())) {
            if (log.isDebugEnabled()) {
                log.debug("Cluster Node [{}] - Trigger extra expected heartbeat from [{}]", self, from);
            }
            failureDetector.heartbeat(from.getAddress());
        }
    }

    private void onHeartbeat(Heartbeat heartbeat) {
        long period = heartbeatCounter.get();
        log.info("[{}] Received heartbeat from: {}", period, heartbeat.getFrom());

        transport.send(heartbeat.getFrom(), new HeartbeatResponse(self, heartbeat.getSeq(), heartbeat.getTimestamp()))
                .subscribe(null, t -> log.warn("[{}] Failed to send heartbeat ack to {}", self, period, t));
    }

    private void doHeartbeatRsp(HeartbeatResponse response) {
        if (log.isDebugEnabled()) {
            log.debug("[{}] Cluster Node [{}] - Heartbeat response from [{}] -Sequence number [{}] - Creation time [{}]",
                    heartbeatCounter.get(), self, response.getFrom(), response.getSeq(), response.getTimestamp());
        }
        State oldState = state.get();
        State newState = oldState.heartbeatRsp(response.getFrom());
        if (!state.compareAndSet(oldState, newState)) {
            doHeartbeatRsp(response);
        }
    }

    private Mono<Void> handleMembershipChanged(ClusterMembershipEvent event) {
        return Mono.fromRunnable(
                () -> {
                    Member member = event.member();
                    log.info("Received membership event in heartbeat {}", event);
                    if (event.isRemoved()) {
                        onMemberRemoved(member);
                        log.info("[{}][{}] Removed {} from heartbeat list (size={})",
                                heartbeatCounter.get(), self, member, state.get().size());
                    }
                    if (event.isAdded()) {
                        if (!member.getUniqueAddress().equals(self)) {
                            onMemberAdded(member);
                            log.info("[{}][{}] Added {} to heartbeat list (size={})",
                                    heartbeatCounter.get(), self, member, state.get().size());
                        }
                    }
                });
    }

    private Mono<Void> handleReachabilityChanged(ClusterReachabilityEvent event) {
        return Mono.fromRunnable(
                () -> {
                    Member member = event.getMember();
                    if (event.isReachable()) {
                        onMemberReachable(member);
                        log.debug("[{}][{}] Marked {} as REACHABLE by failure detector",
                                heartbeatCounter.get(), self, member) ;
                    }
                    if (event.isUnreachable()) {
                        onMemberUnreachable(member);
                        log.debug("[{}][{}] Marked {} as UNREACHABLE by failure detector",
                                heartbeatCounter.get(), self, member) ;
                    }
                });
    }

    private void onMemberAdded(Member member) {
        State oldState = state.get();
        State newState = oldState.addMember(member.getUniqueAddress());
        if (!state.compareAndSet(oldState, newState)) {
            onMemberAdded(member);
        }
    }

    private void onMemberRemoved(Member member) {
        State oldState = state.get();
        State newState = oldState.removeMember(member.getUniqueAddress());
        if (!state.compareAndSet(oldState, newState)) {
            onMemberRemoved(member);
        }
    }

    private void onMemberUnreachable(Member member) {
        State oldState = state.get();
        State newState = oldState.unreachableMember(member.getUniqueAddress());
        if (!state.compareAndSet(oldState, newState)) {
            onMemberUnreachable(member);
        }
    }

    private void onMemberReachable(Member member) {
        State oldState = state.get();
        State newState = oldState.reachableMember(member.getUniqueAddress());
        if (!state.compareAndSet(oldState, newState)) {
            onMemberReachable(member);
        }
    }

    private void checkTimeInterval() {
        Instant now = Instant.now();
        if ((now.getEpochSecond() - heartbeatTimestamp.getEpochSecond()) >= heartbeatInterval.getSeconds() * 2) {
            log.warn("Scheduled sending of heartbeat was delayed. " +
                            "Previous heartbeat was sent [{}] ms ago, expected interval is [{}] ms. This may cause failure detection " +
                            "to mark members as unreachable. The reason can be thread starvation, CPU overload, or GC.",
                    now.toEpochMilli() - heartbeatTimestamp.toEpochMilli(), heartbeatInterval.toMillis());
        }
        heartbeatTimestamp = Instant.now();
    }

    /**
     * State of the node heartbeat. Encapsulated to facilitate unit testing.
     */
    private static class State {

        private final HeartbeatNodeRing ring;
        private final Set<UniqueAddress> oldReceiversNowUnreachable;
        private final FailureDetector failureDetector;
        private final Set<UniqueAddress> activeReceivers;
        
        State(HeartbeatNodeRing ring, Set<UniqueAddress> oldReceiversNowUnreachable, FailureDetector failureDetector) {
            this.ring = ring;
            this.oldReceiversNowUnreachable = oldReceiversNowUnreachable;
            this.failureDetector = failureDetector;
            this.activeReceivers = Stream.concat(ring.getReceivers().stream(), oldReceiversNowUnreachable.stream())
                    .collect(Collectors.toSet());
        }

        Set<UniqueAddress> getActiveReceivers() {
            return Collections.unmodifiableSet(activeReceivers);
        }

        int size() {
            return ring.size();
        }

        State unreachableMember(UniqueAddress member) {
            return new State(ring.unreachable(member), activeReceivers, failureDetector);
        }

        State reachableMember(UniqueAddress member) {
            return new State(ring.reachable(member), activeReceivers, failureDetector);
        }

        State addMember(UniqueAddress member) {
            return membershipChange(ring.addMember(member));
        }

        State removeMember(UniqueAddress member) {
            State newState = membershipChange(ring.removeMember(member));

            failureDetector.remove(member.getAddress());
            if (newState.oldReceiversNowUnreachable.contains(member)) {
                final var adjustedOldReceiversNowUnreachable = new HashSet<>(newState.oldReceiversNowUnreachable);
                adjustedOldReceiversNowUnreachable.remove(member);
                return new State(newState.ring, adjustedOldReceiversNowUnreachable, newState.failureDetector);
            }
            return newState;
        }

        private State membershipChange(HeartbeatNodeRing newRing) {
            final var oldReceivers = ring.getReceivers();
            final var newReceivers = newRing.getReceivers();

            final var removedReceivers = new HashSet<>(oldReceivers);
            removedReceivers.removeAll(newReceivers);

            final var adjustedOldReceiversNowUnreachable = new HashSet<>(oldReceiversNowUnreachable);
            removedReceivers.forEach(member -> {
                if (failureDetector.isAvailable(member.getAddress())) {
                    failureDetector.remove(member.getAddress());
                } else {
                    adjustedOldReceiversNowUnreachable.add(member);
                }
            });

            return new State(newRing, adjustedOldReceiversNowUnreachable, failureDetector);
        }

        State heartbeatRsp(UniqueAddress from) {
            if (activeReceivers.contains(from)) {
                failureDetector.heartbeat(from.getAddress());
                if (oldReceiversNowUnreachable.contains(from)) {
                    if (!ring.getReceivers().contains(from)) {
                        failureDetector.remove(from.getAddress());
                    }
                    final var newReceiversNowUnreachable = new HashSet<>(oldReceiversNowUnreachable);
                    newReceiversNowUnreachable.remove(from);
                    return new State(ring, newReceiversNowUnreachable, failureDetector);
                }
                return this;
            }
            return this;
        }
    }

    /**
     * Immutable collection for picking heartbeat members base on consistent hashing algorithm
     */
    private static class HeartbeatNodeRing {

        private final UniqueAddress self;
        private final Set<UniqueAddress> members;
        private final Set<UniqueAddress> unreachable;
        private final int monitoredByNrOfMembers;

        // Lazy collection to avoid receivers recalculation
        private volatile Set<UniqueAddress> receivers = new LinkedHashSet<>();

        HeartbeatNodeRing(UniqueAddress self, Set<UniqueAddress> members, Set<UniqueAddress> unreachable,
                          int monitoredByNrOfMembers) {
            if (!members.contains(self)) {
                throw new IllegalArgumentException("Member set must contains self address: " + self);
            }
            if (monitoredByNrOfMembers <= 0) {
                throw new IllegalArgumentException("MonitoredByNrOfMembers must be a positive value");
            }
            this.self = self;
            this.members = Collections.unmodifiableSet(members);
            this.unreachable = Collections.unmodifiableSet(unreachable);
            this.monitoredByNrOfMembers = monitoredByNrOfMembers;
        }

        /**
         * Get picked heartbeat receivers using lazy initialization to avoid every pick recalculation.
         *
         * @return the picked heartbeat receivers
         */
        Set<UniqueAddress> getReceivers() {
            if (receivers == null || receivers.isEmpty()) {
                receivers = receivers();
            }
            return receivers;
        }

        private boolean allReceivers() {
            return monitoredByNrOfMembers >= members.size() - 1;
        }

        private Set<UniqueAddress> receivers() {
            final var receivers = new LinkedHashSet<>(allReceivers() ? members : calculateReceivers());
            receivers.remove(self);
            return receivers;
        }

        private Set<UniqueAddress> calculateReceivers() {
            final var nodeRing = new TreeSet<>(members);
            final var result = new LinkedHashSet<UniqueAddress>();

            int remaining = monitoredByNrOfMembers;
            remaining = take(remaining, nodeRing.tailSet(self).iterator(), result);
            if (remaining > 0) {
                take(remaining, nodeRing.headSet(self).iterator(), result);
            }

            return result;
        }

        // Pick nodes from the iterator until n nodes that are not unreachable have been selected.
        // Intermediate unreachable nodes up to `monitoredByNrOfMembers` are also included in the result.
        // The reason for not limiting it to strictly monitoredByNrOfMembers is that the leader must
        // be able to continue its duties (e.g. removal of downed nodes) when many nodes are shutdown
        // at the same time and nobody in the remaining cluster is monitoring some of the shutdown nodes.
        private int take(int limit, Iterator<UniqueAddress> iterator, Set<UniqueAddress> acc) {
            while (iterator.hasNext() && limit > 0) {
                UniqueAddress next = iterator.next();
                boolean isUnreachable = unreachable.contains(next);
                if (isUnreachable && acc.size() >= monitoredByNrOfMembers) {
                    continue;
                } else if (isUnreachable) {
                    acc.add(next);
                } else {
                    acc.add(next);
                    limit--;
                }
            }
            return limit;
        }

        int size() {
            return members.size();
        }

        HeartbeatNodeRing unreachable(UniqueAddress member) {
            if (unreachable.contains(member)) {
                return this;
            }
            HashSet<UniqueAddress> newUnreachable = new HashSet<>(unreachable);
            newUnreachable.remove(member);
            return new HeartbeatNodeRing(self, members, newUnreachable, monitoredByNrOfMembers);
        }

        HeartbeatNodeRing reachable(UniqueAddress member) {
            if (!unreachable.contains(member)) {
                return this;
            }
            HashSet<UniqueAddress> newUnreachable = new HashSet<>(unreachable);
            newUnreachable.remove(member);
            return new HeartbeatNodeRing(self, members, newUnreachable, monitoredByNrOfMembers);
        }

        HeartbeatNodeRing addMember(UniqueAddress member) {
            if (members.contains(member)) {
                return this;
            }
            HashSet<UniqueAddress> newMembers = new HashSet<>(members);
            newMembers.add(member);
            return new HeartbeatNodeRing(self, newMembers, unreachable, monitoredByNrOfMembers);
        }

        HeartbeatNodeRing removeMember(UniqueAddress member) {
            if (!members.contains(member) && !unreachable.contains(member)) {
                return this;
            }
            HashSet<UniqueAddress> newMembers = new HashSet<>(members);
            newMembers.remove(member);
            return new HeartbeatNodeRing(self, newMembers, unreachable, monitoredByNrOfMembers);
        }
    }
} 
