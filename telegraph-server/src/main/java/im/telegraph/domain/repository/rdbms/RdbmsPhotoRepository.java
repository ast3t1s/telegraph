package im.telegraph.domain.repository.rdbms;

import im.telegraph.common.rdbms.RdbmsOperations;
import im.telegraph.domain.entity.FileReferenceId;
import im.telegraph.domain.entity.Id;
import im.telegraph.domain.entity.photo.Photo;
import im.telegraph.domain.entity.photo.PhotoSize;
import im.telegraph.domain.repository.PhotoRepository;
import im.telegraph.engine.serialization.stream.BinaryInputStream;
import im.telegraph.engine.serialization.stream.BinaryOutputStream;
import io.r2dbc.spi.*;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Ast3t1s
 */
@Repository
public class RdbmsPhotoRepository extends AbstractRdbmsRepository implements PhotoRepository {

    private static final String SELECT_CLAUSE = "id, user_id, file_id, photo_small, photo_large, created_at";

    private static final String SAVE_PHOTO_QUERY = "INSERT INTO photos (user_id, file_id, access_salt, photo_small," +
            " photo_large, created_at) VALUES (:userId, :fileId, :accessSalt, :photoSmall, :photoLarge, :createdAt)";

    private static final String LOAD_BY_IDS_QUERY =
            "SELECT " + SELECT_CLAUSE + " FROM photos WHERE id IN (:ids)";

    private static final String GET_PHOTO_QUERY =
            "SELECT " + SELECT_CLAUSE + " from photos WHERE id = :id";

    public static final String GET_PHOTOS_QUERY =
            "SELECT " + SELECT_CLAUSE + " from photos WHERE user_id = :userId ORDER BY id";

    private static final String GET_PHOTO_COUNT_QUERY =
            "SELECT COUNT(*) AS count FROM photos WHERE user_id = :userId";

    private static final String DELETE_PHOTOS_QUERY =
            "DELETE FROM photos WHERE id IN (:ids)";

    private final Function<Photo, Map<String, Object>> photoParameterMapper = new PhotoParameterMapper();
    private final BiFunction<Row, RowMetadata, Photo> photoRowMapper = new PhotoRowMapper();

    private final RdbmsOperations rdbmsOperations;

    public RdbmsPhotoRepository(ConnectionFactory connectionFactory) {
        super(connectionFactory);
        this.rdbmsOperations = RdbmsOperations.create(connectionFactory);
    }

    @Override
    public Mono<Photo> insert(Photo photo) {
        return rdbmsOperations.withHandle(handle -> handle.createUpdate(SAVE_PHOTO_QUERY)
                .bindMap(photoParameterMapper.apply(photo))
                .execute().then(Mono.just(photo)))
                .next();
    }

    @Override
    public Mono<Photo> findById(Id id) {
        return rdbmsOperations.withHandle(handle -> handle.createQuery(GET_PHOTO_QUERY)
                .bind("id", id.asLong())
                .mapRow(photoRowMapper))
                .next();
    }

    @Override
    public Flux<Photo> findAllByIdIn(Collection<Id> ids) {
        return rdbmsOperations.withHandle(handle -> handle.createQuery(LOAD_BY_IDS_QUERY)
                .bind("ids", ids.stream().map(Id::asLong).collect(Collectors.toList()))
                .mapRow(photoRowMapper));
    }

    @Override
    public Flux<Photo> findByUserId(Id userId, int limit, int offset) {
        return rdbmsOperations.withHandle(handle -> handle
                .createQuery(limitQuery(GET_PHOTOS_QUERY, limit, offset))
                .bind("userId", userId.asLong())
                .mapRow(photoRowMapper));
    }

    @Override
    public Mono<Long> countByUserId(Id userId) {
        return rdbmsOperations.withHandle(handle -> handle.createQuery(GET_PHOTO_COUNT_QUERY)
                .bind("userId", userId.asLong())
                .mapRow(row -> row.get(0, Long.class)))
                .next();
    }

    @Override
    public Mono<Long> deleteAllByIdIn(Collection<Id> ids) {
        return rdbmsOperations.withHandle(handle -> handle.createUpdate(DELETE_PHOTOS_QUERY)
                .bind("ids", ids.stream().map(Id::asLong).collect(Collectors.toList()))
                .execute())
                .next();
    }

    private static byte[] serialize(PhotoSize photoSize) {
        final BinaryOutputStream outputStream = new BinaryOutputStream();
        outputStream.writeInt64(photoSize.getFileReferenceId().getFileId());
        outputStream.writeInt64(photoSize.getFileReferenceId().getAccessHash());
        outputStream.writeInt32(photoSize.getWidth());
        outputStream.writeInt32(photoSize.getHeight());
        outputStream.writeInt32(photoSize.getFileSize());
        return outputStream.toByteArray();
    }

    private static PhotoSize deserialize(byte[] bytes) {
        try {
            BinaryInputStream stream = new BinaryInputStream(bytes);
            final var fileId = stream.readInt64();
            final var accessHash = stream.readInt64();
            final var width = stream.readInt();
            final var height = stream.readInt();
            final var fileSize = stream.readInt();
            return new PhotoSize(new FileReferenceId(fileId, accessHash), width, height, fileSize);
        } catch (IOException e) {
            throw new IllegalStateException("Unable to deserialize photo image", e);
        }
    }

    static final class PhotoParameterMapper implements Function<Photo, Map<String, Object>> {

        @Override
        public Map<String, Object> apply(Photo photo) {
            final Map<String, Object> parameters = new HashMap<>();
            parameters.put("id", Parameters.in(R2dbcType.BIGINT, photo.getId().asLong()));
            parameters.put("userId", Parameters.in(R2dbcType.BIGINT, photo.getUserId().asLong()));
            parameters.put("fileId", Parameters.in(R2dbcType.BIGINT, photo.getFileId().asLong()));
            parameters.put("photoSmall", Parameters.in(R2dbcType.BINARY, serialize(photo.getPhotoSmall())));
            parameters.put("photoLarge", Parameters.in(R2dbcType.BINARY, serialize(photo.getPhotoLarge())));
            parameters.put("createdAt", Parameters.in(R2dbcType.TIMESTAMP,
                    LocalDateTime.ofInstant(photo.getCreatedAt(), ZoneOffset.UTC)));
            return parameters;
        }
    }

    static final class PhotoRowMapper implements BiFunction<Row, RowMetadata, Photo> {

        @Override
        public Photo apply(Row row, RowMetadata metadata) {
            final var id = row.get("id", Long.class);
            final var userId = row.get("user_id", Long.class);
            final var fileId = row.get("file_id", Long.class);
            final var accessSalt = row.get("access_salt", String.class);

            PhotoSize small = Optional.ofNullable(row.get("photo_small", byte[].class))
                    .map(RdbmsPhotoRepository::deserialize)
                    .orElse(null);

            PhotoSize big = Optional.ofNullable(row.get("photo_large", byte[].class))
                    .map(RdbmsPhotoRepository::deserialize)
                    .orElse(null);

            final var createdAt = row.get("created_At", LocalDateTime.class);
            return new Photo(Id.of(id), Id.of(userId), Id.of(fileId), accessSalt, small, big,
                    createdAt.toInstant(ZoneOffset.UTC));
        }
    }
}
