package im.telegraph.domain.repository.redis;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import im.telegraph.common.JacksonResources;
import im.telegraph.domain.entity.Id;
import im.telegraph.domain.entity.SessionInfo;
import im.telegraph.domain.repository.SessionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * <pre>
 * +-------------+------------------------------+-------------------------+
 * |   User ID   |               Field          |          Value          |
 * +-------------+------------------------------+-------------------------+
 * |             | status (Key for User Status) |     1 (User Status)     |
 * |             +------------------------------+-------------------------+
 * |             |          1 (Device Type)     |   node-0001 (Node ID)   |
 * |             +------------------------------+-------------------------+
 * |  123456789  |          2 (Device Type)     |   node-0001 (Node ID)   |
 * |             +------------------------------+-------------------------+
 * |             |          3 (Device Type)     |   node-0002 (Node ID)   |
 * |             +------------------------------+-------------------------+
 * |             |                ...           |           ...           |
 * +-------------+------------------------------+-------------------------+
 * </pre>
 * "status" is the fixed hash key of the user status value,
 * and its value is the user status value represented in number.
 * <p>
 * The number (e.g. 1,2,3) represents the online device type,
 * and its value is the node ID that the client connects to.
 *
 * @author Ast3t1s
 */
public class RedisSessionRepository implements SessionRepository {

    private static final Logger log = LoggerFactory.getLogger(RedisSessionRepository.class);

    private static final ObjectMapper MAPPER = new JacksonResources()
            .withMapperFunction(mapper -> mapper.addMixIn(SessionInfo.UserDevice.class, DeviceMixin.class))
            .getObjectMapper();

    private static final String PREFIX = "telegraph:session:";

 //   private final ReactiveRedisTemplate<String, byte[]> redisTemplate;

    private Duration presenceTTL = Duration.ofSeconds(180);

    private final Cache<Id, SessionInfo> cache;

    public RedisSessionRepository() {
       // this.redisTemplate = redisTemplate;
        this.cache = Caffeine.newBuilder()
                .maximumSize(1_000_000)
                .expireAfterWrite(presenceTTL)
                .build();
    }

    public void setTimeToLive(Duration timeToLive) {
        this.presenceTTL = timeToLive;
    }

    @Override
    public Mono<SessionInfo> save(SessionInfo sessionInfo) {
        if (sessionInfo == null) {
            return Mono.empty();
        }

        final String presenceKey = getUserSessionKey(sessionInfo.getUserId());

        Mono<Boolean> setTtl;
        if (this.presenceTTL.getSeconds() > 0) {
          //  setTtl = this.redisTemplate.expire(presenceKey, this.presenceTTL);
        } else {
           // setTtl = this.redisTemplate.persist(presenceKey);
        }

        Map<String, Object> delta = new LinkedHashMap<>();
        delta.put(RedisSessionMapper.USER_ID, sessionInfo.getUserId().asLong());

        sessionInfo.getDevices().forEach((authId, device) -> {
            delta.put(getDeviceKey(String.valueOf(authId)), serialize(device));
        });

        return Mono.empty();
       /* return this.redisTemplate.opsForHash().putAll(presenceKey, new HashMap<>(delta)).then(setTtl)
                .thenReturn(sessionInfo);*/
    }

    private static String serialize(SessionInfo.UserDevice userDevice) {
        try {
            return MAPPER.writeValueAsString(userDevice);
        } catch (Exception e) {
            throw new IllegalStateException("Error serialize session device", e);
        }
    }

    private static SessionInfo.UserDevice deserialize(String source) {
        try {
            return MAPPER.readValue(source, SessionInfo.UserDevice.class);
        } catch (Exception e) {
            throw new IllegalStateException("Error deserialize session device", e);
        }
    }

    @Override
    public Mono<SessionInfo> findById(Id userId) {
        final String presenceKey = getUserSessionKey(userId);

        return Mono.justOrEmpty(cache.getIfPresent(userId))
                .switchIfEmpty(Mono.defer(() -> fetchUserSessionInfo(presenceKey)))
                .doOnNext(sessionInfo -> cache.put(userId, sessionInfo));
    }

    private Mono<SessionInfo> fetchUserSessionInfo(String key) {
      /*  return this.redisTemplate.opsForHash().entries(key)
                .collectMap((e) -> e.getKey().toString(), Map.Entry::getValue)
                .filter((map) -> !map.isEmpty())
                .map(new RedisSessionMapper());*/
        return Mono.empty();
    }

    @Override
    public Mono<Void> delete(Id userId) {
        final String presenceKey = getUserSessionKey(userId);
        return Mono.empty();
      //  return this.redisTemplate.delete(presenceKey).then();
    }

    private static String getUserSessionKey(final Id id) {
        return PREFIX + id;
    }

    private static String getDeviceKey(final String key) {
        return RedisSessionMapper.DEVICE_PREFIX + key;
    }

    static final class RedisSessionMapper implements Function<Map<String, Object>, SessionInfo> {

        /**
         * The key in the hash representing {@link SessionInfo#getUserId()}
         */
        static final String USER_ID = "userId";

        /**
         * The prefix of the key in the hash used for devices. For example device:authIdm, device:nodeId, device:expireAt/
         */
        static final String DEVICE_PREFIX = "device:";

        @Override
        public SessionInfo apply(Map<String, Object> map) {
            if (map.isEmpty()) {
                throw new IllegalArgumentException("map must not be empty");
            }
            Integer userId = (Integer) map.get(USER_ID);
            if (userId == null) {
                handleMissingStatus(USER_ID);
            }

            HashMap<Long, SessionInfo.UserDevice> devices = new HashMap<>();
            map.forEach((name, value) -> {
                if (name.startsWith(DEVICE_PREFIX)) {
                    Long authId = Long.parseLong(name.substring(DEVICE_PREFIX.length()));
                    devices.put(authId, deserialize((String) value));
                }
            });

            return new SessionInfo(Id.of(userId), devices);
        }

        private void handleMissingStatus(final String key) {
            throw new IllegalStateException(key + " key must not be null");
        }
    }

    private static abstract class DeviceMixin {

        @JsonCreator
        DeviceMixin(@JsonProperty("auth_id") long authId,
                    @JsonProperty("node") String node,
                    @JsonProperty("expire_at") Instant expireAt) {
        }
    }
}
