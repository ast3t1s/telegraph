package im.telegraph.modules.contacts;

import im.telegraph.common.DateTimeFactory;
import im.telegraph.common.OffsetPageRequest;
import im.telegraph.domain.entity.Contact;
import im.telegraph.domain.entity.Contacts;
import im.telegraph.domain.entity.Id;
import im.telegraph.domain.repository.ContactsRepository;
import im.telegraph.exceptions.RpcException;
import im.telegraph.service.ContactsService;
import im.telegraph.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ContactsServiceTest {

    @Mock
    private ContactsRepository contactsRepository;

    @Mock
    private UserService userService;

    private final DateTimeFactory timeFactory = new DateTimeFactory();

    private ContactsService relationsService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        this.relationsService = new ContactsService(this.contactsRepository, userService, this.timeFactory);
    }

    @Test
    void createOutboundRequest_shouldSave() {
        final Id firstUser = Id.of(1);
        final Id secondUser = Id.of(2);

        final Contact outRequest = new Contact(firstUser, secondUser, Contact.Status.RECV_FROM, timeFactory.getClock().instant());
        final Contact inRequest = new Contact(secondUser, firstUser, Contact.Status.RECV_TO, timeFactory.getClock().instant());

       /* when(this.contactsRepository.findByUserIdAndContactId(any(UserId.class), any(UserId.class))).thenReturn(Mono.empty());

        when(this.contactsRepository.save(outRequest)).thenReturn(Mono.just(outRequest));
        when(this.contactsRepository.save(inRequest)).thenReturn(Mono.just(inRequest));*/

        StepVerifier.create(this.relationsService.addContact(firstUser, secondUser))
                .expectSubscription()
               // .expectNext(true)
                .verifyComplete();

        verify(this.contactsRepository, times(1)).insert(outRequest);
        verify(this.contactsRepository, times(1)).insert(inRequest);
        verify(this.contactsRepository, times(2)).insert(any(Contact.class));
        verify(this.contactsRepository, times(2))
                .findByUserIdAndContactId(any(Id.class), any(Id.class));
        verifyNoMoreInteractions(this.contactsRepository);
    }

    @Test
    void createOutbound_ExistsRequest() {
        final Id firstUser = Id.of(1);
        final Id secondUser = Id.of(2);

        final Contact outRequest = new Contact(firstUser, secondUser, Contact.Status.RECV_FROM, timeFactory.getClock().instant());
        final Contact inRequest = new Contact(secondUser, firstUser, Contact.Status.RECV_TO, timeFactory.getClock().instant());

        when(this.contactsRepository.findByUserIdAndContactId(firstUser, secondUser)).thenReturn(Mono.just(outRequest));
        when(this.contactsRepository.findByUserIdAndContactId(secondUser, firstUser)).thenReturn(Mono.just(inRequest));

        StepVerifier.create(this.relationsService.addContact(firstUser, secondUser))
                .expectSubscription()
                .expectError(RpcException.class)
                .verify();

        verify(this.contactsRepository, times(1)).findByUserIdAndContactId(firstUser, secondUser);
        verify(this.contactsRepository, times(1)).findByUserIdAndContactId(secondUser, firstUser);
        verify(this.contactsRepository, times(2)).findByUserIdAndContactId(any(Id.class), any(Id.class));
        verify(this.contactsRepository, times(0)).insert(any(Contact.class));
        verifyNoMoreInteractions(this.contactsRepository);
    }

    @Test
    void approveInboundRequest() {
        final Id firstUser = Id.of(1);
        final Id secondUser = Id.of(2);

        final Contact outRequest = new Contact(firstUser, secondUser, Contact.Status.RECV_FROM, timeFactory.getClock().instant());
        final Contact inRequest = new Contact(secondUser, firstUser, Contact.Status.RECV_TO, timeFactory.getClock().instant());

        when(this.contactsRepository.findByUserIdAndContactId(firstUser, secondUser)).thenReturn(Mono.just(outRequest));
        when(this.contactsRepository.findByUserIdAndContactId(secondUser, firstUser)).thenReturn(Mono.just(inRequest));

        when(this.contactsRepository.insert(outRequest)).thenReturn(Mono.just(outRequest.withStatus(Contact.Status.BOTH)));
        when(this.contactsRepository.insert(inRequest)).thenReturn(Mono.just(inRequest.withStatus(Contact.Status.BOTH)));

        StepVerifier.create(this.relationsService.acceptContact(firstUser, secondUser))
                .expectSubscription()
              //  .expectNext(true)
                .verifyComplete();

        verify(this.contactsRepository, times(1)).findByUserIdAndContactId(firstUser, secondUser);
        verify(this.contactsRepository, times(1)).findByUserIdAndContactId(secondUser, firstUser);
        verify(this.contactsRepository, times(2)).findByUserIdAndContactId(any(Id.class), any(Id.class));
        verify(this.contactsRepository, times(1)).insert(outRequest);
        verify(this.contactsRepository, times(1)).insert(inRequest);
        verify(this.contactsRepository, times(2)).insert(any(Contact.class));
        verifyNoMoreInteractions(this.contactsRepository);
    }

    @Test
    void approveRequest_NonExists() {
        final Id firstUser = Id.of(1);
        final Id secondUser = Id.of(2);

        when(this.contactsRepository.findByUserIdAndContactId(firstUser, secondUser)).thenReturn(Mono.empty());
        when(this.contactsRepository.findByUserIdAndContactId(secondUser, firstUser)).thenReturn(Mono.empty());

        StepVerifier.create(relationsService.acceptContact(firstUser, secondUser))
                .expectSubscription()
                .expectError(RpcException.class)
                .verify();

        verify(this.contactsRepository, times(1)).findByUserIdAndContactId(firstUser, secondUser);
        verify(this.contactsRepository, times(1)).findByUserIdAndContactId(secondUser, firstUser);
        verify(this.contactsRepository, times(2)).findByUserIdAndContactId(any(Id.class), any(Id.class));
        verifyNoMoreInteractions(this.contactsRepository);
    }

    @Test
    void testRemoveValidRelation() {
        final Id firstUser = Id.of(1);
        final Id secondUser = Id.of(2);

        final Contact first = new Contact(firstUser, secondUser, Contact.Status.BOTH, timeFactory.getClock().instant());
        final Contact second = new Contact(secondUser, firstUser, Contact.Status.BOTH, timeFactory.getClock().instant());

        when(this.contactsRepository.findByUserIdAndContactId(firstUser, secondUser)).thenReturn(Mono.just(first));
        when(this.contactsRepository.findByUserIdAndContactId(secondUser, firstUser)).thenReturn(Mono.just(second));

        when(this.contactsRepository.deleteByUserIdAndContactId(first.getUserId(), first.getContactId()))
                .thenReturn(Mono.empty());

        when(this.contactsRepository.insert(second))
                .thenReturn(Mono.just(second.withStatus(Contact.Status.REMOVE)));

        StepVerifier.create(relationsService.deleteContact(firstUser, secondUser))
                .expectSubscription()
              //  .expectNext(true)
                .expectComplete()
                .verify();

        verify(this.contactsRepository, times(1)).findByUserIdAndContactId(firstUser, secondUser);
        verify(this.contactsRepository, times(1)).findByUserIdAndContactId(secondUser, firstUser);
        verify(this.contactsRepository, times(2)).findByUserIdAndContactId(any(Id.class), any(Id.class));
        verify(this.contactsRepository, times(1)).deleteByUserIdAndContactId(any(Id.class), any(Id.class));
        verify(this.contactsRepository, times(1)).insert(second);
        verify(this.contactsRepository, times(1)).insert(any(Contact.class));
        verifyNoMoreInteractions(this.contactsRepository);
    }

    @Test
    void testRemoveNonExistsRelation() {
        final Id firstUser = Id.of(1);
        final Id secondUser = Id.of(2);

        when(this.contactsRepository.findByUserIdAndContactId(firstUser, secondUser)).thenReturn(Mono.empty());
        when(this.contactsRepository.findByUserIdAndContactId(secondUser, firstUser)).thenReturn(Mono.empty());

        StepVerifier.create(relationsService.deleteContact(firstUser, secondUser))
                .expectSubscription()
                .expectError(RpcException.class)
                .verify();

        verify(this.contactsRepository, times(1)).findByUserIdAndContactId(firstUser, secondUser);
        verify(this.contactsRepository, times(1)).findByUserIdAndContactId(secondUser, firstUser);
        verify(this.contactsRepository, times(2)).findByUserIdAndContactId(any(Id.class), any(Id.class));
        verify(this.contactsRepository, times(0)).deleteByUserIdAndContactId(any(Id.class), any(Id.class));
        verify(this.contactsRepository, times(0)).insert(any(Contact.class));
        verifyNoMoreInteractions(this.contactsRepository);
    }

    @Test
    void testGetUserRelations() {
        final Id firstUser = Id.of(1);

        List<Contact> contactsList = IntStream.range(1, 100)
                .boxed().map(id -> new Contact(firstUser, Id.of(id), Contact.Status.BOTH, timeFactory.getClock().instant()))
                .collect(Collectors.toList());

        Contacts contacts = Contacts.of(contactsList);
        final OffsetPageRequest page = OffsetPageRequest.of(50, 0);

      /*  when(this.contactsRepository.findAllByUserId(firstUser, page))
                .thenReturn(Flux.fromIterable(contacts));*/

       // when(this.contactsRepository.countByUserId(firstUser)).thenReturn(Mono.just(contactsList.size()));

        List<Contact> loaded = this.relationsService.getContacts(firstUser).block();
        assertThat(loaded).isNotNull();
        assertThat(loaded).hasSize(contactsList.size());
        assertThat(loaded).containsAll(contacts);

      //  verify(this.contactsRepository, times(1)).findAllByUserId(firstUser, page);
        verify(this.contactsRepository, times(1)).countByUserId(firstUser);
        verifyNoMoreInteractions(this.contactsRepository);
    }

}