package im.telegraph.api.rpc;

import static im.telegraph.engine.serialization.StreamUtils.*;

import im.telegraph.engine.serialization.StreamReader;
import im.telegraph.engine.serialization.StreamWriter;
import im.telegraph.engine.serialization.io.IOContext;
import im.telegraph.rpc.base.Request;
import java.io.IOException;
import java.lang.Override;
import java.lang.String;

/**
 * Generated by the Telegraph API Scheme generator. DO NOT EDIT!
 * @author Telegraph Scheme Generator
 */
public class RequestContactsSearchContacts extends Request<ResponseContactsSearchContacts> {
    public static final int CLASS_ID = 0x70;

    protected String request;

    protected int limit;

    public RequestContactsSearchContacts() {
    }

    public RequestContactsSearchContacts(String request, int limit) {
        this.request = request;
        this.limit = limit;
    }

    public static RequestContactsSearchContacts fromBytes(byte[] data) throws IOException {
        return IOContext.fromByteArray(new RequestContactsSearchContacts(), data);
    }

    @Override
    public int getHeader() {
        return CLASS_ID;
    }

    public String getRequest() {
        return this.request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public int getLimit() {
        return this.limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    @Override
    public void serialize(StreamWriter writer) throws IOException {
        if (this.request == null) {
            throw new IOException();
        }
        writer.writeString(1, this.request);
        writer.writeInt(2, this.limit);
    }

    @Override
    public void deserialize(StreamReader reader) throws IOException {
        this.request = reader.readString(1);
        this.limit = reader.readInt(2);
    }

    @Override
    public String toString() {
        String res = "rpc SearchContacts{";
        res += "request=" + this.request;
        res += ", limit=" + this.limit;
        res += "}";
        return res;
    }
}
