package im.telegraph.server.test.schedule;

import im.telegraph.protocol.model.mtproto.MTRpcRequest;
import im.telegraph.protocol.model.mtproto.MessageAck;
import im.telegraph.protocol.model.mtproto.ProtoMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class SchedullerV2 {

    private static final Logger log = LoggerFactory.getLogger(SchedullerV2.class);

    private static final int ACK_THRESHOLD = 10;

    private static final int ACK_DELAY = 10 * 1000;

    private static final int MAX_WORKLOAD_SIZE = 1024;

    private final Map<Long, ProtoMessage> unsentPackages = Collections.synchronizedMap(new LinkedHashMap<>());

    private final Set<Long> confirm = new HashSet<>();

    private final Set<Long> pendingConfig = new HashSet<>();

    public synchronized long postPackage(SendMessage sendMessage) {
        ProtoMessage protoMessage = new ProtoMessage(sendMessage.mid,
                new MTRpcRequest(sendMessage.message).toByteArray());
        unsentPackages.put(protoMessage.getMessageId(), protoMessage);
        return protoMessage.getMessageId();
    }

    public synchronized void forgetMessage(long id) {
        unsentPackages.remove(id);
    }

    public synchronized PreparedPackage doSchedule() {

        List<ProtoMessage> messages = new ArrayList<>(unsentPackages.values());
        if (messages.size() > 0) {
            /*
             * if (confirm.size() > 0) { if (log.isDebugEnabled()) {
             * log.debug("Sending acks in package"); } messages.add(0, new
             * ProtoMessage(idHelper.nextId(), buildAck().toByteArray())); }
             */
            ProtoMessage protoMessage = messages.get(0);
            unsentPackages.clear();
            return new PreparedPackage(protoMessage.toByteArray(), 0, protoMessage.toByteArray().length);
        }
        /*
         * if (messages.size() == 1) { ProtoMessage message = messages.get(0); byte[]
         * payload = message.toByteArray(); return new PreparedPackage(payload, 0,
         * payload.length); } else if (messages.size() > 1) { ArrayList<ProtoMessage>
         * protoMessages = new ArrayList<>(); int currentPayload = 0; for (int i = 0; i <
         * messages.size(); i++) { ProtoMessage message = messages.get(i); currentPayload
         * += message.getPayload().length; protoMessages.add(message); /*if
         * (currentPayload > MAX_WORKLOAD_SIZE) { Container container = new
         * Container(protoMessages.toArray(new ProtoMessage[protoMessages.size()]));
         * byte[] payload = new ProtoMessage(idHelper.nextId(),
         * container.toByteArray()).toByteArray(); protoMessages.clear(); currentPayload =
         * 0; return new PreparedPackage(payload, 0, payload.length); }
         */
        // }
        /*
         * if (protoMessages.size() > 0) { Container container = new
         * Container(protoMessages.toArray(new ProtoMessage[protoMessages.size()]));
         * byte[] payload = new ProtoMessage(idHelper.nextId(),
         * container.toByteArray()).toByteArray(); return new PreparedPackage(payload, 0,
         * payload.length); } }
         */
        return null;
    }

    private MessageAck buildAck() {
        long[] ids = new long[confirm.size()];
        Long[] ids2 = confirm.toArray(new Long[confirm.size()]);

        String acks = "";
        for (int i = 0; i < ids.length; i++) {
            ids[i] = ids2[i];
            if (log.isDebugEnabled()) {
                if (acks.length() != 0) {
                    acks += ",";
                }
                acks += "#" + ids2[i];
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("Sending acks " + acks);
        }

        pendingConfig.addAll(confirm);
        confirm.clear();
        return new MessageAck(ids);
    }

    public static class SendMessage {

        public long mid;

        public byte[] message;

        public boolean isRPC;

        public SendMessage(long mid, byte[] message) {
            this.mid = mid;
            this.message = message;
        }

    }

}
