package im.telegraph.common;

import im.telegraph.common.property.TlsProperties;
import im.telegraph.util.KeyStoreUtils;
import io.netty.handler.ssl.OpenSsl;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.SslProvider;
import lombok.experimental.UtilityClass;
import org.springframework.util.ResourceUtils;

import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Arrays;

/**
 * And SSL Helper class.
 *
 * @author Ast3t1s
 */
@UtilityClass
public class SSLFactory {

    /**
     * Build netty ssl server context.
     *
     * @param properties the ssl properties
     * @return the {@link SslContext} instance.
     */
    public static SslContext buildSslServerContext(final TlsProperties properties) {
        try {
            KeyStore keyStore = KeyStoreUtils.createKeyStore(properties.getKeyStoreType(),
                    ResourceUtils.getURL(properties.getKeyStore()).openStream(), properties.getKeyStorePassword());
            String alias = keyStore.aliases().nextElement();
            Certificate certificate = keyStore.getCertificate(alias);
            KeyStore.Entry entry = keyStore.getEntry(alias,
                    new KeyStore.PasswordProtection(properties.getKeyStorePassword().toCharArray()));
            PrivateKey privateKey = ((KeyStore.PrivateKeyEntry) entry).getPrivateKey();

            return SslContextBuilder.forServer(privateKey, (X509Certificate) certificate)
                    .sslProvider(OpenSsl.isAvailable() ? SslProvider.OPENSSL : SslProvider.JDK)
                    .protocols(properties.getEnabledProtocols())
                    .ciphers(Arrays.asList(properties.getCiphers()))
                    .build();
        } catch (Exception e) {
            throw new RuntimeException("Error load ssl certificate from classpath storage", e);
        }
    }
} 
