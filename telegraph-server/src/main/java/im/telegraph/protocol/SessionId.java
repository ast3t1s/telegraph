package im.telegraph.protocol;

import org.springframework.util.ConcurrentReferenceHashMap;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuples;

import java.io.Serializable;
import java.util.Map;
import java.util.Objects;

/**
 * A container to express a MTProto session ID.
 */
public final class SessionId implements Comparable<SessionId>, Serializable {

    private static final Map<Tuple2<Long, Long>, SessionId> CACHE = new ConcurrentReferenceHashMap<>();

    private final long authId;
    private final long sessionId;

    private SessionId(long authId, long sessionId) {
        this.authId = authId;
        this.sessionId = sessionId;
    }

    /**
     * Create instance of {@link SessionId}. Note: avoid to create a new object every time using cache map.
     *
     * @param authId the auth ID.
     * @param sessionId the session ID.
     * @return the instance of {@link SessionId}.
     */
    public static SessionId create(long authId, long sessionId) {
        return CACHE.computeIfAbsent(Tuples.of(authId, sessionId), t2 -> new SessionId(t2.getT1(), t2.getT2()));
    }

    /**
     * Returns the auth ID represented by this {@link SessionId}.
     *
     * @return the auth ID.
     */
    public long getAuthId() {
        return authId;
    }

    /**
     * Returns the session ID represented by this {@link SessionId}.
     *
     * @return the session ID.
     */
    public long getSessionId() {
        return sessionId;
    }

    /**
     * Returns this {@link SessionId} object in array representation: [authId, sessionId].
     *
     * @return an array with 2 elements, {@code authId} and {@code sessionId}.
     */
    public long[] asArray() {
        return new long[] {authId, sessionId};
    }

    /**
     * Return a string form of this {@link SessionId} using the following pattern: {@code authId,sessionId}
     *
     * @return a formatted string representing this object
     */
    public String format() {
        return authId + "," + sessionId;
    }

    @Override
    public int compareTo(SessionId o) {
        return format().compareTo(o.format());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SessionId sessionId1 = (SessionId) o;
        return authId == sessionId1.authId && sessionId == sessionId1.sessionId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(authId, sessionId);
    }

    @Override
    public String toString() {
        return "SessionId{" +
                "authId=" + authId +
                ", sessionId=" + sessionId +
                '}';
    }
}
