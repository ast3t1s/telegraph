package im.telegraph.util;

import org.reactivestreams.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;

import java.util.Comparator;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiFunction;
import java.util.function.IntFunction;
import java.util.function.ToIntFunction;
import java.util.function.ToLongFunction;

public final class PaginationUtils {

    private static final Logger log = LoggerFactory.getLogger(PaginationUtils.class);

    public static final int DEFAULT_PAGE_SIZE = 100;

    public static <T> Flux<T> loadPageAsc(final BiFunction<Integer, Integer, Flux<T>> nextPage,
                                          final ToLongFunction<T> keyExtractor,
                                          final int start, final long end) {
        return loadPage(nextPage, keyExtractor, start, end, DEFAULT_PAGE_SIZE, false);
    }

    public static <T> Flux<T> paginate(BiFunction<Integer, Integer, Flux<T>> nextPage, final int start,
                                       final long end) {
        final AtomicInteger offset = new AtomicInteger(start);

        return Flux.defer(() -> nextPage.apply(offset.get(), DEFAULT_PAGE_SIZE))
                .collectList()
                .doOnNext(slice -> {
                    offset.addAndGet(slice.size());
                    if (log.isDebugEnabled()) {
                        log.debug("Page loaded offset: {} count: {}", offset.get(), end);
                    }
                })
                .flatMapMany(Flux::fromIterable)
                .repeat(() -> offset.get() != end);
    }

    public static <T> Flux<T> paginate(IntFunction<? extends Publisher<T>> prod, ToIntFunction<T> countExtractor, int offset, int limit) {
        AtomicInteger localOffset = new AtomicInteger(offset);
        AtomicInteger count = new AtomicInteger(-1);

        return Flux.defer(() -> prod.apply(localOffset.get()))
                .doOnNext(c -> {
                    count.set(countExtractor.applyAsInt(c));
                    localOffset.addAndGet(limit);
                })
                .repeat(() -> count.get() > localOffset.get());
    }

    public static <T> Flux<T> loadPage(final BiFunction<Integer, Integer, Flux<T>> nextPage,
                                       final ToLongFunction<T> keyExtractor,
                                       final int start, final long end, final int pageSize, final boolean reverse) {

        final Comparator<T> comparator = Comparator.comparingLong(keyExtractor);
        final AtomicInteger offset = new AtomicInteger(start);

        return Flux.defer(() -> nextPage.apply(offset.get(), pageSize))
                .sort(reverse ? comparator.reversed() : comparator)
                .collectList()
                .doOnNext(list -> {
                    offset.addAndGet(list.size());
                    if (log.isDebugEnabled()) {
                        log.debug("Page loaded offset: {} count: {}", offset.get(), end);
                    }
                })
                .flatMapMany(Flux::fromIterable)
                .repeat(() -> offset.get() != end);
    }
} 
