package im.telegraph.domain.repository.rdbms;

import im.telegraph.common.rdbms.RdbmsOperations;
import im.telegraph.domain.entity.AuthKey;
import im.telegraph.domain.repository.AuthKeyRepository;
import io.r2dbc.spi.*;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * @author Ast3t1s
 */
@Repository
public class RdbmsAuthKeyRepository implements AuthKeyRepository {

    private static final String FIELDS = "id, public_key_hash, created_at, deleted_at";

    private static final String SAVE_KEY_QUERY =
            "INSERT INTO auth_keys (" + FIELDS + ") VALUES (:authId, :publicKey, :createdAt, :deletedAt)";

    private static final String UPDATE_KEY_QUERY =
            "UPDATE auth_keys SET deleted_at = :deletedAt";

    private static final String LOAD_AUTH_KEY_QUERY =
            "SELECT " + FIELDS + " FROM auth_keys WHERE id = :authId AND deleted_at IS NULL";

    private static final String DELETE_KEY_QUERY =
            "DELETE FROM auth_keys WHERE id = :authKeyId";

    private final RdbmsOperations rdbmsOperations;

    private final Function<AuthKey, Map<String, Parameter>> authKeyParameterMapper = new AuthKeyParameterMapper();

    public RdbmsAuthKeyRepository(ConnectionFactory connectionFactory) {
        this.rdbmsOperations = RdbmsOperations.create(connectionFactory);
    }

    @Override
    public Mono<AuthKey> insert(AuthKey authKey) {
        return rdbmsOperations.withHandle(handle -> handle.createUpdate(SAVE_KEY_QUERY)
                .bindMap(authKeyParameterMapper.apply(authKey))
                .execute().then(Mono.just(authKey)))
                .last();
    }

    @Override
    public Mono<AuthKey> update(AuthKey authKey) {
        return rdbmsOperations.withHandle(handle -> handle.createUpdate(UPDATE_KEY_QUERY)
                .bindMap(authKeyParameterMapper.apply(authKey))
                .execute())
                .then(Mono.just(authKey));
    }

    @Override
    public Mono<AuthKey> findByAuthIdAndDeletedAtIsNull(Long authId) {
        return rdbmsOperations.withHandle(handle -> handle.createQuery(LOAD_AUTH_KEY_QUERY)
                .bind("authId", authId)
                .mapRow(new AuthKeyRowMapper()))
                .next();
    }

    @Override
    public Mono<Boolean> deleteById(Long authKeyId) {
        return rdbmsOperations.withHandle(handle -> handle.createUpdate(DELETE_KEY_QUERY)
                .bind("authKeyId", authKeyId)
                .execute()
                .map(updatedCount -> updatedCount > 0)).next();
    }

    private static class AuthKeyParameterMapper implements Function<AuthKey, Map<String, Parameter>> {

        @Override
        public Map<String, Parameter> apply(AuthKey authKey) {
            final Map<String, Parameter> parameters = new HashMap<>();
            parameters.put("authId",
                    Parameters.in(R2dbcType.BIGINT, authKey.getId()));
            parameters.put("publicKey",
                    Parameters.in(R2dbcType.BINARY, authKey.getPublicKeyHash()));
            parameters.put("createdAt",
                    Parameters.in(R2dbcType.TIMESTAMP, authKey.getCreatedAt()));
            parameters.put("deletedAt",
                    Parameters.in(R2dbcType.TIMESTAMP,
                            authKey.getDeletedAt().map(ts -> LocalDateTime.ofInstant(ts, ZoneOffset.UTC)).orElse(null)));
            return parameters;
        }
    }

    private static class AuthKeyRowMapper implements BiFunction<Row, RowMetadata, AuthKey> {

        @Override
        public AuthKey apply(Row row, RowMetadata metadata) {
            final var authId = row.get("id", Long.class);
            final var publicKeyHash = row.get("public_key_hash", byte[].class);
            final var createdAt = row.get("created_at", LocalDateTime.class).toInstant(ZoneOffset.UTC);
            final var deletedAt = Optional.ofNullable(row.get("deleted_at", LocalDateTime.class))
                    .map(time -> time.toInstant(ZoneOffset.UTC)).orElse(null);
            return new AuthKey(authId, publicKeyHash, createdAt, deletedAt);
        }
    }
}
