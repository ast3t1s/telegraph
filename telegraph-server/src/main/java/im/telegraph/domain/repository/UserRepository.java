package im.telegraph.domain.repository;

import im.telegraph.domain.entity.Id;
import im.telegraph.domain.entity.User;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * @author Ast3t1s
 */
public interface UserRepository {

    /**
     * Creates a new User object.
     *
     * @param user the User object to create
     * @return A {@link Mono} where, upon successful completion, emits the {@link User}.If an error is received,
     * it is emitted through the {@code Mono}.
     */
    Mono<User> insert(User user);

    /**
     * Updates an existing User object.
     *
     * @param user the User object to update
     * @return A {@link Mono} where, upon successful completion, emits the {@link User}.If an error is received,
     * it is emitted through the {@code Mono}.
     */
    Mono<User> update(User user);

    /**
     * Find a user by the given ID.
     *
     * @param userId the id of the user
     * @return A {@link Mono} where, upon successful completion, emits the {@link User}.If an error is received,
     * it is emitted through the {@code Mono}.
     */
    Mono<User> findById(Id userId);

    /**
     * Find a user by the given username
     *
     * @param username the user's username
     * @return A {@link Mono} where, upon successful completion, emits the {@link User}.If an error is received,
     * it is emitted through the {@code Mono}.
     */
    Mono<User> findByUsername(String username);

    /**
     * Find a user by the given email ignoring case sensitivity
     *
     * @param email the user's email
     * @return A {@link Mono} where, upon successful completion, emits the {@link User}.If an error is received,
     * it is emitted through the {@code Mono}.
     */
    Mono<User> findByEmailIgnoreCase(String email);

    /**
     * Find a user by the given verification key
     *
     * @param key the user's account verification key
     * @return A {@link Mono} where, upon successful completion, emits the {@link User}.If an error is received,
     * it is emitted through the {@code Mono}.
     */
    Mono<User> findByVerificationKey(String key);

    /**
     * Find a user by the given reset key
     *
     * @param resetKey the user's account reset key
     * @return A {@link Mono} where, upon successful completion, emits the {@link User}.If an error is received,
     * it is emitted through the {@code Mono}.
     */
    Mono<User> findByResetKey(String resetKey);

    Flux<User> findAllById(Iterable<Id> ids);

    /**
     * Check whether or user exists by the given username
     *
     * @param username the user's username
     * @return A {@link Mono} where, upon successful completion, emits the {@link User}.If an error is received,
     * it is emitted through the {@code Mono}.
     */
    Mono<Boolean> existsByUsername(String username);

    /**
     * Check whether or user exists by the given email
     *
     * @param email the user's email
     * @return A {@link Mono} where, upon successful completion, emits the {@link User}.If an error is received,
     * it is emitted through the {@code Mono}.
     */
    Mono<Boolean> existsByEmail(String email);

    /**
     * Find all the users
     *
     * @return A {@link Flux} where, upon successful completion, emits the {@link User}.If an error is received,
     * it is emitted through the {@code Flux}.
     */
    Flux<User> findAllBy(int limit, int offset);

    /**
     * Search user by the given username
     *
     * @param username the username to find
     * @param limit the limit of records
     * @return A {@link Flux} where, upon successful completion, emits the {@link User}.If an error is received,
     * it is emitted through the {@code Flux}.
     */
    Flux<User> searchByUsername(String username, int limit);
}
