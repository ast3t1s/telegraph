package im.telegraph.admin;

import im.telegraph.admin.object.MessageBean;
import im.telegraph.admin.object.request.DeleteMessagesRequest;
import im.telegraph.admin.object.request.SendMessageRequest;
import im.telegraph.admin.route.AbstractRoute;
import im.telegraph.admin.route.Endpoint;
import im.telegraph.admin.route.Route;
import im.telegraph.admin.route.RouteUtils;
import im.telegraph.api.ApiTextMessage;
import im.telegraph.domain.entity.Id;
import im.telegraph.domain.entity.Peer;
import im.telegraph.service.MessagingService;
import io.netty.handler.codec.http.HttpMethod;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.netty.http.server.HttpServerRequest;
import reactor.netty.http.server.HttpServerResponse;

import java.time.Instant;
import java.util.Base64;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static io.netty.handler.codec.http.HttpResponseStatus.NO_CONTENT;
import static io.netty.handler.codec.http.HttpResponseStatus.OK;

/**
 * @author Ast3t1s
 */
@Component
public class MessageRouterHandler extends AbstractRoute {

    private final MessagingService messagingService;

    public MessageRouterHandler(MessagingService messagingService) {
        this.messagingService = messagingService;
    }

    @Override
    public List<Route> routes() {
        return List.of(
                Route.builder()
                        .endpoint(Endpoint.ofFixedPath(HttpMethod.POST, "/messages/create"))
                        .action(this::handleCreateMessage)
                        .corsHeaders(),
                Route.builder()
                        .endpoint(Endpoint.ofFixedPath(HttpMethod.GET, "/messages/{userId}/{peer}"))
                        .action(this::handleGetMessages)
                        .corsHeaders(),
                Route.builder()
                        .endpoint(Endpoint.ofFixedPath(HttpMethod.POST, "/messages/{userId}/{peer}"))
                        .action(this::handleDeleteMessages)
                        .corsHeaders()
        );
    }

    protected Mono<Void> handleCreateMessage(HttpServerRequest request, HttpServerResponse response) {
        return readJson(request, SendMessageRequest.class)
                .flatMap(req -> messagingService.createMessage(Id.of(req.getUserId()),
                        Peer.fromUniqueId(req.getPeer()), req.getRandomId(), null,
                        new ApiTextMessage(req.getText(), Collections.emptyList(), null)))
                .flatMap(message -> response.status(OK).send());
    }

    protected Mono<Void> handleGetMessages(HttpServerRequest request, HttpServerResponse response) {
        final Id userId = Optional.ofNullable(request.param("userId"))
                .map(Integer::valueOf).map(Id::of).orElse(null);
        final Peer peer = Optional.ofNullable(request.param("peer"))
                .map(Long::valueOf).map(Peer::fromUniqueId).orElse(null);

        if (userId == null || peer == null) {
            return response.sendNotFound();
        }

        final int size = RouteUtils.queryParam("limit", request.uri())
                .map(Integer::parseInt).orElse(20);
        final long date = RouteUtils.queryParam("date", request.uri())
                .map(Long::parseLong).orElse(0L);

        return messagingService.getHistoryMessages(userId, peer, Instant.ofEpochMilli(date), size)
                .map(message -> new MessageBean(message.getId().asLong(),
                        message.getSenderId().asString(),
                        message.getRandomId(),
                        message.getType().getValue(),
                        Base64.getEncoder().encodeToString(message.getContent())))
                .collectList()
                .flatMap(messages -> writeJson(response.status(OK), messages));
    }

    protected Mono<Void> handleDeleteMessages(HttpServerRequest request, HttpServerResponse response) {
        final Id userId = Optional.ofNullable(request.param("userId"))
                .map(Integer::parseInt).map(Id::of)
                .orElse(null);
        final Peer peer = Optional.ofNullable(request.param("peer"))
                .map(Long::parseLong).map(Peer::fromUniqueId)
                .orElse(null);

        if (userId == null || peer == null) {
            return response.sendNotFound();
        }

        return readJson(request, DeleteMessagesRequest.class)
                .map(DeleteMessagesRequest::getRids)
                .flatMapIterable(ids -> ids.stream().map(Id::of).collect(Collectors.toList()))
                .collectList()
                .flatMap(rids -> messagingService.deleteMessages(userId, peer, rids).then())
                .then(response.status(NO_CONTENT).send());
    }
}
