package im.telegraph.pubsub.internal;

/**
 * @author Ast3t1s
 */
public class ValueHolder {

    private final long version;
    private final String destination;

    public ValueHolder(long version, String destination) {
        this.version = version;
        this.destination = destination;
    }

    public long getVersion() {
        return version;
    }

    public String getDestination() {
        return destination;
    }

    @Override
    public String toString() {
        return "ValueHolder{" +
                "version=" + version +
                ", destination='" + destination + '\'' +
                '}';
    }
}
