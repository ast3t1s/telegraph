package im.telegraph.protocol.model.mtproto.auth;

import im.telegraph.engine.serialization.DataInputStream;
import im.telegraph.engine.serialization.DataOutputStream;
import im.telegraph.protocol.model.mtproto.ProtoStruct;

import java.io.IOException;

public class RequestGetServerKey extends ProtoStruct {

	public static final int HEADER = 0xE2;

	private long keyId;

	public RequestGetServerKey(long keyId) {
		this.keyId = keyId;
	}

	public RequestGetServerKey(DataInputStream inputStream) throws IOException {
		super(inputStream);
	}

	public long getKeyId() {
		return keyId;
	}

	@Override
	protected byte getHeader() {
		return (byte) HEADER;
	}

	@Override
	protected void writeBody(DataOutputStream outputStream) throws IOException {
		outputStream.writeInt64(keyId);
	}

	@Override
	protected void readBody(DataInputStream inputStream) throws IOException {
		this.keyId = inputStream.readInt64();
	}

}
