package im.telegraph.rpc.base;

public class PacketTypeConstants {

	public static final int HEADER_PROTO = 0;

	public static final int HEADER_PING = 1;

	public static final int HEADER_PONG = 2;

	public static final int HEADER_DROP = 3;

	public static final int HEADER_ACK = 6;

	public static final int HEADER_HANDSHAKE_REQUEST = 0xFF;

	public static final int HEADER_HANDSHAKE_RESPONSE = 0xFE;

}
