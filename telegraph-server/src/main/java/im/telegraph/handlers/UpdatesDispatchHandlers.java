package im.telegraph.handlers;

import im.telegraph.api.ApiAbsUser;
import im.telegraph.api.ApiChat;
import im.telegraph.api.ApiDifferenceUpdate;
import im.telegraph.api.rpc.RequestSequenceGetDifference;
import im.telegraph.api.rpc.RequestSequenceGetState;
import im.telegraph.api.rpc.ResponseSequenceGetDifference;
import im.telegraph.api.rpc.ResponseSequenceGetState;
import im.telegraph.domain.entity.Id;
import im.telegraph.domain.entity.SequenceUpdate;
import im.telegraph.handlers.dispatch.DispatchContext;
import im.telegraph.handlers.internal.ApiEntityMapper;
import im.telegraph.handlers.internal.CombinedDifference;
import im.telegraph.handlers.internal.GetDiffCombiner;
import im.telegraph.handlers.mapper.UserConverter;
import im.telegraph.service.ChatService;
import im.telegraph.service.UpdatesService;
import im.telegraph.service.UserService;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.function.TupleUtils;

import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Component
public class UpdatesDispatchHandlers {

    private final UpdatesService updateService;
    private final UserService userService;
    private final ChatService chatService;

    private final UserConverter userConverter;

    public UpdatesDispatchHandlers(UpdatesService updateService,
                                   UserService userService,
                                   ChatService chatService,
                                   UserConverter userConverter) {
        this.updateService = updateService;
        this.userService = userService;
        this.chatService = chatService;
        this.userConverter = userConverter;
    }

    public Mono<ResponseSequenceGetState> handleGetState(DispatchContext<RequestSequenceGetState> context) {
        return Mono.justOrEmpty(context.getUserId().map(Id::of))
                .flatMap(updateService::findLastSeqUpdate)
                .map(seqState -> new ResponseSequenceGetState(seqState.getSequence(), seqState.getTimestamp().toEpochMilli()))
                .defaultIfEmpty(new ResponseSequenceGetState(0, Instant.EPOCH.toEpochMilli()));
    }

    public Mono<ResponseSequenceGetDifference> handleGetDifference(DispatchContext<RequestSequenceGetDifference> context) {
        return Mono.justOrEmpty(context.getUserId().map(Id::of))
                .flatMap(selfId -> updateService.getDifference(selfId, context.getAuthId(), context.getRequest().getSeq()))
                .flatMap(diffAcc -> {
                    CombinedDifference diff = GetDiffCombiner.buildSequenceDiff(context.getAuthId(), diffAcc.getUpdates());

                    Mono<Integer> seq = Flux.fromIterable(diffAcc.getUpdates())
                            .map(SequenceUpdate::getSequence)
                            .defaultIfEmpty(context.getRequest().getSeq())
                            .last();

                    Mono<Boolean> needMore = Mono.justOrEmpty(diffAcc.isHasMore())
                            .defaultIfEmpty(false);

                    Mono<List<ApiAbsUser>> apiUsers = Flux.fromIterable(diff.getUpdatedUsers())
                            .map(Id::of)
                            .collectList()
                            .flatMapMany(userService::getUsers)
                            .map(user -> Optional.ofNullable(userConverter.convert(context.getAuthId(), user)))
                            .filter(Optional::isPresent)
                            .map(Optional::get)
                            .collectList()
                            .defaultIfEmpty(Collections.emptyList());

                    Mono<List<ApiChat>> apiChats = Flux.fromIterable(diff.getUpdatedChats())
                            .map(Id::of)
                            .collectList()
                            .flatMapMany(chatService::getChats)
                            .map(chat -> Optional.ofNullable(ApiEntityMapper.toApiChat(chat)))
                            .filter(Optional::isPresent)
                            .map(Optional::get)
                            .collectList()
                            .defaultIfEmpty(Collections.emptyList());

                    Mono<List<ApiDifferenceUpdate>> apiUpdates = Flux.fromIterable(diff.getOtherUpdates())
                            .collectList()
                            .defaultIfEmpty(Collections.emptyList());

                    return Mono.zip(seq, apiUsers, apiChats, apiUpdates, needMore);
                })
                .map(TupleUtils.function(ResponseSequenceGetDifference::new));
    }

}
