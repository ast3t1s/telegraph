package im.telegraph.server.reactor;

import im.telegraph.server.AbstractTransportConnection;
import im.telegraph.server.TransportFrame;
import org.reactivestreams.Publisher;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.netty.Connection;

import java.net.SocketAddress;

/**
 * @author Ast3t1s
 */
public final class TcpTransportConnection extends AbstractTransportConnection<Connection> {

    public TcpTransportConnection(Connection delegate) {
        super(delegate);
    }

    @Override
    protected Mono<Void> sendInternal(Publisher<? extends byte[]> packets) {
        return getDelegate().outbound().sendByteArray(packets).then();
    }

    @Override
    protected Flux<TransportFrame> receiveInternal() {
        return getDelegate().inbound().receiveObject().cast(TransportFrame.class);
        //return getDelegate().inbound().receive().asByteBuffer();
    }

    @Override
    public SocketAddress remoteAddress() {
        return getDelegate().channel().remoteAddress();
    }

    @Override
    public boolean isOpen() {
        return getDelegate().isDisposed();
    }

    @Override
    public Mono<Void> onClose() {
        return getDelegate().onDispose();
    }

    @Override
    public Mono<Void> close() {
        return Mono.fromRunnable(() -> getDelegate().disposeNow());
    }

    @Override
    public String toString() {
        return "TcpTransportConnection{" +
                "Created date=" + createdDate() +
                ", Last access time=" + getLastAccessTime() +
                ", Remote address=" + remoteAddress() +
                ", Received packets=" + getReceivedPackets() +
                ", Sent packets=" + getSentPackets() +
                ", Id='" + getId() + '\'' +
                '}';
    }
}
