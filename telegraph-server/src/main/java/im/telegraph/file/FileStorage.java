package im.telegraph.file;

import org.reactivestreams.Publisher;
import reactor.core.publisher.Mono;

/**
 * @author Ast3t1s
 */
public interface FileStorage extends DownloadOperations, UploadOperations {

    /**
     * Deletes a file by given file key.
     *
     * @param fileKey The key of the file to delete.
     * @return A {@link Mono} which signals the completion of the file deletion.
     */
    Mono<Void> delete(String fileKey);

    /**
     * Deletes the files associated with the provided file keys.
     *
     * @param fileKeys A stream of file keys to delete files for.
     * @return A {@link Mono} which signals the completion of the deletion of the files.
     */
    Mono<Void> delete(Publisher<String> fileKeys);
}
