package im.telegraph.domain.event;

/**
 * @author Ast3t1s
 */
public class TerminateSessionEvent implements Event {

    private final Long userId;
    private final long hash;

    public TerminateSessionEvent(Long userId, long hash) {
        this.userId = userId;
        this.hash = hash;
    }

    public Long getUserId() {
        return userId;
    }

    public long getHash() {
        return hash;
    }

    @Override
    public String toString() {
        return "TerminateSessionEvent{" +
                "userId=" + userId +
                '}';
    }
}
