package im.telegraph.generator.type;

public class SchemeAliasType extends SchemeType {

    private String name;

    public SchemeAliasType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
} 
