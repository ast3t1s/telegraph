package im.telegraph.config;

import im.telegraph.cluster.Cluster;
import im.telegraph.internal.remote.RemoteTransport;
import im.telegraph.pubsub.DistributedPubSubMediator;
import im.telegraph.pubsub.PubSubMediator;
import im.telegraph.pubsub.ScheduleEventBust;
import im.telegraph.pubsub.SenderEvent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Ast3t1s
 */
@Configuration
public class PubSubConfiguration {

    private final TelegraphProperties properties;

    public PubSubConfiguration(TelegraphProperties properties) {
        this.properties = properties;
    }

    @Bean
    public DistributedPubSubMediator pubSubMediator(Cluster cluster, RemoteTransport transport) {
        return new DistributedPubSubMediator(properties.getPubSub(), cluster, transport);
    }

    //@Bean(initMethod = "start", destroyMethod = "stop")
    public SenderEvent senderEvent(PubSubMediator eventBus) {
        return new SenderEvent(eventBus);
    }

    //@Bean(initMethod = "start", destroyMethod = "stop")
    public ScheduleEventBust scheduleEventBust(PubSubMediator eventBus) {
        return new ScheduleEventBust(eventBus);
    }
} 
