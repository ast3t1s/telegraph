package im.telegraph.rpc.base;

import java.io.IOException;

public abstract class BaseParser<T> {

    public abstract T read(int type, byte[] payload) throws IOException;
} 
