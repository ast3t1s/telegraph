package im.telegraph.service;

import im.telegraph.common.DateTimeFactory;
import im.telegraph.crypto.primitives.util.ByteStrings;
import im.telegraph.domain.entity.AuthKey;
import im.telegraph.domain.repository.AuthKeyRepository;
import im.telegraph.util.Crypto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.Objects;

/**
 * @author Ast3t1s
 */
@Service
public class AuthKeyService {

    private static final Logger log = LoggerFactory.getLogger(AuthKeyService.class);

    private final AuthKeyRepository authKeyRepository;

    private final DateTimeFactory dateFactory;

    public AuthKeyService(AuthKeyRepository authKeyRepository, DateTimeFactory dateFactory) {
        this.authKeyRepository = authKeyRepository;
        this.dateFactory = dateFactory;
    }

    /**
     * Create a new authorization key {@code AuthKey} from master key.
     *
     * @param masterKey the master key.
     * @return A {@link Mono} where, upon successful completion, emits the created {@link AuthKey}.If an error is
     * received, it is emitted through the {@code Mono}.
     */
    public Mono<AuthKey> create(final byte[] masterKey) {
        Objects.requireNonNull(masterKey, "'masterKey' must be null");
        return Mono.defer(
                () -> {
                    final var sha256 = Crypto.SHA256(masterKey);
                    final var authKeyIdData = Arrays.copyOfRange(sha256, 0, Long.BYTES);
                    final var authKeyId = ByteStrings.bytesToLong(authKeyIdData);

                    return Mono.fromCallable(() -> AuthKey.of(authKeyId, masterKey, dateFactory.getClock().instant()))
                            .flatMap(authKeyRepository::insert)
                            .doOnSuccess(saved -> log.trace("Auth key saved: {}", saved));
                });
    }

    public Mono<AuthKey> getAuthKey(final Long authKeyId) {
        return authKeyRepository.findByAuthIdAndDeletedAtIsNull(authKeyId);
    }

    public Mono<Boolean> delete(final Long authKeyId) {
        return authKeyRepository.findByAuthIdAndDeletedAtIsNull(authKeyId)
                .map(authKey -> authKey.withDeletedAt(dateFactory.getClock().instant()))
                .flatMap(authKeyRepository::update)
                .doOnSuccess(deleted -> {
                    if (log.isTraceEnabled()) {
                        log.trace("Auth key: {} deleted", deleted);
                    }
                })
                .hasElement();
    }
} 
