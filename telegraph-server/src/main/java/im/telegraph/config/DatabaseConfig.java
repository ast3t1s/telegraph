package im.telegraph.config;

import im.telegraph.common.rdbms.RdbmsOperations;
import io.r2dbc.spi.ConnectionFactories;
import io.r2dbc.spi.ConnectionFactory;
import io.r2dbc.spi.ConnectionFactoryOptions;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import static io.r2dbc.spi.ConnectionFactoryOptions.*;

@Configuration
public class DatabaseConfig {

    @Bean
    public RdbmsOperations r2dbcOperations(ConnectionFactory connectionFactory) {
        return RdbmsOperations.create(connectionFactory);
    }

    @Bean
    @Primary
    public ConnectionFactory connectionFactory() {

        return ConnectionFactories.get(ConnectionFactoryOptions.builder()
                .option(DRIVER,"pool")
                .option(PROTOCOL,"postgresql") // driver identifier, PROTOCOL is delegated as DRIVER by the pool.
                .option(HOST,"localhost")
                .option(PORT,5432)
                .option(USER,"postgres")
                .option(PASSWORD,"7782588")
                .option(DATABASE,"postgres")
                .build());
    }

    /*@Bean
    @Primary
    public ConnectionPool connectionPool(ConnectionFactory connectionFactory) {
        ConnectionPoolConfiguration poolConfiguration = ConnectionPoolConfiguration.builder(connectionFactory)
                .initialSize(5)
                .maxIdleTime(Duration.ofMillis(1000))
                .maxSize(20)
                .build();

        ConnectionPool pool = new ConnectionPool(poolConfiguration);
        return pool;
    }*/
}
