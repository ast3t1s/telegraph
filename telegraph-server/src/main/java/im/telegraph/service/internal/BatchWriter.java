package im.telegraph.service.internal;

import im.telegraph.common.sinks.EmissionStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.Disposable;
import reactor.core.Disposables;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Sinks;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * @author Ast3t1s
 */
public final class BatchWriter<T> implements Disposable {

    private static final Logger log = LoggerFactory.getLogger(BatchWriter.class);
    private static final Supplier<Scheduler> DEFAULT_BATCH_SCHEDULER = () ->
            Schedulers.newSingle("tl-batch-writer", true);

    private final ConcurrentLinkedQueue<T> dispatchQueue = new ConcurrentLinkedQueue<>();

    private final int batchSize;
    private final Duration interval;
    private final Function<List<T>, Mono<Void>> flushFunc;
    private final Scheduler batchWriterScheduler;
    private final EmissionStrategy emissionStrategy;
    private final Sinks.Many<T> dispatchSink = Sinks.many().multicast().onBackpressureBuffer();
    private final AtomicBoolean flushing = new AtomicBoolean(false);
    private final Disposable.Swap task;

    public BatchWriter(int batchSize, Duration interval, Function<List<T>, Mono<Void>> flushFunc) {
        this(batchSize, interval, flushFunc, DEFAULT_BATCH_SCHEDULER.get());
    }

    public BatchWriter(int batchSize, Duration interval, Function<List<T>, Mono<Void>> flushFunc,
                       Scheduler batchWriterScheduler) {
        this.batchSize = batchSize;
        this.interval = interval;
        this.flushFunc = flushFunc;
        this.batchWriterScheduler = batchWriterScheduler;
        this.emissionStrategy = EmissionStrategy.park(Duration.ofNanos(10));
        this.task = Disposables.swap();

        dispatchSink.asFlux()
                .publishOn(batchWriterScheduler)
                .flatMap(this::processItem)
                .subscribe();
    }

    private Mono<Void> processItem(final T item) {
        return Mono.defer(
                () -> {
                    dispatchQueue.offer(item);
                    if (dispatchQueue.size() >= batchSize) {
                        return flushItems();
                    }
                    return Mono.empty();
                });
    }

    public void enqueue(final T item) {
        emissionStrategy.emitNext(dispatchSink, item);
    }

    public void start() {
       task.replace(Flux.interval(interval, interval, batchWriterScheduler)
               .onBackpressureBuffer()
               .concatMap(tick -> flushItems())
               .onErrorResume(ex -> {
                   log.warn("Error processing batch writer ", ex);
                   return Mono.empty();
               })
               .subscribe());
    }

    public void stop() {
        if (task.get() != null) {
            task.get().dispose();
        }
        dispatchQueue.clear();
        emissionStrategy.emitComplete(dispatchSink);
    }

    private Mono<Void> flushItems() {
        if (flushing.compareAndSet(false, true)) {
            return Flux.<T>generate(sink -> {
                final T item = dispatchQueue.poll();
                if (item != null) {
                    sink.next(item);
                } else {
                    sink.complete();
                }
            }).collectList().flatMap(flushFunc)
                    .doFinally(s -> flushing.set(false)).then();
        }
        return Mono.empty();
    }

    @Override
    public void dispose() {
        task.get().dispose();
    }

    @Override
    public boolean isDisposed() {
        return task.isDisposed();
    }
}
