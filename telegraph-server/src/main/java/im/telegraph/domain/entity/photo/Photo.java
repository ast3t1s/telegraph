package im.telegraph.domain.entity.photo;

import im.telegraph.domain.entity.Id;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

public class Photo implements Serializable {

    public static final String ENTITY_NAME = Photo.class.getSimpleName();

    private static final long serialVersionUID = 1L;

    private final Id id;
    private final Id userId;
    private final Id fileId;
    private final String accessSalt;
    private final PhotoSize photoSmall;
    private final PhotoSize photoLarge;
    private final Instant createdAt;

    /**
     * Constructs a new Photo instance.
     *
     * @param id The unique identifier for this photo (nullable).
     * @param userId The identifier of the user who owns this photo (non-null).
     * @param fileId The identifier of the file associated with this photo (non-null).
     * @param accessSalt A string used for access control (nullable).
     * @param photoSmall The small version of the photo (non-null).
     * @param photoLarge The large version of the photo (non-null).
     * @param createdAt The creation timestamp of the photo (non-null).
     */
    public Photo(final Id id, final Id userId, final Id fileId, final String accessSalt,
                 final PhotoSize photoSmall, final PhotoSize photoLarge, final Instant createdAt) {
        this.id = id;
        this.userId = Objects.requireNonNull(userId);
        this.fileId = Objects.requireNonNull(fileId);
        this.accessSalt = Objects.requireNonNull(accessSalt);
        this.photoSmall = Objects.requireNonNull(photoSmall);
        this.photoLarge = Objects.requireNonNull(photoLarge);
        this.createdAt = Objects.requireNonNull(createdAt);
    }

    public static Builder builder() {
        return new Builder();
    }

    /**
     * Gets the unique identifier for this photo.
     *
     * @return The photo ID, or null if not set.
     */
    public Id getId() {
        return id;
    }

    /**
     * Gets the user identifier associated with this photo.
     *
     * @return The user ID (non-null).
     */
    public Id getUserId() {
        return userId;
    }

    /**
     * Gets the file identifier associated with this photo.
     *
     * @return The file ID (non-null).
     */
    public Id getFileId() {
        return fileId;
    }

    /**
     * Gets the access salt used for securing access to this photo.
     *
     * @return The access salt, or null if not set.
     */
    public String getAccessSalt() {
        return accessSalt;
    }

    /**
     * Gets the small version of this photo.
     *
     * @return The small photo size (non-null).
     */
    public PhotoSize getPhotoSmall() {
        return photoSmall;
    }

    /**
     * Gets the large version of this photo.
     *
     * @return The large photo size (non-null).
     */
    public PhotoSize getPhotoLarge() {
        return photoLarge;
    }

    /**
     * Gets the creation timestamp of this photo.
     *
     * @return The creation timestamp (non-null).
     */
    public Instant getCreatedAt() {
        return createdAt;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        Photo photo = (Photo) object;
        return Objects.equals(id, photo.id) &&
                Objects.equals(userId, photo.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userId);
    }

    public static class Builder {

    }

    @Override
    public String toString() {
        return "Photo{" +
                "id=" + id +
                ", userId=" + userId +
                ", fileId=" + fileId +
                ", accessSalt='" + accessSalt + '\'' +
                ", photoSmall=" + photoSmall +
                ", photoLarge=" + photoLarge +
                ", createdAt=" + createdAt +
                '}';
    }
}
