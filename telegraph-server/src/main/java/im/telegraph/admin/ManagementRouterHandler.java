package im.telegraph.admin;

import im.telegraph.admin.object.ClusterNodeBean;
import im.telegraph.admin.object.request.AddClusterNodeRequest;
import im.telegraph.admin.object.request.RemoveClusterNodeRequest;
import im.telegraph.admin.route.AbstractRoute;
import im.telegraph.admin.route.Endpoint;
import im.telegraph.admin.route.Route;
import im.telegraph.cluster.Cluster;
import im.telegraph.internal.remote.Address;
import io.netty.handler.codec.http.HttpMethod;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.netty.http.server.HttpServerRequest;
import reactor.netty.http.server.HttpServerResponse;

import java.util.Collections;
import java.util.List;

import static io.netty.handler.codec.http.HttpResponseStatus.NO_CONTENT;
import static io.netty.handler.codec.http.HttpResponseStatus.OK;

/**
 * @author Ast3t1s
 */
@Component
public class ManagementRouterHandler extends AbstractRoute {

    private final Cluster cluster;

    public ManagementRouterHandler(Cluster cluster) {
        this.cluster = cluster;
    }

    @Override
    public List<Route> routes() {
        return List.of(
                Route.builder()
                        .endpoint(Endpoint.ofFixedPath(HttpMethod.POST, "/cluster/add"))
                        .action(this::handleAddMember)
                        .corsHeaders(),
                Route.builder()
                        .endpoint(Endpoint.ofFixedPath(HttpMethod.GET, "/cluster"))
                        .action(this::handleGetClusterMembers)
                        .corsHeaders(),
                Route.builder()
                        .endpoint(Endpoint.ofFixedPath(HttpMethod.DELETE, "/cluster/remove"))
                        .action(this::handleRemoveMember)
                        .corsHeaders()
        );
    }

    protected Mono<Void> handleGetClusterMembers(HttpServerRequest request, HttpServerResponse response) {
        return cluster.members()
                .map(member -> new ClusterNodeBean(
                        member.getUniqueAddress().getId(),
                        member.getAddress().getHost(),
                        member.getAddress().getPort(),
                        member.getVersion().toString(),
                        member.getRoles()))
                .collectList()
                .flatMap(members -> writeJson(response.status(OK), members));
    }

    protected Mono<Void> handleAddMember(HttpServerRequest request, HttpServerResponse response) {
        return readJson(request, AddClusterNodeRequest.class)
                .flatMap(req -> cluster.joinSeedNodes(Collections.singletonList(Address.create(req.getAddress()))))
                .then(response.status(OK).send());
    }

    protected Mono<Void> handleRemoveMember(HttpServerRequest request, HttpServerResponse response) {
        return readJson(request, RemoveClusterNodeRequest.class)
                .flatMap(req -> cluster.leave(Address.create(req.getAddress())))
                .then(response.status(NO_CONTENT).send());
    }
}
