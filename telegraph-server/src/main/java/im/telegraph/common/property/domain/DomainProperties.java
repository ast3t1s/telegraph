package im.telegraph.common.property.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

/**
 * @author Ast3t1s
 */
@Getter
@Setter
@Accessors(chain = true)
public class DomainProperties {

    /**
     * Avatar settings.
     */
    @NestedConfigurationProperty
    private AvatarProperties avatar = new AvatarProperties();

    /**
     * Chat settings.
     */
    @NestedConfigurationProperty
    private ChatProperties chat = new ChatProperties();

    /**
     * File settings.
     */
    @NestedConfigurationProperty
    private FileProperties file = new FileProperties();

    /**
     * Message settings.
     */
    @NestedConfigurationProperty
    private MessageProperties message = new MessageProperties();

    /**
     * Updates settings.
     */
    @NestedConfigurationProperty
    private UpdatesProperties updates = new UpdatesProperties();

    /**
     * User settings.
     */
    @NestedConfigurationProperty
    private UserProperties user = new UserProperties();

    /**
     * Session settings.
     */
    @NestedConfigurationProperty
    private SessionProperties session = new SessionProperties();
}
