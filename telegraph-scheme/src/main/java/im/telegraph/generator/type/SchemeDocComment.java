package im.telegraph.generator.type;

public class SchemeDocComment extends SchemeDoc {
    private String text;

    public SchemeDocComment(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}