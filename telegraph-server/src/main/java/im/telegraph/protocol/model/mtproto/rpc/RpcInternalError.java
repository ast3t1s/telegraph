package im.telegraph.protocol.model.mtproto.rpc;

import im.telegraph.engine.serialization.DataInputStream;
import im.telegraph.engine.serialization.DataOutputStream;
import im.telegraph.protocol.model.mtproto.ProtoStruct;

import java.io.IOException;

public class RpcInternalError extends ProtoStruct {

	public static final byte HEADER = (byte) 0x04;

	private boolean canTryAgain;

	private int tryAgainDelay;

	public RpcInternalError(DataInputStream stream) throws IOException {
		super(stream);
	}

	public RpcInternalError(boolean canTryAgain, int tryAgainDelay) {
		this.canTryAgain = canTryAgain;
		this.tryAgainDelay = tryAgainDelay;
	}

	public boolean isCanTryAgain() {
		return canTryAgain;
	}

	public int getTryAgainDelay() {
		return tryAgainDelay;
	}

	@Override
	protected byte getHeader() {
		return HEADER;
	}

	@Override
	protected void writeBody(DataOutputStream outputStream) throws IOException {
		outputStream.writeByte(canTryAgain ? 1 : 0);
		outputStream.writeInt32(tryAgainDelay);
	}

	@Override
	protected void readBody(DataInputStream inputStream) throws IOException {
		this.canTryAgain = inputStream.readByte() > 0;
		this.tryAgainDelay = inputStream.readInt();
	}

}
