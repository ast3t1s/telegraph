package im.telegraph.domain.repository;

import im.telegraph.domain.entity.AuthSession;
import im.telegraph.domain.entity.Id;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Collection;

/**
 * @author Ast3t1s
 */
public interface AuthSessionRepository  {

    Mono<AuthSession> insert(AuthSession session);

    Mono<AuthSession> findById(Id id);

    /**
     * Retrieves an AuthSession by its authId and userId.
     *
     * @param authId the ID of the authentication session
     * @param userId the ID of the user
     * @return A {@link Mono} that emits the created {@link AuthSession} upon successful completion. If an error occurs,
     * it is emitted through the {@code Mono}.
     */
    Mono<AuthSession> findByAuthIdAndUserId(Long authId, Id userId);

    /**
     * Retrieves an AuthSession by its authId where the session has not been deleted.
     *
     * @param authId the ID of the authentication session
     * @return A {@link Mono} that emits the created {@link AuthSession} upon successful completion. If an error occurs,
     * it is emitted through the {@code Mono}.
     */
    Mono<AuthSession> findByAuthIdAndDeletedAtIsNull(Long authId);

    /**
     * Retrieves all AuthSessions associated with a user that have not been deleted.
     *
     * @param userId the ID of the user
     * @return A {@link Flux} that emits the {@link AuthSession} instances for the given {@link Id}. If an error occurs,
     * it is emitted through the {@code Flux}.
     */
    Flux<AuthSession> findAllByUserIdAndDeletedAtIsNull(Id userId);

    /**
     * Deletes multiple AuthSessions based on their IDs.
     *
     * @param ids the list of IDs of the authentication sessions to be deleted
     * @return A {@link Mono} that emits {@code true} if the deletion was successful, {@code false} otherwise.
     * If an error occurs, it is emitted through the {@code Mono}.
     */
    Mono<Long> deleteByIdsIn(Collection<Id> ids);
}
