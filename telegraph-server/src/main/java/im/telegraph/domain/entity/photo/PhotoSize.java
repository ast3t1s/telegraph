package im.telegraph.domain.entity.photo;

import im.telegraph.domain.entity.FileReferenceId;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author Ast3t1s
 */
public final class PhotoSize implements Serializable {

    private static final long serialVersionUID = 1L;

    private final FileReferenceId fileReferenceId;
    private final int width;
    private final int height;
    private final int fileSize;

    public PhotoSize(FileReferenceId fileReferenceId, int width, int height, int fileSize) {
        this.fileReferenceId = Objects.requireNonNull(fileReferenceId);
        this.width = width;
        this.height = height;
        this.fileSize = fileSize;
    }

    public FileReferenceId getFileReferenceId() {
        return fileReferenceId;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getFileSize() {
        return fileSize;
    }

    @Override
    public String toString() {
        return "PhotoSize{" +
                "fileReferenceId=" + fileReferenceId +
                ", width=" + width +
                ", height=" + height +
                ", fileSize=" + fileSize +
                '}';
    }
} 
