package im.telegraph.server;

import org.reactivestreams.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Instant;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.zip.CRC32;

/**
 * @author Vsevolod Bondarenko
 * @param <T>
 */
public abstract class AbstractTransportConnection<T> implements TransportConnection {

    protected final Logger log = LoggerFactory.getLogger(getClass());

    @SuppressWarnings("rawtypes")
    private static final AtomicIntegerFieldUpdater<AbstractTransportConnection> RECEIVED_UPDATER =
            AtomicIntegerFieldUpdater.newUpdater(AbstractTransportConnection.class, "receivedPackets");

    @SuppressWarnings("rawtypes")
    private static final AtomicIntegerFieldUpdater<AbstractTransportConnection> SENT_UPDATER =
            AtomicIntegerFieldUpdater.newUpdater(AbstractTransportConnection.class, "sentPackets");

    private volatile int receivedPackets;
    private volatile int sentPackets;

    private final AtomicInteger packetIndexer = new AtomicInteger(-1);

    private final Instant createdDate = Instant.now();

    private volatile Instant lastAccessTime = Instant.EPOCH;

    private final CRC32 CRC32_ENGINE = new CRC32();

    private final String id;
    private final T delegate;
    private final String logPrefix;

    protected AbstractTransportConnection(T delegate) {
        this.delegate = Objects.requireNonNull(delegate, "'delegate' is required");
        this.id = Integer.toHexString(System.identityHashCode(delegate));
        this.logPrefix = initLogPrefix(id, createdDate());

        if (log.isDebugEnabled()) {
            log.debug(getLogPrefix() + "Connection id\"" + getId());
        }
    }

    private static String initLogPrefix(String id, Instant date) {
        return "[id: " + id + " Initialized date: " + date + "]";
    }

    protected T getDelegate() {
        return delegate;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public int getSentPackets() {
        return sentPackets;
    }

    @Override
    public int getReceivedPackets() {
        return receivedPackets;
    }

    @Override
    public Instant createdDate() {
        return createdDate;
    }

    @Override
    public Instant getLastAccessTime() {
        return lastAccessTime;
    }

    protected String getLogPrefix() {
        return logPrefix;
    }

    @Override
    public Mono<Void> send(Publisher<MTProtoObject> messages) {
        Flux<byte[]> frames = Flux.from(messages)
                .doOnNext(frame -> {
                    if (log.isTraceEnabled()) {
                        log.trace(getLogPrefix() + " Sending " + frame);
                    }
                    onPacketSend();
                })
                .map(this::toTransportRaw);
        return sendInternal(frames);
    }

    @Override
    public Flux<TransportFrame> receive() {
        return receiveInternal()
                .doOnNext(packet -> {
                    if (log.isTraceEnabled()) {
                        log.trace(getLogPrefix() + " Received transport packet: {}", packet);
                    }
                    onPacketReceived();
                });
    }

    protected abstract Mono<Void> sendInternal(Publisher<? extends byte[]> packets);

    protected abstract Flux<TransportFrame> receiveInternal();

    protected void onPacketSend() {
        SENT_UPDATER.incrementAndGet(this);
        updateLastAccessTime(Instant.now());
    }

    protected void onPacketReceived() {
        RECEIVED_UPDATER.incrementAndGet(this);
        updateLastAccessTime(Instant.now());
    }

    private void updateLastAccessTime(Instant currentTime) {
        this.lastAccessTime = currentTime;
    }

    protected byte[] toTransportRaw(final MTProtoObject protoPacket) {
        return rawPostFrame(protoPacket.getHeader(), protoPacket.toByteArray());
    }

    protected byte[] rawPostFrame(final int header, final byte[] data) {
        return rawPostFrame(header, data, 0, data.length);
    }

    protected byte[] rawPostFrame(final int header, final byte[] data, final int offset, final int len) {
        final var packageId = packetIndexer.incrementAndGet();
        CRC32_ENGINE.reset();
        CRC32_ENGINE.update(data, offset, len);
        final var checksum = (int) CRC32_ENGINE.getValue();
        final var transportFrame = new TransportFrame(packageId, header, len, data, checksum);
        return transportFrame.toByteArray();
    }
}
