package im.telegraph;

import im.telegraph.config.TelegraphProperties;
import im.telegraph.util.ProfileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.core.env.Environment;
import reactor.core.Disposable;
import reactor.core.Disposables;
import reactor.tools.agent.ReactorDebugAgent;

import java.net.InetAddress;
import java.net.UnknownHostException;

@SpringBootApplication
@EnableConfigurationProperties({TelegraphProperties.class})
public class TelegraphApplication {

    private static final Logger log = LoggerFactory.getLogger(TelegraphApplication.class);

    private final Environment environment;

    static Disposable.Swap swap = Disposables.swap();

    @Autowired
    public TelegraphApplication(Environment environment) {
        this.environment = environment;
    }

    public static void main(String[] args) throws Exception {
     //   BlockHound.install();
        ReactorDebugAgent.init();
        ReactorDebugAgent.processExistingClasses();
        SpringApplication app = new SpringApplication(TelegraphApplication.class);
        ProfileUtils.addDefaultProfile(app);
        Environment environment = app.run(args).getEnvironment();
        logApplicationStartUp(environment);
    }
    
    private static void logApplicationStartUp(Environment environment) {
        String instanceId = environment.getProperty("server.instance-id");
        String hostAddress = "localhost";
        try {
            hostAddress = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            log.warn("The host name could not be determined, using `localhost` as fallback");
        }
        log.info("\n----------------------------------------------------------\n\t" +
                "Application '{}' is running! Access URLs:\n\t",
                environment.getProperty("spring.application.name"));
    }

}
