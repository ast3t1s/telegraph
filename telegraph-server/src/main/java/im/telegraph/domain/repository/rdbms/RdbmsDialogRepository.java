package im.telegraph.domain.repository.rdbms;

import im.telegraph.common.rdbms.RdbmsOperations;
import im.telegraph.domain.entity.Dialog;
import im.telegraph.domain.entity.Id;
import im.telegraph.domain.entity.Peer;
import im.telegraph.domain.repository.DialogRepository;
import io.r2dbc.spi.*;
import org.reactivestreams.Publisher;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.function.BiFunction;

/**
 * @author Ast3t1s
 */
@Repository
public class RdbmsDialogRepository extends AbstractRdbmsRepository implements DialogRepository {

    private static final String FIELDS = "peer_id, peer_type, user_id, last_message_id, timestamp, unread_count";

    private static final String TABLE_NAME = "dialogs";

    private static final String PK_FILTER = "peer_id = :peerId AND peer_type = :peerType AND user_id = :userId";

    private static final String GET_DIALOG_BY_USER_QUERY =
            "SELECT " + FIELDS + " FROM dialogs WHERE " + PK_FILTER;

    private static final String LIST_DIALOGS_BY_USER_ID_QUERY =
            "SELECT " + FIELDS + " FROM dialogs WHERE user_id = :userId ORDER BY last_message_id DESC";

    private static final String LIST_DIALOGS_QUERY =
            "SELECT " + FIELDS + " FROM dialogs WHERE user_id = :userId ORDER BY timestamp DESC";

    private static final String RESET_UNREAD_QUERY =
            "UPDATE " + TABLE_NAME + " SET unread_count = 0 WHERE " + PK_FILTER + " AND message_id = :msgId AND " +
                    "timestamp >= :timestamp";

    private static final String DELETE_DIALOG_QUERY =
            "DELETE FROM dialogs WHERE " + PK_FILTER;

    private static final String GET_DIALOGS_COUNT_QUERY =
            "SELECT COUNT(*) FROM " + TABLE_NAME + " WHERE user_id = :userId";

    private final RdbmsOperations rdbmsOperations;

    private final BiFunction<Row, RowMetadata, Dialog> dialogsRowMapper = new DialogRowMapper();

    private final String upsertQuery;
    private final String upsertManyQuery;
    private final String upsertIncrementQuery;
    private final String upsertIncrementManyQuery;

    public RdbmsDialogRepository(ConnectionFactory connectionFactory) {
        super(connectionFactory);
        this.rdbmsOperations = RdbmsOperations.create(connectionFactory);
        this.upsertQuery = buildUpsertQuery();
        this.upsertManyQuery = buildUpsertQueryMany();
        this.upsertIncrementQuery = buildUpsertIncrementQuery();
        this.upsertIncrementManyQuery = buildUpsertIncrementQueryMany();
    }

    @Override
    public Mono<Void> save(Dialog dialog) {
        return rdbmsOperations.useHandle(handle -> handle
                .createUpdate(upsertQuery)
                .bind("peerId", dialog.getPeer().getPeerId())
                .bind("peerType", dialog.getPeer().getType().getValue())
                .bind("userId", dialog.getUserId().asLong())
                .bind("msgId", dialog.getMessageId().asLong())
                .bind("timestamp", LocalDateTime.ofInstant(dialog.getTimestamp(), ZoneOffset.UTC))
                .bind("unreadCount", dialog.getUnreadCount())
                .execute());
    }

    @Override
    public Mono<Void> saveAndIncrementCounter(Dialog dialog) {
        return rdbmsOperations.useHandle(handle -> handle
                .createUpdate(upsertIncrementQuery)
                .bind("peerId", dialog.getPeer().getPeerId())
                .bind("peerType", dialog.getPeer().getType().getValue())
                .bind("userId", dialog.getUserId().asLong())
                .bind("msgId", dialog.getMessageId().asLong())
                .bind("timestamp", LocalDateTime.ofInstant(dialog.getTimestamp(), ZoneOffset.UTC))
                .bind("unreadCount", dialog.getUnreadCount())
                .execute());
    }

    @Override
    public Mono<Void> saveAndIncrementCounter(Publisher<Dialog> entryStream) {
        return Flux.from(entryStream)
                .map(this::buildDialogParams)
                .collectList()
                .flatMap(tuples -> rdbmsOperations.useTransaction(handle -> handle
                        .createUpdate(upsertIncrementManyQuery)
                        .bind("tuples", tuples)
                        .execute()));
    }

    @Override
    public Mono<Void> save(Publisher<Dialog> dialogs) {
        return Flux.from(dialogs)
                .map(this::buildDialogParams)
                .collectList()
                .flatMap(tuples -> rdbmsOperations.useTransaction(handle -> handle
                        .createUpdate(upsertManyQuery)
                        .bind("tuples", tuples)
                        .execute()));
    }

    private Object[] buildDialogParams(Dialog dialog) {
        return new Object[]{
                Parameters.in(R2dbcType.BIGINT, dialog.getPeer().getPeerId()),
                Parameters.in(R2dbcType.SMALLINT, dialog.getPeer().getType().getValue()),
                Parameters.in(R2dbcType.BIGINT, dialog.getUserId().asLong()),
                Parameters.in(R2dbcType.BIGINT, dialog.getMessageId().asLong()),
                Parameters.in(R2dbcType.TIMESTAMP, LocalDateTime.ofInstant(dialog.getTimestamp(), ZoneOffset.UTC)),
                Parameters.in(R2dbcType.INTEGER, dialog.getUnreadCount())};
    }

    @Override
    public Mono<Void> resetUnread(Id userId, Peer peer, Id messageId, Instant date) {
        return rdbmsOperations.useHandle(handle -> handle.createUpdate(RESET_UNREAD_QUERY)
                .bind("peerId", peer.getPeerId())
                .bind("peerType", peer.getType().getValue())
                .bind("msgId", messageId.asLong())
                .bind("timestamp", LocalDateTime.ofInstant(date, ZoneOffset.UTC))
                .execute());
    }

    @Override
    public Mono<Dialog> findByUserIdAndPeer(Id userId, Peer peer) {
        return rdbmsOperations.withHandle(handle -> handle.createQuery(GET_DIALOG_BY_USER_QUERY)
                .bind("peerId", peer.getPeerId())
                .bind("peerType", peer.getType())
                .bind("userId", userId.asLong())
                .mapRow(dialogsRowMapper))
                .next();
    }

    @Override
    public Flux<Dialog> findAllByUserId(Id userId, int limit, int offset) {
        return rdbmsOperations.withHandle(handle -> handle
                .createQuery(limitQuery(LIST_DIALOGS_BY_USER_ID_QUERY, limit, offset))
                .bind("userId", userId.asLong())
                .mapRow(dialogsRowMapper));
    }

    @Override
    public Flux<Dialog> finAllByUserIdAndDateLessThanEqual(Id userId, Instant lastMessageDate, int limit) {
        return rdbmsOperations.withHandle(handle -> handle.createQuery(limitQuery(LIST_DIALOGS_QUERY, limit))
                .bind("userId", userId.asLong())
                .mapRow(dialogsRowMapper));
    }

    @Override
    public Mono<Long> countByUserId(Id userId) {
        return rdbmsOperations.withHandle(handle -> handle.createQuery(GET_DIALOGS_COUNT_QUERY)
                .bind("userId", userId.asLong())
                .mapRow(row -> row.get(0, Long.class)))
                .next();
    }

    @Override
    public Mono<Void> deleteByUserIdAndPeer(Id userId, Peer peer) {
        return rdbmsOperations.useHandle(handle -> handle.createUpdate(DELETE_DIALOG_QUERY)
                .bind("peerId", peer.getPeerId())
                .bind("peerType", peer.getType())
                .bind("userId", userId.asLong())
                .execute().then());
    }

    private String buildUpsertQuery() {
        List<String> conflictColumns = Arrays.asList("peer_id", "peer_type", "user_id");
        List<String> updateColumns = Arrays.asList("last_message_id", "timestamp", "unread_count");
        return prepareUpsert(TABLE_NAME, buildParams(), conflictColumns, updateColumns);
    }

    private String buildUpsertQueryMany() {
        List<String> conflictColumns = Arrays.asList("peer_id", "peer_type", "user_id");
        List<String> updateColumns = Arrays.asList("last_message_id", "timestamp", "unread_count");
        return prepareUpsertMany(TABLE_NAME, "tuples", new ArrayList<>(buildParams().keySet()),
                conflictColumns, updateColumns);
    }

    private String buildUpsertIncrementQuery() {
        List<String> conflictColumns = Arrays.asList("peer_id", "peer_type", "user_id");
        List<String> updateColumns = Arrays.asList("last_message_id", "timestamp");
        return prepareUpsert(TABLE_NAME, buildParams(), conflictColumns, updateColumns, "unread_count");
    }

    private String buildUpsertIncrementQueryMany() {
        List<String> conflictColumns = Arrays.asList("peer_id", "peer_type", "user_id");
        List<String> updateColumns = Arrays.asList("last_message_id", "timestamp");
        return prepareUpsertMany(TABLE_NAME, "tuples", new ArrayList<>(buildParams().keySet()),
                conflictColumns, updateColumns, "unread_count");
    }

    private Map<String, Object> buildParams() {
        final Map<String, Object> params = new LinkedHashMap<>();
        params.put("peer_id", "peerId");
        params.put("peer_type", "peerType");
        params.put("user_id", "userId");
        params.put("last_message_id", "msgId");
        params.put("timestamp", "timestamp");
        params.put("unread_count", "unreadCount");
        return params;
    }

    static final class DialogRowMapper implements BiFunction<Row, RowMetadata, Dialog> {

        @Override
        public Dialog apply(Row row, RowMetadata metadata) {
            final var peerId = row.get("peer_id", Long.class);
            final var peerType = row.get("peer_type", Integer.class);
            final var userId = row.get("user_id", Long.class);
            final var lastMessageId = row.get("last_message_id", Long.class);
            final var timestamp = row.get("timestamp", LocalDateTime.class);

            return Dialog.builder()
                    .peer(Peer.of(peerId, Peer.Type.of(peerType)))
                    .userId(Id.of(userId))
                    .messageId(Id.of(lastMessageId))
                    .timestamp(timestamp.toInstant(ZoneOffset.UTC))
                    .unreadCount(row.get("unread_count", Integer.class))
                    .build();
        }
    }
}
