package im.telegraph.admin.object.request;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Ast3t1s
 */
public class AddContactRequest {

    @JsonProperty("contact_id")
    private Integer contactId;

    public Integer getContactId() {
        return contactId;
    }

    @Override
    public String toString() {
        return "AddContactRequest{" +
                "contactId=" + contactId +
                '}';
    }
}
