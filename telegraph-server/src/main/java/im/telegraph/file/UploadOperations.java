package im.telegraph.file;

import org.reactivestreams.Publisher;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import java.io.File;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.time.Duration;
import java.time.Instant;
import java.util.Objects;

/**
 * This interface provides data connection to a store's file source, supporting upload and download operations.
 */
public interface UploadOperations {

    /**
     * Uploads data from a specific source to storage.
     *
     * @param fileName the name of the file
     * @param filePath  the file filePath
     * @return a {@link Mono} that can be used to signal a response.
     */
    Mono<Void> upload(String fileName, Path filePath);

    /**
     * Uploads data from a specific source to storage.
     *
     * @param fileName the name of the file
     * @param payload file payload represented by {@code byte[]}
     * @return a {@link Mono} that can be used to signal a response.
     */
    Mono<Void> upload(String fileName, byte[] payload);

    /**
     * Uploads stream of {@link ByteBuffer} to the specific storage
     *
     * @param fileName the name of the file
     * @param payload file payload represented by stream of {@link ByteBuffer}
     * @return a {@link Mono} that can be used to signal a response.
     */
    Mono<Void> upload(String fileName, Publisher<ByteBuffer> payload);

    /**
     * Upload {@link File} to a storage.
     *
     * @param fileName the name of the file
     * @param file the {@link File} to upload
     * @return a {@link Mono} that can be used to signal a response.
     */
    default Mono<Void> upload(String fileName, File file) {
        Objects.requireNonNull(file);
        return upload(fileName, file.toPath());
    }

    /**
     * Uploads data from a specific source to storage.
     *
     * @param fileName the name of the file
     * @param payload file payload represented by {@link ByteBuffer}
     * @return a {@link Mono} that can be used to signal a response.
     */
    default Mono<Void> upload(String fileName, ByteBuffer payload) {
        Objects.requireNonNull(payload);
        return upload(fileName, payload.array());
    }

    /**
     * Uploads data from a specific source to storage.
     *
     * @param fileName the name of the file
     * @param payload file payload represented by string
     * @return a {@link Mono} that can be used to signal a response.
     */
    default Mono<Void> upload(String fileName, String payload) {
        Objects.requireNonNull(payload);
        return upload(fileName, payload.getBytes(StandardCharsets.UTF_8));
    }

    /**
     * Get file url with time expiration for file uploading.
     *
     * @param fileName the name of the file
     * @param expire   the expiration time
     * @return a {@link Mono} that can be used to signal a response
     */
    Mono<Tuple2<URL, Instant>> getFileUploadUrl(String fileName, Duration expire);

}
