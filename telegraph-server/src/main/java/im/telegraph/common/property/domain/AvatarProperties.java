package im.telegraph.common.property.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.util.unit.DataSize;

/**
 * @author Ast3t1s
 */
@Getter
@Setter
@Accessors(chain = true)
public class AvatarProperties {

    private static final int DEFAULT_SMALL = 160;

    private static final int DEFAULT_LARGE = 320;

    /**
     * Avatar max size in bytes.
     */
    private DataSize maxSize = DataSize.ofMegabytes(1);

    /**
     * The size of small avatar picture.
     */
    private int smallSize = DEFAULT_SMALL;

    /**
     * The size of large avatar picture;
     */
    private int largeSize = DEFAULT_LARGE;

}
