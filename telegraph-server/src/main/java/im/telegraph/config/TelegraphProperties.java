package im.telegraph.config;

import im.telegraph.common.property.EmailProperties;
import im.telegraph.common.property.HealthCheckProperties;
import im.telegraph.common.property.admin.AdminApiProperties;
import im.telegraph.common.property.cluster.ClusterProperties;
import im.telegraph.common.property.domain.DomainProperties;
import im.telegraph.common.property.pubsub.PubSubProperties;
import im.telegraph.common.property.file.FileStoreProperties;
import im.telegraph.common.property.protocol.crypto.CryptoProperties;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

@Getter
@Setter
@ConfigurationProperties(prefix = "telegraph")
public class TelegraphProperties {

    private String appVersion;

    /**
     * Protocol encryption settings.
     */
    @NestedConfigurationProperty
    private final CryptoProperties crypto = new CryptoProperties();

    /**
     * Health monitoring settings.
     */
    @NestedConfigurationProperty
    private final HealthCheckProperties health = new HealthCheckProperties();

    /**
     * Email settings.
     */
    @NestedConfigurationProperty
    private final EmailProperties email = new EmailProperties();

    /**
     * Cluster settings.
     */
    @NestedConfigurationProperty
    private final ClusterProperties cluster = new ClusterProperties();

    /**
     * File storage settings.
     */
    @NestedConfigurationProperty
    private final FileStoreProperties storage = new FileStoreProperties();

    /**
     * Domain settings.
     */
    @NestedConfigurationProperty
    private final DomainProperties domain = new DomainProperties();

    /**
     * PubSub settings.
     */
    @NestedConfigurationProperty
    private final PubSubProperties pubSub = new PubSubProperties();

    /**
     * Admin settings.
     */
    @NestedConfigurationProperty
    private final AdminApiProperties admin = new AdminApiProperties();

}
