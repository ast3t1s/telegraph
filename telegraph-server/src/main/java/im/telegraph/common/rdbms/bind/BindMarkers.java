package im.telegraph.common.rdbms.bind;

/**
 * @author Ast3t1s
 */
@FunctionalInterface
public interface BindMarkers {
    /**
     * Create a new {@link BindMarker}.
     */
    BindMarker next();

    /**
     * Create a new {@link BindMarker} that accepts a {@code hint}.
     * Implementations are allowed to consider/ignore/filter
     * the name hint to create more expressive bind markers.
     * @param hint an optional name hint that can be used as part of the bind marker
     * @return a new {@link BindMarker}
     */
    default BindMarker next(String hint) {
        return next();
    }
}
