package im.telegraph.util;

import im.telegraph.crypto.primitives.BlockCipher;
import im.telegraph.crypto.primitives.Digest;
import im.telegraph.crypto.primitives.aes.AESFastEngine;
import im.telegraph.crypto.primitives.digest.SHA256;

import java.security.DigestException;
import java.security.MessageDigest;

public class Crypto {

    public static Digest createSHA256() {
        return new SHA256();
    }

    public static BlockCipher createAES128(byte[] key) {
        return new AESFastEngine(key);
    }

    /**
     * Calculating SHA256
     *
     * @param data source data
     * @return SHA256 of data
     */
    public static byte[] SHA256(byte[] data) {
        MessageDigest digest = ThreadLocalSHA256.current();
        digest.update(data, 0, data.length);
        byte[] res = new byte[32];
        try {
            digest.digest(res, 0, res.length);
        } catch (DigestException e) {
            e.printStackTrace();
        }
        return res;
    }

    public static void nextBytes(byte[] data) {
        ThreadLocalSecureRandom.current().nextBytes(data);
    }

}
