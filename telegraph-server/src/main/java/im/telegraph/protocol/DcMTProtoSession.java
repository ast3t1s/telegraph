package im.telegraph.protocol;

import im.telegraph.crypto.ActorProtoKey;
import im.telegraph.crypto.box.CBCHmacBox;
import im.telegraph.crypto.primitives.kuznechik.KuznechikFastEngine;
import im.telegraph.crypto.primitives.streebog.Streebog256;
import im.telegraph.crypto.primitives.util.ByteStrings;
import im.telegraph.engine.serialization.stream.BinaryInputStream;
import im.telegraph.protocol.model.mtproto.*;
import im.telegraph.protocol.model.transport.ProtoPackage;
import im.telegraph.server.MTProtoObject;
import im.telegraph.server.TransportConnection;
import im.telegraph.util.Crypto;
import org.reactivestreams.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.Disposable;
import reactor.core.Disposables;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.annotation.Nullable;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

public class DcMTProtoSession implements MTProtoSession, MTProtoCipher {

    enum State {NEW, OPEN, CLOSED}

    private static final Logger log = LoggerFactory.getLogger(DcMTProtoSession.class);

    private static final AtomicIntegerFieldUpdater<DcMTProtoSession> IN_SEQ_UPDATER =
            AtomicIntegerFieldUpdater.newUpdater(DcMTProtoSession.class, "inSeq");

    private static final AtomicIntegerFieldUpdater<DcMTProtoSession> OUT_SEQ_UPDATER =
            AtomicIntegerFieldUpdater.newUpdater(DcMTProtoSession.class, "outSeq");

    private volatile int inSeq = -1;
    private volatile int outSeq = -1;

    private final SessionId id;

    private final ActorProtoKey actorProtoKey;
    private final CBCHmacBox clientUSDecryptor;
    private final CBCHmacBox clientRUDecryptor;
    private final CBCHmacBox serverUSEncryptor;
    private final CBCHmacBox serverRUEncryptor;

    private final AtomicLong messageId = new AtomicLong(0);

    private volatile Duration ackTimeout = Duration.ofMinutes(10);
    private volatile Duration emptySessionTTL = Duration.ofMinutes(5);
    private volatile Instant noConnectionSince = Instant.now();

    private final AtomicReference<TransportConnection> delegate = new AtomicReference<>();
    private final AtomicReference<State> state = new AtomicReference<>(State.NEW);

    private final ConcurrentLinkedDeque<Long> unacknowledgedServerPackets = new ConcurrentLinkedDeque<>();
    private final ConcurrentMap<Long, Schedule> outMessages = new ConcurrentHashMap<>();

    private volatile Object principal;

    private final Disposable.Swap task = Disposables.swap();

    // Add long firstMessageId
    public DcMTProtoSession(long authId, long sessionId, byte[] masterKey) {
        this.id = SessionId.create(authId, sessionId);
        this.actorProtoKey = new ActorProtoKey(masterKey);
        this.serverUSEncryptor = new CBCHmacBox(
                Crypto.createAES128(actorProtoKey.getServerKey()),
                Crypto.createSHA256(),
                actorProtoKey.getServerMacKey());
        this.serverRUEncryptor = new CBCHmacBox(
                new KuznechikFastEngine(actorProtoKey.getServerRussianKey()),
                new Streebog256(),
                actorProtoKey.getServerMacRussianKey());
        this.clientUSDecryptor = new CBCHmacBox(
                Crypto.createAES128(actorProtoKey.getClientKey()),
                Crypto.createSHA256(),
                actorProtoKey.getClientMacKey());
        this.clientRUDecryptor = new CBCHmacBox(
                new KuznechikFastEngine(actorProtoKey.getClientRussianKey()),
                new Streebog256(),
                actorProtoKey.getClientMacRussianKey());
    }

    @Override
    public MTProtoCipher getCipher() {
        return this;
    }

    @Override
    public SessionId getId() {
        return id;
    }

    @Override
    public Object getPrincipal() {
        return principal;
    }

    @Override
    public void setPrincipal(Object principal) {
        this.principal = principal;
    }

    @Override
    public boolean isExpired() {
        return checkExpired(Instant.now());
    }

    @Override
    @Nullable
    public Instant getTimeSinceLastActive() {
        return noConnectionSince;
    }

    @Override
    public void setEmptySessionTtl(Duration duration) {
        this.emptySessionTTL = duration;
    }

    @Override
    public InetSocketAddress getRemoteAddress() {
        return Optional.ofNullable(delegate.get()).map(TransportConnection::remoteAddress)
                .map(socketAddress -> (InetSocketAddress) socketAddress).orElseGet(null);
    }

    protected boolean checkExpired(Instant now) {
        return !emptySessionTTL.isNegative()
                && (noConnectionSince != null && noConnectionSince.plus(emptySessionTTL).isBefore(now));
    }

    public void initializeNativeSession(TransportConnection connection) {
        Objects.requireNonNull(connection, "Native session must not be null");

        this.delegate.set(connection);

        if (state.compareAndSet(State.NEW, State.OPEN)) {
            if (log.isDebugEnabled()) {
                log.debug("New Session: {} initialized", id);
            }
        }
        if (state.compareAndSet(State.CLOSED, State.OPEN)) {
            if (log.isDebugEnabled()) {
                log.debug("Session: {} resumed", id);
            }
        }
        this.noConnectionSince = null;

        connection.onClose()
                .doFinally(s -> log.info("Connection {} disposed", id))
                .subscribe();
    }

    @Override
    public Mono<Void> sendRpcMessage(Publisher<ProtoStruct> messages) {
        return Flux.from(messages)
                .map(rpc -> new Schedule(new ProtoMessage(messageId.getAndIncrement(), rpc.toByteArray())))
                .doOnNext(reminder -> {
                    outMessages.put(reminder.getMessage().getMessageId(), reminder);
                    unacknowledgedServerPackets.addLast(reminder.getMessage().getMessageId());
                })
                .transform(this::deliver)
                .then();
    }

    @Override
    public Mono<Void> resend(Publisher<Long> ids) {
        return Flux.from(ids)
                .doOnNext(id -> {
                    if (log.isTraceEnabled()) {
                        log.trace("Attempt to resend message: {}", id);
                    }
                })
                .map(outMessages::get)
                .transform(this::deliver)
                .then();
    }

    @Override
    public Mono<Void> acknowledge(Publisher<Long> messageIds) {
        return Flux.from(messageIds)
                .doOnNext(messageId -> {
                    if (log.isTraceEnabled()) {
                        log.trace("Attempt to ack message: {}", messageId);
                    }
                    outMessages.remove(messageId);
                })
                .then();
    }

    @Override
    public Mono<Void> cancelRpc(Publisher<Long> messagesIds) {
        return Flux.from(messagesIds)
                .doOnNext(messageId -> {
                    if (log.isTraceEnabled()) {
                        log.trace("Attempt to cancel rpc message: {}", messageId);
                    }
                    outMessages.remove(messageId);
                })
                .then();
    }

    @Override
    public Mono<Void> destroy() {
        return Mono.defer(() -> {
            if (log.isDebugEnabled()) {
                log.debug("Closing " + this);
            }
            IN_SEQ_UPDATER.set(this, -1);
            OUT_SEQ_UPDATER.set(this, -1);

            outMessages.clear();

            state.set(State.CLOSED);

            if (task.get() != null) {
                task.get().dispose();
            }
            noConnectionSince = Instant.now();

            return closeInternal().then(Mono.fromRunnable(() -> delegate.set(null)));
        });
    }

    protected Mono<Void> closeInternal() {
        return Optional.ofNullable(delegate.get()).map(TransportConnection::close)
                .orElse(Mono.empty());
    }

    protected Mono<Void> sendInternal(Publisher<ProtoMessage> messages) {
        Flux<MTProtoObject> frames = Flux.from(messages).flatMap(this::encrypt)
                .map(message -> new ProtoPackage(id.getAuthId(), id.getSessionId(), message));

        return Optional.ofNullable(delegate.get())
             //   .filter(TransportConnection::isOpen)
                .map(connection -> connection.send(frames))
                .orElse(Mono.empty());
    }

    protected Mono<Void> schedule() {
        final Instant now = Instant.now();
        return Flux.fromIterable(outMessages.values())
                .filter(schedule -> schedule.getDeliverAt().plus(ackTimeout).isBefore(now))
                .transform(this::deliver)
                .then();
    }

    protected Mono<Void> deliver(Publisher<Schedule> messages) {
        return Flux.from(messages)
                .collectList()
                .flatMap(schedules -> {
                    final List<ProtoMessage> toSend = new ArrayList<>(0);

                    for (Schedule schedule : schedules) {
                        schedule.updateDelivered(true, Instant.now());
                        toSend.add(schedule.getMessage());
                    }

                    return sendInternal(Mono.just(new ProtoMessage(messageId.getAndIncrement(),
                            new Container(toSend.toArray(new ProtoMessage[0])).toByteArray())));
                })
                .then();
    }

    @Override
    public Publisher<ProtoMessage> encrypt(ProtoMessage plain) {
        return Mono.fromCallable(() -> new ProtoMessage(new BinaryInputStream(doEncrypt(plain.toByteArray()))))
                .onErrorResume((ex) -> {
                    log.warn("Unexpected error due encrypting package '{}'", plain, ex);
                    return Mono.error(new MTProtoException("Cannot encrypt packet"));
                });
    }

    @Override
    public Publisher<ProtoMessage> decrypt(ProtoMessage encrypted) {
        return Mono.fromCallable(() -> new ProtoMessage(new BinaryInputStream(doDecrypt(encrypted.toByteArray()))))
                .onErrorResume((ex) -> {
                    log.warn("Unexpected error due decrypting proto message", ex);
                    return Mono.error(new MTProtoException("Cannot decrypt EncryptedPackage"));
                });
    }

    protected byte[] doEncrypt(final byte[] data) throws IOException {
        OUT_SEQ_UPDATER.incrementAndGet(this);

        byte[] ruIv = new byte[16];
        Crypto.nextBytes(ruIv);
        byte[] usIv = new byte[16];
        Crypto.nextBytes(usIv);

        byte[] ruCipherText = this.serverRUEncryptor.encryptPackage(ByteStrings.longToBytes(outSeq), ruIv,
                ByteStrings.substring(data, 0, data.length));
        byte[] ruPackage = new EncryptedCBCPackage(ruIv, ruCipherText).toByteArray();

        byte[] usCipherText = this.serverUSEncryptor.encryptPackage(ByteStrings.longToBytes(outSeq), usIv,
                ruPackage);
        byte[] usPackage = new EncryptedCBCPackage(usIv, usCipherText).toByteArray();

        EncryptedPackage encryptedPackage = new EncryptedPackage(outSeq, usPackage);
        return encryptedPackage.toByteArray();
    }

    protected byte[] doDecrypt(final byte[] data) throws IOException {
        EncryptedPackage encryptedPackage = new EncryptedPackage(new BinaryInputStream(data));

        IN_SEQ_UPDATER.incrementAndGet(this);

        int inSeq = (int) encryptedPackage.getSeqNumber();
        if (inSeq != this.inSeq) {
            throw new IOException("Expected " + this.inSeq + ", got: " + inSeq);
        }

        EncryptedCBCPackage usEncryptedPackage =
                new EncryptedCBCPackage(new BinaryInputStream(encryptedPackage.getEncryptedPackage()));

        byte[] ruPackage = this.clientUSDecryptor.decryptPackage(ByteStrings.longToBytes(this.inSeq),
                usEncryptedPackage.getIv(), usEncryptedPackage.getEncryptedContent());
        EncryptedCBCPackage ruEncryptedPackage = new EncryptedCBCPackage(new BinaryInputStream(ruPackage));

        byte[] plain = this.clientRUDecryptor.decryptPackage(ByteStrings.longToBytes(this.inSeq),
                ruEncryptedPackage.getIv(), ruEncryptedPackage.getEncryptedContent());
        return plain;
    }

    private static class Schedule {

        private boolean delivered;
        private Instant deliverAt;
        private final ProtoMessage message;

        private Schedule(ProtoMessage message) {
            this.message = message;
        }

        public void updateDelivered(boolean delivered, Instant deliverAt) {
            this.delivered = delivered;
            this.deliverAt = deliverAt;
        }

        public boolean isDelivered() {
            return delivered;
        }

        public Instant getDeliverAt() {
            return deliverAt;
        }

        public void setDeliverAt(Instant deliverAt) {
            this.deliverAt = deliverAt;
        }

        public void setDelivered(boolean delivered) {
            this.delivered = delivered;
        }

        public ProtoMessage getMessage() {
            return message;
        }
    }
} 
