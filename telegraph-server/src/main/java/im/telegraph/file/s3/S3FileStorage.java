package im.telegraph.file.s3;

import im.telegraph.file.FileStorage;
import im.telegraph.file.FileStorageException;
import org.reactivestreams.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuples;
import reactor.util.retry.Retry;
import reactor.util.retry.RetryBackoffSpec;
import software.amazon.awssdk.core.SdkResponse;
import software.amazon.awssdk.core.async.AsyncRequestBody;
import software.amazon.awssdk.core.async.AsyncResponseTransformer;
import software.amazon.awssdk.core.exception.SdkException;
import software.amazon.awssdk.http.SdkHttpResponse;
import software.amazon.awssdk.services.s3.S3AsyncClient;
import software.amazon.awssdk.services.s3.model.*;
import software.amazon.awssdk.services.s3.presigner.S3Presigner;
import software.amazon.awssdk.services.s3.presigner.model.GetObjectPresignRequest;
import software.amazon.awssdk.services.s3.presigner.model.PutObjectPresignRequest;
import software.amazon.awssdk.transfer.s3.S3TransferManager;
import software.amazon.awssdk.transfer.s3.model.DownloadDirectoryRequest;
import software.amazon.awssdk.transfer.s3.model.DownloadFileRequest;
import software.amazon.awssdk.transfer.s3.model.FailedFileDownload;
import software.amazon.awssdk.transfer.s3.model.UploadRequest;
import software.amazon.awssdk.transfer.s3.progress.LoggingTransferListener;

import java.io.File;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.file.Path;
import java.time.Duration;
import java.time.Instant;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class S3FileStorage implements FileStorage {

    private static final Logger log = LoggerFactory.getLogger(S3FileStorage.class);

    private static final String DEFAULT_MEDIA_TYPE = "application/octet-stream";

    private final RetryBackoffSpec retryAwsException = Retry.backoff(3, Duration.ofMillis(2_000))
            .filter(SdkException.class::isInstance)
            .doBeforeRetry(retry -> log.debug("Retrying after SdkException", retry.failure()));

    private final S3AsyncClient s3AsyncClient;
    private final S3TransferManager transferManager;
    private final S3Presigner s3Presigner;
    private final String bucket;

    /**
     * Create a {@link S3FileStorage} with the given parameters.
     *
     * @param client          the s3 async client
     * @param s3Presigner     the s3 presigner
     * @param transferManager the s3 async transfer manager
     */
    public S3FileStorage(String bucket, S3AsyncClient client, S3Presigner s3Presigner, S3TransferManager transferManager) {
        this.bucket = bucket;
        this.s3AsyncClient = Objects.requireNonNull(client);
        this.s3Presigner = Objects.requireNonNull(s3Presigner);
        this.transferManager = Objects.requireNonNull(transferManager);
    }

    @Override
    public Mono<Void> upload(String fileName, Path filePath) {
        return doUpload0(fileName, filePath);
    }

    @Override
    public Mono<Void> upload(String fileName, byte[] payload) {
        return doUpload0(fileName, payload);
    }

    @Override
    public Mono<Void> upload(String fileName, Publisher<ByteBuffer> payload) {
        return doUpload0(fileName, payload);
    }

    protected Mono<Void> doUpload0(String fileName, Object payload) {
        final UploadRequest uploadRequest = UploadRequest.builder()
                .putObjectRequest(b -> b.bucket(bucket).key(fileName))
                .addTransferListener(LoggingTransferListener.create())
                .requestBody(toRequestBody(payload))
                .build();

        return Mono.fromFuture(transferManager.upload(uploadRequest).completionFuture())
                .doOnNext(completedUpload -> checkUploadResponse(completedUpload.response()))
                .subscribeOn(Schedulers.boundedElastic())
                .onErrorResume(ex -> {
                    log.warn("Unexpected error due uploading file: ({}) to s3 bucket: {}", fileName, bucket, ex);
                    return Mono.error(new FileStorageException(String.format("Error upload file [%s]" +
                            " to s3 bucket[%s] storage", fileName, bucket), ex));
                })
                .retryWhen(retryAwsException)
                .then();
    }

    @Override
    public Mono<Tuple2<URL, Instant>> getFileUploadUrl(String fileName, Duration expire) {
        final PutObjectRequest putObjectRequest = PutObjectRequest.builder()
                .bucket(bucket)
                .key(fileName)
                .contentType(DEFAULT_MEDIA_TYPE)
                .build();

        final PutObjectPresignRequest presignRequest = PutObjectPresignRequest.builder()
                .putObjectRequest(putObjectRequest)
                .signatureDuration(expire)
                .build();

        return Mono.fromCallable(() -> s3Presigner.presignPutObject(presignRequest))
                .map(result -> Tuples.of(result.url(), result.expiration()));
    }

    @SuppressWarnings("unchecked")
    protected AsyncRequestBody toRequestBody(Object payload) {
        if (payload instanceof Path) {
            return AsyncRequestBody.fromFile((Path) payload);
        } else if (payload instanceof File) {
            return AsyncRequestBody.fromFile((File) payload);
        } else if (payload instanceof ByteBuffer) {
            return AsyncRequestBody.fromByteBuffer((ByteBuffer) payload);
        } else if (payload instanceof byte[]) {
            return AsyncRequestBody.fromBytes((byte[]) payload);
        } else if (payload instanceof String) {
            return AsyncRequestBody.fromString((String) payload);
        } else if (payload instanceof Publisher<?>) {
            return AsyncRequestBody.fromPublisher((Publisher<ByteBuffer>) payload);
        } else {
            throw new FileStorageException("Wrong file type " + payload.getClass());
        }
    }

    @Override
    public Mono<Void> delete(String fileName) {
        DeleteObjectRequest request = DeleteObjectRequest.builder().bucket(bucket).key(fileName).build();

        return Mono.fromFuture(s3AsyncClient.deleteObject(request))
                .doOnError(ex -> log.warn("Unexpected error due deleting file: ({}) from s3 bucket: {}",
                        fileName, bucket, ex))
                .then();
    }

    @Override
    public Mono<Void> delete(Publisher<String> fileKeys) {
        return Flux.from(fileKeys)
                .collectList()
                .flatMap(keys -> {
                    final Set<ObjectIdentifier> identifiers = keys.stream()
                            .map(key -> ObjectIdentifier.builder()
                                    .key(key)
                                    .build())
                            .collect(Collectors.toSet());

                    final DeleteObjectsRequest deleteObjectsRequest = DeleteObjectsRequest.builder()
                            .bucket(bucket)
                            .delete(Delete.builder().objects(identifiers)
                                    .build())
                            .build();

                    return Mono.fromFuture(s3AsyncClient.deleteObjects(deleteObjectsRequest));
                })
                .then();
    }

    @Override
    public Mono<Tuple2<URL, Instant>> getDownloadFileUrl(String fileKey, Duration expire) {
        final GetObjectRequest getObjectRequest = GetObjectRequest.builder()
                .bucket(bucket)
                .key(fileKey)
                .build();

        final GetObjectPresignRequest presignRequest = GetObjectPresignRequest.builder()
                .getObjectRequest(getObjectRequest)
                .signatureDuration(expire)
                .build();

        return Mono.fromCallable(() -> s3Presigner.presignGetObject(presignRequest))
                .map(response -> Tuples.of(response.url(), response.expiration()));
    }

    @Override
    public Flux<ByteBuffer> downloadFile(String fileName) {
        GetObjectRequest request = GetObjectRequest.builder().bucket(bucket).key(fileName).build();

        return Mono.fromFuture(s3AsyncClient.getObject(request, AsyncResponseTransformer.toPublisher()))
                .subscribeOn(Schedulers.boundedElastic())
                .flatMapMany(response -> {
                    if (log.isTraceEnabled()) {
                        log.trace("Download file: {}, length: {}", fileName, response.response().contentLength());
                    }
                    checkDownloadResponse(response.response());
                    return Flux.from(response);
                })
                .retryWhen(retryAwsException)
                .onErrorResume(ex -> {
                    log.warn("Unexpected error due download file: ({}) from s3 bucket: {}", fileName, bucket, ex);
                    return Mono.error(new FileStorageException(String.format("Error download file[%s] " +
                            "from S3 bucket[%s]", fileName, bucket), ex));
                });
    }

    @Override
    public Mono<Void> downloadFile(String fileName, Path destination) {
        final DownloadFileRequest downloadFileRequest = DownloadFileRequest.builder()
                .destination(destination)
                .getObjectRequest(GetObjectRequest.builder()
                        .bucket(bucket)
                        .key(fileName)
                        .build())
                .build();

        return Mono.fromFuture(transferManager.downloadFile(downloadFileRequest).completionFuture())
                .doOnNext(completedFileDownload -> checkDownloadResponse(completedFileDownload.response()))
                .subscribeOn(Schedulers.boundedElastic())
                .retryWhen(retryAwsException)
                .onErrorResume(ex -> {
                    log.warn("Unexpected error due downloading file: {} to path: {}", fileName, destination);
                    return Mono.error(new FileStorageException(
                            String.format("Error downloading file [%s] from S3 bucket [%s]", fileName, bucket), ex));
                })
                .then();
    }

    @Override
    public Mono<Void> downloadDirectory(String fileKey, String prefix, Path destination) {
        final DownloadDirectoryRequest downloadDirectoryRequest = DownloadDirectoryRequest.builder()
                .bucket(bucket)
                .destination(destination)
                .listObjectsV2RequestTransformer(b -> b.prefix(prefix))
                .build();

        return Mono.fromFuture(transferManager.downloadDirectory(downloadDirectoryRequest).completionFuture())
                .subscribeOn(Schedulers.boundedElastic())
                .flatMap(complete -> {
                    if (complete.failedTransfers().size() > 0) {
                        log.debug("Unexpected error due downloading file: ({}) parts: '{}'",
                                fileKey, complete.failedTransfers());
                        if (log.isWarnEnabled()) {
                            for (FailedFileDownload fileDownload : complete.failedTransfers()) {
                                log.warn("Failed download file", fileDownload.exception());
                            }
                        }
                        return Mono.error(new FileStorageException(String.format("Couldn't download file[%s] " +
                                "to directory[%s]", fileKey, destination)));
                    }
                    return Mono.just(complete);
                })
                .retryWhen(retryAwsException)
                .then();
    }

    private void checkUploadResponse(SdkResponse response) {
        if (response.sdkHttpResponse() == null || !response.sdkHttpResponse().isSuccessful()) {
            throw new IllegalStateException("Failed to upload file");
        }
    }

    private void checkDownloadResponse(GetObjectResponse response) {
        SdkHttpResponse sdkResponse = response.sdkHttpResponse();
        if (sdkResponse == null || !sdkResponse.isSuccessful()) {
            throw new IllegalStateException("Failed to download file code");
        }
    }

    @Override
    public String toString() {
        return "S3FileStorage{" +
                "bucket='" + bucket + '\'' +
                '}';
    }
}
