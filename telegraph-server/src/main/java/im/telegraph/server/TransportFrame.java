package im.telegraph.server;

import im.telegraph.engine.serialization.DataInputStream;
import im.telegraph.engine.serialization.DataOutputStream;
import im.telegraph.protocol.model.mtproto.ProtoObject;

import java.io.IOException;

public class TransportFrame extends ProtoObject {

	private int packageIndex;
	private int header;
	private int size;
	private byte[] payload;
	private int checksum;

	public TransportFrame(int packageIndex, int header, int size, byte[] payload, int checksum) {
		this.packageIndex = packageIndex;
		this.header = header;
		this.size = size;
		this.payload = payload;
		this.checksum = checksum;
	}

	public int getPackageIndex() {
		return packageIndex;
	}

	public int getHeader() {
		return header;
	}

	public int getSize() {
		return size;
	}

	public byte[] getPayload() {
		return payload;
	}

	public int getChecksum() {
		return checksum;
	}

	@Override
	public void writeObject(DataOutputStream outputStream) throws IOException {
		outputStream.writeInt32(packageIndex);
		outputStream.writeByte(header);
		outputStream.writeInt32(size);
		outputStream.writeBytes(payload, 0, payload.length);
		outputStream.writeInt32(checksum);
	}

	@Override
	public ProtoObject readObject(DataInputStream inputStream) throws IOException {
		this.packageIndex = inputStream.readInt();
		this.header = inputStream.readByte();
		this.size = inputStream.readInt();
		this.payload = inputStream.readBytes(size);
		this.checksum = inputStream.readInt();
		return this;
	}

	@Override
	public String toString() {
		return "TransportPackage{" + "packageIndex=" + packageIndex + ", header=" + header + ", size=" + size
				+ ", checksum=" + checksum + '}';
	}
}
