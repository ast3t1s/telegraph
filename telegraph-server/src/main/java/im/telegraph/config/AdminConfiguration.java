package im.telegraph.config;

import im.telegraph.admin.route.AdminApiServer;
import im.telegraph.admin.route.Routes;
import im.telegraph.common.SSLFactory;
import im.telegraph.common.property.admin.AdminApiProperties;
import im.telegraph.domain.entity.User;
import im.telegraph.service.UserService;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * @author Ast3t1s
 */
@Configuration
public class AdminConfiguration {

    private final AdminApiProperties properties;

    public AdminConfiguration(TelegraphProperties properties, UserService userService) {
        this.properties = properties.getAdmin();
        this.userService = userService;
    }

    private final UserService userService;

    //@Bean(initMethod = "start", destroyMethod = "stop")
    public AdminApiServer adminApiServer(List<Routes> routes) {
        return AdminApiServer.builder()
                .host(properties.getHost())
                .port(properties.getPort())
                .routes(routes)
                .authHandle((credentials)-> userService.authenticate(credentials.getUsername(), credentials.getPassword())
                        .filter(user -> user.getRole() == User.Role.ROLE_ADMIN)
                        .hasElement().onErrorReturn(false))
                .ssl(properties.getSsl().isEnabled() ? SSLFactory.buildSslServerContext(properties.getSsl()) : null)
                .build();
    }

} 
