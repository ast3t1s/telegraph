package im.telegraph.service.generator;

import im.telegraph.domain.entity.Id;

/**
 * @author Ast3t1s
 */

public interface IdGenerator {

    /**
     * Generate an id based on the given entity
     * @return the entity id
     */
    Id generateId();
}
