package im.telegraph.protocol.model.transport;

import im.telegraph.engine.serialization.DataInputStream;
import im.telegraph.engine.serialization.DataOutputStream;
import im.telegraph.server.MTProtoObject;

import java.io.IOException;

public abstract class Handshake extends MTProtoObject {

	private int mtprotoVersion;
	private int apiMajorVersion;
	private int apiMinorVersion;
	private byte[] handshakeData;

	public Handshake(DataInputStream inputStream) throws IOException {
		super(inputStream);
	}

	public Handshake(int mtprotoVersion, int apiMajorVersion, int apiMinorVersion, byte[] handshakeData) {
		this.mtprotoVersion = mtprotoVersion;
		this.apiMajorVersion = apiMajorVersion;
		this.apiMinorVersion = apiMinorVersion;
		this.handshakeData = handshakeData;
	}

	public int getMtprotoVersion() {
		return mtprotoVersion;
	}

	public int getApiMajorVersion() {
		return apiMajorVersion;
	}

	public int getApiMinorVersion() {
		return apiMinorVersion;
	}

	public byte[] getHandshakeData() {
		return handshakeData;
	}

	@Override
	public void writeObject(DataOutputStream outputStream) throws IOException {
		outputStream.writeByte(mtprotoVersion);
		outputStream.writeByte(apiMajorVersion);
		outputStream.writeByte(apiMinorVersion);
		outputStream.writeInt32(handshakeData.length);
		outputStream.writeBytes(handshakeData, 0, handshakeData.length);
	}

	@Override
	public MTProtoObject readObject(DataInputStream inputStream) throws IOException {
		this.mtprotoVersion = inputStream.readByte();
		this.apiMajorVersion = inputStream.readByte();
		this.apiMinorVersion = inputStream.readByte();
		final int size = inputStream.readInt();
		this.handshakeData = inputStream.readBytes(size);
		return this;
	}
}
