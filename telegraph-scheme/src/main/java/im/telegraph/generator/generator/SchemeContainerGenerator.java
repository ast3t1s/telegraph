package im.telegraph.generator.generator;

import im.telegraph.generator.type.SchemeContainer;
import im.telegraph.generator.type.SchemeDefinition;

import java.io.IOException;

public interface SchemeContainerGenerator<T> {

    void generateFields(T generator, SchemeDefinition definition, SchemeContainer container) throws IOException;

    void generateConstructor(T generator, SchemeDefinition definition, SchemeContainer container) throws IOException;

    void generateGettersAndSetters(T generator, SchemeDefinition definition, SchemeContainer container) throws IOException;

    void generateDeserialization(T generator, SchemeDefinition definition, SchemeContainer container) throws IOException;

    void generateSerialization(T generator, SchemeDefinition definition, SchemeContainer container) throws IOException;

    void generateToString(T generator, SchemeDefinition definition, SchemeContainer container) throws IOException;
}
