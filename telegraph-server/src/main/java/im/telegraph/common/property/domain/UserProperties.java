package im.telegraph.common.property.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * @author Ast3t1s
 */
@Getter
@Setter
@Accessors(chain = true)
public class UserProperties {

    /**
     * Whether to delete use soft or logically.
     */
    private boolean softDelete = false;

    /**
     * Send email after user registered.
     */
    private boolean sendEmailAfterRegistered = false;

    /**
     * Minimum password length
     */
    private int minPasswordLength = 8;

    /**
     * Maximum password length
     */
    private int maxPasswordLength = 128;

    /**
     * Minimum username length.
     */
    private int minUsernameLength = 1;

    /**
     * Max username length
     */
    private int maxUsernameLength = 30;

    /**
     * Max bio length
     */
    private int maxBioLength;

    /**
     * Soft delete or not.
     */
    private boolean deleteUserLogically;
} 
