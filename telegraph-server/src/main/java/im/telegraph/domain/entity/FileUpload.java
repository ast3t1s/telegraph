package im.telegraph.domain.entity;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

public class FileUpload implements Serializable {

    public static final String ENTITY_NAME = FileUpload.class.getSimpleName();

    private static final long serialVersionUID = 1L;

    private final Id id;
    private final String accessSalt;
    private final String uploadKey;
    private final Long size;
    private final Instant createdAt;

    /**
     * Constructs an {@link FileUpload} instance with the given parameters.
     *
     * @param id         Unique file identifier.
     * @param accessSalt Access salt of the file.
     * @param uploadKey  Upload key of the file
     * @param size       File size, in bytes; 0 if unknown.
     * @param createdAt  Date (Unix timestamp) when file was created.
     */
    @lombok.Builder(builderClassName = "Builder", toBuilder = true)
    FileUpload(final Id id, final String accessSalt, final String uploadKey, final Long size, final Instant createdAt) {
        this.id = id;
        this.accessSalt = Objects.requireNonNull(accessSalt);
        this.uploadKey = uploadKey;
        this.size = validateFileSize(size);
        this.createdAt = Objects.requireNonNull(createdAt);
    }

    /**
     * Validate file size
     */
    private static Long validateFileSize(Long size) {
        if (size == null || size < 0) {
            throw new IllegalArgumentException("File size can't be negative or null");
        }
        return size;
    }

    public Id getId() {
        return id;
    }

    public String getAccessSalt() {
        return accessSalt;
    }

    public String getUploadKey() {
        return uploadKey;
    }

    public Long getSize() {
        return size;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FileUpload that = (FileUpload) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "FileUpload{" +
                "id=" + id +
                ", uploadKey='" + uploadKey + '\'' +
                ", size=" + size +
                ", createdAt=" + createdAt +
                '}';
    }
}
