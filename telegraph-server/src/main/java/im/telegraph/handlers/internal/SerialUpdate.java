package im.telegraph.handlers.internal;

import im.telegraph.engine.serialization.BinaryStreamObject;
import im.telegraph.engine.serialization.StreamReader;
import im.telegraph.engine.serialization.StreamWriter;
import im.telegraph.engine.serialization.io.IOContext;
import org.springframework.lang.Nullable;

import java.io.IOException;
import java.util.List;

public class SerialUpdate extends BinaryStreamObject {

    public static SerialUpdate fromBytes(byte[] data) throws IOException {
        return IOContext.fromByteArray(new SerialUpdate(), data);
    }

    private int header;

    private byte[] body;

    @Nullable
    private List<Long> users;

    @Nullable
    private List<Long> groups;

    public SerialUpdate(int header, byte[] body, @Nullable List<Long> users, @Nullable List<Long> groups) {
        this.header = header;
        this.body = body;
        this.users = users;
        this.groups = groups;
    }

    public SerialUpdate() {
    }

    public int getHeader() {
        return header;
    }

    public byte[] getBody() {
        return body;
    }

    @Nullable
    public List<Long> getUsers() {
        return users;
    }

    @Nullable
    public List<Long> getGroups() {
        return groups;
    }

    public void setHeader(int header) {
        this.header = header;
    }

    public void setBody(byte[] body) {
        this.body = body;
    }

    public void setUsers(@Nullable List<Long> users) {
        this.users = users;
    }

    public void setGroups(@Nullable List<Long> groups) {
        this.groups = groups;
    }

    @Override
    public void deserialize(StreamReader reader) throws IOException {
        this.header = reader.readInt(1);
        this.body = reader.readByteArray(2);
        this.users = reader.readLongList(3);
        this.groups = reader.readLongList(4);
    }

    @Override
    public void serialize(StreamWriter writer) throws IOException {
        writer.writeInt(1, this.header);
        if (this.body == null) {
            throw new IOException();
        }
        writer.writeBytes(2, this.body);
        if (this.users != null) {
            writer.writeLongs(3, this.users);
        }
        if (this.groups != null) {
            writer.writeLongs(4, this.groups);
        }
    }

    @Override
    public String toString() {
        return "SerialUpdate{" +
                "header=" + header +
                ", users=" + users +
                ", groups=" + groups +
                '}';
    }
}
