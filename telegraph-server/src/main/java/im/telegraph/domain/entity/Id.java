package im.telegraph.domain.entity;

import reactor.util.annotation.Nullable;

/**
 * @author Ast3t1s
 */
public class Id implements Comparable<Id> {

    private final long value;

    /**
     * Constructs an {@code ID} utilizing an <i>unsigned</i> ID.
     *
     * @param value The <i>unsigned</i> ID to construct a {@code ID}.
     * @return A constructed {@code ID} with the <i>unsigned</i> ID.
     */
    public static Id of(final long value) {
        return new Id(value);
    }

    /**
     * Constructs a {@code Id} utilizing an <i>unsigned</i> ID.
     *
     * @param value The <i>unsigned</i> ID to construct a {@code Id}.
     */
    protected Id(final long value) {
        this.value = value;
    }

    /**`
     * Gets the <i>unsigned</i> ID as a primitive long.
     *
     * @return The <i>unsigned</i> ID as long.
     */
    public long asLong() {
        return value;
    }

    /**
     * Gets the <i>unsigned</i> ID of this {@code Snowflake} as an object String.
     *
     * @return The <i>unsigned</i> ID of this {@code Snowflake} as an object String.
     */
    public String asString() {
        return Long.toUnsignedString(value);
    }

    @Override
    public int compareTo(Id other) {
        return Long.signum((value >>> 22) - (other.value >>> 22));
    }

    @Override
    public boolean equals(@Nullable final Object obj) {
        return (obj instanceof Id) && (((Id) obj).value == value);
    }

    @Override
    public int hashCode() {
        return Long.hashCode(value);
    }

    @Override
    public String toString() {
        return "Id{" + asString() + "}";
    }
}
