package im.telegraph.protocol.model.mtproto;

import im.telegraph.engine.serialization.DataInputStream;
import im.telegraph.engine.serialization.DataOutputStream;

import java.io.IOException;

public class RequestResend extends ProtoStruct {

    public static final byte HEADER = (byte) 0x09;

    private long messageId;

    public RequestResend(long messageId) {
        this.messageId = messageId;
    }

    public RequestResend(DataInputStream inputStream) throws IOException {
        super(inputStream);
    }

    public long getMessageId() {
        return messageId;
    }

    @Override
    protected byte getHeader() {
        return HEADER;
    }

    @Override
    protected void writeBody(DataOutputStream outputStream) throws IOException {
        outputStream.writeInt64(messageId);
    }

    @Override
    protected void readBody(DataInputStream inputStream) throws IOException {
        this.messageId = inputStream.readInt64();
    }
}
