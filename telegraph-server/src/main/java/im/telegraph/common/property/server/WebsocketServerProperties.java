package im.telegraph.common.property.server;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Ast3t1s
 */
public class WebsocketServerProperties extends BaseServerProperties {
    /**
     * Headers that will be send with a response header
     */
    private Map<String, String> headers = new LinkedHashMap<>();

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }
} 
