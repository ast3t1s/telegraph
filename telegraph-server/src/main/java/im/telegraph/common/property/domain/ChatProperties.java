package im.telegraph.common.property.domain;

import im.telegraph.common.property.cache.SimpleCacheProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

/**
 * @author Ast3t1s
 */
@Getter
@Setter
@Accessors(chain = true)
public class ChatProperties {

    /**
     * Maximum chat participants.
     */
    private int maxParticipants = 200;

    /**
     * Maximum title length.
     */
    private int maxTitleLength = 128;

    /**
     * Maximum chat description length.
     */
    private int maxAboutLength = 255;

    @NestedConfigurationProperty
    private SimpleCacheProperties cache = new SimpleCacheProperties();

}
