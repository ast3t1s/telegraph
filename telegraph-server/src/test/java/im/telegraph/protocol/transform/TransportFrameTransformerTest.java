package im.telegraph.protocol.transform;

import im.telegraph.server.TransportFrameTransformer;
import im.telegraph.server.TransportFrame;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;

import java.nio.ByteBuffer;
import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

public class TransportFrameTransformerTest {

    private static final int TRANSPORT_FRAME_SIZE = 1024 * 1024; //1 Mb

    private static final int MIN_PACKET_SIZE = 9; // 9 bytes

    private TransportFrameTransformer transformer;

    @BeforeEach
    public void setUp() {
        transformer = new TransportFrameTransformer(TRANSPORT_FRAME_SIZE, MIN_PACKET_SIZE, "test");
    }

    @Test
    public void testTransformPacket() {
        TransportFrame transportPackage = new TransportFrame(1, 2, 3, new byte[] {1, 2, 3}, 31);

        DefaultDataBufferFactory factory = DefaultDataBufferFactory.sharedInstance;

        ByteBuffer buffer = ByteBuffer.wrap(transportPackage.toByteArray());

        List<TransportFrame> packageList = transformer.apply(buffer);

        TransportFrame packet = packageList.get(0);

        assertThat(packageList).hasSize(1);
        assertThat(packet.getPackageIndex()).isEqualTo(1);
        assertThat(packet.getHeader()).isEqualTo(2);
        assertThat(packet.getSize()).isEqualTo(3);
        assertThat(packet.getPayload()).isEqualTo(new byte[] {1, 2, 3});
        assertThat(packet.getChecksum()).isEqualTo(31);
    }
}