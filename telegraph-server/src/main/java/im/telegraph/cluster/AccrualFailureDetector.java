package im.telegraph.cluster;

import im.telegraph.internal.remote.Address;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author Ast3t1s
 */
public class AccrualFailureDetector implements FailureDetector {

    private static final Logger log = LoggerFactory.getLogger(AccrualFailureDetector.class);

    static final Instant NO_HEARTBEAT_TIMESTAMP = Instant.EPOCH;

    private final double threshold;
    private final int maxSampleSize;
    private final Duration minStdDeviation;
    private final Duration acceptableHeartbeatPause;
    private final Duration firstHeartbeatEstimate;
    private final Clock clock;

    private final AtomicReference<State> state = new AtomicReference<>();

    public AccrualFailureDetector(double threshold, int maxSampleSize, Duration minStdDeviation,
                                  Duration acceptableHeartbeatPause, Duration firstHeartbeatEstimate) {
        this(threshold, maxSampleSize, minStdDeviation, acceptableHeartbeatPause, firstHeartbeatEstimate, Clock.systemUTC());
    }

    /**
     * Constructs an instance of {@link AccrualFailureDetector} with the given parameters.
     *
     * @param threshold                      A low threshold is prone to generate many wrong suspicions but ensures
     *                                       a quick detection in the event of a real crash. Conversely, a high threshold
     *                                       generates fewer mistakes but needs more time to detect actual crashes
     * @param maxSampleSize                  Number of samples to use for calculation of mean and standard deviation of
     *                                       inter-arrival times.
     * @param minStdDeviation               Minimum standard deviation to use for the normal distribution used when
     *                                       calculating phi. Too low standard deviation might result in too much sensitivity
     *                                       for sudden, but normal, deviations in heartbeat inter arrival times.
     * @param acceptableHeartbeatPause      Duration corresponding to number of potentially lost/delayed
     *                                       heartbeats that will be accepted before considering it to be an anomaly.
     *                                       This margin is important to be able to survive sudden, occasional, pauses
     *                                       in heartbeat arrivals, due to for example garbage collect or network drop.
     * @param firstHeartbeatEstimate        Bootstrap the stats with heartbeats that corresponds to this duration,
     *                                       with a with rather high standard deviation (since environment is unknown
     *                                       in the beginning)
     * @param clock                         The clock, returning current time in milliseconds, but can be faked for testing
     *                                       purposes. It is only used for measuring intervals (duration).
     */
    public AccrualFailureDetector(double threshold, int maxSampleSize, Duration minStdDeviation,
                                  Duration acceptableHeartbeatPause, Duration firstHeartbeatEstimate, Clock clock) {
        if (threshold <= 0) {
            throw new IllegalArgumentException("Threshold must be a positive number");
        }
        if (minStdDeviation.isZero() || minStdDeviation.isNegative()) {
            throw new IllegalArgumentException("MinStdDeviation must be a positive number");
        }
        if (acceptableHeartbeatPause.isZero() || acceptableHeartbeatPause.isNegative()) {
            throw new IllegalArgumentException("AcceptableHeartbeatPause must be a positive number");
        }
        if (firstHeartbeatEstimate.isZero() || firstHeartbeatEstimate.isNegative()) {
            throw new IllegalArgumentException("FirstHeartbeatEstimate must be a positive number");
        }
        this.threshold = threshold;
        this.maxSampleSize = maxSampleSize;
        this.minStdDeviation = minStdDeviation;
        this.acceptableHeartbeatPause = acceptableHeartbeatPause;
        this.firstHeartbeatEstimate = firstHeartbeatEstimate;
        this.clock = Objects.requireNonNull(clock);

        this.state.set(new State(0, Collections.emptyMap(), Collections.emptyMap()));
    }

    private HeartbeatHistory firstHeartbeat() {
        long firstHeartbeatEstimateMillis = firstHeartbeatEstimate.toMillis();
        long stdDeviationMillis = firstHeartbeatEstimateMillis / 4;
        HeartbeatHistory heartbeatHistory = new HeartbeatHistory(maxSampleSize);
        heartbeatHistory.add(firstHeartbeatEstimateMillis - stdDeviationMillis);
        heartbeatHistory.add(firstHeartbeatEstimateMillis + stdDeviationMillis);
        return heartbeatHistory;
    }

    @Override
    public boolean isAvailable(Address address) {
        return phi(address) < threshold;
    }

    @Override
    public boolean isMonitoring(Address address) {
        return Optional.ofNullable(state.get())
                .map(state1 -> state1.getTimestamps().get(address))
                .isPresent();
    }

    @Override
    public void heartbeat(Address address) {
        if (isMonitoring(address)) {
            log.debug("address from member: [{}]", address);
        } else {
            log.info("First heartbeat from connection [{}] ", address);
        }

        long timestampMillis = clock.millis();
        State oldState = state.get();

        HeartbeatHistory newHistory;

        Long latestTimestamp = oldState.getTimestamps().get(address);
        if (latestTimestamp != null) {
            long interval = timestampMillis - latestTimestamp;
            newHistory = oldState.getHistory().get(address);
            newHistory.add(interval);
        } else {
            newHistory = firstHeartbeat();
        }

        Map<Address, HeartbeatHistory> newHistories = new HashMap<>(oldState.getHistory());
        newHistories.put(address, newHistory);

        Map<Address, Long> timestamps = new HashMap<>(oldState.getTimestamps());
        timestamps.put(address, timestampMillis);

        State newState = oldState.copy(oldState.getVersion() + 1, newHistories, timestamps);

        if (!state.compareAndSet(oldState, newState)) {
            heartbeat(address);
        }
    }

    private double phi(Address address) {
        State oldState = state.get();
        Long oldTimestamps = oldState.getTimestamps().get(address);

        if (oldTimestamps == null || oldTimestamps == NO_HEARTBEAT_TIMESTAMP.toEpochMilli()) {
            return 0.0;
        }

        HeartbeatHistory heartbeatHistory = oldState.getHistory().get(address);

        long timeDiffMillis = clock.millis() - oldTimestamps;
        double meanMillis = heartbeatHistory.mean();
        double stdDeviationMillis = ensureValidStdDeviation(heartbeatHistory.stdDeviation());

        double phi = phi(timeDiffMillis, meanMillis + acceptableHeartbeatPause.toMillis(), stdDeviationMillis);

        if (phi > 1.0 && timeDiffMillis < (acceptableHeartbeatPause.toMillis() + 5000)) {
            log.info("Phi value [{}] for address [{}], after [{} ms], based on [{}]",
                    phi, address, timeDiffMillis, "N(" + meanMillis + ", " + stdDeviationMillis +")");
        }

        return phi;
    }

    private double ensureValidStdDeviation(double stdDeviationMillis) {
        return Math.max(stdDeviationMillis, (double) minStdDeviation.toMillis());
    }

    /**
     * Calculation of phi, derived from the Cumulative distribution function for
     * N(mean, stdDeviation) normal distribution, given by
     * 1.0 / (1.0 + math.exp(-y * (1.5976 + 0.070566 * y * y)))
     * where y = (x - mean) / standard_deviation
     * This is an approximation defined in β Mathematics Handbook (Logistic approximation).
     * Error is 0.00014 at +- 3.16
     * The calculated value is equivalent to -log10(1 - CDF(y))
     */
    @SuppressWarnings("checkstyle:magicnumber")
    private static double phi(long timeDiffMillis, double meanMillis, double stdDeviationMillis) {
        double y = (timeDiffMillis - meanMillis) / stdDeviationMillis;
        double e = Math.exp(-y * (1.5976 + 0.070566 * y * y));
        if (timeDiffMillis > meanMillis) {
            return -Math.log10(e / (1.0 + e));
        } else {
            return -Math.log10(1.0 - 1.0 / (1.0 + e));
        }
    }
    
    @Override
    public void remove(Address address) {
        log.debug("Remove address [{}]", address);
        State oldState = state.get();

        if (oldState.getHistory().containsKey(address)) {
            HashMap<Address, HeartbeatHistory> newHistory = new HashMap<>(oldState.getHistory());
            newHistory.remove(address);

            HashMap<Address, Long> newTimestamps = new HashMap<>(oldState.getTimestamps());
            newTimestamps.remove(address);
            State newState = oldState.copy(oldState.getVersion() + 1, newHistory, newTimestamps);

            // if we won the race then update else try again
            if (!state.compareAndSet(oldState, newState)) {
                remove(address);
            }
        }
    }

    @Override
    public void reset() {
        log.debug("Resetting failure detector");
        State oldState = state.get();
        State newState = oldState.copy(oldState.getVersion() + 1, Collections.emptyMap(), Collections.emptyMap());
        // if we won the race then update else try again
        if (!state.compareAndSet(oldState, newState)) {
            reset();
        }
    }

    /**
     * Implement using optimistic lockless concurrency, all state is represented
     * by this immutable case class and managed by an AtomicReference.
     */
    private static class State {

        private final long version;
        private final Map<Address, HeartbeatHistory> history;
        private final Map<Address, Long> timestamps;

        State(long version, Map<Address, HeartbeatHistory> history, Map<Address, Long> timestamps) {
            this.version = version;
            this.history = history;
            this.timestamps = timestamps;
        }

        long getVersion() {
            return version;
        }

        Map<Address, HeartbeatHistory> getHistory() {
            return history;
        }

        Map<Address, Long> getTimestamps() {
            return timestamps;
        }

        State copy(long version, Map<Address, HeartbeatHistory> history, Map<Address, Long> timestamps) {
            return new State(version, history, timestamps);
        }
    }

    /**
     * Holds the heartbeat statistics for a specific member.
     * It is capped by the number of samples specified in `maxSampleSize`.
     * <p>
     * The stats (mean, variance, stdDeviation) are not defined for
     * for empty HeartbeatHistory, i.e. throws ArithmeticException.
     */
    private static class HeartbeatHistory {

        private final int maxSampleSize;
        private final LinkedList<Long> intervals = new LinkedList<>();
        private long intervalSum;
        private long squaredIntervalSum;

        HeartbeatHistory(int maxSampleSize) {
            if (maxSampleSize < 1) {
                throw new IllegalArgumentException("Sample size must be positive number: " + maxSampleSize);
            }
            this.maxSampleSize = maxSampleSize;
        }

        double mean() {
            return (double) intervalSum / intervals.size();
        }

        double variance() {
            double mean = mean();
            return ((double) squaredIntervalSum / intervals.size()) - (mean * mean);
        }

        double stdDeviation() {
            return Math.sqrt(variance());
        }

        void add(long interval) {
            if (intervals.size() >= maxSampleSize) {
                dropOldest();
            }
            intervals.add(interval);
            intervalSum += interval;
            squaredIntervalSum += Math.pow(interval, 2);
        }

        private void dropOldest() {
            long dropped = intervals.pollFirst();
            intervalSum -= dropped;
            squaredIntervalSum -= Math.pow(dropped, 2);
        }

    }
}
