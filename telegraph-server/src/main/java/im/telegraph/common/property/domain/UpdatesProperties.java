package im.telegraph.common.property.domain;

import org.springframework.util.unit.DataSize;

/**
 * @author Ast3t1s
 */
public class UpdatesProperties {

    private int maxBatchSize = 200;

    private DataSize maxDiffSize = DataSize.ofKilobytes(60);

    public int getMaxBatchSize() {
        return maxBatchSize;
    }

    public void setMaxBatchSize(int maxBatchSize) {
        this.maxBatchSize = maxBatchSize;
    }

    public DataSize getMaxDiffSize() {
        return maxDiffSize;
    }

    public void setMaxDiffSize(DataSize maxDiffSize) {
        this.maxDiffSize = maxDiffSize;
    }
}
