package im.telegraph.config;

import im.telegraph.common.ACLHashGenerator;
import im.telegraph.common.DateTimeFactory;
import im.telegraph.common.StateHolder;
import im.telegraph.common.health.*;
import im.telegraph.common.security.crypto.PasswordEncoder;
import im.telegraph.common.security.crypto.bcrypt.BCryptPasswordEncoder;
import im.telegraph.common.store.caffeine.CaffeineStoreService;
import im.telegraph.domain.entity.AuthSession;
import im.telegraph.domain.entity.FileUpload;
import im.telegraph.domain.entity.Message;
import im.telegraph.domain.entity.User;
import im.telegraph.domain.entity.chat.Chat;
import im.telegraph.domain.entity.photo.Photo;
import im.telegraph.domain.repository.redis.ReactiveMapSessionRepository;
import im.telegraph.domain.repository.redis.RedisSessionRepository;
import im.telegraph.protocol.cryptography.ResourceTrustedKeyStore;
import im.telegraph.protocol.cryptography.TrustedKeyStore;
import im.telegraph.service.IdGeneratorManager;
import im.telegraph.service.generator.SnowflakeIdGenerator;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.core.annotation.Order;

import java.time.Clock;
import java.time.Duration;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadLocalRandom;

@Configuration
public class TelegraphConfiguration {

    private final TelegraphProperties telegraphProperties;

    public TelegraphConfiguration(TelegraphProperties telegraphProperties) {
        this.telegraphProperties = telegraphProperties;
    }

    @Bean
    public IdGeneratorManager idGeneratorService() {
        final long workerId =  ThreadLocalRandom.current().nextLong(0, 1023);
        IdGeneratorManager idGeneratorService = new IdGeneratorManager(() -> new SnowflakeIdGenerator(workerId));
        idGeneratorService.register(Message.ENTITY_NAME);
        idGeneratorService.register(User.ENTITY_NAME);
        idGeneratorService.register(Photo.ENTITY_NAME);
        idGeneratorService.register(Chat.ENTITY_NAME);
        idGeneratorService.register(FileUpload.ENTITY_NAME);
        idGeneratorService.register(AuthSession.ENTITY_NAME);
        return idGeneratorService;
    }

    @Bean
    @ConditionalOnMissingBean(PasswordEncoder.class)
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public ACLHashGenerator hashGenerator() {
        return new ACLHashGenerator(telegraphProperties.getCrypto().getSharedSecret());
    }

    @Bean(initMethod = "start", destroyMethod = "stop")
    public HealthTracker healthCheckManager(HealthCheckIndicator indicator) {
        return new HealthTracker(telegraphProperties.getHealth(), indicator);
    }

    @Bean
    public DateTimeFactory dateTimeFactory() {
        DateTimeFactory dateTimeFactory = new DateTimeFactory();
        dateTimeFactory.setClock(Clock.systemUTC());
        return dateTimeFactory;
    }

    @Bean
    @ConditionalOnMissingBean
    @ConditionalOnProperty(value = "telegraph.domain.session.backend", havingValue = "memory")
    public ReactiveMapSessionRepository mapSessionRepository() {
        return new ReactiveMapSessionRepository(new ConcurrentHashMap<>());
    }

    @Bean
    @ConditionalOnMissingBean
    public TrustedKeyStore trustedKeyStore() {
        return new ResourceTrustedKeyStore(telegraphProperties.getCrypto());
    }

    @Bean
    public StateHolder stateHolder() {
        return new StateHolder(new CaffeineStoreService(builder -> builder.maximumSize(10_000)
                .expireAfterWrite(Duration.ofMinutes(5))));
    }

    @Configuration(proxyBeanMethods = false)
    @ConditionalOnProperty(prefix = "telegraph.domain.session", value = "backend", havingValue = "redis")
    @Lazy(false)
    public static class RedisSessionConfiguration {

        private final TelegraphProperties properties;

        public RedisSessionConfiguration(TelegraphProperties properties) {
            this.properties = properties;
        }


        @Bean
        public RedisSessionRepository sessionRepository() {
            RedisSessionRepository sessionRepository = new RedisSessionRepository();
            sessionRepository.setTimeToLive(properties.getDomain().getSession().getTtl());
            return sessionRepository;
        }

    }

    @Configuration(proxyBeanMethods = false)
    public static class HealthCheckConfiguration {

        @Bean
        @Order(10)
        public CPUHealthCheckIndicator cpuHealthCheckIndicator(TelegraphProperties properties) {
            return new CPUHealthCheckIndicator(properties.getHealth());
        }

        @Bean
        @Order(20)
        public MemoryHealthCheckIndicator memoryHealthCheckIndicator(TelegraphProperties properties) {
            return new MemoryHealthCheckIndicator(properties.getHealth());
        }

        @Bean
        @Primary
        @Order(30)
        public CompositeHealthIndicator healthIndicator(ObjectProvider<List<HealthCheckIndicator>> delegates) {
            return new CompositeHealthIndicator(delegates.getIfAvailable(Collections::emptyList));
        }

    }
}
