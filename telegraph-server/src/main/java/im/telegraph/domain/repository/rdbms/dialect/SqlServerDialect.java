package im.telegraph.domain.repository.rdbms.dialect;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Ast3t1s
 */
public class SqlServerDialect implements Dialect {

    @Override
    public String getLimit(int limit) {
        return "OFFSET 0 ROWS FETCH NEXT " + limit + " ROWS ONLY";
    }

    @Override
    public String getOffset(int offset) {
        return "OFFSET " + offset + " ROWS";
    }

    @Override
    public String getLimitOffset(int limit, int offset) {
        return String.format("OFFSET %d ROWS FETCH NEXT %d ROWS ONLY", offset, limit);
    }

    @Override
    public String buildUpsertQuery(String table, Map<String, Object> insertSpec, List<String> uniqueColumns,
                                   List<String> updateColumns, String... incrementFields) {

        String valuesStr = insertSpec.values().stream()
                .map(value -> ":" + value)
                .collect(Collectors.joining(", "));

        String columnStr = String.join(", ", insertSpec.keySet());

        String conflictCondition = uniqueColumns.stream()
                .map(column -> "target." + column + " = source." + column)
                .collect(Collectors.joining(" AND "));

        String updatePart = updateColumns.stream()
                .map(column -> "target." + column + " = source." + column)
                .collect(Collectors.joining(", "));

        StringBuilder query = new StringBuilder("MERGE ")
                .append(table).append(" target USING (VALUES (")
                .append(valuesStr).append(")) AS source (")
                .append(columnStr).append(") ON ").append(conflictCondition)
                .append(" WHEN MATCHED THEN UPDATE SET ").append(updatePart);

        if (incrementFields.length > 0) {
            String incrementExpression = Arrays.stream(incrementFields)
                    .map(value -> "target." + value + " = target." + value + " + source." + value)
                    .collect(Collectors.joining(", "));
            query.append(", ").append(incrementExpression);
        }

        query.append(" WHEN NOT MATCHED THEN INSERT (").append(columnStr).append(") VALUES (")
                .append(insertSpec.keySet().stream()
                        .map(column -> "source." + column)
                        .collect(Collectors.joining(", ")))
                .append(")");

        return query.toString();
    }

    @Override
    public String buildUpsertQueryMany(String table, List<String> insertSpec, String batchName, List<String> uniqueColumns,
                                       List<String> updateColumns, String... incrementFields) {
        String insertFields = insertSpec.stream()
                .collect(Collectors.joining(", "));

        String columnStr = String.join(", ", insertSpec);

        String conflictCondition = uniqueColumns.stream()
                .map(column -> "target." + column + " = source." + column)
                .collect(Collectors.joining(" AND "));

        StringBuilder query = new StringBuilder("MERGE ")
                .append(table).append("(VALUES :").append(batchName)
                .append(") AS source (").append(insertFields)
                .append(") ON ").append(conflictCondition);

        if (incrementFields.length > 0) {
            String incrementExpression = Arrays.stream(incrementFields)
                    .map(value -> "target." + value + " = target." + value + " + source." + value)
                    .collect(Collectors.joining(", "));
            query.append(", ").append(incrementExpression);
        }

        query.append(" WHEN MATCHED THEN INSERT (").append(columnStr)
                .append(") VALUES (").append(insertSpec.stream()
                .map(column -> "source." + column)
                .collect(Collectors.joining(", ")))
                .append(")");

        return query.toString();
    }
}
