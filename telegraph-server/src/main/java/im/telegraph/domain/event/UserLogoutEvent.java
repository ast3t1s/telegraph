package im.telegraph.domain.event;

import im.telegraph.domain.entity.Id;

/**
 * @author Ast3t1s
 */
public class UserLogoutEvent implements Event {

    private static final long serialVersionUID = 1L;

    private final Id userId;

    public UserLogoutEvent(Id userId) {
        this.userId = userId;
    }

    public Id getUserId() {
        return userId;
    }

    @Override
    public String toString() {
        return "UserLogoutEvent{" +
                "userId=" + userId +
                '}';
    }
}
