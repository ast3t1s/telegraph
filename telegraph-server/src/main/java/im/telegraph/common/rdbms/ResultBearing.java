package im.telegraph.common.rdbms;

import io.r2dbc.spi.Result;
import io.r2dbc.spi.Row;
import io.r2dbc.spi.RowMetadata;
import org.reactivestreams.Publisher;
import reactor.core.publisher.Flux;

import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * An interface indicating that a type returns results.
 *
 * @author Ast3t1s
 */
public interface ResultBearing {

    /**
     * Transforms the {@link Result}s that are returned from execution.
     *
     * @param mappingFunction a {@link Function} used to transform each {@link Result} into a {@code Publisher} of values
     * @param <T> the type of results
     * @return the values resulting from the {@link Result} transformation
     * @see #mapRow(Function)
     * @see #mapRow(BiFunction)
     */
    <T> Flux<T> mapResult(Function<Result, ? extends Publisher<? extends T>> mappingFunction);

    /**
     * Transforms each {@link Row} and {@link RowMetadata} pair into an object.
     *
     * @param mappingFunction a {@link BiFunction} used to transform each {@link Row} and {@link RowMetadata} pair into an object
     * @param <T> the type of results
     * @return the values resulting from the {@link Row} and {@link RowMetadata} transformation
     */
    default <T> Flux<T> mapRow(BiFunction<Row, RowMetadata, ? extends T> mappingFunction) {
        Objects.requireNonNull(mappingFunction, "mappingFunction must not be null");
        return mapResult(result -> result.map(mappingFunction));
    }

    /**
     * Transforms each {@link Row} into an object.
     *
     * @param mappingFunction a {@link Function} used to transform each {@link Row} into an object
     * @param <T> the type of the results
     * @return the values resulting from the {@link Row} transformation
     */
    default <T> Flux<T> mapRow(Function<Row, ? extends T> mappingFunction) {
        Objects.requireNonNull(mappingFunction, "mappingFunction must not be null");
        return mapRow((row, rowMetadata) -> mappingFunction.apply(row));
    }

} 
