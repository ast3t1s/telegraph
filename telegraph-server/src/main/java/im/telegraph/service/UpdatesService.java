package im.telegraph.service;

import im.telegraph.common.DateTimeFactory;
import im.telegraph.common.property.domain.UpdatesProperties;
import im.telegraph.config.TelegraphProperties;
import im.telegraph.domain.entity.Id;
import im.telegraph.domain.entity.SequenceUpdate;
import im.telegraph.domain.event.SequenceUpdateEvent;
import im.telegraph.domain.event.WeakUpdateEvent;
import im.telegraph.domain.repository.UpdatesRepository;
import im.telegraph.handlers.internal.CombinedDifference;
import im.telegraph.handlers.internal.GetDiffCombiner;
import im.telegraph.handlers.internal.SerialMapping;
import im.telegraph.handlers.internal.SerialUpdate;
import im.telegraph.pubsub.PubSubMediator;
import im.telegraph.rpc.base.Update;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuples;
import reactor.util.retry.Retry;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service
public class UpdatesService {

    private static final Logger log = LoggerFactory.getLogger(UpdatesService.class);

    private static final String UPDATES_TOPIC = "gio:bus:updates";

    private final Retry retryDuplicateException = Retry.backoff(Long.MAX_VALUE, Duration.ofMillis(200))
            .doBeforeRetry(s -> log.debug("Retrying {} after DataIntegrityViolationException",
                    s.totalRetries(), s.failure()));
            //.filter(DataIntegrityViolationException.class::isInstance);

    private final UpdatesRepository updatesRepository;
    private final PubSubMediator pubSubMediator;
    private final SessionService sessionService;

    private final DateTimeFactory dateFactory;
    private final UpdatesProperties updatesProperties;

    public UpdatesService(UpdatesRepository updatesRepository,
                          PubSubMediator pubSubMediator,
                          SessionService sessionService,
                          DateTimeFactory dateFactory,
                          TelegraphProperties properties) {
        this.updatesRepository = updatesRepository;
        this.pubSubMediator = pubSubMediator;
        this.sessionService = sessionService;
        this.dateFactory = dateFactory;
        this.updatesProperties = properties.getDomain().getUpdates();
    }

    /**
     * Gets user's sequence state (the last sequence update).
     *
     * @param userId the ID of the user.
     * @return A {@link Mono} where, upon successful completion, emits the {@link SequenceUpdate}. If an error is
     * received, it is emitted through the {@code Mono}.
     */
    public Mono<SequenceUpdate> findLastSeqUpdate(final Id userId) {
        return updatesRepository.findMaxSeq(userId);
    }

    /**
     * Forward update to all users sessions and return {@code SequenceUpdate}.
     *
     * @param userId the ID of the user.
     * @param update the update to deliver
     * @return A {@link Mono} where, upon successful completion, emits nothing; indicating the update has been saved.
     * If an error is received, it is emitted through the {@code Mono}.
     */
    public Mono<SequenceUpdate> forwardSeqUpdate(final Id userId, final Update update) {
        return saveUpdate(userId, update, Collections.emptyMap())
                .retryWhen(retryDuplicateException);
                //.as(operator::transactional)
              //
               /* .doOnSuccess(seqUpdate -> routeUpdate(seqUpdate, Collections.emptySet())
                        .subscribe((v) -> {
                        }, ex -> log.warn("Failed to route sequence update event '{}'", seqUpdate, ex)));*/
    }

    /**
     * Forward update to all user's sessions with some excluded sessions and return {@code SequenceUpdate}
     *
     * @param userId          the ID of the user.
     * @param excludedAuthIds the excluded session auth IDs.
     * @param update          the update to send.
     * @return A {@link Mono} where, upon successful completion, emits the created {@link SequenceUpdate}. If an error is
     * received, it is emitted through the {@code Mono}.
     */
    public Mono<SequenceUpdate> forwardSeqUpdate(final Id userId, final Set<Long> excludedAuthIds, final Update update) {
        return saveUpdate(userId, update, Collections.emptyMap())
                .retryWhen(retryDuplicateException)
                .flatMap(seqUpdate -> routeUpdate(seqUpdate, excludedAuthIds).thenReturn(seqUpdate));
    }

    /**
     * Send update to all devices of user with ability to customize update for specific {@param authId}.
     * Customized updates are provided via map from {@param authId} to {@param Update}. For authId's not included
     * in {@param custom} map, {@param default} update will be send.
     *
     * @param userId        the ID of the user.
     * @param update the default update.
     * @param custom        the custom map updates.
     * @return A {@link Mono} where, upon successful completion, emits {@link SequenceUpdate}; indicating the update has been saved.
     * If an error is received, it is emitted through the {@code Mono}.
     */
    public Mono<SequenceUpdate> forwardSeqUpdate(final Id userId, final Update update, final Map<Long, Update> custom) {
        return saveUpdate(userId, update, custom)
                .retryWhen(retryDuplicateException)
                .doOnSuccess(seqUpdate -> routeUpdate(seqUpdate, Collections.emptySet())
                        .subscribe((v) -> {},
                                ex -> log.warn("Unexpected error due publish seq update '{}'", seqUpdate, ex)));

    }

    /**
     * Send update to all devices of users from {@param userIds} set and return empty {@link Mono}
     *
     * @param userIds the set of user's IDs.
     * @param update  the update to deliver.
     * @return A {@link Mono} where, upon successful completion, emits nothing; indicating the update has been saved.
     * If an error is received, it is emitted through the {@code Mono}.
     */
    @SuppressWarnings("unchecked")
    public Mono<Void> forwardSeqUpdate(final Iterable<Id> userIds, final Update update) {
        final Instant now = dateFactory.getClock().instant();

        return Flux.fromIterable(userIds)
                .distinct()
                .buffer(100)
                .flatMap(users -> {
                    SerialMapping mapping = buildMapping(update, Collections.emptyMap());
                    Mono<Void>[] updatesMono = users.stream()
                            .map(userId -> updatesRepository.insert(userId, now.toEpochMilli(), mapping.toByteArray())
                                    .flatMap(seqUpdate -> routeUpdate(seqUpdate, Collections.emptySet()))
                                    .onErrorResume(e -> Mono.empty()))
                            .toArray(Mono[]::new);

                    return Flux.merge(updatesMono.length, updatesMono);
                })
                .retryWhen(retryDuplicateException)
                .then();
    }

    /**
     * Forward weak update to all devices with some excluded sessions.
     *
     * @param userId the identifier of the user.
     * @param update the update to delivery.
     * @param excluded the excluded devices.
     * @return A {@link Mono} where, upon successful completion, emits nothing; indicating the update has been saved.
     * If an error is received, it is emitted through the {@code Mono}.
     */
    public Mono<Void> forwardWeakUpdate(final Id userId, final Update update, Set<Long> excluded) {
        final String updateBody = Base64.getEncoder().encodeToString(update.toByteArray());
        final WeakUpdateEvent event = new WeakUpdateEvent(update.getHeader(), updateBody, userId,
                dateFactory.getClock().instant().toEpochMilli(), excluded);
        return pubSubMediator.publish(UPDATES_TOPIC, event);
    }

    /**
     * Forward weak update to all devices of the set of the users.
     *
     * @param userIds the set of the user's identifiers.
     * @param update the update to delivery.
     * @return A {@link Mono} where, upon successful completion, emits nothing; indicating the update has been saved.
     * If an error is received, it is emitted through the {@code Mono}.
     */
    @SuppressWarnings("unchecked")
    public Mono<Void> forwardWeakUpdate(final Set<Id> userIds, final Update update) {
        return Flux.fromIterable(userIds)
                .buffer(100)
                .flatMap(chunk -> {
                    Mono<Void>[] weakUpdatesBatch = chunk.stream()
                            .map(userId -> forwardWeakUpdate(userId, update, Collections.emptySet())
                                    .doOnError(ex -> log.warn("Unexpected error due publishing weak update", ex))
                                    .onErrorResume(ex -> Mono.empty()))
                            .toArray(Mono[]::new);

                    return Flux.merge(weakUpdatesBatch.length, weakUpdatesBatch).then();
                })
                .then();
    }

    protected Mono<SequenceUpdate> saveUpdate(Id userId, Update update, Map<Long, Update> custom) {
        final Instant now = dateFactory.getClock().instant();
        final SerialMapping mapping = buildMapping(update, custom);
        return updatesRepository.insert(userId, now.toEpochMilli(), mapping.toByteArray());
    }

    protected Mono<Void> routeUpdate(SequenceUpdate update, Set<Long> excluded) {
        SequenceUpdateEvent event = new SequenceUpdateEvent(
                update.getUserId(),
                update.getSequence(),
                update.getTimestamp(),
                excluded,
                Base64.getEncoder().encodeToString(update.getMapping()));

        return sessionService.getSessionInfo(update.getUserId())
                .flatMapIterable(sessionInfo -> sessionInfo.getDevices().values())
                .flatMap(device -> pubSubMediator.send(UPDATES_TOPIC, device.getNode(), event))
                .then();
    }

    /**
     * Get difference between the current state an the given seq.
     *
     * @param userId   Identifier of the user.
     * @param authId   Identifier of the authorization key, which represents user session.
     * @param sequence Sequence common for all user's authorizations.
     *                 Must be highest sequence number from all updates in {@link DiffAcc}.
     * @return A {@link Mono} where, upon successful completion, emits {@link DiffAcc}; indicating the updates has been accumulate.
     * If an error is received, it is emitted through the {@code Mono}.
     */
    public Mono<DiffAcc> getDifference(final Id userId, final Long authId, final int sequence) {
        return getDifference(userId, authId, sequence, updatesProperties.getMaxDiffSize().toBytes());
    }

    /**
     * Get difference between the current state an the given seq.
     *
     * @param userId   Identifier of the user.
     * @param authId   Identifier of the authorization key, which represents user session.
     * @param sequence Sequence common for all user's authorizations.
     *                 Must be highest sequence number from all updates in {@link DiffAcc}.
     * @param maxBytes Max updates size, in bytes.
     * @return A {@link Mono} where, upon successful completion, emits {@link DiffAcc}; indicating the updates has been accumulate.
     * If an error is received, it is emitted through the {@code Mono}.
     */
    public Mono<DiffAcc> getDifference(final Id userId, final Long authId, final int sequence, final Long maxBytes) {
        return updatesRepository.findSequenceAfter(userId, sequence, updatesProperties.getMaxBatchSize())
                .collectList()
                .flatMap(updates -> {
                    final AtomicInteger currentSize = new AtomicInteger(0);
                    return Flux.fromIterable(updates)
                            .doOnNext(seqUpdate -> {
                                SerialUpdate update = fromBytes(seqUpdate.getMapping()).getCustomOrDefault(authId);
                                if (log.isTraceEnabled()) {
                                    log.trace("[{}] Supply sequence {} update {} bytes",
                                            userId, seqUpdate.getSequence(), update.getBody().length);
                                }
                                currentSize.addAndGet(update.getBody().length);
                            })
                            .takeUntil(sequenceUpdate -> currentSize.get() > maxBytes)
                            .collectList()
                            .map(list -> DiffAcc.of(list, updates.size() > list.size()));
                })
                .defaultIfEmpty(DiffAcc.of(Collections.emptyList(), false));
    }

    private static SerialMapping buildMapping(Update update, Map<Long, Update> custom) {
        final Map<Long, SerialUpdate> customUpdates = custom.entrySet()
                .stream()
                .map(entry -> Tuples.of(entry.getKey(), buildSerialUpdate(entry.getValue())))
                .collect(Collectors.toMap(Tuple2::getT1, Tuple2::getT2));
        return new SerialMapping(buildSerialUpdate(update), customUpdates);
    }

    private static SerialUpdate buildSerialUpdate(Update update) {
        final CombinedDifference difference = GetDiffCombiner.buildDiff(update);

        final SerialUpdate serialUpdate = new SerialUpdate();
        serialUpdate.setHeader(update.getHeader());
        serialUpdate.setBody(update.toByteArray());
        serialUpdate.setUsers(difference.getUpdatedUsers());
        serialUpdate.setGroups(difference.getUpdatedChats());
        return serialUpdate;
    }

    private static SerialMapping fromBytes(byte[] source) {
        try {
            return SerialMapping.fromBytes(source);
        } catch (IOException e) {
            return new SerialMapping();
        }
    }

    public static final class DiffAcc {

        private final List<SequenceUpdate> updates;
        private final boolean hasMore;

        private DiffAcc(List<SequenceUpdate> updates, boolean hasMore) {
            this.updates = updates;
            this.hasMore = hasMore;
        }

        private static DiffAcc of(List<SequenceUpdate> updates, boolean hasMore) {
            return new DiffAcc(updates, hasMore);
        }

        public List<SequenceUpdate> getUpdates() {
            return updates;
        }

        public boolean isHasMore() {
            return hasMore;
        }
    }

}
