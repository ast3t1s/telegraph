package im.telegraph.handlers.mapper;

import im.telegraph.api.*;
import im.telegraph.domain.entity.Dialog;
import im.telegraph.domain.entity.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import reactor.util.annotation.Nullable;

import java.io.IOException;
import java.util.Collections;
import java.util.Objects;

@Component
public class DialogConverter {

    private static final Logger log = LoggerFactory.getLogger(DialogConverter.class);

    @Nullable
    public ApiDialog convert(Dialog dialog) {
        if (dialog == null) {
            return null;
        }
        final ApiDialog apiDialog = new ApiDialog();
        if (dialog.getPeer().isUser()) {
            apiDialog.setPeer(new ApiPeerUser(dialog.getPeer().getPeerId()));
        } else if (dialog.getPeer().isChat()) {
            apiDialog.setPeer(new ApiPeerChat(dialog.getPeer().getPeerId()));
        }
        apiDialog.setDate(dialog.getTimestamp().toEpochMilli());
        apiDialog.setUnreadCount(dialog.getUnreadCount());
        return apiDialog;
    }

    /**
     * Map {@link Message} entity tp {@link ApiHistoryMessage}
     *
     * @param message the message entity.
     * @return the instance of {@link ApiHistoryMessage}
     */
    @Nullable
    public ApiHistoryMessage convert(Dialog dialog, Message message) {
        if (dialog == null || message == null) {
            return null;
        }
        try {
            if (message.isEmpty()) {
                final ApiTextMessage emptyTextMessage = new ApiTextMessage();
                emptyTextMessage.setText("");
                emptyTextMessage.setMentions(Collections.emptyList());

                final ApiHistoryMessage emptyMessage = new ApiHistoryMessage();
                emptyMessage.setMessage(emptyTextMessage);
                emptyMessage.setRid(message.getRandomId());
                emptyMessage.setDate(message.getCreatedAt().toEpochMilli());
                emptyMessage.setSenderUid(message.getSenderId().asLong());

                return emptyMessage;
            }

            final ApiHistoryMessage historyMessage = new ApiHistoryMessage();
            historyMessage.setMessage(ApiMessage.fromBytes(message.getContent()));
            historyMessage.setRid(message.getRandomId());
            historyMessage.setDate(message.getCreatedAt().toEpochMilli());
            historyMessage.setSenderUid(message.getSenderId().asLong());

            final boolean isOut = Objects.equals(dialog.getUserId(), message.getSenderId());

            if (isOut) {
                if (message.getPeer().isUser()) {
                    if (message.getCreatedAt().toEpochMilli() <= dialog.getTimestamp().toEpochMilli()) {
                        historyMessage.setState(ApiMessageState.READ);
                    } else if (message.getCreatedAt().toEpochMilli() <= dialog.getTimestamp().toEpochMilli()) {
                        historyMessage.setState(ApiMessageState.RECEIVED);
                    } else {
                        historyMessage.setState(ApiMessageState.SENT);
                    }
                } else if (message.getPeer().isChat()) {
                    if (message.getCreatedAt().toEpochMilli() <= dialog.getTimestamp().toEpochMilli()) {
                        historyMessage.setState(ApiMessageState.READ);
                    } else if (message.getCreatedAt().toEpochMilli() <= dialog.getTimestamp().toEpochMilli()) {
                        historyMessage.setState(ApiMessageState.RECEIVED);
                    } else {
                        historyMessage.setState(ApiMessageState.SENT);
                    }
                }
            }
            return historyMessage;
        } catch (IOException e) {
            log.warn("Unexpected error while parsing api message", e);
            return null;
        }
    }

    /**
     * Map {@link Message} entity tp {@link ApiHistoryMessage}
     *
     * @param message the message entity.
     * @return the instance of {@link ApiHistoryMessage}
     */
    @Nullable
    public ApiHistoryMessage convert(Message message) {
        if (message == null) {
            return null;
        }
        try {
            if (message.isEmpty()) {
                final ApiTextMessage emptyTextMessage = new ApiTextMessage();
                emptyTextMessage.setText("");
                emptyTextMessage.setMentions(Collections.emptyList());

                final ApiHistoryMessage emptyMessage = new ApiHistoryMessage();
                emptyMessage.setMessage(emptyTextMessage);
                emptyMessage.setRid(message.getRandomId());
                emptyMessage.setDate(message.getCreatedAt().toEpochMilli());
                emptyMessage.setSenderUid(message.getSenderId().asLong());

                return emptyMessage;
            }

            final ApiHistoryMessage historyMessage = new ApiHistoryMessage();
            historyMessage.setMessage(ApiMessage.fromBytes(message.getContent()));
            historyMessage.setRid(message.getRandomId());
            historyMessage.setDate(message.getCreatedAt().toEpochMilli());
            historyMessage.setSenderUid(message.getSenderId().asLong());

            return historyMessage;
        } catch (IOException e) {
            log.warn("Unexpected error while parsing api message", e);
            return null;
        }
    }
} 
