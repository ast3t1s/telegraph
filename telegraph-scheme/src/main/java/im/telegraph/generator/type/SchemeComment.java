package im.telegraph.generator.type;

public class SchemeComment implements SchemeRecord {
    private String text;

    public SchemeComment(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
