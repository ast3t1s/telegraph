package im.telegraph.handlers.internal;

import im.telegraph.engine.serialization.BinaryStreamObject;
import im.telegraph.engine.serialization.StreamReader;
import im.telegraph.engine.serialization.StreamWriter;
import im.telegraph.engine.serialization.io.IOContext;
import org.springframework.lang.Nullable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SerialMapping extends BinaryStreamObject {

    public static SerialMapping fromBytes(byte[] data) throws IOException {
        return IOContext.fromByteArray(new SerialMapping(), data);
    }

    private SerialUpdate update;

    @Nullable
    private Map<Long, SerialUpdate> custom;

    public SerialMapping(SerialUpdate update, @Nullable Map<Long, SerialUpdate> custom) {
        this.update = update;
        this.custom = custom;
    }

    public SerialMapping() {
    }

    public SerialUpdate getUpdate() {
        return update;
    }

    @Nullable
    public Map<Long, SerialUpdate> getCustom() {
        return custom;
    }

    public SerialUpdate getCustomOrDefault(Long authId) {
        if (custom != null) {
            return custom.getOrDefault(authId, this.update);
        }
        return update;
    }

    public void setUpdate(SerialUpdate update) {
        this.update = update;
    }

    public void setCustom(@Nullable Map<Long, SerialUpdate> custom) {
        this.custom = custom;
    }

    @Override
    public void deserialize(StreamReader reader) throws IOException {
        this.update = reader.readSerializable(1, new SerialUpdate());
        List<Long> ids = reader.readLongList(2);
        List<SerialUpdate> _updates = new ArrayList<>();
        for (int i = 0; i < reader.getListSize(3); i++) {
            _updates.add(new SerialUpdate());
        }
        List<SerialUpdate> updates = reader.readSerializableList(3, _updates);

        Map<Long, SerialUpdate> _custom = new HashMap<>();
        for (int i = 0; i < ids.size(); i++) {
            _custom.put(ids.get(i), updates.get(i));
        }
        this.custom = _custom.isEmpty() ? null : _custom;
    }

    @Override
    public void serialize(StreamWriter writer) throws IOException {
        if (this.update == null) {
            throw new IOException();
        }
        writer.writeSerializable(1, this.update);;
        if (this.custom != null) {
            writer.writeLongs(2, this.custom.keySet());
            writer.writeSerializableCollection(3, this.custom.values());
        }
    }
}
