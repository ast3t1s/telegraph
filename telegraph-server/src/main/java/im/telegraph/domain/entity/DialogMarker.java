package im.telegraph.domain.entity;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

public final class DialogMarker implements Serializable {

    private static final long serialVersionUID = 1L;

    private final Id userId;
    private final Peer peer;
    private final State state;
    private final Instant timestamp;

    /**
     * Constructs an {@code DialogMarker} with an associated data.
     *
     * @param userId              the ID of the user.
     * @param peer                the ID of the peer.
     * @param state               the state
     * @param timestamp           the timestamp
     */
    @lombok.Builder(builderClassName = "Builder")
    DialogMarker(final Id userId, final Peer peer, final State state, final Instant timestamp) {
        this.userId = Objects.requireNonNull(userId);
        this.peer = Objects.requireNonNull(peer);
        this.state = Objects.requireNonNull(state);
        this.timestamp = Objects.requireNonNull(timestamp);
    }

    /**
     * Creates a {@link DialogMarker.State#RECEIVED success} button.
     *
     * @param userId The id of the user
     * @param peer The peer of the cha
     * @param timestamp The timestamp of the mark
     * @return A history box with the given data.
     */
    public static DialogMarker received(Id userId, Peer peer, Instant timestamp) {
        return of(userId, peer, State.RECEIVED, timestamp);
    }

    /**
     * Creates a {@link DialogMarker.State#READ success} button.
     *
     * @param userId The id of the user
     * @param peer The peer of the cha
     * @param timestamp The timestamp of the mark
     * @return A history box with the given data.
     */
    public static DialogMarker read(Id userId, Peer peer, Instant timestamp) {
        return of(userId, peer, State.READ, timestamp);
    }

    public static DialogMarker of(Id userId, Peer peer, State state, Instant timestamp) {
        return new DialogMarker(userId, peer, state, timestamp);
    }

    /**
     * Gets the peer {@link Peer} of the inbox.
     *
     * @return peer of the inbox
     */
    public Peer getPeer() {
        return peer;
    }

    /**
     * Gets the identifier of the user.
     *
     * @return identifier of the user
     */
    public Id getUserId() {
        return userId;
    }

    /**
     * Gets the state of the message.
     *
     * @return the state of the message
     */
    public State getState() {
        return state;
    }

    /**
     * Gets the timestamp of the mark.
     *
     * @return the timestamp of the mark
     */
    public Instant getTimestamp() {
        return timestamp;
    }

    public boolean isReceived() {
        return getState() == State.RECEIVED;
    }

    public boolean isRead() {
        return getState() == State.READ;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DialogMarker that = (DialogMarker) o;
        return Objects.equals(userId, that.userId)
                && Objects.equals(peer, that.peer);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, peer);
    }

    @Override
    public String toString() {
        return "DialogMarker{" +
                "userId=" + userId +
                ", peer=" + peer +
                ", state=" + state +
                ", timestamp=" + timestamp +
                '}';
    }

    /** Represents the various states of message inbox*/
    public enum State {

        /** Unknown state */
        UNKNOWN(-1),

        /** Received state */
        RECEIVED(0),

        /** Read state */
        READ(1);

        /** The underlying value as represented by Telegraph. */
        private final int value;

        /**
         * Constructs a {@code State}.
         *
         * @param value The underlying value as represented by Telegraph.
         */
        State(final int value) {
            this.value = value;
        }

        /**
         * Gets the underlying value as represented by Telegraph.
         *
         * @return The underlying value as represented by Telegraph.
         */
        public int getValue() {
            return value;
        }

        /**
         * Gets the inbox message state. It is guaranteed that invoking {@link #getValue()} from the
         * returned enum will equal ({@code ==}) the supplied {@code value}.
         *
         * @param value The underlying value as represented by Telegraph.
         * @return The inbox message state
         */
        public static State of(final int value) {
            return switch (value) {
                case 0 -> RECEIVED;
                case 1 -> READ;
                default -> UNKNOWN;
            };
        }
    }
}
