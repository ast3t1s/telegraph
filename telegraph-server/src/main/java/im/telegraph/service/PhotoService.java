package im.telegraph.service;

import im.telegraph.common.ACLHashGenerator;
import im.telegraph.common.DateTimeFactory;
import im.telegraph.common.Page;
import im.telegraph.common.property.domain.AvatarProperties;
import im.telegraph.config.TelegraphProperties;
import im.telegraph.domain.entity.FileReferenceId;
import im.telegraph.domain.entity.Id;
import im.telegraph.domain.entity.Peer;
import im.telegraph.domain.entity.photo.Photo;
import im.telegraph.domain.entity.photo.PhotoSize;
import im.telegraph.domain.repository.PhotoRepository;
import im.telegraph.exceptions.RpcErrors;
import im.telegraph.exceptions.RpcException;
import im.telegraph.util.BufferUtils;
import im.telegraph.util.ImageUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import reactor.function.TupleUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

@Service
public class PhotoService {

    private static final Logger log = LoggerFactory.getLogger(PhotoService.class);

    private final PhotoRepository photoRepository;

    private final FileService fileService;
    private final DateTimeFactory dateFactory;
    private final AvatarProperties avatarProperties;
    private final IdGeneratorManager idGenerator;
    private final ACLHashGenerator hashGenerator;

    public PhotoService(PhotoRepository photoRepository,
                        FileService fileService,
                        DateTimeFactory dateFactory,
                        TelegraphProperties properties,
                        IdGeneratorManager idGenerator,
                        ACLHashGenerator hashGenerator) {
        this.photoRepository = photoRepository;
        this.fileService = fileService;
        this.dateFactory = dateFactory;
        this.avatarProperties = properties.getDomain().getAvatar();
        this.idGenerator = idGenerator;
        this.hashGenerator = hashGenerator;
    }

    /**
     * Process and uploaded user avatar image from the given file.
     *
     * @param userId     the identifier of the user
     * @param fileId     the identifier of the file
     * @param accessHash the file access hash
     * @return a {@link Mono} that emits {@link Photo}.
     */
    public Mono<Photo> uploadProfilePhoto(final Id userId, final Id fileId, final long accessHash) {
        return fileService.withFileLocation(fileId, accessHash, avatarProperties.getMaxSize().toBytes())
                .flatMapMany(file -> fileService.downloadFile(file.getId()))
                .collectList()
                .map(BufferUtils::concatBuffers)
                .map(BufferUtils::asByteArray)
                .publishOn(Schedulers.boundedElastic())
                .flatMap(originalImg -> {
                    final byte[] small = ImageUtils.resizeTo(originalImg, avatarProperties.getSmallSize());
                    final byte[] large = ImageUtils.resizeTo(originalImg, avatarProperties.getLargeSize());

                    if (small == null || large == null) {
                        log.debug("Error due processing photo user {} file {}", userId, fileId);
                        return Mono.error(() -> RpcException.create(RpcErrors.IMAGE_PROCESS_FAILED));
                    }

                    final Mono<FileReferenceId> uploadSmall = fileService.uploadFile("small-photo.jpg", small);
                    final Mono<FileReferenceId> uploadBig = fileService.uploadFile("big-photo.jpg", large);

                    return Mono.zip(uploadSmall, uploadBig)
                            .map(TupleUtils.function((smallPhoto, largePhoto) -> {
                                PhotoSize photoSmall = new PhotoSize(smallPhoto,
                                        avatarProperties.getSmallSize(), avatarProperties.getSmallSize(), small.length);
                                PhotoSize photoLarge = new PhotoSize(largePhoto,
                                        avatarProperties.getLargeSize(), avatarProperties.getLargeSize(), large.length);
                                return new Photo(idGenerator.generateEntityId(Photo.ENTITY_NAME), userId, fileId,
                                        hashGenerator.nextAccessSalt(), photoSmall, photoLarge, dateFactory.getClock().instant());
                            }))
                            .flatMap(photoRepository::insert);
                });
    }

    /**
     * Retrieves the {@link Photo} of user/chat photo.
     *
     * @param photoId the identifier of the avatar
     * @return A {@link Mono} that continually emits the {@link Photo avatar} of the user/chat.
     * If an error is received, it is emitted through the {@code Mono}.
     */
    public Mono<Photo> getPhoto(final Id photoId, final long accessHash) {
        return photoRepository.findById(photoId)
                .filter(photo -> hashGenerator.generatePhotoAccessHash(photoId.asLong(), photo.getAccessSalt()) == accessHash);
    }

    /**
     * Retrieves the {@link Page} of user/chat photos.
     *
     * @param userId the peer of the chat
     * @return A {@link Mono} that continually emits the {@link Page<  Photo  > avatars} of the user/chat.
     * If an error is received, it is emitted through the {@code Flux}.
     */
    public Mono<Page<Photo>> getUserPhotos(final Id userId, final int limit, final int offset) {
        return photoRepository.findByUserId(userId, limit, offset)
                .collectList()
                .zipWith(photoRepository.countByUserId(userId))
                .map(TupleUtils.function((BiFunction<List<Photo>, Long, Page<Photo>>) Page::new));
    }

    /**
     * Delete photos of the {@link Peer peer} by the given ids.
     *
     * @param userId the given chat peer
     * @param ids    the given ids
     * @return A {@link Mono} where, upon successful completion, emits {@code List} of ids of deleted avatars;
     * indicating that avatars has been deleted. If an error is received, it is emitted through the {@code Mono}.
     */
    public Flux<Id> deletePhotos(final Id userId, final List<Id> ids) {
        return Flux.fromIterable(ids)
                .distinct()
                .buffer(100)
                .flatMap(photoIds -> photoRepository.findAllByIdIn(photoIds)
                        .collectList()
                        .flatMapMany(allPhotos -> {
                            final List<Photo> eligiblePhotos = new ArrayList<>(0);
                            final Collection<Photo> ineligiblePhotos = new ArrayList<>(0);

                            for (final Photo photo : allPhotos) {
                                if (!Objects.equals(userId, photo.getUserId())) {
                                    ineligiblePhotos.add(photo);
                                } else {
                                    eligiblePhotos.add(photo);
                                }
                            }

                            if (!ineligiblePhotos.isEmpty()) {
                                return Flux.error(() -> RpcException.create(RpcErrors.MESSAGE_FORBIDDEN));
                            }

                            final Collection<Id> eligibleIds = eligiblePhotos.stream()
                                    .distinct()
                                    .map(Photo::getId)
                                    .collect(Collectors.toList());

                            if (eligibleIds.isEmpty()) {
                                return Flux.fromIterable(eligibleIds);
                            }

                            return photoRepository.deleteAllByIdIn(eligibleIds)
                                    .doOnNext(deleted -> {
                                        if (log.isTraceEnabled()) {
                                            log.trace("Deleted user {} photos expected: {}, but got: {}",
                                                    userId, eligibleIds.size(), deleted);
                                        }
                                    })
                                    .thenMany(Flux.fromIterable(eligibleIds));
                        }));
    }
}
