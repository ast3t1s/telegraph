package im.telegraph.internal.remote.serializer;

import org.springframework.util.FastByteArrayOutputStream;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * @author Ast3t1s
 */
public class JdkSerializer implements Serializer {

    @Override
    public byte[] toBinary(Object object) throws SerializationException {
        try (final FastByteArrayOutputStream byteArrayOutputStream = new FastByteArrayOutputStream()) {
            try (final ObjectOutputStream outputStream = new ObjectOutputStream(byteArrayOutputStream)) {
                outputStream.writeObject(object);
                outputStream.flush();
                return byteArrayOutputStream.toByteArray();
            }
        } catch (IOException e) {
            throw new SerializationException("Error jdk serialization payload", e);
        }
    }

    @Override
    public Object fromBinary(byte[] array) throws SerializationException {
        try (final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(array)) {
            try (final ObjectInputStream inputStream = new ObjectInputStream(byteArrayInputStream)) {
                return inputStream.readObject();
            }
        }  catch (IOException | ClassNotFoundException e) {
            throw new SerializationException("Error deserializing message payload", e);
        }
    }

    @Override
    public int serializerId() {
        return 0;
    }
}
