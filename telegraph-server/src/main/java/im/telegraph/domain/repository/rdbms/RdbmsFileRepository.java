package im.telegraph.domain.repository.rdbms;

import im.telegraph.common.rdbms.RdbmsOperations;
import im.telegraph.domain.entity.FileUpload;
import im.telegraph.domain.entity.Id;
import im.telegraph.domain.repository.FileRepository;
import io.r2dbc.spi.*;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * @author Ast3t1s
 */
@Repository
public class RdbmsFileRepository implements FileRepository {

    private static final String INSERT_FILE_QUERY = "INSERT INTO files (access_salt, upload_key, size, created_at) " +
            "VALUES (:accessSalt, :uploadKey, :size, :createdAt)";

    private static final String GET_FILE_QUERY =
            "SELECT id, access_salt, upload_key, size, created_at FROM files WHERE id = :id";

    private static final String DELETE_BY_ID_QUERY =
            "DELETE FROM files WHERE id = :fileId";

    private final RdbmsOperations rdbmsOperations;

    public RdbmsFileRepository(ConnectionFactory connectionFactory) {
        this.rdbmsOperations = RdbmsOperations.create(connectionFactory);
    }

    @Override
    public Mono<FileUpload> insert(FileUpload fileUpload) {
        return rdbmsOperations.withHandle(handle -> handle.createUpdate(INSERT_FILE_QUERY)
                .bindMap(new FileParameterMapper().apply(fileUpload))
                .execute())
                .then(Mono.just(fileUpload));
    }

    @Override
    public Mono<FileUpload> findById(Id id) {
        return rdbmsOperations.withHandle(handle -> handle.createQuery(GET_FILE_QUERY)
                .bind("id", id.asLong())
                .mapRow(new FileRowMapper())).next();
    }

    @Override
    public Mono<Void> deleteById(Id id) {
        return rdbmsOperations.useHandle(handle -> handle.createUpdate(DELETE_BY_ID_QUERY)
                .bind("fileId", id.asLong())
                .execute());
    }

    private static class FileParameterMapper implements Function<FileUpload, Map<String, Parameter>> {

        @Override
        public Map<String, Parameter> apply(FileUpload fileUpload) {
            final Map<String, Parameter> parameters = new HashMap<>();
            parameters.put("id", Parameters.in(R2dbcType.BIGINT, fileUpload.getId().asLong()));
            parameters.put("accessSalt", Parameters.in(R2dbcType.VARCHAR, fileUpload.getAccessSalt()));
            parameters.put("uploadKey", Parameters.in(R2dbcType.VARCHAR, fileUpload.getUploadKey()));
            parameters.put("size", Parameters.in(R2dbcType.BIGINT, fileUpload.getSize()));
            parameters.put("createdAt", Parameters.in(R2dbcType.TIMESTAMP,
                    LocalDateTime.ofInstant(fileUpload.getCreatedAt(), ZoneOffset.UTC)));
            return parameters;
        }
    }

    private static class FileRowMapper implements BiFunction<Row, RowMetadata, FileUpload> {

        @Override
        public FileUpload apply(Row row, RowMetadata metadata) {
            return FileUpload.builder()
                    .id(Id.of(row.get("id", Long.class)))
                    .accessSalt(row.get("access_salt", String.class))
                    .uploadKey(row.get("upload_key", String.class))
                    .size(row.get("size", Long.class))
                    .createdAt(row.get("created_at", LocalDateTime.class).toInstant(ZoneOffset.UTC))
                    .build();
        }
    }
}
