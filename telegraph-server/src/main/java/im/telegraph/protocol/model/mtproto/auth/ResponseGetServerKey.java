package im.telegraph.protocol.model.mtproto.auth;

import im.telegraph.engine.serialization.DataInputStream;
import im.telegraph.engine.serialization.DataOutputStream;
import im.telegraph.protocol.model.mtproto.ProtoStruct;

import java.io.IOException;

public class ResponseGetServerKey extends ProtoStruct {

	public static final int HEADER = 0xE3;

	private long keyId;
	private byte[] key;

	public ResponseGetServerKey(DataInputStream inputStream) throws IOException {
		super(inputStream);
	}

	public ResponseGetServerKey(long keyId, byte[] key) {
		this.keyId = keyId;
		this.key = key;
	}

	public long getKeyId() {
		return keyId;
	}

	public byte[] getKey() {
		return key;
	}

	@Override
	protected byte getHeader() {
		return (byte) HEADER;
	}

	@Override
	protected void writeBody(DataOutputStream outputStream) throws IOException {
		outputStream.writeInt64(keyId);
		outputStream.writeProtoBytes(key);
	}

	@Override
	protected void readBody(DataInputStream inputStream) throws IOException {
		this.keyId = inputStream.readInt64();
		this.key = inputStream.readProtoBytes();
	}

}
