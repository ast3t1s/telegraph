package im.telegraph.common;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.time.Duration;
import java.util.function.Function;

import static org.mockito.Mockito.*;

public class ResettableIntervalTest {

    private final Function<Long, Mono<Void>> testFn = mock(Function.class, (tick) -> Mono.empty());

    private final ResettableInterval intervalCheck = new ResettableInterval(Schedulers.newSingle("test", true),
            Duration.ofMillis(10), Duration.ofMillis(10), this.testFn);

    @AfterEach
    public void tearDown() {
        this.intervalCheck.stop();
    }

    @Test
    public void test_check_after_being_started() throws InterruptedException {
        this.intervalCheck.start();
        Thread.sleep(500);
        verify(this.testFn, atLeastOnce()).apply(1L);
    }

    @Test
    public void test_should_not_check_when_stopped() throws InterruptedException {
        this.intervalCheck.stop();
        Thread.sleep(100);
        verify(this.testFn, never()).apply(anyLong());
    }

    @Test
    public void test_should_check_after_error() throws InterruptedException {
        when(this.testFn.apply(anyLong())).thenReturn(Mono.error(new RuntimeException("Test"))).thenReturn(Mono.empty());

        this.intervalCheck.start();
        Thread.sleep(500);
        verify(testFn, atLeast(1)).apply(2L);
    }
}