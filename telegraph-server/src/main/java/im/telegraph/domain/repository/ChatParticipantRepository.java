package im.telegraph.domain.repository;

import im.telegraph.domain.entity.Id;
import im.telegraph.domain.entity.chat.ChatParticipant;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * @author Ast3t1s
 */
public interface ChatParticipantRepository {

    /**
     * Creates a new chat participant.
     *
     * @param participant the chat participant to create
     * @return A {@link Mono} that emits the created {@link ChatParticipant} upon successful completion. If an error occurs,
     * it is emitted through the {@code Mono}.
     */
    Mono<Void> insert(ChatParticipant participant);

    /**
     * Updates an existing chat participant.
     *
     * @param participant the chat participant to update
     * @return A {@link Mono} that emits the updated {@link ChatParticipant} upon successful completion. If an error occurs,
     * it is emitted through the {@code Mono}.
     */
    Mono<Void> update(ChatParticipant participant);

    /**
     * Find a chat participant by the given chat id and user id
     *
     * @param chatId the id of the chat
     * @param userId the id of the user
     * @return A {@link Mono} where, upon successful completion, emits the {@link ChatParticipant}.If an error is received,
     * it is emitted through the {@code Mono}.
     */
    Mono<ChatParticipant> findByChatIdAndUserId(Id chatId, Id userId);

    /**
     * Finds all chat participants by the given chat ID with pagination.
     *
     * @param chatId the ID of the chat
     * @param limit the maximum number of chat members to return
     * @param offset the starting position in the result set
     * @return A {@link Flux} that emits the {@link ChatParticipant}s. If an error occurs, it is emitted through the {@code Flux}.
     */
    Flux<ChatParticipant> findAllByChatId(Id chatId, int limit, int offset);

    /**
     * Counts the participant of chat participants in the given chat ID.
     *
     * @param chatId the ID of the chat
     * @return A {@link Mono} that emits the count of chat members. If an error occurs, it is emitted through the {@code Mono}.
     */
    Mono<Long> countByChatId(Id chatId);

    /**
     * Deletes a chat member.
     *
     * @param participant the chat member to delete
     * @return A {@link Mono} that emits {@code true} if the deletion was successful, or {@code false} otherwise. If an error occurs,
     * it is emitted through the {@code Mono}.
     */
    Mono<Void> delete(ChatParticipant participant);
}
