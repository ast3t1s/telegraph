package im.telegraph.generator.type;

public class SchemePrimitiveType extends SchemeType {
    private String name;

    public SchemePrimitiveType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }
} 
