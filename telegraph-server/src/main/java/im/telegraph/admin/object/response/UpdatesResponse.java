package im.telegraph.admin.object.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.lang.Nullable;

/**
 * @author Ast3t1s
 */
public class UpdatesResponse<T> {

    @JsonProperty("seq")
    private final int sequence;
    private final long timestamp;
    @Nullable
    private final T data;

    public UpdatesResponse(int sequence, long timestamp, @Nullable T data) {
        this.sequence = sequence;
        this.timestamp = timestamp;
        this.data = data;
    }

    @Override
    public String toString() {
        return "UpdatesResponse{" +
                "sequence=" + sequence +
                ", timestamp=" + timestamp +
                ", data=" + data +
                '}';
    }
}
