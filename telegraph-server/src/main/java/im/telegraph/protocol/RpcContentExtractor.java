package im.telegraph.protocol;

import im.telegraph.api.parser.RpcParser;
import im.telegraph.api.parser.UpdatesParser;
import im.telegraph.rpc.base.Request;
import im.telegraph.rpc.base.Response;
import im.telegraph.rpc.base.Rpc;
import im.telegraph.rpc.base.Update;
import im.telegraph.protocol.model.ProtoSerializer;
import im.telegraph.protocol.model.mtproto.ProtoStruct;
import im.telegraph.protocol.model.mtproto.rpc.RpcRequest;
import org.reactivestreams.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.Exceptions;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.util.Objects;

/**
 * Utility class for reading from a {@code byte[]} and decoding its content to a {@link Publisher} of
 * {@link ProtoStruct}.
 *
 * @author Vsevolod Bondarenko
 */
public abstract class RpcContentExtractor {

    private static final Logger log = LoggerFactory.getLogger(RpcContentExtractor.class);

    private static final RpcParser RPC_PARSER = new RpcParser();

    private static final UpdatesParser UPDATES_PARSER = new UpdatesParser();

    /**
     * Attempt to extract {@link ProtoStruct} message from {@code byte[]} raw payload.
     *
     * @param content the {@code byte[]} input source.
     * @return the {@link Publisher} with {@code ProtoStruct} payload.
     */
    public static Publisher<? extends ProtoStruct> read(byte[] content) {
        Objects.requireNonNull(content);
        return Mono.create((sink) -> {
            try {
                ProtoStruct payload = ProtoSerializer.readMessagePayload(content);
                sink.success(payload);
            } catch (IOException | IllegalArgumentException ex) {
                log.warn("Error while decoding RPC message payload ({}): {}", ex.toString(), content.length);
                sink.error(Exceptions.propagate(new MTProtoException("Error parsing rpc payload")));
            }
        });
    }

    /**
     * Attempt to extract {@link ProtoStruct} from raw {@code byte[]} and cast to the given type.
     *
     * @param type the type which payload will cast.
     * @param content the {@code byte[]} input source.
     * @param <T> the type of the payload object.
     * @return the {@link Publisher} with {@code ProtoStruct} payload.
     */
    public static <T extends ProtoStruct> Publisher<T> read(Class<T> type, byte[] content) {
        Objects.requireNonNull(type);
        Objects.requireNonNull(content);
        return Mono.create((sink) -> {
            try {
                ProtoStruct payload = ProtoSerializer.readMessagePayload(content);
                sink.success(type.cast(payload));
            } catch (IOException | ClassCastException ex) {
                log.warn("Error while decoding RPC message payload ({}): {} and case to: {}", ex.toString(),
                        content.length, type);
                sink.error(Exceptions.propagate(new MTProtoException("Error parsing rpc payload")));
            }
        });
    }

    /**
     * Attempt to extract {@link RpcRequest} from raw {@code byte[]}.
     *
     * @param content the {@code byte[]} input source.
     * @return the {@link Publisher} with {@code RpcRequest} payload.
     */
    public static Publisher<RpcRequest> readRpcRequestPayload(byte[] content) {
        Objects.requireNonNull(content);
        return Mono.create((sink) -> {
            try {
                ProtoStruct rpcRequest = ProtoSerializer.readRpcRequestPayload(content);
                sink.success((RpcRequest) rpcRequest);
            } catch (IOException | ClassCastException ex) {
                log.warn("Error while decoding RPC request payload ({}): {}", ex.toString(), content.length);
                sink.error(Exceptions.propagate(new MTProtoException("Error parsing rpc request payload")));
            }
        });
    }

    /**
     * Attempt to extract {@link Request} from raw {@code byte[]}.
     *
     * @param requestType the request type.
     * @param payload the {@code byte[]} source.
     * @return the {@link Publisher} with {@code Request} payload.
     */
    @SuppressWarnings("unchecked")
    public static <T extends Request<? extends Response>> Publisher<T> readRpcRequest(int requestType, byte[] payload) {
       Objects.requireNonNull(payload);
       return Mono.create((sink) -> {
           try {
               Rpc rpc = RPC_PARSER.read(requestType, payload);
               sink.success((T) rpc);
           } catch (IOException | ClassCastException ex) {
               log.warn("Error while decoding RPC request type ({}): {}", ex.toString(), requestType);
               sink.error(Exceptions.propagate(new MTProtoException("Error parsing rpc request")));
           }
       });
    }

    /**
     * Attempt to extract {@link Update} from {@code byte[]}.
     *
     * @param type the update type.
     * @param payload the {@code byte[]} source.
     * @return the {@link Publisher} with {@code Update} payload.
     */
    public static Publisher<Update> readUpdate(final int type, final byte[] payload) {
        Objects.requireNonNull(payload);
        return Mono.create((sink) -> {
            try {
                Update update = UPDATES_PARSER.read(type, payload);
                sink.success(update);
            } catch (IOException ex) {
                log.warn("Error while decoding RPC update ({}):", type, ex);
                sink.error(Exceptions.propagate(ex));
            }
        });
    }

} 
