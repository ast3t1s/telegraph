package im.telegraph.protocol.model.mtproto;

import im.telegraph.engine.serialization.DataInputStream;
import im.telegraph.engine.serialization.DataOutputStream;

import java.io.IOException;

public class SessionLost extends ProtoStruct {

	public static final int HEADER = 0x10;

	public SessionLost(DataInputStream stream) throws IOException {
		super(stream);
	}

	public SessionLost() {
	}

	@Override
	protected byte getHeader() {
		return HEADER;
	}

	@Override
	protected void writeBody(DataOutputStream outputStream) throws IOException {

	}

	@Override
	protected void readBody(DataInputStream inputStream) throws IOException {

	}

}
