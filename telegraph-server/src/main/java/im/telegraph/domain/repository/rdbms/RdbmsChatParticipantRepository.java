package im.telegraph.domain.repository.rdbms;

import im.telegraph.common.rdbms.RdbmsOperations;
import im.telegraph.domain.entity.Id;
import im.telegraph.domain.entity.chat.ChatParticipant;
import im.telegraph.domain.repository.ChatParticipantRepository;
import io.r2dbc.spi.*;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * @author Ast3t1s
 */
@Repository
public class RdbmsChatParticipantRepository extends AbstractRdbmsRepository implements ChatParticipantRepository {

    private static final String FIELDS = "chat_id, user_id, inviter_id, status, created_at";

    private static final String TABLE_NAME = "chat_members";

    private static final String INSERT_PARTICIPANT_QUERY = "INSERT INTO " + TABLE_NAME + " (" + FIELDS + ") " +
            "VALUES (:chatId, :userId, :inviterId, :status, :createdAt)";

    private static final String PK_FILTER = "chat_id = :chatId AND user_id = :userId";

    private static final String GET_PARTICIPANT_QUERY =
            "SELECT " + FIELDS + " from " + TABLE_NAME + " WHERE " + PK_FILTER;

    private static final String GET_PARTICIPANTS_QUERY =
            "SELECT " + FIELDS + " from " + TABLE_NAME + " WHERE chat_id = :chatId ORDER by user_id";

    private static final String GET_PARTICIPANTS_COUNT_QUERY =
            "SELECT COUNT(*) from " + TABLE_NAME + " AS count WHERE chat_id = :chatId";

    private static final String DELETE_PARTICIPANT_QUERY =
            "DELETE FROM " + TABLE_NAME + " WHERE " + PK_FILTER;

    private static final String UPDATE_PARTICIPANT_QUERY =
            "UPDATE" + TABLE_NAME + " SET status = :status WHERE " + PK_FILTER;

    private final RdbmsOperations rdbmsOperations;

    private final Function<ChatParticipant, Map<String, Object>> parameterMapper = new ChatParticipantParameterMapper();
    private final BiFunction<Row, RowMetadata, ChatParticipant> rowMapper = new ChatParticipantRowMapper();

    public RdbmsChatParticipantRepository(ConnectionFactory connectionFactory) {
        super(connectionFactory);
        this.rdbmsOperations = RdbmsOperations.create(connectionFactory);
    }

    @Override
    public Mono<Void> insert(ChatParticipant participant) {
        return rdbmsOperations.useHandle(handle -> handle.createUpdate(INSERT_PARTICIPANT_QUERY)
                .bindMap(parameterMapper.apply(participant))
                .execute());
    }

    @Override
    public Mono<Void> update(ChatParticipant participant) {
        return rdbmsOperations.useHandle(handle -> handle.createUpdate(UPDATE_PARTICIPANT_QUERY)
                .bind("chatId", participant.getChatId().asLong())
                .bind("userId", participant.getUserId().asLong())
                .bind("status", participant.getStatus().getValue())
                .execute());
    }

    @Override
    public Mono<ChatParticipant> findByChatIdAndUserId(Id chatId, Id userId) {
        return rdbmsOperations.withHandle(handle -> handle.createQuery(GET_PARTICIPANT_QUERY)
                .bind("chatId", chatId.asLong())
                .bind("userId", userId.asLong())
                .mapRow(rowMapper))
                .next();
    }

    @Override
    public Flux<ChatParticipant> findAllByChatId(Id chatId, int limit, int offset) {
        return rdbmsOperations.withHandle(handle -> handle
                .createQuery(limitQuery(GET_PARTICIPANTS_QUERY, limit, offset))
                .bind("chatId", chatId.asLong())
                .mapRow(rowMapper));
    }

    @Override
    public Mono<Long> countByChatId(Id chatId) {
        return rdbmsOperations.withHandle(handle -> handle.createQuery(GET_PARTICIPANTS_COUNT_QUERY)
                .bind("chatId", chatId.asLong())
                .mapRow(row -> row.get(0, Long.class)))
                .next();
    }

    @Override
    public Mono<Void> delete(ChatParticipant participant) {
        return rdbmsOperations.useHandle(handle -> handle.createUpdate(DELETE_PARTICIPANT_QUERY)
                .bind("chatId", participant.getChatId().asLong())
                .bind("userId", participant.getUserId().asLong())
                .execute());
    }

    private static class ChatParticipantParameterMapper
            implements Function<ChatParticipant, Map<String, Object>> {

        @Override
        public Map<String, Object> apply(ChatParticipant chatMember) {
            final Map<String, Object> parameters = new HashMap<>();
            parameters.put("chatId", Parameters.in(R2dbcType.BIGINT, chatMember.getChatId().asLong()));
            parameters.put("userId", Parameters.in(R2dbcType.BIGINT, chatMember.getUserId().asLong()));
            parameters.put("inviterId", Parameters.in(R2dbcType.BIGINT, chatMember.getInviterId().asLong()));
            parameters.put("status", Parameters.in(R2dbcType.BIGINT, chatMember.getStatus().getValue()));
            parameters.put("createdAt", Parameters.in(R2dbcType.TIMESTAMP,
                    LocalDateTime.ofInstant(chatMember.getCreatedAt(), ZoneOffset.UTC)));
            return parameters;
        }
    }

    private static class ChatParticipantRowMapper implements BiFunction<Row, RowMetadata, ChatParticipant> {

        @Override
        public ChatParticipant apply(Row row, RowMetadata metadata) {
            return ChatParticipant.builder()
                    .chatId(Id.of(row.get("chat_id", Long.class)))
                    .userId(Id.of(row.get("user_id", Long.class)))
                    .inviterId(Id.of(row.get("inviter_id", Long.class)))
                    .status(ChatParticipant.Status.of(row.get("status", Integer.class)))
                    .createdAt(row.get("created_at", LocalDateTime.class).toInstant(ZoneOffset.UTC))
                    .build();
        }
    }
}