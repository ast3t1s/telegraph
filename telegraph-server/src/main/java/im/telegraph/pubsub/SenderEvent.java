package im.telegraph.pubsub;

import im.telegraph.common.ResettableInterval;
import reactor.core.scheduler.Schedulers;

import java.time.Duration;

/**
 * @author Ast3t1s
 */
public class SenderEvent {

    private final PubSubMediator eventBus;

    private final ResettableInterval interval;

    public SenderEvent(PubSubMediator eventBus) {
        this.eventBus = eventBus;
        this.interval = new ResettableInterval(Schedulers.newSingle("event", true),
                Duration.ofSeconds(5), Duration.ofSeconds(2),
             //   tick -> eventBus.send(Mono.just(new Publish(tick % 2 == 0 ? "test-1" : "test-2", "Next event " + tick))));
                tick -> eventBus.publish(tick % 2 == 0 ? "test-1" : "test-2", "Next event " + tick));
    }

    public void start() {
      /*  Flux.range(1, 2)
                .delayElements(Duration.ofSeconds(5))
                .flatMap(tick -> eventBus.publish(tick % 2 == 0 ? "test-1" : "test-2", "Next event " + tick))
                .subscribe();*/

        interval.start();
    }

    public void stop() {
        interval.stop();
    }
}
