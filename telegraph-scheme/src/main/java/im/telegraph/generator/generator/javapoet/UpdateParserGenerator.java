package im.telegraph.generator.generator.javapoet;

import com.squareup.javapoet.*;
import im.telegraph.generator.generator.Constants;
import im.telegraph.generator.generator.SchemeGenerator;
import im.telegraph.generator.type.SchemeDefinition;
import im.telegraph.generator.type.SchemeUpdate;

import javax.lang.model.element.Modifier;
import java.io.File;
import java.io.IOException;

public class UpdateParserGenerator implements SchemeGenerator {

    @Override
    public void generate(SchemeDefinition definition, String out) throws IOException {
        long start = System.currentTimeMillis();

        TypeSpec.Builder clazz = TypeSpec.classBuilder("UpdatesParser")
                .addJavadoc(Constants.NOTICE)
                .addJavadoc(Constants.DEFAULT_AUTHOR)
                .addModifiers(Modifier.PUBLIC)
                .superclass(ParameterizedTypeName.get(Constants.BASE_PARSER, Constants.TYPE_UPDATE));

        MethodSpec.Builder readBuilder = MethodSpec.methodBuilder("read")
                .addModifiers(Modifier.PUBLIC)
                .returns(Constants.TYPE_UPDATE)
                .addAnnotation(Override.class)
                .addException(IOException.class)
                .addParameter(int.class, "type")
                .addParameter(byte[].class, "payload")
                .beginControlFlow("switch (type)");

        for (SchemeUpdate update : definition.getAllUpdates()) {
            String javaName = TypeHelper.getUpdateName(update);
            readBuilder.addStatement("case $L: return $T.fromBytes(payload)",
                    update.getHeader(), ClassName.get(Constants.PACKAGE_UPDATE, javaName));
        }
        readBuilder.endControlFlow("throw new IOException()");

        clazz.addMethod(readBuilder.build());

        JavaFile.builder(Constants.PACKAGE_PARSER, clazz.build())
                .indent("    ")
                .build().writeTo(new File(out));

        System.out.println("UpdateParser generated in " + (System.currentTimeMillis() - start) + " ms");
    }
}
