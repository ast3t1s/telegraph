package im.telegraph.protocol.model.mtproto;

import im.telegraph.engine.serialization.DataInputStream;
import im.telegraph.engine.serialization.DataOutputStream;

import java.io.IOException;

public class NewSessionCreated extends ProtoStruct {

	public static final byte HEADER = (byte) 0x0C;

	private long sessionId;
	private long messageId;

	public NewSessionCreated(DataInputStream inputStream) throws IOException {
		super(inputStream);
	}

	public NewSessionCreated(long sessionId, long messageId) {
		this.sessionId = sessionId;
		this.messageId = messageId;
	}

	public long getSessionId() {
		return sessionId;
	}

	public long getMessageId() {
		return messageId;
	}

	@Override
	protected byte getHeader() {
		return HEADER;
	}

	@Override
	protected void writeBody(DataOutputStream outputStream) throws IOException {
		outputStream.writeInt64(sessionId);
		outputStream.writeInt64(messageId);
	}

	@Override
	protected void readBody(DataInputStream inputStream) throws IOException {
		sessionId = inputStream.readInt64();
		messageId = inputStream.readInt64();
	}

	@Override
	public String toString() {
		return "NewSession {" + sessionId + "}";
	}

}
