package im.telegraph.cluster.internal;

import im.telegraph.cluster.ClusterMessage;
import im.telegraph.cluster.MembershipState;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Ast3t1s
 */
public class GossipOverview implements ClusterMessage {

    private static final long serialVersionUID = 1L;

    private final List<MembershipState> membership;

    public GossipOverview(Collection<MembershipState> membership) {
        this.membership = new ArrayList<>(membership);
    }

    public Collection<MembershipState> getMembership() {
        return membership;
    }

    @Override
    public String toString() {
        return "GossipOverview{" +
                "membership=" + membership +
                '}';
    }
}
