package im.telegraph.common;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.util.function.Function;

/**
 * @author Ast3t1s
 */
public final class JacksonResources {

    private static final ObjectMapper INITIALIZER = initMapper();

    private static ObjectMapper initMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        mapper.configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL, true);
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        mapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        mapper.configure(SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true);
       /* mapper.activateDefaultTyping(
                LaissezFaireSubTypeValidator.instance,
                ObjectMapper.DefaultTyping.JAVA_LANG_OBJECT,
                JsonTypeInfo.As.WRAPPER_OBJECT);*/
        mapper.findAndRegisterModules();
        return mapper;
    }

    private final ObjectMapper objectMapper;


    /**
     * Create a default {@link com.fasterxml.jackson.databind.ObjectMapper} that allows any field visibility.
     */
    public JacksonResources() {
        this(mapper -> mapper);
    }

    /**
     * Create a custom {@link com.fasterxml.jackson.databind.ObjectMapper}, based on the defaults given by
     * {@link #JacksonResources()} ()}
     *
     * @param mapper a Function to customize the ObjectMapper to be created
     */
    public JacksonResources(Function<ObjectMapper, ObjectMapper> mapper) {
        this.objectMapper = mapper.apply(INITIALIZER);
    }

    /**
     * Create with a pre-configured {@link com.fasterxml.jackson.databind.ObjectMapper}. Using this will replace the
     * recommended default and can lead to unexpected behavior and errors.
     *
     * @param objectMapper a pre-configured ObjectMapper to use
     */
    public JacksonResources(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    /**
     * Get the {@link com.fasterxml.jackson.databind.ObjectMapper} configured by this provider.
     *
     * @return a Jackson ObjectMapper used to map POJOs to and from JSON format
     */
    public ObjectMapper getObjectMapper() {
        return objectMapper;
    }

    /**
     * Return a new {@link JacksonResources} based on this current {@link ObjectMapper} but applying the given function.
     *
     * @param transformer a mapper to enrich the current {@link ObjectMapper}
     * @return a new instance with the {@code transformer} applied
     */
    public JacksonResources withMapperFunction(Function<ObjectMapper, ObjectMapper> transformer) {
        return new JacksonResources(transformer.apply(objectMapper));
    }
} 
