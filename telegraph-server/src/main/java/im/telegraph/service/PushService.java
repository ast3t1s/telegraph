package im.telegraph.service;

import im.telegraph.domain.entity.PushDevice;
import im.telegraph.domain.repository.PushDeviceRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Objects;

@Service
public class PushService {

    private final PushDeviceRepository pushDeviceRepository;

    public PushService(PushDeviceRepository pushDeviceRepository) {
        this.pushDeviceRepository = pushDeviceRepository;
    }

    public Mono<PushDevice> saveToken(final long authId, final PushDevice.Platform platform, final String token) {
        PushDevice device = PushDevice.builder()
                .authId(authId)
                .platform(platform)
                .token(token)
                .enabled(true)
                .updatedAt(LocalDateTime.now(ZoneOffset.UTC))
                .updatedAt(LocalDateTime.now(ZoneOffset.UTC))
                .build();
        return pushDeviceRepository.create(device);
    }

    public Mono<Void> removeToken(final long authKey, final PushDevice.Platform platform, final String token) {
        return pushDeviceRepository.findByToken(token)
                .filter(ignored -> Objects.equals(ignored.getAuthKey(), authKey) && ignored.getPlatform() == platform)
                .flatMap(pushDeviceRepository::delete);
    }

    public Flux<PushDevice> findPushDevices(final long authKey) {
        return pushDeviceRepository.findAllByAuthKey(authKey);
    }
} 
