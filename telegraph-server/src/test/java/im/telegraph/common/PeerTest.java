package im.telegraph.common;

import im.telegraph.domain.entity.Id;
import im.telegraph.domain.entity.Peer;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class PeerTest {

    @Test
    public void testCreatePeer() {
        Peer userPeer = Peer.ofUser(Id.of(1));
        Peer chatPeer = Peer.ofChat(Id.of(1));

        assertThat(userPeer.isUser()).isTrue();
        assertThat(chatPeer.isChat()).isTrue();
       // assertThat(userPeer.getId()).isEqualTo(1);
       // assertThat(chatPeer.getId()).isEqualTo(1);

        long uniqueUserPeerHash = userPeer.asUniqueId();
        long uniqueChatPeerHash = chatPeer.asUniqueId();

        assertThat(userPeer).isEqualTo(Peer.fromUniqueId(uniqueUserPeerHash));
        assertThat(chatPeer).isEqualTo(Peer.fromUniqueId(uniqueChatPeerHash));
    }

    @Test
    public void invariants() {
        //assertThatThrownBy(() -> Peer.of(null, 1)).isInstanceOf(NullPointerException.class);
    }
}