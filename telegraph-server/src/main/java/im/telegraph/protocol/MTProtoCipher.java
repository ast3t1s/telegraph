package im.telegraph.protocol;

import im.telegraph.protocol.model.mtproto.ProtoMessage;
import org.reactivestreams.Publisher;

/**
 * Strategy for encrypting and decrypting {@link ProtoMessage} to a {@link Publisher} of {@link ProtoMessage}
 */
public interface MTProtoCipher {

    /**
     * Encrypt plain {@code ProtoMessage}.
     *
     * @param plain The plain {@code ProtoMessage}.
     * @return a publisher of {@code ProtoMessage} representing the encrypted proto message.
     */
    Publisher<ProtoMessage> encrypt(ProtoMessage plain);

    /**
     * Decrypt encrypted {@code ProtoMessage}.
     *
     * @param encrypted The encrypted {@code ProtoMessage}.
     * @return a publisher of {@code ProtoMessage} representing the decrypted proto message.
     */
    Publisher<ProtoMessage> decrypt(ProtoMessage encrypted);
}
