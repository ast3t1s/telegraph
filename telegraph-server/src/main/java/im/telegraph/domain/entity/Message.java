package im.telegraph.domain.entity;

import org.springframework.lang.Nullable;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import java.util.Optional;

public class Message implements Serializable {

    public static final String ENTITY_NAME = Message.class.getSimpleName();

    private static final long serialVersionUID = 1L;

    private final Id id;
    private final Peer peer;
    private final Id userId;
    private final Id senderId;
    private final Long randomId;
    private final Type type;
    private final byte[] content;
    private final Instant createdAt;
    @Nullable
    private final Instant deletedAt;

    /**
     * Constructs an {@code HistoryMessage} with an associated data.
     * @param id        Message identifier;
     * @param peer      Identifier of the dialog which message belongs.
     * @param senderId  Identifier of the sender of the message.
     * @param randomId  Message identifier given by the sender. Unique for the chat to which the message belongs.
     * @param type      Type of the message (document, json, text, etc.).
     * @param content   Content of the message.
     * @param createdAt Point in time (Unix timestamp) when the message was created.
     * @param deletedAt Point in time (Unix timestamp) when the message has been deleted.
     */
    @lombok.Builder(builderClassName = "Builder", toBuilder = true)
    Message(final Id id, final Peer peer, final Id userId, final Id senderId, final Long randomId, final Type type,
            final byte[] content, final Instant createdAt, @Nullable final Instant deletedAt) {
        this.id = id;
        this.peer = peer;
        this.userId = userId;
        this.randomId = randomId;
        this.senderId = senderId;
        this.type = type;
        this.content = content;
        this.createdAt = createdAt;
        this.deletedAt = deletedAt;
    }

    public static Message empty(Peer peer) {
        return Message.builder()
                .peer(peer)
                .randomId(0L)
                .senderId(Id.of(0))
                .type(Type.TEXT)
                .createdAt(Instant.now())
                .build();
    }

    /**
     * Returns the unique identifier of the message.
     *
     * @return the unique identifier as a {@link Long}.
     */
    public Id getId() {
        return id;
    }

    /**
     * Returns the peer bound to the message.
     *
     * @return the associated {@link Peer}.
     */
    public Peer getPeer() {
        return peer;
    }

    /**
     * Returns the ID of the user associated with this message.
     *
     * @return the {@link Id} of the associated user.
     */
    public Id getUserId() {
        return userId;
    }

    /**
     * Returns the ID of the message sender.
     *
     * @return the {@link Id} of the sender.
     */
    public Id getSenderId() {
        return senderId;
    }

    /**
     * Returns the random ID of the message.
     *
     * @return the random ID as a {@link Long}.
     */
    public Long getRandomId() {
        return randomId;
    }

    /**
     * Returns the type of the message.
     *
     * @return the {@link Type} of this object.
     */
    public Type getType() {
        return type;
    }

    /**
     * Returns the content of the message.
     *
     * @return the content as a byte array.
     */
    public byte[] getContent() {
        return content;
    }

    /**
     * Returns the creation timestamp of the message.
     *
     * @return the {@link Instant} representing when this object was created.
     */
    public Instant getCreatedAt() {
        return createdAt;
    }

    /**
     * Returns an {@link Optional} containing the deletion timestamp if this object has been deleted.
     * If the object has not been deleted, an empty {@code Optional} is returned.
     *
     * @return an {@link Optional<Instant>} containing the deletion timestamp, or empty if not deleted.
     */
    public Optional<Instant> getDeletedAt() {
        return Optional.ofNullable(deletedAt);
    }

    /**
     * Flag whether or not message is empty.
     *
     * @return whether or not message is empty.
     */
    public final boolean isEmpty() {
        return getRandomId() == 0L && getSenderId().asLong() == 0;
    }

    /**
     * Flag whether or not message has text type.
     *
     * @return flag whether or not message has text type.
     */
    public final boolean isText() {
        return getType() == Type.TEXT;
    }

    /**
     * Flag whether or not message has service type.
     *
     * @return flag whether or not message has service type.
     */
    public final boolean isService() {
        return getType() == Type.SERVICE;
    }

    /**
     * Flag whether or not message has document type.
     *
     * @return flag whether or not message has document type.
     */
    public final boolean isDocument() {
        return getType() == Type.DOCUMENT;
    }

    /**
     * Flag whether or not message content is json type.
     *
     * @return flag whether or not message content is json type.
     */
    public final boolean isJSON() {
        return getType() == Type.JSON;
    }

    /**
     * Flag whether or not message content is binary type.
     *
     * @return flag whether or not message content is binary type.
     */
    public final boolean isBinary() {
        return getType() == Type.BINARY;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        Message that = (Message) object;
        return Objects.equals(id, that.id) &&
                Objects.equals(peer, that.peer);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, peer);
    }

    /**
     * Represents the varius types of messages
     */
    public enum Type {

        /**
         * Text message
         */
        TEXT(1),

        /**
         * Service message
         */
        SERVICE(2),

        /**
         * Document message (documents, media, files, voice messages, etc.)
         */
        DOCUMENT(3),

        /**
         * JSON message
         */
        JSON(4),

        /**
         * BINARY message
         */
        BINARY(7),

        /**
         * UNSUPPORTED message
         */
        UNSUPPORTED(5),

        /**
         * Unknown type
         */
        UNKNOWN(-1);

        /**
         * The underlying value.
         */
        private final int value;

        /**
         * Constructs a {@code HistoryMessage.Type}.
         *
         * @param value The underlying value.
         */
        Type(final int value) {
            this.value = value;
        }

        /**
         * Gets the underlying value.
         *
         * @return The underlying value.
         */
        public int getValue() {
            return this.value;
        }

        /**
         * Gets the type of message. It is guaranteed that invoking {@link #getValue()} from the returned enum will be
         * equal ({@code ==}) to the supplied {@code value}.
         *
         * @param value The underlying value as represented by Telegraph.
         * @return The type of message.
         */
        public static Type of(final int value) {
            return switch (value) {
                case 1 -> TEXT;
                case 2 -> SERVICE;
                case 3 -> DOCUMENT;
                case 4 -> JSON;
                case 5 -> UNSUPPORTED;
                case 7 -> BINARY;
                default -> UNKNOWN;
            };
        }
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", peer=" + peer +
                ", userId=" + userId +
                ", senderId=" + senderId +
                ", randomId=" + randomId +
                ", type=" + type +
                ", createdAt=" + createdAt +
                ", deletedAt=" + deletedAt +
                '}';
    }
}
