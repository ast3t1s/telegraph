package im.telegraph.file.s3;

import im.telegraph.TelegraphApplication;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;
import software.amazon.awssdk.core.async.AsyncRequestBody;
import software.amazon.awssdk.core.async.AsyncResponseTransformer;
import software.amazon.awssdk.core.exception.SdkException;
import software.amazon.awssdk.services.s3.S3AsyncClient;
import software.amazon.awssdk.services.s3.S3Utilities;
import software.amazon.awssdk.services.s3.model.*;
import software.amazon.awssdk.services.s3.presigner.S3Presigner;
import software.amazon.awssdk.services.s3.presigner.model.GetObjectPresignRequest;
import software.amazon.awssdk.services.s3.presigner.model.PresignedGetObjectRequest;
import software.amazon.awssdk.services.s3.presigner.model.PresignedPutObjectRequest;
import software.amazon.awssdk.services.s3.presigner.model.PutObjectPresignRequest;

import java.net.URL;
import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.CompletableFuture;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest(classes = TelegraphApplication.class)
class S3FileStorageTest {

    @Autowired
    private S3FileStorage s3FileStoreOperations;

    @MockBean
    private S3AsyncClient s3AsyncClient;

    @MockBean
    private S3Presigner s3Presigner;

    @MockBean
    private S3Utilities s3Utilities;

    @Test
    public void test_get_file_url_correct() throws Exception {
        URL url = new URL("http://some.host.com/image.gif");

        when(this.s3Utilities.getUrl(any(GetUrlRequest.class))).thenReturn(url);

        StepVerifier.create(this.s3FileStoreOperations.getDownloadFileUrl("some-file", Duration.ofDays(1)))
                .expectSubscription()
                .assertNext(fileRef -> assertThat(fileRef.getT1().toExternalForm()).isEqualTo(url.toExternalForm()))
                .verifyComplete();

        verify(this.s3Utilities, atLeastOnce()).getUrl(any(GetUrlRequest.class));
    }

    @Test
    public void test_get_file_url_empty() throws Exception {
        assertThatThrownBy(() -> this.s3FileStoreOperations.getDownloadFileUrl(null, null).block())
                .isInstanceOf(NullPointerException.class);
    }

    @Test
    public void test_get_expired_url() throws Exception {
        PresignedGetObjectRequest getObjectRequest = mock(PresignedGetObjectRequest.class);

        when(this.s3Presigner.presignGetObject(any(GetObjectPresignRequest.class))).thenReturn(getObjectRequest);

        URL url = new URL("http://some.host.com/image.gif");
        Instant now = Instant.now();

        when(getObjectRequest.url()).thenReturn(url);
        when(getObjectRequest.expiration()).thenReturn(now);

        StepVerifier.create(this.s3FileStoreOperations.getFileUploadUrl("some-file", Duration.ofDays(1)))
                .expectSubscription()
                .assertNext(fileRef -> {
                    assertThat(fileRef.getT1().toExternalForm()).isEqualTo(url.toExternalForm());
                    assertThat(fileRef.getT2()).isEqualTo(now);
                })
                .verifyComplete();

        verify(this.s3Presigner, atLeastOnce()).presignGetObject(any(GetObjectPresignRequest.class));
    }

    @Test
    public void test_get_upload_file_url() throws Exception {
        PresignedPutObjectRequest putObjectRequest = mock(PresignedPutObjectRequest.class);

        when(this.s3Presigner.presignPutObject(any(PutObjectPresignRequest.class)))
                .thenReturn(putObjectRequest);

        URL url = new URL("http://some.host.com/image.gif");
        Instant now = Instant.now();

        when(putObjectRequest.url()).thenReturn(url);
        when(putObjectRequest.expiration()).thenReturn(now);

        StepVerifier.create(this.s3FileStoreOperations.getDownloadFileUrl("some-file", Duration.ofDays(1)))
                .expectSubscription()
                .assertNext(fileRef -> {
                    assertThat(fileRef.getT1().toExternalForm()).isEqualTo(url.toExternalForm());
                    assertThat(fileRef.getT2()).isEqualTo(now);
                })
                .verifyComplete();

        verify(this.s3Presigner, atLeastOnce()).presignPutObject(any(PutObjectPresignRequest.class));
    }

    @Test
    public void test_download_file_correct() throws Exception {
        String test = "Some test data";

//        S3FileStorageTemplate.FluxResponse response = new S3FileStorageTemplate.FluxResponse();
  //      response.setFluxStream(Flux.just(ByteBuffer.wrap(test.getBytes())));

        //when(this.s3AsyncClient.getObject(any(GetObjectRequest.class), any(AsyncResponseTransformer.class)))
          //      .thenReturn(CompletableFuture.supplyAsync(() -> response));

        StepVerifier.create(this.s3FileStoreOperations.downloadFile("some-file"))
                .assertNext(buf -> {
                    assertThat(new String(buf.array())).isEqualTo(test);
                })
                .verifyComplete();

        verify(this.s3AsyncClient, atLeastOnce()).getObject(any(GetObjectRequest.class), any(AsyncResponseTransformer.class));
    }

    @Test
    public void test_upload_file_correct() {
        PutObjectResponse response = mock(PutObjectResponse.class);

        when(this.s3AsyncClient.putObject(any(PutObjectRequest.class), any(AsyncRequestBody.class)))
                .thenReturn(CompletableFuture.supplyAsync(() -> response));

        StepVerifier.create(this.s3FileStoreOperations.upload("some-file", new byte[32]))
                .expectSubscription()
                .verifyComplete();

        verify(this.s3AsyncClient, atLeastOnce()).putObject(any(PutObjectRequest.class), any(AsyncRequestBody.class));
    }

    @Test
    public void test_upload_file_retry() {
        when(this.s3AsyncClient.putObject(any(PutObjectRequest.class), any(AsyncRequestBody.class)))
                .thenThrow(SdkException.builder().message("test-message").build());

        assertThatThrownBy(() -> Mono.from(this.s3FileStoreOperations.upload("some-file", new byte[132])))
                .isInstanceOf(SdkException.class);

        verify(this.s3AsyncClient, atLeast(1)).putObject(any(PutObjectRequest.class),
                any(AsyncRequestBody.class));
    }

    @Test
    public void test_delete_object() {
        DeleteObjectResponse response = mock(DeleteObjectResponse.class);

        when(this.s3AsyncClient.deleteObject(any(DeleteObjectRequest.class)))
                .thenReturn(CompletableFuture.supplyAsync(() -> response));

        StepVerifier.create(this.s3FileStoreOperations.delete("some-file"))
                .expectSubscription()
                .verifyComplete();

        verify(this.s3AsyncClient, atLeastOnce()).deleteObject(any(DeleteObjectRequest.class));
    }

    @Test
    public void test_delete_objects() {
        DeleteObjectsResponse response = mock(DeleteObjectsResponse.class);

        when(this.s3AsyncClient.deleteObjects(any(DeleteObjectsRequest.class)))
                .thenReturn(CompletableFuture.supplyAsync(() -> response));

        StepVerifier.create(this.s3FileStoreOperations.delete(Flux.just("file1", "file2", "file3")))
                .expectSubscription()
                .verifyComplete();

        verify(this.s3AsyncClient, atLeastOnce()).deleteObjects(any(DeleteObjectsRequest.class));
    }
}